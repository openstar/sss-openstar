package sss.openstar.account

import scorex.crypto.signatures.PublicKey
import sss.ancillary.Logging
import sss.openstar.UniqueNodeIdentifier
import sss.openstar.account.NodeIdentityManager.{Credentials, GetSignerDetails, PasswordCredentials, SignerDetails}
import sss.openstar.account.SignerFactory.SignerDescriptor
import sss.openstar.util.ThrowUtil.throwIllegalArgument

import java.util
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

object NodeIdentityManager {

  trait Credentials {
    def nodeIdTag: NodeIdTag
  }

  case class PasswordCredentials(
                                  nodeIdTag: NodeIdTag,
                                  password: String) extends Credentials

  case class SignerDetails(
                            nodeIdTag: NodeIdTag,
                            publicKey: PublicKey,
                            externalKeyId: Option[Array[Byte]],
                            signerDescriptor: SignerDescriptor
                          ) {
    private def eqForOpt(a: Option[Array[Byte]], b: Option[Array[Byte]]): Boolean =
      (a, b) match {
        case (None, None) => true
        case (Some(a), Some(b)) => util.Arrays.equals(a, b)
        case _ => false
      }


    override def equals(obj: Any): Boolean = obj match {
      case other: SignerDetails =>
        other.signerDescriptor == this.signerDescriptor &&
          other.nodeIdTag == this.nodeIdTag &&
          util.Arrays.equals(other.publicKey, this.publicKey) &&
          eqForOpt(other.externalKeyId, this.externalKeyId)

      case _ => false
    }

    override def hashCode(): Int = {
      signerDescriptor.hashCode() +
        nodeIdTag.hashCode() +
        externalKeyId.hashCode() +
        util.Arrays.hashCode(publicKey)
    }
  }

  //given a nodeIdentifier return the associated key details
  type GetSignerDetails = UniqueNodeIdentifier => Future[Seq[SignerDetails]]
  type GetSignerDetailsPF = PartialFunction[UniqueNodeIdentifier, Future[Seq[SignerDetails]]]

}

/**
 * Given a way to discover the details of an "Account" this will
 * produce the verifiers and signers associated with the account
 *
 * All signers will not be available to all.
 *
 * I can unlock and make a verifier for an account
 * *If* I have the credentials to unlock the signer *or* the signer is interactive
 *
 * @param keyFactory
 * @param getSignerDetails
 * @param signerFactory
 */
class NodeIdentityManager(verifierFactory: VerifierFactory,
                          getSignerDetails: GetSignerDetails,
                          signerFactory: SignerFactory)(implicit cpuEx: ExecutionContext) extends Logging {

  def nodeVerifier(nodeIdTag: NodeIdTag): Future[NodeVerifier] = {
    getSignerDetails(nodeIdTag.nodeId)
      .map(_.find(_.nodeIdTag == nodeIdTag)
        .getOrElse(throwIllegalArgument(s"No details exist for $nodeIdTag")))
      .map(details =>
        verifierFactory(details.nodeIdTag, TypedPublicKey(details.publicKey, details.signerDescriptor.keyType))
      )
  }

  def signerDetails(nodeIdentity: UniqueNodeIdentifier): Future[Seq[SignerDetails]] =
    getSignerDetails(nodeIdentity)

  @throws(classOf[IllegalArgumentException])
  def toVerifierSigner(nodeIdentity: UniqueNodeIdentifier,
                       details: SignerDetails,
                       existingVerifiers: Seq[NodeVerifierSigner] = Seq.empty,
                       seqCredentials: Seq[Credentials] = Seq.empty): Future[NodeVerifierSigner] = {

    def noNewCredentials(nTag: NodeIdTag): Boolean = !seqCredentials.exists(_.nodeIdTag == nTag)
    def signerIsAlreadyUnlocked(v: NodeVerifierSigner): Boolean = v.signerOpt.isDefined

    existingVerifiers
      .find(v => v
        .nodeIdTag.nodeId == nodeIdentity && v.signerDescriptor == details.signerDescriptor
      ) match {

      case Some(existingSignerVerifier) if signerIsAlreadyUnlocked(existingSignerVerifier) &&
        noNewCredentials(existingSignerVerifier.nodeIdTag) =>

        Future.successful(existingSignerVerifier) //<-- don't lose existing unlocked Signers, you may not have the same credentials on refresh

      case _ =>
        signerFactory
          .withCredentials(seqCredentials)(details)

    }


  }

  def createNodeIdentity(identity: UniqueNodeIdentifier,
                         seqVerifiers: Seq[NodeVerifierSigner],
                         withTag: String): NodeIdentity = new NodeIdentity {
    override def id: UniqueNodeIdentifier = identity

    override lazy val tag: UniqueNodeIdentifier = withTag

    override def verifiers: Seq[NodeVerifierSigner] = seqVerifiers

    override def refresh(moreCredentials: Seq[Credentials] = Seq.empty): Future[NodeIdentity] =
      NodeIdentityManager.this.refresh(this, moreCredentials)
  }

  def apply(nodeIdTag: NodeIdTag,
            password: String): Future[NodeIdentity] =
    apply(nodeIdTag.nodeId, nodeIdTag.tag, password)

  def apply(identity: UniqueNodeIdentifier,
            tag: String,
            password: String
            ): Future[NodeIdentity] =
    apply(identity, tag, Seq(PasswordCredentials(NodeIdTag(identity, tag), password)))

  def refresh(nodeIdentity: NodeIdentity,
              credentials: Seq[Credentials] = Seq.empty): Future[NodeIdentity] =
    makeNodeId(nodeIdentity.id, nodeIdentity.tag, credentials, nodeIdentity.verifiers)


  private def makeNodeId(
                          id: UniqueNodeIdentifier,
                          tag: String,
                          credentials: Seq[Credentials],
                          verifiers: Seq[NodeVerifierSigner] = Seq.empty): Future[NodeIdentity] = for {
    seqDetails <- getSignerDetails(id)
    seqVerifiersOpts <- Future.sequence(
      seqDetails.map(details =>
        Try(toVerifierSigner(id, details, verifiers, credentials)) match {
          case Failure(exception) =>
            log.warn(s"Failed to create verifier Signer", exception)
            Future.successful(None)
          case Success(valueF) =>
            valueF map (Some(_)) recover {
              case e =>
                log.warn(s"Future failed to create verifier Signer", e)
                None
            }
        }
      )
    )
    seqVerifiers = seqVerifiersOpts.collect {
      case Some(v) => v
    }
    ni = createNodeIdentity(id, seqVerifiers, tag)
  } yield ni

  def apply(identity: UniqueNodeIdentifier,
            tag: String,
            credentials: Seq[Credentials] = Seq.empty): Future[NodeIdentity] =
    makeNodeId(identity, tag, credentials)



}