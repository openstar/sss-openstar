package sss.openstar.account

import sss.openstar.account.NodeSigner.KeyType
import sss.openstar.util.Lookup

trait KeyFactory extends Lookup[KeyType, AccountKeys] {
  def supportedKeyTypes: Seq[KeyType]
}




