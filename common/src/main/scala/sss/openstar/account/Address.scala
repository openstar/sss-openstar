package sss.openstar.account


import scorex.util.encode.Base58
import sss.openstar.hash.SecureCryptographicHash._

case class Address(value: String) {

  override def toString: String = value

  override def equals(b: Any): Boolean = b match {
    case a: Address => a.value == value
    case _ => false
  }

  override def hashCode(): Int = value.hashCode()
}


object Address {

  val AddressVersion: Byte = 1
  val ChecksumLength = 4
  val HashLength = 20
  val AddressLength = 1 + ChecksumLength + HashLength

  /**
    * Create account from public key. Used in PublicKeyAccount/PrivateKeyAccount.
    */
  def fromPubkey(publicKey: Array[Byte]): String = {
    val publicKeyHash = hash(publicKey).take(HashLength)
    val withoutChecksum = AddressVersion +: publicKeyHash //prepend ADDRESS_VERSION
    Base58.encode(withoutChecksum ++ calcCheckSum(withoutChecksum))
  }

  def isValidAddress(address: String): Boolean =
    Base58.decode(address).map { addressBytes =>
      if (addressBytes.length != Address.AddressLength)
        false
      else {
        val checkSum = addressBytes.takeRight(ChecksumLength)

        val checkSumGenerated = calcCheckSum(addressBytes.dropRight(ChecksumLength))

        checkSum.sameElements(checkSumGenerated)
      }
    }.getOrElse(false)

  private def calcCheckSum(withoutChecksum: Array[Byte]): Array[Byte] = hash(withoutChecksum).take(ChecksumLength)

}
