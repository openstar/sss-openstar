package sss.openstar.account

import scorex.crypto.signatures.{MessageToSign, PrivateKey, PublicKey, SharedSecret, Signature}
import sss.openstar.account.NodeSigner.KeyType
import sss.openstar.hash.SecureCryptographicHash

import java.util


object KeyPair {
  def apply(tup: (Array[Byte], Array[Byte])): KeyPair = KeyPair(PublicKey(tup._2), PrivateKey(tup._1))
}
case class KeyPair(pub: PublicKey, priv: PrivateKey) {
  val tuple: (Array[Byte], Array[Byte]) = (priv, pub)
}

case class TypedPublicKey(publicKey: PublicKey, keyType: KeyType) {

  override def equals(obj: Any): Boolean = obj match {
    case that: TypedPublicKey =>
      util.Arrays.equals(that.publicKey, publicKey) &&
        that.keyType == keyType

    case _ => false
  }


  override def hashCode(): Int =
    util.Arrays.hashCode(publicKey) +
    keyType.hashCode

}

trait Signer {
  def keyType: KeyType
  def keypair: KeyPair
  def sign(msg: MessageToSign): Signature
  def createSharedSecret(otherPublicKey: PublicKey): SharedSecret
  def createSharedSecret(otherPublicKey: TypedPublicKey): SharedSecret = {
    require(keyType == otherPublicKey.keyType, s"Local ${keyType} not same as param ${otherPublicKey.keyType}")
    createSharedSecret(otherPublicKey.publicKey)
  }
  def typedPublicKey: TypedPublicKey = TypedPublicKey(keypair.pub, keyType)
}

trait Verifier {
  def typedPublicKey: TypedPublicKey
  def verify(message: MessageToSign, signature: Signature): Boolean
}

object AccountKeys {
  type AccountVerifyAndDets = AccountKeysVerify with AccountKeyDets
  type AccountSignAndDets = AccountKeysSign with AccountKeyDets
}

trait AccountKeyDets {
  def KeyLength: Int
  def SigLength: Int
}

trait AccountKeysType {
  val keysType: KeyType

  override def equals(obj: Any): Boolean = obj match {
    case that: AccountKeysType => keysType == that.keysType
    case _ => false
  }

  override def hashCode(): Int = keysType.hashCode
}


/**
 * Creates keys of different types that can adhere to the interface
 */
trait AccountKeysFactory extends AccountKeysType {

  implicit val accountKeysImp: AccountKeys

  lazy val keysType: KeyType = accountKeysImp.keysType

  def keyFactoryMatcher: PartialFunction[KeyType, AccountKeysFactory] = {
    case `keysType` => this
  }

  def createKeyPair(): KeyPair

  def keySigner(): Signer =
    keySigner(createKeyPair())

  def keySigner(keyPair: KeyPair): Signer = new Signer {

    override def keyType: KeyType = keysType

    override def keypair: KeyPair = keyPair

    override def sign(msg: MessageToSign): Signature =
      accountKeysImp.sign(keyPair.priv, msg)

    override def createSharedSecret(otherPublicKey: PublicKey): SharedSecret =
      accountKeysImp.createSharedSecret(keyPair.priv, otherPublicKey)
  }

  def keyVerifier(pubKey: PublicKey): Verifier = new Verifier {

    override def typedPublicKey: TypedPublicKey = TypedPublicKey(pubKey, keysType)

    override def verify(message: MessageToSign, signature: Signature): Boolean =
      accountKeysImp.verify(typedPublicKey.publicKey, message, signature)
  }

}

trait AccountKeysSign {
  def sign(privateKey: PrivateKey, message: MessageToSign): Signature
  def signer(privateKey: PrivateKey): MessageToSign => Signature = msg => sign(privateKey, msg)
}

trait AccountKeysVerify {
  def verify(publicKey: PublicKey, message: MessageToSign, sig: Signature): Boolean
  def verifier(publicKey: PublicKey): (MessageToSign, Signature) => Boolean = (msg, sig) => verify(publicKey, msg, sig)
}

trait AccountKeysSharedSecrets {
  //TODO use a proper key derivation technique.
  def createSharedSecret(aPrivateKey: PrivateKey, aPublicKey: PublicKey): SharedSecret
  def createHashedSharedSecret(aPrivateKey: PrivateKey, aPublicKey: PublicKey): SharedSecret = {
    val secret = createSharedSecret(aPrivateKey, aPublicKey)
    SharedSecret(SecureCryptographicHash.hash(secret))
  }
}
trait AccountKeys
  extends AccountKeysType
    with AccountKeyDets
    with AccountKeysSign
    with AccountKeysVerify
    with AccountKeysSharedSecrets {

  def keysFactory: AccountKeysFactory

  def keyTypeMatcher: PartialFunction[KeyType, AccountKeys] = {
    case `keysType` => this
  }

}
