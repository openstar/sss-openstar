package sss.openstar.account

import scorex.crypto.signatures.Signature
import sss.openstar.ledger.TxSig

import java.nio.charset.StandardCharsets
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

object Ops {

  implicit class MakeTxSigMaker(val n: NodeIdentity)(implicit ec: ExecutionContext) {
    def txSigMaker: Array[Byte] => Future[TxSig] = txId => {
      n.defaultNodeVerifier.signer.sign(txId).map { sig =>
        TxSig(Seq(Seq(n.idBytes, n.tagBytes, sig)))
      }
    }
  }

  implicit class ExtractFromSigs(val txSig: TxSig) extends AnyVal {
    def toTryTruple: Try[(String, String, Signature)] = Try {
      val s = txSig.sigs.head
      require(s.size == 3, "Sig must contain id, tag and sig in bytes")
      val id = new String(s.head, StandardCharsets.UTF_8)
      val tag =  new String(s(1), StandardCharsets.UTF_8)
      (id, tag, Signature(s(2)))
    }
  }

  implicit class MakeTxSig(val n: NodeIdentity) extends AnyVal {
    def txSig(txId: Array[Byte], others: Seq[Seq[Array[Byte]]] = Seq(Seq()))(implicit ec: ExecutionContext): Future[TxSig] = {
      n.defaultNodeVerifier.signer.sign(txId).map { sig =>
        TxSig(Seq(n.idBytes, n.tagBytes, sig) +: others)
      }
    }
  }

  implicit class MakeTxSigSigner(val signer: NodeSigner) extends AnyVal {
    def txSig(txId: Array[Byte], others: Seq[Seq[Array[Byte]]] = Seq(Seq()))(implicit ec: ExecutionContext): Future[TxSig] = {
      signer.sign(txId).map { sig =>
        TxSig(
          Seq(
            signer.verifier.nodeIdTag.nodeIdBytes,
            signer.verifier.nodeIdTag.tagBytes,
            sig) +: others
        )
      }
    }
  }

  implicit class MakeTxSigArys(val sigs: Seq[Seq[Array[Byte]]]) extends AnyVal {
    def txSig: TxSig = TxSig(sigs)
  }
}
