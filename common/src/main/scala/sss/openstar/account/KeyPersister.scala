package sss.openstar.account


import sss.ancillary.Logging
import sss.openstar.common.users.MementoKeyPersist
import sss.openstar.crypto.keypairs.CBCKeyEncryption
import sss.openstar.crypto.password.PasswordRules
import sss.openstar.util.IOExecutionContext

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

trait KeyPersistWrite {
  def write(identity: String, tag: String, content: String): Future[Unit]
  def remove(identity: String, tag: String): Future[Boolean]
}

trait KeyPersistRead {
  def read(identity: String, tag: String): Future[Option[String]]

  def exists(identity: String, tag: String)(implicit ec: ExecutionContext): Future[Boolean] =
    read(identity, tag).map(_.isDefined)
}

object KeyPersister {
  private[openstar] def fileOnly(implicit ioEc: IOExecutionContext): KeyPersister = {
    new KeyPersister(Seq(MementoKeyPersist), MementoKeyPersist, CBCKeyEncryption)
  }
}
/**
 * Persists the key pair, tag and identity...
 *
 */
private[openstar] class KeyPersister(
                                      orderedSeqKeyReader: Seq[KeyPersistRead],
                                      keyWriter: KeyPersistWrite,
                                      keyEncryption: KeyEncryption)(implicit ioEc: IOExecutionContext) extends Logging {

  import ioEc.ec

  def deleteKey(identity: String, tag: String): Future[Boolean] =
    keyWriter.remove(identity, tag)

  def keyExists(identity: String, tag: String): Future[Boolean] = {

    Future.sequence(orderedSeqKeyReader.map(_.read(identity, tag))).map { founds =>
      // find the first one, the order of key readers is important
      founds
        .exists(_.isDefined)
    }
  }

  def findOrCreate(
                    identity: String,
                    tag: String,
                    phrase: String,
                    keyGenerator: () => KeyPair)
  : Future[KeyPair] = {

    findAndRetrieve(identity, tag, phrase).flatMap {

      case Some(found) =>
        Future.successful(KeyPair(found))

      case None =>
        val newKey = keyGenerator()
        val encString = keyEncryption.encrypt(newKey.tuple, phrase)
        keyWriter.write(identity, tag, encString).map { _ =>
          newKey
        }
    }
  }


  def findAndRetrieve(
            identity: String,
            tag: String,
            phrase: String
          ): Future[Option[(Array[Byte], Array[Byte])]] = {

    require(PasswordRules.isValidPassword(phrase), PasswordRules.descriptiveMessage)

    Future.sequence(orderedSeqKeyReader.map(_.read(identity, tag))).map { founds =>
      // find the first one, the order of key readers is important
      founds
        .find(_.isDefined)
        .flatten
        .map { encStr =>

          keyEncryption.decrypt(encStr, phrase) match {
            case Failure(exception) =>
              log.warn(s"Key decryption for ${identity} ${tag} failed!", exception)
              throw exception
            case Success(value) => value
          }
        }
    }
  }

}