package sss.openstar.account

import sss.openstar.account.NodeIdentityManager.{Credentials, SignerDetails}
import sss.openstar.account.NodeSigner.{InteractionType, KeyType, SecurityLevel, SignerType}
import sss.openstar.account.SignerFactory.SignerDescriptor
import sss.openstar.util.Lookup

import scala.concurrent.Future
import scala.util.Try

object SignerFactory {

  case class SignerDescriptor(
                               keyType: KeyType,
                               securityLevel: SecurityLevel,
                               signerType: SignerType,
                               interactionType: InteractionType
                             )
}


trait SignerFactory extends Lookup[SignerDetails, Future[NodeVerifierSigner]] {

  def seqCredentials: Seq[Credentials]

  protected def mergeCredentials(existing: Seq[Credentials], additional: Seq[Credentials]): Seq[Credentials] = {
    val additionalNodeTags = additional.map(_.nodeIdTag)
    val notReplacedByAdditional = existing.filterNot(cred => additionalNodeTags.contains(cred.nodeIdTag))
    additional ++ notReplacedByAdditional
  }

  final def withCredentials(seqCredentials: Seq[Credentials]): SignerFactory = {
    withMergedCredentials(mergeCredentials(this.seqCredentials, seqCredentials))
  }

  //implement this with simple case class copy
  protected def withMergedCredentials(seqMergedCredentials: Seq[Credentials]): SignerFactory

  def signerDescriptors: Seq[SignerDescriptor]

  def merge(otherSignerFactory: SignerFactory): SignerFactory = MergeSignerFactory(
    this, otherSignerFactory
  )
}

case class MergeSignerFactory(
                               a: SignerFactory,
                               b: SignerFactory) extends SignerFactory {

  override lazy val seqCredentials: Seq[Credentials] = mergeCredentials(a.seqCredentials, b.seqCredentials)



  override def lookupMatcher: PartialFunction[SignerDetails, Future[NodeVerifierSigner]] =
    a.lookupMatcher orElse b.lookupMatcher

  override protected def withMergedCredentials(seqMergedCredentials: Seq[Credentials]): SignerFactory = {
    a.withCredentials(seqMergedCredentials).merge(b.withCredentials(seqMergedCredentials))
  }

  override def signerDescriptors: Seq[SignerDescriptor] =
    a.signerDescriptors ++ b.signerDescriptors

}

trait CreatesSigner {
  def signerMatcher: PartialFunction[SignerDescriptor, CreatesSigner]
  def createSigner(
                    signerDetails: SignerDetails,
                    seqCreds: Seq[Credentials]): Future[NodeVerifierSigner]
}