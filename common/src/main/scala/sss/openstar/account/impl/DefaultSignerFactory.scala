package sss.openstar.account.impl

import sss.ancillary.Logging
import sss.openstar.account.NodeIdentityManager.{Credentials, SignerDetails}
import sss.openstar.account.SignerFactory.SignerDescriptor
import sss.openstar.account.{NodeVerifierSigner, SignerFactory, SignerInteractionTypes}
import sss.openstar.crypto.keypairs.{Curve25519AccountKeys, Ed25519AccountKeys}

import scala.concurrent.Future

case class DefaultSignerFactory(localPrivateKeyAccountSignerCreator: EncryptedKeyFileAccountSignerCreator,
                                seqCredentials: Seq[Credentials] = Seq.empty,
                               ) extends SignerFactory with Logging {


  override def lookupMatcher: PartialFunction[SignerDetails, Future[NodeVerifierSigner]] = {
    case s if localPrivateKeyAccountSignerCreator
      .signerMatcher.isDefinedAt(s.signerDescriptor) =>
      localPrivateKeyAccountSignerCreator.signerMatcher(s.signerDescriptor).createSigner(s, seqCredentials)
  }

  protected override def withMergedCredentials(seqMergedCredentials: Seq[Credentials]): SignerFactory =
    copy(seqCredentials = seqMergedCredentials)

  override def signerDescriptors: Seq[SignerDescriptor] = Seq(
    SignerDescriptor(
      Curve25519AccountKeys.keysType,
      EncryptedKeyFileAccountSignerCreator.SecurityLevel,
      EncryptedKeyFileAccountSignerCreator.SignerType,
      SignerInteractionTypes.AutomaticSignerInteractionType
    ),
    SignerDescriptor(
      Ed25519AccountKeys.keysType,
      EncryptedKeyFileAccountSignerCreator.SecurityLevel,
      EncryptedKeyFileAccountSignerCreator.SignerType,
      SignerInteractionTypes.AutomaticSignerInteractionType
    )
  )

}
