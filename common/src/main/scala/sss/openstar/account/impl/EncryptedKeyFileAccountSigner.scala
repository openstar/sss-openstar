package sss.openstar.account.impl

import scorex.crypto.signatures._
import sss.ancillary.FutureOps.AwaitResult
import sss.openstar.account.NodeIdentityManager.{Credentials, PasswordCredentials, SignerDetails}
import sss.openstar.account.NodeSigner.{InteractionType, SecurityLevel, SignerType}
import sss.openstar.account.SignerFactory.SignerDescriptor
import sss.openstar.account._
import sss.openstar.account.impl.EncryptedKeyFileAccountSignerCreator.{InteractionType, MaxExpectedSignatureWaitDuration, SecurityLevel, SignerType}
import sss.openstar.util.TryOps.FutureFromT

import scala.concurrent.duration.{Duration, DurationInt}
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

object EncryptedKeyFileAccountSignerCreator {
  val SignerType: SignerType = "LocalPrivateKeyAccountSigner"
  val SecurityLevel: SecurityLevel = SignerSecurityLevels.SecurityLevel1
  val InteractionType: InteractionType = SignerInteractionTypes.AutomaticSignerInteractionType
  val MaxExpectedSignatureWaitDuration: Duration = 5.seconds
}

class EncryptedKeyFileAccountSignerCreator(
                                           keyPersister: KeyPersister,
                                           keysFactoryLookup: KeyFactory)(implicit ec: ExecutionContext) extends CreatesSigner {


  override def signerMatcher: PartialFunction[SignerFactory.SignerDescriptor, CreatesSigner] = {
    case SignerDescriptor(
    keyType,
    secLevel,
    SignerType,
    InteractionType
    ) if SecurityLevel >= secLevel &&
      keysFactoryLookup.contains(keyType) => this
  }

  override def createSigner(
                             signerDetails: SignerDetails,
                             seqCredentials: Seq[Credentials]): Future[NodeVerifierSigner] = {


    seqCredentials.find(_.nodeIdTag == signerDetails.nodeIdTag) match {
      case Some(PasswordCredentials(nodeIdTag, password)) =>
        keyPersister
          .findAndRetrieve(nodeIdTag.nodeId, nodeIdTag.tag, password)
          .map(_.getOrElse(throw new IllegalArgumentException(s"No key found for $nodeIdTag")))
          .map {
            case (priv, _) =>
              val keyTypeImpl = keysFactoryLookup
                .get(signerDetails.signerDescriptor.keyType)
                .getOrElse(throw new IllegalArgumentException(s"No key type on record for ${signerDetails.signerDescriptor.keyType}"))

              new EncryptedKeyFileAccountSigner(signerDetails, PrivateKey(priv), keyTypeImpl)
          }
      case Some(_) =>
        Future.failed(new IllegalArgumentException(s"No recognised credentials found for ${signerDetails.nodeIdTag}"))
      case None =>
        Future.failed(new IllegalArgumentException(s"No password credentials found for ${signerDetails.nodeIdTag}"))
    }
  }
}

class EncryptedKeyFileAccountSigner(
                                    signerDetails: SignerDetails,
                                    priv: PrivateKey,
                                    accountKeys: AccountKeys) extends NodeVerifierSigner {

  require(
    signerDetails.signerDescriptor.keyType == accountKeys.keysType,
    s"Impl key type ${accountKeys.keysType} not matching ${signerDetails.signerDescriptor.keyType}"
  )

  val signerOpt = Some(new NodeSigner {

    override def sign(msg: MessageToSign, timeoutAfter: Duration): Future[Signature] =
      accountKeys.sign(priv, msg).tryAsFuture

    override def createSharedSecret(publicKey: PublicKey, timeoutAfter: Duration): Future[SharedSecret] =
      accountKeys.createSharedSecret(priv, publicKey).tryAsFuture

    override def verifier: NodeVerifierSigner = EncryptedKeyFileAccountSigner.this

    override def maxExpectedSignatureWaitDuration: Duration = MaxExpectedSignatureWaitDuration
  })

  override def signerDescriptor: SignerDescriptor = SignerDescriptor(
    signerDetails.signerDescriptor.keyType, SecurityLevel, SignerType, InteractionType
  )

  override def verifier: Verifier = accountKeys.keysFactory.keyVerifier(signerDetails.publicKey)

  override def nodeIdTag: NodeIdTag = signerDetails.nodeIdTag
}
