package sss.openstar.account.serialize

import sss.ancillary.Serialize.{SerializeHelper, Serializer, StringDeSerialize, StringSerializer}
import sss.openstar.account.NodeIdTag

object NodeIdTagSerializer extends Serializer[NodeIdTag] {
  override def toBytes(t: NodeIdTag): Array[Byte] =
    StringSerializer(t.nodeId) ++ StringSerializer(t.tag).toBytes

  override def fromBytes(b: Array[Byte]): NodeIdTag = {
    val (n, t) = (b.extract(StringDeSerialize, StringDeSerialize))
    NodeIdTag(n, t)
  }
}
