package sss.openstar.account.serialize

import scorex.crypto.signatures.PublicKey
import sss.ancillary.Serialize.{ByteArrayDeSerialize, ByteArraySerializer, SerializeHelper, Serializer, StringDeSerialize, StringSerializer}
import sss.openstar.account.TypedPublicKey

object TypedPublicKeySerializer extends Serializer[TypedPublicKey] {
  override def toBytes(t: TypedPublicKey): Array[Byte] =
    ByteArraySerializer(t.publicKey) ++ StringSerializer(t.keyType).toBytes

  override def fromBytes(b: Array[Byte]): TypedPublicKey =
    TypedPublicKey.tupled(b.extract(ByteArrayDeSerialize(PublicKey(_)), StringDeSerialize))
}
