package sss.openstar.account

import scorex.crypto.signatures.{MessageToSign, PublicKey, SharedSecret, Signature}
import sss.openstar.account.NodeSigner.{InteractionType, SecurityLevel}
import sss.openstar.account.SignerFactory.SignerDescriptor

import scala.concurrent.Future
import scala.concurrent.duration.Duration


object SignerInteractionTypes {
  val ManualSignerInteractionType: InteractionType = "Manual"
  val AutomaticSignerInteractionType: InteractionType = "Automatic"
}

object SignerSecurityLevels {
  val InSecure: SecurityLevel = 0
  val SecurityLevel1: SecurityLevel = 1 // Key protected by user password Hashed and stored server side
  val SecurityLevel2: SecurityLevel = 2 // Stored encrypted server side with user password
  val SecurityLevel3: SecurityLevel = 3 // System never knows user secret
  // ... next level could be key storage in hardware wallet.

}

object NodeSigner {
  type SignerType = String
  type SecurityLevel = Int
  type InteractionType = String
  type KeyType = String
}

trait NodeSigner {
  def maxExpectedSignatureWaitDuration: Duration
  def verifier: NodeVerifierSigner
  def sign(msg: MessageToSign, timeoutAfter: Duration = maxExpectedSignatureWaitDuration): Future[Signature]

  def createSharedSecret(publicKey: PublicKey, timeoutAfter: Duration): Future[SharedSecret]

  def createSharedSecret(typedPublicKey: TypedPublicKey,
                         timeoutAfter: Duration = maxExpectedSignatureWaitDuration): Future[SharedSecret] = {
    require(typedPublicKey.keyType == verifier.signerDescriptor.keyType, "Cannot create chared secret unless the keys are the same type")
    createSharedSecret(typedPublicKey.publicKey, timeoutAfter)
  }

}


