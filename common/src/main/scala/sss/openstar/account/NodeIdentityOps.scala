package sss.openstar.account

import sss.openstar.account.NodeSigner.SecurityLevel
import sss.openstar.crypto.ECEncryption.CreateSharedSecretF
import sss.openstar.util.ThrowUtil.throwIllegalArgument

object NodeIdentityOps {

  implicit class Finder(val n: NodeIdentity) extends AnyVal {

    def verifierSigners(minSecurityLevel: SecurityLevel): Seq[NodeVerifierSigner] =
      n.verifiers.filter(_.signerDescriptor.securityLevel >= minSecurityLevel)

    @throws[IllegalArgumentException]
    def sharedSecretCreator(
                           minSecurityLevel: SecurityLevel
                           ): CreateSharedSecretF = typedPublicKey => {
      verifierSigners(minSecurityLevel)
        .filter(_.signerDescriptor.keyType == typedPublicKey.keyType)
        .find(_.signerOpt.isDefined)
        .getOrElse(throwIllegalArgument(s"${n.id} has no available signer for ${typedPublicKey.keyType}"))
        .signer
        .createSharedSecret(typedPublicKey)
    }
  }
}
