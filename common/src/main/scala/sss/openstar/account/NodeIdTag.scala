package sss.openstar.account

import java.nio.charset.StandardCharsets

object NodeIdTag {
  val defaultTag: String = "defaultTag"
}
case class NodeIdTag(nodeId: String, tag: String = NodeIdTag.defaultTag) {
  lazy val tagBytes: Array[Byte] = tag.getBytes(StandardCharsets.UTF_8)
  lazy val nodeIdBytes: Array[Byte] = nodeId.getBytes(StandardCharsets.UTF_8)
  def hasTag(aTag: String): Boolean = tag == aTag

}
