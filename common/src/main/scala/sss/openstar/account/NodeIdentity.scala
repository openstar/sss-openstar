package sss.openstar.account


import scorex.crypto.signatures.PublicKey
import sss.openstar.UniqueNodeIdentifier
import sss.openstar.account.NodeIdentityManager.Credentials
import sss.openstar.account.NodeSigner.{InteractionType, KeyType, SecurityLevel}

import java.nio.charset.StandardCharsets
import scala.concurrent.Future

/**
  * Node identity binds a string identifier to a set of public private keys.
  * The id is the main identifier, but the tag signifies a different pair of keys for
  * the same 'id'.
  */

object NodeIdentity {


  def filterVerifiers(verifiers: Seq[NodeVerifierSigner],
                      requiredInteractionType: Option[InteractionType],
                      minSecurityLevel: SecurityLevel): Seq[NodeVerifierSigner] = {

    requiredInteractionType.map(interactionType => verifiers
      .filter(_.signerDescriptor.interactionType == interactionType))
      .getOrElse(verifiers) //use verifiers if no "required interaction type" was supplied
      .filter(_.signerDescriptor.securityLevel >= minSecurityLevel)
  }

  def findVerifier(verifiers: Seq[NodeVerifierSigner],
                   requiredInteractionType: Option[InteractionType],
                   minSecurityLevel: SecurityLevel): Option[NodeVerifierSigner] =
    filterVerifiers(verifiers, requiredInteractionType, minSecurityLevel).headOption


  def findVerifierSigner(verifiers: Seq[NodeVerifierSigner],
                         requiredInteractionType: Option[InteractionType],
                         minSecurityLevel: SecurityLevel): Option[NodeVerifierSigner] = {
    filterVerifiers(verifiers, requiredInteractionType, minSecurityLevel).find(_.signerOpt.isDefined)
  }
}

/**
 * Entry point for interacting with identities.
 * This is where the identifier's verifiers and signers are linked.
 */
trait NodeIdentity {

  def verifiers: Seq[NodeVerifierSigner]

  //loads any newly added verifiers/signers
  //Will use newly added credentials to replace old unlocked verifiers
  //Will preserve older unlocked signers if no new creds are provided
  //Will drop signers that no longer exist
  def refresh(seqCredentials: Seq[Credentials]): Future[NodeIdentity]

  def findVerifier(tag: String = NodeIdTag.defaultTag): Option[NodeVerifierSigner] =
    verifiers.find(_.nodeIdTag.tag == tag)

  lazy val defaultNodeVerifier = {
    val tmp = findVerifier(tag)
      .getOrElse(
        throwBadState(s"Default Verifier MUST exist id:${id} tag:${tag}")
      )
    //require(tmp.signerDescriptor.interactionType == AutomaticSignerInteractionType, "The default signer *must* not require user intervention for each signature")
    tmp
  }

  lazy val defaultKeyType: KeyType = defaultNodeVerifier.verifier.typedPublicKey.keyType
  lazy val publicKey: PublicKey = defaultNodeVerifier.verifier.typedPublicKey.publicKey
  def id: UniqueNodeIdentifier
  def tag: String
  lazy val idBytes: Array[Byte] = id.getBytes(StandardCharsets.UTF_8)
  lazy val tagBytes: Array[Byte] = tag.getBytes(StandardCharsets.UTF_8)

  private def throwBadState(msg: String) = throw new IllegalStateException(msg)
}
