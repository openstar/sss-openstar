package sss.openstar.account

import sss.openstar.account.SignerFactory.SignerDescriptor
import sss.openstar.util.ThrowUtil

trait NodeVerifierSigner extends NodeVerifier {
  def signerDescriptor: SignerDescriptor
  def signerOpt: Option[NodeSigner]
  def signer: NodeSigner = signerOpt.getOrElse(ThrowUtil.throwIllegalState(s"${nodeIdTag} cannot supply a signer! (${signerDescriptor})"))
}

trait NodeVerifier {
  def nodeIdTag: NodeIdTag
  def verifier: Verifier
}

class VerifierFactory(keyFactory: KeyFactory) {

  def apply(aNodeIdTag: NodeIdTag, aTypedPublicKey: TypedPublicKey): NodeVerifier = new NodeVerifier {
    override def nodeIdTag: NodeIdTag = aNodeIdTag

    override def verifier: Verifier =
      keyFactory(aTypedPublicKey.keyType)
        .keysFactory
        .keyVerifier(aTypedPublicKey.publicKey)
  }

}