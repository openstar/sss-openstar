package sss.openstar.account

import scala.util.Try

/**
 * Keys need to be stored encrypted in storage
 */
trait KeyEncryption {
  def encrypt(keyPair: (Array[Byte], Array[Byte]), passPhrase: String): String
  def decrypt(encrypted: String, passPhrase: String): Try[(Array[Byte], Array[Byte])]
}