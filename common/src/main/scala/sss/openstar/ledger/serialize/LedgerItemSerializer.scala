package sss.openstar.ledger.serialize

import sss.ancillary.SeqSerializer
import sss.ancillary.Serialize.Serializer
import sss.openstar.ledger.{LedgerItem, SeqLedgerItem, TxIdLen}

object LedgerItemSerializer extends Serializer[LedgerItem] {

  override def toBytes(l: LedgerItem): Array[Byte] =
   l.ledgerId.id +: (l.txId ++ l.txEntryBytes)

  override def fromBytes(bytes: Array[Byte]): LedgerItem = {
    val ledgerId = bytes.head
    val (ledgerTxId, ledgerBytes) = bytes.tail.splitAt(TxIdLen)
    LedgerItem(ledgerId, ledgerTxId, ledgerBytes)
  }
}

object SeqLedgerItemSerializer extends Serializer[SeqLedgerItem] {

  override def toBytes(v: SeqLedgerItem): Array[Byte] = {
    SeqSerializer.toBytes(v.value map LedgerItemSerializer.toBytes)
  }

  override def fromBytes(b: Array[Byte]): SeqLedgerItem = {
    SeqLedgerItem(
      SeqSerializer.fromBytes(b) map LedgerItemSerializer.fromBytes
    )
  }

}