package sss.openstar.ledger

import akka.actor.{Actor, ActorLogging, Stash, Status}
import akka.pattern.pipe
import sss.db.Db
import sss.openstar.chains.Chains.Checkpointer
import sss.openstar.hash.Digest32
import sss.openstar.ledger.LedgerCheckpoints.Checkpoint

import scala.util.{Failure, Success}

trait CheckpointSupport {

  me: Actor with Stash with ActorLogging =>


  val checkpoint: Checkpointer

  // TODO checkpoints won't handle hi loads and should be rewritten.
  protected def isCheckpointHeight(height: Long): Boolean = LedgerCheckpoints.isCheckpointHeight(height)


  protected def passControlToCheckpoint(height: Long)(implicit db: Db): Unit = {
    if (LedgerCheckpoints.isCheckpointHeight(height)) {
      LedgerCheckpoints.getLastCheckpoint match {
        case Some(Checkpoint(`height`, _)) =>
        case _ =>
          log.info(s"Chain checkpoint {} begins", height)
          import context.dispatcher
          context.become(waitingForCheckpoint, false)
          checkpoint(height) pipeTo self
      }
    }
  }

  protected def waitingForCheckpoint: Receive = {

    case Failure(e) =>
      log.error(s"Chain checkpoint failed {} ", e)
      context.unbecome()
      unstashAll()

    case Status.Failure(e) =>
      log.error(s"Chain checkpoint failed {} ", e)
      context.unbecome()
      unstashAll()

    case Success(d: Digest32) =>
      context.unbecome()
      log.info(s"Checkpoint successful $d")
      unstashAll()

    case x => stash()
  }

}
