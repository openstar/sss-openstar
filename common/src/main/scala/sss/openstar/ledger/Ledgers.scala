package sss.openstar.ledger

import sss.db.ops.DbOps.DbRunOps
import sss.db.{Db, FutureTx}
import sss.openstar.common.block.{BlockId, BlockTx}
import sss.openstar.hash.Digest32
import sss.openstar.ledger.LedgerOps.FutureTxTOps
import sss.openstar.util.HashUtils

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

class Ledgers(ledgers: Map[Byte, Ledger],
              applicationLedgers: Map[Byte, Ledger],
              tollLedger: TollLedger) {

  val all = Map(tollLedger.ledgerId.id -> tollLedger) ++ ledgers ++ applicationLedgers

  def initializeLedgers(): Seq[Try[String]] = {
    ordered.foldLeft[Seq[Try[String]]](Nil) {
      case (acc, e) if acc.isEmpty => acc :+ e.initialize()
      case (acc, e) if acc.last.isSuccess => acc :+ e.initialize()
      case (acc, _) => acc
    }
  }

  val ordered: Seq[Ledger] = {
    all
      .keys
      .toSeq
      .sortWith(_ < _).map(all)
  }

  /*
  Can we debit this users account ok?
   */
  def validate(blockTx: BlockTx)(implicit ec:ExecutionContext, db: Db): Future[Unit] = Try {

    blockTx.ledgerItems.value.foldLeft(FutureTx.unit(())) {
      case (acc, e) =>
        val entry = e.txEntryBytes.toSignedTxEntry
        acc.flatMap(_ => tollLedger.validate(e.ledgerId, entry, blockTx.index)) //check it
    }.dbRun

  } match {
    case Failure(exception) => Future.failed(exception)
    case Success(value) => value
  }

  def applyFTx(
                ledgerItem: LedgerItem,
                blockId: BlockId,
                txSeqApplied: TxSeqApplied
              ): LedgerResultFTx = {

    (for {
      _ <- FutureTx.unit(log.info(s"Applying ${ledgerItem.txIdHexStr} $blockId"))
      res <- tollLedger.applyFTx(ledgerItem, blockId, txSeqApplied)

    } yield res).flatMap {
        case Right(tollTxApplied: TxSeqApplied) =>

          val updatedEvents = txSeqApplied.withTxResult(tollTxApplied)
          log.info(s" TollTxApplied $tollTxApplied $blockId")
          val coreLedgerOpt = ledgers
            .get(ledgerItem.ledgerId.id)

          val nextLedger =
            if (coreLedgerOpt.isEmpty) applicationLedgers.get(ledgerItem.ledgerId.id)
            else coreLedgerOpt

          nextLedger.map { ledger =>
            Try(ledger.applyFTx(ledgerItem, blockId, updatedEvents)) match {
              case Failure(exception) =>
                FutureTx.unit(
                  Right(
                    updatedEvents
                      .withTxResult(
                        TxNotApplied(ledgerItem.txId, exception.getMessage)(ledger.ledgerId)
                      )
                  )
                )

              case Success(value) =>
                value.map(_.map(updatedEvents.withTxResult))
                  .unbreakable(t =>
                    Right(updatedEvents.withTxResult(TxNotApplied(ledgerItem.txId, t.getMessage)(ledger.ledgerId))))
            }
          }.getOrElse(FutureTx.unit(Right(updatedEvents)))

        case err =>
          FutureTx.unit(err)
      }
  }

  def coinbase(
                forBlockHeight: Long
              ): Iterable[LedgerItem] = {
    val opts = all map {
      case (k, v) =>
        v.coinbase(
          forBlockHeight,
          k
        )
    }
    opts collect { case Some(le) => le }
  }

  def importState(height: Long): Try[Unit] = Try {
    ordered.foreach(_.importState(height).get)
  }

  def deleteCheckpoint(height: Long): Try[Unit] = Try {
    ordered.foreach(_.deleteCheckpoint(height).get)
  }

  def checkpoint(height: Long): Try[Digest32] = {
    val digests = ordered.map(_.createCheckpoint(height))
    digests.foreach {
      case Failure(e) =>
        log.error(s"H:$height checkpoint - $e")
      case Success(digest) =>
        log.info(s"H:$height Digest:${digest.asString}")
    }
    HashUtils(
      digests
    )
  }
}
