package sss.openstar.ledger

import sss.ancillary.Logging
import sss.db.FutureTx
import sss.openstar.common.block.TxMessage
import sss.openstar.ledger.LedgerId.NoLedgerId
import sss.openstar.util.StringCheck.UtilStrOps
import sss.openstar.{CodeMsg, CodeMsgResult}

import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

object LedgerOps extends Logging {


  implicit class ExToCode(val ex: Exception) extends AnyVal {
    def exToLedgerErrorCode: LedgerErrorCode = ex match {
      case e: LedgerTxApplyError => e.code
      case _ => LedgerErrorCodeGenericException
    }
  }


  implicit class LedgerExceptionOps(val ex: Throwable) extends AnyVal {

    def exceptionToLedgerError(implicit ledgerId: LedgerId): LedgerTxApplyError = ex match {
      case ex: LedgerTxApplyError => ex
      case _ => LedgerTxApplyError(LedgerErrorCodeGenericException, ex.getMessage)
    }

    def toTxMessage(chainId: Byte, txId: TxId): TxMessage = ex match {
      case ex: LedgerTxApplyError => TxMessage(chainId, ex.ledgerId, ex.code, txId, ex.msg.truncate(100))
      case _ =>
        TxMessage(chainId, NoLedgerId, LedgerErrorCodeGenericException, txId, ex.getMessage.truncate(100))
    }
  }



  implicit class PartialLedgerOps[A](val a: CodeMsgResult[A]) extends AnyVal {

    def liftMap[B](f: A => FutureTx[CodeMsgResult[B]]): FutureTx[CodeMsgResult[B]] = {
      a match {
        case Left(err) => FutureTx.lazyUnit(Left(err))
        case Right(value) => f(value)
      }
    }

    def toLedgerResult(txApplied: A => TxApplied)(implicit ledgerId: LedgerId) :LedgerResult = {
      a match {
        case Left(e) => Left(e.toLedgerTxApplyError)
        case Right(v) => Right(txApplied(v))
      }

    }
  }

  implicit class CodeMsgOps[A](val a: CodeMsg) extends AnyVal {
    def toLedgerTxApplyError(implicit ledgerId: LedgerId): LedgerTxApplyError = {
      LedgerTxApplyError(a.code, a.msg)
    }
  }

  implicit class TryLedgerResultToTryApplied(val triedLedgerResult: Try[LedgerResult]) extends AnyVal {
    def toTryTxApplied: Try[TxApplied] = triedLedgerResult match {
      case Success(Right(value)) => Success(value)
      case Success(Left(error)) => Failure(error)
      case Failure(e) => Failure(e)
    }

    def unwrap(implicit ledgerId: LedgerId): LedgerResult = triedLedgerResult match {
      case Failure(e) => Left(e.exceptionToLedgerError)
      case Success(r) => r
    }
  }

  implicit class LedgerResultOps(val ledgerResult: LedgerResult) extends AnyVal {
    def toTryTxApplied: Try[TxApplied] = ledgerResult match {
      case Right(value) => Success(value)
      case Left(error) => Failure(error)
    }

  }


  implicit class FTxLedgerResultToFTxApplied(val fTxApplied: FutureTx[LedgerResult]) extends AnyVal {
    def toFTxApplied: FutureTx[TxApplied] = fTxApplied flatMap {
      case Right(value) => FutureTx.unit(value)
      case Left(error) => FutureTx.failed(error)
    }
  }


  implicit class FutureTxTOps[A](f: FutureTx[A]) {
    def unbreakable(insteadOfFailure: Throwable => A): FutureTx[A] = context => {
      Try(f(context)) match {
        case Failure(exception) => Future.successful(insteadOfFailure(exception))
        case Success(value) =>
          import context.ec
          value.transformWith {
            case Failure(exception) => Future.successful(insteadOfFailure(exception))
            case Success(value) => Future.successful(value)
          }
      }
    }
  }

  implicit class FutureTxTryOps[A](f: FutureTx[Try[A]]) {
    def flattenTry: FutureTx[A] = {
      f.flatMap {
        case Success(a) => FutureTx.unit(a)
        case Failure(e) => FutureTx.failed(e)
      }
    }
  }

}
