package sss.openstar.ledger

import sss.db.FutureTx
import sss.openstar.common.block.BlockId


trait TollLedger extends Ledger  {

  def validate(
              ledgerId: LedgerId,
              signedTxEntry: SignedTxEntry,
              blockIndex: Long): FutureTx[Unit] = FutureTx.failed(new NotImplementedError())
}
