package sss.openstar.ledger

import sss.db.WhereOps.toWhere
import sss.db._
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.hash.Digest32
import sss.openstar.schemamigration.SqlSchemaNames.ColumnNames.{digestCol, heightCol}
import sss.openstar.schemamigration.SqlSchemaNames.TableNames.checkpointsTableName

import java.util

object LedgerCheckpoints {

  

  /*
  Checkpoints should be done manually as long running checkpoints have not been tested,
  they will probably cause tx timeouts
  https://sss.myjetbrains.com/youtrack/issue/OSP1-138
   */
  def isCheckpointHeight(height: Long): Boolean = false //height % checkpointInterval == 0

  case class Checkpoint(height: Long, digest: Digest32) {
    override def equals(o: Any): Boolean = {
      o match {
        case that: Checkpoint if that.height == height && that.digest == digest => true
        case _ => false
      }
    }

    override def hashCode(): Int = height.hashCode() + util.Arrays.hashCode(digest.digest)

    override def toString: String = s"H: $height, Digest: ${digest.asString}"
  }

  private def ledgerTable(implicit db: Db): Table = {
    db.table(checkpointsTableName)
  }

  private def rowToChkpoint(r: Row): Checkpoint = {
    Checkpoint(r.long(heightCol), Digest32(r.arrayByte(digestCol)))
  }

  def recordCheckpoint(height: Long, digest: Digest32)(implicit db: Db): Checkpoint =  {
    ledgerTable.delete(where(heightCol -> height)).flatMap(_ =>
      ledgerTable.insert(Map(heightCol -> height, digestCol -> digest.digest)).map(rowToChkpoint)
    ).dbRunSyncGet

  }

  def getLastCheckpoint(implicit db: Db): Option[Checkpoint] = {
    ledgerTable.map(
      rowToChkpoint,
      OrderDesc(heightCol) limit 1
    ).dbRunSyncGet.headOption
  }

  def compareCheckpoint(chkpoint: Checkpoint)(implicit db: Db): Option[Boolean] = {
    (ledgerTable.find(where(heightCol -> chkpoint.height)).map { futureRow =>
      futureRow.map (row => Digest32(row.arrayByte(digestCol)) == chkpoint.digest)
    }).dbRunSyncGet
  }

  def listObsoleteCheckpoints()(implicit db: Db): Seq[Checkpoint]= {

    val s = ledgerTable.map(
      rowToChkpoint,
      OrderDesc(heightCol)
    ).dbRunSyncGet
    if(s.isEmpty) Seq.empty else s.tail
  }

  def deleteCheckpoint(height: Long)(implicit db: Db): Boolean = {
    ledgerTable.delete(where(heightCol -> height)).dbRunSyncGet == 1
  }
}
