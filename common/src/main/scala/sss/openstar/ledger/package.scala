package sss.openstar

import com.google.common.primitives.Ints
import sss.ancillary.ByteArrayComparisonOps._
import sss.ancillary.ByteArrayEncodedStrOps._
import sss.ancillary.Serialize._
import sss.ancillary.{ByteArrayEncodedStrOps, Logging, SeqSerializer}
import sss.db.FutureTx
import sss.openstar.hash.{FastCryptographicHash, SecureCryptographicHash}
import sss.openstar.ledger.LedgerId.NoLedgerId
import sss.openstar.ledger.serialize.{LedgerItemSerializer, SeqLedgerItemSerializer}

import java.nio.charset.StandardCharsets
import java.util
import scala.util.Try

/**
  * Created by alan on 5/24/16.
  */
package object ledger extends Logging {

  type TxId = Array[Byte]
  val TxIdLen = 32
  // Q09JTkJBU0VDT0lOQkFTRUNPSU5CQVNFQ09JTkJBU0U:0 <-- coinbase as seen in logs.
  val CoinbaseTxId: TxId =
    "COINBASECOINBASECOINBASECOINBASE".getBytes(
      StandardCharsets.UTF_8
    )

  implicit class TxIdFromString(txId: String) {
    def asTxId: TxId = {
      val bytes = ByteArrayEncodedStrOps
        .Base64StrToByteArray(txId)
        .fromBase64Str
      require(
        bytes.length == TxIdLen,
        s"Bytes decoded not right len for txId, ${bytes.length} should be $TxIdLen"
      )
      bytes
    }
  }

  implicit class FromLedgerItem(val le: LedgerItem) extends AnyVal {
    def toBytes: Array[Byte] =
      LedgerItemSerializer.toBytes(le)
  }

  implicit class ToLedgerItem(val bytes: Array[Byte]) extends AnyVal {
    def toLedgerItem: LedgerItem = {
      LedgerItemSerializer.fromBytes(bytes)
    }
  }

  object SeqLedgerItem {
    def apply(value: LedgerItem): SeqLedgerItem = SeqLedgerItem(Seq(value))

    def hash(txIds: Seq[TxId]): TxId = if (txIds.size == 1) {
      txIds.head
    } else {
      FastCryptographicHash.hash(txIds.reduce(_ ++ _))
    }
  }

  case class SeqLedgerItem(value: Seq[LedgerItem]) {

    require(value.nonEmpty, "Cannot create ledger empty Items")

    //lazy val ledgerId = value.head.ledgerId

    lazy val txId: TxId = SeqLedgerItem.hash(value.map(_.txId))


    lazy val txIdHexStr: String = txId.toBase64Str
  }

  implicit class SeqLedgerItemToBytes(v: SeqLedgerItem)
      extends ToBytes {
    override def toBytes: Array[Byte] =
      SeqLedgerItemSerializer.toBytes(v)
  }

  implicit class SeqLedgerItemFromBytes(
    bytes: Array[Byte]
  ) {
    def toSeqLedgerItem: SeqLedgerItem =
      SeqLedgerItemSerializer.fromBytes(bytes)
  }

  object LedgerItem {
    def apply(ledgerId: Byte,
              txId: TxId,
              txEntryBytes: Array[Byte]): LedgerItem = new LedgerItem(LedgerId(ledgerId), txId, txEntryBytes)
  }
  case class LedgerItem(
    ledgerId: LedgerId,
    txId: TxId,
    txEntryBytes: Array[Byte] //TODO add max size of Tx!
  ) extends BusEvent {

    require(
      txId.length == TxIdLen,
      s"LedgerItem cannot create a TxId with len ${txId.length}, must be $TxIdLen"
    )

    lazy val txIdHexStr: String = txId.toBase64Str

    override def toString: String =
      s"LedgerItem(ledgerId:$ledgerId, txId: $txIdHexStr txEntryBytes:[${txEntryBytes.toBase64Str.take(5)}...])"

    override def equals(obj: scala.Any): Boolean =
      obj match {
        case le: LedgerItem =>
          le.ledgerId == ledgerId &&
            le.txId.isSame(txId) &&
            le.txEntryBytes.isSame(txEntryBytes)

        case _ => false
      }

    override def hashCode(): Int = 17 * txId.hash

  }


  object TxSig {
    def emptyTxSig: TxSig = TxSig(Seq(Seq(Array.emptyByteArray, Array.emptyByteArray, Array.emptyByteArray)))
  }

  case class TxSig(sigs: Seq[Seq[Array[Byte]]] = Seq(Seq())) {
    Try {
      require(sigs.nonEmpty, "Must include id, tag, sig to pay toll")
      require(sigs.head.size == 3, "Must include all of id, tag and sig to pay toll")
    } recover {
      case e => log.warn(e.getMessage)
    }

    def withSig(sig: Seq[Array[Byte]]): TxSig =
      copy(sigs = sigs :+ sig)
  }

  //TODO if the system is deterministically ordering Txs, what is the point of TxId?
  //It's a fingerprint of the code that was executed.
  case class SignedTxEntry(
    txEntryBytes: Array[Byte],
    signatures: TxSig = TxSig()
  ) {
    def toBytes: Array[Byte] = {
      val lenAsBytes = Ints.toByteArray(txEntryBytes.length)
      val seqsAsBytes = SeqSerializer.toBytes(
        signatures.sigs.map(SeqSerializer.toBytes(_))
      ) ++ txEntryBytes
      lenAsBytes ++ txEntryBytes ++ seqsAsBytes
    }

    override def equals(obj: scala.Any): Boolean =
      obj match {
        case ste: SignedTxEntry =>
          ste.txEntryBytes.isSame(txEntryBytes) &&
            (ste.signatures.sigs.flatten isSame signatures.sigs.flatten)

        case _ => false
      }

    override def hashCode(): Int =
      (17 + signatures.sigs.flatten.hash) * txId.hash

    lazy val txId: TxId = {
      SecureCryptographicHash.hash(txEntryBytes)
    }

  }

  implicit class ToTxEntryWithSignatures(
    bytes: Array[Byte]
  ) {
    def toSignedTxEntry: SignedTxEntry = {
      val (lenAsBytes, rest) = bytes.splitAt(4)
      val len = Ints.fromByteArray(lenAsBytes)
      val (txEntry, seqsAsBytes) = rest.splitAt(len)
      val seqOfSeq = if(seqsAsBytes.nonEmpty) {
         SeqSerializer.fromBytes(seqsAsBytes)
      } else Seq.empty

      val allDeserialized = seqOfSeq map SeqSerializer.fromBytes
      SignedTxEntry(
        txEntry,
        TxSig(allDeserialized)
      )
    }
  }

  object LedgerId {
    val NoLedgerId = LedgerId(0)
  }

  case class LedgerId(id: Byte)

  //When you apply a LedgerItem to a ledger you get a result
  //a Failure or a list of events to be fired after the item is committed
  trait TxApplied extends BusEvent {
    val txId: TxId

    val ledgerId: LedgerId
    def events: Seq[BusEvent] = Seq(this)

    def isTxIdSame(txId: TxId): Boolean =
      util.Arrays.equals(
        this.txId,
        txId
      )

    //override def toString: String = s"TxId:${txId.toBase64Str}"
  }

  object TxSeqApplied {
    def empty(txId: TxId): TxSeqApplied = TxSeqApplied(txId)(NoLedgerId)
  }
  case class TxSeqApplied(txId: TxId, values: Seq[TxApplied] = Seq.empty)(implicit val ledgerId: LedgerId)
    extends TxApplied {
    override def events: Seq[BusEvent] = super.events ++ values.flatMap(_.events)

    def withTxResult(txResult: TxApplied): TxSeqApplied = {

      if(!txResult.isTxIdSame(txId)) {
        log.info("Merging result from {} and {}", txId.toBase64Str, txResult.txId.toBase64Str)
      }

      txResult match {
        case TxSeqApplied(_, others) =>
          this.copy(values = values ++ others)

        case _ =>
          this.copy(values = values :+ txResult)
      }

    }

    override def toString: String = {
      s"""
         |TxSeqApplied values: ${values.filterNot(_ == this).map(_.toString).mkString(",")},
         |Bus Events: ${events.filterNot(_ == this).map(_.toString).mkString(",")},
         |
         |""".stripMargin
    }
  }

  case class TxNotApplied(txId: TxId, msg: String = "NoReasonGiven")(implicit val ledgerId: LedgerId)
      extends TxApplied {
    override def events: Seq[BusEvent] = Seq.empty

    override def toString: String =
      s"${txId.toBase64Str}, TxNotApplied ($msg)"

    override def equals(obj: Any): Boolean = obj match {
      case TxNotApplied(other, _) =>
        this.ledgerId == ledgerId && isTxIdSame(other)
      case _ => false
    }

    override def hashCode(): Int = txId.hashCode()
  }

  final case class NoSubEventsTxApplied(
    txId: TxId
  )(implicit val ledgerId: LedgerId) extends TxApplied {
    override def toString: String =
      s"$txId ${txId.toBase64Str}, NoSubEvents"

    override def equals(obj: Any): Boolean = obj match {
      case NoSubEventsTxApplied(tx) =>
        this.ledgerId == ledgerId &&
          isTxIdSame(tx)
      case _ => false
    }

    override def hashCode(): Int = 17 * txId.hashCode()
  }

  type LedgerErrorCode = String
  val LedgerErrorCodeGenericException = "GENERIC ERR"

  case class LedgerTxApplyError(code: LedgerErrorCode, msg: String = "")(implicit val ledgerId: LedgerId) extends Exception(msg)

  type LedgerResult = Either[LedgerTxApplyError, TxApplied]
  type LedgerResultFTx = FutureTx[LedgerResult]


}
