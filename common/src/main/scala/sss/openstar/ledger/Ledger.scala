package sss.openstar.ledger

import sss.openstar.common.block.BlockId
import sss.openstar.hash.Digest32

import scala.util.{Failure, Success, Try}

trait Ledger {

  def ledgerId: LedgerId

  def initialize(): Try[String] = Success(s"Ledger $ledgerId needs no initialization.")

  //def fingerprint(): Hash
  def apply(
             ledgerItem: LedgerItem,
             blockId: BlockId,
             txSeqApplied: TxSeqApplied
           ): LedgerResult

  def apply(
             ledgerItem: LedgerItem,
             blockId: BlockId
           ): LedgerResult = apply(ledgerItem, blockId, TxSeqApplied(ledgerItem.txId)(ledgerId))

  def applyFTx(
                ledgerItem: LedgerItem,
                blockId: BlockId,
                txSeqApplied: TxSeqApplied
              ): LedgerResultFTx

  def coinbase(
                forBlockHeight: Long,
                ledgerId: Byte,
              ): Option[LedgerItem] = None

  def importState(height: Long): Try[Unit] = Failure(new IllegalStateException("No support for state saving"))

  def deleteCheckpoint(height: Long): Try[Unit] = Failure(new IllegalStateException("No support for state saving"))

  def createCheckpoint(id: Long): Try[Digest32] = Failure(new IllegalStateException("No support for state saving"))

}

