package sss.openstar.util

import sss.db.{Db, FutureTx, OrderBy, Query, Row, Table}
import sss.db.ops.DbOps.DbRunOps

import scala.util.Try

trait TableCopy {

  private def createSnapshotTableSql(oldTableName: String, newTableName: String): Seq[String] = Seq(
    s"CREATE CACHED TABLE IF NOT EXISTS $newTableName ( LIKE $oldTableName INCLUDING DEFAULTS INCLUDING IDENTITY INCLUDING GENERATED);",
    s"INSERT INTO $newTableName ( SELECT * FROM $oldTableName);"
  )

  protected def copyTable(oldTableName: String, newTableName: String)(implicit db:Db): FutureTx[Table] = {
    val createBackupStateTableSql = createSnapshotTableSql(oldTableName, newTableName)
    db.executeSqls(createBackupStateTableSql).map(_ =>  db.table(newTableName))
  }

  protected def replaceTable(tableToReplace: String, replacementTableName: String)(implicit db:Db): FutureTx[Unit] = {

    val backupReplacedTable = s"${tableToReplace}_retired"

    db.executeSqls(Seq(
      s"DROP TABLE IF EXISTS $backupReplacedTable;",
      s"ALTER TABLE $tableToReplace RENAME TO $backupReplacedTable;",
      s"ALTER TABLE $replacementTableName RENAME TO $tableToReplace;"
    ))
  }.map(_ => ())

  protected def pageGenerator[T](query: Query, f: Row => T, orderBys: Seq[OrderBy])
                              (offset: Long, pagesize: Int)(implicit db:Db): Seq[T] = {
    query
      .page(offset, pagesize, orderBys)
      .map(_.map(f)).dbRunSyncGet
  }


}
