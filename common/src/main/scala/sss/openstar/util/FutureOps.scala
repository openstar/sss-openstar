package sss.openstar.util

import sss.ancillary.Logging

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.language.postfixOps
import scala.util.{Failure, Success}


class IOExecutionContext(executionContext: ExecutionContext) {
  implicit val ec = executionContext
}

trait LoggingFutureSupport {

  self : Logging =>

  def loggingFuture[T](f: => T)(implicit ec: ExecutionContext): Future[T] = Future {
    f
  } andThen {
    case Failure(e) => log.error(s"Future Error ${e.toString}")
    case Success(s) => log.trace(s.toString.take(100))
  }

}

