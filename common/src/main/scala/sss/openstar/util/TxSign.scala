package sss.openstar.util

import sss.openstar.account.Ops.{MakeTxSig, MakeTxSigSigner}
import sss.openstar.account.{NodeIdentity, NodeSigner}
import sss.openstar.ledger.{SignedTxEntry, TxId}

import java.nio.charset.StandardCharsets
import scala.concurrent.{ExecutionContext, Future}

object TxSign {

  def simplySignedTxEntry(user: NodeIdentity, txId: TxId, txBytes: Array[Byte])(implicit ec: ExecutionContext): Future[SignedTxEntry] = {
    user.txSig(txId).map(SignedTxEntry(txBytes, _))
  }

  def simplySignedTxEntry(canSign: NodeSigner, txId: TxId, txBytes: Array[Byte])(implicit ec: ExecutionContext): Future[SignedTxEntry] = {
    canSign
      .txSig(txId)
      .map(SignedTxEntry(txBytes, _))
  }
}
