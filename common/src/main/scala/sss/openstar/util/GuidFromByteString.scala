package sss.openstar.util

import akka.util.ByteString

object GuidFromByteString {

  def apply(value: ByteString): sss.ancillary.Guid = sss.ancillary.Guid(value.toArray)
  def apply(): sss.ancillary.Guid = sss.ancillary.Guid()
}

