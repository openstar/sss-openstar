package sss.openstar.util

import akka.actor.ActorLogging
import com.typesafe.scalalogging.Logger
import sss.ancillary.Logging

import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

object TryOps {

  implicit class LogTry[T](t: Try[T]) {
    def andLog(msg: String)(implicit log: Logger): Try[T] = {
      t.recover {
        case e => log.warn(msg, e)
      }
      t
    }
  }

  implicit class FutureFromTry[T](val t: Try[T]) extends AnyVal {
    def toFuture: Future[T] = Future.fromTry(t)
  }

  implicit class FutureFromT[T](t: => T) {
    def tryAsFuture: Future[T] = Future.fromTry(Try(t))
  }

  implicit class TryToOpt[T](val t: Try[T]) extends AnyVal {
    def toOptAndLogException(implicit log: Logger): Option[T] = {
      t match {
        case Success(value) => Some(value)
        case Failure(exception) =>
          log.warn(s"ToOpt", exception)
          None
      }
    }
  }
}

trait ActorTryAndLog {

  self: ActorLogging =>

  def tryAndLog[T](f: => T): Try[T] = Try(f) match {
    case f@Failure(e) =>
      log.error(e.toString)
      f
    case x => x
  }
}

trait TryAndLog {

  self: Logging =>

  def tryAndLog[T](f: => T): Try[T] = Try(f) match {
    case f@Failure(e) =>
      log.error(e.toString)
      f
    case x => x
  }
}