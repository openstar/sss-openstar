package sss.openstar.util

import com.typesafe.config.Config

import java.util
import scala.jdk.CollectionConverters.CollectionHasAsScala

object ConfigHelper {

  def toListOfStringLists(config: Config, path: String): List[List[String]] = {
    val listOfLists = config.getAnyRefList(path)
    listOfLists.asScala.toList.map { l =>
        l.asInstanceOf[util.ArrayList[String]].asScala.toList
    }
  }
}
