package sss.openstar.util

import java.security.SecureRandom

@deprecated("This is not a unique increasing number")
object UniqueIncreasing {

  type UniqueIncreasing = Long
  def apply(): UniqueIncreasing = SecureRandom.getInstanceStrong.nextLong()
}

