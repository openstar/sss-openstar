package sss.openstar.util

import java.util.Collections.synchronizedMap

import scala.collection.mutable
import scala.jdk.CollectionConverters._


class SynchronizedLruMap[K, V] private (maxEntries: Int)
  extends java.util.LinkedHashMap[K, V](100, .75f, true) {

  override def removeEldestEntry(eldest: java.util.Map.Entry[K, V]): Boolean
  = size > maxEntries

}

object SynchronizedLruMap {
  def apply[K, V](maxEntries: Int): mutable.Map[K, V]
  = synchronizedMap(new SynchronizedLruMap[K, V](maxEntries)).asScala
}
