package sss.openstar.util


trait Lookup[K, O] {

  def apply(key: K): O = get(key).getOrElse(throw new IllegalArgumentException(s"No value exists for key ${key}"))

  def get(key: K): Option[O] = lookupMatcher.lift(key)

  def contains(k: K): Boolean = get(k).isDefined

  def lookupMatcher: PartialFunction[K, O]

}
