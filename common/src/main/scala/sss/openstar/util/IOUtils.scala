package sss.openstar.util

import java.io.{IOException, InputStream, OutputStream}

object IOUtils {

  private val DEFAULT_BUFFER_SIZE = 8192

  object InputStreamOps {

    implicit class InputOp(val input: InputStream) extends AnyVal {
      @throws(classOf[IOException])
      def transferTo(out: OutputStream): Long = {
        var transferred: Long = 0
        val buffer: Array[Byte] = new Array[Byte](DEFAULT_BUFFER_SIZE)
        var read: Int = input.read(buffer, 0, DEFAULT_BUFFER_SIZE)
        while (read >= 0) {
          out.write(buffer, 0, read)
          transferred += read
          read = input.read(buffer, 0, DEFAULT_BUFFER_SIZE)
        }
        transferred
      }
    }
  }
}
