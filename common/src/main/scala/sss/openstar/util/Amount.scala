package sss.openstar.util

import sss.openstar.Currency

import scala.util.Try

trait Amount {
  val value: Long
  type C <: Currency

  private def add(amount: Long): Try[Long] = Try {
    Math.addExact(value, amount)
  }

  lazy val valueOrZeroIfNegative: Amount = newAmount(Math.max(value, 0))

  private def subtract(amount: Long): Try[Long] = Try {
    Math.subtractExact(value, amount)
  }

  private def multiply(amount: Long): Try[Long] = Try {
    Math.multiplyExact(value, amount)
  }

  def newAmount(value: Long): Amount

  def *(amount: Long): Try[Amount] = multiply(amount).map(newAmount)
  def -(amount: Long): Try[Amount] = subtract(amount).map(newAmount)
  //def unary_-(amount: Long): Try[Amount] = Try(Math.subtractExact(amount, value)).map(newAmount)
  def +(amount: Long): Try[Amount] = add(amount).map(newAmount)
}



trait AmountBuilder[C <: Currency] {
  def apply(value: Long): Amount
}

object Amount {

  def subtract(a: Long, b: Long): Long = trySubtract(a, b).get
  def trySubtract(a: Long, b: Long): Try[Long] = Try(Math.subtractExact(a, b))
  def trySubtract(a: Long, b: Amount): Try[Amount] = Try(b.newAmount(Math.subtractExact(a, b.value)))
  def trySubtract(a: Long, b: Try[Amount]): Try[Amount] = b.flatMap(v => trySubtract(a, v))

  def apply[C <: Currency](value: Long)(implicit cb: AmountBuilder[C]): Amount =
    cb(value)

  object Ops {

    implicit class AmountOps[A <: Amount](amount: A) {
      def +[B <: Amount](other: B)(implicit ev: A#C =:= B#C = null): Try[Amount] = Try {
        val isSame = ev != null
        require(isSame, s"${amount.getClass} and ${other.getClass} not same type")
        other.value
      }.flatMap(amount.add(_).map(amount.newAmount))

      def -[B <: Amount](other: B)(implicit ev: A#C =:= B#C = null): Try[Amount] = Try {
        val isSame = ev != null
        require(isSame, s"${amount} and ${other} not same type")
        other.value
      }.flatMap(amount.subtract(_).map(amount.newAmount))

      def *[B <: Amount](other: B)(implicit ev: A#C =:= B#C = null): Try[Amount] = Try {
        val isSame = ev != null
        require(isSame, s"${amount} and ${other} not same type")
        other.value
      }.flatMap(amount.multiply(_).map(amount.newAmount))

      /*def unary_-[B <: Amount](other: B)(implicit ev: A#C =:= B#C = null): Try[Amount] = Try {
        val isSame = ev != null
        require(isSame, s"${amount} and ${other} not same type")
        other.value
      }.flatMap(other.subtract(_).map(amount.newAmount))*/

    }

    implicit class TryAmountOps(amountTry: Try[Amount]) {

      def +(other: Long): Try[Amount] = amountTry.flatMap(_.+(other))

      def +(other: Amount): Try[Amount] = amountTry.flatMap(_ + other)

      def +(otherTry: Try[Amount]): Try[Amount] =
        amountTry.flatMap(otherTry + _)

      def -(other: Long): Try[Amount] = amountTry.flatMap(_ - other)

      def -(other: Amount): Try[Amount] = amountTry.flatMap(_ - other)

      //def unary_-(other: Amount): Try[Amount] = amountTry.flatMap(amount => other - (amount.value))

      def -(otherTry: Try[Amount]): Try[Amount] = for {
        am1 <- amountTry
        am2 <- otherTry
        r <- am1 - am2
      } yield(r)

    }

  }

}
