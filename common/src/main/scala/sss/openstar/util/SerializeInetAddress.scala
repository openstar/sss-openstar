package sss.openstar.util

import java.net.{InetAddress, InetSocketAddress}

import sss.ancillary.Serialize.ToBytes
import sss.openstar.util.serialize.{InetAddressSerializer, InetSocketAddressSerializer}

object SerializeInetAddress {


  implicit class InetAddressToBytes(val inetAddr: InetAddress)  extends ToBytes {
    def toBytes: Array[Byte]= InetAddressSerializer.toBytes(inetAddr)
  }

  implicit class InetAddressFromBytes(val bs: Array[Byte])  {
    def toInetAddress: InetAddress = InetAddressSerializer.fromBytes(bs)
  }

  implicit class InetAddressSocketToBytes(val inetAddr: InetSocketAddress)  extends ToBytes {
    def toBytes: Array[Byte]= InetSocketAddressSerializer.toBytes(inetAddr)
  }

  implicit class InetAddressSocketFromBytes(val bs: Array[Byte])  {
    def toInetSocketAddress: InetSocketAddress = InetSocketAddressSerializer.fromBytes(bs)
  }
}
