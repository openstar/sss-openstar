package sss.openstar.util.serialize

import sss.ancillary.Serialize._
import sss.openstar.util.HashedSecret

object HashedSecretSerializer extends Serializer[HashedSecret] {

  override def toBytes(p: HashedSecret): Array[Byte] = {
    ByteArraySerializer(p.bytes).toBytes
  }

  override def fromBytes(bs: Array[Byte]): HashedSecret = {
    bs.extract(
      ByteArrayDeSerialize(HashedSecret.apply)
    )
  }
}