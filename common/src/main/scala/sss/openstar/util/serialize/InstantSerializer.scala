package sss.openstar.util.serialize

import sss.ancillary.Serialize.{SerializeHelper, Serializer, StringDeSerialize, StringSerializer}

import java.time.Instant


object InstantSerializer extends Serializer[Instant] {
  override def toBytes(t: Instant): Array[Byte] = StringSerializer(t.toString).toBytes

  override def fromBytes(b: Array[Byte]): Instant = Instant.parse(b.extract(StringDeSerialize))
}

