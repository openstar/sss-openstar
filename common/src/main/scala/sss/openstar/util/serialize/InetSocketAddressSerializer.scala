package sss.openstar.util.serialize

import java.net.InetSocketAddress

import sss.ancillary.Serialize._
import sss.openstar.util.SerializeInetAddress.{InetAddressFromBytes, InetAddressToBytes}

object InetSocketAddressSerializer extends Serializer[InetSocketAddress] {

  override def toBytes(p: InetSocketAddress): Array[Byte] = {

      ByteArraySerializer(p.getAddress.toBytes) ++
      IntSerializer(p.getPort)
        .toBytes
  }

  override def fromBytes(bs: Array[Byte]): InetSocketAddress = {
    val extracted = bs.extract(
      ByteArrayDeSerialize(_.toInetAddress),
      IntDeSerialize
    )
    new InetSocketAddress(extracted._1, extracted._2)

  }
}

