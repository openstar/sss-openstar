package sss.openstar.util

import sss.ancillary.Logging

import java.nio.file.{CopyOption, Files, Path}

object FileUtils extends Logging {

  private val cannotDeletePaths: Set[String] = Set("/", "/home")

  def pathLast(path: Path): Path = {
    path.getFileName
  }

  private def deleteAllImpl(path: Path, depth: Int, deleteContentOnly: Boolean): Boolean = {
    require(!cannotDeletePaths.contains(path.toString), s"Cannot delete ${path.toString} path")

    if(path.toFile.isDirectory) {
      Files.list(path).forEach(p => {
        deleteAllImpl(p, depth + 1, deleteContentOnly)
      })
    }
    if(depth == 0) {
      if(!deleteContentOnly) {
        path.toFile.delete()
      } else true
    } else {
      path.toFile.delete()
    }

  }

  def deleteContents(path: Path): Unit = {
    deleteAllImpl(path, 0, deleteContentOnly = true)
  }

  def deleteAll(path: Path): Boolean = {
    deleteAllImpl(path, 0, false)
  }

  def copyFolder(sourceDirectoryLocation: Path,
                 destinationDirectoryLocation: Path): Unit = {
    val folderName = pathLast(sourceDirectoryLocation).toString
    copyFolderContents(sourceDirectoryLocation, destinationDirectoryLocation.resolve(folderName))
  }

  def copyFolderContents(sourceDirectoryLocation: Path,
                         destinationDirectoryLocation: Path,
                         copyOption: CopyOption*): Unit = {
    if (sourceDirectoryLocation.toFile.isDirectory) {
      lazy val onlyOnce = Files.createDirectories(destinationDirectoryLocation)
      Files.walk(sourceDirectoryLocation)
        .forEach(source => {
          if (source != sourceDirectoryLocation) {
            onlyOnce
            val what = source.toString.substring(sourceDirectoryLocation.toString.length + 1)
            val destination =
              destinationDirectoryLocation.resolve(what)
            Files.copy(source, destination, copyOption:_*)
          }
        }
        )
    }
  }
}
