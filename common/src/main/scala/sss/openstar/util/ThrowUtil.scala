package sss.openstar.util

object ThrowUtil {

  def throwIllegalState[T](msg: String): T = throw new IllegalStateException(msg)
  def throwIllegalArgument[T](msg: String): T = throw new IllegalArgumentException(msg)
  def throwRuntime[T](msg: String): T = throw new RuntimeException(msg)

}
