package sss.openstar.util


import akka.util.ByteString
import com.google.common.primitives.Ints
import sss.ancillary.Serialize.{DeSerializeTarget, DeSerialized, ToBytes}

/**
  * Copyright Stepping Stone Software Ltd. 2016, all rights reserved.
  * mcsherrylabs on 3/3/16.
  *
  *
  */
object SerializeByteString {

  case class ByteStringDeSerialized(payload: ByteString)
      extends DeSerialized[ByteString]


  case class ByteStringSerializer(payload: ByteString) extends ToBytes {
    override def toBytes: Array[Byte] = {
      assert(payload.toArray.length == payload.length, "WHAT?")
      Ints.toByteArray(payload.length) ++ payload
    }
  }

  case class ByteStringDeSerialize[T](mapper: ByteString => T)
    extends DeSerializeTarget {
    type t = T
    override def extract(bs: Array[Byte]): (DeSerialized[T], Array[Byte]) = {
      val (deserialized, rest) = ByteStringDeSerialize.extract(bs)
      (new DeSerialized[T] { val payload = (mapper(deserialized.payload)) }, rest)
    }
  }

  object ByteStringDeSerialize extends DeSerializeTarget {
    type t = ByteString
    override def extract(
        bs: Array[Byte]): (ByteStringDeSerialized, Array[Byte]) = {
      val (bytes, rest) = bs.splitAt(4)
      val len = Ints.fromByteArray(bytes)
      val (ary, returnBs) = rest.splitAt(len)
      (ByteStringDeSerialized(ByteString(ary)), returnBs)
    }
  }


}
