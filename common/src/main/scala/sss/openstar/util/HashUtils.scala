package sss.openstar.util


import sss.ancillary.PagesToStream
import sss.ancillary.PagesToStream.Pager
import sss.db.RowSerializer._
import sss.db.{ColumnsMetaInfo, Db, Row}
import sss.openstar.hash.{Digest32, FastCryptographicHash, Message}

import scala.util.Try

object HashUtils {

  def apply(digests: Iterable[Try[Digest32]]): Try[Digest32] = Try {
    Digest32(FastCryptographicHash(digests.map(_.get.digest).to(LazyList)))
  }

  def apply(stream: Seq[Message]): Try[Digest32] = Try {
    Digest32(FastCryptographicHash(stream))
  }

  def apply(pager: Pager[Message]): Try[Digest32] = Try {
    PagesToStream(pager, 25)
  } flatMap apply

  def fromRows(pager: Pager[Row], meta: ColumnsMetaInfo)(implicit db: Db): Try[Digest32] = Try[Pager[Message]] {
    (index, offset) => pager(index, offset).map(_.toBytes(meta))
  } flatMap apply


}
