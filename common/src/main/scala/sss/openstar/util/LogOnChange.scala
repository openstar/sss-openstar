package sss.openstar.util

/**
  * Not thread safe!
  *
  * @param comparator
  * @param logAction
  * @tparam T
  */
class LogOnChange[T](
                      logAction: T => Unit,
                      logAnyway: Int = 20,
                      comparator: (T, T) => Boolean = (t1: T, t2: T) => t1 == t2
                    ) {

  private var previous: Option[T] = None

  private var noLogCount = 0

  def apply(t: T): Unit = {
    previous match {
      case None =>
        logAction(t)
        noLogCount = 0
      case Some(oldValue) if !comparator(t, oldValue) =>
        logAction(t)
        noLogCount = 0
      case Some(_) if(noLogCount >= logAnyway) =>
        logAction(t)
        noLogCount = 0
      case _ =>
        noLogCount += 1
    }
    previous = Some(t)
  }
}
