package sss.openstar.util

import java.util
import java.util.Base64
import sss.openstar.hash.SecureCryptographicHash
import sss.openstar.util.HashedSecret.{asUrlSafeBase64String, fromBase64String}

import scala.util.Try

object HashedSecret {

  def asUrlSafeBase64String(bytes: Array[Byte]): String = Base64.getUrlEncoder.encodeToString(bytes)

  def fromBase64String(base64String: String): Try[Array[Byte]] = {
    Try(Base64.getUrlDecoder.decode(base64String))
      .orElse(Try(Base64.getDecoder.decode(base64String)))
  }

  def hashSecret(secretAsString: String): HashedSecret = {
    hashSecret(fromBase64String(secretAsString).get)
  }

  def hashSecret(secret: Array[Byte]): HashedSecret = HashedSecret(SecureCryptographicHash.hash(secret))

}

final case class HashedSecret(bytes: Array[Byte]) {

  lazy val hashedSecretAsString: String = asUrlSafeBase64String(bytes)

  def matches(secretAsString: String): Boolean =
    fromBase64String(secretAsString).fold(_ => false, matches)

  def matches(secret: Array[Byte]): Boolean =
    util.Arrays.equals(SecureCryptographicHash.hash(secret), bytes)

  override def hashCode(): Int = util.Arrays.hashCode(bytes)

  override def equals(o: Any): Boolean = o match {
    case HashedSecret(thatBytes) if util.Arrays.equals(bytes, thatBytes) => true
    case _ => false
  }
}


