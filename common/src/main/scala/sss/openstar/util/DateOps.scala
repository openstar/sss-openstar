package sss.openstar.util

import java.time.{Instant, LocalDateTime, ZoneOffset}

object DateOps {

  def getAsMilli(l: LocalDateTime): Long = l.atOffset(ZoneOffset.UTC).toInstant.toEpochMilli
  def getFromMilli(l: Long): LocalDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(l), ZoneOffset.UTC)

  implicit class DateToLong(val d: LocalDateTime) extends AnyVal {
    def toMillis: Long = getAsMilli(d)
  }

  implicit class LongToDate(val millis: Long) extends AnyVal {
    def toLocalDateTime: LocalDateTime = getFromMilli(millis)
  }
}
