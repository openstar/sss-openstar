package sss

import akka.actor.ActorRef
import sss.db.FutureTx
import sss.openstar.chains.Chains.GlobalChainIdMask
import sss.openstar.ledger.{LedgerId, LedgerResult, LedgerTxApplyError, TxApplied}

package object openstar {

  trait Event
  trait BusEvent extends Event

  trait Currency

  type UniqueNodeIdentifier = String

  case object QueryStatus extends BusEvent
  case class Status(any: Any) extends BusEvent

  trait QueryStatusSupport {
    protected val ref: ActorRef
    def queryStatus = ref ! QueryStatus
  }

  object CodeMsg {
    def apply[C <: Enumeration#Value](code: C, msg: String = "No Detail"): CodeMsg =
      CodeMsg(code.toString, msg)
  }

  case class CodeMsg(code: String, msg: String)

  type CodeMsgResult[T] = Either[CodeMsg, T]

}
