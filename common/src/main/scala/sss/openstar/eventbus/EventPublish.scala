package sss.openstar.eventbus

import java.nio.charset.StandardCharsets

import sss.ancillary.Serialize.Serializer
import sss.openstar.BusEvent

import scala.reflect.ClassTag


object PureEvent {
  def apply(code: Byte, many: Array[Byte]): PureEvent = {
    require(many.isEmpty, s"A pure event has no body of bytes associated with it. (payload size ${many.size}")
    PureEvent(code)
  }
}

case class StringMessage(value: String)

object StringMessageSerializer extends Serializer[StringMessage] {

  override def toBytes(t: StringMessage): Array[Byte] = t.value.getBytes(StandardCharsets.UTF_8)

  override def fromBytes(b: Array[Byte]): StringMessage = StringMessage(new String(b, StandardCharsets.UTF_8))
}

case class PureEvent(code: Byte)

class PureEventSerializer(p: Byte) extends Serializer[PureEvent] {

  override def toBytes(t: PureEvent): Array[Byte] = Array.empty

  override def fromBytes(b: Array[Byte]): PureEvent = PureEvent(p)
}

trait EventPublish {
  def publish[T <: BusEvent: ClassTag](event: T): Unit
}