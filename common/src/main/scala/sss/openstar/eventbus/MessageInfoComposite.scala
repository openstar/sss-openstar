package sss.openstar.eventbus

import sss.ancillary.Logging
import sss.ancillary.Serialize.Serializer

import scala.reflect.runtime.universe._


case class MessageInfoComposite[K](
                                     msgCode: Byte,
                                     clazz: Class[K],
                                     serialize: Serializer[K],
                                     next: Option[MessageInfoComposite[_]] = None
                                   )
  extends MessageInfo
    with MessageInfos
    with Logging
{

  override type T = K

  override def fromBytes(bytes: Array[Byte]): T = serialize.fromBytes(bytes)

  override def toBytes[A: TypeTag](a: A): Array[Byte] = serialize.toBytes(a.asInstanceOf[K])

  override def findByClass(a: Any): Option[MessageInfo] = {
    a.getClass match {
      case `clazz` => Some(this)
      case _ if next.isDefined => next.flatMap(_.findByClass(a))
      case _ => None
    }
  }

  def find(code: Byte): Option[MessageInfo] = {
    if (code == msgCode) Option(this)
    else next flatMap (_.find(code))
  }

  def +:(other: MessageInfoComposite[_]): MessageInfoComposite[_] = prepend(other)

  def :+(other: MessageInfoComposite[_]): MessageInfoComposite[_] = append(other)

  private def accCodes(other: Option[MessageInfoComposite[_]], acc: Seq[Byte]): Seq[Byte] = {
    other match {
      case None => acc
      case Some(msgInfo) => accCodes(msgInfo.next, msgInfo.msgCode +: acc)
    }
  }

  private def append(other: MessageInfoComposite[_]): MessageInfoComposite[_] = {
    val clashes = for {
      code1 <- accCodes(Option(this), Seq.empty)
      code2 <- accCodes(Option(other), Seq.empty)
      if code1 == code2
    } yield code1

    require(clashes.isEmpty, s"Duplicate msgCodes not allowed $clashes")

    if (next.isDefined) this.copy(next = Option(next.get.append(other)))
    else this.copy(next = Option(other))
  }

  private def prepend(other: MessageInfoComposite[_]): MessageInfoComposite[_] = {
    other.append(this)
  }
}
