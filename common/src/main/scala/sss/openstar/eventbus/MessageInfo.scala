package sss.openstar.eventbus

import scala.reflect.runtime.universe._

trait MessageInfo {
  type T
  val clazz: Class[T]

  val msgCode: Byte
  def fromBytes(bytes: Array[Byte]): T
  def toBytes[A: TypeTag](t: A): Array[Byte]

}