package sss.openstar.eventbus



trait MessageInfos {

  self: MessageInfoComposite[_] =>

  def find(code: Byte): Option[MessageInfo]

  def findByClass(a: Any): Option[MessageInfo]

  def toMessageComposite: MessageInfoComposite[_] = this

  def ++(others: MessageInfos): MessageInfos =
    toMessageComposite :+
      others.toMessageComposite

}
