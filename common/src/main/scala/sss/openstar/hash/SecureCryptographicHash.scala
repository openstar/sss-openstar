package sss.openstar.hash


import scorex.crypto.hash.Keccak256



/**
  * The chain of two hash functions, Blake and Keccak
  */

object SecureCryptographicHash extends CryptographicHash {

  def hash(in: Message): Digest = Keccak256.hash(in)
}
