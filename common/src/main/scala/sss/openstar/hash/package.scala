package sss.openstar


import sss.ancillary.ByteArrayEncodedStrOps._

import java.util
import scala.annotation.tailrec


package object hash {

  type Message = Array[Byte]
  type Digest = Array[Byte]

  trait CryptographicHash {

    def apply(asStream: Seq[Message], failOnEmpty: Boolean = false): Digest = {

      @tailrec
      def fold(stream: Seq[Message], acc: Digest, stillEmpty: Boolean): Digest = {
        stream match {
          case Seq() =>
            if(failOnEmpty) require(!stillEmpty, "This stream was completely empty, hash is useless")
            acc
          case Array() +: tail =>
            fold(tail, acc, stillEmpty)
          case head +: tail =>
            fold(tail, hash(acc ++ head), false)
        }
      }

      fold(asStream, Array.emptyByteArray, true)
    }

    def apply(in: Message): Digest = hash(in)

    def hash(in: Message): Digest
  }


  case class Digest32(digest: Array[Byte]) {
    require(digest.length == 32, s"Digest32 must have 32 bytes, but it has ${digest.length}")

    override def equals(obj: Any): Boolean = obj match {
      case other: Digest32 =>
        util.Arrays.equals(digest, other.digest)
      case _ => false
    }

    override def hashCode(): Int = util.Arrays.hashCode(digest)

    def asString: String = digest.toBase64Str
  }

}
