package sss.openstar.hash


import scorex.crypto.hash.Blake2b256


/**
  * Interface for fast and secure Blake hash function
  */

object FastCryptographicHash extends CryptographicHash {

  def hash(in: Message): Digest = Blake2b256.hash(in)

}
