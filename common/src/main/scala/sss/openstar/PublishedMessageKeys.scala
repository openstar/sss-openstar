package sss.openstar

import sss.openstar.common.block.{BlockChainTxId, _}
import sss.openstar.common.block.serialize.{BlockChainTxIdSerializer, BlockChainTxSerializer, TxMessageSerializer}
import sss.openstar.eventbus._
import sss.openstar.ledger._
import sss.openstar.ledger.serialize.{LedgerItemSerializer, SeqLedgerItemSerializer}


/**
  * Created by alan on 5/24/16.
  */
trait PublishedMessageKeys {

  val SignedTx: Byte = 100
  val SignedTxAck: Byte = 101
  val SignedTxNack: Byte = 102
  val SeqSignedTx: Byte = 103
  val SignedTxConfirm: Byte = 104 //logically belongs with SignedTx, is a client confirm

  val AckConfirmTx: Byte = 105
  val NackConfirmTx: Byte = 106
  val TempNack: Byte = 107
  val ConfirmTx: Byte = 108
  val CommittedTxId: Byte = 109
  val SouthCommittedTx: Byte = 110
  val QuorumRejectedTx: Byte = 111
  val SouthQuorumRejectedTx: Byte = 112

  val MalformedMessage: Byte = 20
  val GenericErrorMessage: Byte = 21

  val StarzBalanceLedger: Byte = 70
  val IdentityLedger: Byte = 71
  val QuorumLedger: Byte = 72
  val OracleLedgerOwnerLedger: Byte = 73
  val CardanoLedger: Byte = 74
  val TestBalanceLedger: Byte = 75
  val CounterLedger: Byte = 76
  val SystemLedger: Byte = 77
  val TollLedger: Byte = 78


  protected val publishedMsgs: MessageInfos =
    MessageInfoComposite[LedgerItem](SignedTx, classOf[LedgerItem], LedgerItemSerializer) +:
    MessageInfoComposite[BlockChainTxId](SignedTxAck, classOf[BlockChainTxId], BlockChainTxIdSerializer) +:
    MessageInfoComposite[TxMessage](SignedTxNack, classOf[TxMessage], TxMessageSerializer) :+
    MessageInfoComposite[SeqLedgerItem](SeqSignedTx, classOf[SeqLedgerItem], SeqLedgerItemSerializer) +:
    MessageInfoComposite[BlockChainTx](ConfirmTx, classOf[BlockChainTx], BlockChainTxSerializer) +:
    MessageInfoComposite[BlockChainTxId](SignedTxConfirm, classOf[BlockChainTxId], BlockChainTxIdSerializer) +:
    MessageInfoComposite[BlockChainTxId](CommittedTxId, classOf[BlockChainTxId], BlockChainTxIdSerializer) +:
    MessageInfoComposite[BlockChainTxId](SouthQuorumRejectedTx, classOf[BlockChainTxId], BlockChainTxIdSerializer) +:
    MessageInfoComposite[BlockChainTxId](QuorumRejectedTx, classOf[BlockChainTxId], BlockChainTxIdSerializer) +:
    MessageInfoComposite[BlockChainTx](SouthCommittedTx, classOf[BlockChainTx], BlockChainTxSerializer) +:
    MessageInfoComposite[BlockChainTxId](AckConfirmTx, classOf[BlockChainTxId], BlockChainTxIdSerializer) +:
    MessageInfoComposite[BlockChainTxId](NackConfirmTx, classOf[BlockChainTxId], BlockChainTxIdSerializer) +:
    MessageInfoComposite[TxMessage](TempNack, classOf[TxMessage], TxMessageSerializer) +:
    MessageInfoComposite[StringMessage](MalformedMessage, classOf[StringMessage], StringMessageSerializer) +:
    MessageInfoComposite[StringMessage](GenericErrorMessage, classOf[StringMessage], StringMessageSerializer)


}