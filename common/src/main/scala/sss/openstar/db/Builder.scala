package sss.openstar.db

import sss.db.Db

object Builder {

  trait RequireDb {
    implicit val db: Db
  }

  trait RequireRemoteDb {
    val remoteDb: Db
  }

}
