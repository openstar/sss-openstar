package sss.openstar.db

object BlockHeightToTablePartitionId {
  type BlockHeightToTablePartitionId = Long => Long
}
