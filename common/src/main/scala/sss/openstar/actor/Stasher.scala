package sss.openstar.actor


import akka.actor.{Actor, ActorLogging, Stash}
import akka.pattern.pipe

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

object Stasher {

}

trait Stasher {

  self: Actor with Stash with ActorLogging =>

  private var stashDepth: Int = 0

  case class StasherFutureSuccess[T](t: T)
  case class StasherFutureFailure(t: Throwable)

  def pop(popDepth: Int = stashDepth): Unit = {

    0 to popDepth foreach { _ =>
      if(stashDepth > 0) {
        stashDepth -= 1
        //println(s"Unbecome, depth now $stashDepth")
        context.unbecome()
      }
    }

    if(stashDepth == 0) {
      unstashAll()
    }
  }


  private val defaultReceive: Receive = {
    case x => stash()

  }

  def stashWhileDoing[T](f: => Future[T], receive: Receive): Unit = {

    import context.dispatcher

    stashDepth += 1
    //println(s"Stash d now - $stashDepth")

    context.become(receive orElse defaultReceive, discardOld = false)

    Future.fromTry(Try(f)).flatten.map(StasherFutureSuccess[T])
      .recover {
        case e => StasherFutureFailure(e)
      } pipeTo context.self

  }

}
