package sss.openstar.actor

import akka.actor.{Actor, ActorLogging}
import sss.ancillary.Restarter.AttemptRestart

import scala.concurrent.duration._
import scala.reflect.classTag

case object AllStop

trait SystemPanic {

  this: Actor with ActorLogging =>

  def systemPanic(e: Throwable): Unit = {
    log.error("System panic {} ", e)
    systemPanic()
  }

  def systemPanic(msg: String = ""): Unit = {
    log.error(s"***** Game over man, game over *****")
    log.error(msg)
    systemExit()
  }

  def systemExit(): Unit = context.system.eventStream.publish(AllStop)
}
