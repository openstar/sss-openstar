package sss.openstar.merkle

import sss.ancillary.Logging
import sss.db._
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.db.BlockHeightToTablePartitionId.BlockHeightToTablePartitionId
import sss.openstar.hash.SecureCryptographicHash
import sss.openstar.schemamigration.dynamic.MerkleMigration

import scala.collection.immutable

/** Copyright Stepping Stone Software Ltd. 2016, all rights reserved. mcsherrylabs on 3/16/16.
  */
object MerklePersister {

  implicit def hash(a: immutable.ArraySeq[Byte], b: immutable.ArraySeq[Byte]): immutable.ArraySeq[Byte] = {
    val inOrder: immutable.ArraySeq[Byte] = if (a.head > b.head) {
      a ++ b
    } else b ++ a
    immutable.ArraySeq.from(SecureCryptographicHash.hash(inOrder.toArray))
  }

  def apply(blockHeightToTablePartitionId: BlockHeightToTablePartitionId): MerklePersister =
    new MerklePersister(tableName = height => MerkleMigration.merkleTableName(blockHeightToTablePartitionId(height)))
}

class MerklePersister private (tableName: Long => String) extends Logging {

  private def getView(height: Long)(implicit db: Db): InsertableView = db.insertableView(tableName(height), where("height" -> height))

  private def insert(height: Long, values: Map[String, Any])(implicit db: Db) = {
    val view = getView(height)
    view.maxId().map(_ + 1L).flatMap(index =>
        view.insertNoIdentity(Map("id" -> index, "height" -> height) ++ values).map {
          case 1 => index
          case _ =>
            throw new DbError(
              s"Failed to insert the row with id as $index and height as $height into table $tableName"
            )
        }
      )
      .flatMap(view.getRow)
      .map(_.getOrElse(DbError(s"Failed to retrieve row just written to ${tableName(height)} with height as $height")))
  }

  def persist(height: Long, mt: MerkleTree[_])(implicit db: Db): Unit = persistFTx(height, mt).dbRunSyncGet

  def persistFTx(height: Long, mt: MerkleTree[_])(implicit db: Db): FutureTx[Unit] = {
    def persist(subTree: MerkleTree[_], parentIdOpt: Option[Int]): FutureTx[Unit] = {

      insert(height, Map("hash" -> subTree.root, "parentId" -> parentIdOpt)).map(_.int("id")).flatMap { pId =>
        val nextParentId = Option(pId)

        subTree match {
          case mtb: MerkleTreeBranch[_] =>
            persist(mtb.left, nextParentId)
              .flatMap(r => if (mtb.left != mtb.right) persist(mtb.right, nextParentId) else FutureTx.unit(r))

          case MerkleTreeLeaf(left: IndexedSeq[_], right: IndexedSeq[_]) =>
            insert(height, Map("hash" -> left, "parentId" -> nextParentId)).flatMap { _ =>
              if (left != right) {
                insert(height, Map("hash" -> right, "parentId" -> nextParentId)).map(_ => ())
              } else FutureTx.unit(())
            }
        }
      }
    }

    for {
      _ <- getView(height) delete where("height" -> height)
      count <- getView(height).count
      _ = require(count == 0, s"$tableName table should have 0 elements with height==$height but has $count, the results will be invalid.")
      _ <- persist(mt, None)
    } yield ()
  }

  def path(height: Long, leaf: Array[Byte])(implicit db: Db): Option[Seq[Array[Byte]]] = {
    val view = getView(height)

    def getParentHash(acc: Seq[Array[Byte]], parentIdOpt: FutureTx[Option[Int]]): FutureTx[Option[Seq[Array[Byte]]]] =
      parentIdOpt.flatMap({
        case Some(pId) =>
          view.getRow(where("id" -> pId)).flatMap {
            case Some(row) =>
              val hash: Array[Byte] = row.arrayByte("hash")
              val nextParentId      = Option(row.int("parentId"))
              getParentHash(hash +: acc, FutureTx.unit(nextParentId))
            case None => throw new Error(s"Impossible to not read parentId $pId")
          }
        case None if acc.isEmpty => FutureTx.unit(None)
        case None                => FutureTx.unit(Some(acc))
      })

    val parentIdOptFutTx = view.find(where("hash" -> leaf)).map(rOpt => rOpt.flatMap(_.intOpt("parentId")))
    getParentHash(Nil, parentIdOptFutTx)

  }.dbRunSyncGet

  def drop(height: Long)(implicit db: Db): Unit = (getView(height) delete where("height" -> height)).dbRunSyncGet
}
