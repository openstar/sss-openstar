package sss.openstar.common.block.serialize

import sss.ancillary.Serialize._
import sss.openstar.common.block._
import sss.openstar.ledger._

/**
  * Copyright Stepping Stone Software Ltd. 2016, all rights reserved.
  * mcsherrylabs on 3/3/16.
  */
object BlockTxSerializer extends Serializer[BlockTx] {

  override def toBytes(btx: BlockTx): Array[Byte] =
    LongSerializer(btx.index) ++
      SequenceSerializer(btx.ledgerItems.value.map(_.toBytes))
        .toBytes

  override def fromBytes(b: Array[Byte]): BlockTx = {
    val extracted =

      b.extract(
        LongDeSerialize,
        SequenceDeSerialize(_.toLedgerItem)
      )

    BlockTx(extracted._1, SeqLedgerItem(extracted._2))
  }

}
