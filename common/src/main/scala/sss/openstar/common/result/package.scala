package sss.openstar.common

package object result {


  /**
   * A generic trait for widespread use in indicating an error
   * The code is a string that indicates a well defined error used to allow
   * clients to react to the code.
   * The msg is
   */
  trait ErrorMsg {
    val code: String
    val logMsg: Option[String]
    val userMsg: Option[String]

    override def toString: String = {
      val logMsgStr = logMsg.map(m => s", $m").getOrElse("")
      val userStr = userMsg.map(m => s", $m").getOrElse("")
      s"ErrorMsg: ${code}${logMsgStr}${userStr}"
    }
  }

  case class ErrorMessage(code: String,
                          logMsg: Option[String],
                          userMsg: Option[String]) extends ErrorMsg

  object ErrorMessage {
    def apply(code: String): ErrorMessage = ErrorMessage(code, None, None)
    def apply(code: String, logMsg: String, userMsg: String): ErrorMessage =
      ErrorMessage(code, Some(logMsg), Some(userMsg))

    def LogMsg(code: String, logMsg: String): ErrorMessage =
      ErrorMessage(code, Some(logMsg), None)

    def UserMsg(code: String, userMsg: String): ErrorMessage =
      ErrorMessage(code, None, Some(userMsg))
  }

  type Res[T] = Either[ErrorMsg, T]

  def exception[T](msg: String): T = throw new RuntimeException(msg)
  def illegalArgumentEx[T](msg: String): T = throw new IllegalArgumentException(msg)

  object ResultOps {
    implicit class ResT[T](val r: Res[T]) extends AnyVal {
      def get: T = r.fold(e => exception(e.toString), identity)
    }
  }

}
