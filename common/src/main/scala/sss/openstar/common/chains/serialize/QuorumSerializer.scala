package sss.openstar.common.chains.serialize

import sss.ancillary.Serialize._
import sss.openstar.common.chains.Quorum

object QuorumSerializer extends Serializer[Quorum]{

  override def toBytes(q: Quorum): Array[Byte] = {

    (ByteSerializer(q.chainId) ++
      SequenceSerializer(q.members.toSeq.map(StringSerializer)) ++
      IntSerializer(q.minConfirms)).toBytes

  }

  override def fromBytes(b: Array[Byte]): Quorum =  {
    val extracted = b.extract(
      ByteDeSerialize,
      SequenceDeSerialize(_.extract(StringDeSerialize)),
      IntDeSerialize)

    Quorum(
      extracted._1,
      extracted._2.toSet,
      extracted._3
    )
  }
}
