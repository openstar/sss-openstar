package sss.openstar.common

import sss.ancillary.Serialize.ToBytes
import sss.openstar.chains.Chains.GlobalChainIdMask
import sss.openstar.common.chains.serialize.QuorumSerializer
import sss.openstar.{BusEvent, UniqueNodeIdentifier}


package object chains {

  /**
    *
    * @param chainId
    * @param members the currently online members of the quorum
    * @param minConfirms the minimum number of members required to be in agreement to move forward.
    */
  case class Quorum(chainId: GlobalChainIdMask, members: Set[UniqueNodeIdentifier], minConfirms: Int) extends BusEvent

  implicit class QuorumTo(q: Quorum) extends ToBytes {
    override def toBytes: Array[Byte] = QuorumSerializer.toBytes(q)
  }

  implicit class QuorumFrom(bs: Array[Byte]) {
    def toQuorum: Quorum = QuorumSerializer.fromBytes(bs)
  }
}
