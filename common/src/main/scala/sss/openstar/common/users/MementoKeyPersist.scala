package sss.openstar.common.users

import sss.ancillary.Memento
import sss.openstar.account.{KeyPersistRead, KeyPersistWrite}

import scala.concurrent.Future
import scala.util.Try

object MementoKeyPersist extends KeyPersistRead with KeyPersistWrite {

  private def memento(identity: String, tag: String): Memento = Memento(s"$identity.$tag")

  override def read(identity: String, tag: String): Future[Option[String]] =
    Future.fromTry(Try(memento(identity, tag).read))

  override def write(identity: String, tag: String, content: String): Future[Unit] =
    Future.fromTry(Try(memento(identity, tag).write(content)))

  override def remove(identity: String, tag: String): Future[Boolean] =
    Future.fromTry(Try(memento(identity, tag).clear))

}
