package sss.openstar.common.users

import sss.ancillary.Logging
import sss.openstar.account.KeyPersistRead
import sss.openstar.util.IOExecutionContext

import java.net.URL
import scala.concurrent.{ExecutionContext, Future}
import scala.io.Source
import scala.jdk.CollectionConverters.EnumerationHasAsScala
import scala.util.{Try, Using}

class ClasspathKeyPersistRead(implicit ioEc :IOExecutionContext) extends KeyPersistRead with Logging {

  import ioEc.ec

  override def read(identity: String, tag: String): Future[Option[String]] = Future {
    readClasspathKeyFor(identity, tag)
  }

  private def readClasspathKeyFor(identity: String, tag: String): Option[String] = {
    def safeRead(resource: URL) =
      Using(Source.fromFile(resource.toURI))(_.getLines().toSeq.headOption).toOption.flatten

    val fileName = s"$identity.$tag"
    Try(getClass.getClassLoader.getResources(fileName).asScala.toList).toOption
      .flatMap { urls =>
        log.debug(s"Found ${urls.size} keys from classpath for $fileName. Loading: ${urls.headOption}")
        urls.headOption
      }
      .flatMap(safeRead)
  }

}
