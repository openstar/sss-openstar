package sss.openstar.common.users

import sss.db.ops.DbOps.{DbRunOps, FutureTxOps, OptFutureTxOps}
import sss.db.{Db, FutureTx, PagedView, Row, where}
import sss.openstar.UniqueNodeIdentifier
import sss.openstar.account.{KeyPersistRead, KeyPersistWrite}
import sss.openstar.common.users.AdminStatus.AdminStatus
import sss.openstar.schemamigration.SqlSchemaNames.ColumnNames.{identityCol, statusCol, _}
import sss.openstar.schemamigration.SqlSchemaNames.TableNames.{usersKeysTableName, usersTableName}

import java.sql.SQLIntegrityConstraintViolationException
import scala.concurrent.Future

class DbUserService(schemaName: String)(implicit db: Db) extends KeyPersistWrite
  with KeyPersistRead
  with UserDirectory {

  private def addSchemaName(tableName: String): String = s"${schemaName}.$tableName"

  private lazy val tagsTable  = db.table(addSchemaName(usersKeysTableName))
  private lazy val userTable = db.table(addSchemaName(usersTableName))

  import db.asyncRunContext.ec

  override def read(identity: String, tag: String): Future[Option[String]] = (for {
    parentRowOpt <- findUser(identity)
    tagRowOpt <- parentRowOpt.map { parentRow =>
      tagsTable.find(where(identityLnkCol -> parentRow.id, tagCol -> tag))
    }.toFutureTxOpt
    result = tagRowOpt.flatten.map(_.string(encryptedKeyFileCol))
  } yield result).dbRun

  private def findUser(identifier: UniqueNodeIdentifier): FutureTx[Option[Row]] = {
    userTable.find(
      where(identityCol -> identifier)
    )
  }

  private def findOrCreate(identifier: UniqueNodeIdentifier,
                           status: AdminStatus = AdminStatus.normal): FutureTx[Row] = (for {

    userOpt <- findUser(identifier)
    foundOrCreated <- userOpt match {
      case None => userTable.insert(
        Map(
        identityCol -> identifier,
        statusCol -> status.id
        )
      )
      case Some(row) => FutureTx.unit(row)
    }
  } yield foundOrCreated).recoverWith {
    case _: SQLIntegrityConstraintViolationException =>
      findUser(identifier)
        .map(_.getOrElse(throw new IllegalStateException(s"User ($identifier) cannot be inserted and does not exist?")))
  }

  private def addTag(parentRow: Row, tag: String, content: String): FutureTx[Row] = for {
    newRow <- tagsTable.insert(
      Map(
        identityLnkCol -> parentRow.id,
        tagCol -> tag,
        encryptedKeyFileCol -> content
      )
    )
  } yield newRow

  override def write(
                      identity: String,
                      tag: String,
                      content: String): Future[Unit] = (for {

    row <- findOrCreate(identity)
    _ <- addTag(row, tag, content)
  } yield ()).dbRun



  private def rowToUser(row: Row): User =
    User(
      row.string(identityCol),
      AdminStatus(row.int(statusCol))
    )

  def lockUser(identity: UniqueNodeIdentifier): Future[Option[User]] = {
    setUserKeyStatus(identity, AdminStatus.locked)
  }

  def unLockUser(identity: UniqueNodeIdentifier): Future[Option[User]] = {
    setUserStatus(identity, AdminStatus.normal)
  }

  private def setUserKeyStatus(identity: UniqueNodeIdentifier, adminStatus: AdminStatus): Future[Option[User]] = {
    userTable.update(Map(
      statusCol -> adminStatus.id
    ), where(identityCol -> identity)).flatMap { rowsUpdated =>
      userTable.find(where(identityCol -> identity))
    }.dbRun.map(_.map(rowToUser))
  }

  private def setUserStatus(identity: UniqueNodeIdentifier, adminStatus: AdminStatus): Future[Option[User]] =
    userTable.update(Map(
      statusCol -> adminStatus.id
    ), where(identityCol -> identity))
    .flatMap { numUpdates =>
    findUser(identity)
      .map(_.map(rowToUser))
  }.dbRun

  override def tags(identity: UniqueNodeIdentifier): Future[Seq[String]] = {
    for {
      rowOpt <- findUser(identity)
      row = rowOpt.getOrElse(throw new IllegalArgumentException(s"No such user $identity"))
      rows <- tagsTable.filter(where(identityLnkCol -> row.id))
      tags = rows.map(_.string(tagCol))
    } yield tags

  }.dbRun

  override def listUsers(pageSize: Int): LazyList[User] = {
    import db.syncRunContext
    PagedView(userTable, pageSize).toStream.map(rowToUser)
  }


  override def remove(identity: String, tag: String): Future[Boolean] =
    deleteUserKey(identity, tag).map(_.getOrElse(false))

  override def deleteUserKey(identity: UniqueNodeIdentifier, tag: String): Future[Option[Boolean]] = {
    for {
      rowOpt <- findUser(identity)
      deletedOkOpt <- rowOpt
        .map(row => tagsTable
          .delete(where(tagCol -> tag, identityLnkCol -> row.id)))
        .toFutureTxOpt

    } yield deletedOkOpt.map(_ == 1)
  }.dbRun

  override def deleteUser(identity: UniqueNodeIdentifier): Future[Unit] = {
    for {
      rowOpt <- findUser(identity)
      row = rowOpt.getOrElse(throw new IllegalArgumentException(s"No such user $identity"))
      _ <- tagsTable.delete(where(identityLnkCol -> row.id))
      _ <- userTable.delete(where(identityCol -> identity))
    } yield ()
  }.dbRun

}
