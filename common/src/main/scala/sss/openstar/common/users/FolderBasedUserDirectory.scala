package sss.openstar.common.users

import sss.ancillary.Configure
import sss.openstar.UniqueNodeIdentifier
import sss.openstar.account.NodeIdTag
import sss.openstar.common.users.FolderBasedUserDirectory.NodeIdTagPath

import java.io.File
import java.nio.file.{FileVisitOption, Files, Path, Paths}
import scala.concurrent.Future
import scala.jdk.StreamConverters._
import scala.util.Try

object FolderBasedUserDirectory extends Configure{

  private case class NodeIdTagPath(n: NodeIdTag, p: Path)

  def apply(): FolderBasedUserDirectory = {
    val mementoRoot = config.getString("memento.folder")
    new FolderBasedUserDirectory(Paths.get(mementoRoot))
  }
}

class FolderBasedUserDirectory(keyFolder: Path) extends UserDirectory {


  private val descendOnlyOneLevel = 1


  private def validate(p: Path): Boolean =
    p.toFile.exists() && p.toFile.isFile

  private def pathToNameTag(p: Path): NodeIdTagPath = {
    if (p.toFile.getName.split("\\.").length == 2) {
      val nameTag = p.toFile.getName.split("\\.")
      NodeIdTagPath(NodeIdTag(nameTag.head, nameTag.last), p)
    } else {
      val nameTag = p.toFile.getName.split("\\.")
      NodeIdTagPath(NodeIdTag(nameTag.dropRight(1).mkString("."), nameTag.last), p)
    }
  }

  /**
   *
   * @param pageSize Ignored in this implementation.
   * @return
   */
  def listUsers(pageSize: Int): LazyList[User] = {

    Files.walk(keyFolder, descendOnlyOneLevel, FileVisitOption.FOLLOW_LINKS)
      .filter(validate)
      .map(pathToNameTag)
      .map(n => User(n.n.nodeId, AdminStatus.normal))
      .toScala(LazyList)
      .distinctBy(_.identity)

  }

  override def tags(identity: UniqueNodeIdentifier): Future[Seq[String]] = Future.successful {

    allForUser(identity)
      .map(_.n.tag)

  }

  private def allForUser(identity: UniqueNodeIdentifier): List[NodeIdTagPath] = {
    Files.walk(keyFolder, descendOnlyOneLevel, FileVisitOption.FOLLOW_LINKS)
      .filter(validate)
      .map(pathToNameTag)
      .filter(_.n.nodeId == identity)
      .toScala(List)

  }

  override def deleteUserKey(identity: UniqueNodeIdentifier, tag: String): Future[Option[Boolean]] = Future.successful {
    val asPath = keyFolder.resolve(s"$identity.$tag")
    Option.when(validate(asPath))(asPath.toFile.delete())
  }

  override def deleteUser(identity: UniqueNodeIdentifier): Future[Unit] = Future.fromTry(Try {

    val all = Files.newDirectoryStream(keyFolder, identity + ".*")
    all.iterator().forEachRemaining(p =>
      if (validate(p)) {
        p.toFile.delete()
      }
    )
  })

}

