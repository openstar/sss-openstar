package sss.openstar.common.users

import sss.openstar.UniqueNodeIdentifier
import sss.openstar.common.users.AdminStatus.AdminStatus

import scala.concurrent.{ExecutionContext, Future}


object AdminStatus extends Enumeration {
  type AdminStatus = Value
  val normal = Value(1)
  val locked = Value(2)
}

case class User(
                    identity: UniqueNodeIdentifier,
                    status: AdminStatus
                  )

trait UserDirectory {

  def hasUserKey(identity: UniqueNodeIdentifier, tag: String)(implicit ec: ExecutionContext): Future[Boolean] = {
    tags(identity).map(_.exists(_ == tag))
  }

  def deleteUserKey(identity: UniqueNodeIdentifier, tag: String): Future[Option[Boolean]]

  def deleteUser(identity: UniqueNodeIdentifier): Future[Unit]

  def tags(identity: UniqueNodeIdentifier): Future[Seq[String]]

  /**
   *
   * @param pageSize dictate the page size of the underlying stream generating fetch
   * @return
   */
  def listUsers(pageSize: Int = 100): LazyList[User]

}
