package sss.openstar.common.users

import sss.openstar.common.builders.ConfigBuilder

import java.nio.file.Paths


trait FolderBasedUserDirectoryBuilder {

  self: ConfigBuilder =>

  lazy val keyFolder = {
    val folderName = config.getString("memento.folder")
    val p = Paths.get(folderName)
    p.toFile.mkdirs()
    p
  }

  lazy val folderBaseUserDirectory = new FolderBasedUserDirectory(keyFolder)
}

