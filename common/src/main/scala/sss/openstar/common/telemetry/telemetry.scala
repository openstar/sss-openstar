package sss.openstar.common

import akka.util.ByteString
import sss.ancillary.Serialize.ToBytes
import sss.openstar.common.block.BlockId
import sss.openstar.common.chains.Quorum
import sss.openstar.common.telemetry.serialize.ReportSerializer


package object telemetry {

  case class Report(nodeName: String,
                    reportIntervalSeconds: Int,
                    lastBlock: Option[BlockId],
                    leader: Option[String],
                    quorum: Option[Quorum],
                    numConnections: Int,
                    connections: Seq[String]
                   )

  implicit class ReportToBytes(report:Report) extends ToBytes {
    override def toBytes: Array[Byte] = ReportSerializer.toBytes(report)
    def toByteString: ByteString = ByteString(ReportSerializer.toBytes(report))
  }

  implicit class ReportFromByteString(bs:ByteString)  {
    def toReport: Report = ReportSerializer.fromBytes(bs.toArray)
  }

  implicit class ReportFromBytes(bs:Array[Byte])  {
    def toReport: Report = ReportSerializer.fromBytes(bs)
  }
}
