package sss.openstar.common.telemetry.serialize

import sss.ancillary.SeqSerializer._
import sss.ancillary.Serialize._
import sss.openstar.common.block.{BlockId, BlockIdFrom, BlockIdTo}
import sss.openstar.common.chains.Quorum
import sss.openstar.common.chains.serialize.QuorumSerializer
import sss.openstar.common.telemetry.Report

object ReportSerializer extends Serializer[Report]  {

  override def toBytes(report: Report): Array[Byte] = {
    StringSerializer(report.nodeName) ++
    IntSerializer(report.reportIntervalSeconds) ++
    OptionSerializer[BlockId](report.lastBlock, bId => ByteArraySerializer(bId.toBytes)) ++
      OptionSerializer[String](report.leader, s => StringSerializer(s)) ++
      OptionSerializer[Quorum](report.quorum, r => ByteArraySerializer(r.toBytes)) ++
      IntSerializer(report.numConnections) ++
      report.connections.map(StringSerializer).toBytes
  }

  override def fromBytes(b: Array[Byte]): Report = {

    Report.tupled(b.extract(
      StringDeSerialize,
      IntDeSerialize,
      OptionDeSerialize(
        ByteArrayDeSerialize(
          BlockIdFrom(_).toBlockId
        )
      ),
      OptionDeSerialize(
        StringDeSerialize
      ),
      OptionDeSerialize(
        ByteArrayDeSerialize(
           QuorumSerializer.fromBytes
        )
      ),
      IntDeSerialize,
      SequenceDeSerialize(_.extract(StringDeSerialize))
    ))

  }
}
