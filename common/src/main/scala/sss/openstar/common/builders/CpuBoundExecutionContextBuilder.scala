package sss.openstar.common.builders

import scala.concurrent.ExecutionContext



trait CpuBoundExecutionContextBuilder {

  //Do not use this and then block the thread. use for cpu bound load only.
  implicit val cpuOnlyEc: ExecutionContext

}

trait CpuBoundExecutionContextFromActorSystemBuilder extends CpuBoundExecutionContextBuilder {

  self: ActorSystemBuilder =>

  implicit val cpuOnlyEc: ExecutionContext = actorSystem.dispatcher
}

