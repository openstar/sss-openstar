package sss.openstar.common.builders

import scala.io.StdIn

trait RequirePhrase {

  private def getPhraseFromConsole: String = {

    println("If you mistype the password, you must restart the process to get another chance.")
    println("If this is the first time entering the password, do not forget it, it is unrecoverable.")
    println("")
    println("Unlock key phrase:")
    Option(System.console()) match {
      case None =>
        println("WARNING No system console found, the password may echo to the console")
        StdIn.readLine()
      case Some(standardIn) =>
        val chars = standardIn.readPassword
        new String(chars)
    }
  }

  def phraseProvidedOrFromConsole: String = phrase.getOrElse(getPhraseFromConsole)

  val NodePhraseEnvName = "OS_NODE_PHRASE"

  def phrase: Option[String] = sys.env.get(NodePhraseEnvName)

}

