package sss.openstar.common.builders

trait ShutdownHookBuilder {

  def shutdown(): Unit

  Runtime.getRuntime.addShutdownHook(new Thread() {
    override def run(): Unit = {
      shutdown()
    }
  })
}
