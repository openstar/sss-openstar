package sss.openstar.common.builders

import sss.openstar.util.IOExecutionContext

trait IOExecutionContextBuilder {
  implicit lazy val ioExecutionContext: IOExecutionContext =
    new IOExecutionContext(
      sss.db.ExecutionContextHelper.ioExecutionContext
    )
}
