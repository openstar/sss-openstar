package sss.openstar.common.builders

import sss.openstar.account.{KeyEncryption, KeyPersistRead, KeyPersistWrite, KeyPersister}
import sss.openstar.common.users.{ClasspathKeyPersistRead, MementoKeyPersist}
import sss.openstar.crypto.keypairs.CBCKeyEncryption

trait KeyPersisterBuilder {

  self: IOExecutionContextBuilder =>

  lazy val keyEncryption: KeyEncryption = CBCKeyEncryption

  lazy val orderedSeqKeyReaders: Seq[KeyPersistRead] = Seq(MementoKeyPersist, new ClasspathKeyPersistRead())

  lazy val keyWriter: KeyPersistWrite = MementoKeyPersist

  lazy val keyPersister: KeyPersister =
    new KeyPersister(orderedSeqKeyReaders, keyWriter, keyEncryption)
}

