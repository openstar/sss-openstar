package sss.openstar.common.builders

import com.typesafe.config.Config
import sss.ancillary.Configure


trait RequireConfig {
  val prefixedConf: Config
}

trait ConfigBuilder extends RequireConfig with Configure {
  lazy val configName: String = "node"
  override lazy val prefixedConf: Config = config(configName)
}
