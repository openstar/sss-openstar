package sss.openstar.common.builders

import akka.actor.ActorSystem

trait ActorSystemBuilder {
  lazy implicit val actorSystem: ActorSystem = ActorSystem("openstar-network-node")
}
