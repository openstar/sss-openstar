package sss.openstar.common.builders

import sss.ancillary.FutureOps.AwaitResult
import sss.db.ExecutionContextHelper.ioExecutionContext
import sss.openstar.account.NodeIdentityManager.{GetSignerDetails, PasswordCredentials, SignerDetails}
import sss.openstar.account.SignerFactory.SignerDescriptor
import sss.openstar.account._
import sss.openstar.account.impl.EncryptedKeyFileAccountSignerCreator
import sss.openstar.crypto.keypairs.DefaultAccountKeysFactoryBuilder

import scala.concurrent.Future
import scala.util.Try

trait RequireNodeIdentity {
  val nodeIdentityManager: NodeIdentityManager
  val nodeIdentity: NodeIdentity
}

trait NodeIdTagBuilder {

  self: RequireConfig =>

  val nodeIdKey = "nodeId"
  val tagKey = "tag"

  lazy val nodeIdTag = NodeIdTag(prefixedConf.getString(nodeIdKey), prefixedConf.getString(tagKey))

}

trait NodeIdentityManagerBuilder {

  self: KeyPersisterBuilder
    with VerifierFactoryBuilder
    with SignerFactoryBuilder
    with RequirePhrase
    with NodeIdTagBuilder
    with DefaultAccountKeysFactoryBuilder =>

  def getSignerDetails: GetSignerDetails

  lazy val nodeIdentityManager: NodeIdentityManager =
    new NodeIdentityManager(
      verifierFactory,
      getSignerDetails,
      signerFactory)


  def getLocalKeyFileSignerDetails: GetSignerDetails = nodeId =>
    if (nodeId == nodeIdTag.nodeId) {
      createOrGetSignerDetailsFromKeyFile.map(Seq(_))
    } else Future.successful(Seq.empty)

  private def createOrGetSignerDetailsFromKeyFile: Future[SignerDetails] = {

    val kpGen = () => defaultAccountKeysFactory.createKeyPair()
    //creating it here if necessary means it will exist later.
    val kpF = keyPersister.findOrCreate(
      nodeIdTag.nodeId,
      nodeIdTag.tag,
      phraseProvidedOrFromConsole,
      kpGen
    )

    kpF map { kp =>
      SignerDetails(
        nodeIdTag,
        kp.pub,
        None,
        SignerDescriptor(
          defaultAccountKeys.keysType,
          SignerSecurityLevels.SecurityLevel1,
          EncryptedKeyFileAccountSignerCreator.SignerType,
          SignerInteractionTypes.AutomaticSignerInteractionType
        )
      )
    }
  }
}

trait NodeIdentityBuilder extends RequireNodeIdentity {

  self: NodeIdentityManagerBuilder
    with KeyPersisterBuilder
    with RequirePhrase
    with NodeIdTagBuilder =>

  lazy implicit val nodeIdentity: NodeIdentity =
    nodeIdentityManager(
      nodeIdTag.nodeId, nodeIdTag.tag, Seq(PasswordCredentials(nodeIdTag, phraseProvidedOrFromConsole)))
      .await()

}
