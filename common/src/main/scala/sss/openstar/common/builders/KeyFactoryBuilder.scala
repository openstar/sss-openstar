package sss.openstar.common.builders

import sss.openstar.account.KeyFactory
import sss.openstar.crypto.keypairs.DefaultKeyFactory

trait KeyFactoryBuilder {

  def keysFactoryLookup: KeyFactory = DefaultKeyFactory
}
