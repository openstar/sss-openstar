package sss.openstar.common.builders

import sss.openstar.crypto.SeedBytes

trait SeedBytesBuilder {
  lazy val seedBytes: SeedBytes = SeedBytes
}

