package sss.openstar.common.builders

import sss.openstar.account._
import sss.openstar.account.impl.{DefaultSignerFactory, EncryptedKeyFileAccountSignerCreator}


trait VerifierFactoryBuilder {
  self: KeyFactoryBuilder =>
  implicit lazy val verifierFactory = new VerifierFactory(keysFactoryLookup)

}

trait SignerFactoryBuilder {

  self: KeyPersisterBuilder
    with CpuBoundExecutionContextFromActorSystemBuilder
    with ActorSystemBuilder
    with KeyFactoryBuilder =>


  lazy val signerFactory: SignerFactory = DefaultSignerFactory(
    new EncryptedKeyFileAccountSignerCreator(
      keyPersister,
      keysFactoryLookup
    )(actorSystem.dispatcher)
  )

}
