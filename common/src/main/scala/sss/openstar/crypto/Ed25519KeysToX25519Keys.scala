package sss.openstar.crypto

import org.bouncycastle.crypto.Digest
import org.bouncycastle.math.ec.rfc7748.X25519
import org.bouncycastle.math.ec.rfc8032.Ed25519
import org.bouncycastle.util.Arrays

/**
 * Ref
 * https://github.com/quarck/cxifri/blob/974be361ee2c3443edf2eed7b79ea7c7a47b5709/app/src/main/java/org/bouncycastle/math/ec/rfc8032/Ed25519.java
 */
object Ed25519KeysToX25519Keys {

  import org.bouncycastle.math.ec.rfc7748.X25519Field

  class PointAffine {
    val x: Array[Int] = X25519Field.create
    val y: Array[Int] = X25519Field.create
  }

  val POINT_BYTES = 32

  import org.bouncycastle.math.raw.Nat256

  private def decode32(bs: Array[Byte], off: Int): Int = {
    var offVar = off
    var n = bs(offVar) & 0xFF
    n |= (bs({
      offVar += 1; offVar
    }) & 0xFF) << 8
    n |= (bs({
      offVar += 1; offVar
    }) & 0xFF) << 16
    n |= bs({
      offVar += 1; offVar
    }) << 24
    n
  }

  private val P = Array[Int](0xFFFFFFED, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x7FFFFFFF)
  private val C_d: Array[Int] = Array[Int](0x035978A3, 0x02D37284, 0x018AB75E, 0x026A0A0E, 0x0000E014, 0x0379E898, 0x01D01E5D, 0x01E738CC, 0x03715B7F, 0x00A406D9)

  def decode32(bs: Array[Byte], bsOff: Int, n: Array[Int], nOff: Int, nLen: Int): Array[Int] = {
    for (i <- 0 until nLen) {
      n(nOff + i) = decode32(bs, bsOff + i * 4)
    }
    n
  }

  private def checkPointVar(p: Array[Byte]) = {
    val t: Array[Int] = Array.fill(8)(0)
    decode32(p, 0, t, 0, 8)
    t(7) &= 0x7FFFFFFF
    !Nat256.gte(t, P)
  }

  private def decodePointVar(p: Array[Byte], pOff: Int, negate: Boolean, r: Ed25519KeysToX25519Keys.PointAffine): Boolean = {
    val py: Array[Byte] = Arrays.copyOfRange(p, pOff, pOff + POINT_BYTES)
    if (!checkPointVar(py)) {
      false
    } else {
      val x_0 = (py(POINT_BYTES - 1) & 0x80) >>> 7
      py(POINT_BYTES - 1) = (py(POINT_BYTES - 1) & 0x7F).byteValue()
      X25519Field.decode(py, 0, r.y)
      val u = X25519Field.create
      val v = X25519Field.create
      X25519Field.sqr(r.y, u)
      X25519Field.mul(C_d, u, v)
      X25519Field.subOne(u)
      X25519Field.addOne(v)
      if (!X25519Field.sqrtRatioVar(u, v, r.x)) return false
      X25519Field.normalize(r.x)
      if (x_0 == 1 && X25519Field.isZeroVar(r.x)) return false
      if (negate ^ (x_0 != (r.x(0) & 1))) X25519Field.negate(r.x, r.x)
      true
    }
  }

  private def obtainYFromPublicKey(ed25519PublicKey: Array[Byte]): Array[Int] = {
    val pA = new PointAffine();

    val result = decodePointVar(ed25519PublicKey, 0, true, pA);
    if (!result)
      return null;

    pA.y;
  }

  def toX25519PublicKey(ed25519PublicKey: Array[Byte]): Array[Byte] =
  {
    val one: Array[Int] = Array.fill(X25519Field.SIZE)(0)
    X25519Field.one(one);

    val y: Array[Int] = obtainYFromPublicKey(ed25519PublicKey);
    if (y == null)
      return null;

    val oneMinusY:Array[Int] = Array.fill(X25519Field.SIZE)(0)
    X25519Field.sub(one, y, oneMinusY);

    val onePlusY: Array[Int] =  Array.fill(X25519Field.SIZE)(0)
    X25519Field.add(one, y, onePlusY);

    val oneMinusYInverted: Array[Int] = Array.fill(X25519Field.SIZE)(0)
    X25519Field.inv(oneMinusY, oneMinusYInverted);

    val u: Array[Int] = Array.fill(X25519Field.SIZE)(0)
    X25519Field.mul(onePlusY, oneMinusYInverted, u);

    X25519Field.normalize(u);

    val x25519PublicKey: Array[Byte] = Array.fill(X25519.SCALAR_SIZE)(0.byteValue())
    X25519Field.encode(u, x25519PublicKey, 0);

    x25519PublicKey;
  }

  def toX25519PrivateKey(ed25519PrivateKey: Array[Byte]): Array[Byte] =
  {
    val d: Digest = Ed25519.createPrehash();
    val h: Array[Byte] = Array.fill(d.getDigestSize)(0)

    d.update(ed25519PrivateKey, 0, ed25519PrivateKey.length);
    d.doFinal(h, 0);

    val s: Array[Byte] = Array.fill(X25519.SCALAR_SIZE)(0)

    System.arraycopy(h, 0, s, 0, X25519.SCALAR_SIZE);

    s(0) = (s.head & 0xF8).byteValue
    s(X25519.SCALAR_SIZE - 1) =  (s(X25519.SCALAR_SIZE - 1) & 0x7F).byteValue
    s(X25519.SCALAR_SIZE - 1) = (s(X25519.SCALAR_SIZE - 1) | 0x40).byteValue
    s
  }
}
