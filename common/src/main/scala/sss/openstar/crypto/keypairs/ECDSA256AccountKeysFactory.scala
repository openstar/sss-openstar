package sss.openstar.crypto.keypairs

import scorex.crypto.signatures.{MessageToSign, SharedSecret}
import sss.openstar.account.{AccountKeys, AccountKeysFactory, KeyPair}

import java.security.spec.X509EncodedKeySpec
import java.security.{PublicKey, Signature}
import java.util.Base64

object ECDSA256AccountKeysFactory extends AccountKeysFactory {

  lazy override implicit val accountKeysImp: AccountKeys = ECDSA256AccountKeys

  override def createKeyPair(): KeyPair = {
    throw new IllegalArgumentException(s"NO! Create $keysType")
  }

}

object ECDSA256AccountKeys extends AccountKeys {

  lazy val keysFactory: AccountKeysFactory = ECDSA256AccountKeysFactory

  override val KeyLength: Int = 32
  override val SigLength: Int = 64

  override def sign(privateKey: scorex.crypto.signatures.PrivateKey, message: MessageToSign): scorex.crypto.signatures.Signature = {
//    val priKeySpec: EncodedKeySpec = new X509EncodedKeySpec(privateKey)
//    val keyFactory = KeyFactory.getInstance("EC");
//
//
//    val spec = ECNamedCurveTable.getParameterSpec("secp192r1");
//    val ecPrivateKeySpec = new ECPrivateKeySpec(new BigInteger(1, privateKey), spec);
//
//    val params = new ECNamedCurveSpec("secp192r1", spec.getCurve(), spec.getG(), spec.getN());
//    val w = new java.security.spec.ECPoint(new BigInteger(1, Arrays.copyOfRange(publicKeyBytes, 0, 24)), new BigInteger(1, Arrays.copyOfRange(publicKeyBytes, 24, 48)));
//    PublicKey publicKey = factory.generatePublic(new java.security.spec.ECPublicKeySpec(w, params));
//
//
//    val privKey = keyFactory.generatePrivate(priKeySpec)
//    ecdsaSign.initSign(privKey)
//    ecdsaSign.update(message);
//    scorex.crypto.signatures.Signature(ecdsaSign.sign())
    throw new IllegalArgumentException(s"Cannot create sig with this type $keysType")
  }

  override def verify(publicKey: scorex.crypto.signatures.PublicKey, message: MessageToSign, sig: scorex.crypto.signatures.Signature): Boolean = {
    val ecdsaSign = Signature.getInstance("SHA256withECDSA")
    val keyFactory = java.security.KeyFactory.getInstance("EC")
    val pubKeySpec = new X509EncodedKeySpec(Base64.getDecoder.decode(publicKey))
    val pubKey: PublicKey = keyFactory.generatePublic(pubKeySpec)
    ecdsaSign.initVerify(pubKey)
    ecdsaSign.update(message)
    ecdsaSign.verify(sig)
  }

  override val keysType: String = "SHA256withECDSA"

  override def createSharedSecret(privateKey: scorex.crypto.signatures.PrivateKey, publicKey: scorex.crypto.signatures.PublicKey): SharedSecret = {
    throw new IllegalArgumentException(s"Cannot create shared secret with this type $keysType")
  }
}