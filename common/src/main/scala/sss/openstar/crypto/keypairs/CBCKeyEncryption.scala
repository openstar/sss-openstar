package sss.openstar.crypto.keypairs

import sss.ancillary.ByteArrayEncodedStrOps.{Base64StrToByteArray, ByteArrayToBase64UrlStr}
import sss.ancillary.Logging
import sss.openstar.account.KeyEncryption
import sss.openstar.crypto.AESDetails.AESEncodedKey
import sss.openstar.crypto.password.PasswordStorage
import sss.openstar.crypto.{CBCEncryption, SeedBytes}

import scala.util.Try

object CBCKeyEncryption extends KeyEncryption with Logging {

  val delimiter = ":::"

  override def encrypt(keyPair: (Array[Byte], Array[Byte]), phrase: String): String = {
    val (privKey, pubKey) = keyPair
    val pubKStr: String = pubKey.toBase64Str
    val hashedPhraseWithKey = PasswordStorage.createHash(phrase)
    val iv = CBCEncryption.newInitVector(SeedBytes)
    val encrypted = CBCEncryption.encrypt(AESEncodedKey(hashedPhraseWithKey.getSecretKey.getEncoded), privKey, iv)
    val created = s"$pubKStr:::${hashedPhraseWithKey.getResult}:::${iv.asString}:::${encrypted.toBase64Str}"
    log.debug(s"CREATED - $created")
    created
  }

  override def decrypt(encrypted: String, passPhrase: String): Try[(Array[Byte], Array[Byte])] = Try {
    val aryOfSecuredKeys = encrypted.split(":::")
    require(aryOfSecuredKeys.length == 4,
      s"Encrypted string is not properly delimited, expected 4 segments delimited by $delimiter")
    val pubKStr = aryOfSecuredKeys(0)
    val hashedPhrase = aryOfSecuredKeys(1)
    val iv = CBCEncryption.initVector(aryOfSecuredKeys(2))
    val encryptedPrivateKey = aryOfSecuredKeys(3).fromBase64Str
    val result = PasswordStorage.verifyPassword(passPhrase, hashedPhrase)
    require(result.getResult, "Incorrect password")

    val decryptedKey = CBCEncryption.decrypt(AESEncodedKey(result.getSecretKey.getEncoded), encryptedPrivateKey, iv)
    (decryptedKey, pubKStr.fromBase64Str)
  }
}
