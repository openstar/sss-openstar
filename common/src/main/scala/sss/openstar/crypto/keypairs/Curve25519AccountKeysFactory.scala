package sss.openstar.crypto.keypairs

import scorex.crypto.signatures._
import sss.openstar.account.{AccountKeys, AccountKeysFactory, KeyPair}
import sss.openstar.crypto.SeedBytes

object Curve25519AccountKeysFactory extends AccountKeysFactory {

  lazy override implicit val accountKeysImp: AccountKeys = Curve25519AccountKeys

  def NumSeedBytes = 32

  override def createKeyPair(): KeyPair = {
    val (priv, pub) = Curve25519.createKeyPair(SeedBytes.strongSeed(NumSeedBytes))
    KeyPair(pub, priv)
  }

}

object Curve25519AccountKeys extends AccountKeys {

  lazy val keysFactory: AccountKeysFactory = Curve25519AccountKeysFactory

  override val KeyLength: Int = Curve25519.KeyLength
  override val SigLength: Int = Curve25519.SignatureLength

  override def sign(privateKey: PrivateKey, message: MessageToSign): Signature =
    Curve25519.sign(privateKey, message)

  override def verify(publicKey: PublicKey, message: MessageToSign, sig: Signature): Boolean =
    Curve25519.verify(sig, message, publicKey)

  override val keysType: String = "Curve25519"

  override def createSharedSecret(privateKey: PrivateKey, publicKey: PublicKey): SharedSecret =
    Curve25519.createSharedSecret(privateKey, publicKey)
}