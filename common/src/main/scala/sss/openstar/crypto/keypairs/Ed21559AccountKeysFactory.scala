package sss.openstar.crypto.keypairs

import net.i2p.crypto.eddsa.spec.{EdDSANamedCurveTable, EdDSAParameterSpec, EdDSAPrivateKeySpec, EdDSAPublicKeySpec}
import net.i2p.crypto.eddsa.{EdDSAEngine, EdDSAPrivateKey, EdDSAPublicKey}
import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters
import org.bouncycastle.math.ec.rfc8032.Ed25519
import scorex.crypto.signatures._
import sss.openstar.account.{AccountKeys, AccountKeysFactory, KeyPair}
import sss.openstar.crypto.Ed25519KeysToX25519Keys

import java.security.{MessageDigest, SecureRandom}


object Ed25519AccountKeysFactory extends AccountKeysFactory {

  lazy override implicit val accountKeysImp: AccountKeys = Ed25519AccountKeys

  def createKeyPair(): KeyPair = {
    val random = new SecureRandom()
    val prvKeyParameters = new Ed25519PrivateKeyParameters(random)
    val prvKeyBytes = prvKeyParameters.getEncoded
    val pubKeyBytes = prvKeyParameters.generatePublicKey().getEncoded
    KeyPair(PublicKey(pubKeyBytes), PrivateKey(prvKeyBytes))
  }


}

object Ed25519AccountKeys extends AccountKeys {

  private val spec: EdDSAParameterSpec =
    EdDSANamedCurveTable.getByName(EdDSANamedCurveTable.ED_25519)

  lazy val keysFactory: AccountKeysFactory = Ed25519AccountKeysFactory

  override val KeyLength: Int = Ed25519.PUBLIC_KEY_SIZE
  override val SigLength: Int = Ed25519.SIGNATURE_SIZE

  override def sign(privateKey: PrivateKey, message: MessageToSign): Signature = {
    val signature: java.security.Signature = new EdDSAEngine(MessageDigest.getInstance(spec.getHashAlgorithm))
    signature.initSign(new EdDSAPrivateKey(new EdDSAPrivateKeySpec(privateKey, spec)))
    signature.setParameter(EdDSAEngine.ONE_SHOT_MODE)
    signature.update(message)
    Signature(signature.sign())
  }


  override def verify(publicKey: PublicKey, message: MessageToSign, sig: Signature): Boolean = {
    val sgr: java.security.Signature = new EdDSAEngine(MessageDigest.getInstance(spec.getHashAlgorithm))
    val pubKey = new EdDSAPublicKeySpec(publicKey, spec)
    val vKey: java.security.PublicKey = new EdDSAPublicKey(pubKey)
    sgr.initVerify(vKey)
    sgr.setParameter(EdDSAEngine.ONE_SHOT_MODE)
    sgr.update(message)
    sgr.verify(sig)
  }

  override val keysType: String = EdDSANamedCurveTable.ED_25519

  override def createSharedSecret(aPrivateKey: PrivateKey, aPublicKey: PublicKey): SharedSecret = {
    val xpub = Ed25519KeysToX25519Keys.toX25519PublicKey(aPublicKey)
    val xpriv = Ed25519KeysToX25519Keys.toX25519PrivateKey(aPrivateKey)
    Curve25519.createSharedSecret(PrivateKey(xpriv), PublicKey(xpub))
  }
}