package sss.openstar.crypto.keypairs

import sss.openstar.account.{AccountKeys, AccountKeysFactory}
import sss.openstar.crypto.ECEncryption.CreateNewLocalKeySign

/**
 * This builder is here because it can be, if in the future it requires coupling with other interfaces
 * then it should move to the lowest level (which will be higher than here)
 */
trait DefaultAccountKeysFactoryBuilder {
  implicit def defaultAccountKeysFactory: AccountKeysFactory = Curve25519AccountKeysFactory
  implicit def defaultAccountKeys: AccountKeys = defaultAccountKeysFactory.accountKeysImp

  implicit def newDefaultLocalKeySignerCreator: CreateNewLocalKeySign = () => defaultAccountKeysFactory.keySigner()

}

