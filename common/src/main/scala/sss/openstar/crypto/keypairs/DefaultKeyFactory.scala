package sss.openstar.crypto.keypairs

import sss.openstar.account.KeyFactory
import sss.openstar.account.NodeSigner.KeyType

trait DefaultKeyFactory extends KeyFactory {

  override val lookupMatcher =
    Ed25519AccountKeys.keyTypeMatcher
      .orElse(
        Curve25519AccountKeys.keyTypeMatcher
      ).orElse(
        ECDSA256AccountKeys.keyTypeMatcher
      )

  def supportedKeyTypes: Seq[KeyType] = Seq(
    Ed25519AccountKeys.keysType,
    Curve25519AccountKeys.keysType,
    ECDSA256AccountKeys.keysType
  )

}

object DefaultKeyFactory extends DefaultKeyFactory