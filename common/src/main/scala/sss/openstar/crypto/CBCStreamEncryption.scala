package sss.openstar.crypto

import java.io.InputStream

import javax.crypto.spec.IvParameterSpec

/*
TODO See the comment about using java CipherStream? Should switch to Bouncycastle?
https://crypto.stackexchange.com/questions/20333/encryption-of-big-files-in-java-with-aes-gcm
 */
import javax.crypto.{Cipher, CipherInputStream, SecretKey}
import sss.openstar.crypto.AESDetails._
import sss.openstar.crypto.CBCEncryption._

object CBCStreamEncryption {


  private case class CipherAndKey(cipher: Cipher, key: SecretKey)

  case class EncryptedStream(encrypted: InputStream, key: AESEncodedKey, iv: InitVector)


  private def createInitialisedAESDecryptCipher(key: SecretKey, iv: InitVector): Cipher = {
    val cipher = makeCipher
    cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv.bytes))
    cipher
  }

  private def createInitialisedAESEncryptCipher(): CipherAndKey = {

    val key = AESDetails.generateAESKey()
    createInitialisedAESEncryptCipher(key.toSecretKey)
  }


  private def createInitialisedAESEncryptCipher(key: SecretKey): CipherAndKey = {

    val cipher = makeCipher
    cipher.init(Cipher.ENCRYPT_MODE, key)

    CipherAndKey(cipher, key)
  }

  def decrypt(streamToBeDecrypted: InputStream,
              key: AESEncodedKey,
              iv: InitVector): InputStream = {

    val cipher = createInitialisedAESDecryptCipher(key.toSecretKey, iv)
    new CipherInputStream(streamToBeDecrypted, cipher)
  }

  def encrypt(streamToBeEncrypted: InputStream,
              key: AESEncodedKey
             ): EncryptedStream = {
    val aesKey = key.toSecretKey
    val CipherAndKey(cipher, _) = createInitialisedAESEncryptCipher(aesKey)

    EncryptedStream(
      new CipherInputStream(streamToBeEncrypted, cipher),
      AESEncodedKey(aesKey.getEncoded),
      initVector(cipher.getIV)
    )
  }

  def encrypt(streamToBeEncrypted: InputStream,
             ): EncryptedStream = {

    val CipherAndKey(cipher, key) = createInitialisedAESEncryptCipher()

    EncryptedStream(
      new CipherInputStream(streamToBeEncrypted, cipher),
      AESEncodedKey(key.getEncoded),
      initVector(cipher.getIV)
    )
  }

}
