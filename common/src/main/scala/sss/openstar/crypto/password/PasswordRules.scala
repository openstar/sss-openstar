package sss.openstar.crypto.password

import java.util.regex.Pattern

object PasswordRules {

  /**
   * It contains at least 8 characters and at most 20 characters.
   * It contains at least one digit.
   * It contains at least one upper case alphabet.
   * It contains at least one lower case alphabet.
   * It contains at least one special character which includes !@#$%^&+=
   * It doesn’t contain any white space.
    */
  val regex =  "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&+=])(?=\\S+$).{8,20}$"

  val pattern = Pattern.compile(regex)

  val descriptiveMessage: String = "password must be 8-20 characters, contain digits, no whitespace, upper, lower, and one of !@#$%^&+="

  def isValidPassword(password: String): Boolean =
    pattern.matcher(password).matches()
}
