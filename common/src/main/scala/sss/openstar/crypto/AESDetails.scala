package sss.openstar.crypto

import java.security.MessageDigest
import java.util

import javax.crypto.spec.SecretKeySpec
import javax.crypto.{Cipher, KeyGenerator}

object AESDetails {

  /**
   * https://stackoverflow.com/a/20770158
   * PKCS5 seems to be a synonym for PKCS7 internally
   *
   * Arguably a 128 AES bit key is a better tested algo
   *
   */
  private val transformation = "AES/CBC/PKCS5Padding"
  private val algo = "AES"
  private val keySizeInBits = 256
  private val keySizeInBytes = 32


  private lazy val keyGenerator = {
    val keyGenerator = KeyGenerator.getInstance(algo)
    keyGenerator.init(keySizeInBits)
    keyGenerator
  }

  def makeCipher: Cipher = Cipher.getInstance(transformation)

  case class AESEncodedKey(value: Array[Byte]) {
    require(value.length == keySizeInBytes, s"AES Key size should be $keySizeInBytes bytes")

    private[crypto] def toSecretKey: SecretKeySpec = {
      new SecretKeySpec(value, algo)
    }

    override def equals(o: Any): Boolean = o match {
      case that: AESEncodedKey => that.value sameElements value
      case _ => false
    }

    override def hashCode(): Int = util.Arrays.hashCode(value)
  }


  def generateAESKey(): AESEncodedKey =
    AESEncodedKey(keyGenerator.generateKey.getEncoded)


  /**
   * Given some unknown byte array create structured AESKey.
   * This is a one way function, the param cannot be recovered from the key
   *
   * @param unstructuredByteArray must be size greater than 0
   * @return
   */
  def aesKeyFromByteArray(unstructuredByteArray: Array[Byte]): AESEncodedKey = {
    require(unstructuredByteArray.nonEmpty, "Cannot use an empty array to generate an AES  key")
    val sha: MessageDigest = MessageDigest.getInstance("SHA-256")
    val keyBytesDigested = sha.digest(unstructuredByteArray)
    AESEncodedKey(keyBytesDigested)
  }

}
