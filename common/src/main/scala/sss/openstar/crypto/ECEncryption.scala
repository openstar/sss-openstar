package sss.openstar.crypto

import scorex.crypto.signatures.SharedSecret
import sss.ancillary.ByteArrayEncodedStrOps._
import sss.openstar.account.{Signer, TypedPublicKey}
import sss.openstar.crypto.CBCEncryption.InitVector
import sss.openstar.crypto.serialize.ECEncryptedSerializer
import sss.openstar.hash.Message

import java.util
import scala.concurrent.{ExecutionContext, Future}

object ECEncryption {

  type CreateSharedSecret = TypedPublicKey => SharedSecret
  type CreateSharedSecretF = TypedPublicKey => Future[SharedSecret]
  type CreateNewLocalKeySign = () => Signer


  def fromBase64Encoded(str: String): ECEncrypted = {
    val bytes = str.fromBase64Str
    ECEncryptedSerializer.fromBytes(bytes)
  }

  case class ECEncrypted(encBytes: Message,
                         publicKeyOfSender: TypedPublicKey,
                         iv: InitVector) {

    override def hashCode(): Int = {
      encBytes.hashCode() +
        publicKeyOfSender.hashCode() +
        iv.hashCode()
    }

    override def equals(o: Any): Boolean = o match {
      case that: ECEncrypted =>
        util.Arrays.equals(that.iv.bytes, iv.bytes) &&
          util.Arrays.equals(that.publicKeyOfSender.publicKey, publicKeyOfSender.publicKey) &&
          util.Arrays.equals(that.encBytes, encBytes)

      case _ => false
    }

    lazy val asBytes = ECEncryptedSerializer.toBytes(this)
    lazy val encodedAsBase64String: String = asBytes.toBase64Str

  }

  def decrypt(enc: ECEncrypted, createShared: CreateSharedSecretF)(implicit ec: ExecutionContext): Future[Array[Byte]] = {
    createShared(enc.publicKeyOfSender) map { sharedSec =>
      val aesKey = AESDetails.aesKeyFromByteArray(sharedSec)
      CBCEncryption.decrypt(aesKey, enc.encBytes, enc.iv)
    }
  }

  def encrypt(publicKeyOfRecipient: TypedPublicKey, toEncrypt: Message)(implicit createNewLocalKeySign: CreateNewLocalKeySign): ECEncrypted =
    encrypt(publicKeyOfRecipient, toEncrypt, createNewLocalKeySign())

  def encrypt(
               publicKeyOfRecipient: TypedPublicKey,
               toEncrypt: Message,
               localKeySign: Signer): ECEncrypted = {
    require(localKeySign.keyType == publicKeyOfRecipient.keyType, s"${localKeySign.keyType} == ${publicKeyOfRecipient.keyType}")

    val sharedSec = localKeySign.createSharedSecret(publicKeyOfRecipient.publicKey)
    val aesKey = AESDetails.aesKeyFromByteArray(sharedSec)
    val iv = CBCEncryption.newInitVector(SeedBytes)
    val encBytes = CBCEncryption.encrypt(aesKey, toEncrypt, iv)
    ECEncrypted(encBytes, localKeySign.typedPublicKey, iv)
  }

}
