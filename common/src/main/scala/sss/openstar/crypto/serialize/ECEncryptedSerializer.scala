package sss.openstar.crypto.serialize

import scorex.crypto.signatures.PublicKey
import sss.ancillary.Serialize._
import sss.openstar.account.NodeSigner.KeyType
import sss.openstar.account.TypedPublicKey
import sss.openstar.crypto.CBCEncryption
import sss.openstar.crypto.ECEncryption.ECEncrypted

object ECEncryptedSerializer extends Serializer[ECEncrypted] {

  override def toBytes(t: ECEncrypted): Array[Byte] = {
    ByteArraySerializer(t.encBytes) ++
      ByteArraySerializer(t.publicKeyOfSender.publicKey) ++
      StringSerializer(t.publicKeyOfSender.keyType) ++
      ByteArraySerializer(t.iv.bytes)
        .toBytes
  }

  override def fromBytes(b: Array[Byte]): ECEncrypted = {
    val (enc, pKey, kType, iv) = b.extract(
      ByteArrayDeSerialize,
      ByteArrayDeSerialize(PublicKey(_)),
      StringDeSerialize,
      ByteArrayDeSerialize(CBCEncryption.initVector)
    )
    ECEncrypted(enc, TypedPublicKey(pKey, kType), iv)
  }
}
