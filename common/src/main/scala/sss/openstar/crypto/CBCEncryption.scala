package sss.openstar.crypto

import java.util
import java.util.Base64

import javax.crypto.{Cipher, SecretKey}
import javax.crypto.spec.{IvParameterSpec, SecretKeySpec}
import sss.openstar.crypto.AESDetails._

/**
 * Created by alan on 6/3/16.
 */
object CBCEncryption {

  private val ivSizeInBytes = 16

  trait InitVector {
    val bytes: Array[Byte]
    val asString: String
  }

  def initVector(ivAsString: String): InitVector =
    initVector(Base64.getDecoder.decode(ivAsString))

  def initVector(initVectorBytes: Array[Byte]): InitVector = new InitVector {

    require(initVectorBytes.size == ivSizeInBytes, s"Init Vector should be $ivSizeInBytes bytes long")

    override val bytes = initVectorBytes
    override lazy val asString: String = Base64.getEncoder.encodeToString(bytes)

    override def equals(o: Any): Boolean = o match {
      case iv: InitVector =>
        util.Arrays.equals(iv.bytes, bytes)
      case _ => false
    }

    override def hashCode(): Int = util.Arrays.hashCode(initVectorBytes)
  }

  def newInitVector(seedBytes: SeedBytes): InitVector = initVector(seedBytes.strongSeed(ivSizeInBytes))

  def encrypt(key: AESEncodedKey, value: Array[Byte], iv: InitVector): Array[Byte] = {
    val cipher = makeCipher
    cipher.init(Cipher.ENCRYPT_MODE, key.toSecretKey, new IvParameterSpec(iv.bytes))
    Base64.getEncoder.encode(cipher.doFinal(value))
  }


  def decrypt(key: AESEncodedKey, encryptedValue: Array[Byte], iv: InitVector): Array[Byte] = {
    val cipher = makeCipher
    cipher.init(Cipher.DECRYPT_MODE, key.toSecretKey, new IvParameterSpec(iv.bytes))
    cipher.doFinal(Base64.getDecoder.decode(encryptedValue))
  }


}
