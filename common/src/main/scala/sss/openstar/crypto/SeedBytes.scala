package sss.openstar.crypto

import java.security.SecureRandom

import sss.ancillary.Logging

import scala.util.Random

/**
  Make this the indirection to getting 'random' bytes
  */
trait SeedBytes extends Logging {

  /*
  https://tersesystems.com/blog/2015/12/17/the-right-way-to-use-securerandom/
   */
  private lazy val secureRandom = {
    val rand = new SecureRandom()
    log.info(s"Init secure random, algo: ${rand.getAlgorithm} name: ${rand.getProvider.getName} info: ${rand.getProvider.getInfo}")
    rand
  }
  private lazy val strongSecureRandom = {
    val rand = SecureRandom.getInstanceStrong
    log.info(s"Init strong secure random, algo: ${rand.getAlgorithm} name: ${rand.getProvider.getName} info: ${rand.getProvider.getInfo}")
    rand
  }

  private lazy val random = new Random()

  private def getBytes(num: Int,r :Random) = {
    val bytes = new Array[Byte](num)
    r.nextBytes(bytes)
    bytes
  }

  /**
    * Uses SecureRandom.getInstanceStrong in production
    * @param num
    * @return
    */
  def strongSeed(num: Int) = {
    log.info("Calling SecureRandom strong secure, this may well block waiting for entropy...")
    getBytes(num, strongSecureRandom)
  }

  /**
    * uses a SecureRandom instance
    * @param num
    * @return
    */
  def secureSeed(num: Int) = getBytes(num, secureRandom)

  /**
    * Uses Random() to generate
    *
    * @param num
    * @return
    */
  def randomSeed(num: Int) = getBytes(num, random)
}

object SeedBytes extends SeedBytes

