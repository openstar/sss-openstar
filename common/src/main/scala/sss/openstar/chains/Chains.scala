package sss.openstar.chains


import sss.openstar.UniqueNodeIdentifier
import sss.openstar.chains.Chains.{Chain, GlobalChainIdMask}
import sss.openstar.hash.Digest32
import sss.openstar.ledger.LedgerCheckpoints.Checkpoint
import sss.openstar.ledger.Ledgers

import scala.concurrent.Future
import scala.util.Try


object Chains {

  type GlobalChainIdMask = Byte
  type Checkpointer = Long => Future[Try[Digest32]]

  trait GlobalChainIdBuilder {
    implicit lazy val globalChainId: GlobalChainIdMask = 1.toByte
  }

  trait Chain {
    implicit val id: GlobalChainIdMask
    implicit val ledgers: Ledgers
    def quorumCandidates(): Set[UniqueNodeIdentifier]
    val quorumChangePolicyType: Int

    def importState(height: Long): Try[Unit]
    def mostRecentCheckpoint(): Option[Checkpoint]
    def deleteRecentBlocks(num: Int): Unit
    def checkpoint(height: Long): Future[Try[Digest32]]
  }

}

class Chains(chains: Seq[Chain]) {

  //Note the order of val initialisation is important.
  val ordered: Seq[Chain] = {
    chains.sortWith(_.id < _.id)
  }

  val byId: Map[GlobalChainIdMask, Chain] = {
    (ordered map (l => l.id -> l))
      .toMap
  }

  require(byId.size == ordered.size, "Check the chains parameter for duplicate ids...")

  def apply(): Seq[Chain] = ordered
  def apply(id: GlobalChainIdMask) = byId(id)
  def get(id: GlobalChainIdMask)= byId.get(id)
}
