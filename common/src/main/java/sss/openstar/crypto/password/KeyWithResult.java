package sss.openstar.crypto.password;

import javax.crypto.SecretKey;

public class KeyWithResult<T> {

    private final T result;
    private final SecretKey secretKey;

    public KeyWithResult(SecretKey key, T result) {
        this.result = result;
        this.secretKey = key;
    }

    public SecretKey getSecretKey() {
        return secretKey;
    }

    public T getResult() {
        return result;
    }
}
