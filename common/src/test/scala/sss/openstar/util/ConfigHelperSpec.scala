package sss.openstar.util

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.ancillary.Configure

/**
  * Created by alan on 2/11/16.
  */
class ConfigHelperSpec extends AnyFlatSpec with Matchers with Configure {


  "ConfigHelper" should "parse test config to List of List of strings" in {
    val result = ConfigHelper.toListOfStringLists(config, "testStringLists")

    result shouldBe List(
      Nil,
      List("a"),
      List("a", "b"),
      List("a", "b", "c"),
    )

  }


}
