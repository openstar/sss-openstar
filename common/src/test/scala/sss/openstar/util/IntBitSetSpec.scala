package sss.openstar.util

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

/**
  * Created by alan on 2/11/16.
  */
class IntBitSetSpec extends AnyFlatSpec with Matchers {


  "IntBitSet " should " behave as expected " in {
    assert(!IntBitSet(2).contains(1), "should not overlap ")
    assert(!IntBitSet(4).contains(2), "should not overlap ")
    assert(IntBitSet(7).contains(6), "should overlap ")
    assert(!IntBitSet(6).contains(7), "should overlap ")
    assert(IntBitSet(7).contains(5), "should overlap ")
  }

  it should "unset correctly" in {
    assert(IntBitSet(0).set(4).value == 16, "should not overlap ")
    assert(IntBitSet(0).set(4).unSet(4).value == 0, "should have unset correctly")
  }


}
