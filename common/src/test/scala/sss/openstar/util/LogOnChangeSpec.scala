package sss.openstar.util

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

/**
  * Created by alan on 2/11/16.
  */
class LogOnChangeSpec extends AnyFlatSpec with Matchers {

  "LogOnChange" should "log on change" in {

    var results: List[Set[String]] = List.empty

    val logOnChange = new LogOnChange[Set[String]] (
       setStr => {
         results = results :+ setStr
       }
    )
    val s = Set("a")
    val s2 = Set("a")
    val s3 = Set("a", "b")
    logOnChange(s) // log
    logOnChange(s) // no log
    logOnChange(s2) // no log
    logOnChange(s3) // log
    logOnChange(s3) // no log
    assert(results == List(s, s3))

  }


  "LogOnChange" should "use given comparator" in {

    var results: List[Set[String]] = List.empty

    val logOnChange = new LogOnChange[Set[String]] (
      setStr => results = results :+ setStr,
      20,
      (s1, s2) => s1 == s2
    )

    val s = Set("a")
    val s2 = Set("a")
    val s3 = Set("a", "b")
    logOnChange(s) // log
    logOnChange(s) // no log
    logOnChange(s2) // no log
    logOnChange(s3) // log
    logOnChange(s3) // no log
    assert(results == List(s, s3))

  }

  "LogOnChange" should "use given bad comparator" in {

    var results: List[Set[String]] = List.empty

    val logOnChange = new LogOnChange[Set[String]] (
      setStr => results = results :+ setStr,
      50,
      (_, _) => false
    )

    val s = Set("a")
    val s2 = Set("a")
    val s3 = Set("a", "b")
    logOnChange(s) // log
    logOnChange(s) // no log
    logOnChange(s2) // no log
    logOnChange(s3) // log
    logOnChange(s3) // no log
    assert(results == List(s, s, s2, s3, s3))

  }


  "LogOnChange" should "log anyway according to the param" in {

    var results: List[Set[String]] = List.empty

    val logOnChange = new LogOnChange[Set[String]] (
      setStr => results = results :+ setStr,
      1
    )

    val s = Set("a")
    val s2 = Set("a")
    val s3 = Set("a", "b")
    logOnChange(s) // log
    logOnChange(s) // no log
    logOnChange(s2) // log
    logOnChange(s2) // no log
    logOnChange(s2) // log
    logOnChange(s3) // log
    logOnChange(s3) // no log
    logOnChange(s3) // log
    assert(results == List(s, s2, s2, s3, s3))

  }
}
