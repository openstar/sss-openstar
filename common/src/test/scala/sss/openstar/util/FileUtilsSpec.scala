package sss.openstar.util

import FileUtils.deleteAll
import org.scalatest.{BeforeAndAfterAll, Suite}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import java.io.{File, PrintWriter}
import java.nio.file.{Files, Path, Paths}

trait FileUtilsFixture extends BeforeAndAfterAll {

  self: Suite =>

  val workingFolder: File = new File("./working")


  def clearWorkingFolder(): Unit = {
    if(workingFolder.isDirectory) {
      deleteAll(workingFolder.toPath)
      workingFolder.delete()
    }
  }

  def createFolder(name: Path): Path = {
    Files.createDirectories(name)
  }

  def createFolderWithFiles(nameFolder: Path, numFiles: Int = 10): Path = {
    val folder = createFolder(nameFolder)
    (0 to numFiles) foreach { i =>
      createFile(folder, nameFolder.toFile.getName + i, "Some")
    }
    folder
  }

  def createFile(folderName: Path, name: String, content: String): File = {

    val file = folderName.resolve(name).toFile
    val out = new PrintWriter(file, "UTF-8")
    try {
      out.print(content)
    }
    finally {
      out.close()
    }
    file
  }

}
class FileUtilsSpec extends AnyFlatSpec with Matchers with FileUtilsFixture {

  override def beforeAll(): Unit = {
    super.beforeAll()
    clearWorkingFolder()
  }

  override def afterAll(): Unit = {
    super.afterAll()
    clearWorkingFolder()
  }

  "FileUtils" should "copy folder from existing folder to non existing" in {

    val destName = "testcopydest"

    val source = workingFolder.toPath.resolve("testcopy")
    val dest = workingFolder.toPath.resolve(destName)

    assert(!dest.toFile.isDirectory, "Is already directory")
    createFolderWithFiles(source, 10)

    FileUtils.copyFolder(source, dest)

    assert(dest.toFile.isDirectory, "Should be directory")
    assert(dest.toFile.list().nonEmpty, "Should be non empty directory")
    FileUtils.deleteAll(dest)
    assert(!dest.toFile.exists(), "Should be gone")
  }

  it should "delete folder contents" in {

    val source = workingFolder.toPath.resolve("testcopy2")

    assert(!source.toFile.isDirectory, "Is already directory")
    createFolderWithFiles(source, 10)
    assert(source.toFile.isDirectory, "Should be directory")
    FileUtils.deleteContents(source)
    assert(source.toFile.isDirectory, "Should be directory")
    assert(source.toFile.list().isEmpty, "Should be empty directory")

  }
}

