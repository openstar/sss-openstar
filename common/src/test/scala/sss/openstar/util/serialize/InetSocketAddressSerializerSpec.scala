package sss.openstar.util.serialize

import java.net.{InetAddress, InetSocketAddress}

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class InetSocketAddressSerializerSpec extends AnyFlatSpec with Matchers {

  "An InetAddressSocketSerializer " should " be able to serialize and deserialize " in {

    val testList = InetAddress.getAllByName("www.google.at")
    val socketTestList = testList.indices map { i =>
      new InetSocketAddress(testList(i), 1000 + i)
    }

    val asBytes = socketTestList.map (InetSocketAddressSerializer.toBytes)
    val deserialized = asBytes map (InetSocketAddressSerializer.fromBytes)

    deserialized foreach (println)
    socketTestList foreach (println)


    val allTrue = deserialized zip socketTestList map { zipped =>
      zipped._1.getPort === zipped._2.getPort &&
        zipped._1.getAddress === zipped._2.getAddress
    }

    assert(!allTrue.contains(false))
  }

}
