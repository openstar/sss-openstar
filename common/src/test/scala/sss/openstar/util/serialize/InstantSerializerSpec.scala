package sss.openstar.util.serialize

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import java.net.InetAddress
import java.time.Instant

class InstantSerializerSpec extends AnyFlatSpec with Matchers {

  "An InstantSerializer " should "be able to serialize and deserialize " in {

    val now = Instant.now()

    val asBytes = InstantSerializer.toBytes(now)
    val deserialized = InstantSerializer.fromBytes(asBytes)

    assert(now == deserialized)
  }

}
