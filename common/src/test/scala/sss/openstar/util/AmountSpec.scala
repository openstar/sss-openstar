package sss.openstar.util

import org.scalatest.matchers.should.Matchers
import sss.openstar.Currency
import org.scalatest.flatspec.AnyFlatSpec
import sss.openstar.util.Amount.Ops.AmountOps

import scala.util.{Failure, Success, Try}

/**
  * Created by alan on 2/11/16.
  */
class AmountSpec extends AnyFlatSpec with Matchers {


  trait StarzCurr extends Currency
  trait OtherCurr extends Currency

  import NotStarz.otherCurrBuilder
  import Starz.starzBuilder


  object Starz {

    implicit val starzBuilder = new AmountBuilder[StarzCurr] {
      override def apply(value: Long): Amount = Starz(value)
    }
  }


  object NotStarz {
    implicit val otherCurrBuilder = new AmountBuilder[OtherCurr] {
      override def apply(value: Long): Amount = NotStarz(value)

    }
  }

  case class NotStarz(value: Long) extends Amount {
    type C = OtherCurr

    override def newAmount(value: Long): Amount = NotStarz(value)
  }


  case class Starz(value: Long) extends Amount {
    type C = StarzCurr
    override def newAmount(value: Long): Amount = Starz(value)
  }


  "Amount" should "be created" in {


    val starz = Starz(4)
    starz shouldBe Amount[StarzCurr](4)
    Amount[starz.C](4) shouldBe starz

  }

  it should "add correctly" in {
    Starz(4) + 4 shouldBe Success(Starz(8))
  }

  it should "add correctly to successful add" in {
    val okCurrency = Starz(10)
    val secondAdd = okCurrency + 200
    secondAdd should matchPattern { case Success(a : Amount) if a.value == 210 =>  }
  }

  it should "prevent different currencies adding" in {

    val val1 = NotStarz(4)
    val val2 = Starz(4)
    val1 + val2 should matchPattern { case Failure(_) => }
  }


  it should "prevent overflow adding" in {
    val val1 = Starz(Long.MaxValue)
    val val2 = 1
    val1 + val2 should matchPattern { case Failure(_: ArithmeticException) => }
  }

  it should "allow subtracting a long" in {
    val val1 = Starz(1)
    val val2 = 1
    val1 - val2 shouldBe Success(Starz(0))
  }

  it should "allow subtracting an Amount" in {
    val val1 = Starz(1)
    val val2 = Starz(1)
    val1 - val2 shouldBe Success(Starz(0))
  }

  it should "prevent overflow subtracting" in {
    val val1 = Starz(Long.MinValue)
    val val2 = 1
    val1 - val2 should matchPattern { case Failure(_: ArithmeticException) => }
  }

  it should "allow same currencies adding" in {
    val val1 = Starz(10)
    val val2 = Starz(8)

    val1 + val2 should matchPattern { case Success(a: Amount) if a.value == 18 => }
  }

  it should "allow add try amounts" in {
    import Amount.Ops.TryAmountOps
    val val1 = Try(Starz(10))
    val val2 = Try(Starz(8))

    val1 + val2 should matchPattern { case Success(a: Amount) if a.value == 18 => }
  }

  it should "allow add subtract amounts" in {
    import Amount.Ops.TryAmountOps
    val val1 = Try(Starz(10))
    val val2 = Try(Starz(8))

    val1 - val2 should matchPattern { case Success(a: Amount) if a.value == 2 => }
  }

  it should "allow subtract amounts from try" in {
    import Amount.Ops.TryAmountOps
    val val1 = Try(Starz(10))
    val val2 = Starz(8)

    val1 - val2 should matchPattern { case Success(a: Amount) if a.value == 2 => }
  }

  it should "allow add try with long amounts" in {
    import Amount.Ops.TryAmountOps
    val val1 = Try(Starz(10))
    val val2 = 8

    val1 + val2 should matchPattern { case Success(a: Amount) if a.value == 18 => }
  }

  it should "allow * and fail if too big" in {

    val val1 = Starz(Long.MaxValue)
    val val2 = 2
    val val3 = Starz(Int.MaxValue)

    val3 * val2 shouldBe Success(Starz(2 * (Int.MaxValue.toLong)))
    val1 * val2 should matchPattern { case Failure(a: ArithmeticException) => }
  }
}
