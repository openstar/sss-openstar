package sss.openstar.db

import org.scalatest.{BeforeAndAfterAll, Suite}
import sss.db.Db
import sss.db.datasource.DataSource
import sss.db.datasource.DataSource.CloseableDataSource
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.db.Builder.RequireDb
import sss.openstar.schemamigration.RequireDatasource

trait DbFixture extends BeforeAndAfterAll with RequireDatasource with RequireDb {
  self: Suite =>

  lazy implicit val dataSource: CloseableDataSource = DataSource()
  lazy implicit val db: Db                          = Db(ds = dataSource)

  override def afterAll(): Unit = {
    try super.afterAll()
    finally db.shutdown.dbRunSync
  }
}
