package sss.openstar.db

import org.scalatest.{BeforeAndAfterAll, Suite}
import sss.db.Db
import sss.db.datasource.DataSource
import sss.db.datasource.DataSource.CloseableDataSource
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.db.Builder.RequireDb
import sss.openstar.schemamigration.{RemoteSchemaMigrationBuilder, RequireDatasource, RequireRemoteDatasource}

trait DbRemoteFixture extends BeforeAndAfterAll
  with RequireRemoteDatasource
  with RemoteSchemaMigrationBuilder {

  self: Suite =>

  val remoteDbConfigPrefix = "remote.database"

  lazy implicit val remoteDataSource: CloseableDataSource = DataSource(remoteDbConfigPrefix + ".datasource")

  implicit lazy val remoteDb: Db                          = Db(
    dbConfigName = remoteDbConfigPrefix,
    ds = remoteDataSource
  )


  override def beforeAll(): Unit = {
    super.beforeAll()
    migrateRemoteSqlSchema()
  }

  override def afterAll(): Unit = {
    try super.afterAll()
    finally remoteDb.shutdown.dbRunSync
  }
}
