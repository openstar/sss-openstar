package sss.openstar.merkle

import org.scalatest.BeforeAndAfterAll
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.ancillary.Logging
import sss.db._
import sss.db.datasource.DataSource
import sss.openstar.schemamigration.dynamic.MerkleMigration

import scala.collection.immutable.ArraySeq
import java.util.Date

/**
  * Created by alan on 2/15/16.
  */
class MerklePersisterTest extends AnyFlatSpec with Matchers with Logging with BeforeAndAfterAll  {


  //type MerkleTreeType = Array[Byte]
  implicit val db: Db = Db()
  implicit val datasource = DataSource()

  val tableCount = 100
  val tableNamePrefix = "merkle"

  override protected def beforeAll(): Unit = {
    MerkleMigration.migrateV1(tableCount, datasource.getConnection)
  }

  private def getTableCount(tableNamePrefix: String): Long = {
    import sss.db.ops.DbOps._
    db.select(s"SELECT COUNT(TABLE_NAME) TABLE_COUNT FROM INFORMATION_SCHEMA.SYSTEM_TABLES ")
      .getRow(where(s"REGEXP_MATCHES(TABLE_NAME,'^${tableNamePrefix.toUpperCase}_[0-9].*')"))
      .map(_.flatMap(_.longOpt("TABLE_COUNT")))
      .map(_.getOrElse(0L))
      .dbRunSyncGet
  }

  import MerklePersister._
  val txIds: IndexedSeq[ArraySeq[Byte]] = Seq(ArraySeq(1.toByte,2.toByte,3.toByte)).toIndexedSeq

  lazy val mt = MerkleTree(txIds)
  lazy val root = mt.root

  "MerkleMigration" should "able to create required number of tables" in {

    getTableCount(tableNamePrefix) should equal(tableCount)
  }

  "Merkle trees " should " be writable to disk " in {
    MerklePersister(_ => 2).persist(2, mt)
  }


  /*"A merkle path " should " be retrievable from disk " in {

    val start = new Date()

    //val graph = MerkleTest.analyse(1, mt, Map())

    /*val keys = graph.keys.toIndexedSeq.sorted
    keys.foreach { k =>
      val all = graph(k)
      println(s"Level $k, VALUES: $all")
    }*/

    println(s"${mt.leafs.keys.size} should b ${mt.leafs.keySet.size} $countReused, ${usedHashes.size}")

    var count = 0
    var success = 0
    log.info(s"Root len ${mt.root.length}")

    @tailrec
    def printPath(leaf: MerkleTreeType, mTree: MerkleTree[MerkleTreeType], p: Seq[MerkleTreeType]): Unit = {
      val hash = hashBS(leaf, p.head)
      println(s"ROOT ${mTree.root} PATH ${p.head}, Hash $hash")
      if (mTree.parent.isDefined) printPath(hash, mTree.parent.get, p.tail)
    }

    val results = leafs map { leaf =>

      count += 1
      //val p = MerklePersist.path("test1", leaf.array).get
      val p = mt.path(leaf)


      //println(s"Count $count Leaf path len: ${p.size}")
      //val ary = p.map( bs => ByteString(bs))
      val good = MerkleTree.verify[MerkleTreeType](leaf, p, root) //, "Path Not found")

      //println(s"IsGood? $good")
      val leafTree = mt.leafs(leaf)
      //printPath(leaf, leafTree, p)


      if (good) success += 1

      val pp = p.mkString(":")
      s"$leaf |$good "
    }

    //TODO Fix verify, a small precentage of verifyes will fail if a large leaf count is used. 99999
    println(s"Count $count, Success $success TODO MAKE Verify ASSERT, FIX")

    //results foreach (l => log.info(l))

    val dur = new Date().getTime - start.getTime
    println(s"${leafs.size} took $dur ms => ${dur / leafs.size} ms / retrieve and verify")
  }*/

}
