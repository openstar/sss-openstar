package sss.openstar.ledger

import org.scalatest.BeforeAndAfterAll
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.openstar.DummySeedBytes
import sss.openstar.db.DbFixture
import sss.openstar.hash.Digest32
import sss.openstar.ledger.LedgerCheckpoints.Checkpoint
import sss.openstar.schemamigration.SchemaMigrationBuilder
import sss.openstar.schemamigration.dynamic.GlobalTableNameTag
import sss.openstar.schemamigration.dynamic.UtxoDbStorageMigration.UtxoDbStorageId

class LedgerCheckpointsSpec extends AnyFlatSpec with Matchers with DbFixture with BeforeAndAfterAll with SchemaMigrationBuilder {

  override protected val tablePartitionsCount: Int = 10
  val randomBytes1 = DummySeedBytes.randomSeed(32)

  override def beforeAll(): Unit = {
    super.beforeAll()
    implicit val globalTableNameTag: GlobalTableNameTag = GlobalTableNameTag("1")
    migrateSqlSchema()
  }

  "A LedgerCheckpoints" should "find no checkpoint in the beginning" in {
    assert(LedgerCheckpoints.getLastCheckpoint.isEmpty)
  }

  it should "allow chkpoint to be added" in {
    LedgerCheckpoints.recordCheckpoint(45, Digest32(randomBytes1))
    assert(LedgerCheckpoints.getLastCheckpoint.isDefined)
    assert(LedgerCheckpoints.getLastCheckpoint.get === Checkpoint(45, Digest32(randomBytes1)))
  }

  it should "return the highest checkpoint" in {
    LedgerCheckpoints.recordCheckpoint(47, Digest32(DummySeedBytes.randomSeed(32)))
    LedgerCheckpoints.recordCheckpoint(46, Digest32(DummySeedBytes.randomSeed(32)))
    LedgerCheckpoints.recordCheckpoint(49, Digest32(randomBytes1))
    assert(LedgerCheckpoints.getLastCheckpoint.isDefined)
    assert(LedgerCheckpoints.getLastCheckpoint.get === Checkpoint(49, Digest32(randomBytes1)))
  }

  it should "compare checkpoints" in {
    val c = LedgerCheckpoints.getLastCheckpoint.get
    assert(LedgerCheckpoints.compareCheckpoint(c).get === true)
  }

  it should "report missing checkpoints" in {
    val c = Checkpoint(Long.MaxValue, Digest32(DummySeedBytes(32)))
    assert(LedgerCheckpoints.compareCheckpoint(c).isEmpty)
  }

  it should "remove checkpoints" in {
    val c = LedgerCheckpoints.getLastCheckpoint
    assert(LedgerCheckpoints.deleteCheckpoint(c.get.height))
    assert(!LedgerCheckpoints.deleteCheckpoint(c.get.height))
    val c2 = LedgerCheckpoints.getLastCheckpoint
    assert(c !== c2)
  }

  it should "list obsolete" in {
    val obsolete = LedgerCheckpoints.listObsoleteCheckpoints()
    assert(obsolete.size === 2)
    obsolete map { c =>
      LedgerCheckpoints.deleteCheckpoint(c.height)
    }
    val obsolete2 = LedgerCheckpoints.listObsoleteCheckpoints()
    assert(obsolete2.size === 0)

  }

  override def migrateSqlSchema(): Unit = {
    implicit val globalTableNameTag: GlobalTableNameTag = GlobalTableNameTag("1")
    migrateSqlSchema(Set.empty[UtxoDbStorageId])
  }
}
