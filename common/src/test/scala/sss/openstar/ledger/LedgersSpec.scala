package sss.openstar.ledger

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.db.FutureTx
import sss.openstar.common.block
import sss.openstar.hash

import scala.util.{Failure, Success, Try}

class LedgersSpec  extends AnyFlatSpec with Matchers {

  def dummyLedger(id: Int, initResult: Try[String]): TollLedger = new TollLedger {
    override def ledgerId: LedgerId = LedgerId(id.toByte)

    override def apply(ledgerItem: LedgerItem, blockId: block.BlockId, txSeqApplied: TxSeqApplied): LedgerResult = ???

    override def applyFTx(ledgerItem: LedgerItem, blockId: block.BlockId, txSeqApplied: TxSeqApplied): LedgerResultFTx = FutureTx.unit[LedgerResult](Right(TxNotApplied(ledgerItem.txId)(ledgerItem.ledgerId)))

    override def initialize(): Try[String] = initResult
  }

  def makeLedgers(ls: Seq[Ledger]): Ledgers = {
    new Ledgers(
      ls.map(l => (l.ledgerId.id, l)).toMap, Map.empty, dummyLedger(45, Success("OK")))
  }

  "A ledgers class" should "exercise initializer" in {
    val ledgers = makeLedgers(Seq(dummyLedger(1,Success("1"))))
    ledgers.initializeLedgers() shouldBe Seq(Success("1"), Success("OK"))
  }

  it should "exercise all initializers" in {
    val ledgers = makeLedgers(
      Seq(
        dummyLedger(1,Success("1")),
        dummyLedger(2,Success("2"))
      )
    )
    ledgers.initializeLedgers() shouldBe Seq(Success("1"), Success("2"), Success("OK"))
  }

  it should "fail on the first init failure" in {
    val failure = Failure(new RuntimeException("FAIL"))
    val ledgers = makeLedgers(
      Seq(
        dummyLedger(2,failure),
        dummyLedger(1,Success("1")),
        dummyLedger(3,Success("3"))
      )
    )
    ledgers.initializeLedgers() shouldBe Seq(Success("1"), failure)
  }

  it should "initialise the ledgers in order" in {
    val failure = Failure(new RuntimeException("FAIL"))
    val ledgers = makeLedgers(
      Seq(
        dummyLedger(11,Success("11")),
        dummyLedger(9,Success("9")),
        dummyLedger(6,Success("6")),
        dummyLedger(4,Success("4")),
        dummyLedger(12,Success("12")),
        dummyLedger(3,Success("2"))
      )
    )
    ledgers.initializeLedgers() shouldBe Seq(
      Success("2"),
      Success("4"),
      Success("6"),
      Success("9"),
      Success("11"),
      Success("12"),
      Success("OK")
    )
  }

  it should "pass txApplied results to "
}
