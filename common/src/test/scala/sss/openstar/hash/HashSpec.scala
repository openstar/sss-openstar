package sss.openstar.hash

import akka.util.ByteString
import org.scalatest.DoNotDiscover
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.ancillary.ByteArrayComparisonOps
import sss.openstar.DummySeedBytes

import scala.util.Random

/**
  * Strong randomness is just too slow.
  */
@DoNotDiscover
class HashSpec extends AnyFlatSpec with Matchers with ByteArrayComparisonOps {


  "Fast Hash" should "be deterministic" in {

    val sizeSamples = 99999
    val bytesToHashes = (0 to sizeSamples).map { i =>
      DummySeedBytes(1024)
    }.map { bytes =>
      bytes -> ByteString(FastCryptographicHash.hash(bytes))
    }

    val maxIteration = 50000
    (0 until maxIteration) foreach { iter =>
      val sample = Random.nextInt(sizeSamples)
      val (bs, hashed) = bytesToHashes(sample)
      assert(ByteString(FastCryptographicHash.hash(bs)) === hashed)
    }
  }

  "Secure Hash" should "be deterministic" in {

    val sizeSamples = 99999
    val bytesToHashes = (0 to sizeSamples).map { i =>
      DummySeedBytes(1024)
    }.map { bytes =>
      bytes -> ByteString(SecureCryptographicHash.hash(bytes))
    }

    val maxIteration = 50000
    (0 until maxIteration) foreach { iter =>
      val sample = Random.nextInt(sizeSamples)
      val (bs, hashed) = bytesToHashes(sample)
      assert(ByteString(SecureCryptographicHash.hash(bs)) === hashed)
    }
  }

  "Hash of empty Stream" should "fail" in {

    intercept[IllegalArgumentException](FastCryptographicHash(LazyList.empty, true))
    intercept[IllegalArgumentException](SecureCryptographicHash(LazyList.empty, true))
  }

  "Hash of non empty Stream with empty arrays" should "fail" in {

    val emptyStream: LazyList[Message] = LazyList.continually(Array.emptyByteArray).take(10)
    intercept[IllegalArgumentException](FastCryptographicHash(emptyStream, true))
    intercept[IllegalArgumentException](SecureCryptographicHash(emptyStream, true))
  }

  "Hash of mixed Stream" should "produce Digest32" in {

    val fiveMessages = LazyList.continually(DummySeedBytes(1024)).take(5)
    val fiveMoreMessages = LazyList.continually(DummySeedBytes(1024)).take(5)
    val emptyMessage = LazyList.continually(Array.emptyByteArray).take(1)

    val emptyInMiddle = fiveMessages ++ emptyMessage ++ fiveMoreMessages
    val notEmptyInMiddle = fiveMessages ++ fiveMoreMessages

    val a = Digest32(FastCryptographicHash(emptyInMiddle))
    val b = Digest32(SecureCryptographicHash(emptyInMiddle))
    val a1 = Digest32(FastCryptographicHash(notEmptyInMiddle))
    val b1 = Digest32(SecureCryptographicHash(notEmptyInMiddle))

    assert(a === a1, "Empty element caused issue in hash?")
    assert(b === b1, "Empty element caused issue in hash?")

  }
}


