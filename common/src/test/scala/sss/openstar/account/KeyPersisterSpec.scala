package sss.openstar.account

import org.scalatest.concurrent.ScalaFutures
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.ancillary.ByteArrayComparisonOps
import sss.ancillary.ByteArrayEncodedStrOps._
import sss.ancillary.FutureOps.AwaitResult
import sss.openstar.common.users.{ClasspathKeyPersistRead, MementoKeyPersist}
import sss.openstar.crypto.keypairs.CBCKeyEncryption

import scala.io.Source

/**
  * Created by alan on 2/11/16.
  */
class KeyPersisterSpec extends AnyFlatSpec
  with ScalaFutures
  with Matchers
  with ByteArrayComparisonOps
  with KeyPersisterFixtureWithScalaFutures
  with AccountKeysFactoryFixture
  with KeyGeneratorFixture {

  val newId = "Totallyrandomwa"
  val newTag = "tag1"
  val classPathKeyFileId = "foo"
  val classPathKeyFileTag = "defaultTag"
  val phrase = "Hell0There!"

  //override this to use the classpath lookup which
  //allows the classpath test to succeed.
  override lazy val keyPersister = new KeyPersister(
        Seq(
          new ClasspathKeyPersistRead(),
          MementoKeyPersist
        ), MementoKeyPersist, CBCKeyEncryption
  )

  "A KeyPersister" should "generate a new public and private key " in {

    val kp = keyPersister.findOrCreate(newId, newTag, phrase, keyGenerator).futureValue
    val Some((retrievedPKey, retrievedPubKey)) = keyPersister.findAndRetrieve(newId, newTag, phrase).futureValue

    assert(retrievedPKey sameElements(kp.priv), "private key is good ")
    assert(retrievedPubKey sameElements(kp.pub), "public key is good")

  }

  it should "load key from classpath if exists on classpath" in {
    val kp = keyPersister.findOrCreate(classPathKeyFileId, classPathKeyFileTag, "Password10!", keyGenerator).futureValue
    val actualKey = kp.pub.toBase64Str

    val expectedKey = Source.fromResource(s"$classPathKeyFileId.$classPathKeyFileTag").mkString.split(":::").head

    assert(actualKey == expectedKey, s"public key mismatch! Expected $expectedKey actual $actualKey")
  }

  it should "allow a key to be deleted" in {
    keyPersister.deleteKey(newId, newTag).futureValue
    keyPersister.findAndRetrieve(newId, newTag, phrase).futureValue shouldBe None

  }
}
