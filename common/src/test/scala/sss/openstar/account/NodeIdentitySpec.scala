package sss.openstar.account

import org.scalacheck.Gen
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import scorex.crypto.signatures.{MessageToSign, PublicKey, Signature}
import sss.ancillary.ByteArrayComparisonOps
import sss.openstar.account.SignerFactory.SignerDescriptor
import sss.openstar.crypto.keypairs.DefaultKeyFactory

/**
 * Created by alan on 2/11/16.
 */
class NodeIdentitySpec extends AnyFlatSpec
  with Matchers
  with ScalaFutures
  with ByteArrayComparisonOps
  with KeyPersisterFixtureWithScalaFutures
  with AccountKeysFactoryFixture {

  val interactionTypes = Seq(SignerInteractionTypes.ManualSignerInteractionType, SignerInteractionTypes.AutomaticSignerInteractionType)
  val signerTypes = Seq(SignerInteractionTypes.ManualSignerInteractionType, SignerInteractionTypes.AutomaticSignerInteractionType)
  val keyTypes = DefaultKeyFactory.supportedKeyTypes

  def signerDescriptorToVerifier(sd: SignerDescriptor): NodeVerifierSigner = new NodeVerifierSigner {

    override def signerDescriptor: SignerDescriptor = sd

    override def signerOpt: Option[NodeSigner] = ???

    override def nodeIdTag: NodeIdTag = ???

    override def verifier: Verifier = ???
  }

  val verifierGen = for {
    securityLevel <- Gen.choose(0, 10)
    interactionType <- Gen.oneOf(interactionTypes)
    signerType <- Gen.oneOf(signerTypes)
    keyType <- Gen.oneOf(keyTypes)
  } yield signerDescriptorToVerifier(
    SignerDescriptor(keyType, securityLevel, signerType, interactionType)
  )

//
//  "A list of Verifier metadata" should "homour query parameters" in {
//    forAll(Gen.listOf(verifierGen)) { seqVer =>
//      NodeIdentity.findVerifier(seqVer, Some(SignerInteractionTypes.AutomaticSignerInteractionType), 0).isDefined
//    }
//  }
  //  lazy val nodeIdentityManager = {
  //    val nodeIdentityManager = new NodeIdentityManager(keyPersister, accountKeysFactory)
  //    nodeIdentityManager.deleteKey(newId, newTag)
  //    nodeIdentityManager
  //  }

  val newId = "Totallyrandomw"
  val newTag = "tag1"
  val passPhrase = "N0tpassword$"

  //  "An node identity " should "generate a new public and private key" in {
  //
  //    val nodeIdentity = nodeIdentityManager(NodeIdTag(newId, newTag), passPhrase).futureValue
  //    assert(nodeIdentity.id == newId)
  //    assert(nodeIdentity.tag == newTag)
  //
  //  }
  //
  //  it should " be retrievable " in {
  //    val nodeIdentity = nodeIdentityManager(NodeIdTag(newId, newTag), passPhrase).futureValue
  //    assert(nodeIdentity.id == newId)
  //    assert(nodeIdentity.tag == newTag)
  //  }

}
