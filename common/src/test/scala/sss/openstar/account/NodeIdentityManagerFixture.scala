package sss.openstar.account

import scorex.crypto.signatures.{PrivateKey, PublicKey}
import sss.openstar.UniqueNodeIdentifier
import sss.openstar.account.NodeIdentityManager.{GetSignerDetails, GetSignerDetailsPF, SignerDetails}
import sss.openstar.account.SignerFactory.SignerDescriptor
import sss.openstar.common.builders.{ActorSystemBuilder, ConfigBuilder, CpuBoundExecutionContextBuilder, CpuBoundExecutionContextFixture, CpuBoundExecutionContextFromActorSystemBuilder, KeyFactoryBuilder, NodeIdTagBuilder, NodeIdTagFixture, NodeIdentityBuilder, NodeIdentityManagerBuilder, PhraseFixture, RequireConfig, RequirePhrase, SignerFactoryBuilder, VerifierFactoryBuilder}
import sss.ancillary.ByteArrayEncodedStrOps._
import sss.ancillary.FutureOps.AwaitResult
import sss.openstar.account.impl.EncryptedKeyFileAccountSignerCreator
import sss.openstar.crypto.keypairs.Curve25519AccountKeys

import scala.concurrent.Future
import scala.util.Try



trait NodeIdentityManagerFixture
  extends NodeIdentityManagerBuilder
    with NodeIdentityBuilder
    with KeyFactoryBuilder
    with AccountKeysFactoryFixture
    with KeyPersisterFixture
    with VerifierFactoryBuilder
    with SignerFactoryBuilder
    with PhraseFixture
    with NodeIdTagFixture
    with ConfigBuilder
    with CpuBoundExecutionContextFromActorSystemBuilder
    with ActorSystemBuilder {

  case class NodeIdentityTestData(nodeIdTag: NodeIdTag, keys: KeyPair) {
    def nodeId: UniqueNodeIdentifier = nodeIdTag.nodeId
    def tag: String = nodeIdTag.tag
    def publicKey: PublicKey = keys.pub
    def privateKey: PrivateKey = keys.priv
  }

  implicit val nm = nodeIdentityManager
  def createSignerDetailsPFFor(testData: NodeIdentityTestData): GetSignerDetailsPF  = {
    createSignerDetailsPFFor(testData.nodeIdTag, testData.publicKey)
  }

  def createSignerDetailsFor(pfs: GetSignerDetailsPF*): GetSignerDetails = pfs.foldLeft(
    PartialFunction.empty[UniqueNodeIdentifier, Future[Seq[SignerDetails]]]
  ) {
    case (acc, e) => acc.orElse(e)
  }

  def createSignerDetailsPFFor(nodeIdTag: NodeIdTag, publicKey: PublicKey): GetSignerDetailsPF  = {
    case nodeId if nodeIdTag.nodeId == nodeId => Future.fromTry(Try {
      val signerDescriptor = SignerDescriptor(
        Curve25519AccountKeys.keysType,
        EncryptedKeyFileAccountSignerCreator.SecurityLevel,
        EncryptedKeyFileAccountSignerCreator.SignerType,
        EncryptedKeyFileAccountSignerCreator.InteractionType
      )
      Seq(
        SignerDetails(
          nodeIdTag,
          publicKey,
          None,
          signerDescriptor
        )
      )
    })
  }

  private var testData: Map[UniqueNodeIdentifier, NodeIdentityTestData] = Map.empty
  private val lock = new Object

  def clearNodeIdentityFixture(): Unit = {
    lock.synchronized {
      testData.foreach(td => keyPersister.deleteKey(td._2.nodeId, td._2.tag).await())
      testData = Map.empty
    }
  }

  def addNodeIdentityToFixture(nodeIdTags: NodeIdTag*): Seq[NodeIdentityTestData] =
    nodeIdTags.map(nodeIdTag => addNodeIdentityToFixture(nodeIdTag.nodeId, nodeIdTag.tag))

  def addToSignerDetailsFixture(nodeId: UniqueNodeIdentifier,
                                tag: String = NodeIdTag.defaultTag): NodeIdentityTestData = {
    val ac = createPrivateAc()
    val data = NodeIdentityTestData(
      NodeIdTag(nodeId, tag),
      ac.keypair
    )
    lock.synchronized {
      testData = testData + (nodeId -> data)
    }
    data
  }

  def addNodeIdentityToFixture(nodeId: UniqueNodeIdentifier,
                               tag: String = NodeIdTag.defaultTag,
                               password: String = testPassword): NodeIdentityTestData = {

    val data = lock.synchronized {
      val data = addToSignerDetailsFixture(nodeId, tag)
      keyPersister.findOrCreate(nodeId, tag, password, () => data.keys).await()
      data
    }

    data
  }

  override def getSignerDetails: GetSignerDetails = nodeId => {
    createSignerDetailsFor(testData.values.map(createSignerDetailsPFFor).toSeq :_*)(nodeId)
  }

  def addToFixtureAndGetNodeIdentity(nodeId: UniqueNodeIdentifier, tag: String = NodeIdTag.defaultTag, password: String = testPassword): NodeIdentity = {
    addNodeIdentityToFixture(nodeId, tag, password)
    getNodeIdentity(nodeId, tag, password)
  }

  def getNodeIdentity(nodeId: UniqueNodeIdentifier, tag: String = NodeIdTag.defaultTag, password: String = testPassword): NodeIdentity = {
    nodeIdentityManager(nodeId, tag, password).await()
  }

  override lazy implicit val nodeIdentity: NodeIdentity = {
    addToFixtureAndGetNodeIdentity(nodeIdTag.nodeId, nodeIdTag.tag, testPassword)
  }

}
