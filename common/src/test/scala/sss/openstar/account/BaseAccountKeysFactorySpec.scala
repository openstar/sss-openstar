package sss.openstar.account

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import scorex.crypto.signatures.{PrivateKey, PublicKey}
import sss.ancillary.ByteArrayComparisonOps

import java.util
import scala.util.{Random, Success, Try}

/**
 *
 */
trait BaseAccountKeysFactorySpec extends AnyFlatSpec with Matchers with ByteArrayComparisonOps with AccountKeysFactoryFixture  {

  def KeyTypeInfo: String = defaultAccountKeysFactory.keysType
  import defaultAccountKeysFactory.accountKeysImp

  it should "allow access to the correct factory" in {
    assert(defaultAccountKeysFactory.keysType == defaultAccountKeysFactory.accountKeysImp.keysFactory.keysType, "keysFactory wired up incorrectly")
  }

  it should "sign and verify signatures" in {

    0 to 500 foreach { _ =>
      val kp = defaultAccountKeysFactory.createKeyPair()

      val message = Random.nextBytes(Random.between(2, 500000))
      val sig = accountKeysImp.sign(kp.priv, message)
      val result = accountKeysImp.verify(kp.pub, message, sig)

      assert(result, "Sig not verified!!!")

      val sigBadMessage = accountKeysImp.sign(kp.priv, message.tail)
      val resultBadSig = accountKeysImp.verify(kp.pub, message, sigBadMessage)

      assert(!resultBadSig, "Bad message signature verified!!!")

      val resultBadMessage = accountKeysImp.verify(kp.pub, message.tail, sig)
      assert(!resultBadMessage, "Bad message verified!!!")

    }
  }

}

trait WithSharedSecretTests {

  self: BaseAccountKeysFactorySpec =>
  import defaultAccountKeysFactory.accountKeysImp
  s"A ${KeyTypeInfo} signing key pair" should "support creating valid shared secrets (repeatedly)" in {

    0 to 1000 foreach { _ =>
      val kp = defaultAccountKeysFactory.createKeyPair()
      val kp2 = defaultAccountKeysFactory.createKeyPair()

      val shared1 = accountKeysImp.createSharedSecret(kp.priv, kp2.pub)
      val shared2 = accountKeysImp.createSharedSecret(kp2.priv, kp.pub)

      assert(util.Arrays.equals(shared1, shared2))
    }
  }

  it should "always fail if keys are wrong" in {

    0 to 500 foreach { _ =>
      val kp = defaultAccountKeysFactory.createKeyPair()
      val kp2 = defaultAccountKeysFactory.createKeyPair()

      val shared1 = accountKeysImp.createSharedSecret(kp.priv, kp.pub)
      val shared2 = accountKeysImp.createSharedSecret(kp2.priv, kp2.pub)

      assert(!util.Arrays.equals(shared1, shared2))

      val sharedReversed = Try(accountKeysImp.createSharedSecret(kp.priv, PublicKey(kp2.pub.reverse)))
      val sharedReversed2 = Try(accountKeysImp.createSharedSecret(PrivateKey(kp2.priv.reverse), kp2.pub))
      (sharedReversed, sharedReversed2) match {
        case (Success(a), Success(b)) if util.Arrays.equals(a, b) => fail("Bad keys produced common shared secrets")
        case _ =>
      }
    }
  }

}
