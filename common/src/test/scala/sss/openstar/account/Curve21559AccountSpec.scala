package sss.openstar.account

import sss.openstar.crypto.keypairs.Curve25519AccountKeysFactory


class Curve21559AccountSpec extends BaseAccountKeysFactorySpec with WithSharedSecretTests {

  lazy override implicit val defaultAccountKeysFactory: AccountKeysFactory = Curve25519AccountKeysFactory

}