package sss.openstar.account

import sss.openstar.DummySeedBytes

trait KeyGeneratorFixture  {

  self: AccountKeysFactoryFixture =>

  implicit val keyGenerator: () => KeyPair = () =>
    createPrivateAc().keypair


}
