package sss.openstar.account

import sss.openstar.crypto.keypairs.{Curve25519AccountKeysFactory, DefaultAccountKeysFactoryBuilder}


trait AccountKeysFactoryFixture extends DefaultAccountKeysFactoryBuilder {

  lazy override implicit val defaultAccountKeysFactory: AccountKeysFactory = Curve25519AccountKeysFactory

  def createPrivateAc(): Signer = defaultAccountKeysFactory.keySigner()

}
