package sss.openstar.account

import scorex.crypto.signatures.PublicKey
import sss.openstar.account.NodeIdentityManager.{GetSignerDetails, SignerDetails}
import sss.openstar.account.NodeSigner.{InteractionType, KeyType, SecurityLevel, SignerType}
import sss.openstar.account.SignerFactory.SignerDescriptor
import sss.openstar.account.impl.EncryptedKeyFileAccountSignerCreator
import sss.openstar.crypto.keypairs.DefaultKeyFactory

import scala.concurrent.Future


trait TaggedSignerVerifierFixture  {

  val signerDescriptor: SignerDescriptor = SignerDescriptor(
    DefaultKeyFactory.supportedKeyTypes.head,
     SignerSecurityLevels.SecurityLevel1,
     EncryptedKeyFileAccountSignerCreator.SignerType,
     SignerInteractionTypes.ManualSignerInteractionType
  )

  def makeSignersDetails(
                          publicKey: PublicKey, tag: String): GetSignerDetails = n => Future.successful{
    Seq(SignerDetails(NodeIdTag(n, tag), publicKey, None, signerDescriptor))
  }
}
