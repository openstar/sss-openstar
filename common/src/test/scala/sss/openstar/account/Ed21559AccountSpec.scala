package sss.openstar.account

import sss.openstar.crypto.keypairs.Ed25519AccountKeysFactory

/**
 *
 */
class Ed21559AccountSpec extends BaseAccountKeysFactorySpec with WithSharedSecretTests {

  lazy override implicit val defaultAccountKeysFactory: AccountKeysFactory = Ed25519AccountKeysFactory

}