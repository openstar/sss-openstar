package sss.openstar.account

import java.nio.charset.StandardCharsets

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import scorex.crypto.signatures.Curve25519
import sss.ancillary.ByteArrayComparisonOps
import sss.openstar.DummySeedBytes
import sss.openstar.crypto.{AESDetails, CBCEncryption}


/**
  * Created by alan on 2/11/16.
  */
class EndToEndEncryptionSpec extends AnyFlatSpec with Matchers with ByteArrayComparisonOps with AccountKeysFactoryFixture {

  lazy val pkPair1 = createPrivateAc()
  lazy val pkPair2 = createPrivateAc()

  lazy val message = "There once was a man from Nantucket, and rode her like Billy the Kid"

  "End to End encryption " should " successfully encrypt and decrypted with a public/private shared secret " in {

    val sharedSecret = Curve25519.createSharedSecret(pkPair1.keypair.priv, pkPair2.keypair.pub)
    val aesKey = AESDetails.aesKeyFromByteArray(sharedSecret)
    val iv = CBCEncryption.newInitVector(DummySeedBytes)

    val encrypted = CBCEncryption.encrypt(aesKey, message.getBytes(StandardCharsets.UTF_8), iv)

    val reverseSharedSecret = Curve25519.createSharedSecret(pkPair2.keypair.priv, pkPair1.keypair.pub)
    val reverseAesKey = AESDetails.aesKeyFromByteArray(reverseSharedSecret)
    val reconstitutedIv = CBCEncryption.initVector(iv.asString)

    assert(reverseSharedSecret.sameElements(sharedSecret))

    val dencrypted = CBCEncryption.decrypt(reverseAesKey, encrypted, reconstitutedIv)

    assert(dencrypted.sameElements(message.getBytes(StandardCharsets.UTF_8)))
    assert(new String(dencrypted, StandardCharsets.UTF_8) == message)

  }

}
