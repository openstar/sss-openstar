package sss.openstar.account

import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Millis, Minutes, Span}
import sss.openstar.common.builders.{IOExecutionContextBuilder, KeyPersisterBuilder}
import sss.openstar.common.users.{ClasspathKeyPersistRead, InMemoryKeyPersister}

trait KeyPersisterFixture extends
  KeyPersisterBuilder
  with IOExecutionContextBuilder {

  // create a new TrieMap everytime this trait is initialised
  lazy val inMemoryKeyPersister = new InMemoryKeyPersister {}

  lazy val testKeyReader: KeyPersistRead = inMemoryKeyPersister
  lazy val testKeyWriter: KeyPersistWrite = inMemoryKeyPersister

  override lazy val orderedSeqKeyReaders: Seq[KeyPersistRead] = Seq(testKeyReader, new ClasspathKeyPersistRead())

  override lazy val keyWriter: KeyPersistWrite = testKeyWriter

  def clearInMemKeyPersister(): Unit = inMemoryKeyPersister.clear()

}

trait KeyPersisterFixtureWithScalaFutures extends KeyPersisterFixture {

  self: ScalaFutures =>

  implicit override val patienceConfig: PatienceConfig =
    PatienceConfig(timeout = scaled(Span(1, Minutes)), interval = scaled(Span(20, Millis)))

}
