package sss.openstar.common.users

import org.scalatest.concurrent.ScalaFutures
import org.scalatest.concurrent.ScalaFutures._
import org.scalatest.concurrent.Waiters.scaled
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.scalatest.time.{Millis, Minutes, Seconds, Span}
import sss.openstar.account.NodeIdTag
import sss.openstar.util.IOExecutionContext

import scala.concurrent.ExecutionContext.global


class ClasspathKeyPersistReadSpec extends AnyFlatSpec with Matchers with ScalaFutures {

  implicit override val patienceConfig: PatienceConfig =
    PatienceConfig(timeout = scaled(Span(1, Minutes)), interval = scaled(Span(20, Millis)))

  private implicit val ioEc = new IOExecutionContext(global)
  val sut = new ClasspathKeyPersistRead()

  val fooContent = "OyFmmJpyQePW6lHzmZW0xs9nC-NE189X_54-WXgFm3U:::sha1:64000:32:kNw8dyKOTlzq6Z7vDcjA/2TcPb05aCJH:4eD3lqyKui4GdfbvUPHFUaalu72dlLqncPPSUCBPEG0=:::jDo2PCVV0b/9eu6sG84eew==:::R2ZlWFBWeFh5d0hwOFJlRkFwbndqbUY3YnFsRzlydERYVFNyNnlqMHhNNGlSM3R5N05YeE41M0x5UzIrcG1Rbw"
  val staticKeyFileOnTestClasspathIdName = "foo"
  val missingKeyFileMsg = s"Where is the $staticKeyFileOnTestClasspathIdName key file, should be on classpath"

  "ClasspathKeyPersistRead" should "read a key file on classpath" in {
    val readOpt = sut.read(staticKeyFileOnTestClasspathIdName, NodeIdTag.defaultTag).futureValue
    assert(readOpt.isDefined, missingKeyFileMsg)
    assert(readOpt.get == fooContent, s"content of $staticKeyFileOnTestClasspathIdName wrong?")
  }

  it should "return true for exists" in {
    import ioEc.ec
    val result = sut.exists(staticKeyFileOnTestClasspathIdName, NodeIdTag.defaultTag).futureValue
    assert(result,missingKeyFileMsg)
  }

  it should "fail for non existent classpath file" in {
    import ioEc.ec
    val result = sut.exists(staticKeyFileOnTestClasspathIdName.tail, NodeIdTag.defaultTag).futureValue
    assert(!result,"File shold not exist")
  }


}
