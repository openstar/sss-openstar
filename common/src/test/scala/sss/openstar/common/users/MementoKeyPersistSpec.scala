package sss.openstar.common.users

import org.scalatest.concurrent.ScalaFutures.convertScalaFuture
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.openstar.account.NodeIdTag

class MementoKeyPersistSpec extends AnyFlatSpec with Matchers  {

  import scala.concurrent.ExecutionContext.Implicits.global
  val sut = MementoKeyPersist

  val fooContent = "OyFmmJpyQePW6lHzmZW0xs9nC-NE189X_54-WXgFm3U:::sha1:64000:32:kNw8dyKOTlzq6Z7vDcjA/2TcPb05aCJH:4eD3lqyKui4GdfbvUPHFUaalu72dlLqncPPSUCBPEG0=:::jDo2PCVV0b/9eu6sG84eew==:::R2ZlWFBWeFh5d0hwOFJlRkFwbndqbUY3YnFsRzlydERYVFNyNnlqMHhNNGlSM3R5N05YeE41M0x5UzIrcG1Rbw"
  val name = "foor"


  "MementoKeyPersist" should "write a key file" in {
    sut.write(name, NodeIdTag.defaultTag, fooContent).futureValue
  }

  it should "now exist" in {
    val exists = sut.exists(name, NodeIdTag.defaultTag).futureValue
    assert(exists)
  }

  it should "be readable with the correcet content" in {
    val contents = sut.read(name, NodeIdTag.defaultTag).futureValue
    assert(contents.contains(fooContent))
  }

  it should "be successfully removed" in {
    val removed = sut.remove(name, NodeIdTag.defaultTag).futureValue
    assert(removed, "Should have removed this file")
  }

  it should "fail to read non existent key file" in {
    val stillExists = sut.exists(name, NodeIdTag.defaultTag).futureValue
    assert(!stillExists, "File should be gone")
  }

}
