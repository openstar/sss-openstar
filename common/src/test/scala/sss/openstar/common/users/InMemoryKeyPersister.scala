package sss.openstar.common.users

import sss.openstar.account.{KeyPersistRead, KeyPersistWrite}

import scala.collection.concurrent.TrieMap
import scala.concurrent.Future
import scala.util.Try

trait InMemoryKeyPersister extends KeyPersistRead with KeyPersistWrite {

  private val keys = TrieMap.empty[String, String]

  private val delim = "::::::"

  override def read(identity: String, tag: String): Future[Option[String]] =
    Future.fromTry(
      Try(
        keys.get(s"$identity${delim}${tag}")
      )
    )

  override def write(identity: String, tag: String, content: String): Future[Unit] =
    Future.fromTry(
      Try(
        keys.put(s"$identity${delim}${tag}", content)
      )
    )

  override def remove(identity: String, tag: String): Future[Boolean] =
    Future.fromTry(Try(keys.remove(s"$identity${delim}${tag}").isDefined))

  def clear(): Unit = keys.clear()

}

object InMemoryKeyPersister extends InMemoryKeyPersister