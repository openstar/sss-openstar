package sss.openstar.common.users


import org.scalatest.concurrent.ScalaFutures
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.openstar.db.DbRemoteFixture

import scala.concurrent.Future

class DbUserServiceSpec extends AnyFlatSpec
  with Matchers
  with DbRemoteFixture
  with ScalaFutures {

  import scala.concurrent.ExecutionContext.Implicits.global

  lazy val sut = new DbUserService(remoteSchemaName)
  val id = "asdasd"
  val id2 = "asdasd2"
  val tag = "tag"
  val tag2 = "tag2"
  val content = "asdasdasdasdasdasd"
  val content2 = "asdasdasdasdasdasd2"

  val normalUser2 = User(id2, AdminStatus.normal)
  val normalUser = User(id, AdminStatus.normal)
  val lockedUser = User(id, AdminStatus.locked)

  "Db User Service" should "allow insert and retrieval" in {

    sut.listUsers().toList shouldBe empty
    sut.write(id, tag, content).futureValue
    sut.read(id, tag).futureValue shouldBe Some(content)
    sut.listUsers().toList shouldBe List(normalUser)
  }

  it should "support user locking" in {
    var userOpt = sut.lockUser(id).futureValue
    userOpt shouldBe Some(lockedUser)
    userOpt = sut.unLockUser(id).futureValue
    userOpt shouldBe Some(normalUser)
  }

  it should "differentiate between users" in {
    sut.write(id2, tag2, content2).futureValue
    sut.read(id, tag).futureValue shouldBe Some(content)
    sut.read(id, tag2).futureValue shouldBe None
    sut.read(id2, tag2).futureValue shouldBe Some(content2)
    sut.read(id2, tag).futureValue shouldBe None
    sut.listUsers().toList should contain allOf (normalUser, normalUser2)
  }

  it should "support adding multiple tags" in {
    sut.tags(id2).futureValue shouldBe Seq(tag2)
    sut.tags(id).futureValue shouldBe Seq(tag)
    sut.write(id, tag2, content2).futureValue
    sut.tags(id).futureValue should contain allOf (tag, tag2)
    sut.tags(id2).futureValue shouldBe Seq(tag2)
    sut.write(id2, tag, content2).futureValue
    sut.tags(id).futureValue should contain allOf (tag, tag2)
    sut.tags(id2).futureValue should contain allOf (tag2, tag)
  }

  it should "support removal of tags" in {

    sut.hasUserKey(id, tag).futureValue shouldBe true
    sut.remove(id, tag).futureValue shouldBe(true)
    sut.hasUserKey(id, tag).futureValue shouldBe false

    sut.tags(id).futureValue shouldBe Seq(tag2)
    sut.remove(id, tag).futureValue shouldBe(false)
    sut.tags(id).futureValue shouldBe Seq(tag2)
    sut.remove(id, tag2).futureValue shouldBe(true)
    sut.remove(id, tag2).futureValue shouldBe(false)
    sut.tags(id).futureValue shouldBe Seq.empty
  }

  it should "support removal of user" in {
    sut.listUsers().find(_.identity == id) shouldBe Some(normalUser)
    sut.deleteUser(id).futureValue
    sut.listUsers().find(_.identity == id) shouldBe None
  }

  it should "support removal of user and her tags" in {
    sut.listUsers().find(_.identity == id2) shouldBe Some(normalUser2)
    sut.tags(id2).futureValue should contain allOf (tag, tag2)
    sut.deleteUser(id2).futureValue
    sut.listUsers().find(_.identity == id2) shouldBe None
    sut.tags(id2).failed.futureValue shouldBe an [IllegalArgumentException]

  }

  it should "support a longish list of users" in {
    val allTags = Seq("defaultTag", "mobileTag", "desktopTag")

    val newUsers = 0 to 10 map (i => id + "different" + i)
    val newUsersWithTags = newUsers flatMap { u => {
      allTags map { tag =>
        (u, tag)
      }
    }}

    //make a bit more realistic by allowing all writes in futures.
    val allF = newUsersWithTags.map {
      case (newUser, tag) => sut.write(newUser, tag, "blahdehblahdehblahdehblahdehblahdehblahdeh")
    }

    Future.sequence(allF).futureValue

    sut.listUsers().forall(u => {
      newUsers.contains(u.identity) && sut.tags(u.identity).futureValue.forall(allTags.contains)
    }) shouldBe true
  }

}
