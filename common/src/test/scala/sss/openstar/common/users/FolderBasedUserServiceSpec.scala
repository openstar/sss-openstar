package sss.openstar.common.users

import org.scalatest.concurrent.ScalaFutures.convertScalaFuture
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.ancillary.Configure
import sss.ancillary.FutureOps.AwaitResult

import java.nio.file.{Files, Paths}

class FolderBasedUserServiceSpec extends AnyFlatSpec
  with Matchers with Configure {

  import scala.concurrent.ExecutionContext.Implicits.global
  val id = "asdasd"
  val emailId = "john.m@some.domain.com"
  val tag = "tag"
  val tag2 = "tag2"

  val tmpFolder = Files.createTempDirectory("FOLDER_TEST")

  require(tmpFolder.toFile.isDirectory)

  def createUserKey(user: String, tag: String): Unit = {
    Files.createFile(tmpFolder.resolve(s"${user}.${tag}"))
  }

  val sut = new FolderBasedUserDirectory(tmpFolder)

  "User Service" should "start with empty folder" in {
    val all = sut.listUsers()
    assert(all.isEmpty)
  }

  it should "find created users tags" in {

    createUserKey(id, tag)
    var tags = sut.tags(id).futureValue
    assert(tags == Seq(tag))
    createUserKey(id, tag2)
    tags = sut.tags(id).futureValue
    tags should contain allOf (tag, tag2)
  }

  it should "create user tags for correct emails" in {
    createUserKey(emailId, tag)
    var tags = sut.tags(emailId).futureValue
    assert(tags == Seq(tag))
    createUserKey(emailId, tag2)
    tags = sut.tags(emailId).futureValue
    tags should contain allOf(tag, tag2)
    //cleanup
    sut.deleteUserKey(emailId, tag).futureValue shouldEqual Some(true)
    sut.deleteUserKey(emailId, tag2).futureValue shouldEqual Some(true)
  }

  it should "list new users" in {
    sut.listUsers().toList shouldEqual List(User(id, AdminStatus.normal))
  }

  it should "remove an existing tag" in {
    sut.deleteUserKey(id, tag).futureValue shouldEqual Some(true)
  }

  it should "not remove a non existing tag" in {
    sut.deleteUserKey(id, tag).futureValue shouldEqual None
  }

  it should "re add tag" in {

    createUserKey(id, tag)
    val tags = sut.tags(id).futureValue
    sut.hasUserKey(id, tag).futureValue shouldBe true
    tags should contain allOf (tag, tag2)
  }

  it should "remove all tags for a user" in {

    sut.deleteUser(id).futureValue
    sut.listUsers() shouldBe empty
    sut.tags(id).futureValue shouldBe empty
    sut.deleteUserKey(id, tag).futureValue shouldBe None
  }

  it should "support large amounts of users" in {

    val tags = Seq("default", "mobile", "desktop")

    val users = (0 to 1000).tapEach(i => {
      tags.foreach(tag => createUserKey(s"user$i", tag))
    }).map(i => User(s"user$i", AdminStatus.normal))

    sut.listUsers().forall(users.contains) shouldBe true

    //remove one key
    sut
      .listUsers()
      .map(u => (u.identity, tags.head)).toList.foreach {
      case (id, tag) => sut.deleteUserKey(id, tag).futureValue
    }

    //all users still present through other keys
    sut
      .listUsers()
      .forall(users.contains) shouldBe true

    //remove users

    def iterate(s: LazyList[String]): Unit = {
      val asList = s.toList
      if(asList.nonEmpty) {
        asList.foreach(u => sut.deleteUser(u).futureValue)
        iterate(sut.listUsers().take(10).map(_.identity))
      }
    }

    //Avoid stale stream issues
    iterate(sut.listUsers().take(10).map(_.identity))

    //all gone
    sut
      .listUsers() shouldBe empty
  }

}
