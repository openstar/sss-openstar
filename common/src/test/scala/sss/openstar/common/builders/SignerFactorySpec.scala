package sss.openstar.common.builders

import org.scalatest.concurrent.ScalaFutures
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import scorex.crypto.signatures.{MessageToSign, PublicKey, Signature}
import sss.ancillary.ByteArrayComparisonOps
import sss.ancillary.FutureOps.AwaitResult
import sss.openstar.account.{NodeIdTag, NodeSigner, NodeVerifier, NodeVerifierSigner, TypedPublicKey, Verifier}
import sss.openstar.account.NodeIdentityManager.{PasswordCredentials, SignerDetails}
import sss.openstar.account.SignerFactory.SignerDescriptor
import sss.openstar.account.impl.EncryptedKeyFileAccountSignerCreator
import sss.openstar.crypto.keypairs.Curve25519AccountKeys

import scala.concurrent.Future

class SignerFactorySpec extends AnyFlatSpec
  with Matchers
  with ScalaFutures
  with ByteArrayComparisonOps
  with SignerFactoryBuilderFixture {

  "The default factory" should "start with no creds" in {
    signerFactory.seqCredentials shouldBe Seq.empty
  }

  val nidATag = NodeIdTag("a", "default")
  val nidMobileTag = NodeIdTag("a", "mobile")
  val nidBTag = NodeIdTag("b", "default")
  val nidCTag = NodeIdTag("c", "default")

  val dummyPassword = "Password10!"

  val pc = PasswordCredentials(nidATag,dummyPassword)

  val sd: SignerDescriptor = SignerDescriptor(
    Curve25519AccountKeys.keysType,
    EncryptedKeyFileAccountSignerCreator.SecurityLevel,
    EncryptedKeyFileAccountSignerCreator.SignerType,
    EncryptedKeyFileAccountSignerCreator.InteractionType
  )


  val pk = PublicKey(Array.emptyByteArray)

  val goodSignerDesc: SignerDetails = SignerDetails(
    nidATag, pk, None, sd
  )

  val nvs = new NodeVerifierSigner {
    override def signerDescriptor: SignerDescriptor = sd

    override def signerOpt: Option[NodeSigner] = None

    override def nodeIdTag: NodeIdTag = NodeIdTag("aa", "bb")

    override def verifier: Verifier = new Verifier {
      override def typedPublicKey: TypedPublicKey = TypedPublicKey(
        PublicKey(Array.emptyByteArray), "kt")

      override def verify(message: MessageToSign, signature: Signature): Boolean = true
    }
  }

  it should "allow finding known signer" in {

    signerFactory.get(goodSignerDesc).isDefined shouldBe true

  }

  it should "allow finding known singer when merged" in {

    val tf =  TestSignerFactory(
      Seq.empty, Seq.empty, {
        case n if n.signerDescriptor.signerType == "MINE" => Future.successful(nvs)
      }
    )

    tf.merge(signerFactory).get(goodSignerDesc).isDefined shouldBe true
    val merged = signerFactory.merge(tf)
    merged.get(goodSignerDesc).isDefined shouldBe true


    val resCreds = merged.withCredentials(Seq(pc))
    resCreds.get(goodSignerDesc).isDefined shouldBe(true)

  }

  it should "correctly merge the credentials" in {
    val tf1 = TestSignerFactory(
      Seq(PasswordCredentials(nidATag, dummyPassword)), Seq.empty, {
        case n if n.signerDescriptor.signerType == "MINE" => Future.successful(nvs)
      }
    )

    val tf2 = TestSignerFactory(
      Seq(PasswordCredentials(nidBTag, dummyPassword)), Seq.empty, {
        case n if n.signerDescriptor.signerType == "MINE" => Future.successful(nvs)
      }
    )
    val toBeFound = PasswordCredentials(nidATag, dummyPassword)
    val toBeFoundB = PasswordCredentials(nidBTag, dummyPassword)

    tf1.seqCredentials.contains(toBeFound) shouldBe true
    tf1.seqCredentials.contains(toBeFoundB) shouldBe false
    tf2.seqCredentials.contains(toBeFound) shouldBe false
    tf2.seqCredentials.contains(toBeFoundB) shouldBe true
    val tf1tf2 = tf1.merge(tf2)
    val tf2tf1 = tf2.merge(tf1)

    tf1tf2.seqCredentials.contains(toBeFound) shouldBe true
    tf2tf1.seqCredentials.contains(toBeFound) shouldBe true

    tf1tf2.seqCredentials.contains(toBeFoundB) shouldBe true
    tf2tf1.seqCredentials.contains(toBeFoundB) shouldBe true

    val withNew = tf1tf2.withCredentials(Seq(PasswordCredentials(nidMobileTag, dummyPassword)))
    withNew.seqCredentials.contains(toBeFoundB) shouldBe true
    withNew.seqCredentials.contains(toBeFound) shouldBe true
    withNew.seqCredentials.contains(PasswordCredentials(nidMobileTag, dummyPassword)) shouldBe true
  }
}
