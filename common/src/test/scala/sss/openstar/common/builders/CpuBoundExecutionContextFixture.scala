package sss.openstar.common.builders
import scala.concurrent.ExecutionContext
import scala.concurrent.ExecutionContext.Implicits

trait CpuBoundExecutionContextFixture extends CpuBoundExecutionContextBuilder {
  override implicit val cpuOnlyEc: ExecutionContext = Implicits.global
}
