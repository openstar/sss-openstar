package sss.openstar.common.builders

trait PhraseFixture extends RequirePhrase {

  override lazy val phrase: Option[String] = Some("Password10!")
  lazy val testPassword = phrase.get
}
