package sss.openstar.common.builders

import sss.openstar.account.{NodeIdentityManager, NodeVerifierSigner, SignerFactory}

import scala.concurrent.Future

trait SignerFactoryBuilderFixture extends SignerFactoryBuilder
  with KeyPersisterBuilder
  with IOExecutionContextBuilder
  with CpuBoundExecutionContextFromActorSystemBuilder
  with ActorSystemBuilder
  with KeyFactoryBuilder {

  case class TestSignerFactory(
                                seqCredentials: Seq[NodeIdentityManager.Credentials],
                                signerDescriptors: Seq[SignerFactory.SignerDescriptor],
                                lookupMatcher: PartialFunction[NodeIdentityManager.SignerDetails, Future[NodeVerifierSigner]]) extends SignerFactory {

    override protected def withMergedCredentials(seqMergedCredentials: Seq[NodeIdentityManager.Credentials]): SignerFactory =
      copy(seqCredentials = seqMergedCredentials)

  }
}


