package sss.openstar.common.builders

import sss.openstar.account.NodeIdTag

trait NodeIdTagFixture extends NodeIdTagBuilder with ConfigBuilder {
  override lazy val nodeIdTag = NodeIdTag("TestNodeFromNodeIdTagFixture".toLowerCase(), NodeIdTag.defaultTag)
}
