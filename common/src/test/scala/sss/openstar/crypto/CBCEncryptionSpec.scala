package sss.openstar.crypto

import java.util

import org.scalatest.matchers.should.Matchers
import org.scalatest.propspec.AnyPropSpec
import org.scalatestplus.scalacheck.ScalaCheckDrivenPropertyChecks
import sss.openstar.DummySeedBytes

/**
  * Created by alan on 2/11/16.
  */
class CBCEncryptionSpec extends AnyPropSpec
  with ScalaCheckDrivenPropertyChecks
  with Matchers {

  property("decrypted byte array should match the pre encryption array when key is also byte array") {

    forAll(minSuccessful(500)) {
      (key: Array[Byte], value: Array[Byte]) => {
        whenever(key.length > 0 && value.length > 0) {
          val iv = CBCEncryption.newInitVector(DummySeedBytes)

          val aesKey = AESDetails.aesKeyFromByteArray(key)
          val encrypted = CBCEncryption.encrypt(aesKey, value, iv)
          encrypted shouldNot be(value)
          encrypted shouldNot be(key)

          val decrypted = CBCEncryption.decrypt(aesKey, encrypted, iv)
          decrypted should be(value)

          val k = AESDetails.generateAESKey()
          intercept[Exception] {
            val badDec = CBCEncryption.decrypt(k, encrypted, iv)
            assert(util.Arrays.equals(badDec, value), "Value correctly decrypted with wrong key!")
          }
        }
      }
    }
  }
}


