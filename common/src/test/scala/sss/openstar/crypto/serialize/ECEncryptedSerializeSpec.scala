package sss.openstar.crypto.serialize

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import scorex.crypto.signatures.PublicKey
import sss.ancillary.ByteArrayComparisonOps
import sss.openstar.DummySeedBytes
import sss.openstar.account.TypedPublicKey
import sss.openstar.crypto.CBCEncryption
import sss.openstar.crypto.ECEncryption.ECEncrypted
import sss.openstar.crypto.keypairs.DefaultKeyFactory

class ECEncryptedSerializeSpec extends AnyFlatSpec with Matchers with ByteArrayComparisonOps {

  "A ECEncrypted" should "serialize and de" in {

    val iv = CBCEncryption.newInitVector(DummySeedBytes)
    val t = TypedPublicKey(PublicKey(DummySeedBytes(32)), DefaultKeyFactory.supportedKeyTypes.head)
    val sut = ECEncrypted(DummySeedBytes(300), t, iv)
    val asBytes = ECEncryptedSerializer.toBytes(sut)
    val again = ECEncryptedSerializer.fromBytes(asBytes)
    assert(sut === again)

  }
}

