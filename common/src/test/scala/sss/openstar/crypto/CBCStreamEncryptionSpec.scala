package sss.openstar.crypto

import java.io.{ByteArrayInputStream, InputStream}
import java.nio.charset.StandardCharsets

import akka.util.ByteString
import org.scalatest.DoNotDiscover
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.openstar.crypto.CBCStreamEncryption.EncryptedStream

/**
  * Strong randomness is just too slow.
  */
class CBCStreamEncryptionSpec extends AnyFlatSpec with Matchers {


  val inClear = (0 to 10).map (_ => "Hello everybody and welcome to the cabaret").mkString
  val asBytes = inClear.getBytes(StandardCharsets.UTF_8)
  val inStreamClear = new ByteArrayInputStream(asBytes)


  "CBCStreamEncryption" should "encrypt and decrypt small inputstream" in {

    val EncryptedStream(encIn, key, iv) = CBCStreamEncryption.encrypt(inStreamClear)

    val readInEnc = inputStreamToByteString(encIn)

    val newIn = new ByteArrayInputStream(readInEnc.toArray)

    val inDec = CBCStreamEncryption.decrypt(newIn,
      key,
      CBCEncryption.initVector(iv.bytes))

    val readInDec = inputStreamToByteString(inDec)

    val result = new String(readInDec.toArray, StandardCharsets.UTF_8)
    assert(result == inClear)
  }

  /*it should "encrypt a large file" in {
    val f = createFile(10000)
    val in1 = new FileInputStream(f)
    val EncryptedStream(in, key, iv) = CBCStreamEncryption.encrypt(in1)
    val outputStream = new BufferedOutputStream((new FileOutputStream(file)))
  }
  ABANDONED
  def createFile(numLines: Int): File = {
    val file =  File.createTempFile("testStreamEncryption", ".txt")
    try {
      val outputStream = new BufferedOutputStream((new FileOutputStream(file)))
      try for (i <- 0 until numLines) {
        outputStream.write(new String("Hello World\n").getBytes)
      }
      finally if (outputStream != null) outputStream.close()
      file
    }
  }*/

  def inputStreamToByteString(in: InputStream): ByteString = {
    val buf = new Array[Byte](10)

    var num = in.read(buf)
    var byteString = ByteString(buf.take(num))
    while(num >= 0) {
      num = in.read(buf)
      byteString = byteString ++ ByteString(buf.take(num))
    }

    in.close()
    byteString
  }


}


