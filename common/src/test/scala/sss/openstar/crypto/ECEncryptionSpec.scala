package sss.openstar.crypto


import org.scalatest.matchers.should.Matchers
import org.scalatest.propspec.AnyPropSpec
import org.scalatestplus.scalacheck.ScalaCheckDrivenPropertyChecks
import sss.ancillary.FutureOps.AwaitResult
import sss.openstar.account.{AccountKeysFactoryFixture, Signer}
import sss.openstar.crypto.ECEncryption.{CreateSharedSecret, CreateSharedSecretF}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
/**
  * Created by alan on 2/11/16.
  */
class ECEncryptionSpec extends AnyPropSpec
  with ScalaCheckDrivenPropertyChecks
  with AccountKeysFactoryFixture
  with Matchers {

  property("decrypted string should match the pre encryption string ") {

    forAll(minSuccessful(500)) {
      (value: Array[Byte]) => {
        whenever(value.length > 0) {

          val tmpAc = createPrivateAc()
          val tmpAc2 = createPrivateAc()

          def makeShareSecretAndWait(privateAc: Signer): CreateSharedSecretF =
            key => Future.successful(privateAc.createSharedSecret(key))

          val encrypted = ECEncryption.encrypt(tmpAc.typedPublicKey, value, tmpAc2)
          val decrypted = ECEncryption.decrypt(encrypted, makeShareSecretAndWait(tmpAc)).await()
          assert(value === decrypted)
          val str = encrypted.encodedAsBase64String
          val ec2 = ECEncryption.fromBase64Encoded(str)
          val decrypted2 = ECEncryption.decrypt(ec2,  makeShareSecretAndWait(tmpAc)).await()
          assert(value === decrypted2)

        }
      }
    }
  }

}


