
name := "openstar-common"

libraryDependencies += "org.scorexfoundation" %% "scrypto" % Vers.scryptoVer

libraryDependencies += "com.mcsherrylabs" %% "sss-ancillary" % Vers.ancillaryVer

libraryDependencies += "com.typesafe.akka" %% "akka-actor" % Vers.akkaVer

libraryDependencies += "com.mcsherrylabs" %% "sss-db" % Vers.sssDbVer

libraryDependencies += "org.hsqldb" % "hsqldb" % Vers.hsqldbVer % Test

libraryDependencies += "org.flywaydb" % "flyway-core" % Vers.flywayVersion

// https://mvnrepository.com/artifact/net.i2p.crypto/eddsa
libraryDependencies += "net.i2p.crypto" % "eddsa" % Vers.ip2Version
