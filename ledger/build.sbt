name := "openstar-ledger"

Test / parallelExecution := false

libraryDependencies += "com.mcsherrylabs" %% "sss-ancillary" % Vers.ancillaryVer

libraryDependencies += "com.mcsherrylabs" %% "sss-db" % Vers.sssDbVer

libraryDependencies += "org.hsqldb" % "hsqldb" % Vers.hsqldbVer % Test

libraryDependencies += "org.typelevel" %% "cats-core" % Vers.catsCore
