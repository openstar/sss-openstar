package sss.openstar.contract

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.ancillary.ByteArrayComparisonOps
import sss.ancillary.FutureOps.AwaitResult
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.DummySeedBytes
import sss.openstar.account.AccountKeysFactoryFixture
import sss.openstar.balanceledger.CoinbaseBalanceLedger.CoinbaseBalanceLedgerContext
import sss.openstar.common.builders.{CpuBoundExecutionContextFixture, KeyFactoryBuilder, VerifierFactoryBuilder}
import sss.openstar.db.LedgerMigratedDbFixture
import sss.openstar.identityledger.{IdentityService, IdentityServiceFixture}

import java.nio.charset.StandardCharsets
import scala.concurrent.Future

/**
  * Created by alan on 2/15/16.
  */
class SaleOrReturnSecretEncSpec extends AnyFlatSpec with Matchers with ByteArrayComparisonOps with LedgerMigratedDbFixture
  with IdentityServiceFixture
  with AccountKeysFactoryFixture
  with CpuBoundExecutionContextFixture
  with VerifierFactoryBuilder
  with KeyFactoryBuilder {

  lazy val accountOpt: FindPublicKeyAccFTxOpt = (id, tag) =>
    identityService.accountOptFTx(id, tag).map(_.map(identityService.toVerifier))

  lazy val lc = CoinbaseBalanceLedgerContext(0L, accountOpt, keysFactoryLookup)

  val myTag = "homepc"
  val myTagBytes = myTag.getBytes(StandardCharsets.UTF_8)
  val myTag2 = "mobile"
  val myTag2Bytes = myTag2.getBytes(StandardCharsets.UTF_8)

  val txId = DummySeedBytes.randomSeed(32)
  lazy val pkPair = createPrivateAc()
  val pkPairSign = (a: Array[Byte]) => Future.successful(pkPair.sign(a))

  lazy val pkPair2 = createPrivateAc()
  val pkPair2Sign = (a: Array[Byte]) => Future.successful(pkPair2.sign(a))

  val sellerIdentity = "seller_identity"
  val buyerId = "buyer_id"
  val secret = "ohnowhathavetheydone"
  val badSecret = s"$secret!"

  val returnBlockHeight = 10

  import sss.openstar.identityledger.BackwardsCompatibilityOps.PublicKeyOps

  override def beforeAll(): Unit = {
    super.beforeAll()
    identityService.claim( pkPair.typedPublicKey.publicKey.toDetails(sellerIdentity, myTag)).dbRunSyncGet
    identityService.claim( pkPair2.typedPublicKey.publicKey.toDetails(buyerId, myTag2)).dbRunSyncGet
  }


  "A sale or encumbrance " should "be decumbered by identity key sig with the correct secret " in {

    val enc = SaleOrReturnSecretEnc(sellerIdentity, buyerId, Some(secret.getBytes(StandardCharsets.UTF_8)), returnBlockHeight )

    val sig = SaleSecretDec.createUnlockingSignature(
      txId, buyerId, myTag2.getBytes(StandardCharsets.UTF_8), pkPair2Sign, secret.getBytes(StandardCharsets.UTF_8)).await()
    assert(enc.decumber(txId +: sig, lc, SaleSecretDec).dbRunSyncGet)

  }

  it should "fail if the secret is bad" in {
    val enc = SaleOrReturnSecretEnc(sellerIdentity, buyerId, Some(secret.getBytes(StandardCharsets.UTF_8)), returnBlockHeight )

    val sig = SaleSecretDec.createUnlockingSignature(
      txId,
      buyerId,
      myTag2.getBytes(StandardCharsets.UTF_8),
      pkPair2Sign,
      badSecret.getBytes(StandardCharsets.UTF_8))
      .await()

    assert(!(enc.decumber(txId +:sig, lc, SaleSecretDec).dbRunSyncGet), "Should not decumber.... ")

  }

  it should " fail if the identity is bad  " in {

    val enc = SaleOrReturnSecretEnc(sellerIdentity, buyerId + "!", Some(secret.getBytes(StandardCharsets.UTF_8)), returnBlockHeight )

    val sig = SaleSecretDec.createUnlockingSignature(
      txId, buyerId, myTag2.getBytes(StandardCharsets.UTF_8), pkPair2Sign, secret.getBytes(StandardCharsets.UTF_8)).await()
    intercept[Exception](!enc.decumber(txId +:sig, lc, SaleSecretDec).dbRunSyncGet)

  }

  it should "not succeed if it now returnable " in {

    val enc = SaleOrReturnSecretEnc(sellerIdentity, buyerId, Some(secret.getBytes(StandardCharsets.UTF_8)), returnBlockHeight )
    val identityService = IdentityService()
    val accountOpt: FindPublicKeyAccFTxOpt = (id, tag) => identityService.accountOptFTx(id, tag).map(_.map(identityService.toVerifier))
    val lc = CoinbaseBalanceLedgerContext(11L, accountOpt, keysFactoryLookup)
    val sig = SaleSecretDec.createUnlockingSignature(
      txId, buyerId, myTag2Bytes, pkPair2Sign, secret.getBytes(StandardCharsets.UTF_8)).await()
    intercept[Exception](enc.decumber(txId +:sig, lc, SaleSecretDec))

  }

  it should " be impossible to decumber by seller before block height " in {
    val enc = SaleOrReturnSecretEnc(sellerIdentity, buyerId, Option(secret.getBytes(StandardCharsets.UTF_8)), returnBlockHeight )
    val identityService = IdentityService()
    val accountOpt: FindPublicKeyAccFTxOpt = (id, tag) => identityService.accountOptFTx(id, tag).map(_.map(identityService.toVerifier))
    val lc = CoinbaseBalanceLedgerContext(9L, accountOpt, keysFactoryLookup)
    val sig = ReturnSecretDec.createUnlockingSignature(
      txId, myTag2Bytes, pkPair2Sign).await()
    intercept[Exception](enc.decumber(txId +:sig, lc, ReturnSecretDec))
  }

  it should " be possible to decumber by seller after block height " in {
    val enc = SaleOrReturnSecretEnc(sellerIdentity, buyerId, Option(secret.getBytes(StandardCharsets.UTF_8)), returnBlockHeight )
    val identityService = IdentityService()
    val accountOpt: FindPublicKeyAccFTxOpt = (id, tag) =>
      identityService.accountOptFTx(id, tag).map(_.map(identityService.toVerifier))

    val lc2 = CoinbaseBalanceLedgerContext(10L, accountOpt, keysFactoryLookup)

    val sig = ReturnSecretDec.createUnlockingSignature(
      txId, myTagBytes, pkPairSign).await()
    assert(enc.decumber(txId +:sig, lc2, ReturnSecretDec).dbRunSyncGet)
  }

  it should " recognise the claimant and seller are reversed" in {
    val enc = SaleOrReturnSecretEnc(buyerId, sellerIdentity, Option(secret.getBytes(StandardCharsets.UTF_8)), returnBlockHeight )
    val identityService = IdentityService()
    val accountOpt: FindPublicKeyAccFTxOpt = (id, tag) => identityService.accountOptFTx(id, tag).map(_.map(identityService.toVerifier))
    val lc2 = CoinbaseBalanceLedgerContext(10L, accountOpt, keysFactoryLookup)

    val sig = ReturnSecretDec.createUnlockingSignature(
      txId, myTagBytes, pkPairSign).await()
    intercept[Exception](enc.decumber(txId +:sig, lc2, ReturnSecretDec).dbRunSyncGet)
  }

  it should "decumber (Sale) without using a secret if no secret was set up" in {
    val enc = SaleOrReturnSecretEnc(sellerIdentity, buyerId, None, returnBlockHeight )

    val sig = SaleSecretDec.createUnlockingSignature(
      txId, buyerId, myTag2Bytes, pkPair2Sign).await()
    assert(enc.decumber(txId +: sig, lc, SaleSecretDec).dbRunSyncGet)

  }

  it should "decumber (Sale) using any of the claimants" in {
    val enc = SaleOrReturnSecretEnc(sellerIdentity, Seq(buyerId, sellerIdentity), None, returnBlockHeight )

    val sig = SaleSecretDec.createUnlockingSignature(
      txId, sellerIdentity, myTag.getBytes(StandardCharsets.UTF_8), pkPairSign).await()
    assert(enc.decumber(txId +: sig, lc, SaleSecretDec).dbRunSyncGet)

  }

  it should "decumber (Return) without using a secret if no secret was set up" in {
    val enc = SaleOrReturnSecretEnc(sellerIdentity, buyerId, None, returnBlockHeight )
    val identityService = IdentityService()
    val accountOpt: FindPublicKeyAccFTxOpt = (id, tag) => identityService.accountOptFTx(id, tag).map(_.map(identityService.toVerifier))
    val lc2 = CoinbaseBalanceLedgerContext(10L, accountOpt, keysFactoryLookup)

    val sig = ReturnSecretDec.createUnlockingSignature(
      txId, myTagBytes, pkPairSign).await()
    assert(enc.decumber(txId +:sig, lc2, ReturnSecretDec).dbRunSyncGet)
  }

  it should "not decumber using a secret if no secret was set up" in {
    val enc = SaleOrReturnSecretEnc(sellerIdentity, buyerId, None, returnBlockHeight )

    val sig = SaleSecretDec.createUnlockingSignature(
      txId, buyerId, myTag2Bytes, pkPair2Sign, secret.getBytes(StandardCharsets.UTF_8))
      .await()

    intercept[Exception](enc.decumber(txId +: sig, lc, SaleSecretDec).dbRunSyncGet)

  }
}
