package sss.openstar.contract


import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.ancillary.ByteArrayComparisonOps
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.DummySeedBytes
import sss.openstar.account.AccountKeysFactoryFixture
import sss.openstar.balanceledger.CoinbaseBalanceLedger.CoinbaseBalanceLedgerContext
import sss.openstar.common.builders.{CpuBoundExecutionContextFixture, KeyFactoryBuilder, VerifierFactoryBuilder}
import sss.openstar.db.LedgerMigratedDbFixture
import sss.openstar.identityledger.{IdentityServiceFixture, SignerVerifierDetailsFixture}
import sss.ancillary.FutureOps._

import java.nio.charset.StandardCharsets
import scala.concurrent.Future

/**
  * Created by alan on 2/15/16.
  */
class ContractSpec extends AnyFlatSpec with Matchers
  with ByteArrayComparisonOps
  with LedgerMigratedDbFixture
  with IdentityServiceFixture
  with VerifierFactoryBuilder
  with KeyFactoryBuilder
  with AccountKeysFactoryFixture
  with CpuBoundExecutionContextFixture
  with SignerVerifierDetailsFixture {

  lazy val pkPair = createPrivateAc()

  lazy val accountOpt: FindPublicKeyAccFTxOpt = (id, tag) =>
    identityService.accountOptFTx(id, tag).map(_.map(identityService.toVerifier))

  lazy val coinbaseBalanceLedgerContext = CoinbaseBalanceLedgerContext(0L, accountOpt, keysFactoryLookup)

  "A single sig " should " unlock a single key contract " in {

    val enc = SinglePrivateKey(pkPair.typedPublicKey.publicKey)
    assert(enc.pKey.isSame(pkPair.typedPublicKey.publicKey))

    val msg = "sfsfsdfsdfsdf"
    val sig = pkPair.sign(msg.getBytes)

    assert(enc.decumber(Seq(msg.getBytes(), sig), coinbaseBalanceLedgerContext, PrivateKeySig).dbRunSyncGet)
  }

  it  should " correctly support equality and hashcode " in {

    val otherPkPair = createPrivateAc()
    val enc = SinglePrivateKey(pkPair.typedPublicKey.publicKey)
    val enc2 = SinglePrivateKey(pkPair.typedPublicKey.publicKey)
    val enc3 = SinglePrivateKey(otherPkPair.typedPublicKey.publicKey)
    assert(enc == enc2)
    assert(enc.hashCode() == enc2.hashCode())
    assert(enc != enc3)
    assert(enc2 != enc3)

  }

  it  should " fail if the wrong decumbrance is used " in {

    val enc = SinglePrivateKey(pkPair.typedPublicKey.publicKey)
    assert(enc.pKey.isSame(pkPair.typedPublicKey.publicKey))

    val msg = "sfsfsdfsdfsdf"
    val sig = pkPair.sign(msg.getBytes)

    assert(!enc.decumber(Seq(msg.getBytes(), sig), coinbaseBalanceLedgerContext, NullDecumbrance()).dbRunSyncGet)
  }

  it  should " fail if the msg is different  " in {

    val enc = SinglePrivateKey(pkPair.typedPublicKey.publicKey)
    assert(enc.pKey.isSame(pkPair.typedPublicKey.publicKey))

    val msg = "sfsfsdfsdfsdf"
    val sig = pkPair.sign(msg.getBytes)

    assert(!enc.decumber(Seq(msg.getBytes() ++ Array[Byte](0), sig),coinbaseBalanceLedgerContext, PrivateKeySig).dbRunSyncGet)
  }

  it  should " fail if the sig is different  " in {

    val enc = SinglePrivateKey(pkPair.typedPublicKey.publicKey)
    assert(enc.pKey.isSame(pkPair.typedPublicKey.publicKey))

    val msg = "sfsfsdfsdfsdf"
    val sig = pkPair.sign(msg.getBytes)

    assert(!enc.decumber(Seq(msg.getBytes(), sig ++ Array[Byte](0)),coinbaseBalanceLedgerContext, PrivateKeySig).dbRunSyncGet)
  }

  "A null ecumbrance" should " be decumbered by Null decumbrance " in {
    assert(NullEncumbrance.decumber(Seq(), coinbaseBalanceLedgerContext, NullDecumbrance()).dbRunSyncGet)
  }

  it should "  be decumbered by any decumbrance " in {
    assert(NullEncumbrance.decumber(Seq(), coinbaseBalanceLedgerContext, PrivateKeySig).dbRunSyncGet)
  }

  "An indentity encumbrance " should "be decumbered by any identity key sig " in {

    val myTag = "homepc"
    val myTagBytes = myTag.getBytes(StandardCharsets.UTF_8)
    val myTag2 = "mobile"
    val myTag2Bytes = myTag2.getBytes(StandardCharsets.UTF_8)

    val txId = DummySeedBytes.randomSeed(32)
    lazy val pkPair = createPrivateAc()
    lazy val pkPair2 = createPrivateAc()
    val myIdentity = "hithereworld"

    import sss.openstar.identityledger.BackwardsCompatibilityOps.PublicKeyOps
    identityService.claim(pkPair.typedPublicKey.publicKey.toDetails(myIdentity, myTag)).dbRunSyncGet
    val enc = SingleIdentityEnc(myIdentity, None, 0)
    def signer(ary: Array[Byte]): Future[Array[Byte]] = Future.successful(pkPair.sign(ary))
    def signer2(ary: Array[Byte]): Future[Array[Byte]] = Future.successful(pkPair2.sign(ary))


    val badSig = SingleIdentityDec.createUnlockingSignature(txId, myTag2Bytes, signer2).await()
    val goodSig = SingleIdentityDec.createUnlockingSignature(txId, myTagBytes, signer).await()
    assert(enc.decumber(txId +: goodSig, coinbaseBalanceLedgerContext, SingleIdentityDec).dbRunSyncGet)
    intercept[IllegalArgumentException](enc.decumber(txId +: badSig, coinbaseBalanceLedgerContext, SingleIdentityDec).dbRunSyncGet)

    require(pkPair2.typedPublicKey.keyType == svSignerVerifierDetails.typedPublicKey.keyType, "The test keys are out of sync!")

    val signDetails = signerDetailsWith(myIdentity,myTag2, pkPair2.typedPublicKey.publicKey )
    identityService.link(signDetails).dbRunSyncGet
    val goodSigPostLink = badSig
    assert(enc.decumber(txId +: goodSigPostLink , coinbaseBalanceLedgerContext, SingleIdentityDec).dbRunSyncGet)
  }

}
