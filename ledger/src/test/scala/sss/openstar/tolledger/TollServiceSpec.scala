package sss.openstar.tolledger

import org.scalatest.concurrent.ScalaFutures
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.account.NodeIdentityManagerFixture
import sss.openstar.tollledger.TollService.PayTollResultInfo.{BalanceTooLow, PaidWithBalance, PaidWithLicense}
import sss.openstar.tollledger.TollService.{TollServiceCredit, TollServiceResult}

import scala.util.{Failure, Success}


class TollServiceSpec extends AnyFlatSpec
  with Matchers
  with ScalaFutures
  with TollLedgerFixture
  with NodeIdentityManagerFixture {


  val alanNodeDesc = addNodeIdentityToFixture("alan")
  val otherNodeDesc = addNodeIdentityToFixture("other")

  val alanStartingBalance = 100000
  val testTransferAmount = 5000

  val licensedUntilHeight = 10
  val alanSignerDetails = signerDetails(alanNodeDesc.nodeIdTag, alanNodeDesc.publicKey)
  val otherSignerDetails = signerDetails(otherNodeDesc.nodeIdTag, otherNodeDesc.publicKey)
  identityService.claim(alanStartingBalance, Some(licensedUntilHeight), alanSignerDetails, Seq.empty).dbRunSyncGet
  identityService.claim(0, None, otherSignerDetails, Seq.empty).dbRunSyncGet

  val alanNodeId = getNodeIdentity(alanNodeDesc.nodeId, alanNodeDesc.tag)
  val otherNodeId = getNodeIdentity(otherNodeDesc.nodeId, otherNodeDesc.tag)

  "A Toll Service" should "initalize balances correctly" in {
      val bal = tollService.tollBalanceFTx(alanNodeDesc.nodeId).dbRunSyncGet
      bal.balance shouldBe alanStartingBalance
      val balOther = tollService.tollBalanceFTx(otherNodeDesc.nodeId).dbRunSyncGet
      balOther.balance shouldBe 0
  }

  it should "transfer without taking a toll" in {

    tollService.transfer(alanNodeDesc.nodeId, otherNodeDesc.nodeId, testTransferAmount).dbRunSyncGet
    tollService.tollBalanceFTx(alanNodeDesc.nodeId).dbRunSyncGet.balance shouldBe alanStartingBalance - testTransferAmount
    tollService.tollBalanceFTx(otherNodeDesc.nodeId).dbRunSyncGet.balance shouldBe testTransferAmount
  }

  it should "allow transfer making sender 0" in {

    tollService.transfer(otherNodeDesc.nodeId, alanNodeDesc.nodeId, testTransferAmount).dbRunSyncGet

    tollService.tollBalanceFTx(alanNodeDesc.nodeId).dbRunSyncGet.balance shouldBe alanStartingBalance
    tollService.tollBalanceFTx(otherNodeDesc.nodeId).dbRunSyncGet.balance shouldBe 0
  }

  it should "fail to pay a toll if no balance" in {
    tollService
      .payToll(
        otherNodeDesc.nodeId,
        tollAmount, 4)
      .dbRunSyncGet shouldBe TollServiceResult(false, BalanceTooLow, 0, None)
  }

  it should "pay toll and update balance if balance good" in {
    val transferResult = tollService.transfer(alanNodeDesc.nodeId, otherNodeDesc.nodeId, testTransferAmount).dbRunSyncGet
    val credit = Some(TollServiceCredit(Some(alanNodeDesc.nodeId), testTransferAmount, Seq(otherNodeDesc.nodeId)))
    transferResult shouldBe TollServiceResult(true, PaidWithBalance, alanStartingBalance - testTransferAmount, credit)

    tollService
      .payToll(
        otherNodeDesc.nodeId,
        tollAmount, 4)
      .dbRunSyncGet shouldBe TollServiceResult(true, PaidWithBalance, testTransferAmount - tollAmount, None)
  }

  it should "pay using license and not use balance" in {

    tollService
      .payToll(
        alanNodeDesc.nodeId,
        tollAmount, 4)
      .dbRunSyncGet shouldBe TollServiceResult(true, PaidWithLicense, alanStartingBalance - testTransferAmount, None)
  }

  it should "pay using balance when license expired" in {
    tollService
      .payToll(
        alanNodeDesc.nodeId,
        tollAmount, licensedUntilHeight + 1)
      .dbRunSyncGet shouldBe TollServiceResult(true, PaidWithBalance, alanStartingBalance - testTransferAmount - tollAmount, None)
  }

  it should "allow credit to support coinbase" in {
    val cred = 50000
    val otherBal = tollService.tollBalanceFTx(otherNodeDesc.nodeId).dbRunSyncGet
    val c = TollServiceCredit(None, cred)
    tollService.credit(otherNodeDesc.nodeId, c).dbRunSyncGet
    val otherBalAgain = tollService.tollBalanceFTx(otherNodeDesc.nodeId).dbRunSyncGet
    otherBalAgain.balance shouldBe(otherBal.balance + cred)
  }

  it should "prevent credit exceeding Long max size" in {

    val otherBal = tollService.tollBalanceFTx(otherNodeDesc.nodeId).dbRunSyncGet
    val c = TollServiceCredit(None, Long.MaxValue - otherBal.balance)
    tollService.credit(otherNodeDesc.nodeId, c).dbRunSyncGet
    val otherBalAgain = tollService.tollBalanceFTx(otherNodeDesc.nodeId).dbRunSyncGet
    otherBalAgain.balance shouldBe (Long.MaxValue )
    val c2 = TollServiceCredit(None, 1)
    tollService.credit(otherNodeDesc.nodeId, c2).dbRunSync match {
      case Failure(_: IllegalArgumentException) => succeed
      case _ => fail("?")
    }

  }
}
