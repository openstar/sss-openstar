package sss.openstar.tolledger

import org.scalatest.Suite
import sss.openstar.UniqueNodeIdentifier
import sss.openstar.account.AccountKeysFactoryFixture
import sss.openstar.common.builders.{KeyFactoryBuilder, VerifierFactoryBuilder}
import sss.openstar.db.DbFixture
import sss.openstar.identityledger.IdentityServiceFixture
import sss.openstar.ledger.LedgerId
import sss.openstar.schemamigration.SchemaMigrationBuilder
import sss.openstar.schemamigration.dynamic.GlobalTableNameTag
import sss.openstar.tollledger.{TollLedger, TollService}

trait TollLedgerFixture extends DbFixture
  with SchemaMigrationBuilder
  with IdentityServiceFixture
  with AccountKeysFactoryFixture
  with VerifierFactoryBuilder
  with KeyFactoryBuilder {

  self: Suite =>


  override def migrateSqlSchema(): Unit = migrateSqlSchema(GlobalTableNameTag(""))

  override protected val tablePartitionsCount: Int = 20

  migrateSqlSchema()

  def validateGoodIdentifier(s: String): String = s

  def tollService = new TollService(validateGoodIdentifier)

  implicit val tollLedgerId = LedgerId(87)

  def tollAmount: Long = 1000

  def inflationRatePerBlock: Long = 10000

  def candidates(): Set[UniqueNodeIdentifier] = Set.empty

  lazy val tollLedger = new TollLedger(
    tollService,
    identityService,
    () => candidates(),
    tollAmount,
    inflationRatePerBlock,
    tollLedgerId
  )
}
