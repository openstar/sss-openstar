package sss.openstar.tolledger.serialize

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.openstar.tollledger.TollLedger.Ops._
import sss.openstar.tollledger.TollLedger.Transfer

import scala.util.Random

/**
  * Created by alan on 2/15/16.
  */

class TollLedgerSerializationSpec extends AnyFlatSpec with Matchers {

  "A Transfer Tx" should "be parseable to bytes" in {
    val amount = Long.MaxValue
    val strings = (0 to 10) map (_ => Random.nextString(40))

    val sut = Transfer(strings, amount)
    val asBytes = sut.toBytes
    val backagain = asBytes.toTollLedgerTx
    assert(backagain == sut)
  }

}