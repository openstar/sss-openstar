package sss.openstar.tolledger

import org.scalatest.concurrent.ScalaFutures
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.ancillary.FutureOps.AwaitResult
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.account.NodeIdentityManagerFixture
import sss.openstar.account.Ops.MakeTxSigArys
import sss.openstar.common.block.BlockId
import sss.openstar.ledger.{LedgerId, LedgerItem, LedgerTxApplyError, SignedTxEntry, TxSeqApplied}
import sss.openstar.tollledger.TollLedger
import sss.openstar.tollledger.TollLedger.Ops.TollLedgerTxToBytes
import sss.openstar.tollledger.TollLedger.Transfer
import sss.openstar.tollledger.TollLedger.TollLedgerTxApplied
import sss.openstar.tollledger.TollLedger.CoinbaseTollLedgerTxApplied
import sss.openstar.tollledger.TollService.PayTollResultInfo.PaidWithBalance
import sss.openstar.tollledger.TollService.{TollServiceCredit, TollServiceResult}

import java.util


class TollLedgerSpec extends AnyFlatSpec
  with Matchers
  with ScalaFutures
  with TollLedgerFixture
  with NodeIdentityManagerFixture {


  val alanNodeDesc = addNodeIdentityToFixture("alan")
  val otherNodeDesc = addNodeIdentityToFixture("other")

  val alanStartingBalance = 100000

  val alanSignerDetails = signerDetails(alanNodeDesc.nodeIdTag, alanNodeDesc.publicKey)
  val otherSignerDetails = signerDetails(otherNodeDesc.nodeIdTag, otherNodeDesc.publicKey)
  identityService.claim(alanStartingBalance, None, alanSignerDetails, Seq.empty).dbRunSyncGet
  identityService.claim(0, None, otherSignerDetails, Seq.empty).dbRunSyncGet

  val alanNodeId = getNodeIdentity(alanNodeDesc.nodeId, alanNodeDesc.tag)
  val otherNodeId = getNodeIdentity(otherNodeDesc.nodeId, otherNodeDesc.tag)

  "A Toll ledger" should "fail if no sigs" in {
    val tx = Transfer(Seq.empty, 2000)

    val sigs: Seq[Seq[Array[Byte]]] = Seq(Seq.empty)
    val sTx = SignedTxEntry(tx.toBytes, sigs.txSig)
    val l = LedgerItem(tollLedgerId, tx.txId, sTx.toBytes)
    val bId = BlockId(2, 2)
    val result = tollLedger(l, bId)
    result match {
      case Left(l : LedgerTxApplyError) => println(l)
      case _ => fail("Should have failed")
    }
   }


  it should "fail if bad sig" in {
    val tx = Transfer(Seq.empty, 20)
    val sig = TollLedger.sign(tx, alanNodeId).await()

    val badSigs: Seq[Array[Byte]] = sig.take(2) :+ sig.last.take(sig.last.length - 1)

    val sigs: Seq[Seq[Array[Byte]]] = Seq(badSigs)
    val sTx = SignedTxEntry(tx.toBytes, sigs.txSig)

    val l = LedgerItem(tollLedgerId, tx.txId, sTx.toBytes)
    val bId = BlockId(2, 2)
    val result = tollLedger(l, bId)

    result match {
      case Left(l: LedgerTxApplyError) => println(l)
      case _ => fail("Should have failed")
    }
  }

  it should "deduct correct amount for any Tx" in {

    val tx = Transfer(Seq.empty, 20)
    val sigAry = TollLedger.sign(tx, alanNodeId).await()

    val sigs: Seq[Seq[Array[Byte]]] = Seq(sigAry)
    val sTx = SignedTxEntry(tx.toBytes, sigs.txSig)

    val l = LedgerItem(LedgerId((tollLedgerId.id + 1).toByte), tx.txId, sTx.toBytes)
    val bId = BlockId(2, 2)
    val result = tollLedger(l, bId)

    val newBalShouldBe = alanStartingBalance - tollAmount
    val txId = sTx.txId
    result match {
      case Right(TxSeqApplied(txId1, Seq(
      TollLedgerTxApplied(
      txIdBack,
      TollServiceResult(true, PaidWithBalance, `newBalShouldBe`, None)))))
        if util.Arrays.equals(txIdBack, txId) &&  util.Arrays.equals(txIdBack, txId1) => println(l)

      case err => fail(s"Should not have failed $err")
    }
  }


  it should "allow gas transfers" in {

    val bal = tollService.tollBalanceFTx(alanNodeId.id).dbRunSyncGet
    assert(bal.canDebit(bal.balance - tollAmount))
    assert(bal.balance == alanStartingBalance - tollAmount)

    val balToTransfer = bal.balance - tollAmount
    val tx = Transfer(Seq(otherNodeDesc.nodeId), balToTransfer)
    val sigAry = TollLedger.sign(tx, alanNodeId).await()

    val sigs: Seq[Seq[Array[Byte]]] = Seq(sigAry)
    val sTx = SignedTxEntry(tx.toBytes, sigs.txSig)

    val l = LedgerItem(LedgerId(tollLedgerId.id), tx.txId, sTx.toBytes)
    val bId = BlockId(2, 2)
    val result = tollLedger(l, bId)

    val otherBal = tollService.tollBalanceFTx(otherNodeId.id).dbRunSyncGet
    assert(otherBal.balance == alanStartingBalance - (tollAmount * 2))

    val newBalShouldBe = 0
    val cred = TollServiceCredit(Some(alanNodeId.id), balToTransfer, Seq(otherNodeDesc.nodeId))
    val txId = sTx.txId
      result match {
        case Right(
          TxSeqApplied(
            txId1,
            Seq(TollLedgerTxApplied(
            txIdBack,
            TollServiceResult(true, PaidWithBalance, `newBalShouldBe`, Some(`cred`))))))
          if util.Arrays.equals(txIdBack, txId) && util.Arrays.equals(txIdBack, txId1) => println(l)

      case _ => fail("Should not have failed")
    }
  }

  it should "allow coinbase" in {

    val startingAlan = tollService.tollBalanceFTx(alanNodeId.id).dbRunSyncGet
    val startingOther = tollService.tollBalanceFTx(otherNodeId.id).dbRunSyncGet

    val coinbaseAmount = 50000
    val tx = Transfer(Seq(otherNodeDesc.nodeId, alanNodeDesc.nodeId), coinbaseAmount)

    val sTx = SignedTxEntry(tx.toBytes, Seq(Seq.empty).txSig)

    val l = LedgerItem(LedgerId(tollLedgerId.id), tx.txId, sTx.toBytes)
    val bId = BlockId(2, 1)
    tollLedger(l, bId) match {
      case Right(TxSeqApplied(l.txId, Seq(CoinbaseTollLedgerTxApplied(txId, recps, amount))))
        if recps.size == 2 &&
          amount == coinbaseAmount &&
          util.Arrays.equals(txId, tx.txId) =>
      case err =>
        println(err)
        fail(s"Should not fail? $err")
    }

    val endAlan = tollService.tollBalanceFTx(alanNodeId.id).dbRunSyncGet
    val endOther = tollService.tollBalanceFTx(otherNodeId.id).dbRunSyncGet

    assert(endAlan.balance - startingAlan.balance  == coinbaseAmount)
    assert(endOther.balance - startingOther.balance == coinbaseAmount)

  }


}
