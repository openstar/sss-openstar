package sss.openstar.quorumledger

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.ancillary.ByteArrayComparisonOps
import sss.ancillary.FutureOps.AwaitResult
import sss.db.FutureTx
import sss.openstar.DummySeedBytes
import sss.openstar.LedgerTestOps._
import sss.openstar.account.Ops.MakeTxSigArys
import sss.openstar.account.{AccountKeysFactoryFixture, NodeIdTag, NodeVerifier, Signer}
import sss.openstar.chains.Chains.GlobalChainIdMask
import sss.openstar.common.block.BlockId
import sss.openstar.common.builders.{KeyFactoryBuilder, VerifierFactoryBuilder}
import sss.openstar.db.LedgerMigratedDbFixture
import sss.openstar.ledger.{LedgerId, LedgerItem, LedgerTxApplyError, SignedTxEntry}

import java.nio.charset.StandardCharsets
import scala.util.Try

/**
  * Created by alan on 4/22/16.
  */
class QuorumLedgerSpec extends AnyFlatSpec
  with Matchers with ByteArrayComparisonOps
  with LedgerMigratedDbFixture
  with VerifierFactoryBuilder
  with KeyFactoryBuilder
  with AccountKeysFactoryFixture {

  override lazy val globalChainId: GlobalChainIdMask = 99.toByte
  private val privateAcc1 = createPrivateAc()
  private val privateAcc2 =  createPrivateAc()
  private val privateAcc3 =  createPrivateAc()

  private implicit val ledgerId = LedgerId(globalChainId)

  object Services
  private var quorumService: QuorumService = _
  private var quorumLedger: QuorumLedger = _

  override def beforeAll(): Unit = {
    super.beforeAll()
    quorumService = Try(
      QuorumService.create(globalChainId, "id1"))
      .toOption
      .getOrElse(new QuorumService(globalChainId))
    quorumLedger = new QuorumLedger(globalChainId,
        quorumService,
        new AllAgreeQuorumChangePolicy,
        findAccounts)
  }

  private def makeSig(msg: Array[Byte], account: Signer): Array[Byte] = {
    account.sign(msg)
  }

  private def serialize(id: String, tag: String, sig: Array[Byte]) = {
    Seq(
      id.getBytes(StandardCharsets.UTF_8),
      tag.getBytes(StandardCharsets.UTF_8),
      sig
    )
  }

  private def findAccounts(id: String, tag: String): FutureTx[Option[NodeVerifier]] = FutureTx.unit {
    id match {
      case "id1" => Some(verifierFactory(NodeIdTag("id1"), privateAcc1.typedPublicKey))
      case "id2" => Some(verifierFactory(NodeIdTag("id2"), privateAcc2.typedPublicKey))
      case "id3" => Some(verifierFactory(NodeIdTag("id3"), privateAcc3.typedPublicKey))
      case _ => None
    }
  }

  private def makeLedgerItem(tx: QuorumLedgerTx, sigs: Seq[Seq[Array[Byte]]] = Seq()): LedgerItem = {
    val ste = SignedTxEntry(tx.toBytes, sigs.txSig)
    val le = LedgerItem(ledgerId, tx.txId, ste.toBytes)
    le
  }

  "The quorum ledger " should " have an initial owner" in {

    assert(quorumService.candidates() === Set("id1"))
  }


  it should " disallow adding non existent member " in {
    val item = makeLedgerItem(AddNodeId("nosuchid"))
    intercept[IllegalArgumentException] {
      quorumLedger(item, BlockId(0, 0)).get
    }
    assert(quorumService.candidates() === Set("id1"))
  }

  it should " require a signature when adding a second member" in {

    intercept[IllegalArgumentException] {
      quorumLedger(makeLedgerItem(AddNodeId("id2")), BlockId(0, 0)).get
    }
    //any further adds must be signed by id1
    assert(quorumService.candidates() === Set("id1"))
  }

  it should " reject an incorrect signature when adding a second member" in {

    val tx = AddNodeId("id2")
    val sig = makeSig(tx.txId, privateAcc2) // <-- using id2 (wrong)
    val sigs = Seq(serialize("id1", "defaultTag", sig))
    intercept[IllegalArgumentException] {
      quorumLedger(makeLedgerItem(tx, sigs), BlockId(0, 0)).get
    }
    assert(quorumService.candidates() === Set("id1"))
  }

  it should " accept an correct signature when adding a second member" in {

    val tx = AddNodeId("id2")
    val sig = makeSig(tx.txId, privateAcc1) // <-- using id1 (correct)
    val sigs = Seq(serialize("id1", "defaultTag", sig))

    quorumLedger(makeLedgerItem(tx, sigs), BlockId(0, 0))

    //any further adds must be signed by both id1 and id2
    assert(quorumService.candidates() === Set("id1",  "id2"))
  }

  it should " reject an add when not signed by *all* members " in {

    val tx = AddNodeId("id3")
    val sig = makeSig(tx.txId, privateAcc1) // <-- using id1 (correct)
    val sigs = Seq(serialize("id1", "defaultTag", sig))
    intercept[IllegalArgumentException] {
      quorumLedger(makeLedgerItem(tx, sigs), BlockId(0, 0)).get
    }

    assert(quorumService.candidates() === Set("id1",  "id2"))
  }

  it should " accept an add when signed by *all* members " in {

    val tx = AddNodeId("id3")
    val sig1 = makeSig(tx.txId, privateAcc1) // <-- using id1 (correct)
    val sig2 = makeSig(tx.txId, privateAcc2) // <-- using id1 (correct)
    val sigs = Seq(
      serialize("id1", "defaultTag", sig1),
      serialize("id2", "defaultTag", sig2)
    )

    quorumLedger(makeLedgerItem(tx, sigs), BlockId(0, 0))
    assert(quorumService.candidates() === Set("id1",  "id2", "id3"))
  }

  it should " reject a remove when not signed by other members " in {

    val tx = RemoveNodeId("id3")
    val sig1 = makeSig(tx.txId, privateAcc1) // <-- using id1 (correct)

    val sigs = Seq(serialize("id1", "defaultTag", sig1))

    intercept[IllegalArgumentException] {
      quorumLedger(makeLedgerItem(tx, sigs), BlockId(0, 0)).get
    }
    // No change
    assert(quorumService.candidates() === Set("id1",  "id2", "id3"))
  }

  it should " accept a remove when signed by other members " in {

    val tx = RemoveNodeId("id3")
    val sig1 = makeSig(tx.txId, privateAcc1)
    val sig2 = makeSig(tx.txId, privateAcc2)

    val sigs = Seq(
      serialize("id1", "defaultTag", sig1),
      serialize("id2", "defaultTag", sig2)
    )

    quorumLedger(makeLedgerItem(tx, sigs), BlockId(0, 0))
    assert(quorumService.candidates() === Set("id1",  "id2"))
  }

  /*it should " allow a member to resign " in {

    val tx = RemoveNodeId("id2")
    val sig = makeSig(tx.txId, privateAcc2)
    val sigs = Seq(serialize("id2", "defaultTag", sig))

    quorumLedger(makeLedgerItem(tx, sigs), 0)
    assert(quorumService.members() == Seq("id1"))
  }*/

  it should " reject another remove if no more members remain" in {

    val tx = RemoveNodeId("id2")
    val sig1 = makeSig(tx.txId, privateAcc1)
    val sigs = Seq(
      serialize("id1", "defaultTag", sig1),
    )
    quorumLedger(makeLedgerItem(tx, sigs), BlockId(0, 0))

    val txNoMore = RemoveNodeId("id1")
    val sigNoMore = makeSig(txNoMore.txId, privateAcc1)
    val sigsNoMore = Seq(serialize("id1", "defaultTag", sigNoMore))

    quorumLedger(makeLedgerItem(txNoMore, sigsNoMore), BlockId(0, 0)) should matchPattern {
      case Left(_ : LedgerTxApplyError) =>
    }

    assert(quorumService.candidates() === Set("id1"))
  }


}
