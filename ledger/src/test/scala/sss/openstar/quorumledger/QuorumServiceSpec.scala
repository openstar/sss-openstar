package sss.openstar.quorumledger


import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.ancillary.ByteArrayComparisonOps
import sss.openstar.chains.Chains.GlobalChainIdMask
import sss.openstar.db.LedgerMigratedDbFixture

import scala.util.Try

/**
  * Created by alan on 4/22/16.
  */
class QuorumServiceSpec extends AnyFlatSpec with Matchers with ByteArrayComparisonOps with LedgerMigratedDbFixture {

  override lazy val globalChainId: GlobalChainIdMask = 99.toByte
  private val ledgerId = globalChainId

  private val quorumService = Try(QuorumService.create(ledgerId)).toOption.getOrElse(new QuorumService(ledgerId))

  private val id1 = "id1"
  private val id2 = "id2"

  "The quorum service " should " allow a valid add " in {

    assert(quorumService.add("id1") == Set(id1))
    assert(quorumService.candidates() == Set(id1))
  }

  it should " disallow adding the same member twice " in {
    assert(quorumService.add(id1) == Set(id1))
    assert(quorumService.candidates() == Set(id1))
  }

  it should " allow adding another member " in {
    assert(quorumService.add(id2) == Set(id1, id2))
  }

  it should " allow removing another member " in {
    assert(quorumService.remove(id2) == Set(id1))
    assert(quorumService.candidates() == Set(id1))
  }

  it should "delete backed up state" in {
    quorumService.createCheckpoint(203).get
    val cands0 = quorumService.candidates()
    val cands1 = quorumService.add("newcandidate")
    val cands2 = quorumService.add("newcandidate")

    assert(quorumService.deleteCheckpoint(205).isFailure, "non existent should fail")
    assert(quorumService.importState(203).isSuccess)

    assert(quorumService.candidates() === Set(id1))

    val cands3 = quorumService.add("newcandidate")
    val cands4 = quorumService.add("newcandidate")
    assert(cands3 == cands4)

    quorumService.remove("newcandidate")
    val cands5 = quorumService.candidates()
    assert(cands0 == cands5)

    quorumService.createCheckpoint(203).get

    assert(quorumService.deleteCheckpoint(203).isSuccess)
  }
}
