package sss.openstar.identityledger.serialize

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import scorex.crypto.signatures.PublicKey
import sss.ancillary.ByteArrayComparisonOps
import sss.openstar.DummySeedBytes
import sss.openstar.identityledger._
import sss.openstar.util.{HashedSecret, IntBitSet}
import serialize.Serializers._
import sss.ancillary.Serialize.SerializerOps._
import sss.openstar.account.{AccountKeysFactoryFixture, NodeIdTag, TypedPublicKey}
import sss.openstar.account.NodeSigner.{InteractionType, KeyType, SecurityLevel, SignerType}

import java.nio.charset.StandardCharsets

/**
  * Created by alan on 6/7/16.
  */
class SerializeSpec extends AnyFlatSpec with Matchers with ByteArrayComparisonOps
  with SignerVerifierDetailsFixture
  with AccountKeysFactoryFixture {


  "An IdentityRole Attribute" should "serialize and deserialize" in {

    val bitsset = IntBitSet(0).set(1).set(2).set(0)

    val attr = IdentityRoleAttribute(bitsset.value)
    val sut = SetAttribute("someone", attr)
    val asBytes = sut.serialize
    val SetAttribute(someone, attrRehydrated: IdentityRoleAttribute) = asBytes.deserialize[SetAttribute]
    assert(someone == "someone")
    assert(attrRehydrated.bitfield.value == bitsset.value)
    assert(attrRehydrated.bitfield.get(0))
    assert(attrRehydrated.bitfield.get(1))
    assert(attrRehydrated.bitfield.get(2))
    assert(!attrRehydrated.bitfield.get(3))
    assert(!attrRehydrated.bitfield.get(31))
  }

  "A Bearer Auth Attribute" should "serialize and deserialize" in {

    val bytes = DummySeedBytes(32)
    val hashed = HashedSecret.hashSecret(bytes)
    val attr = BearerAuthorizationAttribute(hashed)
    val sut = SetAttribute("someone", attr)
    val asBytes = sut.serialize
    val SetAttribute(someone, attrRehydrated: BearerAuthorizationAttribute) = asBytes.deserialize[SetAttribute]
    assert(someone == "someone")
    assert(attrRehydrated.hashOfSecret.matches(bytes))
    assert(attrRehydrated == attr)
  }

  "A URL SetAttribute" should "serialize and deserialize" in {

    val attr = ProviderAttachmentUrlAttribute("https://crazytown.com/monkeybutz./")
    val sut = SetAttribute("someone", attr)
    val asBytes = sut.serialize
    val rehydrated = asBytes.deserialize[SetAttribute]
    assert(rehydrated == sut)
  }

  "A Charge SetAttribute" should "serialize and deserialize" in {

    val attr = ProviderChargeAttribute(200)
    val sut = SetAttribute("someone", attr)
    val asBytes = sut.serialize
    val rehydrated = asBytes.deserialize[SetAttribute]
    assert(rehydrated == sut)
  }

  "A RemoveAttribute" should "serialize and deserialize" in {
    val attr = ProviderChargeAttribute(200)
    val sut = RemoveAttribute("someone", attr.category)
    val asBytes = sut.serialize
    val rehydrated = asBytes.deserialize[RemoveAttribute]
    assert(rehydrated == sut)
  }

  "A URL SetOwnedAttribute" should "serialize and deserialize" in {

    val attr = ProviderAttachmentUrlAttribute("https://crazytown.com/monkeybutz./")
    val sut = SetOwnedAttribute("owner", "someone", attr)
    val asBytes = sut.serialize
    val rehydrated = asBytes.deserialize[SetOwnedAttribute]
    assert(rehydrated == sut)
  }

  "A Charge SetOwnedAttribute" should "serialize and deserialize" in {

    val attr = ProviderChargeAttribute(200)
    val sut = SetOwnedAttribute("owner", "someone", attr)
    val asBytes = sut.serialize
    val rehydrated = asBytes.deserialize[SetOwnedAttribute]
    assert(rehydrated == sut)
  }

  "A RemoveOwnedAttribute" should "serialize and deserialize" in {
    val attr = ProviderChargeAttribute(200)
    val sut = RemoveOwnedAttribute("owner", "someone", attr.category)
    val asBytes = sut.serialize
    val rehydrated = asBytes.deserialize[RemoveOwnedAttribute]
    assert(rehydrated == sut)
  }

  "A SetPaywall " should "serialize and deserialize " in {
    val setPaywall = SetPaywall("idenairy", "catesdfsf", Some(9999))
    val asBytes = setPaywall.serialize
    val hydrated = asBytes.deserialize[SetPaywall]
    assert(setPaywall == hydrated)
    assert(setPaywall.uniqueMessage == hydrated.uniqueMessage)
    assert(setPaywall.hashCode == hydrated.hashCode)
  }

  "A Claim" should " serialize and deserialize " in {

    import BackwardsCompatibilityOps.PublicKeyOps

    val seqDetails =
      PublicKey(DummySeedBytes.randomSeed(56)).toDetails(s"tagi")


    val claim = Claim("foo", "idenairy", seqDetails)
    val asBytes = claim.serialize
    val hydrated = asBytes.deserialize[Claim]
    assert(claim.identity == hydrated.identity)
    assert(claim.authorizedOwner == hydrated.authorizedOwner)
    assert(claim.uniqueMessage == hydrated.uniqueMessage)

    assert(claim == hydrated)
    assert(claim.hashCode == hydrated.hashCode)
  }


  "A Claim0 (outdated Claim version)" should " serialize and deserialize " in {
    val claim = Claim0("foo", "idenairy", PublicKey(DummySeedBytes.randomSeed(56)))
    val asBytes = claim.serialize
    val hydrated = asBytes.deserialize[Claim0]
    assert(claim.identity == hydrated.identity)
    assert(claim.authorizedOwner == hydrated.authorizedOwner)
    assert(claim.uniqueMessage == hydrated.uniqueMessage)
    assert(claim.pKey isSame hydrated.pKey)
    assert(claim == hydrated)
    assert(claim.hashCode == hydrated.hashCode)
  }

  "A Link Rescuer " should " serialize and deserialize " in {
    val test = LinkRescuer("idenairy", "asdalsdjalsdjalid")
    val asBytes = test.serialize
    val hydrated = asBytes.deserialize[LinkRescuer]
    assert(test.identity == hydrated.identity)
    assert(test.uniqueMessage == hydrated.uniqueMessage)
    assert(test.rescuer == hydrated.rescuer)
    assert(test == hydrated)
    assert(test.hashCode == hydrated.hashCode)
  }


  "A Link  " should " serialize and deserialize " in {
    val test = Link(svSignerVerifierDetails)
    val asBytes = test.serialize
    val hydrated = asBytes.deserialize[Link]
    assert(test.uniqueMessage == hydrated.uniqueMessage)
    assert(test == hydrated)
    assert(test.hashCode == hydrated.hashCode)
  }


  "A Rescue  " should " serialize and deserialize " in {

    val test = Rescue("rescuerasdasd", svSignerVerifierDetails)
    val asBytes = test.serialize
    val hydrated = asBytes.deserialize[Rescue]
    assert(test.uniqueMessage == hydrated.uniqueMessage)
    assert(test.rescuer == hydrated.rescuer)
    assert(test == hydrated)
    assert(test.hashCode == hydrated.hashCode)
  }

  "An Unlink by Key " should " serialize and deserialize " in {
    val test = UnLinkByKey("rescuerasdasd", PublicKey(DummySeedBytes.randomSeed(56)))

    val asBytes = test.serialize
    val hydrated = asBytes.deserialize[UnLinkByKey]
    assert(test.identity == hydrated.identity)
    assert(test.pKey isSame hydrated.pKey)
    assert(test.uniqueMessage == hydrated.uniqueMessage)
    assert(test == hydrated)
    assert(test.hashCode == hydrated.hashCode)
  }

  "An UnLinkRescuer " should " serialize and deserialize " in {
    val test = UnLinkRescuer("rescuerasdasd", "asdfadsasdasdadsasd")
    val asBytes = test.serialize
    val hydrated = asBytes.deserialize[UnLinkRescuer]
    assert(test.identity == hydrated.identity)
    assert(test.rescuer == hydrated.rescuer)
    assert(test.uniqueMessage == hydrated.uniqueMessage)
    assert(test == hydrated)
    assert(test.hashCode == hydrated.hashCode)
  }

  "An UnLink " should " serialize and deserialize " in {
    val test = UnLink("rescuerasdasd", "asdfadsasdasdadsasd")
    val asBytes = test.serialize
    val hydrated = asBytes.deserialize[UnLink]
    assert(test.identity == hydrated.identity)
    assert(test.tag == hydrated.tag)
    assert(test.uniqueMessage == hydrated.uniqueMessage)
    assert(test == hydrated)
    assert(test.hashCode == hydrated.hashCode)
  }

  "A charge provider" should "ser and deser" in {
    val sut = ProviderChargeAttribute(34)
    val bs = sut.serialize
    val rehydrated = bs.toLedgerAttribute
    assert(rehydrated === sut)
  }

  "A Delete id" should "ser and deser" in {
    val sut = Delete("sdfsdfsdfsdf", "fffffff")
    val bs = sut.serialize
    val rehydrated = bs.toIdentityLedgerTx
    assert(rehydrated === sut)
  }

  "An Generic Attribute Category" should "throw an exception if the content lies out side the range [-128, -1]" in {

    val cause = intercept[IllegalArgumentException](GenericAttributeCategory(23))

    assert(cause.getMessage == "requirement failed: Generic attribute category should lies between the range [-128, -1], but found 23.")
    assert(GenericAttributeCategory(-23).value == -23)
  }

  "An Generic Attribute" should "serialize and deserialize" in {

    val sampleData = "Test Message!"

    val cat = GenericAttributeCategory(-34)
    val attr = GenericAttribute(sampleData.getBytes(StandardCharsets.UTF_8), cat)
    val sut = SetAttribute("someone", attr)
    val asBytes = sut.serialize
    val SetAttribute(someone, attrRehydrated: GenericAttribute) = asBytes.deserialize[SetAttribute]
    assert(someone == "someone")
    assert(new String(attrRehydrated.content) == sampleData)
    assert(attrRehydrated.category == cat)
  }

  "An Generic Attribute" should "throw an exception if there are more than 1024 bytes." in {

    val inCorrectTestData = scala.util.Random.nextBytes(1025)
    val correctTestData = scala.util.Random.nextBytes(1024)
    val cat = GenericAttributeCategory(-34)
    val exception = intercept[IllegalArgumentException](GenericAttribute(inCorrectTestData, cat))
    assert(exception.getMessage == s"requirement failed: Content length should not exceed 1024 bytes but found ${inCorrectTestData.length}.")
    assert(GenericAttribute(correctTestData, cat).content sameElements correctTestData)
  }
}
