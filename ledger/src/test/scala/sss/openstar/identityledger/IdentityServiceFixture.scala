package sss.openstar.identityledger

import org.scalatest.Suite
import scorex.crypto.signatures.PublicKey
import sss.openstar.account.NodeSigner.{InteractionType, SecurityLevel, SignerType}
import sss.openstar.account.{AccountKeysFactoryFixture, NodeIdTag, SignerInteractionTypes, SignerSecurityLevels, TypedPublicKey}
import sss.openstar.account.impl.EncryptedKeyFileAccountSignerCreator
import sss.openstar.common.builders.VerifierFactoryBuilder
import sss.openstar.crypto.keypairs.Curve25519AccountKeys
import sss.openstar.db.Builder.RequireDb

trait IdentityServiceFixture {
  self: Suite with RequireDb with AccountKeysFactoryFixture with VerifierFactoryBuilder =>

  val identityService: IdentityService = IdentityService()


  def signerDetails(
                     nodeIdTag: NodeIdTag,
                     publicKey: PublicKey,
                     externalKeyIdOpt: Option[Array[Byte]] = None,
                     securityLevel: SecurityLevel = SignerSecurityLevels.SecurityLevel1,
                     signerType: SignerType = EncryptedKeyFileAccountSignerCreator.SignerType,
                     interactionTypes: InteractionType = SignerInteractionTypes.AutomaticSignerInteractionType
                   ): SignerVerifierDetails = signerDetails2(
    nodeIdTag,
    TypedPublicKey(publicKey, Curve25519AccountKeys.keysType),
    externalKeyIdOpt,
    securityLevel,
    signerType,
    interactionTypes
  )

  def signerDetails2(
                     nodeIdTag: NodeIdTag,
                     typedPublicKey: TypedPublicKey,
                     externalKeyIdOpt: Option[Array[Byte]] = None,
                     securityLevel: SecurityLevel = SignerSecurityLevels.SecurityLevel1,
                     signerType: SignerType = EncryptedKeyFileAccountSignerCreator.SignerType,
                     interactionType: InteractionType = SignerInteractionTypes.AutomaticSignerInteractionType
                   ): SignerVerifierDetails = SignerVerifierDetails(
    nodeIdTag,
    externalKeyIdOpt,
    typedPublicKey,
    securityLevel,
    signerType,
    interactionType
  )
}
