package sss.openstar.identityledger

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.openstar.common.result.ErrorMessage

import java.util.Random


/**
  * Created by alan on 2/15/16.
  */
class IdentifierSpec extends AnyFlatSpec with Matchers {

  val sut = new DefaultIdentifier()

  "DefaultIdentifier" should "prevent uppercase" in {
    sut.toIdentifier("CAPS") should matchPattern {
      case Left(ErrorMessage(Identifier.ErrorMessageCode, _, _)) =>
    }
  }

  it should "allow simple strings" in {
    val id = "abcdefgh"

    sut.toIdentifier(id) should matchPattern {
      case Right(`id`) =>
    }
  }

  it should "disallow spaces" in {
    sut.toIdentifier("caps ") should matchPattern {
      case Left(ErrorMessage(Identifier.ErrorMessageCode, _, _)) =>
    }
  }

  it should "allow numbers" in {
    val id = "abcdefgh12"
    sut.toIdentifier(id) should matchPattern {
      case Right(`id`) =>
    }
  }

  it should "prevent special characters" in {
    val id = "id!#"

    val t = sut.toIdentifier("R !")
    sut.toIdentifier(id) should matchPattern {
      case Left(ErrorMessage(Identifier.ErrorMessageCode, _, _)) =>
    }
  }

  it should "prevent long names" in {
    val id: String = List.fill(MaxLenOfIdentity + 1)("a").toString()
    sut.toIdentifier(id) should matchPattern {
      case Left(ErrorMessage(Identifier.ErrorMessageCode, _, _)) =>
    }
  }
}
