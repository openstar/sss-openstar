package sss.openstar.identityledger

import scorex.crypto.signatures.PublicKey
import sss.openstar.{DummySeedBytes, UniqueNodeIdentifier}
import sss.openstar.account.NodeSigner.{InteractionType, KeyType, SecurityLevel, SignerType}
import sss.openstar.account.{AccountKeysFactoryFixture, NodeIdTag, TypedPublicKey}


trait SignerVerifierDetailsFixture {

  self: AccountKeysFactoryFixture =>

  val svNodeIdTag: NodeIdTag = NodeIdTag("user", "tag")
  val svExternalKeyIdOpt: Option[Array[Byte]] = Some(DummySeedBytes(130))
  val svSignerDetailsNewKeyAcc = createPrivateAc()
  val svPubKey: PublicKey = svSignerDetailsNewKeyAcc.typedPublicKey.publicKey
  val svKeyType: KeyType = svSignerDetailsNewKeyAcc.keyType
  val svTypedPublicKey: TypedPublicKey = TypedPublicKey(
    svPubKey,
    svKeyType
  )
  val svSecurityLevel: SecurityLevel = 4
  val svSignerType: SignerType = "HLOOOESigner"
  val svInteractionType: InteractionType = "Manual"
  val svSignerVerifierDetails: SignerVerifierDetails = SignerVerifierDetails(
    svNodeIdTag,
    svExternalKeyIdOpt,
    svTypedPublicKey,
    svSecurityLevel,
    svSignerType,
    svInteractionType
  )

  def signerDetailsWith(
                          nodeId: UniqueNodeIdentifier = svSignerVerifierDetails.nodeIdTag.nodeId,
                          tag: String = svSignerVerifierDetails.nodeIdTag.tag,
                          pKey: PublicKey = svSignerVerifierDetails.typedPublicKey.publicKey
                       ): SignerVerifierDetails = svSignerVerifierDetails
    .copy(
      nodeIdTag = NodeIdTag(nodeId, tag),
      typedPublicKey = svSignerVerifierDetails.typedPublicKey.copy(publicKey = pKey)
    )
}
