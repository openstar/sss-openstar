package sss.openstar.identityledger

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import scorex.crypto.signatures.PublicKey
import sss.ancillary.ByteArrayComparisonOps
import sss.db.FutureTx
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.account.Ops.MakeTxSigArys
import sss.openstar.account.{AccountKeysFactoryFixture, NodeIdTag, Signer}
import sss.openstar.common.block.BlockId
import sss.openstar.common.builders.{KeyFactoryBuilder, VerifierFactoryBuilder}
import sss.openstar.db.LedgerMigratedDbFixture
import sss.openstar.identityledger.IdentityRolesOps.ReservedIdentityRolesAccessor
import sss.openstar.ledger.{LedgerId, LedgerItem, LedgerTxApplyError, SignedTxEntry}
import sss.openstar.util.IntBitSet

import java.nio.charset.StandardCharsets

/**
  * Created by alan on 4/22/16.
  */
class IdentityLedgerSpec extends AnyFlatSpec with Matchers with ByteArrayComparisonOps
  with LedgerMigratedDbFixture
  with IdentityServiceFixture
  with VerifierFactoryBuilder
  with KeyFactoryBuilder
  with AccountKeysFactoryFixture
  with SignerVerifierDetailsFixture {

  import sss.openstar.LedgerTestOps.EitherToTry

  val myIdentity = "intothelight"
  val rescuerIdentity = "someguy"
  val authorizedOwnerIdentity = "owner"
  val ownerAccount = createPrivateAc()
  val privateAcc1 = createPrivateAc()
  val privateAcc2 = createPrivateAc()
  val privateAcc3 = createPrivateAc()
  val ownerKey = ownerAccount.typedPublicKey.publicKey
  val key1 = privateAcc1.typedPublicKey.publicKey
  val key2 = privateAcc2.typedPublicKey.publicKey
  val key3 = privateAcc3.typedPublicKey.publicKey
  implicit val ledgerId = LedgerId(99.toByte)
  val lookup = (_: LedgerId) => FutureTx.lazyUnit(Seq(authorizedOwnerIdentity))
  val identityLedger = new IdentityLedger(identityService, lookup)
  val tagForKey2 = "sometag"
  val ledgerAttribute = IdentityRoleAttribute(IntBitSet(0).set(31))
  val selfOwnedLedgerAttribute = IdentityRoleAttribute().setSelfSovereign()

  import sss.openstar.identityledger.BackwardsCompatibilityOps.PublicKeyOps

  override def beforeAll(): Unit = {
    super.beforeAll()
    identityService.claim(ownerKey.toDetails(authorizedOwnerIdentity)).dbRunSyncGet
  }

  private def expectErrorWithMessage(fun: => Any, errorMsg: String) =
    intercept[IllegalArgumentException](fun).getMessage should include (errorMsg)


  private def makeClaim(
                         authorizedOwner: String = authorizedOwnerIdentity,
                         identity: String = myIdentity,
                         key: PublicKey = key1,
                         authorizedOwnerAccount: Signer = ownerAccount,
                         emptySignature: Boolean = false
                       ): LedgerItem = {
    val claim = Claim(authorizedOwner, identity, key.toDetails(identity), IdentityRoleAttribute().setNetworkAuditor())
    val sig = if (emptySignature) Seq.empty else Seq(Seq(identity.getBytes(StandardCharsets.UTF_8), identityService.defaultTag.getBytes, authorizedOwnerAccount.sign(claim.txId)))
    val ste = SignedTxEntry(claim.toBytes, sig.txSig)
    val le = LedgerItem(ledgerId, claim.txId, ste.toBytes)
    le
  }

  private def makeSetSelfOwnedAttribute(account: Signer, set: Boolean): LedgerItem = {

    val setAttribute = SetAttribute(myIdentity,
      if(set) selfOwnedLedgerAttribute.setSelfSovereign()
      else selfOwnedLedgerAttribute.unSetSelfSovereign()
    )
    val sig = Seq(Seq(Array.emptyByteArray, identityService.defaultTag.getBytes, account.sign(setAttribute.txId)))
    val ste = SignedTxEntry(setAttribute.toBytes, sig.txSig)
    val le = LedgerItem(ledgerId, setAttribute.txId, ste.toBytes)
    le
  }

  private def makeSetOwnedAttribute(emptySignature: Boolean = false, authorizedOwnerAccount: Signer = ownerAccount): LedgerItem = {
    val setOwnedAttribute = SetOwnedAttribute(authorizedOwnerIdentity, myIdentity, ledgerAttribute)
    val sig = if (emptySignature) Seq.empty else Seq(Seq(Array.emptyByteArray, identityService.defaultTag.getBytes, authorizedOwnerAccount.sign(setOwnedAttribute.txId)))
    val ste = SignedTxEntry(setOwnedAttribute.toBytes, sig.txSig)
    val le = LedgerItem(ledgerId, setOwnedAttribute.txId, ste.toBytes)
    le
  }

  private def makeRemoveOwnedAttribute(emptySignature: Boolean = false, authorizedOwnerAccount: Signer = ownerAccount): LedgerItem = {
    val removeOwnedAttribute = RemoveOwnedAttribute(authorizedOwnerIdentity, myIdentity, ledgerAttribute.category)
    val sig = if (emptySignature) Seq.empty else Seq(Seq(Array.emptyByteArray, identityService.defaultTag.getBytes, authorizedOwnerAccount.sign(removeOwnedAttribute.txId)))
    val ste = SignedTxEntry(removeOwnedAttribute.toBytes, sig.txSig)
    val le = LedgerItem(ledgerId, removeOwnedAttribute.txId, ste.toBytes)
    le
  }

  private def makeDelete(emptySignature: Boolean = false, authorizedOwnerAccount: Signer = ownerAccount): LedgerItem = {
    val deleteTx = Delete(authorizedOwnerIdentity, myIdentity)
    val sig = if (emptySignature) Seq.empty else Seq(Seq(Array.emptyByteArray, identityService.defaultTag.getBytes, authorizedOwnerAccount.sign(deleteTx.txId)))
    val ste = SignedTxEntry(deleteTx.toBytes, sig.txSig)
    val le = LedgerItem(ledgerId, deleteTx.txId, ste.toBytes)
    le
  }

  "The identity ledger" should "be able to claim an identity to a key" in {

    assert(identityService.accounts(myIdentity).isEmpty)
    val le = makeClaim()
    identityLedger(le, BlockId(0, 0)).get

    assert(identityService.account(myIdentity).typedPublicKey.publicKey isSame key1)
  }

  it should "fail claim if no owners" in {
    val emptyOwnersIdentityLedger = new IdentityLedger(identityService, (_: LedgerId) => FutureTx.unit(Seq.empty))
    val le = makeClaim()
    expectErrorWithMessage(emptyOwnersIdentityLedger(le, BlockId(0, 0)).get, s"Could not find owners for this $ledgerId")
  }

  it should "fail claim if unauthorized owner" in {
    val wrongOwnersIdentityLedger = new IdentityLedger(identityService, (_: LedgerId) => FutureTx.unit(Seq("foo")))
    val le = makeClaim()
    expectErrorWithMessage(wrongOwnersIdentityLedger(le, BlockId(0, 0)).get, s"Unauthorized owner")
  }

  it should "fail claim if owner signature not match" in {
    val le = makeClaim(authorizedOwnerAccount = privateAcc1)
    expectErrorWithMessage(identityLedger(le, BlockId(0, 0)).get, "The signature does not match the txId")
  }

  it should "fail claim if not signed" in {
    val le = makeClaim(emptySignature = true)
    expectErrorWithMessage(identityLedger(le, BlockId(0, 0)).get, "A id/tag/sig pair must be provided to continue")
  }

  it should "prevent a second claim to the same identity" in {

    val le = makeClaim()
    expectErrorWithMessage(identityLedger(le, BlockId(0, 0)).get, s"Identifier $myIdentity already taken")
  }

  it should "be able to link a second key to an identity" in {

    val signerVerifierDetails = svSignerVerifierDetails
      .copy(typedPublicKey = svTypedPublicKey.copy(publicKey = key2),
        nodeIdTag = svNodeIdTag.copy(nodeId = myIdentity, tag = tagForKey2)
      )


    val link = Link(signerVerifierDetails)
    val sig = privateAcc1.sign(link.txId)

    val sigs: Seq[Seq[Array[Byte]]] = Seq(Seq(
      myIdentity.getBytes(StandardCharsets.UTF_8),
      NodeIdTag.defaultTag.getBytes(StandardCharsets.UTF_8),
      sig))

    val ste = SignedTxEntry(link.toBytes, sigs.txSig)
    val le = LedgerItem(ledgerId, link.txId, ste.toBytes)
    identityLedger(le, BlockId(0, 0)).get

    val pKey = identityService.account(myIdentity, tagForKey2)
    assert(pKey.typedPublicKey.publicKey isSame key2)

  }

  it should "be able to unlink a key by key from an identity" in {

    val unlink = UnLinkByKey(myIdentity, key1)
    val sig = privateAcc1.sign(unlink.txId)
    val sigs: Seq[Seq[Array[Byte]]] = Seq(Seq(myIdentity.getBytes(StandardCharsets.UTF_8), identityService.defaultTag.getBytes, sig))
    val ste = SignedTxEntry(unlink.toBytes, sigs.txSig)
    val le = LedgerItem(ledgerId, unlink.txId, ste.toBytes)
    identityLedger(le, BlockId(0, 0)).get

    assert(identityService.accountOpt(myIdentity).isEmpty)
    assert(identityService.accountOpt(myIdentity, tagForKey2).isDefined)

  }


  it should "prevent unlink from a previously unlinked key!" in {

    val unlink = UnLink(myIdentity, tagForKey2)
    val sig = privateAcc1.sign(unlink.txId)
    val sigs: Seq[Seq[Array[Byte]]] = Seq(Seq(myIdentity.getBytes(StandardCharsets.UTF_8),identityService.defaultTag.getBytes, sig))
    val ste = SignedTxEntry(unlink.toBytes, sigs.txSig)
    val le = LedgerItem(ledgerId, unlink.txId, ste.toBytes)
    expectErrorWithMessage(identityLedger(le, BlockId(0, 0)).get, s"Could not find an account for identity/tag pair")
  }

  it should "allow unlink from a valid key" in {

    val unlink = UnLink(myIdentity, tagForKey2)
    val sig = privateAcc2.sign(unlink.txId)
    val sigs: Seq[Seq[Array[Byte]]] = Seq(Seq(myIdentity.getBytes(StandardCharsets.UTF_8),tagForKey2.getBytes, sig))
    val ste = SignedTxEntry(unlink.toBytes, sigs.txSig)
    val le = LedgerItem(ledgerId, unlink.txId, ste.toBytes)
    identityLedger(le, BlockId(0, 0)).get
    assert(identityService.accountOpt(myIdentity, tagForKey2).isEmpty)

    db.table("identity_tbl").map(println)
    db.table("key_tbl").map(println)

  }

  it should "allow reclaiming an identity with no linked keys" in {

    val le = makeClaim()
    identityLedger(le, BlockId(0, 0)).get
    assert(identityService.account(myIdentity).typedPublicKey.publicKey isSame key1)
  }

  it should "allow adding a rescuer" in {

    identityLedger(makeClaim(authorizedOwnerIdentity, rescuerIdentity, key2), BlockId(0, 0))
    assert(identityService.accountOpt(rescuerIdentity).isDefined)
    val link = LinkRescuer(rescuerIdentity, myIdentity)
    val sig = privateAcc1.sign(link.txId)
    val sigs: Seq[Seq[Array[Byte]]] = Seq(Seq(myIdentity.getBytes(StandardCharsets.UTF_8), identityService.defaultTag.getBytes, sig))
    val ste = SignedTxEntry(link.toBytes, sigs.txSig)
    val le = LedgerItem(ledgerId, link.txId, ste.toBytes)
    identityLedger(le, BlockId(0, 0)).get

    assert(identityService.rescuers(myIdentity).contains(rescuerIdentity))
  }

  it should "prevent adding a self signed rescuer" in {

    val link = LinkRescuer(rescuerIdentity, myIdentity)
    val sig = privateAcc2.sign(link.txId)
    val sigs: Seq[Seq[Array[Byte]]] = Seq(Seq(myIdentity.getBytes(StandardCharsets.UTF_8),identityService.defaultTag.getBytes, sig))
    val ste = SignedTxEntry(link.toBytes, sigs.txSig)
    val le = LedgerItem(ledgerId, link.txId, ste.toBytes)
    expectErrorWithMessage(identityLedger(le, BlockId(0, 0)).get, "The signature does not match the txId")
  }


  it should "allow rescue from an approved rescuer (followed by removal of rescuer)" in {

    val rescue = Rescue(rescuerIdentity, signerDetailsWith(nodeId = myIdentity, tag = "rescued"))
    val sig = privateAcc2.sign(rescue.txId)
    val sigs: Seq[Seq[Array[Byte]]] = Seq(Seq(myIdentity.getBytes(StandardCharsets.UTF_8),identityService.defaultTag.getBytes, sig))
    val ste = SignedTxEntry(rescue.toBytes, sigs.txSig)
    val le = LedgerItem(ledgerId, rescue.txId, ste.toBytes)
    identityLedger(le, BlockId(0, 0)).get

    {
      // Now check it by removing the rescuer with the new rescue key :D

      val unLink = UnLinkRescuer(rescuerIdentity, myIdentity)
      val sig = svSignerDetailsNewKeyAcc.sign(unLink.txId)

      val sigs: Seq[Seq[Array[Byte]]] = Seq(Seq(
        rescuerIdentity.getBytes(StandardCharsets.UTF_8),
        "rescued".getBytes(StandardCharsets.UTF_8), sig)
      )

      val ste = SignedTxEntry(unLink.toBytes, sigs.txSig)
      val le = LedgerItem(ledgerId, unLink.txId, ste.toBytes)
      identityLedger(le, BlockId(0, 0)).get
    }
    assert(!identityService.rescuers(myIdentity).contains(rescuerIdentity))
  }


  val randomerIdentity = "randomer"

  it should "prevent rescue from an unapproved rescuer " in {

    identityLedger(makeClaim(authorizedOwnerIdentity, randomerIdentity, key3), BlockId(0, 0)).get
    val rescue = Rescue(randomerIdentity, signerDetailsWith(myIdentity))
    val sig = privateAcc2.sign(rescue.txId)
    val sigs: Seq[Seq[Array[Byte]]] = Seq(Seq(myIdentity.getBytes(StandardCharsets.UTF_8),identityService.defaultTag.getBytes, sig))
    val ste = SignedTxEntry(rescue.toBytes, sigs.txSig)
    val le = LedgerItem(ledgerId, rescue.txId, ste.toBytes)
    val applied = identityLedger(le, BlockId(0, 0))
    applied should matchPattern { case Left(_:LedgerTxApplyError) => }

  }

  it should "allow adding a secondary message store" in {

    val addStore = AddMessageStore(randomerIdentity, rescuerIdentity)
    val sig = privateAcc3.sign(addStore.txId)
    val sigs: Seq[Seq[Array[Byte]]] = Seq(Seq(Array.emptyByteArray, NodeIdTag.defaultTag.getBytes, sig))
    val ste = SignedTxEntry(addStore.toBytes, sigs.txSig)
    val le = LedgerItem(ledgerId, addStore.txId, ste.toBytes)

    val before = IdentityService().messageStores(randomerIdentity).size
    identityLedger(le, BlockId(0, 0)).get

    assert(IdentityService().messageStores(randomerIdentity).size === before + 1)
    assert(IdentityService().messageStores(randomerIdentity).contains( rescuerIdentity))

  }

  it should "process Add if sig is wrong" in {

    val addStore = AddMessageStore(randomerIdentity, rescuerIdentity)
    val sig = privateAcc1.sign(addStore.txId)
    val sigs: Seq[Seq[Array[Byte]]] = Seq(Seq(Array.emptyByteArray, identityService.defaultTag.getBytes, sig))
    val ste = SignedTxEntry(addStore.toBytes, sigs.txSig)
    val le = LedgerItem(ledgerId, addStore.txId, ste.toBytes)

    expectErrorWithMessage(identityLedger(le, BlockId(0, 0)).get, "The signature does not match the txId")
  }


  it should "allow removing a secondary message store" in {

    val before = IdentityService().messageStores(randomerIdentity).size
    val addStore = RemoveMessageStore(randomerIdentity, rescuerIdentity)
    val sig = privateAcc3.sign(addStore.txId)
    val sigs: Seq[Seq[Array[Byte]]] = Seq(Seq(Array.emptyByteArray, identityService.defaultTag.getBytes, sig))
    val ste = SignedTxEntry(addStore.toBytes, sigs.txSig)
    val le = LedgerItem(ledgerId, addStore.txId, ste.toBytes)

    identityLedger(le, BlockId(0, 0)).get

    assert(IdentityService().messageStores(randomerIdentity).size === before - 1)

  }

  it should "fail to process Remove if the sig is wrong" in {

    val addStore = RemoveMessageStore(randomerIdentity, rescuerIdentity)
    val sig = privateAcc1.sign(addStore.txId)
    val sigs: Seq[Seq[Array[Byte]]] = Seq(Seq(Array.emptyByteArray, identityService.defaultTag.getBytes, sig))
    val ste = SignedTxEntry(addStore.toBytes, sigs.txSig)
    val le = LedgerItem(ledgerId, addStore.txId, ste.toBytes)

    expectErrorWithMessage(identityLedger(le, BlockId(0, 0)).get, "The signature does not match the txId")

  }

  it should "allow adding a paywall" in {
    val addPaywall = SetPaywall(randomerIdentity, "category", Some(1000))
    val sig = privateAcc3.sign(addPaywall.txId)
    val sigs: Seq[Seq[Array[Byte]]] = Seq(Seq(Array.emptyByteArray, identityService.defaultTag.getBytes, sig))
    val ste = SignedTxEntry(addPaywall.toBytes, sigs.txSig)
    val le = LedgerItem(ledgerId, addPaywall.txId, ste.toBytes)

    identityLedger(le, BlockId(0, 0)).get

    assert(IdentityService().paywalls(randomerIdentity)
      .contains(
        Paywall(addPaywall.identity, addPaywall.category, addPaywall.amount))
    )
  }

  it should "set owned attribute" in {
    assert(!IdentityService().listOwnedAttributes(myIdentity).contains(ledgerAttribute))
    val le = makeSetOwnedAttribute()

    identityLedger(le, BlockId(0, 0)).get

    assert(IdentityService().listOwnedAttributes(myIdentity).contains(ledgerAttribute))
  }

  it should "fail to set owned attribute, if no owners" in {
    val emptyOwnersIdentityLedger = new IdentityLedger(identityService, (_: LedgerId) => FutureTx.unit(Seq.empty))
    val le = makeSetOwnedAttribute()
    expectErrorWithMessage(emptyOwnersIdentityLedger(le, BlockId(0, 0)).get, s"Could not find owners for this $ledgerId")
  }

  it should "fail to set owned attribute, if unauthorized owner" in {
    val wrongOwnersIdentityLedger = new IdentityLedger(identityService, (_: LedgerId) => FutureTx.unit(Seq("foo")))
    val le = makeSetOwnedAttribute()
    expectErrorWithMessage(wrongOwnersIdentityLedger(le, BlockId(0, 0)).get, s"Unauthorized owner")
  }

  it should "fail to set owned attribute, if signatures not match" in {
    val le = makeSetOwnedAttribute(authorizedOwnerAccount = privateAcc1)
    expectErrorWithMessage(identityLedger(le, BlockId(0, 0)).get, "The signature does not match the txId")
  }

  it should "fail to set owned attribute, if not signed" in {
    val le = makeSetOwnedAttribute(emptySignature = true)
    expectErrorWithMessage(identityLedger(le, BlockId(0, 0)).get, "A id/tag/sig pair must be provided to continue")
  }

  it should "remove owned attribute" in {
    assert(IdentityService().listOwnedAttributes(myIdentity).nonEmpty)
    val le = makeRemoveOwnedAttribute()
    identityLedger(le, BlockId(0, 0)).get
    assert(IdentityService().listOwnedAttributes(myIdentity).isEmpty)
  }

  it should "fail to remove owned attribute, if no owners" in {
    val emptyOwnersIdentityLedger = new IdentityLedger(identityService, (_: LedgerId) => FutureTx.unit(Seq.empty))
    val le = makeRemoveOwnedAttribute()
    expectErrorWithMessage(emptyOwnersIdentityLedger(le, BlockId(0, 0)).get, s"Could not find owners for this $ledgerId")
  }

  it should "fail to remove owned attribute, if unauthorized owner" in {
    val wrongOwnersIdentityLedger = new IdentityLedger(identityService, (_: LedgerId) => FutureTx.unit(Seq("foo")))
    val le = makeRemoveOwnedAttribute()
    expectErrorWithMessage(wrongOwnersIdentityLedger(le, BlockId(0, 0)).get, s"Unauthorized owner")
  }

  it should "fail to remove owned attribute, if signatures not match" in {
    val le = makeRemoveOwnedAttribute(authorizedOwnerAccount = privateAcc1)
    expectErrorWithMessage(identityLedger(le, BlockId(0, 0)).get, "The signature does not match the txId")
  }

  it should "fail to remove owned attribute, if not signed" in {
    val le = makeRemoveOwnedAttribute(emptySignature = true)
    expectErrorWithMessage(identityLedger(le, BlockId(0, 0)).get, "A id/tag/sig pair must be provided to continue")
  }

  it should "fail to delete identity, if not signed" in {
    val le = makeDelete(emptySignature = true)
    expectErrorWithMessage(identityLedger(le, BlockId(0, 0)).get, "A id/tag/sig pair must be provided to continue")
  }

  it should "fail to delete identity, if badly signed" in {
    val le = makeDelete(authorizedOwnerAccount = privateAcc1)
    expectErrorWithMessage(identityLedger(le, BlockId(0, 0)).get, "The signature does not match the txId")
  }

  it should "fail to delete identity, if 'self owned' attribute set" in {
    val le = makeDelete()
    val setPreventingAttr = makeSetSelfOwnedAttribute(privateAcc1, true)
    identityLedger(setPreventingAttr, BlockId(0, 0)).get
    val f = identityLedger(le, BlockId(0, 0))

    //Now unset it again so it can be deleted
    val unSetPreventingAttr = makeSetSelfOwnedAttribute(privateAcc1, false)
    identityLedger(unSetPreventingAttr, BlockId(0, 0)).get

  }

  it should "delete identity, if signed" in {
    val le = makeDelete()
    identityLedger(le, BlockId(0, 0)).get
    assert(identityService.accounts(myIdentity).isEmpty, "Should no longer exist")
  }
}
