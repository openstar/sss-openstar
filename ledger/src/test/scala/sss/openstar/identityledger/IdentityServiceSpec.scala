package sss.openstar.identityledger

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.ancillary.ByteArrayComparisonOps
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.account.{AccountKeysFactoryFixture, NodeIdTag}
import sss.openstar.common.builders.{KeyFactoryBuilder, VerifierFactoryBuilder}
import sss.openstar.db.LedgerMigratedDbFixture
import sss.openstar.util.IntBitSet

import scala.util.Try

/**
  * Created by alan on 4/22/16.
  */
class IdentityServiceSpec extends AnyFlatSpec
  with Matchers with ByteArrayComparisonOps
  with LedgerMigratedDbFixture
  with IdentityServiceFixture
  with VerifierFactoryBuilder
  with KeyFactoryBuilder
  with AccountKeysFactoryFixture
  with SignerVerifierDetailsFixture {

  val myIdentity = "intothelight_diffferent2" //be advised these are already 'norm' identities
  val myIdentity2 = "intothelight_diffferent3"
  val myRescuerIdentity = "backupguy_different2" // so they succeed in ==

  val paywallCat1 = "categroy1"
  val paywallCat2 = "categroy2"

  val key1 = svTypedPublicKey.publicKey
  val key2 = createPrivateAc().typedPublicKey.publicKey

  def someKey = createPrivateAc().typedPublicKey.publicKey
  import sss.openstar.identityledger.BackwardsCompatibilityOps.PublicKeyOps

  "The identity ledger " should " be able to claim an identity to a key" in {

    assert(identityService.accountOpt(myIdentity).isEmpty)
    assert(!identityService.matches(myIdentity, key1))

    identityService.claim(signerDetailsWith(myIdentity)).dbRunSyncGet

    identityService.ownedAttribute(myIdentity, SystemAttributeCategory.IdentityRole) shouldBe Some(IdentityRoleAttribute())
    identityService.roleOwnedAttribute(myIdentity) shouldBe IdentityRoleAttribute()
    assert(identityService.accounts(myIdentity).size == 1)
    assert(identityService.accountOpt(myIdentity, svNodeIdTag.tag).isDefined)
    assert(identityService.account(myIdentity, svNodeIdTag.tag).typedPublicKey.publicKey.isSame(key1))
    assert(identityService.matches(myIdentity, key1))
  }

  import sss.openstar.identityledger.BackwardsCompatibilityOps.PublicKeyOps
  it should "support role attributes in a claim" in {

    assert(identityService.accountOpt(myIdentity2).isEmpty)
    assert(!identityService.matches(myIdentity2, key2))

    val idRoles = IdentityRoleAttribute(IntBitSet(0).set(31))

    identityService.claim(key2.toDetails(myIdentity2), Seq(idRoles)).dbRunSyncGet

    identityService.ownedAttribute(myIdentity2, SystemAttributeCategory.IdentityRole) shouldBe Some(idRoles)
    identityService.roleOwnedAttribute(myIdentity2) shouldBe idRoles
  }


  it should "prevent adding the same key to an identity twice" in {
    intercept[Exception](identityService.link(signerDetailsWith(myIdentity)).dbRunSyncGet)
  }

  it should "create a default paywall" in {
    val Seq(Paywall(`myIdentity`, _, _), Paywall(`myIdentity`, _, _)) = identityService.paywalls(myIdentity)
  }

  it should "be able to link a second key to an identity " in {
    val acc = createPrivateAc()
    val acm = IdentityService()
    assert(acm.accountOpt(myIdentity, svNodeIdTag.tag).isDefined)
    val deets = signerDetailsWith(nodeId = myIdentity, pKey = acc.typedPublicKey.publicKey)
    acm.link(deets).dbRunSyncGet
    assert(acm.matches(myIdentity,acc.typedPublicKey.publicKey))

  }


  it should " be able to find an identity from a key " in {

    assert(identityService.identify(key1).isDefined)
    assert(identityService.identify(key1).get.nodeId == myIdentity)

  }

  it should " be able to unlink a key from an identity " in {

    assert(identityService.matches(myIdentity, key1))
    identityService.unlink(myIdentity, key1).dbRunSyncGet

    assert(!identityService.matches(myIdentity, key1))
    assert(!identityService.accounts(myIdentity).exists(_.typedPublicKey.publicKey.isSame(key1)))

  }

  import sss.openstar.identityledger.BackwardsCompatibilityOps.PublicKeyOps
  it should "delete an id with attributes and so on if last key is unlinked" in {

    val myIdentity = "asdasdadsasdsdasdasd"
    val myMessageStore = "backupguy_differentasdasdas" // so they succeed in ==
    val key = createPrivateAc().typedPublicKey.publicKey
    val key2 = createPrivateAc().typedPublicKey.publicKey
    assert(identityService.accounts(myIdentity).isEmpty)

    identityService.claim(key.toDetails(myIdentity)).dbRunSyncGet
    identityService.claim(key2.toDetails(myMessageStore)).dbRunSyncGet
    assert(identityService.matches(myIdentity, key))

    val set = ProviderChargeAttribute(99)
    val changedAttribute = identityService.setAttribute(myIdentity, set).dbRunSyncGet
    identityService.setPaywall(myIdentity, paywallCat1, 1000).dbRunSyncGet
    val changedAttribute2 = identityService.setOwnedAttribute(myIdentity, set).dbRunSyncGet
    identityService.addMessageStore(myIdentity, myMessageStore).dbRunSyncGet

    identityService.unlink(myIdentity, key).dbRunSyncGet
    assert(identityService.accounts(myIdentity).isEmpty)
    assert(Try(identityService.paywalls(myIdentity)).isFailure)

    identityService.unlink(myMessageStore, key2).dbRunSyncGet
    assert(identityService.accounts(myMessageStore).isEmpty)

  }

  it should "prevent linking a non existing rescuer to an identity" in {
    intercept[IllegalArgumentException](identityService.linkRescuer(myIdentity, myRescuerIdentity).dbRunSyncGet)
  }

  it should "allow linking an existing rescuer to an identity" in {

    assert(identityService.accounts(myRescuerIdentity).isEmpty)
    identityService.claim( someKey.toDetails(myRescuerIdentity)).dbRunSyncGet
    assert(identityService.accounts(myRescuerIdentity).size == 1)
    identityService.linkRescuer(myIdentity, myRescuerIdentity).dbRunSyncGet
    val rescuers = identityService.rescuers(myIdentity)
    assert(rescuers.size == 1)
    assert(rescuers.contains(myRescuerIdentity))
  }

  it should "allow unlinking an existing rescuer from an identity" in {

    identityService.unLinkRescuer(myIdentity, myRescuerIdentity).dbRunSyncGet
    val rescuers = identityService.rescuers(myIdentity)
    assert(rescuers.isEmpty)
  }


  it should "prevent adding oneself as a message store" in {

    intercept[IllegalArgumentException] {
      identityService.addMessageStore(myIdentity, myIdentity).dbRunSyncGet
    }
    assert(identityService.messageStores(myIdentity).isEmpty)
  }

  it should "allow adding a message store" in {

    identityService.addMessageStore(myIdentity, myRescuerIdentity).dbRunSyncGet
    val messageStores = identityService.messageStores(myIdentity)
    assert(messageStores.size == 1)
    assert(messageStores.head == myRescuerIdentity)
  }

  it should "allow removing a message store" in {

    identityService.removeMessageStore(myIdentity, myRescuerIdentity).dbRunSyncGet
    val messageStores = identityService.messageStores(myIdentity)
    assert(messageStores.isEmpty)

  }

  it should "allow adding/ up to max message stores" in {

    val ids = (1 to identityService.maxMessageStores + 1) map (i => s"$myIdentity$i")

    ids foreach (id => identityService.claim(someKey.toDetails(id)).dbRunSyncGet)

    ids.take(identityService.maxMessageStores) foreach (identityService.addMessageStore(myIdentity, _).dbRunSyncGet)

    val k = identityService.messageStores(myIdentity)

    intercept[IllegalArgumentException] {
      identityService.addMessageStore(myIdentity, ids.last).dbRunSyncGet
    }

    val messageStores = identityService.messageStores(myIdentity)
    assert(messageStores.size == identityService.maxMessageStores)

    assert(messageStores.forall(ids.contains(_)))

  }

  it should "not allow adding same message store twice" in {
    intercept[IllegalArgumentException] {
      identityService.addMessageStore(myIdentity, myIdentity + "1").dbRunSyncGet
    }
  }

  it should "allow removing message stores" in {
    identityService.removeMessageStore(myIdentity, myIdentity + "1").dbRunSyncGet
    val messageStores = identityService.messageStores(myIdentity)
    assert(messageStores.size == 4)
    assert(!messageStores.contains(myIdentity + "1"))
  }

  it should "allow adding a paywall category" in {

    identityService.setPaywall(myIdentity, paywallCat1, 1000).dbRunSyncGet

    val pws = identityService.paywalls(myIdentity)

    assert(pws.size == 3) //new one plus two defaults
    assert(pws.contains(Paywall(myIdentity, paywallCat1, Some(1000))))
  }

  it should "allow removing a paywall category" in {

    val Some(Paywall(`myIdentity`, `paywallCat1`, None)) = identityService.removePaywall(myIdentity, paywallCat1).dbRunSyncGet
    val pws = identityService.paywalls(myIdentity)
    assert(pws.size == 2)
  }

  it should "prevent removing default category" in {

    intercept[IllegalArgumentException](
      identityService.paywalls(myIdentity)
        .foreach(
          pw => identityService.removePaywall(myIdentity, pw.category).dbRunSyncGet
        )
    )
  }

  it should "respect max number of paywalls" in {

    intercept[IllegalArgumentException](
      0 to identityService.maxPaywalls map { i =>
        identityService.setPaywall(myIdentity, s"${paywallCat1}${i}", 1000).dbRunSyncGet
      })
  }

  it should "prevent released identities from being rescuers" in {

    identityService.linkRescuer(myIdentity, myRescuerIdentity).dbRunSyncGet
    val rescuers = identityService.rescuers(myIdentity)
    assert(rescuers.contains(myRescuerIdentity))
    assert(identityService.accounts(myRescuerIdentity).size == 1)
    identityService.unlink(myRescuerIdentity, identityService.defaultTag).dbRunSyncGet
    assert(identityService.accounts(myRescuerIdentity).isEmpty)
    val emptyRescuers = identityService.rescuers(myIdentity)
    assert(emptyRescuers.isEmpty)

  }

  it should "add attribute" in {
    val set = ProviderChargeAttribute(90)
    val changedAttribute = identityService.setAttribute(myIdentity, set).dbRunSyncGet
    assert(changedAttribute.attr == set)
    val all = identityService.listAttributes(myIdentity)
    assert(all.contains(set))
  }

  it should "alter attribute" in {
    val set = ProviderChargeAttribute(99)
    val changedAttribute = identityService.setAttribute(myIdentity, set).dbRunSyncGet
    assert(changedAttribute.attr == set)
    val all = identityService.listAttributes(myIdentity)
    assert(all.contains(set))
  }

  it should "delete attribute" in {

    val changedAttribute = identityService.removeAttribute(myIdentity, SystemAttributeCategory.ProviderCharge).dbRunSyncGet
    assert(changedAttribute.isDefined)
    assert(changedAttribute.get.attr == SystemAttributeCategory.ProviderCharge)
    assert(!identityService.listAttributes(myIdentity).map(_.category).contains(SystemAttributeCategory.ProviderCharge))
  }

  it should "list by attribute" in {

    val all = identityService.listByAttribute(SystemAttributeCategory.ProviderCharge, 0, 100)
    assert(!all.exists(_.identity == myIdentity))
    val set = ProviderChargeAttribute(998)
    identityService.setAttribute(myIdentity, set).dbRunSyncGet

    val all2 = identityService.listByAttribute(SystemAttributeCategory.ProviderCharge, 0, 100)
    assert(all2.contains(IdentityAttribute(myIdentity, set)))

  }

  it should "alter owned attribute" in {
    val set = ProviderChargeAttribute(99)
    val changedAttribute = identityService.setOwnedAttribute(myIdentity, set).dbRunSyncGet
    assert(changedAttribute.attr == set)
    val all = identityService.listOwnedAttributes(myIdentity)
    assert(all.contains(set))
  }

  it should "delete owned attribute" in {
    val changedAttribute = identityService.removeOwnedAttribute(myIdentity, SystemAttributeCategory.ProviderCharge).dbRunSyncGet
    assert(changedAttribute.isDefined)
    assert(changedAttribute.get.attr == SystemAttributeCategory.ProviderCharge)
    assert(!identityService.listOwnedAttributes(myIdentity).map(_.category).contains(SystemAttributeCategory.ProviderCharge))
  }

  it should "list by owned attribute" in {
    val all = identityService.listByOwnedAttribute(SystemAttributeCategory.ProviderCharge, 0, 100)
    assert(!all.exists(_.identity == myIdentity))
    val set = ProviderChargeAttribute(998)
    identityService.setOwnedAttribute(myIdentity, set).dbRunSyncGet

    val all2 = identityService.listByOwnedAttribute(SystemAttributeCategory.ProviderCharge, 0, 100)
    assert(all2.contains(IdentityAttribute(myIdentity, set)))
  }

  it should "allow checkpoints" in {

    println("PRE CHECKPOINT")
    db.table("identity_tbl").map(println)
    db.table("key_tbl").map(println)
    val digest = identityService.createCheckpoint(100).dbRunSyncGet.get

    /*println("PRE CHECKPOINT - BACKUP")
    db.table("identity_tbl_100").map(println)
    db.table("key_tbl_100").map(println)*/

    val accs1 = identityService.accounts(myIdentity)
    val msgStores1 = identityService.messageStores(myIdentity)
    val rescuers1 = identityService.rescuers(myIdentity)

    val c1 = identityService.count
    val newId = myIdentity + "100"
    identityService.claim(someKey.toDetails(newId)).dbRunSyncGet
    val c2 = identityService.count


    identityService.importState(100).dbRunSyncGet
    val c3 = identityService.count
    val accs2 = identityService.accounts(myIdentity)
    val msgStores2 = identityService.messageStores(myIdentity)
    val rescuers2 = identityService.rescuers(myIdentity)

    assert(c1 + 1 === c2)
    assert(c1 === c3)

    assert(accs1 === accs2)
    assert(msgStores1 === msgStores2)
    assert(rescuers1 === rescuers2)

    println("POST CHECKPOINT")

    db.table("identity_tbl").map(println)
    db.table("key_tbl").map(println)
  }

  it should "continue to function (no sql exception) after state import" in {

    identityService.createCheckpoint(102).dbRunSyncGet
    identityService.importState(102).dbRunSyncGet

    val idForThisTest = myIdentity + "101"

    identityService.claim(someKey.toDetails(idForThisTest)).dbRunSyncGet
    identityService.link(signerDetailsWith(idForThisTest)).dbRunSyncGet

    identityService.claim(someKey.toDetails(myRescuerIdentity)).dbRunSyncGet
    identityService.linkRescuer(idForThisTest, myRescuerIdentity).dbRunSyncGet
    val rescuers = identityService.rescuers(idForThisTest)
    assert(rescuers.contains(myRescuerIdentity))

    val ids = (1 to 4) map (i => s"$myIdentity$i")
    ids foreach (identityService.addMessageStore(idForThisTest, _).dbRunSyncGet)
    val messageStores = identityService.messageStores(idForThisTest)
    assert(messageStores.size == 4)
    identityService.removeMessageStore(idForThisTest, s"${myIdentity}1").dbRunSyncGet
    val messageStores2 = identityService.messageStores(idForThisTest)
    assert(messageStores2.size == 3)
    assert(identityService.unlink(idForThisTest, svNodeIdTag.tag).dbRunSyncGet)
  }

  it should "delete states" in {
    identityService.createCheckpoint(102).dbRunSyncGet
    assert(identityService.deleteCheckpoint(999).dbRunSync.isFailure, "None existent should fail")
    assert(identityService.deleteCheckpoint(102).dbRunSync.isSuccess)
    assert(identityService.importState(102).dbRunSync.isFailure)
  }

  it should "list using the filter " in {
    val countNoFilter = identityService.count(None)
    val countEmptyFilter = identityService.list(Some("")).size
    val count = identityService.count
    val listNofilter = identityService.list(None).size
    val listEmptyFilter = identityService.list(Some("")).size
    assert(count > 1)
    assert(count == countNoFilter)
    assert(count == countEmptyFilter)
    assert(count == listNofilter)
    assert(count == listEmptyFilter)

    val prefixIntoTheLight = "intothelight"
    val filter = identityService.list(Some(prefixIntoTheLight))
    assert(filter.forall(_.startsWith(prefixIntoTheLight)))

    val filter2 = identityService.list(Some("backup"))
    assert(filter2.forall(!_.startsWith(prefixIntoTheLight)))
    assert(filter.forall(!_.startsWith("backup")))
    assert(filter2.forall(_.startsWith("backup")))
    assert(filter.nonEmpty)
    assert(filter2.nonEmpty)

  }

  it should "delete 'fully loaded' user" in {
    val myIdentity = "intothelight_diffferent4" //be advised these are already 'norm' identities
    val key1 = createPrivateAc().typedPublicKey.publicKey
    identityService.claim(key1.toDetails(myIdentity)).dbRunSyncGet
    val set = ProviderChargeAttribute(99)
    val changedAttribute = identityService.setAttribute(myIdentity, set).dbRunSyncGet
    identityService.setPaywall(myIdentity, paywallCat1, 1000).dbRunSyncGet
    val changedAttribute2 = identityService.setOwnedAttribute(myIdentity, set).dbRunSyncGet
    identityService.addMessageStore(myIdentity, myRescuerIdentity).dbRunSyncGet

    identityService.linkRescuer(myIdentity, myRescuerIdentity).dbRunSyncGet

    val deets = signerDetailsWith(myIdentity)
    identityService.link(deets).dbRunSyncGet
    identityService.deleteIdentity(myIdentity).dbRunSyncGet
    identityService.deleteIdentity(myIdentity).dbRunSync.isFailure shouldBe true // already deleted

    assert(identityService.accounts(myIdentity).isEmpty, "Id should be gone")
    assert(Try(identityService.rescuers(myIdentity)).isFailure, "rescuers should be gone")

  }

}