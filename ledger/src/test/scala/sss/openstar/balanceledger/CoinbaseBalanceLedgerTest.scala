package sss.openstar.balanceledger

import org.scalatest.concurrent.ScalaFutures
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.scalatest.time.{Millis, Minutes, Span}
import scorex.crypto.signatures.MessageToSign
import sss.openstar.LedgerTestOps.EitherToTry
import sss.openstar.account.{AccountKeysFactoryFixture, KeyPersisterFixtureWithScalaFutures, NodeIdentity, NodeIdentityManager}
import sss.openstar.common.block.BlockId
import sss.openstar.common.builders.{CpuBoundExecutionContextFixture, KeyFactoryBuilder, VerifierFactoryBuilder}
import sss.openstar.contract._
import sss.openstar.db.LedgerMigratedDbFixture
import sss.openstar.identityledger.IdentityServiceFixture
import sss.openstar.ledger._
import sss.openstar.util.Amount.Ops.TryAmountOps
import sss.openstar.util.{Amount, AmountBuilder}
import sss.openstar.{Currency, DummySeedBytes, UniqueNodeIdentifier}

import java.nio.charset.StandardCharsets
import scala.concurrent.Future
import scala.language.implicitConversions
import scala.util.Success
import sss.ancillary.FutureOps._
import sss.openstar.account.Ops.MakeTxSigArys
/**
 * Created by alan on 2/15/16.
 */
class CoinbaseBalanceLedgerTest extends AnyFlatSpec with Matchers with ScalaFutures
  with LedgerMigratedDbFixture
  with KeyPersisterFixtureWithScalaFutures
  with IdentityServiceFixture
  with VerifierFactoryBuilder
  with CpuBoundExecutionContextFixture
  with KeyFactoryBuilder
  with AccountKeysFactoryFixture {

  import sss.openstar.identityledger.BackwardsCompatibilityOps.PublicKeyOps

  trait TestCurrency extends Currency
  trait TestCurrency2 extends Currency


  implicit override val patienceConfig: PatienceConfig =
    PatienceConfig(timeout = scaled(Span(1, Minutes)), interval = scaled(Span(20, Millis)))


  val nodeId = "testkey"
  val tag = "tag"
  val tagBytes = tag.getBytes(StandardCharsets.UTF_8)

  val phrase = "Phrase1233333333$"
  val nodeIdKeys = createPrivateAc()
  val signerF: MessageToSign => Future[Array[Byte]] = m => Future.successful(nodeIdKeys.sign(m))

  val ledgerId = LedgerId(globalChainId)
  def pKeyOfFirstSigner(blockHeight: Long) = Option(nodeIdKeys.typedPublicKey.publicKey)

  def nodeIdentityToQuorumCandidates(uniqueId: UniqueNodeIdentifier) = () => Set(uniqueId)

  val cbv = CoinbaseValidator(100, 0, nodeIdentityToQuorumCandidates(nodeId))
  lazy val accountOpt: FindPublicKeyAccFTxOpt = (id, tag) => identityService.accountOptFTx(id, tag).map(_.map(identityService.toVerifier))

  identityService.claim(nodeIdKeys.typedPublicKey.publicKey.toDetails(nodeId, tag))

  case class TestCurrencyAmount(value: Long) extends Amount {
    override type C = TestCurrency
    override def newAmount(value: Long): Amount = new TestCurrencyAmount(value)
  }


  case class TestCurrencyAmount2(value: Long) extends Amount {
    override type C = TestCurrency2
    override def newAmount(value: Long): Amount = new TestCurrencyAmount2(value)
  }

  implicit val cb = new AmountBuilder[TestCurrency] {
    override def apply(value: Long): Amount = TestCurrencyAmount(value)
  }

  val ledger: CoinbaseBalanceLedger[TestCurrency] = CoinbaseBalanceLedger(ledgerId, cbv, accountOpt, keysFactoryLookup)

  //FIXME SHAMEFULLY the state of validOut is used throughout this test.
  var validOut: TxIndex = _
  var cbTx: SignedTxEntry = _

  "Test case classes" should "be unequal" in {
    TestCurrencyAmount(1) shouldBe  TestCurrencyAmount(1)
    TestCurrencyAmount(2) should not be TestCurrencyAmount2(1)
  }

  "A coinbase tx " should "honour the 'num blocks in future' setting" in {
    val numBlockInFuture = 10
    val cbv = CoinbaseValidator(100, numBlockInFuture, nodeIdentityToQuorumCandidates(nodeId))
    val ledger = CoinbaseBalanceLedger[TestCurrency](ledgerId, cbv, accountOpt, keysFactoryLookup)
    val le = ledger.coinbase(50, 1.toByte).get

    val initBal = ledger.balance
    assert(
      le.txEntryBytes.toSignedTxEntry.txEntryBytes.toTx.outs.head.encumbrance
        .asInstanceOf[SingleIdentityEnc]
        .minBlockHeight == 60
    )

    // can only ledger into same block
    val Right(r) = ledger(le, BlockId(50, 1))

    assert(ledger(le, BlockId(51, 1)).isLeft)

    ledger.balance - initBal shouldBe Success(Amount[TestCurrency](100))
  }

  "A Ledger" should "allow coinbase tx" in {
    val le = ledger.coinbase(2, 1.toByte).get
    cbTx = le.txEntryBytes.toSignedTxEntry
    val initBal = ledger.balance
    ledger(le, BlockId(2, 1))
    ledger.balance - initBal shouldBe Success(Amount[TestCurrency](100))

  }

  it should "only allow 1 coinbase per block" in {

    val le = ledger.coinbase(2, 1.toByte).get
    assert(ledger(le, BlockId(2, 1)).isLeft)
  }

  it should " prevent txs with bad balances " in {

    val ins = Seq(TxInput(TxIndex(cbTx.txId, 0), 1000, NullDecumbrance()))
    val outs = Seq(TxOutput(1, NullEncumbrance), TxOutput(1, NullEncumbrance))
    val stx = SignedTxEntry(StandardTx(ins, outs).toBytes)

    intercept[IllegalArgumentException] {
      val le = ledger
        .apply(
          toLedgerItem(stx),
          BlockId(0, 1)
        )
        .get
    }
  }

  implicit def toLedgerItem(sTx: SignedTxEntry): LedgerItem =
    LedgerItem(1.toByte, sTx.txId, sTx.toBytes)

  it should " prevent txs with no inputs " in {

    val ins: Seq[TxInput] = Seq()
    val outs: Seq[TxOutput] = Seq()
    intercept[IllegalArgumentException] {
      val le =
        ledger.apply(toLedgerItem(SignedTxEntry(StandardTx(ins, outs).toBytes, Seq().txSig)), BlockId(0, 1)).get
    }
  }

  it should " not allow out balance to be greater than in " in {

    val ins = Seq(TxInput(TxIndex(cbTx.txId, 0), 100, NullDecumbrance()))
    val outs = Seq(TxOutput(99, NullEncumbrance), TxOutput(11, NullEncumbrance))
    intercept[IllegalArgumentException] {
      val le = ledger
        .apply(toLedgerItem(SignedTxEntry(StandardTx(ins, outs).toBytes, Seq(Seq()).txSig)), BlockId(0, 1))
        .get
    }
  }

  it should " prevent double spend and honour min block height " in {

    val ins = Seq(TxInput(TxIndex(cbTx.txId, 0), 100, SingleIdentityDec))
    val outs = Seq(TxOutput(99, NullEncumbrance), TxOutput(1, NullEncumbrance))
    val tx = StandardTx(ins, outs)

    val sig = SingleIdentityDec.createUnlockingSignature(tx.txId, tagBytes, signerF).await()
    val stx = SignedTxEntry(tx.toBytes, TxSig.emptyTxSig.withSig(sig))
    validOut = TxIndex(stx.txId, 0)

    intercept[IllegalArgumentException] {
      // minimum block height not exceeded
      ledger.apply(toLedgerItem(SignedTxEntry(StandardTx(ins, outs).toBytes)), BlockId(3, 1)).get
    }

    val le = ledger.apply(toLedgerItem(stx), BlockId(4, 2))

    intercept[IllegalArgumentException] {
      // cannot double spend.
      ledger.apply(toLedgerItem(SignedTxEntry(StandardTx(ins, outs).toBytes)), BlockId(4, 1)).get
    }
  }

  it should " prevent spend from invalid tx in" in {

    val ins = Seq(TxInput(TxIndex(DummySeedBytes.randomSeed(3), 2), 100, NullDecumbrance()))
    val outs = Seq(TxOutput(99, NullEncumbrance), TxOutput(1, NullEncumbrance))
    intercept[IllegalArgumentException] {
      ledger.apply(toLedgerItem(SignedTxEntry(StandardTx(ins, outs).toBytes)), BlockId(0, 1)).get
    }
  }

  it should "allow spending from a tx out that was also a tx" in {

    val ins = Seq(TxInput(validOut, 99, NullDecumbrance()))
    val outs = Seq(TxOutput(98, NullEncumbrance), TxOutput(1, NullEncumbrance))
    val stx = SignedTxEntry(StandardTx(ins, outs).toBytes, TxSig.emptyTxSig.withSig(Seq()))
    ledger(stx, BlockId(0, 2))
    val nextIns = Seq(TxInput(TxIndex(stx.txId, 0), 98, NullDecumbrance()))
    val nextOuts = Seq(TxOutput(1, NullEncumbrance), TxOutput(97, NullEncumbrance))
    val nextTx = SignedTxEntry(StandardTx(nextIns, nextOuts).toBytes, TxSig.emptyTxSig.withSig(Seq()))
    ledger(nextTx, BlockId(0, 2))

    validOut = TxIndex(nextTx.txId, 1)

    intercept[IllegalArgumentException] {
      ledger(stx, BlockId(0, 2)).get
    }

    intercept[IllegalArgumentException] {
      ledger(nextTx, BlockId(0, 2)).get
    }
  }

  it should " handle different encumbrances on different inputs " in {

    lazy val pkPair1 = createPrivateAc()
    lazy val pkPair2 = createPrivateAc()

    val ins = Seq(TxInput(validOut, 97, NullDecumbrance()))

    val outs =
      Seq(TxOutput(1, SinglePrivateKey(pkPair1.typedPublicKey.publicKey)), TxOutput(96, SinglePrivateKey(pkPair2.typedPublicKey.publicKey)))
    val stx = SignedTxEntry(StandardTx(ins, outs).toBytes, TxSig.emptyTxSig.withSig(Seq()))
    ledger(stx, BlockId(0, 2))
    val nextIns =
      Seq(TxInput(TxIndex(stx.txId, 0), 1, PrivateKeySig), TxInput(TxIndex(stx.txId, 1), 96, PrivateKeySig))
    val nextOuts = Seq(TxOutput(97, NullEncumbrance))
    val nextTx = StandardTx(nextIns, nextOuts)

    val nextSignedTx =
      SignedTxEntry(nextTx.toBytes, Seq(TxSig.emptyTxSig.sigs.head, Seq(pkPair1.sign(nextTx.txId)), Seq(pkPair2.sign(nextTx.txId))).txSig)
    ledger(nextSignedTx, BlockId(0, 2))

  }
}
