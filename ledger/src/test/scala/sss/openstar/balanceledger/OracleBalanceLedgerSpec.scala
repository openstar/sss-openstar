package sss.openstar.balanceledger

import org.scalatest.concurrent.ScalaFutures
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.scalatest.time.{Minutes, Seconds, Span}
import sss.ancillary.FutureOps.AwaitResult
import sss.db.FutureTx
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.Currency
import sss.openstar.account.Ops.MakeTxSigArys
import sss.openstar.account.{AccountKeysFactoryFixture, KeyPersisterFixtureWithScalaFutures, NodeIdTag, NodeIdentityManagerFixture}
import sss.openstar.balanceledger.OracleBalanceLedger._
import sss.openstar.balanceledger.db.UtxoDbStorage
import sss.openstar.common.block.BlockId
import sss.openstar.common.builders._
import sss.openstar.contract.{ClosedEncumbrance, NullDecumbrance, NullEncumbrance, SingleIdentityEnc}
import sss.openstar.db.LedgerMigratedDbFixture
import sss.openstar.hash.SecureCryptographicHash
import sss.openstar.identityledger.IdentityServiceFixture
import sss.openstar.ledger.{LedgerId, LedgerItem, SignedTxEntry, TxSig}
import sss.openstar.util.{Amount, AmountBuilder}

import java.nio.charset.StandardCharsets
import sss.openstar.identityledger.BackwardsCompatibilityOps.PublicKeyOps

class OracleBalanceLedgerSpec extends AnyFlatSpec with Matchers with ScalaFutures
  with ConfigBuilder
  with LedgerMigratedDbFixture
  with IdentityServiceFixture
  with AccountKeysFactoryFixture
  with NodeIdentityManagerFixture
  with RequirePhrase
  with NodeIdTagBuilder
  with SignerFactoryBuilder
  with CpuBoundExecutionContextFixture
  with ActorSystemBuilder
  with KeyPersisterFixtureWithScalaFutures {

  implicit override val patienceConfig: PatienceConfig =
    PatienceConfig(timeout = scaled(Span(1, Minutes)), interval = scaled(Span(1, Seconds)))

  val owner = "asdasdad"
  val tag = "tag"
  lazy override val nodeIdTag = NodeIdTag(owner, tag)

  def defaultSigner = nodeIdentity.verifiers.find(_.nodeIdTag.tag == tag).get.signer

  val recipientId = "aguy"
  val recipientAmount = 100000

  override def beforeAll(): Unit = {
    super.beforeAll()
    val deets = nodeIdentity
      .verifiers
      .find(_.nodeIdTag.tag == tag)
      .get
      .verifier
      .typedPublicKey
      .publicKey
      .toDetails(nodeIdentity.id, tag)

    identityService.claim(deets).dbRunSyncGet
  }

  val oracleLedgerId = LedgerId(2.toByte)

  trait TestCurrency extends Currency

  case class TestAmount(value: Long) extends Amount {
    override type C = TestCurrency

    override def newAmount(value: Long): Amount = TestAmount(value)
  }

  implicit val cb : AmountBuilder[TestCurrency] = TestAmount.apply

  val ownerLookup: OracleLedgerOwnerLookupFTx = {
    case b if b == oracleLedgerId => FutureTx.unit(Seq(owner))
    case _ => FutureTx.unit(Seq.empty)
  }

  implicit val storage = UtxoDbStorage(utxoDbStorageLedgerIds.head)

  lazy val sut = new OracleBalanceLedger[TestCurrency](oracleLedgerId, identityService, ownerLookup, storage, keysFactoryLookup)

  val unusedAddr = "addr1123456789098765432123456789011234567890987654321234567890"
  val gId = SecureCryptographicHash.hash(unusedAddr.getBytes(StandardCharsets.UTF_8))
  val enc = OracleLodgementEncumbrance(gId)
  lazy val (setupLodgementLedgerItem, index) =
    OracleBalanceLedger.createOracleLedgerItem(
      oracleLedgerId,
      Amount[TestCurrency](3),
      enc,
      defaultSigner).await()

  lazy val txId = setupLodgementLedgerItem.txId


  lazy val txIndex = TxIndex(setupLodgementLedgerItem.txId, index)
  lazy val outs = Seq(TxOutput(1, NullEncumbrance),TxOutput(1, NullEncumbrance),TxOutput(1, NullEncumbrance))
  lazy val confirmLodgementLedgerItem = OracleBalanceLedger
    .confirmLodgement(oracleLedgerId, defaultSigner, 3, unusedAddr, txIndex, outs).await()

  lazy val confirmTxId = confirmLodgementLedgerItem.txId

  implicit class LedgerIdOps(val ledgerId: LedgerId) {
    def +(i: Int): LedgerId = LedgerId((ledgerId.id + i.toByte).toByte)
  }

  "An oracle based ledger" should "allow an oracle to insert new coin" in {

    val result = sut.apply(setupLodgementLedgerItem, BlockId(1, 1))

    result should matchPattern {
      case Right(UtxoChanges(`oracleLedgerId`,
        retTxId,
        1,
        Seq(OracleInUtxoChange(TxIndex(retTxId2, 0), 1)),
        Seq(
          NewUtxo(TxIndex(retTxId3, 0), TxOutput(3, OracleLodgementEncumbrance(_))),
        ))
      ) if retTxId.isSame(txId) =>
    }

    //now enable it
    val result2 = sut(confirmLodgementLedgerItem, BlockId(1, 1))

    result2 should matchPattern {
      case Right(UtxoChanges(`oracleLedgerId`,
      txIdAry,
      1,
      Seq(ConsumedUtxoChange(TxIndex(consumedTxIdAry, 0))),
      Seq(
      NewUtxo(TxIndex(newUtxoAry1, 0), TxOutput(1, NullEncumbrance)),
      NewUtxo(TxIndex(newUtxoAry2, 1), TxOutput(1, NullEncumbrance)),
      NewUtxo(TxIndex(newUtxoAry3, 2), TxOutput(1, NullEncumbrance))
      ))
      ) if (txIdAry sameElements confirmTxId) &&
        (newUtxoAry1 sameElements confirmTxId) &&
        (newUtxoAry2 sameElements confirmTxId) &&
        (newUtxoAry3 sameElements confirmTxId) &&
        (consumedTxIdAry sameElements txId) =>
    }


  }


  it should "fail an oracle to remove coin on bad ledger Id" in {

    val ins = Seq(TxInput(TxIndex(txId, 1), 1, NullDecumbrance()))
    val outs = Seq(TxOutput(1, OracleWithdrawEncumbrance("mydetails")))
    val tx = StandardTx(ins, outs)
    val txBytes= tx.toBytes

    val sTx = SignedTxEntry(txBytes, Seq(Seq.empty).txSig)


    val ins2 = Seq(TxInput(TxIndex(sTx.txId, 0), 1, OracleDecumbrance(oracleLedgerId+1)))
    val outs2 = Seq(TxOutput(1, ClosedEncumbrance))
    val tx2 = StandardTx(ins2, outs2)
    val txBytes2 = tx2.toBytes
    val sig = createOracleDecumbranceSig(defaultSigner, tx2.txId).await()
    val sTx2 = SignedTxEntry(txBytes2, TxSig.emptyTxSig.withSig(sig))

    val le2 = LedgerItem(1.toByte, sTx2.txId, sTx2.toBytes)
    val result2 = sut.apply(le2, BlockId(1, 1))
    assert(result2.isLeft)

  }


  it should "fail an oracle to remove coin on bad signature" in {

    val ins = Seq(TxInput(TxIndex(txId, 1), 1, NullDecumbrance()))
    val outs = Seq(TxOutput(1, OracleWithdrawEncumbrance("mydetails")))
    val tx = StandardTx(ins, outs)
    val txBytes= tx.toBytes

    val sTx = SignedTxEntry(txBytes, Seq(Seq.empty).txSig)


    val ins2 = Seq(TxInput(TxIndex(sTx.txId, 0), 1, OracleDecumbrance((oracleLedgerId+1))))
    val outs2 = Seq(TxOutput(1, ClosedEncumbrance))
    val tx2 = StandardTx(ins2, outs2)
    val txBytes2 = tx2.toBytes
    val Seq(sigBytes, tagBytes) = createOracleDecumbranceSig(defaultSigner, tx2.txId).await()
    val badSig = sigBytes.reverse
    val sTx2 = SignedTxEntry(txBytes2, TxSig.emptyTxSig.withSig(Seq(badSig, tagBytes)))

    val le2 = LedgerItem(1.toByte, sTx2.txId, sTx2.toBytes)
    val result2 = sut.apply(le2, BlockId(1, 1))
    assert(result2.isLeft)

  }

  it should "allow a user to transfer coin" in {

    val ins = Seq(TxInput(TxIndex(confirmTxId, 0), 1, NullDecumbrance()))
    val person = "someperson"
    val outs = Seq(TxOutput(1, SingleIdentityEnc(person)))
    val tx = StandardTx(ins, outs)
    val txBytes= tx.toBytes

    val sTx = SignedTxEntry(txBytes, TxSig.emptyTxSig.withSig(Seq.empty))
    val le = LedgerItem(1.toByte, sTx.txId, sTx.toBytes)
    val result = sut.apply(le, BlockId(1, 1))

    result should matchPattern {
      case Right(UtxoChanges(`oracleLedgerId`,
      txIdAry,
      1,
      Seq(ConsumedUtxoChange(TxIndex(_, 0))),
      Seq(
      NewUtxo(TxIndex(_, 0), TxOutput(1, SingleIdentityEnc(`person`, None, 0))),
      ))
      ) =>
    }


  }

  it should "allow an oracle to remove coin on request" in {

    val ins = Seq(TxInput(TxIndex(confirmTxId, 1), 1, NullDecumbrance()))
    val outs = Seq(TxOutput(1, OracleWithdrawEncumbrance("mydetails")))
    val tx = StandardTx(ins, outs)
    val txBytes= tx.toBytes

    val sTx = SignedTxEntry(txBytes, TxSig.emptyTxSig.withSig(TxSig.emptyTxSig.sigs.head))

    val le = LedgerItem(1.toByte, sTx.txId, sTx.toBytes)
    val result = sut.apply(le, BlockId(1,1))
    assert(result.isRight)

    assert(storage.get(TxIndex(sTx.txId, 0)).isDefined)
    val ins2 = Seq(TxInput(TxIndex(sTx.txId, 0), 1, OracleDecumbrance(oracleLedgerId)))
    val outs2 = Seq(TxOutput(1, ClosedEncumbrance))
    val tx2 = StandardTx(ins2, outs2)
    val txBytes2 = tx2.toBytes
    val sTx2 = SignedTxEntry(txBytes2, TxSig.emptyTxSig.withSig(createOracleDecumbranceSig(defaultSigner, tx2.txId).await()))

    val le2 = LedgerItem(1.toByte, sTx2.txId, sTx2.toBytes)
    val result2 = sut.apply(le2, BlockId(1, 1))
    assert(result2.isRight)
    val getInputAfterDelete = storage.get(TxIndex(sTx.txId, 0))
    assert(getInputAfterDelete.isEmpty)
    val indexRemovedDueToUtxoClosed = TxIndex(sTx2.txId, 0)
    assert(storage.get(indexRemovedDueToUtxoClosed).isEmpty)
  }

}
