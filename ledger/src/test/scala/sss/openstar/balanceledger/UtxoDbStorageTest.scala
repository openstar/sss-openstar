package sss.openstar.balanceledger

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.account.AccountKeysFactoryFixture
import sss.openstar.balanceledger.db.UtxoDbStorage
import sss.openstar.contract.SinglePrivateKey
import sss.openstar.db.LedgerMigratedDbFixture
import sss.openstar.ledger.{LedgerId, SignedTxEntry}

import java.sql.SQLIntegrityConstraintViolationException

/**!
  * Created by alan on 2/15/16.
  */
class UtxoDbStorageTest extends AnyFlatSpec with Matchers with LedgerMigratedDbFixture with AccountKeysFactoryFixture{

  override lazy val utxoDbStorageLedgerIds: Set[LedgerId] = Set(LedgerId(1), LedgerId(2))
  lazy val pkPair = createPrivateAc()
  val genisis = SignedTxEntry(
    StandardTx(
      ins = Seq(),
      outs = Seq(TxOutput(100, SinglePrivateKey( pkPair.typedPublicKey.publicKey)),

    TxOutput(
      100,
      SinglePrivateKey( pkPair.typedPublicKey.publicKey)))).toBytes
  )

  lazy val tx = genisis.txEntryBytes.toTx

  val dbStorage = UtxoDbStorage(utxoDbStorageLedgerIds.head)
  val dbStorage2 = UtxoDbStorage(utxoDbStorageLedgerIds.tail.head)

  "UTXO Storage " should " allow outputs to be persisted " in {

    tx.outs.indices foreach { i =>
      dbStorage.write(TxIndex(genisis.txId, i), tx.outs(i)).dbRunSyncGet
    }

    dbStorage.entries().foreach(println)
  }

  it should "prevent double inputs " in {

    intercept[SQLIntegrityConstraintViolationException] {
      dbStorage.write(TxIndex(genisis.txId, 0), tx.outs(0)).dbRunSyncGet
    }

  }

  it should "allow retrieval of outputs " in {

    val txOutput = dbStorage(TxIndex(genisis.txId, 0))

    assert(txOutput.amount === 100)
    val asPKeyEnc = txOutput.encumbrance.asInstanceOf[SinglePrivateKey]
    assert(asPKeyEnc.pKey === pkPair.typedPublicKey.publicKey)

  }


  it should " differentiate between indexes " in {

    val txOutput = dbStorage(TxIndex(genisis.txId, 0))
    dbStorage.delete(TxIndex(genisis.txId, 0)).dbRunSyncGet

    intercept[NoSuchElementException] {
      dbStorage(TxIndex(genisis.txId, 0))
    }

    val txOutput2 = dbStorage(TxIndex(genisis.txId, 1))
    assert(txOutput2.amount === 100)
  }

  it should " differentiate between tagged storages " in {

    dbStorage.write(TxIndex(genisis.txId, 0), tx.outs(0)).dbRunSyncGet
    val txOutput = dbStorage(TxIndex(genisis.txId, 0))

    intercept[NoSuchElementException] {
      dbStorage2(TxIndex(genisis.txId, 0))
    }

    dbStorage2.write(TxIndex(genisis.txId, 0), tx.outs(0)).dbRunSyncGet
    dbStorage.delete(TxIndex(genisis.txId, 0)).dbRunSyncGet

    intercept[NoSuchElementException] {
      dbStorage(TxIndex(genisis.txId, 0))
    }

    val txOutput2 = dbStorage2(TxIndex(genisis.txId, 0))

  }
}
