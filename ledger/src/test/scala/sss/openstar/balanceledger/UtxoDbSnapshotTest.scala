package sss.openstar.balanceledger

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.DummySeedBytes
import sss.openstar.account.{AccountKeysFactoryFixture}
import sss.openstar.balanceledger.db.UtxoDbStorage
import sss.openstar.contract.SinglePrivateKey
import sss.openstar.db.LedgerMigratedDbFixture
import sss.openstar.ledger.{LedgerId, SignedTxEntry}
import sss.openstar.util.StringCheck._

import java.sql.SQLIntegrityConstraintViolationException

/** ! Created by alan on 2/15/16.
  */
class UtxoDbSnapshotTest extends AnyFlatSpec with Matchers with LedgerMigratedDbFixture with AccountKeysFactoryFixture{

  lazy val pkPair = createPrivateAc()
  val genisis = SignedTxEntry(
    StandardTx(
      ins = Seq(),
      outs = Seq(
        TxOutput(100, SinglePrivateKey( pkPair.typedPublicKey.publicKey)),
        TxOutput(10, SinglePrivateKey( pkPair.typedPublicKey.publicKey)),
        TxOutput(1300, SinglePrivateKey( pkPair.typedPublicKey.publicKey)),
        TxOutput(140, SinglePrivateKey( pkPair.typedPublicKey.publicKey)),
        TxOutput(150, SinglePrivateKey( pkPair.typedPublicKey.publicKey)),
        TxOutput(160, SinglePrivateKey( pkPair.typedPublicKey.publicKey)),
        TxOutput(107, SinglePrivateKey( pkPair.typedPublicKey.publicKey))
      )
    ).toBytes
  )

  lazy val tx = genisis.txEntryBytes.toTx

  override lazy val utxoDbStorageLedgerIds: Set[LedgerId] = Set(LedgerId(9), LedgerId(10))
  val dbStorage  = UtxoDbStorage(utxoDbStorageLedgerIds.toSeq.head)
  val dbStorage2 = UtxoDbStorage(utxoDbStorageLedgerIds.toSeq(1))

  "UTXOStorage" should "have an identical digest to it's copy" in {

    import sss.openstar.hash._

    tx.outs.indices foreach { i =>
      dbStorage.write(TxIndex(genisis.txId, i), tx.outs(i)).dbRunSyncGet
    }
    val newDigest = dbStorage.createCheckpoint(200)
    val digest = dbStorage.stateDigest()

    assert(digest.get === newDigest.get, "Digest of back up should be same as state digest")
  }

  it should "make a faithful copy of the table" in {

    val entries = dbStorage.entries().toList
    val keys = dbStorage.keys(10).toList
    val count = dbStorage.count

    dbStorage.importState(200)

    dbStorage.count

    assert(count > 0, "Bad comparison")
    assert(dbStorage.count === count)

    entries.zip(dbStorage.entries()).foreach { case (a: TxOutput,b: TxOutput) =>
      assert(a == b, "Output should be the same")
    }

    keys.zip(dbStorage.keys(10)).foreach { case (a: TxIndex,b: TxIndex) =>
      assert(a == b, "Index should be the same")
    }

  }

  it should "have an different digest to a different state" in {

    import sss.openstar.hash._
    dbStorage2.write(TxIndex(genisis.txId, 0), tx.outs(0)).dbRunSyncGet

    val newDigest = dbStorage2.stateDigest()
    val digest = dbStorage.stateDigest()
    assert(!(digest.get === newDigest.get), "Digest of back up should be same as state digest")
  }

  it should "import a backed up state" in {

    dbStorage.createCheckpoint(201)
    val digest = dbStorage.stateDigest()

    val c1 = dbStorage.count
    val newIndex = TxIndex(genisis.txId, 200)
    dbStorage.write(newIndex, tx.outs.head).dbRunSyncGet

    val c2 = dbStorage.count

    val digest2 = dbStorage.stateDigest()

    dbStorage.importState(201).get

    val digest3 = dbStorage.stateDigest()
    val c3 = dbStorage.count

    assert(!(digest.get == digest2.get), "Extra write should change digest")
    assert(digest.get == digest3.get, "digest of imported state should match original digest")

    assert(dbStorage.get(newIndex).isEmpty, "index should not have survived")
  }

  it should "delete backed up state" in {
    dbStorage.createCheckpoint(203).get
    assert(dbStorage.deleteCheckpoint(205).isFailure, "non existent should fail")
    assert(dbStorage.importState(203).isSuccess)

    val newIndex = TxIndex(genisis.txId, 203)
    dbStorage.write(newIndex, tx.outs.head).dbRunSyncGet
    intercept[SQLIntegrityConstraintViolationException](dbStorage.write(newIndex, tx.outs.head).dbRunSyncGet)

    dbStorage.createCheckpoint(203).get

    assert(dbStorage.deleteCheckpoint(203).isSuccess)
  }
}
