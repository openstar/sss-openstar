package sss.openstar.oracleownerledger

import org.scalatest.Suite
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.concurrent.ScalaFutures.convertScalaFuture
import sss.ancillary.FutureOps.AwaitResult
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.account.NodeIdentityManager.PasswordCredentials
import sss.openstar.account.Ops.MakeTxSigArys
import sss.openstar.account.{AccountKeysFactoryFixture, KeyPersisterFixtureWithScalaFutures, NodeIdTag, NodeIdentity, NodeIdentityManager, NodeIdentityManagerFixture}
import sss.openstar.chains.Chains.GlobalChainIdMask
import sss.openstar.common.block.BlockId
import sss.openstar.common.builders.{ConfigBuilder, CpuBoundExecutionContextBuilder, KeyFactoryBuilder, NodeIdTagBuilder, PhraseFixture, RequireConfig, RequirePhrase, SignerFactoryBuilder}
import sss.openstar.contract.{FindPublicKeyAccFTxOpt, FindPublicKeyAccOpt}
import sss.openstar.db.LedgerMigratedDbFixture
import sss.openstar.identityledger.{DefaultIdentifier, IdentityServiceFixture}
import sss.openstar.ledger.{LedgerId, LedgerItem, LedgerResult, SignedTxEntry}
import sss.openstar.util.TxSign
import sss.openstar.{DummySeedBytes, UniqueNodeIdentifier}

import scala.concurrent.ExecutionContext
import scala.concurrent.ExecutionContext.Implicits


trait OracleOwnerLedgerFixture extends LedgerMigratedDbFixture
  with IdentityServiceFixture
  with AccountKeysFactoryFixture
  with NodeIdentityManagerFixture
  with KeyFactoryBuilder
  with SignerFactoryBuilder
  with CpuBoundExecutionContextBuilder
  with NodeIdTagBuilder
  with ConfigBuilder
  with PhraseFixture
  with KeyPersisterFixtureWithScalaFutures { self: Suite with ScalaFutures =>

  override implicit val cpuOnlyEc: ExecutionContext = Implicits.global
  val owner1Id = "aqweqweqweeq"
  val owner2Id = "aqweqweqweeq2"
  val tag = NodeIdTag.defaultTag

  lazy val nodeIdentity1 = addToFixtureAndGetNodeIdentity(
    owner1Id,
    tag, "Phrase1233333333!"
  )

  lazy val nodeIdentity2 = addToFixtureAndGetNodeIdentity(
    owner2Id,
    tag, "Phrase1233333333!"
  )

  val nodeIdentity1publicKey = nodeIdentity1.publicKey
  val nodeIdentity2publicKey = nodeIdentity2.publicKey

  val maxAllowedOwnersPerLedger = 3
  val maxAllowedLedgers = 4

  val notOwnerLedgerId = 4.toByte
  implicit val ownerLedgerId = LedgerId(5.toByte)
  implicit val chainId: GlobalChainIdMask = 1.toByte
  import sss.openstar.identityledger.BackwardsCompatibilityOps.PublicKeyOps

  override def beforeAll(): Unit = {
    super.beforeAll()
    identityService.claim(
      nodeIdentity1.publicKey.toDetails(owner1Id, tag),

    ).dbRunSyncGet
    identityService.claim(
      nodeIdentity2.publicKey.toDetails(owner2Id,tag)
    ).dbRunSyncGet
  }

  lazy val accountOptFTx: FindPublicKeyAccFTxOpt = (id, tag) => identityService.accountOptFTx(id, tag).map(_.map(identityService.toVerifier))

  lazy val sut = new OracleLedgerOwnerLedger(
    new DefaultIdentifier,
    accountOptFTx,
    maxAllowedOwnersPerLedger,
    maxAllowedLedgers,
    keysFactoryLookup
  )


  trait OracleClaimer {
    val ledgerId: LedgerId
    val instigator: UniqueNodeIdentifier
    val otherOwners: Seq[UniqueNodeIdentifier]
    def oracleClaim: OracleClaim = OracleClaim(ledgerId, instigator, otherOwners)
    def run(ledger: OracleLedgerOwnerLedger): LedgerResult = OracleClaimer.apply(this, ledger)
  }

  trait OracleClaimerWithSign extends OracleClaimer {
    val signer: NodeIdentity
  }

  object OracleClaimer {
    def apply(aLedgerId: LedgerId,
              anInstigator: UniqueNodeIdentifier,
              someOwners: Seq[UniqueNodeIdentifier]): OracleClaimer = new OracleClaimer {
      override val ledgerId: LedgerId = aLedgerId
      override val instigator: UniqueNodeIdentifier = anInstigator
      override val otherOwners: Seq[UniqueNodeIdentifier] = someOwners
    }

    def apply(aLedgerId: LedgerId,
              aSigner: NodeIdentity,
              someOwners: Seq[UniqueNodeIdentifier]): OracleClaimerWithSign = new OracleClaimerWithSign {
      override val signer: NodeIdentity = aSigner
      override val ledgerId: LedgerId = aLedgerId
      override val instigator: UniqueNodeIdentifier = aSigner.id
      override val otherOwners: Seq[UniqueNodeIdentifier] = someOwners
    }

    def apply(o: OracleClaimer, oracleLEdger: OracleLedgerOwnerLedger): LedgerResult = {

      val claim = o.oracleClaim

      val stx =
        o match {
          case withSig: OracleClaimerWithSign =>
            TxSign.simplySignedTxEntry(withSig.signer, claim.txId, claim.toBytes).await()
          case noSig =>
            val sigs = Seq.empty
            SignedTxEntry(claim.toBytes, Seq(sigs).txSig)

        }

      val li = LedgerItem(
        ownerLedgerId,
        claim.txId,
        stx.toBytes
      )

      oracleLEdger.apply(
        li,
        BlockId(0, 0)
      )
    }

  }
}