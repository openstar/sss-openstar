package sss.openstar.oracleownerledger.serialize

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.openstar.ledger.LedgerId
import sss.openstar.oracleownerledger._

import scala.util.Random

/**
  * Created by alan on 2/15/16.
  */

class OracleTxSerializationSpec extends AnyFlatSpec with Matchers {

  val others = Seq("asdas", "asdaasdass", "asdasasda")
  val moreOthers = Seq("asds", "asdaasdas", "asdsda")

  "A Claim Tx" should " be parseable to bytes " in {
    val sut = OracleClaim(LedgerId(3.toByte), "adsasdasdasdas", others)
    val asBytes = sut.toBytes
    val backagain = asBytes.toOracleLedgerTx
    assert(backagain == sut)
  }

}
