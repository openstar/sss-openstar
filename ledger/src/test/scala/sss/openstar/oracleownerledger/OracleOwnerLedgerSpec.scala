package sss.openstar.oracleownerledger

import org.scalatest.concurrent.ScalaFutures
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.scalatest.time.{Millis, Minutes, Span}
import sss.openstar.ledger.{LedgerId, LedgerTxApplyError}

import scala.util.Success


class OracleOwnerLedgerSpec extends AnyFlatSpec with ScalaFutures with OracleOwnerLedgerFixture with Matchers {

  implicit override val patienceConfig: PatienceConfig =
    PatienceConfig(timeout = scaled(Span(1, Minutes)), interval = scaled(Span(25, Millis)))

  "An owner ledger" should "allow direct writes to set up chain wide genesis" in {
    val newOwners = Seq(owner1Id, "bbbbb")
    val tooManyNewOwners = (0 to maxAllowedOwnersPerLedger) map (i => owner1Id + owner1Id.take(i))
    // safe assumption as all ledgers used at this point
    val ledgerId =  LedgerId(1)
    val resultTooMany  = sut.setOwners(tooManyNewOwners, ledgerId)
    resultTooMany.isFailure shouldBe true

    val setOwners = sut.setOwners(newOwners, ledgerId)
    setOwners shouldBe Success(newOwners)
  }

  it should "claim un claimed ledger id" in {

    val result = OracleClaimer(LedgerId(2), nodeIdentity1, Seq.empty).run(sut)
    result should matchPattern { case Right(applied : OracleOwnerLedgerTxApplied) if applied.updatedOwners == Seq(owner1Id) => }

  }


  it should "not allow an unsigned ledger item" in {

    val result = OracleClaimer(LedgerId(3), owner1Id, Seq.empty).run(sut)
    result should matchPattern { case Left(_: LedgerTxApplyError) => }
  }


  it should "allow well signed ownership expansion" in {

    val result = OracleClaimer(LedgerId((maxAllowedOwnersPerLedger - 1).toByte),nodeIdentity1,
      Seq(owner2Id)
    ).run(sut)

    result should matchPattern { case Right(applied: OracleOwnerLedgerTxApplied) if applied.updatedOwners == Seq(owner1Id, owner2Id) => }
  }

  it should "not allow more than max owned ledgers" in {

    (0 until maxAllowedLedgers).foreach { id =>

      val result = OracleClaimer(LedgerId(id.toByte),nodeIdentity1,
        Seq(owner1Id)
      ).run(sut)
      result should matchPattern { case Right(applied: OracleOwnerLedgerTxApplied) if applied.updatedOwners == Seq(owner1Id) => }
    }

    val result = OracleClaimer(
      LedgerId((maxAllowedLedgers).toByte),
      owner1Id,
      Seq(owner1Id)
    ).run(sut)

    result should matchPattern { case Left(_: LedgerTxApplyError) => }
  }

  it should "allow max ledger owners" in {

    val rootId = "aaaaaaaaa"
    val newOwners = (1 until maxAllowedOwnersPerLedger) map ( i => rootId + rootId.take(i))

    val result = OracleClaimer(
      LedgerId(1.toByte),
      nodeIdentity1,
      newOwners
    ).run(sut)
    result should matchPattern { case Right(applied: OracleOwnerLedgerTxApplied) if applied.updatedOwners == Seq(owner1Id) ++ newOwners => }
  }

  it should "not allow more than max ledger owners" in {

    val rootId = "aaaaaaaaa"
    val newOwners = (0 to maxAllowedOwnersPerLedger) map ( i => rootId + rootId.take(i))

      val result = OracleClaimer(
        LedgerId(maxAllowedLedgers.toByte),
        nodeIdentity1,
        newOwners
      ).run(sut)

    result should matchPattern { case Left(_: LedgerTxApplyError) => }
  }

  it should "prevent direct writes where owners already exist (no overwriting allowed)" in {
    val newOwners = Seq("aaaaa", "bbbbb")
    // safe assumption as all ledgers used at this point
    val ledgerId =  LedgerId(1)
    val setOwners = sut.setOwners(newOwners, ledgerId)
    setOwners.isFailure shouldBe true
  }

}
