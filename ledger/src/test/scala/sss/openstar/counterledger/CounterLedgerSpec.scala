package sss.openstar.counterledger

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.openstar.LedgerTestOps.EitherToTry
import sss.openstar.chains.Chains.GlobalChainIdMask
import sss.openstar.common.block.BlockId
import sss.openstar.db.LedgerMigratedDbFixture
import sss.openstar.ledger.{LedgerId, LedgerItem, SignedTxEntry}

class CounterLedgerSpec extends AnyFlatSpec with Matchers with LedgerMigratedDbFixture {

  override implicit lazy val globalChainId: GlobalChainIdMask = 13.toByte
  implicit val ledgerId = LedgerId(99.toByte)
  val ledger = CounterLedger("FOO")
  import db.syncRunContext

  private def createLedgerItem(cmd: CounterLedgerTx) =
    LedgerItem(ledgerId, cmd.txId, SignedTxEntry(cmd.toBytes).toBytes)
  private def createPrintout = createLedgerItem(Printout())
  private def createFailOnNodes(nodes: String*) = createLedgerItem(FailOnTheseNodes(nodes))
  private def createIncrementFailure = createLedgerItem(IncrementFailure())

  "The counter ledger" should "be able to increment and printout" in {
    ledger(createFailOnNodes(), BlockId(0, 0)).get
    ledger(createPrintout, BlockId(0, 0)).get

    assert(ledger.successCounter().runSyncAndGet == 1)
    assert(ledger.failureCounter().runSyncAndGet == 0)

    ledger(createFailOnNodes(), BlockId(0, 0)).get
    ledger(createIncrementFailure, BlockId(0, 0)).get

    assert(ledger.successCounter().runSyncAndGet == 2)
    assert(ledger.failureCounter().runSyncAndGet == 1)

    ledger(createPrintout, BlockId(0, 0)).get
  }

  it should "be able to fail tx on specified node id" in {
    intercept[IllegalArgumentException] {
      ledger(createFailOnNodes("FOO", "BAR"), BlockId(0, 0)).get
    }
  }
}
