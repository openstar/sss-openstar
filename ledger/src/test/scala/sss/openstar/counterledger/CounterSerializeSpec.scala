package sss.openstar.counterledger

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class CounterSerializeSpec extends AnyFlatSpec with Matchers {

  "FailOnTheseNodes" should "serialize and deserialize" in {
    val expected = FailOnTheseNodes(Seq("FOO", "BAR"))
    val actual = expected.toBytes.toFailOnTheseNodes
    assert(expected == actual)

    val expectedEmpty = FailOnTheseNodes(Seq.empty)
    val actualEmpty = expectedEmpty.toBytes.toFailOnTheseNodes
    assert(expectedEmpty == actualEmpty)
  }

  "IncrementFailure" should "serialize and deserialize" in {
    val expected = IncrementFailure()
    val actual = expected.toBytes.toIncrementFailure
    assert(expected == actual)
  }

  "Printout" should "serialize and deserialize" in {
    val expected = Printout()
    val actual = expected.toBytes.toPrintout
    assert(expected == actual)
  }

}
