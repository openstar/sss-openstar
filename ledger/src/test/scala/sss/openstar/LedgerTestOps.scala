package sss.openstar

import sss.openstar.ledger.{LedgerTxApplyError, TxApplied}

object LedgerTestOps {

  implicit class EitherToTry(val e: Either[LedgerTxApplyError, TxApplied]) extends AnyVal {
    def get: TxApplied = e match {
      case Left(p) => throw new IllegalArgumentException(p.toString)
      case Right(s) => s
    }
  }
}
