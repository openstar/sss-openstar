package sss.openstar.systemledger

import sss.db.{Db, FutureTx}
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.systemledger.SystemService.UpdateProposal

import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext, Future}

object SystemServiceTestHelper {

  implicit class Ops(val systemService: SystemService) extends AnyVal {
    def deleteExistingProposals(implicit ec:ExecutionContext, db: Db): Unit = {
      Await.ready(systemService.listProposals(None)
        .flatMap(allProposals =>
          Future.sequence(
            allProposals.map(p => systemService.deleteProposal(p.uniqueUpdateIdentifier).dbRun)
          )
        )
      , 1.minutes)
    }
    def insertUpdateProposal(updateProposal: UpdateProposal)(implicit db: Db): UpdateProposal = {
      import updateProposal._
      systemService.addUpdateProposal(proposingNodeId,uniqueUpdateIdentifier, url, checksum, requiredFromBlockHeight).dbRunSyncGet
    }
  }
}
