package sss.openstar.systemledger.serialize

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.openstar.DummySeedBytes
import sss.openstar.hash.Digest32
import sss.openstar.systemledger.{CancelUpdate, CancelUpdateCode, PreparedToUpdate, SystemLedgerTxFromBytes, UpdateNotification}

/**
  * Created by alan on 2/15/16.
  */

class SystemLedgerSerializationSpec extends AnyFlatSpec with Matchers {

  "A PreparedToUpdate Tx" should "be parseable to bytes" in {
    val replayH = Long.MaxValue
    val uId = "asdasdasdasdasdasdar"
    val sut = PreparedToUpdate(replayH, uId)
    val asBytes = sut.toBytes
    val backagain = asBytes.toSystemLedgerTx
    assert(backagain == sut)
  }

  "An UpdateNotification Tx" should "be parseable to bytes" in {
    val replayPreventionBlockHeight: Long = 989898
    val blockHeight: Long = 98
    val uniqueIdentifier: String = "sdfsdfsdfsdfsdfsdfsdfsf"
    val url: String = "https://asdasdasdasd.asdasdasdasda.?asdasdasdasda"
    val checksum: Digest32 = Digest32(DummySeedBytes(32))
    val sut = UpdateNotification(replayPreventionBlockHeight, blockHeight, uniqueIdentifier, url, checksum)
    val asBytes = sut.toBytes
    val backagain = asBytes.toSystemLedgerTx
    assert(backagain == sut)

  }

  "An CancelNotification Tx" should "be parseable to bytes" in {
    val replayPreventionBlockHeight: Long = 989898
    val uniqueIdentifier: String = "sdfsdfsdfsdfsdfsdfsdfsf"
    val sut = CancelUpdate(replayPreventionBlockHeight, uniqueIdentifier)
    val asBytes = sut.toBytes
    val backagain = asBytes.toSystemLedgerTx
    assert(backagain == sut)
  }
}
