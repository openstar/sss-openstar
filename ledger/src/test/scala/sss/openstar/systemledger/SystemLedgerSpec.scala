package sss.openstar.systemledger

import org.scalatest.concurrent.ScalaFutures
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.ancillary.ByteArrayComparisonOps
import sss.ancillary.FutureOps.AwaitResult
import sss.db.FutureTx
import sss.openstar.DummySeedBytes
import sss.openstar.account.{NodeIdTag, NodeIdentity, NodeIdentityManagerFixture}
import sss.openstar.balanceledger.OracleBalanceLedger.OracleLedgerOwnerLookupFTx
import sss.openstar.common.block.BlockId
import sss.openstar.common.builders.CpuBoundExecutionContextFixture
import sss.openstar.db.LedgerMigratedDbFixture
import sss.openstar.hash.Digest32
import sss.openstar.ledger.{LedgerId, LedgerItem, LedgerResult, LedgerTxApplyError, SignedTxEntry, TxId}
import sss.openstar.systemledger.SystemLedger._
import sss.openstar.systemledger.SystemService.UpdateProposal

import scala.concurrent.Future



class SystemLedgerSpec extends AnyFlatSpec
  with Matchers
  with ScalaFutures
  with ByteArrayComparisonOps
  with LedgerMigratedDbFixture
  with NodeIdentityManagerFixture
  with CpuBoundExecutionContextFixture {

  val proposalUniqueId = "version2.0"

  val url = "https://github.com/asdasdasdasd?asdasdasdasdasdasd"
  val updateChecksum = Digest32(DummySeedBytes(32))

  private def createCancelTx:(BlockId, CancelUpdate) = {
    val replayHeight = 1
    val blockId = BlockId(replayHeight, 1)
    val prepared = CancelUpdate(replayHeight, proposalUniqueId)
    (blockId, prepared)
  }

  private def createPreparedToUpdateTx:(BlockId, PreparedToUpdate) = {
    val replayHeight = 1
    val blockId = BlockId(replayHeight, 1)
    val prepared = PreparedToUpdate(replayHeight, proposalUniqueId)
    (blockId, prepared)
  }

  private def createUpdateNotificationTx(replayHeight: Long = 1):(BlockId, UpdateNotification) = {

    val proposalUniqueId = "version3.0"
    val requiredFromBlockHeight: Long = 2
    val blockId = BlockId(replayHeight,1)
    val tx = UpdateNotification(replayHeight, requiredFromBlockHeight, proposalUniqueId, url, updateChecksum)
    (blockId, tx)
  }

  private def applyPreparedToUpdate(
                    proposalUniqueId: String,
                    nodeIdentity: NodeIdentity,
                    replayHeight: Long = 1,
                    blockId: BlockId = BlockId(1,1))(implicit ledgerId: LedgerId, sut: SystemLedger): (TxId, LedgerResult) = {

    val prepared = PreparedToUpdate(replayHeight, proposalUniqueId)
    val stxPrepared: SignedTxEntry = toSignedSystemLedgerTxEntry(nodeIdentity, prepared).await()
    val ledgerItemPrepared = LedgerItem(ledgerId.id, stxPrepared.txId, stxPrepared.toBytes)

    (stxPrepared.txId, sut(ledgerItemPrepared, blockId))

  }

  private def makeGetVerifier(seqNodes: Seq[NodeIdentity]): GetVerifier = nodeIdTag => _ =>  Future.successful {
    seqNodes
      .find(node => NodeIdTag(node.id, node.tag) == nodeIdTag)
      .map(_.defaultNodeVerifier)
  }

  "A system update ledger" should "have 'performUpdate' marked false until all owners are prepared" in new SystemLedgerFixture {

    val allNodeIds@Seq(owner1, owner2, owner3, notAnOwner) =
      Seq(
        NodeIdTag("nodeone"),
        NodeIdTag("nodetwo"),
        NodeIdTag("nodethree"),
        NodeIdTag("nodefour")
      )

    val allNodes@ Seq(owner1Node, owner2Node, owner3Node, notAnOwnerNode) =
      allNodeIds.map(n => addToFixtureAndGetNodeIdentity(n.nodeId))

    override lazy val getVerifier: GetVerifier = makeGetVerifier(allNodes)

    override lazy val ownerLookup: OracleLedgerOwnerLookupFTx = createOwnerLookup(owner2.nodeId, owner1.nodeId, owner3.nodeId)

    override implicit val nodeIdentityForSigningTxs: NodeIdentity = owner1Node

    val proposalUniqueId = "version3.0"
    val replayHeight = 1
    val requiredFromBlockHeight: Long = 2
    val blockId = BlockId(replayHeight,1)
    val tx: SystemLedgerTx = UpdateNotification(replayHeight, requiredFromBlockHeight, proposalUniqueId, url, updateChecksum)
    val stx: SignedTxEntry = toSignedSystemLedgerTxEntry(owner3Node, tx).await()

    val ledgerItem = LedgerItem(ledgerId.id, stx.txId, stx.toBytes)

    val w = sut(ledgerItem, blockId)

    val (txId1, result1) = applyPreparedToUpdate(proposalUniqueId, owner1Node, replayHeight, blockId)
    val resultTxAppliedPrepared1 = NewUpdateAckTxApplied(txId1, proposalUniqueId, false)
    result1 should matchPattern {
      case Right(`resultTxAppliedPrepared1`) =>
    }

    val (_, resultNotOwner) = applyPreparedToUpdate(proposalUniqueId, notAnOwnerNode, replayHeight, blockId)
    shouldBeErr(resultNotOwner, SystemLedgerErrorCode.NotOwner)


    val (txId2, result2) = applyPreparedToUpdate(proposalUniqueId, owner2Node, replayHeight, blockId)
    val resultTxAppliedPrepared2 = NewUpdateAckTxApplied(txId2, proposalUniqueId, false)
    result2 should matchPattern {
      case Right(`resultTxAppliedPrepared2`)  =>
    }

    val (txId3, result3) = applyPreparedToUpdate(proposalUniqueId, owner3Node, replayHeight, blockId)
    val resultTxAppliedPrepared3 = NewUpdateAckTxApplied(txId3, proposalUniqueId, true)
    result3 should matchPattern {
      case Right(`resultTxAppliedPrepared3`) =>
    }
  }


  it should "accept new proposal from a single owner" in new SystemLedgerFixture {

    override lazy val ownerLookup: OracleLedgerOwnerLookupFTx = createOwnerLookup(nodeIdentityForSigningTxs.id)

    override implicit val nodeIdentityForSigningTxs: NodeIdentity = SystemLedgerSpec.this.nodeIdentity

    override val getVerifier: GetVerifier = makeGetVerifier(Seq(nodeIdentityForSigningTxs))

    val replayHeight = 1
    val requiredFromBlockHeight: Long = 2
    val blockId = BlockId(replayHeight,1)
    val tx: SystemLedgerTx = UpdateNotification(replayHeight, requiredFromBlockHeight, proposalUniqueId, url, updateChecksum)
    val stx: SignedTxEntry = toSignedSystemLedgerTxEntry(nodeIdentityForSigningTxs, tx).await()

    val ledgerItem = LedgerItem(ledgerId.id, stx.txId, stx.toBytes)

    val result = sut(ledgerItem, blockId)

    val newProposal = UpdateProposal(
      nodeIdentityForSigningTxs.id,
      proposalUniqueId,
      url,
      updateChecksum,
      requiredFromBlockHeight,
      false)

    val resultTxApplied = NewUpdateProposalTxApplied(tx.txId, newProposal)
    result should matchPattern {
      case Right(`resultTxApplied`) =>
    }

    val prepared = PreparedToUpdate(replayHeight, proposalUniqueId)
    val stxPrepared: SignedTxEntry = toSignedSystemLedgerTxEntry(nodeIdentityForSigningTxs, prepared).await()
    val ledgerItemPrepared = LedgerItem(ledgerId.id, stxPrepared.txId, stxPrepared.toBytes)

    val resultTxAppliedPrepared = NewUpdateAckTxApplied(prepared.txId, proposalUniqueId, true)

    sut(ledgerItemPrepared, blockId) should matchPattern {
      case Right(`resultTxAppliedPrepared`) =>
    }

  }

  it should "cancel a proposal from an owner" in new SystemLedgerFixture {

    override lazy val ownerLookup: OracleLedgerOwnerLookupFTx = createOwnerLookup(nodeIdentityForSigningTxs.id)

    override implicit val nodeIdentityForSigningTxs: NodeIdentity = SystemLedgerSpec.this.nodeIdentity

    override val getVerifier: GetVerifier = makeGetVerifier(Seq(nodeIdentityForSigningTxs))

    val replayHeight = 1
    val requiredFromBlockHeight: Long = 2
    val blockId = BlockId(replayHeight,1)
    val tx: SystemLedgerTx = UpdateNotification(replayHeight, requiredFromBlockHeight, proposalUniqueId, url, updateChecksum)
    val stx: SignedTxEntry = toSignedSystemLedgerTxEntry(nodeIdentityForSigningTxs, tx).await()

    val ledgerItem = LedgerItem(ledgerId.id, stx.txId, stx.toBytes)

    sut(ledgerItem, blockId)


    val cancelPropsal = CancelUpdate(
      replayHeight,
      proposalUniqueId,
      )
    val sTxCancel: SignedTxEntry = toSignedSystemLedgerTxEntry(nodeIdentityForSigningTxs, cancelPropsal).await()
    val ledgerItemCancel = LedgerItem(ledgerId.id, sTxCancel.txId, sTxCancel.toBytes)
    val resultCancel = sut(ledgerItemCancel, blockId)

    val resultTxApplied = CancelUpdateTxApplied(cancelPropsal.txId, cancelPropsal.uniqueIdentifier)
    resultCancel should matchPattern {
      case Right(`resultTxApplied`) =>
    }

    val resultCancel2 = sut(ledgerItemCancel, blockId)

    // the second time no such proposal exists
    resultCancel2 should matchPattern {
      case Left(_: LedgerTxApplyError) =>
    }
  }

  it should "reject tx's if ledger has no owners" in new SystemLedgerFixture {

    override implicit val nodeIdentityForSigningTxs: NodeIdentity = SystemLedgerSpec.this.nodeIdentity

    override val getVerifier: GetVerifier = makeGetVerifier(Seq(nodeIdentityForSigningTxs))

    override lazy val ownerLookup: OracleLedgerOwnerLookupFTx = createOwnerLookup()

    checkRejectedIfLedgerHasNoOwners(createUpdateNotificationTx())

    checkRejectedIfLedgerHasNoOwners(createCancelTx)

    checkRejectedIfLedgerHasNoOwners(createPreparedToUpdateTx)

  }

  it should "reject new proposal if not from a ledger owner" in new SystemLedgerFixture {

    val randomOwner = addToFixtureAndGetNodeIdentity("notdefaultowner")

    override implicit val nodeIdentityForSigningTxs: NodeIdentity = SystemLedgerSpec.this.nodeIdentity

    override val getVerifier: GetVerifier = makeGetVerifier(Seq(nodeIdentityForSigningTxs))

    override lazy val ownerLookup: OracleLedgerOwnerLookupFTx = createOwnerLookup(randomOwner.id)

    checkRejectedIfNotFromLedgerOwner( createUpdateNotificationTx())

    checkRejectedIfNotFromLedgerOwner(createPreparedToUpdateTx)

    checkRejectedIfNotFromLedgerOwner(createCancelTx)

  }


   it should "reject tx's if block height mismatch" in new SystemLedgerFixture {

     override implicit val nodeIdentityForSigningTxs: NodeIdentity = SystemLedgerSpec.this.nodeIdentity
     override val getVerifier: GetVerifier = makeGetVerifier(Seq(nodeIdentityForSigningTxs))

     val replayHeight = 1
     val prepared = PreparedToUpdate(replayHeight, proposalUniqueId)
     checkRejectedIfHeightsDontMatch(replayHeight, prepared)

     val cancelTx = CancelUpdate(replayHeight, proposalUniqueId)
     checkRejectedIfHeightsDontMatch(replayHeight, cancelTx)

     val (_, notifyTx) = createUpdateNotificationTx(replayHeight)

     checkRejectedIfHeightsDontMatch(replayHeight, notifyTx)
  }

  it should "reject new 'prepared to update' if not from an owner" in new SystemLedgerFixture {

    override lazy val ownerLookup: OracleLedgerOwnerLookupFTx = createOwnerLookup(nodeIdentityForSigningTxs.id)

    override implicit val nodeIdentityForSigningTxs: NodeIdentity = SystemLedgerSpec.this.nodeIdentity
    override val getVerifier: GetVerifier = makeGetVerifier(Seq(nodeIdentityForSigningTxs))

    val notOwner = addToFixtureAndGetNodeIdentity("someoneelse")

    checkRejectedIfNotFromAnOwner(createPreparedToUpdateTx, notOwner)

    checkRejectedIfNotFromAnOwner(createCancelTx, notOwner)

    checkRejectedIfNotFromAnOwner( createUpdateNotificationTx(), notOwner)

  }

  it should "reject all tx's if not signed" in new SystemLedgerFixture {

    override lazy val ownerLookup: OracleLedgerOwnerLookupFTx = createOwnerLookup(nodeIdentityForSigningTxs.id)

    override implicit val nodeIdentityForSigningTxs: NodeIdentity = SystemLedgerSpec.this.nodeIdentity
    override val getVerifier: GetVerifier = makeGetVerifier(Seq(nodeIdentityForSigningTxs))

    val notOwner = addToFixtureAndGetNodeIdentity(scala.util.Random.alphanumeric.take(10).toString)

    checkRejectIfNotSigned(createPreparedToUpdateTx, notOwner)

    checkRejectIfNotSigned(createUpdateNotificationTx(), notOwner)

    checkRejectIfNotSigned(createCancelTx, notOwner)

  }

  it should "reject tx's if badly signed" in new SystemLedgerFixture {

    override lazy val ownerLookup: OracleLedgerOwnerLookupFTx = createOwnerLookup(nodeIdentityForSigningTxs.id)

    override implicit val nodeIdentityForSigningTxs: NodeIdentity = SystemLedgerSpec.this.nodeIdentity
    override val getVerifier: GetVerifier = makeGetVerifier(Seq(nodeIdentityForSigningTxs))

    checkBadSignatureRejected(createPreparedToUpdateTx, nodeIdentityForSigningTxs)

    checkBadSignatureRejected(createCancelTx, nodeIdentityForSigningTxs)

    checkBadSignatureRejected(createPreparedToUpdateTx, nodeIdentityForSigningTxs)

  }

  it should "reject any tx from a non existent user" in new SystemLedgerFixture {

    override lazy val ownerLookup: OracleLedgerOwnerLookupFTx = createOwnerLookup(nodeIdentityForSigningTxs.id)

    override implicit val nodeIdentityForSigningTxs: NodeIdentity = SystemLedgerSpec.this.nodeIdentity

    override lazy val getVerifier: GetVerifier = _ => FutureTx.unit(
      None
    )

    checkNonExistentUserRejected(createCancelTx)
    checkNonExistentUserRejected(createPreparedToUpdateTx)
    checkNonExistentUserRejected(createUpdateNotificationTx())

  }
}
