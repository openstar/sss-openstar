package sss.openstar.systemledger

import org.scalatest.concurrent.ScalaFutures
import org.scalatest.concurrent.ScalaFutures.convertScalaFuture
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper
import org.scalatest.matchers.should.Matchers.matchPattern
import sss.ancillary.FutureOps.AwaitResult
import sss.db.{Db, FutureTx}
import sss.openstar.account.NodeIdentity
import sss.openstar.balanceledger.OracleBalanceLedger.OracleLedgerOwnerLookupFTx
import sss.openstar.common.block.BlockId
import sss.openstar.ledger.{LedgerId, LedgerItem, LedgerResult, LedgerTxApplyError, SignedTxEntry}
import sss.openstar.systemledger.SystemLedger.{GetVerifier, toSignedSystemLedgerTxEntry}
import sss.openstar.systemledger.SystemLedgerErrorCode.SystemLedgerErrorCode
import sss.openstar.UniqueNodeIdentifier
import sss.openstar.account.Ops.MakeTxSigArys
import sss.openstar.common.builders.CpuBoundExecutionContextFixture

trait SystemLedgerFixture
    extends CpuBoundExecutionContextFixture {

  lazy implicit val db = Db()
  lazy implicit val ledgerId = LedgerId(88.toByte)

  val nodeIdentityForSigningTxs: NodeIdentity

  def createOwnerLookup(owners: UniqueNodeIdentifier*): OracleLedgerOwnerLookupFTx =  {
    case `ledgerId` => FutureTx.unit(owners)
    case _ => throw new IllegalArgumentException("Use the correct ledger Id")
  }

  lazy val systemService = new SystemService()
  val getVerifier: GetVerifier

  lazy val ownerLookup: OracleLedgerOwnerLookupFTx = _ => FutureTx.unit(Seq())
  implicit lazy val sut = new SystemLedger(systemService, getVerifier, ownerLookup)

  def corruptSignature(signatures: Seq[Array[Byte]]): Seq[Array[Byte]] =
    // corrupts only the last 2 elements
    signatures.take(2) ++ signatures.drop(2).map(_.tail)


  def checkNonExistentUserRejected(tup: (BlockId, SystemLedgerTx)) : Unit =
    checkNonExistentUserRejected(tup._1, tup._2)


  def checkNonExistentUserRejected(blockId: BlockId, systemLedgerTx: SystemLedgerTx): Unit = {

    val stxPrepared: SignedTxEntry = toSignedSystemLedgerTxEntry(nodeIdentityForSigningTxs, systemLedgerTx).futureValue

    val ledgerItemPrepared = LedgerItem(ledgerId.id, stxPrepared.txId, stxPrepared.toBytes)
    val result = sut(ledgerItemPrepared, blockId)

    shouldBeErr(result, SystemLedgerErrorCode.NoSuchUser)
  }

  def checkRejectIfNotSigned(tup: (BlockId, SystemLedgerTx),
                             notOwner: NodeIdentity): Unit = {
    checkRejectIfNotSigned(tup._1, tup._2, notOwner)
  }

  def checkRejectIfNotSigned(blockId: BlockId,
                             tx: SystemLedgerTx,
                             notOwner: NodeIdentity): Unit = {

    val stxPrepared: SignedTxEntry = toSignedSystemLedgerTxEntry(notOwner, tx).futureValue
    val stxPreparedNoSigs = stxPrepared.copy(signatures = sss.openstar.ledger.TxSig(Seq()))
    val ledgerItemPrepared = LedgerItem(ledgerId.id, stxPreparedNoSigs.txId, stxPreparedNoSigs.toBytes)
    val result = sut(ledgerItemPrepared, blockId)

    shouldBeErr(result, SystemLedgerErrorCode.NoSignatures)
  }

  def shouldBeErr(result: LedgerResult, err: SystemLedgerErrorCode): Unit = {
    val errStr = err.toString
    result should matchPattern {
      case Left(LedgerTxApplyError(`errStr`, _)) =>
    }

  }


  def checkBadSignatureRejected(
                                 tup: (BlockId, SystemLedgerTx),
                                 nodeIdentity: NodeIdentity): Unit = {

    val (blockId: BlockId, tx: SystemLedgerTx) = tup

    val stx: SignedTxEntry = toSignedSystemLedgerTxEntry(nodeIdentity, tx).futureValue
    //drop a single byte from the head of the signature
    val badSig = corruptSignature(stx.signatures.sigs.head)
    val stxPreparedBadSig = stx.copy(signatures = Seq(badSig).txSig)

    val ledgerItemPrepared = LedgerItem(ledgerId.id, stxPreparedBadSig.txId, stxPreparedBadSig.toBytes)
    val result = sut(ledgerItemPrepared, blockId)

    shouldBeErr(result, SystemLedgerErrorCode.BadSignature)

  }

  def checkRejectedIfNotFromAnOwner(tup: (BlockId, SystemLedgerTx),
                                    notOwner: NodeIdentity): Unit = {

    val (blockId: BlockId, tx: SystemLedgerTx) = tup
    val sTx: SignedTxEntry = toSignedSystemLedgerTxEntry(notOwner, tx).futureValue
    val ledgerItemPrepared = LedgerItem(ledgerId.id, sTx.txId, sTx.toBytes)
    val result = sut(ledgerItemPrepared, blockId)

    shouldBeErr(result, SystemLedgerErrorCode.NotOwner)

  }

  def checkRejectedIfHeightsDontMatch(replayHeight: Long, tx: SystemLedgerTx): Unit = {
    val blockId = BlockId(replayHeight+1,1) // block not same as replay block height


    val stxPrepared: SignedTxEntry = toSignedSystemLedgerTxEntry(nodeIdentityForSigningTxs, tx).futureValue
    val ledgerItemPrepared = LedgerItem(ledgerId.id, stxPrepared.txId, stxPrepared.toBytes)
    val result = sut(ledgerItemPrepared, blockId)

    shouldBeErr(result, SystemLedgerErrorCode.ReplayAttackPrevention)
  }

  def checkRejectedIfLedgerHasNoOwners(tup: (BlockId, SystemLedgerTx)): Unit = {

    val (blockId: BlockId, tx: SystemLedgerTx) = tup

    val stx: SignedTxEntry = toSignedSystemLedgerTxEntry(nodeIdentityForSigningTxs, tx).await()

    val ledgerItem = LedgerItem(ledgerId.id, stx.txId, stx.toBytes)

    val result = sut(ledgerItem, blockId)

    shouldBeErr(result, SystemLedgerErrorCode.NotOwner)

  }

  def checkRejectedIfNotFromLedgerOwner(tup: (BlockId, SystemLedgerTx)): Unit = {

    val (blockId: BlockId, tx: SystemLedgerTx) = tup
    val stx: SignedTxEntry = toSignedSystemLedgerTxEntry(nodeIdentityForSigningTxs, tx).await()

    val ledgerItem = LedgerItem(ledgerId.id, stx.txId, stx.toBytes)

    val result = sut(ledgerItem, blockId)

    shouldBeErr(result, SystemLedgerErrorCode.NotOwner)
  }
}

