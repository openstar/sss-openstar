package sss.openstar.systemledger

import org.scalatest.concurrent.ScalaFutures.convertScalaFuture
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.ancillary.ByteArrayComparisonOps
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.DummySeedBytes
import sss.openstar.db.LedgerMigratedDbFixture
import sss.openstar.hash.Digest32
import sss.openstar.systemledger.SystemServiceTestHelper.Ops

class SystemServiceSpec extends AnyFlatSpec
  with Matchers
  with ByteArrayComparisonOps
  with LedgerMigratedDbFixture {

  import db.asyncRunContext.ec

  val sut = new SystemService()
  val proposingNodeId = "wertyuio"
  val preparingNodeIda = "preparingnodea"
  val preparingNodeIdb = "preparingnodeb"

  val proposalUniqueId = "SystemServiceSpec"
  val url = "https://github.com/asdasdasdasd?asdasdasdasdasdasd"
  val mandatoryToUpdateBefore: Long = 9999
  val updateChecksum = Digest32(DummySeedBytes(32))

  override def beforeAll(): Unit = {
    super.beforeAll()
    sut.deleteExistingProposals
  }

  "A system update service" should "accept a new proposal" in {

    sut.listProposals().futureValue shouldBe Seq.empty
    sut.nextProposal(proposalUniqueId).failed.futureValue shouldBe an[IllegalArgumentException]

    val upProposal = sut.addUpdateProposal(
      proposingNodeId, proposalUniqueId, url, updateChecksum, mandatoryToUpdateBefore
    ).dbRunSyncGet

    assert(upProposal.proposingNodeId == proposingNodeId)
    assert(upProposal.uniqueUpdateIdentifier == proposalUniqueId)
    assert(upProposal.url == url)
    assert(upProposal.requiredFromBlockHeight == mandatoryToUpdateBefore)
    assert(upProposal.checksum == updateChecksum)
    assert(!upProposal.updatePreparationComplete)

    sut.listProposals().futureValue shouldBe Seq(upProposal)
    sut.nextProposal(proposalUniqueId).futureValue shouldBe None
  }

  it should "not allow another with the same id" in {
    val upProposal = sut.addUpdateProposal(
      "proposingNodeId", proposalUniqueId, "url", updateChecksum, mandatoryToUpdateBefore
    ).dbRunSync

    assert(upProposal.isFailure)
  }

  it should "allow a node to record preparation" in {

    var signatories =  sut.getSignatories(proposalUniqueId).futureValue
    signatories shouldBe Seq.empty

    var seqPrepared = sut.addPreparedNode(
      preparingNodeIda, proposalUniqueId
    ).dbRunSyncGet

    signatories = sut.getSignatories(proposalUniqueId).futureValue
    assert(signatories == Seq(preparingNodeIda))
    assert(seqPrepared == Seq(preparingNodeIda))

    seqPrepared = sut.addPreparedNode(
      preparingNodeIdb, proposalUniqueId
    ).dbRunSyncGet

    signatories = sut.getSignatories(proposalUniqueId).futureValue
    signatories should contain theSameElementsAs Seq(preparingNodeIdb, preparingNodeIda)
    seqPrepared should contain theSameElementsAs Seq(preparingNodeIdb, preparingNodeIda)
  }

  it should "not be complete until marked complete" in {
    var isComplete: Boolean = sut.isUpdatePreparationCompleteFTx(proposalUniqueId).dbRunSyncGet
    assert(!isComplete)
    sut.updatePreparationComplete(proposalUniqueId).dbRunSyncGet
    isComplete = sut.isUpdatePreparationCompleteFTx(proposalUniqueId).dbRunSyncGet
    assert(isComplete)
  }

  it should "no longer available if deleted" in {
    val isDeleted: Boolean = sut.deleteProposal(proposalUniqueId).dbRunSyncGet
    assert(isDeleted)
    val queryShouldFail = sut.isUpdatePreparationCompleteFTx(proposalUniqueId).dbRunSync
    assert(queryShouldFail.isFailure)
  }

}
