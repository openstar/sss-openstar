package sss.openstar.db

import org.scalatest.Suite
import sss.openstar.chains.Chains.{GlobalChainIdBuilder, GlobalChainIdMask}
import sss.openstar.ledger.LedgerId
import sss.openstar.schemamigration.SchemaMigrationBuilder
import sss.openstar.schemamigration.dynamic.GlobalTableNameTag
import sss.openstar.schemamigration.dynamic.UtxoDbStorageMigration.UtxoDbStorageId

trait LedgerMigratedDbFixture extends DbFixture with GlobalChainIdBuilder with SchemaMigrationBuilder {
  self: Suite =>

  protected lazy val utxoDbStorageLedgerIds: Set[LedgerId] = Set(LedgerId(1))
  protected implicit def globalTableNameTag(implicit chainId: GlobalChainIdMask): GlobalTableNameTag = GlobalTableNameTag(chainId.toString)
  override protected val tablePartitionsCount: Int = 100

  override def beforeAll(): Unit = {
    migrateSqlSchema()
    super.beforeAll()
  }

  override def migrateSqlSchema(): Unit =
    migrateSqlSchema(utxoDbStorageLedgerIds.map(id => UtxoDbStorageId(id.id)))
}
