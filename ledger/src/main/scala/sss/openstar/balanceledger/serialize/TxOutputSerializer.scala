package sss.openstar.balanceledger.serialize


import sss.ancillary.Serialize.{ByteArrayDeSerialize, ByteArraySerializer, LongDeSerialize, LongSerializer, SerializeHelper, Serializer}
import sss.openstar.balanceledger._
import sss.openstar.contract.ContractSerializer._

object TxOutputSerializer extends Serializer[TxOutput]{

  override def toBytes(t: TxOutput): Array[Byte] = {
    LongSerializer(t.amount) ++
      ByteArraySerializer(t.encumbrance.toBytes).toBytes
  }

  override def fromBytes(b: Array[Byte]): TxOutput = {

    val extracted = b.extract(
    LongDeSerialize, ByteArrayDeSerialize(_.toEncumbrance))

    TxOutput(extracted._1, extracted._2)
  }
}

