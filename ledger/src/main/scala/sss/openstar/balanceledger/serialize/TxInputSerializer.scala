package sss.openstar.balanceledger.serialize

import sss.ancillary.Serialize.{ByteArrayDeSerialize, ByteArraySerializer, LongDeSerialize, LongSerializer, SerializeHelper, Serializer}
import sss.openstar.balanceledger.{TxIndexFrom, TxInput}
import sss.openstar.contract.ContractSerializer._
import sss.openstar.util.Amount

/**
  * Copyright Stepping Stone Software Ltd. 2016, all rights reserved. 
  * mcsherrylabs on 3/3/16.
  */
object TxInputSerializer extends Serializer[TxInput]{

  override def toBytes(t: TxInput): Array[Byte] = {

    ByteArraySerializer(t.txIndex.toBytes) ++
      LongSerializer(t.amount) ++
      ByteArraySerializer(t.sig.toBytes).toBytes

  }

  override def fromBytes(b: Array[Byte]): TxInput = {
    TxInput tupled b.extract(
      ByteArrayDeSerialize(_.toTxIndex),
      LongDeSerialize,
    ByteArrayDeSerialize(_.toDecumbrance))

  }
}
