package sss.openstar.balanceledger.db

import sss.ancillary.{Logging, PagesToStream}
import sss.db.RowSerializer._
import sss.db._
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.balanceledger.{TxIndex, TxOutput, TxOutputFrom}
import sss.openstar.hash.Digest32
import sss.openstar.ledger.LedgerId
import sss.openstar.schemamigration.dynamic.UtxoDbStorageMigration.UtxoDbStorageId
import sss.openstar.schemamigration.dynamic.{GlobalTableNameTag, UtxoDbStorageMigration}
import sss.openstar.util.StringCheck.SimpleTag
import sss.openstar.util.{HashUtils, TableCopy}

import scala.util.Try

private[balanceledger] class UtxoDbStorage private (val currentStateTableName: String)(implicit db: Db)
  extends Logging
  with TableCopy {

  private val utxoPk = UtxoDbStorageMigration.utxoPk(currentStateTableName)

  private def addIndexSql: String = UtxoDbStorageMigration.addIndexSql(currentStateTableName)

  private def dropIndexSql: String = UtxoDbStorageMigration.dropIndexSql(currentStateTableName)

  private def createSnapshotTableName(tag: SimpleTag): String = s"${currentStateTableName}_$tag"

  def dropPKConstraintSql: String = s"ALTER TABLE $currentStateTableName DROP CONSTRAINT ${UtxoDbStorageMigration.utxoPkConstraintName(currentStateTableName)}"
  def addPKConstraintSql: String = s"ALTER TABLE $currentStateTableName ADD $utxoPk"

  private val utxoLedgerTable = db.table(currentStateTableName)

  def count: Long = utxoLedgerTable.count.dbRunSyncGet

  import sss.openstar.schemamigration.dynamic.UtxoDbStorageMigration.ColumnNames._

  private def toTxIndex(row: Row): TxIndex = TxIndex(row.arrayByte(txIdCol), row.int(indxCol))

  def keys(pageSize: Int): LazyList[TxIndex] = PagesToStream(
    pageGenerator(utxoLedgerTable, toTxIndex, Seq(OrderAsc(txIdCol), OrderAsc(indxCol))),
    pageSize)



  def importState(height: Long): Try[Unit] =  {
    val tableToImport = createSnapshotTableName(height)
    for {
      _ <- db.table(tableToImport).count // cause instant fail if it doesn't exist
      _ <- db.executeSqls(Seq(dropPKConstraintSql, dropIndexSql))
      _ <- replaceTable (currentStateTableName, tableToImport)
      _ <- db.executeSqls(Seq(addPKConstraintSql, addIndexSql))
    } yield ()
  }.dbRunSync


  def createCheckpoint(id: Long): Try[Digest32] = {
    for {
      _ <- db.executeSqls(Seq(dropPKConstraintSql, dropIndexSql))
      newTableName = createSnapshotTableName(id)
      newTable <- copyTable(currentStateTableName, newTableName)
      _ <- db.executeSqls(Seq(addIndexSql, addPKConstraintSql))

    } yield newTable

  }.dbRunSync.flatMap { newTable =>
    HashUtils(pageGenerator(newTable, _.toBytes(newTable.columnsMetaInfo), Seq(OrderAsc(txIdCol), OrderAsc(indxCol))) _)
  }

  def stateDigest(): Try[Digest32] = HashUtils(
    pageGenerator(utxoLedgerTable, _.toBytes(utxoLedgerTable.columnsMetaInfo),Seq(OrderAsc(txIdCol), OrderAsc(indxCol))) _
  )

  def deleteCheckpoint(height: Long): Try[Unit] =  {
    val tableName = createSnapshotTableName(height)
    db.executeSql(s"DROP TABLE $tableName;").dbRunSync.map(_ => ())
  }

  def entries(pageSize: Int = 500): LazyList[TxOutput] =
    PagesToStream(pageGenerator(utxoLedgerTable, _.arrayByte(entryCol).toTxOutput,
      Seq(OrderAsc(txIdCol), OrderAsc(indxCol))), pageSize)

  def apply(k: TxIndex): TxOutput = get(k).get

  //def inTransaction[T](f: => T): T = utxoLedgerTable.inTransaction[T](f)

  def delete(k: TxIndex): FutureTx[Boolean] = {
    utxoLedgerTable.delete(
      where(
        txIdCol -> k.txId,
        indxCol -> k.index
      )
    ).map(_ == 1)

  }

  private def findIndexRow(k: TxIndex): FutureTx[Option[Row]] = {
    utxoLedgerTable.find(
      where(
        txIdCol -> k.txId,
        indxCol -> k.index
      )
    )
  }

  def contains(k: TxIndex): FutureTx[Boolean] = findIndexRow(k).map(_.isDefined)

  def get(k: TxIndex): Option[TxOutput] = getFTx(k).dbRunSyncGet

  def getFTx(k: TxIndex): FutureTx[Option[TxOutput]] = {
    findIndexRow(k)
      .map(r =>
        r.map(_.arrayByte(entryCol).toTxOutput)
      )
  }

  def write(k: TxIndex, le: TxOutput): FutureTx[Long] = {
    val bs = le.toBytes
    val res = utxoLedgerTable.insert(k.txId, k.index, bs)
    res.map(_.toLong)
  }

}

object UtxoDbStorage {
  def apply(ledgerId: LedgerId)(implicit globalTableNameTag: GlobalTableNameTag, db: Db): UtxoDbStorage =
    new UtxoDbStorage(UtxoDbStorageMigration.utxoTableName(UtxoDbStorageId(ledgerId.id)))
}