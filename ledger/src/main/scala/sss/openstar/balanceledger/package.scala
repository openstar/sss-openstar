package sss.openstar


import java.util
import java.util.Objects

import akka.util.ByteString
import sss.ancillary.ByteArrayComparisonOps
import sss.ancillary.ByteArrayEncodedStrOps._
import sss.ancillary.Serialize._
import sss.openstar.balanceledger.serialize._
import sss.openstar.contract.ContractSerializer._
import sss.openstar.contract.{Decumbrance, Encumbrance}
import sss.openstar.hash.SecureCryptographicHash
import sss.openstar.ledger._
import sss.openstar.util.Amount

/**
 * Copyright Stepping Stone Software Ltd. 2016, all rights reserved.
 * mcsherrylabs on 3/3/16.
 */
package object balanceledger extends ByteArrayComparisonOps {


  val TxIndexLen = TxIdLen + 4

  object TxIndex {
    def apply(txIdByteString: ByteString, index: Int): TxIndex =
      new TxIndex(txIdByteString.toByteBuffer.array(), index)
  }

  case class TxIndex(txId: TxId, index: Int) {
    override def equals(obj: scala.Any): Boolean = obj match {
      case txIndx: TxIndex => txIndx.index == index && txIndx.txId.isSame(txId)
      case _ => false
    }

    override def hashCode(): Int = (17 + index) * txId.hash

    override def toString: String = txId.toBase64Str + ":" + index
  }

  case class TxInput(txIndex: TxIndex, amount: Long, sig: Decumbrance)

  case class TxOutput(amount: Long, encumbrance: Encumbrance)

  class LazyTxOutput(val amount: Long, encBytes: Either[Array[Byte], Encumbrance]) {

    override def toString: UniqueNodeIdentifier = toTxOutput.toString

    lazy val encumbrance = toTxOutput.encumbrance
    lazy val toTxOutput: TxOutput = encBytes match {
      case Left(bytes) => TxOutput(amount, bytes.toEncumbrance)
      case Right(e) => TxOutput(amount, e)
    }
  }


  trait Tx {
    val ins: Seq[TxInput]
    val outs: Seq[TxOutput]
    val txId: TxId
  }

  case class StandardTx(ins: Seq[TxInput], outs: Seq[TxOutput]) extends Tx {
    lazy val txId: TxId = {
      val asBytes = TxSerializer.toBytes(this)
      SecureCryptographicHash.hash(asBytes)
    }
  }


  sealed trait UtxoChange {
    val txIndx: TxIndex
  }

  final case class UtxoChanges(ledgerId: LedgerId, txId: TxId, blockHeight: Long, consumed: Seq[InUtxoChange], emmitted: Seq[OutUtxoChange])
    extends TxApplied with BusEvent {
    override def toString: UniqueNodeIdentifier = s"txId: ${txId.toBase64Str}, ${super.toString}"

    override def equals(obj: Any): Boolean = obj match {
      case o: UtxoChanges =>
        util.Arrays.equals(o.txId, txId) &&
          o.blockHeight == blockHeight &&
          o.consumed == consumed &&
          o.emmitted == emmitted &&
          o.ledgerId == ledgerId

      case _ => false
    }

    override def hashCode(): Int = Objects.hash(txId, blockHeight, emmitted, consumed, ledgerId)
  }

  trait InUtxoChange extends UtxoChange

  trait OutUtxoChange extends UtxoChange

  sealed trait ClosedOutUtxoChange extends OutUtxoChange

  case class ConsumedUtxoChange(txIndx: TxIndex) extends InUtxoChange

  case class NewUtxo(txIndx: TxIndex, out: TxOutput) extends OutUtxoChange

  case class ClosedUtxoChange(txIndx: TxIndex, out: TxOutput) extends ClosedOutUtxoChange

  case class OracleInUtxoChange(txIndx: TxIndex, blockHeight: Long) extends InUtxoChange

  implicit class TxIndexTo(t: TxIndex) extends ToBytes {
    override def toBytes: Array[Byte] = TxIndexSerializer.toBytes(t)
  }

  implicit class TxIndexFrom(b: Array[Byte]) {
    def toTxIndex: TxIndex = TxIndexSerializer.fromBytes(b)
  }

  implicit class TxInputTo(t: TxInput) extends ToBytes {
    override def toBytes: Array[Byte] = TxInputSerializer.toBytes(t)
  }

  implicit class TxInputFrom(b: Array[Byte]) {
    def toTxInput[C <: Currency]: TxInput = TxInputSerializer.fromBytes(b)
  }

  implicit class TxOutputTo[C <: Currency](t: TxOutput) extends ToBytes {
    override def toBytes: Array[Byte] = TxOutputSerializer.toBytes(t)
  }

  implicit class TxOutputFrom(b: Array[Byte]) {
    def toTxOutput: TxOutput = TxOutputSerializer.fromBytes(b)
  }

  implicit class TxTo(t: Tx) extends ToBytes {
    override def toBytes: Array[Byte] = TxSerializer.toBytes(t)
  }

  implicit class TxFrom(b: Array[Byte]) {
    def toTx: Tx = TxSerializer.fromBytes(b)
  }

  object TxUtils {
    implicit class Ops(val tx: Tx) extends AnyVal {

      def addOutput(amount: Amount, encumbrance: Encumbrance): Tx = {
        val newOuts = tx.outs :+ TxOutput(amount.value, encumbrance)
        StandardTx(tx.ins, newOuts)
      }
    }
  }
}
