package sss.openstar.balanceledger

import sss.ancillary.ByteArrayEncodedStrOps.ByteArrayToBase64UrlStr
import sss.ancillary.Logging
import sss.db.FutureTx
import sss.openstar.Currency
import sss.openstar.balanceledger.db.UtxoDbStorage
import sss.openstar.common.block.BlockId
import sss.openstar.contract._
import sss.openstar.hash.Digest32
import sss.openstar.ledger.{Ledger, LedgerItem, LedgerResult, ToTxEntryWithSignatures, TxId, TxSeqApplied}
import sss.openstar.util.Amount.Ops._
import sss.openstar.util.{Amount, AmountBuilder}

import java.util
import scala.annotation.nowarn
import scala.util.Try

abstract class UtxoLedgerImpl[+C <: Currency](utxoDbStorage: UtxoDbStorage)(implicit cb: AmountBuilder[C])
    extends Ledger
    with BalanceLedgerQuery[C] {

  self: Logging =>

  /**
   * Used for debug only.
   *
   * @param f
   * @tparam M
   * @return
   */
  def map[M](f: TxOutput => M): Seq[M] = utxoDbStorage.entries().map(f)

  override def importState(height: Long): scala.util.Try[Unit] = utxoDbStorage.importState(height)

  override def createCheckpoint(id: Long): Try[Digest32] = utxoDbStorage.createCheckpoint(id)

  override def deleteCheckpoint(height: Long): Try[Unit] = utxoDbStorage.deleteCheckpoint(height)

  override def balance: Try[Amount] = utxoDbStorage
    .entries()
    .foldLeft(Try(Amount[C](0))) { (acc, e) =>
      val r = acc + e.amount
      r
    }


  override def entry(inIndex: TxIndex): Option[TxOutput] = utxoDbStorage.get(inIndex)

  override def entryFTx(inIndex: TxIndex): FutureTx[Option[TxOutput]] = utxoDbStorage.getFTx(inIndex)

  override def contains(inIndex: TxIndex): FutureTx[Boolean] = utxoDbStorage.contains(inIndex)

  override def keys(pageSize: Int): LazyList[TxIndex] = utxoDbStorage.keys(pageSize)

  override def utxoCount: Long = utxoDbStorage.count

  protected def toClosedOutUtxo(txIndex: TxIndex, amount: Long, out: ClosedEncumbrance): FutureTx[ClosedOutUtxoChange] =
    FutureTx.unit(ClosedUtxoChange(txIndex, TxOutput(amount, out)))

  protected def toNewUtxo(txIndexOut: TxIndex, output: TxOutput): FutureTx[NewUtxo] = {
    utxoDbStorage
      .write(txIndexOut, output)
      .map(_ => NewUtxo(txIndexOut, output))
  }

  def ledgerContext(blockId: BlockId): LedgerContext

  protected def consumeTxIns(txId: TxId,
                             blockId: BlockId,
                             ins: Seq[TxInput],
                             signatures: SeqSignatures,
                             outs: Seq[TxOutput]
  ): FutureTx[(Long, Seq[InUtxoChange])] = {
    val (amount, seqFuts) = ins.indices.foldLeft((0L, Seq.empty[FutureTx[InUtxoChange]])) { case ((accAmount, accChanges), i) =>
      val in = ins(i)
      val newChange = entryFTx(in.txIndex).flatMap(e => validateTxIn(txId, blockId, in, e, signatures(i), outs.lift(i)))
      (accAmount + in.amount, accChanges :+ newChange)
    }
    FutureTx.sequence(seqFuts).map((amount, _))
  }

  protected def decumber(
    txId: TxId,
    blockId: BlockId,
    encumbrance: Encumbrance,
    signatures: Signatures,
    decumbrance: Decumbrance
  ): FutureTx[Boolean] =
    encumbrance.decumber(txId +: signatures, ledgerContext(blockId), decumbrance)

  protected final def validateTxIn(txId: TxId,
                                   blockId: BlockId,
                                   inInput: TxInput,
                                   existingTxOutOpt: Option[TxOutput],
                                   signatures: Signatures,
                                   newOutputAtSameIndexOpt: Option[TxOutput]
  ): FutureTx[InUtxoChange] = existingTxOutOpt match {
    case Some(txOut) => validateTxIn(txId, blockId, inInput, txOut, signatures, newOutputAtSameIndexOpt)
    case None        => validateTxIn(txId, blockId, inInput, signatures, newOutputAtSameIndexOpt)
  }

  protected def validateTxIn(
    txId: TxId,
    blockId: BlockId,
    txInput: TxInput,
    signatures: Signatures,
    newOutputAtSameIndexOpt: Option[TxOutput]
  ): FutureTx[InUtxoChange] = throw new IllegalArgumentException(s"No txOutput exists for input $txInput")

  protected def validateTxIn(txId: TxId,
                             blockId: BlockId,
                             in: TxInput,
                             txOut: TxOutput,
                             signatures: Seq[Array[Byte]],
                             newOutputAtSameIndexOpt: Option[TxOutput]
  ): FutureTx[ConsumedUtxoChange] = for {
    _ <- FutureTx.lazyUnit(require(txOut.amount == in.amount, s"In amount ${in.amount}) must equal out amount ${txOut.amount} "))
    ok <- decumber(txId, blockId, txOut.encumbrance, signatures, in.sig)
    _ = require(ok, "Failed to decumber!")
    _ <- utxoDbStorage.delete(in.txIndex)
  } yield ConsumedUtxoChange(in.txIndex)

  protected def consumeTxOuts(txId: TxId,
                              blockId: BlockId,
                              outs: Seq[TxOutput],
                              signatures: SeqSignatures
  ): FutureTx[(Long, Seq[OutUtxoChange])] = {
    val (amount, seqFuts) = outs.indices.foldLeft((0L, Seq.empty[FutureTx[OutUtxoChange]])) { case ((accAmount, accChanges), i) =>
      val out = outs(i)
      require(out.amount >= 0, "Out amount cannot be negative.")

      val txIndexOut = TxIndex(txId, i)
      val outUtxoChange = out match {
        case TxOutput(_, removeFromUtxo: ClosedEncumbrance) =>
          toClosedOutUtxo(txIndexOut, out.amount, removeFromUtxo)
        case _ =>
          toNewUtxo(txIndexOut, out)
      }
      (accAmount + out.amount, accChanges :+ outUtxoChange)
    }
    FutureTx.sequence(seqFuts).map((amount, _))
  }

  protected def apply(tx: Tx, signatures: SeqSignatures, blockId: BlockId, txSeqApplied: TxSeqApplied): FutureTx[LedgerResult] = {

    import tx._


    for {
      (totalIn, consumedIns) <- consumeTxIns(txId, blockId, ins, signatures, outs): @nowarn
      (totalOut, newUtxos) <- consumeTxOuts(txId, blockId, outs, signatures): @nowarn
      _ = require(totalOut == totalIn, s"Total out (${totalOut}) *must* equal total in (${totalIn})")
    } yield Right(UtxoChanges(ledgerId, txId, blockId.blockHeight, consumedIns, newUtxos))

  }

  def applyFTx(ledgerItem: LedgerItem, blockId: BlockId, txSeqApplied: TxSeqApplied): FutureTx[LedgerResult] = FutureTx.unit {

    val stx = ledgerItem.txEntryBytes.toSignedTxEntry
    require(
      util.Arrays.equals(stx.txId, ledgerItem.txId),
      s"The transmitted txId (${ledgerItem.txId.toBase64Str}) " +
        s"does not match the generated TxId (${stx.txId.toBase64Str})"
    )
    val tx = stx.txEntryBytes.toTx
    require(tx.ins.nonEmpty, "Tx has no inputs")
    require(tx.outs.nonEmpty, "Tx has no outputs")
    require(
      stx.signatures.sigs.length == tx.ins.length + 1, // + 1 for the toll sig
      "Every input *must* have a specified param sequence, even if it's empty."
    )
    (tx, stx)

  }.flatMap{
    case (tx, stx) => apply(tx, stx.signatures.sigs.tail, blockId, txSeqApplied)
  }

}
