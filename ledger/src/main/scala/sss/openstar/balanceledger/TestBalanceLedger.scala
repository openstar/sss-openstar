package sss.openstar.balanceledger

import sss.ancillary.Logging
import sss.db.ops.DbOps.DbRunOps
import sss.db.{Db, FutureTx}
import sss.openstar.{Currency, UniqueNodeIdentifier}
import sss.openstar.account.KeyFactory
import sss.openstar.account.Ops.MakeTxSig
import sss.openstar.balanceledger.CoinbaseBalanceLedger.{CoinBaseInUtxoChange, CoinbaseBalanceLedgerContext}
import sss.openstar.balanceledger.TestBalanceLedger.TestBalanceLedgerCoinbaseTxId
import sss.openstar.balanceledger.db.UtxoDbStorage
import sss.openstar.chains.Chains.GlobalChainIdMask
import sss.openstar.common.block.BlockId
import sss.openstar.contract._
import sss.openstar.ledger.LedgerOps.TryLedgerResultToTryApplied
import sss.openstar.ledger.{LedgerId, LedgerItem, LedgerResult, SignedTxEntry, TxSeqApplied, TxSig}
import sss.openstar.schemamigration.dynamic.GlobalTableNameTag
import sss.openstar.util.AmountBuilder

import java.nio.charset.StandardCharsets
import java.util
import scala.concurrent.{ExecutionContext, Future}

object TestBalanceLedger {

  val TestBalanceLedgerCoinbaseTxId = "TESTBASETESTBASETESTBASETESTBASE".getBytes(
    StandardCharsets.UTF_8
  )


  def createTestCurrencyTx(
                          makeTxSig: Array[Byte] => Future[TxSig],
                            output: TxOutput,
                            ledgerId: LedgerId)(implicit executionContext: ExecutionContext): Future[LedgerItem] = {
    val tx = StandardTx(
      Seq(
        TxInput(
          TxIndex(TestBalanceLedgerCoinbaseTxId, 0),
          output.amount,
          NullDecumbrance()
        )
      ),
      Seq(output))

    makeTxSig(tx.txId) map { txSig =>
      val sTx = SignedTxEntry(tx.toBytes, txSig.withSig(Seq(Array.emptyByteArray)))
      LedgerItem(ledgerId.id, tx.txId, sTx.toBytes)
    }

  }

  def apply[C <: Currency](ledgerId: LedgerId,
                           accountOpt: FindPublicKeyAccFTxOpt,
                           keyFactory: KeyFactory
                          )(implicit db: Db,
                            globalTableNameTag: GlobalTableNameTag, cb: AmountBuilder[C]): TestBalanceLedger[C] = {
    val storage = UtxoDbStorage(ledgerId)
    implicit val lId = ledgerId
    new TestBalanceLedger[C](storage, accountOpt, keyFactory)
  }
}

class TestBalanceLedger[C <: Currency](storage: UtxoDbStorage,
                                       accountOpt: FindPublicKeyAccFTxOpt, keyFactory: KeyFactory
                           )(implicit val ledgerId: LedgerId, db: Db, cb: AmountBuilder[C])
  extends UtxoLedgerImpl[C](storage) with Logging {



  override def apply(ledgerItem: LedgerItem, blockId: BlockId, txSeqApplied: TxSeqApplied): LedgerResult =
    applyFTx(ledgerItem, blockId, txSeqApplied)
      .dbRunSync
      .unwrap

  override protected def apply(
                                tx: Tx,
                                signatures: SeqSignatures,
                                blockId: BlockId,
                                txSeqApplied: TxSeqApplied): FutureTx[LedgerResult] = {

    validateCoinbase(blockId.blockHeight, signatures, tx) match {
      case Some(inAmount) =>
        consumeTxOuts(tx.txId, blockId, tx.outs, signatures).map {
          case (amountOut, outEvents)  =>
            require(inAmount == amountOut, s"Coinbase in and out amount not equal out:$amountOut in:$inAmount")
            Right(UtxoChanges(ledgerId, tx.txId, blockId.blockHeight, Seq(CoinBaseInUtxoChange(tx.ins.head.txIndex)), outEvents))
        }
      case None =>
        super.apply(tx, signatures, blockId, txSeqApplied)
    }
  }

  private def validateCoinbase(blockHeight: Long,
                               sigs: Seq[Seq[Array[Byte]]],
                               tx: Tx): Option[Long] = {

    require(tx.ins.size == 1, "Coinbase can only have 1 input")

    val in = tx.ins.head
    in.txIndex match {
      case TxIndex(coinbaseTxId, 0) if util.Arrays.equals(TestBalanceLedgerCoinbaseTxId,coinbaseTxId) =>

        log.debug(s"Coinbase validation for height $blockHeight")

        // No need to delete from storage because it's not in storage.
        if (blockHeight % 100 == 0) {
          log.info(s"Balance ledger balance is ${balance} at height $blockHeight, " +
            s"adding ${in.amount} via coinbase")
        }
        Some(in.amount)

      case _ =>  None
    }
  }


  override def ledgerContext(blockId: BlockId): LedgerContext =
    CoinbaseBalanceLedgerContext(blockId.blockHeight, accountOpt, keyFactory)

}


