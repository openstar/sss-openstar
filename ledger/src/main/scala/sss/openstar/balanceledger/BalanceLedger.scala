package sss.openstar.balanceledger

import sss.db.FutureTx
import sss.openstar.Currency
import sss.openstar.util.Amount

import scala.util.Try


trait BalanceLedgerQuery[+C <: Currency] {
  def balance: Try[Amount]
  def entry(inIndex: TxIndex): Option[TxOutput]
  def entryFTx(inIndex: TxIndex): FutureTx[Option[TxOutput]]
  def contains(inIndex: TxIndex): FutureTx[Boolean]
  def map[M](f: (TxOutput) => M): Seq[M]
  def keys(pageSize: Int = 500): LazyList[TxIndex]
  def utxoCount: Long
}

