package sss.openstar.balanceledger

import java.nio.charset.StandardCharsets
import java.security.SecureRandom
import java.util
import scorex.crypto.signatures.Signature
import sss.ancillary.Logging
import sss.db.{Db, FutureTx}
import sss.openstar.account.{KeyFactory, NodeIdentity, NodeSigner}
import sss.openstar.balanceledger.OracleBalanceLedger.{OracleBalanceLedgerContext, OracleDecumbrance, OracleLedgerOwnerLookupFTx, OracleLodgementEncumbrance, OracleWithdrawEncumbrance}
import sss.openstar.chains.Chains.GlobalChainIdMask
import sss.openstar.common.block.BlockId
import sss.openstar.contract.{ClosedEncumbrance, Decumbrance, Encumbrance, FindPublicKeyAccFTxOpt, FindPublicKeyAccOpt, LedgerContext, SeqSignatures, Signatures}
import sss.openstar.crypto.SeedBytes
import sss.openstar.hash.SecureCryptographicHash
import sss.openstar.identityledger.IdentityServiceQuery
import sss.openstar.ledger.{LedgerId, LedgerItem, LedgerResult, SignedTxEntry, TxId, TxIdLen, TxSeqApplied, TxSig}
import sss.openstar.util.{Amount, AmountBuilder}
import sss.openstar.{Currency, UniqueNodeIdentifier, contract}
import Amount.Ops._
import sss.ancillary.FutureOps.AwaitResult
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.account.AccountKeys.AccountVerifyAndDets
import sss.openstar.account.Ops.{MakeTxSigArys, MakeTxSigSigner}
import sss.openstar.balanceledger.db.UtxoDbStorage
import sss.openstar.ledger.LedgerOps.TryLedgerResultToTryApplied
import sss.openstar.schemamigration.dynamic.GlobalTableNameTag

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration.DurationInt
import scala.util.Try

object OracleBalanceLedger {


  def apply[C <: Currency](ledgerId: LedgerId,
                           keyFactory: KeyFactory)(implicit db: Db,
                                                   identityServiceQuery: IdentityServiceQuery,
                                                   ownerLoookup: OracleLedgerOwnerLookupFTx,
                                                   globalTableNameTag: GlobalTableNameTag,
                                                   cb: AmountBuilder[C]): OracleBalanceLedger[C] = {
    val storage = UtxoDbStorage(ledgerId)
    new OracleBalanceLedger[C](ledgerId, identityServiceQuery, ownerLoookup, storage, keyFactory)
  }

  type OracleLedgerOwnerLookupFTx = LedgerId => FutureTx[Seq[UniqueNodeIdentifier]]


  case class OracleLodgementEncumbrance(globalUniqueIdentifier: Array[Byte]) extends OracleEncumbrance {
    override def decumber(params: Seq[Array[Byte]], context: LedgerContext, decumbrance: Decumbrance)
    : FutureTx[Boolean] = {
      require(params.size == 4, "Missing sig")
      require(util.Arrays.equals(params.last, globalUniqueIdentifier), "Provide the gId")
      super.decumber(params.take(3), context, decumbrance)
    }
  }

  case class OracleWithdrawEncumbrance(exitInstructions: String) extends OracleEncumbrance

  sealed trait OracleEncumbrance extends Encumbrance {

    private def fail(msg: String): FutureTx[Boolean] = FutureTx.failed(new IllegalArgumentException(msg))

    override def decumber(
                           params: Seq[Array[Byte]],
                           context: LedgerContext,
                           decumbrance: Decumbrance): FutureTx[Boolean] = context match {

      case correct: OracleBalanceLedgerContext =>
        decumbrance match {
          case OracleDecumbrance(ledgerId) =>
            correct.ownerLookup(ledgerId).flatMap {
              case owners if(owners.isEmpty) =>
                fail(s"No owners found for ledger id $ledgerId")

              case owners =>

                require(params.size == 3, "Need txId, sig and tag in params")
                val txId = params.head
                val sig = Signature(params(1))
                val tag = new String(params(2), StandardCharsets.UTF_8)

                owners.foldLeft(FutureTx.unit(false)) {
                  case (found, owner) => for {
                    f <- found
                    f2 <- context.accountOpt(owner, tag).map(_.exists(_.verifier.verify(txId, sig)))
                  } yield (f || f2)
                }
            }
          case _ =>
            fail(s"Only OracleDecumber is supported, not $decumbrance ")
        }

      case _ =>
        fail("Oracle decumber needs Oracle ledger context")

    }
  }


  def confirmLodgement(
                        ledgerId: LedgerId,
                        ledgerOwner: NodeSigner,
                        amount: Long,
                        globalUniqueId: String,
                        txIndex: TxIndex,
                        outs: Seq[TxOutput])(implicit ec: ExecutionContext): Future[LedgerItem] = {

    val in = TxInput(txIndex, amount, OracleDecumbrance(ledgerId))
    val ins = Seq(in)

    val tx = StandardTx(ins, outs)
    val hashOfUniqueId = SecureCryptographicHash.hash(globalUniqueId.getBytes(StandardCharsets.UTF_8))

    OracleBalanceLedger.createOracleDecumbranceSig(ledgerOwner, tx.txId) map { sigs =>
      val sigsWithHash = sigs :+ hashOfUniqueId
      val txSig = TxSig.emptyTxSig.withSig(sigsWithHash)
      val sTx = SignedTxEntry(tx.toBytes, txSig)
      LedgerItem(ledgerId, sTx.txId, sTx.toBytes)
    }


  }

  def createOracleLedgerItem[C <: Currency](
                              ledgerId: LedgerId,
                              amount: Amount,
                              encumbrance: OracleEncumbrance,
                              ledgerOwner: NodeSigner)(implicit ec: ExecutionContext): Future[(LedgerItem, Int)] = {

    val ins: Seq[TxInput] = Seq(createTxInput(amount, ledgerId))
    val outs: Seq[TxOutput] = Seq(TxOutput(amount.value, encumbrance))
    val tx = StandardTx(ins, outs)
    val txId = tx.txId
    createOracleDecumbranceSig(ledgerOwner, txId) map { sigs =>
      val sTx = SignedTxEntry(tx.toBytes, TxSig.emptyTxSig.withSig(sigs))
      (LedgerItem(ledgerId, sTx.txId, sTx.toBytes), 0)
    }
  }

  private def createTxInput(amount: Amount, ledgerId:LedgerId): TxInput = {
    val rndBytes = SeedBytes.strongSeed(TxIdLen)
    TxInput(TxIndex(rndBytes, 0), amount.value,  OracleDecumbrance(ledgerId))
  }

  def createOracleDecumbranceSig(
                                  ledgerOwner: NodeSigner,
                                  txId: TxId)(implicit ec: ExecutionContext): Future[Signatures] = {
    ledgerOwner.sign(txId, 1.seconds) map { sig =>
      Seq(
        sig,
        ledgerOwner.verifier.nodeIdTag.tagBytes,
      )
    }

  }

  case class OracleDecumbrance(ledgerId: LedgerId) extends Decumbrance

  case class OracleBalanceLedgerContext(blockHeight: Long,
                                        ownerLookup: OracleLedgerOwnerLookupFTx,
                                        accountOpt: FindPublicKeyAccFTxOpt,
                                        keyFactory: KeyFactory) extends LedgerContext {
    override def accountKeysVerify(keyType: String): AccountVerifyAndDets = keyFactory.apply(keyType)
  }

}

class OracleBalanceLedger[C <: Currency](
                                          val ledgerId: LedgerId,
                                          identityServiceQuery: IdentityServiceQuery,
                                          ownerLookup: OracleLedgerOwnerLookupFTx,
                                          utxoDbStorage: UtxoDbStorage,
                                          keyFactory: KeyFactory)(implicit db: Db,
                                                                       cb: AmountBuilder[C])
  extends UtxoLedgerImpl[C](utxoDbStorage: UtxoDbStorage)
    with Logging {

  private implicit val implictLedgerId: LedgerId = ledgerId

  val accountOpt: FindPublicKeyAccFTxOpt = (id, tag) =>
    identityServiceQuery
      .accountOptFTx(id, tag)
      .map(_.map(identityServiceQuery.toVerifier))

  override def balance: Try[Amount] = utxoDbStorage.entries().foldLeft(Try(Amount[C](0))){
    case (acc, TxOutput(amount, enc: OracleLodgementEncumbrance)) => acc
    case (acc, TxOutput(amount, enc)) =>  acc + amount
  }

  override def apply(ledgerItem: LedgerItem, blockId: BlockId, txSeqApplied: TxSeqApplied): LedgerResult =
    super.applyFTx(ledgerItem, blockId, txSeqApplied).dbRunSync.unwrap

  override protected def validateTxIn(
                                       txId: TxId,
                                       blockId: BlockId,
                                       in: TxInput,
                                       txOut: TxOutput,
                                       signatures: Seq[Array[Byte]],
                                       newOutputAtSameIndexOpt: Option[TxOutput]): FutureTx[ConsumedUtxoChange] = {

    (txOut, newOutputAtSameIndexOpt) match {
      case (TxOutput(amountOut, OracleWithdrawEncumbrance(_)),Some(TxOutput(amountPrevOut, _: ClosedEncumbrance))) =>
        // if the previous out has been locked to withdrawal the only acceptable way to process this
        // is to have the new output as a closed encumbrance (which will cause it to disappear)
        require(amountOut == amountPrevOut)

      case (TxOutput(_, OracleWithdrawEncumbrance(_)), _) =>
        //fail
        throw new IllegalArgumentException(s"OracleWithdrawEncumbrance has no matching ClosedEncumbrance")
      case _ => //ignore

    }

    super.validateTxIn(txId, blockId, in, txOut, signatures, newOutputAtSameIndexOpt)

  }

  private def isSignedByAnOwner(
                                 owners: Seq[UniqueNodeIdentifier],
                                 txId: TxId,
                                 sig: Signature,
                                 tag: String): FutureTx[Boolean] = for {

    isSigned <- owners.foldLeft(FutureTx.unit(false)) {
      case (acc, owner) => for {
        alreadyFound <- acc
        ac <- accountOpt(owner, tag)
        isSigned = ac.exists(_.verifier.verify(txId, sig))
      } yield (alreadyFound || isSigned)
    }

  } yield isSigned


  override protected def validateTxIn(
                                       txId: TxId,
                                       blockId: BlockId,
                                       txInput: TxInput,
                                       signatures: Seq[Array[Byte]],
                                       txOutputAtSameIndexOpt: Option[TxOutput]
                                     ): FutureTx[InUtxoChange] = {

    (txInput, txOutputAtSameIndexOpt) match {
      case (TxInput(_, inAmount, OracleDecumbrance(`ledgerId`)), Some(TxOutput(outAmount, _: OracleLodgementEncumbrance))) if (inAmount == outAmount) =>

        for {
          owners <- ownerLookup(ledgerId)
          _ = require (owners.nonEmpty, s"No owners for ledger Id $ledgerId, cannot validate")
          _ = require (signatures.size == 2, "Need sig and tag in signatures")
          sig = Signature(signatures.head)
          tag = new String(signatures(1), StandardCharsets.UTF_8)
          signedByAnOwner <- isSignedByAnOwner(owners, txId, sig, tag)
          _ = require(signedByAnOwner, s"Couldn't validate the sig against any owner ${owners.mkString}")
        } yield {
          OracleInUtxoChange(txInput.txIndex, blockId.blockHeight)
        }

      case _ =>
        super.validateTxIn(txId, blockId, txInput, signatures, txOutputAtSameIndexOpt)
    }
  }

  override def ledgerContext(blockId: BlockId): contract.LedgerContext =
    OracleBalanceLedgerContext(blockId.blockHeight, ownerLookup, accountOpt, keyFactory)
}
