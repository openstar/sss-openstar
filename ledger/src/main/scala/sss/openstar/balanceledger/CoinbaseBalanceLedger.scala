package sss.openstar.balanceledger

import sss.ancillary.Logging
import sss.db.ops.DbOps.DbRunOps
import sss.db.{Db, FutureTx}
import sss.openstar.Currency
import sss.openstar.account.AccountKeys.AccountVerifyAndDets
import sss.openstar.account.Ops.MakeTxSig
import sss.openstar.account.{KeyFactory, NodeIdentity}
import sss.openstar.balanceledger.CoinbaseBalanceLedger.{CoinBaseInUtxoChange, CoinbaseBalanceLedgerContext}
import sss.openstar.balanceledger.db.UtxoDbStorage
import sss.openstar.chains.Chains.GlobalChainIdMask
import sss.openstar.common.block.BlockId
import sss.openstar.contract.{LedgerContext, _}
import sss.openstar.ledger.LedgerOps.TryLedgerResultToTryApplied
import sss.openstar.ledger.{CoinbaseTxId, LedgerId, LedgerItem, LedgerResult, SignedTxEntry, TxSeqApplied, TxSig}
import sss.openstar.schemamigration.dynamic.GlobalTableNameTag
import sss.openstar.util.{AmountBuilder, TxSign}

object CoinbaseBalanceLedger {

  case class CoinBaseInUtxoChange(txIndx: TxIndex) extends InUtxoChange

  case class CoinbaseBalanceLedgerContext(blockHeight: Long, accountOpt: FindPublicKeyAccFTxOpt, keyFactory: KeyFactory) extends LedgerContext {
      override def accountKeysVerify(keyType: String): AccountVerifyAndDets = keyFactory.apply(keyType)
  }

  def apply[C <: Currency](ledgerId: LedgerId,
                           cbe: CoinbaseValidator,
                           accountOpt: FindPublicKeyAccFTxOpt,
                           keyFactory: KeyFactory
                          )(implicit db: Db, globalTableNameTag: GlobalTableNameTag, cb: AmountBuilder[C]): CoinbaseBalanceLedger[C] = {
    val storage = UtxoDbStorage(ledgerId)
    implicit val lId = ledgerId
    new CoinbaseBalanceLedger[C](storage, cbe, accountOpt, keyFactory)
  }
}

class CoinbaseBalanceLedger[C <: Currency](storage: UtxoDbStorage,
                            coinbaseValidator: CoinbaseValidator,
                                           accountOpt: FindPublicKeyAccFTxOpt,
                                           keyFactory: KeyFactory
                           )(implicit val ledgerId: LedgerId, db: Db, cb: AmountBuilder[C])
  extends UtxoLedgerImpl[C](storage) with Logging {


  override def apply(ledgerItem: LedgerItem,
                     blockId: BlockId,
                     txSeqApplied: TxSeqApplied): LedgerResult =
    applyFTx(ledgerItem, blockId, txSeqApplied)
      .dbRunSync.unwrap


  override protected def apply(
                                tx: Tx,
                                signatures: SeqSignatures,
                                blockId: BlockId,
                                txSeqApplied: TxSeqApplied): FutureTx[LedgerResult] = {
    if(blockId.txIndex == 1) { //the first tx is index 1
      val inAmount = validateCoinbase(blockId.blockHeight, signatures, tx)
      consumeTxOuts(tx.txId, blockId, tx.outs, signatures).map {
        case (amountOut, outEvents)  =>
          require(inAmount == amountOut, s"Coinbase in and out amount not equal out:$amountOut in:$inAmount")
          Right(UtxoChanges(ledgerId, tx.txId, blockId.blockHeight, Seq(CoinBaseInUtxoChange(tx.ins.head.txIndex)), outEvents))
      }
    } else {
      super.apply(tx, signatures, blockId, txSeqApplied)
    }
  }

  private def validateCoinbase(blockHeight: Long,
                               sigs: Seq[Seq[Array[Byte]]],
                               tx: Tx): Long = {

    require(tx.ins.size == 1, "Coinbase can only have 1 input")

    val in = tx.ins.head
    in.txIndex match {
      case TxIndex(coinbaseTxId, 0) if CoinbaseTxId sameElements coinbaseTxId =>

        log.debug(s"Coinbase validation for height $blockHeight")

        coinbaseValidator.validate(blockHeight, sigs, tx)
        // No need to delete from storage because it's not in storage.
        if (blockHeight % 100 == 0) {
          log.info(s"Balance ledger balance is ${balance} at height $blockHeight, " +
            s"adding ${in.amount} via coinbase")
        }
        in.amount

      case TxIndex(_, _) =>
        throw new IllegalArgumentException(s"Only one coinbase tx allowed ${in.txIndex}.")
    }
  }


  override def coinbase(
                         forBlockHeight: Long,
                         ledgerId: GlobalChainIdMask
                         ): Option[LedgerItem] = {

    if (coinbaseValidator.rewardPerBlockAmount > 0) {
      val rewardPerQuorumCandidate = coinbaseValidator.rewardPerBlockAmount
      val coinbaseAmount = rewardPerQuorumCandidate * coinbaseValidator.quorumCandidates().size
      val minNumBlocksWait = coinbaseValidator.numBlocksInTheFuture
      val ins: Seq[TxInput] = Seq(TxInput(TxIndex(CoinbaseTxId, 0),
        coinbaseAmount,
        CoinbaseDecumbrance(forBlockHeight))
      )

      val outs = coinbaseValidator.quorumCandidates() map { cand =>
        TxOutput(rewardPerQuorumCandidate,
          SingleIdentityEnc(cand, None, forBlockHeight + minNumBlocksWait))
      }

      val cb = StandardTx(ins, outs.toSeq)

      val stx = SignedTxEntry(cb.toBytes, TxSig.emptyTxSig.withSig(TxSig.emptyTxSig.sigs.head))
      Some(LedgerItem(ledgerId, stx.txId, stx.toBytes))

    } else None
  }

  override def ledgerContext(blockId: BlockId): LedgerContext = CoinbaseBalanceLedgerContext(blockId.blockHeight, accountOpt, keyFactory)

}


