package sss.openstar.identityledger

import scorex.crypto.signatures.Signature
import sss.ancillary.Logging
import sss.db.ops.DbOps.DbRunOps
import sss.db.{Db, FutureTx}
import sss.openstar.account.NodeIdTag.defaultTag
import sss.openstar.account.{NodeIdTag, SignerSecurityLevels, TypedPublicKey, VerifierFactory}
import sss.openstar.balanceledger.OracleBalanceLedger.OracleLedgerOwnerLookupFTx
import sss.openstar.common.block.BlockId
import sss.openstar.hash
import sss.openstar.identityledger.Ops.ToVerifier
import sss.openstar.ledger.LedgerOps.TryLedgerResultToTryApplied
import sss.openstar.ledger._

import java.nio.charset.StandardCharsets.UTF_8
import scala.annotation.nowarn
import scala.util.Try

/**
  * Created by alan on 5/30/16.
  */
class IdentityLedger(idLedgerService: IdentityService,
                     owners: OracleLedgerOwnerLookupFTx)(implicit val ledgerId: LedgerId, db: Db) extends  Ledger with Logging {


  override def apply(
                      ledgerItem: LedgerItem,
                      blockId: BlockId,
                      txSeqApplied: TxSeqApplied): LedgerResult =
    applyFTx(ledgerItem, blockId, txSeqApplied)
      .dbRunSync
      .unwrap


  def applyFTx(ledgerItem: LedgerItem,
               blockId: BlockId,
               txSeqApplied: TxSeqApplied): LedgerResultFTx = FutureTx.unit {

    require(ledgerItem.ledgerId == ledgerId, s"The ledger id for this (Identity) ledger is $ledgerId but " +
      s"the ledgerItem passed has an id of ${ledgerItem.ledgerId}")

    ledgerItem.txEntryBytes.toSignedTxEntry
  } flatMap { ste =>

    ste.txEntryBytes.toIdentityLedgerTx match {

      case a @ Delete(authorizedOwner, identityForDeletion) =>
        for {
          _ <- verifyOwnerRequest(authorizedOwner, ste, a)
          _ <- idLedgerService.deleteIdentity(identityForDeletion)
        } yield Right(NoSubEventsTxApplied(a.txId))

      case c @ Claim0(authorizedOwner, identity, _, roles) =>
        for {
          _ <- verifyOwnerRequest(authorizedOwner, ste, c)
          _ <- idLedgerService.claim(500000, Some(500), c.toSignerDetails, Seq(roles))
        } yield Right(NoSubEventsTxApplied(c.txId))

      case a @ Claim(authorizedOwner, identity, signerVerifierDetails, ownedAttributes) =>
        for {
          _ <- verifyOwnerRequest(authorizedOwner, ste, a)
          _ <- idLedgerService.claim(500000, Some(500), signerVerifierDetails, ownedAttributes)
        } yield Right(NoSubEventsTxApplied(a.txId))

      case a @ UnLink(identity, tag) =>
        for {
          _ <- verifyChangeRequest(ste, a, identity)
          _ <- idLedgerService.unlink(identity, tag)
        } yield Right(NoSubEventsTxApplied(a.txId))

      case a @ UnLinkByKey(identity, pKey) =>
        for {
          _ <- verifyChangeRequest(ste, a, identity)
          _ <- idLedgerService.unlink(identity, pKey)
        } yield Right(NoSubEventsTxApplied(a.txId))

      case a@Link(signerVerifierDetails) =>
        for {
          _ <- verifyChangeRequest(ste, a, signerVerifierDetails.nodeIdTag.nodeId)
          _ <- idLedgerService.link(signerVerifierDetails)

        } yield Right(NoSubEventsTxApplied(a.txId))

      case a @ Rescue(rescuer, signerVerifierDetails) =>
        for {
          _ <- verifyRescueRequest(rescuer, ste, a, signerVerifierDetails.nodeIdTag.nodeId)
          _ <- idLedgerService.link(signerVerifierDetails)
        } yield Right(NoSubEventsTxApplied(a.txId))

      case a @ LinkRescuer(rescuer, identity) =>
        for {
          _ <- verifyChangeRequest(ste, a, identity)
          _ <- idLedgerService.linkRescuer(identity, rescuer)
        } yield Right(NoSubEventsTxApplied(a.txId))


      case a @ UnLinkRescuer(rescuer, identity) =>
        for {
          _ <- verifyChangeRequest(ste, a, identity)
          unlinked <-  idLedgerService.unLinkRescuer(identity, rescuer)
        } yield Right(NoSubEventsTxApplied(a.txId))

      case a @ AddMessageStore(identity, newStore) =>
        for {
          _ <- verifyChangeRequest(ste, a, identity)
          added <- idLedgerService.addMessageStore(identity, newStore)
        } yield Right(IdentityLedgerTxApplied(a.txId, added))

      case a @ RemoveMessageStore(identity, storeToRemove) =>
        for {
          _ <- verifyChangeRequest(ste, a, identity)
          removed <- idLedgerService.removeMessageStore(identity, storeToRemove)
        } yield Right(IdentityLedgerTxApplied(a.txId, removed))


      case a @ SetPaywall(identity, category, Some(amount)) =>
        require(amount >= 0, s"Paywall amount must be zero or positive")
        for {
          _ <- verifyChangeRequest(ste, a, identity)
          setPaywall <- idLedgerService.setPaywall(identity, category, amount)
        } yield Right(IdentityLedgerTxApplied(a.txId, setPaywall))

      case a @ SetPaywall(identity, category, None) =>
        for {
          _ <- verifyChangeRequest(ste, a, identity)
          removedOpt <- idLedgerService.removePaywall(identity, category)
        } yield removedOpt
          .map(r => Right(IdentityLedgerTxApplied(a.txId, r)))
          .getOrElse(Right(NoSubEventsTxApplied(a.txId)))

      case a @ SetAttribute(identity, attr) =>
        for {
          _ <- verifyChangeRequest(ste, a, identity)
            changed <- idLedgerService.setAttribute(identity, attr)
        } yield  Right(IdentityLedgerTxApplied(a.txId, changed))

      case a @ SetOwnedAttribute(authorizedOwner, identity, attr) =>
        for {
          _ <- verifyOwnerRequest(authorizedOwner, ste, a)
          changed <- idLedgerService.setOwnedAttribute(identity, attr)
        } yield  Right(IdentityLedgerTxApplied(a.txId, changed))

      case a@RemoveAttribute(identity, attr) =>
        for {
          _ <- verifyChangeRequest(ste, a, identity)
          deletedAttr <- idLedgerService.removeAttribute(identity, attr)

        } yield deletedAttr.map(r => Right(IdentityLedgerTxApplied(a.txId, r))).getOrElse(Right(NoSubEventsTxApplied(a.txId)))

      case a@RemoveOwnedAttribute(authorizedOwner, identity, attr) =>
        for {
          _ <- verifyOwnerRequest(authorizedOwner, ste, a)
          deletedAttr <- idLedgerService.removeOwnedAttribute(identity, attr)
        } yield deletedAttr
          .map(r => Right(IdentityLedgerTxApplied(a.txId, r)))
          .getOrElse(Right(NoSubEventsTxApplied(a.txId)))

        }
  }

  private def verifyRescueRequest(rescuer: String, ste: SignedTxEntry, msg: IdentityLedgerTx, identity: String): FutureTx[Unit] = {
    for {
      (sig, tag) <- retrieveSignatures(ste) : @nowarn
      _ <- verifyRescuer(identity, rescuer)
      _ <- verifyAccountAndSignature(rescuer, tag, msg, sig)
    } yield ()
  }

  private def verifyChangeRequest(ste: SignedTxEntry, msg: IdentityLedgerTx, identity: String): FutureTx[Unit] =
    for {
      (sig, tag) <- retrieveSignatures(ste) : @nowarn
      _ <- verifyAccountAndSignature(identity, tag, msg, sig)
    } yield ()

  private def verifyOwnerRequest(owner: String, ste: SignedTxEntry, msg: IdentityLedgerTx): FutureTx[Unit] =
    for {
      _ <- verifyOwner(owner)
      (sig, tag) <- retrieveSignatures(ste): @nowarn
      _ <- verifyAccountAndSignature(owner, tag, msg, sig)
    } yield ()

  private def verifyRescuer(identity: String, rescuer: String) = for {
    rescuers <- idLedgerService.rescuersFTx(identity)
    _ = require(rescuers.contains(rescuer), s"This rescuer is not authorized to rescue $identity")
  } yield ()

  private def verifyOwner(owner: String) = for {
    owns <- owners(ledgerId)
    _ = require(owns.nonEmpty, s"Could not find owners for this $ledgerId")
    _ = require(owns.contains(owner), s"Unauthorized owner for this ledger $ledgerId")
  } yield ()

  private def retrieveSignatures(ste: SignedTxEntry) = for {
    _ <- FutureTx.lazyUnit {
      require(ste.signatures.sigs.nonEmpty && ste.signatures.sigs.head.size == 3, "A id/tag/sig pair must be provided to continue.")
    }
    sig = Signature(ste.signatures.sigs.head(2))
    tag = new String(ste.signatures.sigs.head(1), UTF_8)
  } yield (sig, tag)

  private def verifyAccountAndSignature(identity: String, tag: String, msg: IdentityLedgerTx, sig: Signature) = for {
    accOpt <- idLedgerService.accountOptFTx(identity, tag)

    _ = require(accOpt.isDefined, s"Could not find an account for identity/tag pair ${identity}/$tag provided in signature.")
    _ = require(
      accOpt.get.toVerifier(idLedgerService)
        .verifier
        .verify(msg.txId, sig), "The signature does not match the txId")

  } yield ()

  override def importState(height: Long): Try[Unit] = idLedgerService.importState(height).dbRunSync

  override def createCheckpoint(id: Long): Try[hash.Digest32] = idLedgerService.createCheckpoint(id).dbRunSync.flatten

  override def deleteCheckpoint(height: Long): Try[Unit] = idLedgerService.deleteCheckpoint(height).dbRunSync
}
