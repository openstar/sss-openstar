package sss.openstar.identityledger.serialize

import sss.ancillary.Serialize._
import sss.openstar.identityledger._

/**
  * Created by alan on 6/1/16.
  */
object RemoveMessageStoreSerializer extends Serializer[RemoveMessageStore] {
  def toBytes(messageStore: RemoveMessageStore): Array[Byte] = {
    ByteSerializer(RemoveMessageStoreCode) ++
      LongSerializer(messageStore.uniqueMessage) ++
      StringSerializer(messageStore.identity) ++
      StringSerializer(messageStore.toBeRemovedId).toBytes
  }

  def fromBytes(bytes: Array[Byte]): RemoveMessageStore = {
    val extracted = bytes.extract(ByteDeSerialize,
                                  LongDeSerialize,
                                  StringDeSerialize,
                                  StringDeSerialize)

    require(
      extracted._1 == RemoveMessageStoreCode,
      s"Wrong leading byte for RemoveMessageStore ${bytes.head} instead of $RemoveMessageStoreCode")

    new RemoveMessageStore(extracted._3, extracted._4) {
      private[identityledger] override val uniqueMessage: Long = extracted._2
    }
  }
}
