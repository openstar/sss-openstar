package sss.openstar.identityledger.serialize

import sss.ancillary.Serialize._
import sss.openstar.identityledger._

/**
  * Created by alan on 6/1/16.
  */

object ProviderAttachmentUrlAttributeSerializer extends Serializer[ProviderAttachmentUrlAttribute] {

  override def toBytes(t: ProviderAttachmentUrlAttribute): Array[Byte] = {
    ByteSerializer(SystemAttributeCategory.ProviderAttachmentUrl.toByte) ++
      StringSerializer(t.baseUrl).toBytes
  }

  override def fromBytes(b: Array[Byte]): ProviderAttachmentUrlAttribute = {
    require(b.head == SystemAttributeCategory.ProviderAttachmentUrl.toByte)
    ProviderAttachmentUrlAttribute(b.tail.extract(StringDeSerialize))
  }
}
