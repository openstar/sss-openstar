package sss.openstar.identityledger.serialize

import sss.ancillary.Serialize._
import sss.openstar.identityledger._

/**
  * Created by alan on 6/1/16.
  */

object ProviderChargeAttributeSerializer extends Serializer[ProviderChargeAttribute] {

  override def toBytes(t: ProviderChargeAttribute): Array[Byte] = {
    ByteSerializer(SystemAttributeCategory.ProviderCharge.toByte) ++
      LongSerializer(t.amount).toBytes
  }

  override def fromBytes(b: Array[Byte]): ProviderChargeAttribute = {
    assert(b.head == SystemAttributeCategory.ProviderCharge.toByte)
    ProviderChargeAttribute(b.tail.extract(LongDeSerialize))
  }
}
