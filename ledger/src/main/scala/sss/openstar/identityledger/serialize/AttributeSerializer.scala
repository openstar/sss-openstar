package sss.openstar.identityledger.serialize

import sss.ancillary.Serialize._
import sss.openstar.identityledger._

object SetAttributeSerializer extends Serializer[SetAttribute] {

  def toBytes(setAttribute: SetAttribute): Array[Byte] = {
    ByteSerializer(SetAttributeCode) ++
      LongSerializer(setAttribute.uniqueMessage) ++
      StringSerializer(setAttribute.identity) ++
      ByteArraySerializer(setAttribute.attr.toBytes)
      .toBytes
  }

  def fromBytes(bytes: Array[Byte]): SetAttribute = {
    val (code, unique, identity, attr) = bytes.extract(
      ByteDeSerialize,
      LongDeSerialize,
      StringDeSerialize,
      ByteArrayDeSerialize(_.toLedgerAttribute)
    )

    require(
      code == SetAttributeCode,
      s"Wrong leading byte for SetAttribute ${bytes.head} instead of $SetAttributeCode")

    new SetAttribute(identity, attr) {
      private[identityledger] override val uniqueMessage: Long = unique
    }
  }
}

object RemoveAttributeSerializer extends Serializer[RemoveAttribute] {

  override def toBytes(t: RemoveAttribute): Array[Byte] = {
    ByteSerializer(RemoveAttributeCode) ++
    LongSerializer(t.uniqueMessage) ++
    StringSerializer(t.identity) ++
      ByteSerializer(t.attrId.toByte)
        .toBytes
  }

  override def fromBytes(bytes: Array[Byte]): RemoveAttribute = {

    val (code, unique, identity, attrId) = bytes.extract(ByteDeSerialize, LongDeSerialize, StringDeSerialize, ByteDeSerialize)

    require(
      code == RemoveAttributeCode,
      s"Wrong leading byte for RemoveAttribute ${bytes.head} instead of $RemoveAttributeCode")

    new RemoveAttribute(identity, AttributeCategory(attrId)) {
      private[identityledger] override val uniqueMessage: Long = unique
    }
  }

}

object SetOwnedAttributeSerializer extends Serializer[SetOwnedAttribute] {

  def toBytes(setOwnedAttribute: SetOwnedAttribute): Array[Byte] = {
    ByteSerializer(SetOwnedAttributeCode) ++
      LongSerializer(setOwnedAttribute.uniqueMessage) ++
      StringSerializer(setOwnedAttribute.authorizedOwner) ++
      StringSerializer(setOwnedAttribute.identity) ++
      ByteArraySerializer(setOwnedAttribute.attr.toBytes)
        .toBytes
  }

  def fromBytes(bytes: Array[Byte]): SetOwnedAttribute = {
    val (code, unique, authorizedOwner, identity, attr) = bytes.extract(
      ByteDeSerialize,
      LongDeSerialize,
      StringDeSerialize,
      StringDeSerialize,
      ByteArrayDeSerialize(_.toLedgerAttribute)
    )

    require(
      code == SetOwnedAttributeCode,
      s"Wrong leading byte for SetOwnedAttribute ${bytes.head} instead of $SetOwnedAttributeCode")

    new SetOwnedAttribute(authorizedOwner, identity, attr) {
      private[identityledger] override val uniqueMessage: Long = unique
    }
  }
}

object RemoveOwnedAttributeSerializer extends Serializer[RemoveOwnedAttribute] {

  override def toBytes(t: RemoveOwnedAttribute): Array[Byte] = {
    ByteSerializer(RemoveOwnedAttributeCode) ++
      LongSerializer(t.uniqueMessage) ++
      StringSerializer(t.authorizedOwner) ++
      StringSerializer(t.identity) ++
      ByteSerializer(t.attrId.toByte)
        .toBytes
  }

  override def fromBytes(bytes: Array[Byte]): RemoveOwnedAttribute = {

    val (code, unique, authorizedOwner, identity, attrId) = bytes.extract(
      ByteDeSerialize,
      LongDeSerialize,
      StringDeSerialize,
      StringDeSerialize,
      ByteDeSerialize
    )

    require(
      code == RemoveOwnedAttributeCode,
      s"Wrong leading byte for RemoveOwnedAttribute ${bytes.head} instead of $RemoveOwnedAttributeCode")

    new RemoveOwnedAttribute(authorizedOwner, identity, AttributeCategory(attrId)) {
      private[identityledger] override val uniqueMessage: Long = unique
    }
  }

}