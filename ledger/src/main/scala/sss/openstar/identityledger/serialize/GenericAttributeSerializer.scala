package sss.openstar.identityledger.serialize

import sss.ancillary.Serialize._
import sss.openstar.identityledger.{GenericAttribute, GenericAttributeCategory}

object GenericAttributeSerializer extends Serializer[GenericAttribute] {

  override def toBytes(t: GenericAttribute): Array[Byte] = {
    (ByteSerializer(t.category.toByte) ++
      ByteArraySerializer(t.content))
      .toBytes
  }

  override def fromBytes(b: Array[Byte]): GenericAttribute = {
    val (cat, content) = b.extract(ByteDeSerialize, ByteArrayDeSerialize)
    GenericAttribute(content, GenericAttributeCategory(cat))
  }
}
