package sss.openstar.identityledger.serialize

import scorex.crypto.signatures.PublicKey
import sss.ancillary.Serialize._
import sss.openstar.identityledger._

/**
  * Created by alan on 5/31/16.
  */
object Claim0Serializer extends Serializer[Claim0] {
  def toBytes(claim: Claim0): Array[Byte] = {
    (ByteSerializer(Claim0Code) ++
      LongSerializer(claim.uniqueMessage) ++
      StringSerializer(claim.authorizedOwner) ++
      StringSerializer(claim.identity) ++
      ByteArraySerializer(claim.roleAttribute.toBytes) ++
      ByteArraySerializer(claim.pKey)).toBytes
  }

  def fromBytes(bytes: Array[Byte]): Claim0 = {

    val (claimCode, unique, authorizedOwner, identity, attr, pKey) =
      bytes.extract(ByteDeSerialize,
        LongDeSerialize,
        StringDeSerialize,
        StringDeSerialize,
        ByteArrayDeSerialize(_.toSpecificLedgerAttribute[IdentityRoleAttribute]),
        ByteArrayDeSerialize)

    require(claimCode == Claim0Code,
            s"Wrong leading byte for Claim ${bytes.head} instead of $Claim0Code")
    new Claim0(authorizedOwner, identity, PublicKey(pKey), attr) {
      private[identityledger] override val uniqueMessage: Long = unique
    }
  }

}
