package sss.openstar.identityledger.serialize

import sss.ancillary.Serialize.{ByteArrayDeSerialize, ByteArraySerializer, IntDeSerialize, IntSerializer, OptionDeSerialize, OptionSerializer, SerializeHelper, Serializer, StringDeSerialize, StringSerializer}
import sss.openstar.account.serialize.{NodeIdTagSerializer, TypedPublicKeySerializer}
import sss.openstar.identityledger.SignerVerifierDetails

object SignerVerifierDetailsSerializer extends Serializer[SignerVerifierDetails] {

  override def toBytes(t: SignerVerifierDetails): Array[Byte] = {
    ByteArraySerializer(NodeIdTagSerializer.toBytes(t.nodeIdTag)) ++
      OptionSerializer(t.externalKeyIdOpt, ByteArraySerializer) ++
      ByteArraySerializer(TypedPublicKeySerializer.toBytes(t.typedPublicKey)) ++
      IntSerializer(t.securityLevel) ++
      StringSerializer(t.signerType) ++
      StringSerializer(t.interactionType)
        .toBytes
  }

  override def fromBytes(b: Array[Byte]): SignerVerifierDetails = {
    val extracted =
      b.extract(
        ByteArrayDeSerialize(NodeIdTagSerializer.fromBytes),
        OptionDeSerialize(ByteArrayDeSerialize),
        ByteArrayDeSerialize(TypedPublicKeySerializer.fromBytes),
        IntDeSerialize,
        StringDeSerialize,
        StringDeSerialize,
      )
      SignerVerifierDetails.tupled(extracted)
    }

}
