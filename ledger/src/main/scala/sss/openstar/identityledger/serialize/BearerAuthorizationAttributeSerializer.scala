package sss.openstar.identityledger.serialize

import sss.ancillary.Serialize._
import sss.openstar.identityledger._
import sss.openstar.util.serialize.HashedSecretSerializer

/**
  * Created by alan on 6/1/16.
  */

object BearerAuthorizationAttributeSerializer extends Serializer[BearerAuthorizationAttribute] {

  override def toBytes(t: BearerAuthorizationAttribute): Array[Byte] = {
    (ByteSerializer(SystemAttributeCategory.BearerAuthorization.toByte) ++
      ByteArraySerializer(HashedSecretSerializer.toBytes(t.hashOfSecret))).toBytes
  }

  override def fromBytes(b: Array[Byte]): BearerAuthorizationAttribute = {
    require(b.head == SystemAttributeCategory.BearerAuthorization.toByte)
    BearerAuthorizationAttribute(b.tail.extract(ByteArrayDeSerialize(HashedSecretSerializer.fromBytes)))
  }
}
