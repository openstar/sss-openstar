package sss.openstar.identityledger.serialize

import sss.ancillary.Serialize._
import sss.openstar.identityledger._

/**
  * Created by alan on 5/31/16.
  */
object DeleteSerializer extends Serializer[Delete] {

  def toBytes(deleteIdentity: Delete): Array[Byte] = {
    ByteSerializer(DeleteCode) ++
      LongSerializer(deleteIdentity.uniqueMessage) ++
      StringSerializer(deleteIdentity.authorizedOwner) ++
      StringSerializer(deleteIdentity.identity).toBytes
  }

  def fromBytes(bytes: Array[Byte]): Delete = {
    val (code, unique, authOwner, id) = bytes.extract(ByteDeSerialize,
      LongDeSerialize,
      StringDeSerialize,
      StringDeSerialize)

    require(code == DeleteCode,
            s"Wrong leading byte for Delete ${bytes.head} instead of $DeleteCode")
    new Delete(authOwner, id) {
      private[identityledger] override val uniqueMessage: Long = unique
    }
  }

}
