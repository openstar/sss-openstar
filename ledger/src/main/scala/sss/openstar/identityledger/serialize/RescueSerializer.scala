package sss.openstar.identityledger.serialize

import scorex.crypto.signatures.PublicKey
import sss.ancillary.Serialize._
import sss.openstar.identityledger._

/**
  * Created by alan on 6/1/16.
  */
object RescueSerializer extends Serializer[Rescue] {
  def toBytes(rescuer: Rescue): Array[Byte] = {
    ByteSerializer(RescueCode) ++
      LongSerializer(rescuer.uniqueMessage) ++
      StringSerializer(rescuer.rescuer) ++
      ByteArraySerializer(SignerVerifierDetailsSerializer.toBytes(rescuer.signerVerifierDetails)).toBytes
  }

  def fromBytes(bytes: Array[Byte]): Rescue = {
    val extracted = bytes.extract(ByteDeSerialize,
                                  LongDeSerialize,
                                  StringDeSerialize,
                                  ByteArrayDeSerialize)

    require(
      extracted._1 == RescueCode,
      s"Wrong leading byte for Rescue ${bytes.head} instead of $RescueCode")

    new Rescue(extracted._3, SignerVerifierDetailsSerializer.fromBytes(extracted._4)) {
      private[identityledger] override val uniqueMessage: Long = extracted._2
    }
  }
}
