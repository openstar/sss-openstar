package sss.openstar.identityledger.serialize

import sss.ancillary.Serialize.{ByteSerializer, Serializer, _}
import sss.openstar.identityledger.{IdentityRoleAttribute, SystemAttributeCategory}
import sss.openstar.util.IntBitSet

object IdentityRoleAttributeSerializer extends Serializer[IdentityRoleAttribute] {

  override def toBytes(t: IdentityRoleAttribute): Array[Byte] = {
    (ByteSerializer(SystemAttributeCategory.IdentityRole.toByte) ++
      IntSerializer(t.bitfield.value)).toBytes
  }

  override def fromBytes(b: Array[Byte]): IdentityRoleAttribute = {

    val extracted = b.extract(ByteDeSerialize, IntDeSerialize)

    require(extracted._1 == SystemAttributeCategory.IdentityRole.toByte,
      s"Wrong id? Should be ${SystemAttributeCategory.IdentityRole.id}, is ${extracted._1}")

    IdentityRoleAttribute(IntBitSet(extracted._2))
  }
}