package sss.openstar.identityledger.serialize

import sss.ancillary.Serialize.Serializer
import sss.openstar.identityledger.{AddMessageStore, BearerAuthorizationAttribute, Claim, Claim0, Delete, EmailAttribute, GenericAttribute, IdentityRoleAttribute, Link, LinkRescuer, ProviderAttachmentUrlAttribute, ProviderChargeAttribute, RemoveAttribute, RemoveMessageStore, RemoveOwnedAttribute, Rescue, SetAttribute, SetOwnedAttribute, SetPaywall, UnLink, UnLinkByKey, UnLinkRescuer}

trait Serializers {
  implicit val IdentityRoleAttributeS: Serializer[IdentityRoleAttribute] = IdentityRoleAttributeSerializer
  implicit val ProviderAttachmentUrlAttributeS: Serializer[ProviderAttachmentUrlAttribute] = ProviderAttachmentUrlAttributeSerializer
  implicit val ProviderChargeAttributeS: Serializer[ProviderChargeAttribute] = ProviderChargeAttributeSerializer
  implicit val BearerAuthorizationAttributeS: Serializer[BearerAuthorizationAttribute] = BearerAuthorizationAttributeSerializer
  implicit val EmailAttributeS: Serializer[EmailAttribute] = EmailAttributeSerializer
  implicit val SetAttributeS: Serializer[SetAttribute] = SetAttributeSerializer
  implicit val RemoveAttributeS: Serializer[RemoveAttribute] = RemoveAttributeSerializer
  implicit val RemoveMessageStoreS: Serializer[RemoveMessageStore] = RemoveMessageStoreSerializer
  implicit val AddMessageStoreS: Serializer[AddMessageStore] = AddMessageStoreSerializer
  implicit val SetPaywallS: Serializer[SetPaywall] = SetPaywallSerializer
  implicit val ClaimS0: Serializer[Claim0] = Claim0Serializer
  implicit val ClaimS: Serializer[Claim] = ClaimSerializer
  implicit val LinkS: Serializer[Link] = LinkSerializer
  implicit val RescueS: Serializer[Rescue] = RescueSerializer
  implicit val LinkRescuerS: Serializer[LinkRescuer] = LinkRescuerSerializer
  implicit val UnLinkRescuerS: Serializer[UnLinkRescuer] = UnLinkRescuerSerializer
  implicit val UnLinkByKeyS: Serializer[UnLinkByKey] = UnLinkByKeySerializer
  implicit val UnLinkS: Serializer[UnLink] = UnLinkSerializer
  implicit val SetOwnedAttributeS: Serializer[SetOwnedAttribute] = SetOwnedAttributeSerializer
  implicit val RemoveOwnedAttributeS: Serializer[RemoveOwnedAttribute] = RemoveOwnedAttributeSerializer
  implicit val DeleteS: Serializer[Delete] = DeleteSerializer
  implicit val GenericAttributeS: Serializer[GenericAttribute] = GenericAttributeSerializer
}

object Serializers extends Serializers
