package sss.openstar.identityledger.serialize

import sss.ancillary.Serialize._
import sss.openstar.identityledger.{EmailAttribute, SystemAttributeCategory}

object EmailAttributeSerializer extends Serializer[EmailAttribute] {

  override def toBytes(t: EmailAttribute): Array[Byte] = {
    (ByteSerializer(SystemAttributeCategory.Email.toByte) ++
      StringSerializer(t.email)).toBytes
  }

  override def fromBytes(b: Array[Byte]): EmailAttribute = {
    require(b.head == SystemAttributeCategory.Email.toByte)
    EmailAttribute(b.tail.extract(StringDeSerialize))
  }
}