package sss.openstar.identityledger.serialize

import scorex.crypto.signatures.PublicKey
import sss.ancillary.Serialize._
import sss.openstar.identityledger._

/**
  * Created by alan on 5/31/16.
  */
object LinkSerializer extends Serializer[Link] {

  def toBytes(linkRescuer: Link): Array[Byte] = {
    ByteSerializer(LinkCode) ++
      LongSerializer(linkRescuer.uniqueMessage) ++
      ByteArraySerializer(SignerVerifierDetailsSerializer.toBytes(linkRescuer.signerVerifierDetails))
        .toBytes
  }

  def fromBytes(bytes: Array[Byte]): Link = {
    val extracted = bytes.extract(ByteDeSerialize,
                                  LongDeSerialize,
                                  ByteArrayDeSerialize)
    require(extracted._1 == LinkCode,
            s"Wrong leading byte for Link ${bytes.head} instead of $LinkCode")
    new Link(SignerVerifierDetailsSerializer.fromBytes(extracted._3)) {
      private[identityledger] override val uniqueMessage: Long = extracted._2
    }
  }

}
