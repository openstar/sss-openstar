package sss.openstar.identityledger.serialize

import sss.ancillary.Serialize._
import sss.openstar.identityledger._

/**
  * Created by alan on 5/31/16.
  */
object SetPaywallSerializer extends Serializer[SetPaywall] {
  def toBytes(setPaywall: SetPaywall): Array[Byte] = {
    (ByteSerializer(SetPaywallCode) ++
      LongSerializer(setPaywall.uniqueMessage) ++
      StringSerializer(setPaywall.identity) ++
      StringSerializer(setPaywall.category)) ++
      OptionSerializer(setPaywall.amount, LongSerializer)
      .toBytes
  }

  def fromBytes(bytes: Array[Byte]): SetPaywall = {

    val extracted = bytes.extract(
      ByteDeSerialize,
      LongDeSerialize,
      StringDeSerialize,
      StringDeSerialize,
      OptionDeSerialize(LongDeSerialize)
    )

    require(extracted._1 == SetPaywallCode,
            s"Wrong leading byte for SetPaywall ${bytes.head} instead of $SetPaywallCode")

    new SetPaywall(extracted._3, extracted._4, extracted._5) {
      private[identityledger] override val uniqueMessage: Long = extracted._2
    }
  }

}
