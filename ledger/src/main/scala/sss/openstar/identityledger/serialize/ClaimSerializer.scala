package sss.openstar.identityledger.serialize

import sss.ancillary.Serialize._
import sss.openstar.identityledger._

/**
  * Created by alan on 5/31/16.
  */
object ClaimSerializer extends Serializer[Claim] {

  def toBytes(claim: Claim): Array[Byte] = {
    ByteSerializer(ClaimCode) ++
      LongSerializer(claim.uniqueMessage) ++
      StringSerializer(claim.authorizedOwner) ++
      StringSerializer(claim.identity) ++
      SequenceSerializer(claim.attributes.map(_.toBytes)).toBytes ++
      ByteArraySerializer(SignerVerifierDetailsSerializer.toBytes(claim.signerVerifierDetails))
        .toBytes
  }

  def fromBytes(bytes: Array[Byte]): Claim = {

    val (claimCode, unique, authorizedOwner, identity, attr, details) =
      bytes.extract(ByteDeSerialize,
        LongDeSerialize,
        StringDeSerialize,
        StringDeSerialize,
        SequenceDeSerialize(_.toLedgerAttribute),
        ByteArrayDeSerialize(SignerVerifierDetailsSerializer.fromBytes))

    require(claimCode == ClaimCode,
            s"Wrong leading byte for Claim ${bytes.head} instead of $ClaimCode")

    new Claim(authorizedOwner, identity, details, attr) {
      private[identityledger] override val uniqueMessage: Long = unique
    }
  }

}
