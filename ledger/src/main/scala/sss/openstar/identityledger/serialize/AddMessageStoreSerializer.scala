package sss.openstar.identityledger.serialize

import sss.ancillary.Serialize._
import sss.openstar.identityledger._

/**
  * Created by alan on 6/1/16.
  */
object AddMessageStoreSerializer extends Serializer[AddMessageStore] {
  def toBytes(messageStore: AddMessageStore): Array[Byte] = {
    ByteSerializer(AddMessageStoreCode) ++
      LongSerializer(messageStore.uniqueMessage) ++
      StringSerializer(messageStore.identity) ++
      StringSerializer(messageStore.newStoreId).toBytes
  }

  def fromBytes(bytes: Array[Byte]): AddMessageStore = {
    val extracted = bytes.extract(ByteDeSerialize,
                                  LongDeSerialize,
                                  StringDeSerialize,
                                  StringDeSerialize)

    require(
      extracted._1 == AddMessageStoreCode,
      s"Wrong leading byte for AddMessageStore ${bytes.head} instead of $AddMessageStoreCode")

    new AddMessageStore(extracted._3, extracted._4) {
      private[identityledger] override val uniqueMessage: Long = extracted._2
    }
  }
}
