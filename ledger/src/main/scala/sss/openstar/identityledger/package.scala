package sss.openstar

import java.security.SecureRandom
import scorex.crypto.signatures.PublicKey
import sss.ancillary.ByteArrayComparisonOps._
import sss.ancillary.ByteArrayEncodedStrOps._
import sss.ancillary.Serialize.SerializerOps.{Deserialize, Serialize}
import sss.ancillary.Serialize._
import sss.openstar.account.NodeIdTag.defaultTag
import sss.openstar.account.NodeSigner.{InteractionType, KeyType, SecurityLevel, SignerType}
import sss.openstar.account.impl.EncryptedKeyFileAccountSignerCreator
import sss.openstar.account.{NodeIdTag, NodeVerifier, SignerInteractionTypes, SignerSecurityLevels, TypedPublicKey}
import sss.openstar.crypto.keypairs.{Curve25519AccountKeys}
import sss.openstar.hash.SecureCryptographicHash
import sss.openstar.identityledger.ReservedIdentityRoles.{NetworkAuditorRoleId, SelfSovereignRoleId}
import sss.openstar.ledger.{LedgerId, TxApplied, TxId}
import sss.openstar.util.{HashedSecret, IntBitSet}
import sss.openstar.identityledger.serialize.Serializers._
import sss.openstar.schemamigration.SqlSchemaNames

import java.util

/**
  * Created by alan on 5/31/16.
  */
package object identityledger {

  val MaxLenOfIdentity = SqlSchemaNames.Constants.MaxLenOfIdentity

  val defaultPaywallCategory: String = "unknown"

  case class IdentityLedgerTxApplied(txId: TxId, subEvents: BusEvent*)(implicit val ledgerId: LedgerId) extends TxApplied with BusEvent {
    override def events: Seq[BusEvent] = this +: subEvents

    override def toString: UniqueNodeIdentifier = s"txId: ${txId.toBase64Str}, ${super.toString}"
  }

  object Ops {
    implicit class ToVerifier(val signerVerifierDetails: SignerVerifierDetails) extends AnyVal {
      def toVerifier(implicit identityService: IdentityServiceQuery): NodeVerifier =
        identityService.toVerifier(signerVerifierDetails)
    }
  }

  case class MessageStoreProviders(
                                    nodeId: UniqueNodeIdentifier,
                                    providers: Set[UniqueNodeIdentifier]) extends BusEvent

  case class SignerVerifierDetails(
                                    nodeIdTag: NodeIdTag,
                                    externalKeyIdOpt: Option[Array[Byte]],
                                    typedPublicKey: TypedPublicKey,
                                    securityLevel: SecurityLevel,
                                    signerType: SignerType,
                                    interactionType: InteractionType
                                  ) {

    override def equals(obj: Any): Boolean = obj match {
      case that: SignerVerifierDetails =>
        that.nodeIdTag == nodeIdTag &&
          that.signerType == signerType &&
          that.securityLevel == securityLevel &&
          that.interactionType == interactionType &&
          that.typedPublicKey == typedPublicKey &&
          compareBinaryOpt(that.externalKeyIdOpt, externalKeyIdOpt)

      case _ => false
    }


    private def compareBinaryOpt(a: Option[Array[Byte]], b: Option[Array[Byte]]): Boolean = (a, b) match {
      case (None, None) => true
      case (Some(aryA), Some(aryB)) => util.Arrays.equals(aryA, aryB)
      case _ => false
    }
    override def hashCode(): Int =
        typedPublicKey.hashCode +
        interactionType.hashCode +
        signerType.hashCode +
        securityLevel.hashCode() +
          externalKeyIdOpt.hashCode()
  }

  object BackwardsCompatibilityOps {
    implicit class PublicKeyOps(val publicKey: PublicKey) extends AnyVal {
      def toDetails(nodeId: UniqueNodeIdentifier, tag: String = defaultTag): SignerVerifierDetails =

          SignerVerifierDetails(
            NodeIdTag(nodeId, tag),
            None,
            TypedPublicKey(publicKey, Curve25519AccountKeys.keysType),
            SignerSecurityLevels.SecurityLevel1,
            EncryptedKeyFileAccountSignerCreator.SignerType,
            SignerInteractionTypes.AutomaticSignerInteractionType
          )

    }
  }


  case class Paywall(
                      nodeId: UniqueNodeIdentifier,
                      category: String,
                      amount: Option[Long]) extends BusEvent {
    val isDefault: Boolean = category == defaultPaywallCategory
  }

  case class ChangedAttribute(
                               nodeId: UniqueNodeIdentifier,
                               attr: LedgerAttribute) extends BusEvent


  case class DeletedAttribute(
                               nodeId: UniqueNodeIdentifier,
                               attr: AttributeCategory) extends BusEvent


  abstract class IdentityLedgerTx {

    private[identityledger] val uniqueMessage = SecureRandom.getInstanceStrong.nextLong()

    lazy val txId: Array[Byte] = SecureCryptographicHash.hash(this.toBytes)

  }

  object IdentityRolesOps {
    implicit class ReservedIdentityRolesAccessor(val idRoleAttr: IdentityRoleAttribute) extends AnyVal {

      def isNetworkAuditor: Boolean = idRoleAttr.bitfield.get(NetworkAuditorRoleId)

      def setNetworkAuditor(): IdentityRoleAttribute =
        IdentityRoleAttribute(idRoleAttr.bitfield.set(NetworkAuditorRoleId))

      def isSelfSovereign: Boolean = idRoleAttr.bitfield.get(SelfSovereignRoleId)

      def setSelfSovereign(): IdentityRoleAttribute =
        IdentityRoleAttribute(idRoleAttr.bitfield.set(SelfSovereignRoleId))

      def unSetSelfSovereign(): IdentityRoleAttribute =
        IdentityRoleAttribute(idRoleAttr.bitfield.unSet(SelfSovereignRoleId))
    }
  }

  object ReservedIdentityRoles extends Enumeration {
    type ReservedIdentityRoles = Value
    val NetworkAuditorRole = Value(0)
    val NetworkAuditorRoleId: Short = NetworkAuditorRole.id.toShort
    val SelfSovereignRole = Value(1)
    val SelfSovereignRoleId: Short = SelfSovereignRole.id.toShort
  }

  private[identityledger] val Claim0Code = 1.toByte
  private[identityledger] val LinkCode = 2.toByte
  private[identityledger] val UnLinkByKeyCode = 3.toByte
  private[identityledger] val UnLinkCode = 4.toByte
  private[identityledger] val RescueCode = 5.toByte
  private[identityledger] val LinkRescuerCode = 6.toByte
  private[identityledger] val UnLinkRescuerCode = 7.toByte
  private[identityledger] val AddMessageStoreCode = 8.toByte
  private[identityledger] val RemoveMessageStoreCode = 9.toByte
  private[identityledger] val SetPaywallCode = 10.toByte
  private[identityledger] val SetAttributeCode = 11.toByte
  private[identityledger] val RemoveAttributeCode = 12.toByte
  private[identityledger] val SetOwnedAttributeCode = 13.toByte
  private[identityledger] val RemoveOwnedAttributeCode = 14.toByte
  private[identityledger] val DeleteCode = 15.toByte
  private[identityledger] val ClaimCode = 16.toByte

  // Attribute Category Codes
  private[identityledger] val ProviderChargeCode = 0.toByte
  private[identityledger] val ProviderAttachmentUrlCode = 1.toByte
  private[identityledger] val BearerAuthorizationCode = 2.toByte
  private[identityledger] val EmailCode = 3.toByte
  private[identityledger] val IdentityRoleCode = 4.toByte

  case class ProviderAttachmentUrlAttribute(baseUrl: String) extends LedgerAttribute {
    override val category: AttributeCategory = SystemAttributeCategory.ProviderAttachmentUrl
  }

  case class ProviderChargeAttribute(amount: Long) extends LedgerAttribute {
    override val category: AttributeCategory = SystemAttributeCategory.ProviderCharge
  }

  case class BearerAuthorizationAttribute(hashOfSecret: HashedSecret) extends LedgerAttribute {
    override val category: AttributeCategory = SystemAttributeCategory.BearerAuthorization
  }

  case class EmailAttribute(email: String) extends LedgerAttribute {
    override val category: AttributeCategory = SystemAttributeCategory.Email
  }

  object IdentityRoleAttribute {
    def apply(bitset: Int = 0): IdentityRoleAttribute = IdentityRoleAttribute(IntBitSet(bitset))
  }

  case class IdentityRoleAttribute(bitfield: IntBitSet) extends LedgerAttribute {
    override val category: AttributeCategory = SystemAttributeCategory.IdentityRole
  }

  case class GenericAttribute(content: Array[Byte], override val category: GenericAttributeCategory) extends LedgerAttribute {
    require(content.length <= 1024, s"Content length should not exceed 1024 bytes but found ${content.length}.")
  }

  sealed trait AttributeCategory {

    def toByte: Byte
  }

  object AttributeCategory {

    def apply(id: Byte): AttributeCategory = id match {
      case ProviderChargeCode => SystemAttributeCategory.ProviderCharge
      case ProviderAttachmentUrlCode => SystemAttributeCategory.ProviderAttachmentUrl
      case BearerAuthorizationCode => SystemAttributeCategory.BearerAuthorization
      case EmailCode => SystemAttributeCategory.Email
      case IdentityRoleCode => SystemAttributeCategory.IdentityRole
      case _ if GenericAttributeCategory.isValid(id) => GenericAttributeCategory(id)
      case _ => throw new IllegalArgumentException(s"$id neither match SystemAttributeCategory nor GenericAttributeCategory.")
    }
  }

  object SystemAttributeCategory extends Enumeration {
    type SystemAttributeCategory = Val with AttributeCategory
    val ProviderCharge = value(ProviderChargeCode)
    val ProviderAttachmentUrl = value(ProviderAttachmentUrlCode)
    val BearerAuthorization = value(BearerAuthorizationCode)
    val Email = value(EmailCode)
    val IdentityRole = value(IdentityRoleCode)

    private def value(id: Byte) = new Val(id) with AttributeCategory {
      override def toByte: Byte = id.toByte
    }
  }

  final case class GenericAttributeCategory(value: Byte) extends AttributeCategory {
    require(GenericAttributeCategory.isValid(value), s"Generic attribute category should lies between the range [-128, -1], but found $value.")

    override def toByte: Byte = value
  }

  object GenericAttributeCategory {
    val PublicKeyIdCategory: Byte = -2.toByte

    def isValid(value: Byte): Boolean = value > -128 && value <= -1
  }

  implicit class IdentityLedgerAttributeFromBytes(private val bytes: Array[Byte]) extends AnyVal {
    def toLedgerAttribute: LedgerAttribute = bytes.head match {
      case ProviderChargeCode => bytes.deserialize[ProviderChargeAttribute]
      case ProviderAttachmentUrlCode => bytes.deserialize[ProviderAttachmentUrlAttribute]
      case BearerAuthorizationCode => bytes.deserialize[BearerAuthorizationAttribute]
      case EmailCode => bytes.deserialize[EmailAttribute]
      case IdentityRoleCode => bytes.deserialize[IdentityRoleAttribute]
      case other if GenericAttributeCategory.isValid(other) => bytes.deserialize[GenericAttribute]
      case other => throw new IllegalArgumentException(s"$other neither match SystemAttributeCategory nor GenericAttributeCategory.")
    }

    def toSpecificLedgerAttribute[T <: LedgerAttribute]: T = toLedgerAttribute.asInstanceOf[T]
  }

  implicit class IdentityLedgerAttributeToBytes(attr: LedgerAttribute) extends ToBytes {
    override def toBytes: Array[Byte] = attr match {
      case p: ProviderChargeAttribute => p.serialize
      case p: ProviderAttachmentUrlAttribute => p.serialize
      case p: BearerAuthorizationAttribute => p.serialize
      case p: EmailAttribute => p.serialize
      case p: IdentityRoleAttribute => p.serialize
      case p: GenericAttribute => p.serialize
    }
  }

  trait LedgerAttribute {
    val category: AttributeCategory
  }

  case class IdentityAttribute(identity: String, attr: LedgerAttribute)

  case class SetAttribute(identity: String,
                          attr: LedgerAttribute) extends IdentityLedgerTx

  case class SetOwnedAttribute(authorizedOwner: String,
                               identity: String,
                               attr: LedgerAttribute) extends IdentityLedgerTx

  case class RemoveAttribute(identity: String,
                          attrId: AttributeCategory) extends IdentityLedgerTx

  case class RemoveOwnedAttribute(authorizedOwner: String,
                                  identity: String,
                                  attrId: AttributeCategory) extends IdentityLedgerTx

  case class RemoveMessageStore(identity: String,
                                toBeRemovedId: UniqueNodeIdentifier) extends IdentityLedgerTx

  case class AddMessageStore(identity: String,
                             newStoreId: UniqueNodeIdentifier) extends IdentityLedgerTx

  case class SetPaywall(identity: UniqueNodeIdentifier,
                        category: String,
                        amount: Option[Long]) extends IdentityLedgerTx

  case class Claim0(authorizedOwner: String, identity: String, pKey: PublicKey, roleAttribute: IdentityRoleAttribute = IdentityRoleAttribute()) extends IdentityLedgerTx {
    override def equals(obj: scala.Any): Boolean = {
      obj match {
        case that: Claim0 =>
          that.authorizedOwner == authorizedOwner && that.identity == identity &&
            that.roleAttribute == roleAttribute && that.uniqueMessage == uniqueMessage &&
            (that.pKey isSame pKey)
        case _ => false
      }
    }

    override def hashCode(): Int = uniqueMessage.hashCode

    // This is what an old deprecated claim (Claim 0) looks like to the updated system
    def toSignerDetails: SignerVerifierDetails =
      BackwardsCompatibilityOps.PublicKeyOps(pKey).toDetails(identity)
  }

  object Claim {
    def apply(authorizedOwner: String,
              identity: String,
              signerVerifierDetails: SignerVerifierDetails,
              attribute: LedgerAttribute): Claim = Claim(authorizedOwner, identity, signerVerifierDetails, Seq(attribute))
  }


  case class Claim(authorizedOwner: String, //TODO remove, can be inferred from the toll payment sig
                   identity: String, // TODO remove duplicate, identity is in SignerVerifierDetails.NodeIdTag
                   signerVerifierDetails: SignerVerifierDetails,
                   attributes: Seq[LedgerAttribute] = Seq(IdentityRoleAttribute())) extends IdentityLedgerTx {

    override def equals(obj: scala.Any): Boolean = {
      obj match {
        case that: Claim =>
          that.authorizedOwner == authorizedOwner && that.identity == identity &&
            that.attributes == attributes && that.uniqueMessage == uniqueMessage &&
            that.signerVerifierDetails == signerVerifierDetails
        case _ => false
      }
    }

    override def hashCode(): Int = uniqueMessage.hashCode
  }

  case class Link(
                  signerVerifierDetails: SignerVerifierDetails) extends IdentityLedgerTx {
    override def equals(obj: scala.Any): Boolean = {
      obj match {
        case that: Link =>
          that.signerVerifierDetails == signerVerifierDetails &&
            that.uniqueMessage == uniqueMessage

        case _ => false
      }
    }

    override def hashCode(): Int = uniqueMessage.hashCode
  }

  case class Rescue(rescuer: String, signerVerifierDetails: SignerVerifierDetails)
    extends IdentityLedgerTx {

    override def equals(obj: scala.Any): Boolean = {
      obj match {
        case that: Rescue =>
          that.signerVerifierDetails == signerVerifierDetails &&
          that.rescuer == rescuer &&
            that.uniqueMessage == uniqueMessage

        case _ => false
      }
    }

    override def hashCode(): Int = uniqueMessage.hashCode
  }

  case class LinkRescuer(rescuer: String, identity: String) extends IdentityLedgerTx

  case class UnLinkRescuer(rescuer: String, identity: String) extends IdentityLedgerTx

  case class UnLinkByKey(identity: String, pKey: PublicKey) extends IdentityLedgerTx {
    override def equals(obj: scala.Any): Boolean = {
      obj match {
        case that: UnLinkByKey =>
          that.identity == identity &&
            that.uniqueMessage == uniqueMessage &&
            (that.pKey isSame pKey)
        case _ => false
      }
    }

    override def hashCode(): Int = uniqueMessage.hashCode
  }

  case class Delete(authorizedOwner: String, identity: String) extends IdentityLedgerTx

  case class UnLink(identity: String, tag: String) extends IdentityLedgerTx

  implicit class IdentityLedgerTxFromBytes(private val bytes: Array[Byte]) extends AnyVal {
    def toIdentityLedgerTx: IdentityLedgerTx = bytes.head match {
      case Claim0Code => bytes.deserialize[Claim0]
      case ClaimCode => bytes.deserialize[Claim]
      case LinkCode => bytes.deserialize[Link]
      case UnLinkByKeyCode => bytes.deserialize[UnLinkByKey]
      case UnLinkCode => bytes.deserialize[UnLink]
      case RescueCode => bytes.deserialize[Rescue]
      case LinkRescuerCode => bytes.deserialize[LinkRescuer]
      case UnLinkRescuerCode => bytes.deserialize[UnLinkRescuer]
      case AddMessageStoreCode => bytes.deserialize[AddMessageStore]
      case RemoveMessageStoreCode => bytes.deserialize[RemoveMessageStore]
      case SetPaywallCode => bytes.deserialize[SetPaywall]
      case RemoveAttributeCode => bytes.deserialize[RemoveAttribute]
      case SetAttributeCode => bytes.deserialize[SetAttribute]
      case SetOwnedAttributeCode => bytes.deserialize[SetOwnedAttribute]
      case RemoveOwnedAttributeCode => bytes.deserialize[RemoveOwnedAttribute]
      case DeleteCode => bytes.deserialize[Delete]
    }
  }

  implicit class IdentityLedgerTxToBytes(private val idLedgerMsg: IdentityLedgerTx) extends AnyVal {
    def toBytes: Array[Byte] = idLedgerMsg match {
      case msg: Claim0 => msg.serialize
      case msg: Claim => msg.serialize
      case msg: Link => msg.serialize
      case msg: UnLink => msg.serialize
      case msg: UnLinkByKey => msg.serialize
      case msg: LinkRescuer => msg.serialize
      case msg: Rescue => msg.serialize
      case msg: UnLinkRescuer => msg.serialize
      case msg: AddMessageStore => msg.serialize
      case msg: RemoveMessageStore => msg.serialize
      case msg: SetPaywall => msg.serialize
      case msg: RemoveAttribute => msg.serialize
      case msg: SetAttribute => msg.serialize
      case msg: SetOwnedAttribute => msg.serialize
      case msg: RemoveOwnedAttribute => msg.serialize
      case msg: Delete => msg.serialize
    }
  }
}


