package sss.openstar.identityledger

import scorex.crypto.signatures.PublicKey
import sss.db.FutureTx
import sss.openstar.UniqueNodeIdentifier
import sss.openstar.account.{NodeIdTag, NodeVerifier}

trait IdentityServiceQuery {
  val defaultTag: String = IdentityService.defaultTag

  val identifier: Identifier

  def matches(identity: UniqueNodeIdentifier, publicKey: PublicKey): Boolean

  def identify(publicKey: PublicKey): Option[NodeIdTag]

  def account(identity: UniqueNodeIdentifier, tag: String = defaultTag): SignerVerifierDetails

  def accountFTx(identity: UniqueNodeIdentifier, tag: String = defaultTag): FutureTx[SignerVerifierDetails]

  def accounts(identity: UniqueNodeIdentifier): Seq[SignerVerifierDetails]

  def accountsFTx(identity: String): FutureTx[Seq[SignerVerifierDetails]]

  def accountOpt(identity: UniqueNodeIdentifier, tag: String = defaultTag): Option[SignerVerifierDetails]

  def exists(identity: UniqueNodeIdentifier, tag: String = defaultTag): Boolean = accountOpt(identity, tag).nonEmpty

  def accountOptFTx(identity: UniqueNodeIdentifier, tag: String = defaultTag): FutureTx[Option[SignerVerifierDetails]]

  def rescuersFTx(identity: UniqueNodeIdentifier): FutureTx[Seq[UniqueNodeIdentifier]]

  def rescuers(identity: UniqueNodeIdentifier): Seq[UniqueNodeIdentifier]

  def list(startsWith: Option[String], startIndex: Long = 0, pageSize: Int = Int.MaxValue): Seq[String]

  def messageStores(identity: UniqueNodeIdentifier): Set[UniqueNodeIdentifier]

  def listAttributes(identity: UniqueNodeIdentifier): Seq[LedgerAttribute]

  def attribute(identity: UniqueNodeIdentifier, category: AttributeCategory): Option[LedgerAttribute]

  def attributeFTx(identity: UniqueNodeIdentifier, category: AttributeCategory): FutureTx[Option[LedgerAttribute]]

  def listByAttribute(attribute: AttributeCategory, startIndex: Long, pageSize: Int): Seq[IdentityAttribute]

  def listOwnedAttributes(identity: UniqueNodeIdentifier): Seq[LedgerAttribute]

  def ownedAttribute(identity: UniqueNodeIdentifier, category: AttributeCategory): Option[LedgerAttribute]

  def roleOwnedAttribute(identity: UniqueNodeIdentifier): IdentityRoleAttribute

  def listByOwnedAttribute(attribute: AttributeCategory, startIndex: Long, pageSize: Int): Seq[IdentityAttribute]

  def count: Long

  def count(startsWithOpt: Option[String]): Long

  def paywalls(identity: UniqueNodeIdentifier): Seq[Paywall]

  def getCurrentCharge(identity: UniqueNodeIdentifier, category: String = defaultPaywallCategory): Long = {
    val paywallsForId = paywalls(identity)

    paywallsForId
      .find(_.category == category)
      .flatMap(_.amount)
      .getOrElse(
        paywallsForId
          .find(_.isDefault)
          .flatMap(_.amount)
          .get
      )
  }

  val maxKeys: Int

  val maxPaywalls: Int

  val maxMessageStores: Int

  def toVerifier(signerVerifierDetails: SignerVerifierDetails): NodeVerifier

}