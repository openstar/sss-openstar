package sss.openstar.identityledger

import sss.openstar.UniqueNodeIdentifier
import sss.openstar.common.result.{ErrorMessage, Res}
import sss.openstar.common.result.ResultOps._
import sss.openstar.identityledger.Identifier.ErrorMessageCode

import java.util.Locale
import java.util.regex.Pattern

object Identifier {
  val ErrorMessageCode = "InvalidIdentifier"
}

trait Identifier {

  def toIdentifier(identifierCandidate: String): Res[UniqueNodeIdentifier]

  def isValidIdentifier(identifierCandidate: String): Boolean =
    toIdentifier(identifierCandidate)
      .isRight

  def unsafe(identifierAsString: String): UniqueNodeIdentifier = toIdentifier(identifierAsString).get
}

class DefaultIdentifier extends Identifier {

  private[identityledger] val identityRegex = "^[\\p{L}0-9_]*$"
  private[identityledger] val pattern = Pattern.compile(identityRegex)

  override def toIdentifier(identifierCandidate: String): Res[UniqueNodeIdentifier] = {

    val isAllLower = identifierCandidate.toLowerCase(Locale.ENGLISH) == identifierCandidate
    val notTooLong = identifierCandidate.length <= MaxLenOfIdentity
    val matchesPattern = pattern.matcher(identifierCandidate).matches()

    (isAllLower, notTooLong, matchesPattern) match {
      case (true, true, true) => Right(identifierCandidate)
      case _ =>

        val msgBuilder: StringBuilder = new StringBuilder("Identifier:")

        def addIfFalse(cond: Boolean, errorDesc: String) = {
          if(!cond) msgBuilder.append(s" $errorDesc,")
        }

        addIfFalse(isAllLower, "must be all lower case")
        addIfFalse(notTooLong, s"must be shorter than $MaxLenOfIdentity")
        addIfFalse(matchesPattern, s"must only contain alpha numeric characters, a,b,c,1,2,3 ...")

        val msg :String = msgBuilder.toString()

        Left(ErrorMessage(
          code = ErrorMessageCode,
          logMsg = msg,
          userMsg = msg,
        ))
    }
  }



}


