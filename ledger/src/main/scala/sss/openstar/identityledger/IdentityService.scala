package sss.openstar.identityledger

import scorex.crypto.signatures.PublicKey
import sss.ancillary.ByteArrayEncodedStrOps._
import sss.ancillary.{Logging, PagesToStream}
import sss.db.RowSerializer._
import sss.db._
import sss.db.ops.DbOps.{DbRunOps, FutureTxOps, OptFutureTxOps}
import sss.openstar.UniqueNodeIdentifier
import sss.openstar.account.NodeSigner.{InteractionType, KeyType, SecurityLevel, SignerType}
import sss.openstar.account.{NodeIdTag, NodeVerifier, TypedPublicKey, VerifierFactory}
import sss.openstar.common.result.exception
import sss.openstar.hash.Digest32
import sss.openstar.identityledger.IdentityRolesOps.ReservedIdentityRolesAccessor
import sss.openstar.util.StringCheck.SimpleTag
import sss.openstar.util.{HashUtils, TableCopy}

import java.sql.SQLIntegrityConstraintViolationException
import java.util.Date
import scala.util.{Success, Try}


/**
 * Created by alan on 5/30/16.
 */

trait IdentityService extends IdentityServiceQuery {

  def deleteCheckpoint(height: Long): FutureTx[Unit]

  def importState(height: Long): FutureTx[Unit]

  def createCheckpoint(id: Long): FutureTx[Try[Digest32]]

  def claim(
            balance: Long,
            licensedUntil: Option[Long],
            signerVerifier: SignerVerifierDetails,
            attributes: Seq[LedgerAttribute]
           ): FutureTx[Unit]

  def claim(
            signerVerifier: SignerVerifierDetails,
            attributes: Seq[LedgerAttribute] = Seq(IdentityRoleAttribute())
           ): FutureTx[Unit] = claim( 0, None, signerVerifier, attributes)

  def deleteIdentity(identity: UniqueNodeIdentifier): FutureTx[Unit]

  def link(signerVerifierDetails: SignerVerifierDetails): FutureTx[Unit] =
    link(
      signerVerifierDetails.nodeIdTag.nodeId,
      signerVerifierDetails.typedPublicKey.publicKey,
      signerVerifierDetails.typedPublicKey.keyType,
      signerVerifierDetails.securityLevel,
      signerVerifierDetails.signerType,
      signerVerifierDetails.interactionType,
      signerVerifierDetails.externalKeyIdOpt,
      signerVerifierDetails.nodeIdTag.tag
    )

  def link(identity: UniqueNodeIdentifier,
           publicKey: PublicKey,
           keyType: KeyType,
           securityLevel: SecurityLevel,
           signerType: SignerType,
           interactionType: InteractionType,
           keyExternalKeyId: Option[Array[Byte]],
           tag: String): FutureTx[Unit]

  def unlink(identity: UniqueNodeIdentifier, publicKey: PublicKey): FutureTx[Boolean]

  def unlink(identity: UniqueNodeIdentifier, tag: String): FutureTx[Boolean]

  def linkRescuer(identity: UniqueNodeIdentifier, rescuer: UniqueNodeIdentifier): FutureTx[Unit]

  def unLinkRescuer(identity: UniqueNodeIdentifier, rescuer: UniqueNodeIdentifier): FutureTx[Unit]

  def addMessageStore(identity: UniqueNodeIdentifier, store: UniqueNodeIdentifier): FutureTx[MessageStoreProviders]

  def removeMessageStore(identity: UniqueNodeIdentifier, store: UniqueNodeIdentifier): FutureTx[MessageStoreProviders]

  def setPaywall(identity: UniqueNodeIdentifier, category: String, amount: Long): FutureTx[Paywall]

  def removePaywall(identity: UniqueNodeIdentifier, category: String): FutureTx[Option[Paywall]]

  def setAttribute(identity: UniqueNodeIdentifier, attr: LedgerAttribute): FutureTx[ChangedAttribute]

  def removeAttribute(identity: UniqueNodeIdentifier, attr: AttributeCategory): FutureTx[Option[DeletedAttribute]]

  def setOwnedAttribute(identity: UniqueNodeIdentifier, attr: LedgerAttribute): FutureTx[ChangedAttribute]

  def removeOwnedAttribute(identity: UniqueNodeIdentifier, attr: AttributeCategory): FutureTx[Option[DeletedAttribute]]


}

object IdentityService {

  val defaultTag: String = NodeIdTag.defaultTag


  def apply(
             idLabeler: Identifier = new DefaultIdentifier,
             maxKeysPerIdentity: Int = 10,
             maxRescuersPerIdentity: Int = 5,
             maxMessageStoresPerIdentity: Int = 5,
             maxPaywallsPerIdentity: Int = 5,
             defaultPaywallAmount: Long = 0
  )(implicit db: Db, verifierFactory: VerifierFactory): IdentityService = new IdentityService with TableCopy with Logging {

    override val identifier: Identifier = idLabeler

    override val maxKeys = maxKeysPerIdentity
    override val maxMessageStores = maxMessageStoresPerIdentity
    override val maxPaywalls = maxPaywallsPerIdentity

    private val defaultKnownContactPaywallCategory = "contact"
    private val defaultKnownContactPaywallCategoryAmount = 0L

    private def norm(identifierStr: UniqueNodeIdentifier): String = identifier.unsafe(identifierStr)

    import sss.openstar.schemamigration.SqlSchemaNames.ColumnNames._
    import sss.openstar.schemamigration.SqlSchemaNames.TableNames._

    private val ownedAttributeTablePk = pkConstraintSql(
      ownedAttributeTableName,
      Seq(
        identityLnkCol,
        categoryCol
      )
    )
    private val attributeTablePk = pkConstraintSql(
      attributeTableName,
      Seq(
        identityLnkCol,
        categoryCol
      )
    )
    private val paywallTablePk = pkConstraintSql(
      paywallTableName,
      Seq(
        identityLnkCol,
        categoryCol,
        amountCol
      )
    )
    private val recoveryTablePk = pkConstraintSql(
      recoveryIdentitiesTableName,
      Seq(
        identityLnkCol,
        identityCol,
        domainCol
      )
    )
    private val messageStorePk = pkConstraintSql(
      messageStoreTableName,
      Seq(
        identityLnkCol,
        storeNodeIdCol
      )
    )
    private val keyTablePk = pkConstraintSql(
      keyTableName,
      Seq(
        publicKeyCol
      )
    )
    private val idTablePk = pkConstraintSql(
      identityTableName,
      Seq(identityCol)
    )

    private val selectKeysSql =
      s"""SELECT a.$identityCol, b.$publicKeyCol, b.$tagCol
         |FROM $identityTableName a
         |JOIN $keyTableName b
         |ON a.$idCol = b.$identityLnkCol
         |ORDER BY a.$identityCol ASC
         |""".stripMargin

    private val selectRecoverySql =
      s"""SELECT a.$identityCol, b.$identityCol, b.$domainCol
         |FROM $identityTableName a
         |JOIN $recoveryIdentitiesTableName b
         |ON a.$idCol = b.$identityLnkCol
         |ORDER BY a.$identityCol ASC
         |""".stripMargin

    private val selectPaywallSql =
      s"""SELECT a.$identityCol, b.$categoryCol, b.$amountCol
         |FROM $identityTableName a
         |JOIN $paywallTableName b
         |ON a.$idCol = b.$identityLnkCol
         |ORDER BY a.$identityCol ASC
         |""".stripMargin

    private val selectOwnedAttributeSql =
      s"""SELECT a.$identityCol, b.$categoryCol, b.$attrValueCol
         |FROM $identityTableName a
         |JOIN $ownedAttributeTableName b
         |ON a.$idCol = b.$identityLnkCol
         |ORDER BY a.$identityCol ASC
         |""".stripMargin

    private val selectAttributeSql =
      s"""SELECT a.$identityCol, b.$categoryCol, b.$attrValueCol
         |FROM $identityTableName a
         |JOIN $attributeTableName b
         |ON a.$idCol = b.$identityLnkCol
         |ORDER BY a.$identityCol ASC
         |""".stripMargin

    private val selectMessageStoreSql =
      s"""SELECT a.$identityCol, b.$storeNodeIdCol
         |FROM $identityTableName a
         |JOIN $messageStoreTableName b
         |ON a.$idCol = b.$identityLnkCol
         |ORDER BY a.$identityCol ASC
         |""".stripMargin

    private val createMessageStoreView =
      s"""CREATE VIEW IF NOT EXISTS $messageStoreViewName AS
         |SELECT b.$idCol, a.$identityCol, b.$storeNodeIdCol, b.$createdCol
         |FROM $identityTableName a
         |JOIN $messageStoreTableName b
         |ON a.$idCol = b.$identityLnkCol;
         |""".stripMargin

    private val dropMessageViewSql = s"DROP VIEW IF EXISTS $messageStoreViewName"

    private def dropPKConstraints(): Seq[String] =
      Seq(
        identityTableName,
        keyTableName,
        recoveryIdentitiesTableName,
        messageStoreTableName,
        paywallTableName,
        attributeTableName,
        ownedAttributeTableName
      ).map { name =>
        dropPKConstraintSql(name)
      }

    private def dropFKConstraints(): Seq[String] =
      Seq(
        keyTableName,
        recoveryIdentitiesTableName,
        messageStoreTableName,
        paywallTableName,
        attributeTableName,
        ownedAttributeTableName
      ).map { name =>
        dropFKConstraintSql(name)
      }

    private val dropIdentityTblUnique =
      s"ALTER TABLE $identityTableName DROP CONSTRAINT ${identityTableName}_uq;"

    private val addIdentityTblUnique =
      s"ALTER TABLE $identityTableName ADD CONSTRAINT ${identityTableName}_uq UNIQUE($idCol);"

    private def addPKConstraints(): Seq[String] =
      Seq(
        (identityTableName, idTablePk),
        (keyTableName, keyTablePk),
        (recoveryIdentitiesTableName, recoveryTablePk),
        (messageStoreTableName, messageStorePk),
        (paywallTableName, paywallTablePk),
        (attributeTableName, attributeTablePk),
        (ownedAttributeTableName, ownedAttributeTablePk)
      ).map {
        case (name, cons) =>
          s"ALTER TABLE $name ADD $cons;"
      }

    private def addFKConstraints(): Seq[String] =
      Seq(
        keyTableName,
        recoveryIdentitiesTableName,
        messageStoreTableName,
        paywallTableName,
        attributeTableName,
        ownedAttributeTableName
      ).map { name =>
        addFkConstraintSql(name)
      }

    private def pkConstraintSql(tableName: String, cols: Seq[String]): String = {
      val colsStr = cols.mkString(",")
      s"CONSTRAINT ${tableName}_pk PRIMARY KEY ($colsStr)"
    }

    private def fkConstraintSql(tableName: String): String =
      s"CONSTRAINT ${tableName}_${identityLnkCol}_fk FOREIGN KEY ($identityLnkCol) REFERENCES $identityTableName($idCol)"

    private def addFkConstraintSql(tableName: String): String =
      s"ALTER TABLE $tableName ADD ${fkConstraintSql(tableName)};"

    private def dropPKConstraintSql(tableName: String): String =
      s"ALTER TABLE $tableName DROP CONSTRAINT ${tableName}_pk;"

    private def dropFKConstraintSql(tableName: String): String =
      s"ALTER TABLE $tableName DROP CONSTRAINT ${tableName}_${identityLnkCol}_fk;"

    private lazy val identityTable = db.table(identityTableName)
    private lazy val keyTable = db.table(keyTableName)
    private lazy val recoveryTable = db.table(recoveryIdentitiesTableName)
    private lazy val messageStoreTable = db.table(messageStoreTableName)
    private lazy val attributeTable = db.table(attributeTableName)
    private lazy val ownedAttributeTable = db.table(ownedAttributeTableName)
    private lazy val paywallTable = db.table(paywallTableName)
    private lazy val messageStoreView = db.view(messageStoreViewName)

    private def toIdOpt(identity: UniqueNodeIdentifier): FutureTx[Option[Long]] = identityTable.toLongIdOpt(s"$identityCol" -> norm(identity))

    private def exists(identity: UniqueNodeIdentifier): FutureTx[Boolean] =
      identityTable
        .toLongIdOpt(s"$identityCol" -> norm(identity))
        .map(_.isDefined)


    private def identityToId(identity: UniqueNodeIdentifier): FutureTx[Long] = {
      toIdOpt(identity).map(_.getOrElse(throw new IllegalArgumentException(s"No such identity $identity")))
    }

    private def usingIdentity[T](identity: UniqueNodeIdentifier)(f: Long => FutureTx[T]): FutureTx[T] =
      toIdOpt(identity).flatMap {
        case None        => throw new IllegalArgumentException(s"No such identity $identity")
        case Some(rowId) => f(rowId)
      }

    override def list(startsWithOpt: Option[String], startIndex: Long, pageSize: Int): Seq[String] =
      (startsWithOpt match {
        case Some(startsWith) if startsWith.nonEmpty =>
          Try(norm(startsWith)).map { likeThis =>
            where(s"$identityCol LIKE '${likeThis}%'")
              .limit(
                startIndex,
                pageSize
              )
          }

        case _ =>
          Try(
            where().limit(
              startIndex,
              pageSize
            )
          )

      }).map { w =>
        identityTable.map(
          r => r.string(identityCol),
          w
        ).dbRunSyncGet
      }.getOrElse(Seq.empty)

    override def listByOwnedAttribute(
                                       attribute: AttributeCategory,
                                       startIndex: Long,
                                       pageSize: Int
                                     ): Seq[IdentityAttribute] = {
      listByAttribute(ownedAttributeTable, attribute, startIndex, pageSize)
    }

    override def listByAttribute(attribute: AttributeCategory, startIndex: Long, pageSize: Int): Seq[IdentityAttribute] = {
      listByAttribute(attributeTable, attribute, startIndex, pageSize)
    }

    private def listByAttribute(table: Table,
                                attribute: AttributeCategory,
                                startIndex: Long,
                                pageSize: Int
                               ): Seq[IdentityAttribute] = {


      //TODO Fix this. make async, fix query to join
      table
        .filter(
          where(categoryCol -> attribute.toByte)
            .limit(
              startIndex,
              pageSize
            )
        )
        .flatMap { rows: Rows =>
          FutureTx.sequence(
            rows.map(r =>
              identityTable(
                r.long(identityLnkCol)).map(idRow =>
                IdentityAttribute(idRow.string(identityCol), rowToAttribute(r))
              )
            ))
        }.dbRunSyncGet
    }

    override def matches(identity: UniqueNodeIdentifier, publicKey: PublicKey): Boolean =
      toIdOpt(identity).flatMap {
        case None => FutureTx.unit(None)
        case Some(rowId) =>
          val asChars = publicKey.toBase64Str
          keyTable
            .find(
              where(
                identityLnkCol -> rowId,
                publicKeyCol -> asChars
              )
            )

      }.dbRunSyncGet.isDefined

    override def unlink(identity: UniqueNodeIdentifier, tag: String): FutureTx[Boolean] = {
      for {
        id <- identityToId(identity)
        deleted <- keyTable.delete(
          where(
            identityLnkCol -> id,
            tagCol -> tag
          )
        )
          _ <- freeIdentityIfNoKeysOrRescuers(identity)
      } yield deleted == 1

    }


    private def deleteIdImpl(identityRowId: Long, identityForDeletion: UniqueNodeIdentifier): FutureTx[Unit] = {
      FutureTx.sequence {
        Seq(
          paywallTable delete where(identityLnkCol -> identityRowId),
          keyTable delete where(identityLnkCol -> identityRowId),
          messageStoreTable delete where(identityLnkCol -> identityRowId),
          attributeTable delete where(identityLnkCol -> identityRowId),
          ownedAttributeTable delete where(identityLnkCol -> identityRowId),
          recoveryTable delete where(identityCol -> identityForDeletion),
          recoveryTable delete where(identityLnkCol -> identityRowId),
          identityTable delete where(idCol -> identityRowId),
        )
      }.map(_ => ())
    }

    def deleteIdentity(identityForDeletion: UniqueNodeIdentifier): FutureTx[Unit] = {
      for {
        rowId <- identityToId(identityForDeletion)
        roleAttr <- attributeFTx(identityForDeletion, SystemAttributeCategory.IdentityRole)
        canDelete: Boolean = roleAttr.fold(true) {
          case role: IdentityRoleAttribute => !role.isSelfSovereign
        }
        _ = require(canDelete, "Cannot delete, Self Sovereign attribute set")
        _ <- deleteIdImpl(rowId, identityForDeletion)
      } yield ()
    }

    override def rescuers(identity: UniqueNodeIdentifier): Seq[UniqueNodeIdentifier] = rescuersFTx(identity).dbRunSyncGet

    override def rescuersFTx(identity: UniqueNodeIdentifier): FutureTx[Seq[UniqueNodeIdentifier]] = {

      for {
        fk <- identityTable.toLongId(identityCol -> norm(identity))
        rescuers <- recoveryTable.filter(identityLnkCol -> fk)
      } yield rescuers.map(_.string(identityCol))

    }

    private def freeIdentityIfNoKeysOrRescuers(identity: UniqueNodeIdentifier): FutureTx[Unit] = {
      val freeIfUnecessary = for {
        existingAccs <- accountsFTx(identity)
        existingRescuers <- rescuersFTx(identity)
      } yield(existingRescuers.isEmpty && existingAccs.isEmpty)

      lazy val deleteIdentity = for {
        idOpt <- toIdOpt(identity)
        rowId = idOpt.get
        _ <- deleteIdImpl(rowId, identity)
      } yield ()

      for {
        shouldFree <- freeIfUnecessary
        _ <- if(shouldFree) deleteIdentity
        else FutureTx.unit(())
      } yield ()
    }

    override def unlink(identity: String, publicKey: PublicKey): FutureTx[Boolean] = {
      for {
        id <- identityToId(identity)
        asChars = publicKey.toBase64Str
        result <- keyTable.delete(
          where(
            identityLnkCol -> id,
            publicKeyCol -> asChars
          )
        )
        _ <- freeIdentityIfNoKeysOrRescuers(identity)
      } yield result == 1

    }

    override def link(
                       identity: String,
                       publicKey: PublicKey,
                       keyType: KeyType,
                       securityLevel: SecurityLevel,
                       signerType: SignerType,
                       interactionType: InteractionType,
                       keyExternalId: Option[Array[Byte]],
                       tag: String): FutureTx[Unit] = {
      require(
        accounts(identity).size <= maxKeys,
        s"No more than $maxKeys keys allowed per identity."
      )

      for {
        id <- identityToId(identity)
        asChars = publicKey.toBase64Str
        _ <- keyTable.insert(
          Map(
            identityLnkCol -> id,
            publicKeyCol -> asChars,
            securityLevelCol -> securityLevel,
            interactionTypeCol -> interactionType,
            signerTypeCol -> signerType,
            keyTypeCol -> keyType,
            keyExternalIdCol -> keyExternalId,
            tagCol -> tag,
            createdCol -> new Date().getTime
          )
        )
      } yield ()

    }

    override def accountOpt(identity: String, tag: String): Option[SignerVerifierDetails] = {
      accountOptFTx(identity, tag).dbRunSyncGet
    }

    override def accountOptFTx(identity: String, tag: String): FutureTx[Option[SignerVerifierDetails]] = {

      val dbPlan = for {
        idOpt <- toIdOpt(identity)
        accOptOpt <- idOpt.map(id => keyTable
          .find(
            where(
              identityLnkCol -> id,
              tagCol -> tag
            )
          )
        ).toFutureTxOpt
      } yield accOptOpt.flatten

      dbPlan.map(_.map(rowToSignerVerifierDetails(identity , _)))
    }

    override def account(identity: String, tag: String): SignerVerifierDetails = accountFTx(identity, tag).dbRunSyncGet

    override def accountFTx(identity: String, tag: String): FutureTx[SignerVerifierDetails] =
      accountOptFTx(
        identity,
        tag
      ).map(_.get)

    override def accounts(identity: String): Seq[SignerVerifierDetails] = accountsFTx(identity).dbRunSyncGet

    override def accountsFTx(identity: String): FutureTx[Seq[SignerVerifierDetails]] =
      toIdOpt(identity) flatMap {
        case None => FutureTx.unit(Seq.empty)
        case Some(rowId) =>
          keyTable.filter(where(identityLnkCol -> rowId)).map(
            _.map(rowToSignerVerifierDetails(identity, _))
          )
      }

    override def identify(publicKey: PublicKey): Option[NodeIdTag] = {
      val pKey = publicKey.toBase64Str
      for {
        rowOpt <- keyTable.find(where(publicKeyCol -> pKey))
        linkIdWithTagOpt = rowOpt.map(r => (r.long(identityLnkCol), r.string(tagCol)))
        idWithTag <- linkIdWithTagOpt.map {
          case (lnk, tag) => identityTable(lnk).map(r => (r.string(identityCol), tag))
        }.toFutureTxOpt
      } yield idWithTag.map { case (id, tag) => NodeIdTag(id, tag) }
    }.dbRunSyncGet


    private def rowToSignerVerifierDetails(nodeId: UniqueNodeIdentifier, row: Row): SignerVerifierDetails = {
      SignerVerifierDetails(
        nodeIdTag = NodeIdTag(nodeId, row.string(tagCol)),
        externalKeyIdOpt = row.arrayByteOpt(keyExternalIdCol),
        typedPublicKey = TypedPublicKey(PublicKey(row.string(publicKeyCol).fromBase64Str), row.string(keyTypeCol)),
        securityLevel = row.int(securityLevelCol),
        signerType = row.string(signerTypeCol),
        interactionType = row.string(interactionTypeCol)
      )
    }

    private def signerVerifierDetailsToRowInsertMap(details: SignerVerifierDetails): Map[String, Any] = Map(
      publicKeyCol -> details.typedPublicKey.publicKey.toBase64Str,
      tagCol -> details.nodeIdTag.tag,
      interactionTypeCol -> details.interactionType,
      securityLevelCol -> details.securityLevel,
      signerTypeCol -> details.signerType,
      keyTypeCol -> details.typedPublicKey.keyType,
      keyExternalIdCol -> details.externalKeyIdOpt
    )

    override def claim(
                       balance: Long,
                       licensedUntil: Option[Long],
                       signerVerifierDetail: SignerVerifierDetails,
                       ownedIdentityAttributes: Seq[LedgerAttribute]): FutureTx[Unit] = {

      val identity = signerVerifierDetail.nodeIdTag.nodeId
      val plan = for {
        newRow <- identityTable.insert(
          Map(
            identityCol -> norm(identity),
            balanceCol -> balance,
            allowUntilHeightCol -> licensedUntil,
            createdCol -> new Date().getTime
          )
        )

        lnkDetailsMap = Map(
          identityLnkCol -> newRow.id,
          createdCol -> new Date().getTime
        )
        detailsMaps = signerVerifierDetailsToRowInsertMap(signerVerifierDetail) ++ lnkDetailsMap

        _ <- keyTable.insert(detailsMaps)


        _ <- paywallTable.insert(
          Map(
            identityLnkCol -> newRow.id,
            categoryCol -> defaultPaywallCategory,
            amountCol -> defaultPaywallAmount
          )
        )

        _ <- paywallTable.insert(
          Map(
            identityLnkCol -> newRow.id,
            categoryCol -> defaultKnownContactPaywallCategory,
            amountCol -> defaultKnownContactPaywallCategoryAmount
          )
        )

        _ <- FutureTx.sequence {
          ownedIdentityAttributes.map(attr => ownedAttributeTable.insert(
            Map(
              categoryCol -> attr.category.toByte,
              identityLnkCol -> newRow.id,
              attrValueCol -> attr.toBytes,
            )
          ))
        }

      } yield ()



      import db.asyncRunContext.ec

      plan.recover {
        case e: SQLIntegrityConstraintViolationException =>
          throw new IllegalArgumentException(s"Identifier $identity already taken (${e.getMessage})")

        case e =>
          throw new IllegalArgumentException(
            s"Failed to claim identity $identity",
            e
          )

      }
    }

    override def linkRescuer(identity: String, rescuer: String): FutureTx[Unit] = {

      for {
        accOpt <- accountOptFTx(rescuer)
        idRescuers <- rescuersFTx(identity)
        _ = require(
                 accOpt.isDefined, s"Rescuer $rescuer does not exist!")
        _ = require(idRescuers.size < maxRescuersPerIdentity,
          s"No more than $maxRescuersPerIdentity rescuers allowed per identity."
        )
        _ <- if(!idRescuers.contains(rescuer)) {
          toIdOpt(identity) flatMap {
            case None => throw new IllegalArgumentException(s"No such identity $identity")
            case Some(rowId) =>
              recoveryTable.persist(
                Map(
                  identityLnkCol -> rowId,
                  identityCol -> rescuer,
                  domainCol -> "",
                  createdCol -> new Date().getTime
                )
              )
          }
        } else FutureTx.unit(())
      } yield ()

    }

    override def unLinkRescuer(identity: UniqueNodeIdentifier, rescuer: UniqueNodeIdentifier): FutureTx[Unit] = {

      (for {
        rescuerExists <- exists(rescuer)
        _ = require(
          rescuerExists,
                 s"Rescuer $rescuer does not exist!"
                 )
        rescuerExists <- rescuersFTx(identity)
        _ = require(
          rescuerExists.contains(rescuer),
          s"$rescuer is not in the rescue list of $identity"
        )
        id <- identityToId(identity)
        _ <- recoveryTable
          .delete(
            where(
              identityLnkCol -> id,
              identityCol -> rescuer
            )
          )
      } yield ())

    }

    override def addMessageStore(identity: UniqueNodeIdentifier, store: UniqueNodeIdentifier): FutureTx[MessageStoreProviders] = {
      addMessageStoreImpl(identity, store)
    }

    def addMessageStoreImpl(identity: UniqueNodeIdentifier, store: UniqueNodeIdentifier): FutureTx[MessageStoreProviders] = {
      require(
        norm(identity) != norm(store),
        "Cannot add yourself as an alternative store."
      )
      for {
        flag <- exists(store)
        _ = require(flag, s"Alternative message storer $store does not exist!")
        tmp <- messageStoresFTx(identity)
        _ = require(
          tmp.size < maxMessageStoresPerIdentity,
          s"No more than $maxMessageStoresPerIdentity alternative stores allowed per identity."
        )
        rowId <- identityToId(identity)
        _ <- messageStoreTable.persist(
          Map(
            identityLnkCol -> rowId,
            storeNodeIdCol -> norm(store),
            createdCol -> new Date().getTime
          )
        )
        messageStores <- messageStoresFTx(identity)

      } yield MessageStoreProviders(
        identity,
        messageStores
      )
    }

    private def messageStoresFTx(identity: UniqueNodeIdentifier): FutureTx[Set[UniqueNodeIdentifier]] =
      (messageStoreView filter where(identityCol -> norm(identity)))
        .map(_.map(_.string(storeNodeIdCol)).toSet)


    override def messageStores(identity: UniqueNodeIdentifier): Set[UniqueNodeIdentifier] =
      messageStoresFTx(identity).dbRunSyncGet

    def removeMessageStore(identity: UniqueNodeIdentifier, store: UniqueNodeIdentifier): FutureTx[MessageStoreProviders] =
      removeMessageStoreImpl(identity, store)

    private def removeMessageStoreImpl(identity: UniqueNodeIdentifier, store: UniqueNodeIdentifier): FutureTx[MessageStoreProviders] =
      for {
        rowId <- identityToId(identity)
        _ <- messageStoreTable.delete(
            where(
              identityLnkCol -> rowId,
              storeNodeIdCol -> norm(store),
            )
          )
        stores <- messageStoresFTx(identity)
        } yield MessageStoreProviders(
          identity,
        stores
        )


    def count: Long = identityTable.count.dbRunSyncGet

    def count(startsWithOpt: Option[String]): Long = list(startsWithOpt).size

    def createSnapshotTableName(name: String, tag: SimpleTag): String = s"${name}_$tag"

    private def importTable(name: String, tag: SimpleTag): FutureTx[Unit] = {
      val tableToImport = createSnapshotTableName(
        name,
        tag
      )
      replaceTable(
        name,
        tableToImport
      )
    }

    override def importState(height: Long): FutureTx[Unit] = {
      //This line checks for the existance of the saved state before
      // any changes are made, it will fail early preventing further changes
      // a 'tx' does no seem to effect these changes.
      for {
        _ <- db.table(
          createSnapshotTableName(
            identityTableName,
            height
          )
        )
          .count
        _ <- db.executeSqls(dropPKConstraints() ++ dropFKConstraints() :+ dropMessageViewSql :+ dropIdentityTblUnique)
        _ <- importTable(
          identityTableName,
          height
        )
        _ <- importTable(
          keyTableName,
          height
        )
        _ <- importTable(
          recoveryIdentitiesTableName,
          height
        )
        _ <- importTable(
          messageStoreTableName,
          height
        )
        _ <- importTable(
          paywallTableName,
          height
        )
        _ <- importTable(
          attributeTableName,
          height
        )
        _ <- importTable(
          ownedAttributeTableName,
          height
        )
        _ <- db.executeSqls((addIdentityTblUnique +: addPKConstraints()) ++ addFKConstraints() :+ createMessageStoreView)
      } yield ()
    }

    private def makeDigest(query: Query): Try[Option[Digest32]] = Try {
      PagesToStream(pageGenerator(
        query,
        _.toBytes(query.columnsMetaInfo),
        Seq.empty
      ), 25)
    }.flatMap(stream =>
      if (stream.isEmpty)
        Success(None)
      else
        HashUtils(stream).map(Some(_)))

    override def createCheckpoint(height: Long): FutureTx[Try[Digest32]] = {
      val plan = for {
        // The copy table will not copy the PK
        _ <- db.executeSqls(dropPKConstraints() ++ dropFKConstraints() :+ dropMessageViewSql)
        _ <- copyTable(
          identityTableName,
          createSnapshotTableName(
            identityTableName,
            height
          )
        )
        _ <- copyTable(
          keyTableName,
          createSnapshotTableName(
            keyTableName,
            height
          )
        )
        _ <- copyTable(
          recoveryIdentitiesTableName,
          createSnapshotTableName(
            recoveryIdentitiesTableName,
            height
          )
        )
        _ <- copyTable(
          messageStoreTableName,
          createSnapshotTableName(
            messageStoreTableName,
            height
          )
        )
        _ <- copyTable(
          paywallTableName,
          createSnapshotTableName(
            paywallTableName,
            height
          )
        )
        _ <- copyTable(
          attributeTableName,
          createSnapshotTableName(
            attributeTableName,
            height
          )
        )
        _ <- copyTable(
          ownedAttributeTableName,
          createSnapshotTableName(
            ownedAttributeTableName,
            height
          )
        )
        _ <- db.executeSqls(addPKConstraints() ++ addFKConstraints() :+ createMessageStoreView)
      } yield ()


      import cats.implicits._
      plan.map(_ =>
        HashUtils(
          Seq(
            makeDigest(db.select(selectKeysSql)),
            makeDigest(db.select(selectRecoverySql)),
            makeDigest(db.select(selectMessageStoreSql)),
            makeDigest(db.select(selectPaywallSql)),
            makeDigest(db.select(selectAttributeSql)),
            makeDigest(db.select(selectOwnedAttributeSql))
          ).flatMap(_.sequence)
        )
      )
    }

    def deleteCheckpoint(height: Long): FutureTx[Unit] = FutureTx.sequence {
      Seq(
        createSnapshotTableName(
          identityTableName,
          height
        ),
        createSnapshotTableName(
          keyTableName,
          height
        ),
        createSnapshotTableName(
          recoveryIdentitiesTableName,
          height
        ),
        createSnapshotTableName(
          messageStoreTableName,
          height
        ),
        createSnapshotTableName(
          paywallTableName,
          height
        ),
        createSnapshotTableName(
          attributeTableName,
          height
        ),
        createSnapshotTableName(
          ownedAttributeTableName,
          height
        )
      ).map { name =>
        db.executeSql(s"DROP TABLE $name;")
      }

    }.map(_ => ())


    private def paywallsImpl(identity: UniqueNodeIdentifier): FutureTx[Seq[Paywall]] = {
      val normIdentity = norm(identity)
      usingIdentity(normIdentity) { rowId =>
        paywallTable
          .filter(
            where(identityLnkCol -> rowId)
          )
          .map { rows =>
            rows.map { row =>
              Paywall(
                normIdentity,
                row.string(categoryCol),
                row.longOpt(amountCol)
              )
            }
          }
      }
    }

    override def paywalls(identity: UniqueNodeIdentifier): Seq[Paywall] = paywallsImpl(identity).dbRunSyncGet

    private def rowToAttribute(row: Row): LedgerAttribute = {
      val category = row.byte(categoryCol)
      row
        .arrayByteOpt(attrValueCol)
        .getOrElse(Array(category))
    }.toLedgerAttribute

    override def removeAttribute(owningIdentity: UniqueNodeIdentifier, attrToRemove: AttributeCategory): FutureTx[Option[DeletedAttribute]] =
      removeAttribute(attributeTable, owningIdentity, attrToRemove)

    override def removeOwnedAttribute(owningIdentity: UniqueNodeIdentifier, attrToRemove: AttributeCategory): FutureTx[Option[DeletedAttribute]] =
      removeAttribute(ownedAttributeTable, owningIdentity, attrToRemove)

    private def removeAttribute(table: Table,
                                owningIdentity: UniqueNodeIdentifier,
                                attrToRemove: AttributeCategory
                               ): FutureTx[Option[DeletedAttribute]] = usingIdentity(owningIdentity) { rowId =>
      table.delete(
        where(
          identityLnkCol -> rowId,
          categoryCol -> attrToRemove.toByte
        )
      ).map { numDeleted =>
        require(numDeleted == 0 || numDeleted == 1, s"Num deleted $numDeleted must be 0 or 1 here")
        Option.when(numDeleted != 0)(DeletedAttribute(owningIdentity, attrToRemove))
      }
    }

    override def setAttribute(owningIdentity: UniqueNodeIdentifier, attrToUpdate: LedgerAttribute): FutureTx[ChangedAttribute] =
      setAttribute(attributeTable, owningIdentity, attrToUpdate)

    override def setOwnedAttribute(owningIdentity: UniqueNodeIdentifier, attrToUpdate: LedgerAttribute): FutureTx[ChangedAttribute] =
      setAttribute(ownedAttributeTable, owningIdentity, attrToUpdate)

    private def setAttribute(
                              table: Table,
                              owningIdentity: UniqueNodeIdentifier,
                              attrToUpdate: LedgerAttribute
                            ): FutureTx[ChangedAttribute] = {
      usingIdentity(owningIdentity) { rowId =>
        val lookup = where(
          categoryCol -> attrToUpdate.category.toByte,
          identityLnkCol -> rowId
        )
        table.find(lookup).flatMap {
          case None =>
            table.insert(
              Map(
                categoryCol -> attrToUpdate.category.toByte,
                identityLnkCol -> rowId,
                attrValueCol -> attrToUpdate.toBytes,
              )
            )
          case Some(_) =>
            table.update(
              Map(
                attrValueCol -> attrToUpdate.toBytes
              ),
              lookup
            )
        }.flatMap { _ =>
          table.getRow(lookup).map { rowOpt =>
              ChangedAttribute(
                owningIdentity,
                rowToAttribute(rowOpt.getOrElse(exception("Row *must* exist after insert?")))
              )
          }
        }
      }
    }

    private def listAttributes(table: Table, owningIdentity: UniqueNodeIdentifier): Seq[LedgerAttribute] = {
      usingIdentity(norm(owningIdentity)) { rowId =>
        table.filter(
          where(
            identityLnkCol -> rowId
          )
        ) map (_.map(rowToAttribute))
      }.dbRunSyncGet
    }

    override def listAttributes(identity: UniqueNodeIdentifier): Seq[LedgerAttribute] = listAttributes(attributeTable, identity)

    override def listOwnedAttributes(identity: UniqueNodeIdentifier): Seq[LedgerAttribute] = listAttributes(ownedAttributeTable, identity)

    private def attributeFTx(
                              tableName: String,
                              identity: UniqueNodeIdentifier,
                              category: AttributeCategory): FutureTx[Option[LedgerAttribute]] = {
      db.select(
        s"""SELECT a.$identityCol, b.$categoryCol, b.$attrValueCol
           |FROM $identityTableName a
           |JOIN $tableName b
           |ON a.$idCol = b.$identityLnkCol
           |""".stripMargin
      ).find(
        where(
          s"a.$identityCol" -> identity,
          s"b.$categoryCol" -> category.toByte
        )
      ).map(_.map(rowToAttribute))
    }

    override def attributeFTx(
                               identity: UniqueNodeIdentifier,
                               category: AttributeCategory): FutureTx[Option[LedgerAttribute]] =
      attributeFTx(attributeTableName, identity, category)

    override def attribute(identity: UniqueNodeIdentifier, category: AttributeCategory): Option[LedgerAttribute] =
      attributeFTx(identity: UniqueNodeIdentifier, category: AttributeCategory).dbRunSyncGet

    override def ownedAttribute(identity: UniqueNodeIdentifier, category: AttributeCategory): Option[LedgerAttribute] = {
      attributeFTx(ownedAttributeTableName, identity, category).dbRunSyncGet
    }

    override def setPaywall(identity: UniqueNodeIdentifier, category: String, amount: Long): FutureTx[Paywall] = {

      val normCategory = norm(category)
      require(
        amount >= 0,
        "Amount cannot be negative"
      )
      for {
        rowId <- identityToId(identity)
        newValues = Map(
          identityLnkCol -> rowId,
          categoryCol -> normCategory,
          amountCol -> amount
        )
        foundOpt <- paywallTable.find(
          where(
            identityLnkCol -> rowId,
            categoryCol -> normCategory
          )
        )
        updatedRow <- foundOpt match {
          case Some(row) =>
            paywallTable.updateRow(newValues + (idCol -> row.id))
          case None =>
            for {
              paywalls <- paywallsImpl(identity)
              _ = require(
                paywalls.size < maxPaywalls,
                s"Cannot add more than $maxPaywalls paywalls"
              )
              newRow <- paywallTable.insert(newValues)
            } yield newRow
        }

      } yield Paywall(
        identity,
        updatedRow.string(categoryCol),
        updatedRow.longOpt(amountCol)
      )

    }

    private def removePaywallImpl(identity: UniqueNodeIdentifier, category: String): FutureTx[Option[Paywall]] = {
      val normCategory = norm(category)
      for {
        rowId <- identityToId(identity)
        numDeleted <- paywallTable.delete(
          where(
            identityLnkCol -> rowId,
            categoryCol -> normCategory
          )
        )
      } yield
        if (numDeleted == 1) {
          Some(
            Paywall(
              identity,
              normCategory,
              None
            )
          )
        } else None
    }

    override def removePaywall(identity: UniqueNodeIdentifier, category: String): FutureTx[Option[Paywall]] = {
      require(
        norm(category) != defaultPaywallCategory,
        s"Cannot delete the default paywall ($defaultPaywallCategory)"
      )
      removePaywallImpl(
        identity,
        category
      )
    }

    override def roleOwnedAttribute(identity: UniqueNodeIdentifier): IdentityRoleAttribute = {
      ownedAttribute(identity, SystemAttributeCategory.IdentityRole) match {
        case Some(value: IdentityRoleAttribute) => value
        case _ => exception(s"No IdentityRoleAttribute found for $identity ?")
      }
    }

    override def toVerifier(signerVerifierDetails: SignerVerifierDetails): NodeVerifier =
      verifierFactory(signerVerifierDetails.nodeIdTag, signerVerifierDetails.typedPublicKey)

  }
}
