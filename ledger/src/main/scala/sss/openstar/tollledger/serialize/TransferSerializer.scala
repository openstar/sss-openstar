package sss.openstar.tollledger.serialize

import sss.ancillary.Serialize._
import sss.openstar.tollledger.TollLedger.{Transfer, TransferCode}

object TransferSerializer extends Serializer[Transfer]{
  override def toBytes(t: Transfer): Array[Byte] = {
    ByteSerializer(TransferCode) ++
    LongSerializer(t.amount) ++
      LongSerializer(t.uniqueMessage) ++
      SequenceSerializer(t.recipients.map(StringSerializer)).toBytes
  }

  override def fromBytes(b: Array[Byte]): Transfer = {
    val (code , a, u, recips) = b.extract(
      ByteDeSerialize,
      LongDeSerialize,
      LongDeSerialize,
      SequenceDeSerialize(_.extract(StringDeSerialize))
    )

    require(code == TransferCode, s"Wrong code for Transfer? Was $code, should be $TransferCode")

    new Transfer(recips, a) {
      override private[tollledger] val uniqueMessage = u
    }
  }
}
