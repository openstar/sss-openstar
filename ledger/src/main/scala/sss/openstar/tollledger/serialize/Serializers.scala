package sss.openstar.tollledger.serialize

import sss.ancillary.Serialize.Serializer
import sss.openstar.tollledger.TollLedger.Transfer


trait Serializers {
  implicit val TransferS: Serializer[Transfer] = TransferSerializer
}

object Serializers extends Serializers
