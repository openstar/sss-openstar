package sss.openstar.tollledger

import scorex.crypto.signatures.Signature
import sss.ancillary.Logging
import sss.ancillary.Serialize.SerializerOps.{Deserialize, Serialize}
import sss.db.ops.DbOps.DbRunOps
import sss.db.{Db, FutureTx}
import sss.openstar.account.NodeIdentity
import sss.openstar.account.Ops.{ExtractFromSigs, MakeTxSigArys}
import sss.openstar.common.block
import sss.openstar.hash.SecureCryptographicHash
import sss.openstar.identityledger.IdentityServiceQuery
import sss.openstar.identityledger.Ops.ToVerifier
import sss.openstar.ledger.LedgerOps.TryLedgerResultToTryApplied
import sss.openstar.ledger.{LedgerId, LedgerItem, LedgerResult, LedgerResultFTx, LedgerTxApplyError, SignedTxEntry, ToTxEntryWithSignatures, TxApplied, TxId, TxSeqApplied}
import sss.openstar.tollledger.TollLedger.Ops._
import sss.openstar.tollledger.TollLedger._
import sss.openstar.tollledger.TollService.PayTollResultInfo.Coinbase
import sss.openstar.tollledger.TollService.{TollServiceCredit, TollServiceResult}
import sss.openstar.tollledger.serialize.Serializers._
import sss.openstar.{UniqueNodeIdentifier, ledger}

import java.security.SecureRandom
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

object TollLedger {

  private[tollledger] val TransferCode: Byte = 1.toByte

  def sign(tollTx: TollLedgerTx, node: NodeIdentity)(implicit ec: ExecutionContext): Future[Seq[Array[Byte]]] = {
    node.defaultNodeVerifier.signer.sign(tollTx.txId).map { sig =>
      Seq(node.idBytes, node.tagBytes, sig)
    }
  }

  def toSignedTx(tollTx: TollLedgerTx, node: NodeIdentity)(implicit ec: ExecutionContext): Future[SignedTxEntry] = {
    sign(tollTx, node) map { sigAry =>
      SignedTxEntry(tollTx.toBytes, Seq(sigAry).txSig)
    }
  }

  def throwTollLedgerLedgerException(msg: String) = throw new TollLedgerException(msg)

  class TollLedgerException(msg: String) extends Exception(msg)


  abstract class TollLedgerTx {

    private[tollledger] val uniqueMessage = SecureRandom.getInstanceStrong.nextLong()

    lazy val txId: Array[Byte] = SecureCryptographicHash.hash(this.toBytes)

  }
  case class Transfer(recipients: Seq[UniqueNodeIdentifier],
                      amount: Long,
                               ) extends TollLedgerTx

  case class TollLedgerTxApplied(txId: TxId, result: TollServiceResult)(implicit val ledgerId: LedgerId) extends TxApplied
  case class CoinbaseTollLedgerTxApplied(txId: TxId,
                                         recps: Seq[UniqueNodeIdentifier],
                                         amount: Long)(implicit val ledgerId: LedgerId) extends TxApplied


  object Ops {

    implicit class TollLedgerFromBytes(val bytes: Array[Byte]) extends AnyVal {

      def toTollLedgerTx: TollLedgerTx = bytes.head match {
        case TransferCode => bytes.deserialize[Transfer]
        case x => throwTollLedgerLedgerException(s"Code $x is not a known tx code.")
      }
    }

    implicit class TollLedgerTxToBytes(private val tx: TollLedgerTx) extends AnyVal {
      def toBytes: Array[Byte] = tx match {
        case t: Transfer => t.serialize
      }
    }
  }
}

class TollLedger(tollService: TollService,
                 identityServiceQuery: IdentityServiceQuery,
                 quorumCandidates: () => Set[UniqueNodeIdentifier],
                 tollAmount: Long,
                 blockRewardPerQuorumMember: Long,
                 val ledgerId: LedgerId)(implicit db: Db) extends sss.openstar.ledger.TollLedger with Logging {

  implicit val ledgerIdImpl: LedgerId = ledgerId

  override def apply(
                      ledgerItem: ledger.LedgerItem,
                      blockId: block.BlockId,
                      txSeqApplied: TxSeqApplied): LedgerResult =
    applyFTx(ledgerItem, blockId, txSeqApplied).dbRunSync.unwrap


  override def applyFTx(
                         ledgerItem: ledger.LedgerItem,
                         blockId: block.BlockId,
                         txSeqApplied: TxSeqApplied): LedgerResultFTx = FutureTx.unit {

    ledgerItem.txEntryBytes.toSignedTxEntry
  } flatMap {

    //Charge toll to all Txs that are Not bound for this ledger
    case ste@SignedTxEntry(_, signatures) if(ledgerItem.ledgerId != ledgerId) =>
      val (who, tag, sig) = signatures.toTryTruple.get
      for {
        _ <- verifyAccountAndSignature(who, tag, ste.txId,  sig)
        resService <-  tollService.payToll(who, tollAmount, blockId.blockHeight)

        res =
          if(resService.success) Right(txSeqApplied.withTxResult(TollLedgerTxApplied(ste.txId, resService)))
          else Left(LedgerTxApplyError(resService.info.toString, s"Failed to pay toll $tollAmount!"))
      } yield res

    //Coinbase
    case ste@SignedTxEntry(txEntryBytes, _) if blockId.txIndex == 1 => //this is coinbase
      txEntryBytes.toTollLedgerTx match {
        case Transfer(recipients, amount) =>

          coinbaseCredit( amount, recipients) map { res =>
            if (res.success) Right(txSeqApplied.withTxResult(CoinbaseTollLedgerTxApplied(ste.txId, recipients, amount)))
            else Left(LedgerTxApplyError(res.info.toString, s"Failed to process coinbase!"))
          }

        case x => throwTollLedgerLedgerException(s"Not handling this ${x.toString}")
      }

    //Transfer between users
    case ste@SignedTxEntry(txEntryBytes, signatures) =>
      txEntryBytes.toTollLedgerTx match {
        case Transfer(recipients, amount) =>
          val (who, tag, sig) = signatures.toTryTruple.get
          for {
            _ <- verifyAccountAndSignature(who, tag, ste.txId, sig)
            _ = require(amount > 0, s"Cannot transfer amount < 0 ${amount} (Sender: $who)")
            totalToTransfer = amount * recipients.size
            resToll <- tollService.payToll(who, tollAmount, blockId.blockHeight)
            _ = require(totalToTransfer <= resToll.newBalance, s"You do not have sufficient funds to pay the toll ${tollAmount} (Sender: $who, ${resToll.newBalance})")
            res <- tollService.transferToMany(who, recipients, amount)

          } yield
            if (res.success) Right(txSeqApplied.withTxResult(TxSeqApplied(ste.txId, Seq(TollLedgerTxApplied(ste.txId, res)))))
            else Left(LedgerTxApplyError(res.info.toString, s"Failed to pay toll $tollAmount!"))

        case x => throwTollLedgerLedgerException(s"Not handling this ${x.toString}")
      }
  }

  private def verifyAccountAndSignature(identity: String, tag: String, txId: TxId, sig: Signature): FutureTx[Unit] = for {
    accOpt <- identityServiceQuery.accountOptFTx(identity, tag)
    _ = require(accOpt.isDefined, s"Could not find an account for identity/tag pair ${identity}/$tag provided in signature.")
    _ = require(
      accOpt.get.toVerifier(identityServiceQuery)
        .verifier
        .verify(txId, sig), "The signature does not match the txId")

  } yield ()

  private def coinbaseCredit(amount: Long, recps: Seq[UniqueNodeIdentifier]): FutureTx[TollServiceResult] = {

    val tollCredit = recps.foldLeft(FutureTx.unit(TollServiceCredit(None, amount))) {
      case (acc, e) => acc.flatMap { res =>
          tollService.credit(e,res)
      }
    }

    tollCredit.map(r => TollServiceResult(true, Coinbase, 0, Some(r)))

  }

  override def coinbase(forBlockHeight: Long, ledgerId: Byte): Option[ledger.LedgerItem] = {
    val t = Transfer(quorumCandidates().toSeq, blockRewardPerQuorumMember)
    val stx = SignedTxEntry(t.toBytes, Seq(Seq(Array.emptyByteArray, Array.emptyByteArray, Array.emptyByteArray)).txSig)
    Some(LedgerItem(ledgerId, stx.txId, stx.toBytes))

  }

  override def validate(
                        txLedgerId: LedgerId,
                         signedTxEntry: SignedTxEntry,
                         index: Long): FutureTx[Unit] = {

    if(ledgerId == txLedgerId && index == 1) FutureTx.unit(())
    else {
      val sigs = signedTxEntry.signatures

      sigs.toTryTruple match {
        case Failure(exception) =>
          FutureTx.failed(exception)
        case Success((id, tag, sig)) =>
          verifyAccountAndSignature(id, tag, signedTxEntry.txId, sig)
      }
    }
  }
}
