package sss.openstar.tollledger

import sss.db.ops.DbOps.DbRunOps
import sss.db.{Db, FutureTx, Row, Rows, Table, where}
import sss.openstar.UniqueNodeIdentifier
import sss.openstar.schemamigration.SqlSchemaNames.ColumnNames.{allowUntilHeightCol, amountCol, balanceCol, disallowBeforeHeightCol, identityCol}
import sss.openstar.schemamigration.SqlSchemaNames.TableNames
import sss.openstar.tollledger.TollService.PayTollResultInfo.{BalanceTooLow, CurrentlyBanned, PaidWithBalance, PaidWithLicense, PayTollResultInfo}
import sss.openstar.tollledger.TollService.{TollBalance, TollServiceCredit, TollServiceQuery, TollServiceResult}

import scala.concurrent.Future

object TollService {

  trait TollServiceQuery {

    def tollBalance(identity: UniqueNodeIdentifier): Future[TollBalance]
    def tollBalanceFTx(identity: UniqueNodeIdentifier): FutureTx[TollBalance]
  }
  case class TollServiceCredit(
                                from: Option[UniqueNodeIdentifier],
                                amount: Long,
                                recipients: Seq[UniqueNodeIdentifier] = Seq.empty
                              )
  case class TollServiceResult(success: Boolean, info: PayTollResultInfo, newBalance: Long, recipientsOpt: Option[TollServiceCredit])
  case class TollBalance(
                          bannedUtilHeight: Option[Long],
                          licensedUntilHeight: Option[Long],
                          balance: Long,
                          private[tollledger] val row: Row) {
    def isBannedAtHeight(height: Long): Boolean = bannedUtilHeight.fold(false)(_ < height)

    def isLicensedAtHeight(height: Long): Boolean = licensedUntilHeight.fold(false)(_ >= height)

    def canDebit(amount: Long): Boolean = balance >= amount
  }

  object PayTollResultInfo extends Enumeration {
    type PayTollResultInfo = Value
    val PaidWithLicense = Value("PaidWithLicense")
    val PaidWithBalance = Value("PaidWithBalance")
    val CurrentlyBanned = Value("CurrentlyBanned")
    val BalanceTooLow = Value("BalanceTooLow")
    val Coinbase = Value("Coinbase")
  }
}
class TollService(
                  validateGoodIdentifier: String => String)(implicit db:Db) extends TollServiceQuery {

  private lazy val identityTable: Table = db.table(TableNames.identityTableName)

  private def users(identities: Seq[UniqueNodeIdentifier]): FutureTx[Seq[Row]] = {
    val safeIdentities: Seq[UniqueNodeIdentifier] = identities.map(validateGoodIdentifier)

    identityTable
      .filter(
        where(identityCol) in safeIdentities.toSet
      ).map { rows: Rows =>

      def noExiste: Seq[UniqueNodeIdentifier] = {
        val foundNames = rows.map(_.string(identityCol))
        safeIdentities.filterNot(foundNames.contains(_))
      }

      require(rows.size == safeIdentities.size, s"Some users do not exist s${noExiste.mkString}")
      rows
    }
  }

  private def user(identity: UniqueNodeIdentifier): FutureTx[Row] =
    users(Seq(identity)).map(_.head)

  override def tollBalanceFTx(identity: UniqueNodeIdentifier): FutureTx[TollBalance] = {
    user(identity) map tollBalance
  }

  override def tollBalance(identity: UniqueNodeIdentifier): Future[TollBalance] =
    tollBalanceFTx(identity).dbRun


  private def tollBalance(row: Row): TollBalance = {
    val bannedUntil = row.longOpt(disallowBeforeHeightCol)
    val allowedUntil = row.longOpt(allowUntilHeightCol)
    val balance = row.long(balanceCol)
    TollBalance(bannedUntil, allowedUntil, balance, row)
  }

  private def ftx[R](res: R): FutureTx[R] = FutureTx.unit(res)

  def payToll(identity: UniqueNodeIdentifier,
               amount: Long,
               height: Long): FutureTx[TollServiceResult] = {

    require(amount >= 0, "Cannot pay a negative toll")

    for {
      senderBalance <- tollBalanceFTx(identity)
      result <- if (senderBalance.isBannedAtHeight(height)) {
        ftx(TollServiceResult(success = false, CurrentlyBanned, senderBalance.balance, None))
      } else if (senderBalance.isLicensedAtHeight(height)) {
        ftx(TollServiceResult(success = true, PaidWithLicense, senderBalance.balance, None))
      } else if (!senderBalance.canDebit(amount)) {
        ftx(TollServiceResult(success = false, BalanceTooLow, senderBalance.balance, None))
      } else debit(senderBalance.row, amount)
    } yield result
  }

  private def debit(row:Row, amount:Long): FutureTx[TollServiceResult] = {

    val balance = row.long(balanceCol)
    val newBalance = balance - amount
    require(newBalance >= 0, s"$balance too low to debit $amount")

    for {
      res <- identityTable
        .updateRow(
          row.asMap + (balanceCol -> newBalance)
        ) map (r => TollServiceResult(success = true, PaidWithBalance, r.long(balanceCol), None))
    } yield res
  }

  private def credit(row: Row, acc: TollServiceCredit): FutureTx[TollServiceCredit] = {

    val balance = row.long(balanceCol)
    val recipient = row.string(identityCol)
    val newBalanceGood = (BigDecimal(balance) + BigDecimal(acc.amount)) <= BigDecimal(Long.MaxValue)
    require(newBalanceGood, s"New balance cannot exceed MaxLong value")

    for {
      _ <- identityTable
        .updateRow(
          row.asMap + (balanceCol -> (balance + acc.amount))
        )
    } yield acc.copy(recipients = acc.recipients :+ recipient)
  }

  //Used for coinbase Tx
  def credit(recipient: UniqueNodeIdentifier, acc: TollServiceCredit): FutureTx[TollServiceCredit] = {
    for {
      u <- user(recipient)
      r <- credit(u, acc)
    } yield r
  }

  def transferToMany(identity: UniqueNodeIdentifier,
               recipients: Seq[UniqueNodeIdentifier],
               amount: Long): FutureTx[TollServiceResult] = {
    require(amount >= 0, "Cannot transfer a negative amount")
    require(recipients.nonEmpty, "No one to transfer to?")
    for {
      sender <- user(identity)
      receivers <- users(recipients)
      total = amount * receivers.size
      res <- debit(sender, total)
      acc = TollServiceCredit(Some(identity), amount)
      resCredits <- credit(receivers, acc)

    } yield res.copy(recipientsOpt = Some(resCredits))
  }

  private def credit(recipients: Seq[Row], acc: TollService.TollServiceCredit): FutureTx[TollServiceCredit] = {
    recipients.foldLeft(FutureTx.unit(acc)) {
      case (acc ,e) => acc.flatMap(credit(e, _))
    }
  }

  def transfer(identity: UniqueNodeIdentifier,
               recipient: UniqueNodeIdentifier,
               amount: Long): FutureTx[TollServiceResult] =
    transferToMany(identity, Seq(recipient), amount)

}
