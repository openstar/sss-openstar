package sss.openstar

import sss.ancillary.Serialize.SerializerOps.{Deserialize, Serialize}
import sss.openstar.hash.{Digest32, SecureCryptographicHash}
import sss.openstar.systemledger.serialize.Serializers._

import java.security.SecureRandom

package object systemledger {


  object SystemLedgerErrorCode extends Enumeration {
    type SystemLedgerErrorCode = Value
    val NoSignatures = Value("NoSignatures")
    val ReplayAttackPrevention = Value("ReplayAttackPrevention")
    val BadlyFormedSignature = Value("BadlyFormedSignature")
    val NotOwner = Value("NotOwner")
    val BadSignature = Value("BadSignature")
    val NoSuchUser = Value("NoSuchUser")
  }

  sealed abstract class SystemLedgerTx(val replayPreventionBlockHeight: Long) {
    private[systemledger] val uniqueMessage = SecureRandom.getInstanceStrong.nextLong()
    lazy val txId: Array[Byte] = SecureCryptographicHash.hash(this.toBytes)
  }

  protected[systemledger] val UpdateNotificationCode = 1.toByte
  protected[systemledger] val PreparedToUpdateCode = 2.toByte
  protected[systemledger] val CancelUpdateCode = 3.toByte

  case class CancelUpdate(override val replayPreventionBlockHeight: Long,
                                uniqueIdentifier: String) extends SystemLedgerTx(replayPreventionBlockHeight)

  case class UpdateNotification(override val replayPreventionBlockHeight: Long,
                                blockHeight: Long,
                                uniqueIdentifier: String,
                                url: String,
                                checksum: Digest32) extends SystemLedgerTx(replayPreventionBlockHeight)

  case class PreparedToUpdate(override val replayPreventionBlockHeight: Long,
                              proposalUniqueIdentifier: String) extends SystemLedgerTx(replayPreventionBlockHeight)

  implicit class SystemLedgerTxFromBytes(private val bytes: Array[Byte]) extends AnyVal {
    def toSystemLedgerTx: SystemLedgerTx = bytes.head match {
      case UpdateNotificationCode => bytes.deserialize[UpdateNotification]
      case PreparedToUpdateCode => bytes.deserialize[PreparedToUpdate]
      case CancelUpdateCode => bytes.deserialize[CancelUpdate]
    }
  }

  implicit class SystemLedgerTxToBytes(private val sysLedgerMsg: SystemLedgerTx) extends AnyVal {
    def toBytes: Array[Byte] = sysLedgerMsg match {
      case msg: UpdateNotification => msg.serialize
      case msg: PreparedToUpdate => msg.serialize
      case msg: CancelUpdate => msg.serialize
    }
  }
}
