package sss.openstar.systemledger.serialize

import sss.ancillary.Serialize.{ByteDeSerialize, ByteSerializer, LongDeSerialize, LongSerializer, SerializeHelper, Serializer, StringDeSerialize, StringSerializer}
import sss.openstar.systemledger.{PreparedToUpdate, PreparedToUpdateCode}

object PreparedToUpdateSerializer extends Serializer[PreparedToUpdate]{
  override def toBytes(u: PreparedToUpdate): Array[Byte] = {
    ByteSerializer(PreparedToUpdateCode) ++
    StringSerializer(u.proposalUniqueIdentifier) ++
      LongSerializer(u.replayPreventionBlockHeight) ++
      LongSerializer(u.uniqueMessage).toBytes
  }

  override def fromBytes(b: Array[Byte]): PreparedToUpdate = {
    val (code, uniqueId, replayHeight, uniqueMsg) =
      b.extract(
        ByteDeSerialize,
        StringDeSerialize,
        LongDeSerialize,
        LongDeSerialize,
        )

    require(code == PreparedToUpdateCode, s"Expected UpdateNotificationCode $PreparedToUpdateCode, got $code")

    new PreparedToUpdate(
      replayPreventionBlockHeight = replayHeight,
      proposalUniqueIdentifier = uniqueId) {
      override private[systemledger] val uniqueMessage: Long = uniqueMsg
    }

  }
}
