package sss.openstar.systemledger.serialize

import sss.ancillary.Serialize.Serializer
import sss.openstar.systemledger.{CancelUpdate, PreparedToUpdate, UpdateNotification}

trait Serializers {
  implicit val UpdateNotificationS: Serializer[UpdateNotification] = UpdateNotificationSerializer
  implicit val PreparedToUpdateS: Serializer[PreparedToUpdate] = PreparedToUpdateSerializer
  implicit val cancelUpdateS: Serializer[CancelUpdate] = CancelUpdateSerializer
}

object Serializers extends Serializers

