package sss.openstar.systemledger.serialize

import sss.ancillary.Serialize.{ByteArrayDeSerialize, ByteArraySerializer, ByteDeSerialize, ByteSerializer, LongDeSerialize, LongSerializer, SerializeHelper, Serializer, StringDeSerialize, StringSerializer}
import sss.openstar.hash.Digest32
import sss.openstar.systemledger.{PreparedToUpdateCode, UpdateNotification, UpdateNotificationCode}

object UpdateNotificationSerializer extends Serializer[UpdateNotification]{
  override def toBytes(u: UpdateNotification): Array[Byte] = {
    ByteSerializer(UpdateNotificationCode) ++
    LongSerializer(u.blockHeight) ++
      LongSerializer(u.replayPreventionBlockHeight) ++
      LongSerializer(u.uniqueMessage) ++
      StringSerializer(u.uniqueIdentifier) ++
      StringSerializer(u.url) ++
      ByteArraySerializer(u.checksum.digest).toBytes
  }

  override def fromBytes(b: Array[Byte]): UpdateNotification = {
    val (code, height, replayHeight, uniqueMsg, uniqueId, updateUrl, digest) =
      b.extract(
        ByteDeSerialize,
        LongDeSerialize,
        LongDeSerialize,
        LongDeSerialize,
        StringDeSerialize,
        StringDeSerialize,
        ByteArrayDeSerialize)

    require(
      code == UpdateNotificationCode,
      s"Expected UpdateNotificationCode $UpdateNotificationCode, got $code!"
    )

    new UpdateNotification(
      blockHeight = height,
      replayPreventionBlockHeight = replayHeight,
      uniqueIdentifier = uniqueId,
      url = updateUrl, checksum = Digest32(digest)) {

      override private[systemledger] val uniqueMessage: Long = uniqueMsg
    }

  }
}
