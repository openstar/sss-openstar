package sss.openstar.systemledger.serialize

import sss.ancillary.Serialize.{ByteArrayDeSerialize, ByteArraySerializer, ByteDeSerialize, ByteSerializer, LongDeSerialize, LongSerializer, SerializeHelper, Serializer, StringDeSerialize, StringSerializer}
import sss.openstar.hash.Digest32
import sss.openstar.systemledger.{CancelUpdate, CancelUpdateCode, UpdateNotification, UpdateNotificationCode}

object CancelUpdateSerializer extends Serializer[CancelUpdate]{
  override def toBytes(u: CancelUpdate): Array[Byte] = {
    ByteSerializer(CancelUpdateCode) ++
      LongSerializer(u.replayPreventionBlockHeight) ++
      LongSerializer(u.uniqueMessage) ++
      StringSerializer(u.uniqueIdentifier).toBytes
  }

  override def fromBytes(b: Array[Byte]): CancelUpdate = {
    val (code, replayHeight, uniqueMsg, uniqueId) =
      b.extract(
        ByteDeSerialize,
        LongDeSerialize,
        LongDeSerialize,
        StringDeSerialize)

    require(
      code == CancelUpdateCode,
      s"Expected UpdateNotificationCode $CancelUpdateCode, got $code!"
    )

    new CancelUpdate(
      replayPreventionBlockHeight = replayHeight,
      uniqueIdentifier = uniqueId) {

      override private[systemledger] val uniqueMessage: Long = uniqueMsg
    }

  }
}
