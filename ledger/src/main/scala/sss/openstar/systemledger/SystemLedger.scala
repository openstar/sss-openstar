package sss.openstar.systemledger

import scorex.crypto.signatures.Signature
import sss.ancillary.Logging
import sss.db.{Db, FutureTx}
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.account.Ops.MakeTxSigArys
import sss.openstar.account.{NodeIdTag, NodeIdentity, NodeVerifier}
import sss.openstar.balanceledger.OracleBalanceLedger.OracleLedgerOwnerLookupFTx
import sss.openstar.common.block
import sss.openstar.ledger.LedgerOps.TryLedgerResultToTryApplied
import sss.openstar.{UniqueNodeIdentifier, hash, ledger}
import sss.openstar.ledger.{Ledger, LedgerId, LedgerResult, LedgerResultFTx, LedgerTxApplyError, SignedTxEntry, ToTxEntryWithSignatures, TxApplied, TxId, TxNotApplied, TxSeqApplied}
import sss.openstar.systemledger.SystemLedgerErrorCode.{BadSignature, BadlyFormedSignature, NoSignatures, NoSuchUser, NotOwner, ReplayAttackPrevention, SystemLedgerErrorCode}
import sss.openstar.systemledger.SystemLedger.{CancelUpdateTxApplied, GetVerifier, NewUpdateAckTxApplied, NewUpdateProposalTxApplied}
import sss.openstar.systemledger.SystemService.UpdateProposal

import java.nio.charset.StandardCharsets
import java.util
import scala.annotation.nowarn
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Try}

object SystemLedger {

  type GetVerifier = NodeIdTag => FutureTx[Option[NodeVerifier]]

  case class NewUpdateProposalTxApplied(txId: TxId, proposal: UpdateProposal)(implicit val ledgerId: LedgerId) extends TxApplied {

    override def hashCode(): Int = util.Arrays.hashCode(txId)

    override def equals(obj: Any): Boolean = obj match {

      case other: NewUpdateProposalTxApplied =>

            isTxIdSame(other.txId) &&
              proposal == other.proposal &&
              ledgerId == other.ledgerId

      case _ => false
    }
  }

  case class CancelUpdateTxApplied(
                                    txId: TxId,
                                    propsalId: String)(implicit val ledgerId: LedgerId) extends TxApplied {
    override def equals(obj: Any): Boolean = obj match {
      case other: CancelUpdateTxApplied =>
        isTxIdSame(other.txId) &&
          propsalId == other.propsalId &&
          ledgerId == other.ledgerId
    }

    override def hashCode(): Int = util.Arrays.hashCode(txId)
  }

  case class NewUpdateAckTxApplied(
                                    txId: TxId,
                                    propsalId: String,
                                    performUpdate: Boolean)(implicit val ledgerId: LedgerId) extends TxApplied {
    override def equals(obj: Any): Boolean = obj match {
      case other: NewUpdateAckTxApplied =>
        isTxIdSame(other.txId) &&
        propsalId == other.propsalId &&
        performUpdate == other.performUpdate &&
        ledgerId == other.ledgerId
    }

    override def hashCode(): Int = util.Arrays.hashCode(txId)
  }

  def toSignedSystemLedgerTxEntry(nodeIdentity: NodeIdentity, tx: SystemLedgerTx)(implicit ec: ExecutionContext): Future[SignedTxEntry] = {
    val asBytes = tx.toBytes
    createSig(nodeIdentity, tx.txId) map { sigs =>
      SignedTxEntry(asBytes, Seq(sigs).txSig)
    }
  }

  def createSig(nodeIdentity: NodeIdentity, msg: Array[Byte])(implicit ec: ExecutionContext): Future[Seq[Array[Byte]]] = {
    nodeIdentity.defaultNodeVerifier.signer.sign(msg).map { sig =>
      Seq(
        nodeIdentity.idBytes,
        nodeIdentity.tagBytes,
        sig
      )
    }

  }
}

class SystemLedger(systemLedgerService: SystemService,
                   getPublicKeyAc: GetVerifier,
                   owners: OracleLedgerOwnerLookupFTx)(implicit val ledgerId: LedgerId, db: Db) extends Ledger with Logging {



  private def checkAndExtractSignatures(replayPreventionBlockHeight: Long,
                    blockId: block.BlockId,
                    ste: SignedTxEntry): FutureTx[(NodeIdTag, Signature)] = for {

    _ <- requireLedgerFTx(
      replayPreventionBlockHeight == blockId.blockHeight,
      ReplayAttackPrevention,
      s"Replay prevention: Block heights don't match, try again")

    _ <- requireLedgerFTx(
      ste.signatures.sigs.nonEmpty,
      NoSignatures,
      "No signature details provided in System ledger tx")

    result <- extractSig(ste.signatures.sigs.head) : @nowarn

  } yield result

  private def checkSignature(
                     nodeIdTag: NodeIdTag,
                     sig: Signature,
                     txId: TxId): FutureTx[Unit] = for {

    publicKeyAcOpt <- getPublicKeyAc(nodeIdTag)

    publicKeyAc = publicKeyAcOpt.getOrElse(
      throw LedgerTxApplyError(NoSuchUser.toString, s"No such identity $nodeIdTag")
    )

    _ <- requireLedgerFTx(
      publicKeyAc.verifier.verify(txId, sig),
      BadSignature,
      s"$nodeIdTag has not been signed correctly")

  } yield ()

  private def checkIsOwner(nodeIdTag: NodeIdTag, allOwners: Seq[UniqueNodeIdentifier]): FutureTx[Unit] = for {
    _ <- requireLedgerFTx(
      allOwners.contains(nodeIdTag.nodeId),
      NotOwner,
      s"$nodeIdTag is not an owner of the system ledger?")

  } yield ()


  override def applyFTx(
                         ledgerItem: ledger.LedgerItem,
                         blockId: block.BlockId,
                         txSeqApplied: TxSeqApplied): LedgerResultFTx = {
    require(ledgerItem.ledgerId == ledgerId, s"The ledger id for this (System) ledger is $ledgerId but " +
      s"the ledgerItem passed has an id of ${ledgerItem.ledgerId}")
    val ste = ledgerItem.txEntryBytes.toSignedTxEntry
    val systemLedgerTx = ste.txEntryBytes.toSystemLedgerTx

    for {
      allOwners <- owners(ledgerId)
      (nodeIdTag, sig) <- checkAndExtractSignatures(systemLedgerTx.replayPreventionBlockHeight, blockId, ste): @nowarn
      _ <- checkIsOwner(nodeIdTag, allOwners)
      _ <- checkSignature(nodeIdTag, sig, systemLedgerTx.txId)
      result <- systemLedgerTx match {

        case cancel@CancelUpdate(replayPreventionBlockHeight, proposalId) =>
          for {
            wasDeleted <- systemLedgerService.deleteProposal(proposalId)
          } yield Right(
            if (wasDeleted) {
              CancelUpdateTxApplied(cancel.txId, proposalId)
            } else TxNotApplied(cancel.txId)
          )

        case prepared@PreparedToUpdate(replayPreventionBlockHeight, proposalId) =>
          for {
            preparedNodeIds <- systemLedgerService.addPreparedNode(nodeIdTag.nodeId, proposalId)
            allArePrepared = allOwners.forall(preparedNodeIds.contains)

            _ <- if (allArePrepared) {
              systemLedgerService.updatePreparationComplete(proposalId)
            } else FutureTx.unit(())

          } yield Right(NewUpdateAckTxApplied(prepared.txId, proposalId, allArePrepared))

        case proposal@UpdateNotification(replayPreventionBlockHeight, blockHeight, uniqueIdentifier, url, checksum) =>
          for {
            newProposal <- systemLedgerService.addUpdateProposal(
              nodeIdTag.nodeId,
              uniqueIdentifier,
              url,
              checksum,
              blockHeight
            )
          } yield Right(NewUpdateProposalTxApplied(proposal.txId, newProposal))
      }
    } yield result
  }

  private def extractSig(signatures: Seq[Array[Byte]]): FutureTx[(NodeIdTag, Signature)] = for {

    _ <- requireLedgerFTx(
      signatures.size == 3,
      BadlyFormedSignature,
      "We need the id, tag and sig in the Systemledger tx")
    id = new String(signatures.head, StandardCharsets.UTF_8)
    tag = new String(signatures(1), StandardCharsets.UTF_8)
    sign = signatures(2)

  } yield (NodeIdTag(id, tag), Signature(sign))

  private def requireLedgerFTx(predicate: Boolean, code: SystemLedgerErrorCode, errorMessage: String): FutureTx[Unit] = {
    if (predicate) FutureTx.unit(())
    else FutureTx.failed(LedgerTxApplyError(code.toString, errorMessage))
  }

  override def apply(
                      ledgerItem: ledger.LedgerItem,
                      blockId: block.BlockId,
                      txSeqApplied: TxSeqApplied): LedgerResult = applyFTx(ledgerItem, blockId, txSeqApplied).dbRunSync.unwrap

  override def importState(height: Long): Try[Unit] =
    Failure(new RuntimeException("System ledger does not work with checkpoints"))

  override def deleteCheckpoint(height: Long): Try[Unit] =
    Failure(new RuntimeException("System ledger does not work with checkpoints"))

  override def createCheckpoint(id: Long): Try[hash.Digest32] =
    Failure(new RuntimeException("System ledger does not work with checkpoints"))
}
