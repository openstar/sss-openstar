package sss.openstar.systemledger

import sss.db.ops.DbOps.{DbRunOps, FutureTxOps}
import sss.db.{Db, FutureTx, Row, where}
import sss.openstar.UniqueNodeIdentifier
import sss.openstar.hash.Digest32
import sss.openstar.schemamigration.SqlSchemaNames.ColumnNames._
import sss.openstar.schemamigration.SqlSchemaNames.TableNames.{createSystemUpdatePreparationTableName, systemUpdateNotificationTableName}
import sss.openstar.systemledger.SystemService._

import java.sql.SQLIntegrityConstraintViolationException
import scala.concurrent.Future

object SystemService {

  object UpdateProposal {

    def apply(row: Row): UpdateProposal = {
      UpdateProposal(
        row.string(proposerCol),
        row.string(updateIdentifierCol),
        row.string(downloadUrlCol),
        Digest32(row.arrayByte(digestCol)),
        row.long(maxHeightPreUpdateCol),
        row.boolean(updatePrepCompleteCol)
      )
    }
  }

  case class UpdateProposal private[systemledger] (proposingNodeId: UniqueNodeIdentifier,
                                     uniqueUpdateIdentifier: String,
                                     url: String,
                                     checksum: Digest32,
                                     requiredFromBlockHeight: Long,
                                     updatePreparationComplete: Boolean)
}

trait SystemServiceQuery {
  def isUpdatePreparationComplete(uniqueUpdateId: String): Future[Boolean]

  def getSignatories(uniqueUpdateId: String): Future[Seq[String]]

  def listProposals(): Future[Seq[UpdateProposal]]
  def nextProposal(uniqueUpdateId: String): Future[Option[UpdateProposal]]
}

class SystemService(implicit db:Db) extends SystemServiceQuery {

  import db.asyncRunContext.ec

  private lazy val notificationTable = db.table(systemUpdateNotificationTableName)
  private lazy val updateAckTable = db.table(createSystemUpdatePreparationTableName)


  private[systemledger] def addUpdateProposal(
                   nodeId: UniqueNodeIdentifier,
                   uniqueIdentifier: String,
                   url: String,
                   checksum: Digest32,
                   blockHeight: Long
                 ): FutureTx[UpdateProposal] = {
    notificationTable.insert(Map(
      proposerCol -> nodeId,
      maxHeightPreUpdateCol -> blockHeight,
      digestCol -> checksum.digest,
      downloadUrlCol -> url,
      updateIdentifierCol -> uniqueIdentifier
    )).map(UpdateProposal.apply).recoverWith {
      case _: SQLIntegrityConstraintViolationException =>
        FutureTx.failed(new IllegalArgumentException(s"Proposal $uniqueIdentifier must already exist."))
      case e => FutureTx.failed(e)
    }
  }

  private[systemledger] def updateProposalRow(uniqueUpdateIdentifier: String): FutureTx[Row] = {
    notificationTable
      .find(
        where(updateIdentifierCol -> uniqueUpdateIdentifier)
      ) flatMap {
      case None => FutureTx.failed(new IllegalArgumentException(s"No proposal id '$uniqueUpdateIdentifier' exists"))
      case Some(row) => FutureTx.unit(row)
    }
  }

  private def linkId(uniqueUpdateIdentifier: String): FutureTx[Long] = {
    updateProposalRow(uniqueUpdateIdentifier).map(_.id)
  }

  private[systemledger] def deleteProposal(uniqueUpdateId: String): FutureTx[Boolean] = {

    for {
      lnkId <- linkId(uniqueUpdateId)
      _ <- updateAckTable.delete(where(
        lnkCol -> lnkId
      ))
      numDeleted <- notificationTable.delete(where(idCol -> lnkId))

    } yield numDeleted == 1
  }

  private[systemledger] def addPreparedNode(nodeId: UniqueNodeIdentifier, uniqueUpdateId: String): FutureTx[Seq[UniqueNodeIdentifier]] = {

    for {
      lnkId <- linkId(uniqueUpdateId)
      _ <- updateAckTable.insert(Map(
        lnkCol -> lnkId,
        preparedNodeCol -> nodeId
      ))
      prepared <- updateAckTable
        .filter(where(lnkCol -> lnkId))
        .map(_.map(_.string(preparedNodeCol)))

    } yield prepared

  }

  private[systemledger] def updatePreparationComplete(uniqueUpdateId: String): FutureTx[Unit] = {
    updateProposalRow(uniqueUpdateId).flatMap(row =>
      notificationTable.updateRow(
        row.asMap + (updatePrepCompleteCol -> true)
      ).map(_ => ())
    )
  }

  def isUpdatePreparationCompleteFTx(uniqueUpdateId: String): FutureTx[Boolean] = {
    updateProposalRow(uniqueUpdateId)
      .map(_.boolean(updatePrepCompleteCol))
  }

  def getSignatoriesFTx(uniqueUpdateId: String): FutureTx[Seq[String]] = {
    for {
      lnkId <- linkId(uniqueUpdateId)
      prepared <- updateAckTable
        .filter(where(lnkCol -> lnkId))
        .map(_.map(_.string(preparedNodeCol)))

    } yield prepared
  }

  override def isUpdatePreparationComplete(uniqueUpdateId: String): Future[Boolean] =
    isUpdatePreparationCompleteFTx(uniqueUpdateId).dbRun

  override def getSignatories(uniqueUpdateId: String): Future[Seq[String]] =
    getSignatoriesFTx(uniqueUpdateId).dbRun

  private[systemledger] def listProposals(afterUniqueUpdateIdOpt: Option[String]): Future[Seq[UpdateProposal]] = {

    val startingIdFTx = afterUniqueUpdateIdOpt match {
      case Some(afterUniqueUpdateId) =>
        for {
          startingIdRowOpt <- notificationTable.find(where(updateIdentifierCol -> afterUniqueUpdateId))
          rowId = startingIdRowOpt.map(_.id).getOrElse(throw new IllegalArgumentException(s"No such proposal id $afterUniqueUpdateIdOpt"))

        } yield rowId

      case None =>
        FutureTx.unit(0)
    }

    startingIdFTx
      .flatMap { startingId =>
        notificationTable
          .map(UpdateProposal(_), where(s"$idCol > ?").using(startingId).orderAsc(idCol))
      }.dbRun

  }

  override def nextProposal(afterThisProposalId: String): Future[Option[UpdateProposal]] = {
    listProposals(Some(afterThisProposalId)).map(_.headOption)
  }

  override def listProposals(): Future[Seq[UpdateProposal]] = listProposals(None)
}
