package sss.openstar


import sss.db.FutureTx
import sss.openstar.account.AccountKeys.AccountVerifyAndDets
import sss.openstar.account.NodeVerifier
import sss.openstar.crypto.SeedBytes

/**
  * Copyright Stepping Stone Software Ltd. 2016, all rights reserved. 
  * mcsherrylabs on 2/16/16.
  */
package object contract {

  type SeqSignatures = Seq[Seq[Array[Byte]]]
  type Signatures = Seq[Array[Byte]]

  trait Contract
  trait Decumbrance extends Contract

  trait Encumbrance extends Contract {
    def decumber(params: Seq[Array[Byte]],
                 context: LedgerContext,
                 decumbrance: Decumbrance): FutureTx[Boolean]
  }

  trait ClosedEncumbrance extends Encumbrance {
    final def decumber(params: Seq[Array[Byte]],
                       context: LedgerContext,
                       decumbrance: Decumbrance): FutureTx[Boolean] = FutureTx.unit(false)
  }

  case object ClosedEncumbrance extends ClosedEncumbrance

  case class NullDecumbrance(makeUnique: Array[Byte] = SeedBytes.secureSeed(8)) extends Decumbrance {
    override def equals(obj: Any): Boolean = obj match {
      case NullDecumbrance(bs) if bs sameElements(makeUnique)=> true
      case _ => false
    }
  }

  case object NullEncumbrance extends Encumbrance {

    override def toString: String = "NullEncumbrance!"

    override def decumber(params: Seq[Array[Byte]], context: LedgerContext, decumbrance: Decumbrance): FutureTx[Boolean] =
      FutureTx.unit(true)
  }

  type FindPublicKeyAccOpt = (String, String) => Option[NodeVerifier]
  type FindPublicKeyAccFTxOpt = (String, String) => FutureTx[Option[NodeVerifier]]

  trait LedgerContext {
    val blockHeight: Long
    val accountOpt: FindPublicKeyAccFTxOpt
    def accountKeysVerify(keyType: String): AccountVerifyAndDets
  }

}
