package sss.openstar.contract

import java.nio.charset.StandardCharsets
import scorex.crypto.signatures.Signature
import sss.ancillary.ByteArrayComparisonOps
import sss.db.FutureTx
import sss.openstar.ledger.TxId
import sss.openstar.util.HashedSecret
import sss.openstar.util.HashedSecret.hashSecret

import scala.concurrent.{ExecutionContext, Future}

/**
  * Copyright Stepping Stone Software Ltd. 2016, all rights reserved. 
  * mcsherrylabs on 2/16/16.
  */

object SaleOrReturnSecretEnc {

  def apply(returnIdentity: String,
            claimant: String,
            secret: Option[Array[Byte]],
            returnBlockHeight: Long): SaleOrReturnSecretEnc =
    apply(returnIdentity,Seq(claimant), secret.map(hashSecret), returnBlockHeight)

}

case class SaleOrReturnSecretEnc(
                                  returnIdentity: String,
                                  claimants: Seq[String],
                                  hashOfSecretOpt: Option[HashedSecret],
                                  returnBlockHeight: Long
                                ) extends Encumbrance with ByteArrayComparisonOps {

  override def toString: String = s"SaleOrReturnSecretEnc for claimant $claimants from $returnIdentity after $returnBlockHeight"

  def decumber(params: Seq[Array[Byte]], context: LedgerContext, decumbrance: Decumbrance): FutureTx[Boolean] = {

    val currentBlockHeight: Long = context.blockHeight

    decumbrance match {
      case ReturnSecretDec =>
        val msg = params(0)
        val sig = Signature(params(1))
        val tag = new String(params(2), StandardCharsets.UTF_8)
        require(currentBlockHeight >= returnBlockHeight, s"$currentBlockHeight < $returnBlockHeight, cannot reclaim this yet.")
        context.accountOpt(returnIdentity, tag) map {
          case None => throw new IllegalArgumentException(s"Cannot find identity $returnIdentity")
          case Some(account) => account.verifier.verify(msg, sig)
        }

      case SaleSecretDec =>
        val msg = params(0)
        val sig = Signature(params(1))
        val claimant = new String(params(2), StandardCharsets.UTF_8)
        val tag = new String(params(3), StandardCharsets.UTF_8)

        require(currentBlockHeight < returnBlockHeight, s"$currentBlockHeight >= $returnBlockHeight, too late to claim this")
        require(claimants.contains(claimant), s"$claimant is not a valid claimant ($claimants)")

        context.accountOpt(claimant, tag) map {
          case None => throw new IllegalArgumentException(s"Cannot find identity $returnIdentity")
          case Some(account) =>
            require(account.verifier.verify(msg, sig), s"Signature did not match")
            hashOfSecretOpt match {
              case Some(hashOfSecret) =>
                val secret = params(4)
                hashOfSecret.matches(secret)
              case None =>
                require(params.size < 5, "A secret has been provided, but none is required.")
                true
            }
        }


      case _ => FutureTx.unit(false)
    }
  }

  override def hashCode(): Int = returnBlockHeight.hashCode() +
    returnIdentity.hashCode +
    claimants.hashCode +
    hashOfSecretOpt.map(_.bytes.hash).getOrElse(0)


  override def equals(obj: scala.Any): Boolean = {
    obj match {
      case that: SaleOrReturnSecretEnc =>
        that.returnIdentity == returnIdentity &&
        that.claimants == claimants &&
        that.returnBlockHeight == returnBlockHeight &&
          that.hashOfSecretOpt == hashOfSecretOpt

      case _ => false
    }
  }
}

case object SaleSecretDec extends Decumbrance {
  /**
    * Utility method to make generating signature sequences more organised
    */
  def createUnlockingSignature(txId:TxId,
                               claimant: String,
                               tagBytes: Array[Byte],
                               signer: (Array[Byte]) => Future[Array[Byte]],
                               secret: Array[Byte])(implicit ec: ExecutionContext): Future[Seq[Array[Byte]]] = {
    createUnlockingSignature(txId, claimant, tagBytes, signer).map(_ :+ secret)
  }

  def createUnlockingSignature(txId:TxId,
                               claimant: String,
                               tagBytes:Array[Byte],
                               signer: (Array[Byte]) => Future[Array[Byte]]
                               )(implicit ec: ExecutionContext): Future[Seq[Array[Byte]]] =
    signer(txId).map(Seq(_, claimant.getBytes(StandardCharsets.UTF_8), tagBytes))

}

case object ReturnSecretDec extends Decumbrance {
  /**
    * Utility method to make generating signature sequences more organised
    */
  def createUnlockingSignature(
                                txId:TxId,
                                tagBytes: Array[Byte],
                                signer: Array[Byte] => Future[Array[Byte]])(implicit ec:ExecutionContext): Future[Seq[Array[Byte]]] = {
    signer(txId).map(Seq(_, tagBytes))
  }
}