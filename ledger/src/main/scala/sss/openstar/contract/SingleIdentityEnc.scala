package sss.openstar.contract

import java.nio.charset.StandardCharsets
import scorex.crypto.signatures.Signature
import sss.ancillary.ByteArrayComparisonOps
import sss.db.FutureTx
import sss.openstar.ledger.TxId

import scala.concurrent.{ExecutionContext, Future}

/**
  * Copyright Stepping Stone Software Ltd. 2016, all rights reserved. 
  * mcsherrylabs on 2/16/16.
  */

object SingleIdentityEnc {
  def apply(trusteeIdentity: String, clientIdentity: String): SingleIdentityEnc =
    SingleIdentityEnc(trusteeIdentity, Some(clientIdentity), 0)
}

case class SingleIdentityEnc(
                              identity: String,
                              senderIdentity: Option[String] = None,
                             minBlockHeight: Long = 0) extends Encumbrance with ByteArrayComparisonOps {

  require(
    if(senderIdentity.isDefined) minBlockHeight == 0 else true,
    "If sender identity is defined, this is client money and block height MUST be 0"
  )

  override def toString: String = s"SingleIdentityEnc at min block height " +
    s"$minBlockHeight with identity $identity " +
    s"and sender Id $senderIdentity"

  def decumber(params: Seq[Array[Byte]], context: LedgerContext, decumbrance: Decumbrance): FutureTx[Boolean] = {

    val currentBlockHeight: Long = context.blockHeight


    decumbrance match {
      case SingleIdentityDec =>
        val msg = params(0)
        val sig = Signature(params(1))
        val tag = new String(params(2), StandardCharsets.UTF_8)
        require(currentBlockHeight >= minBlockHeight, s"$currentBlockHeight < $minBlockHeight, cannot spend this yet.")
        context.accountOpt(identity, tag) map {
          case None => throw new IllegalArgumentException(s"Cannot find identity $identity")
          case Some(account) => account.verifier.verify(msg, sig)
        }

      case _ => FutureTx.unit(false)
    }
  }
}

case object SingleIdentityDec extends Decumbrance {
  /**
    * Utility method to make generating signature sequences more organised
    */
  def createUnlockingSignature(
                                txId:TxId,
                                tagBytes: Array[Byte],
                                signer: (Array[Byte]) => Future[Array[Byte]])(implicit ec: ExecutionContext): Future[Seq[Array[Byte]]] = {
    signer(txId).map(Seq(_, tagBytes))
  }
}
