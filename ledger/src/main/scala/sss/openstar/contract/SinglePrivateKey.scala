package sss.openstar.contract


import scorex.crypto.signatures.{PublicKey, Signature}
import sss.ancillary.ByteArrayEncodedStrOps._
import sss.ancillary.{ByteArrayComparisonOps, Logging}
import sss.db.FutureTx
import sss.openstar.crypto.keypairs.Curve25519AccountKeys


/**
  * Copyright Stepping Stone Software Ltd. 2016, all rights reserved. 
  * mcsherrylabs on 2/16/16.
  */
case class SinglePrivateKey(pKey: PublicKey, keyType: String = Curve25519AccountKeys.keysType, minBlockHeight: Long = 0)
  extends Encumbrance
    with ByteArrayComparisonOps
    with Logging {

  override def equals(obj: scala.Any): Boolean = obj match {
    case spk: SinglePrivateKey => spk.pKey.isSame(pKey) && minBlockHeight == spk.minBlockHeight
    case _ => false
  }

  override def toString: String = s"SinglePrivateKey at min block height $minBlockHeight with pKey ${pKey.toBase64Str}"

  override def hashCode(): Int = pKey.hash

  def decumber(params: Seq[Array[Byte]], context: LedgerContext, decumbrance: Decumbrance): FutureTx[Boolean] = {

    val currentBlockHeight: Long = context.blockHeight

    decumbrance match {
      case PrivateKeySig => {
        val msg = params(0)
        val sig = Signature(params(1))
        require(currentBlockHeight >= minBlockHeight, s"$currentBlockHeight < $minBlockHeight, cannot spend this yet.")
        FutureTx.lazyUnit(context.accountKeysVerify(keyType).verify(pKey, msg, sig))
      }
      case _ => FutureTx.lazyUnit(false)
    }
  }
}

case object PrivateKeySig extends Decumbrance {
  def createUnlockingSignature(signatureOfTxId: Array[Byte]): Seq[Array[Byte]] = {
    Seq(signatureOfTxId)
  }
}
