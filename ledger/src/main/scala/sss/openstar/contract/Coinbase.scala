package sss.openstar.contract


import sss.ancillary.Logging
import sss.openstar.UniqueNodeIdentifier
import sss.openstar.balanceledger.{Tx, TxInput}

/**
  * Copyright Stepping Stone Software Ltd. 2016, all rights reserved. 
  * mcsherrylabs on 2/16/16.
  */
case class CoinbaseValidator(
                              rewardPerBlockAmount: Int,
                              numBlocksInTheFuture: Int,
                              quorumCandidates: () => Set[UniqueNodeIdentifier]
                            ) extends Logging {


  def validate(currentBlockHeight: Long, params: Seq[Seq[Array[Byte]]], tx: Tx): Unit = {
    require(tx.ins.size == 1, s"Only one input per coinbase tx allowed (not ${tx.ins.size})")
    val in = tx.ins.head
    in.sig match {
      case CoinbaseDecumbrance(blockHeight) =>

        var candidates = quorumCandidates()
        val totalReward = rewardPerBlockAmount * candidates.size
        require(in.amount == totalReward, s"${in.amount} is not the amount expected for this block ($totalReward)")


        tx.outs.foreach { out =>
          out.encumbrance match {
            case SingleIdentityEnc(identity, _, minBlockHeight) =>
              require(candidates.contains(identity), s"Cannot have a coinbase output to $identity when quorum is $candidates")
              candidates -= identity
              require(blockHeight == currentBlockHeight, s"Must claim coinbase reward for block ${blockHeight} in the same block, not $currentBlockHeight")
              require(minBlockHeight >= blockHeight + numBlocksInTheFuture,
                s"The earliest spend block must be ${numBlocksInTheFuture} blocks in the future." +
                  s"(min - ${minBlockHeight} cur -  $blockHeight)")

            case _ => require(false, "The reward coins must be locked using SinglePrivateKey/SingleIdentityEnc encumbrance ")
          }
        }
        require(candidates.isEmpty, s"No coinbase output for candidates $candidates")

      case _ => require(false, "Must use CoinbaseDecumbrance to decumber coinbase tx.")
    }
  }
}

case class CoinbaseDecumbrance(blockHeight: Long) extends Decumbrance
