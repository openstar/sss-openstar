package sss.openstar.contract

import scorex.crypto.signatures.PublicKey
import sss.ancillary.Serialize._
import sss.openstar.balanceledger.OracleBalanceLedger.{OracleDecumbrance, OracleLodgementEncumbrance, OracleWithdrawEncumbrance}
import sss.openstar.ledger.LedgerId
import sss.openstar.util.HashedSecret

/**
 * Copyright Stepping Stone Software Ltd. 2016, all rights reserved.
 * mcsherrylabs on 3/3/16.
 */
object ContractSerializer {

  private[contract] val SinglePrivateKeyCode = 1.toByte
  private[contract] val PrivateKeySigCode = 2.toByte
  private[contract] val NullEncumberCode = 3.toByte
  private[contract] val NullDecumberCode = 4.toByte
  private[contract] val CoinBaseDecumbranceCode = 5.toByte
  private[contract] val SingleIdentityEncCode = 6.toByte
  private[contract] val SingleIdentityDecCode = 7.toByte
  private[contract] val SaleOrReturnSecretEncCode = 8.toByte
  private[contract] val SaleSecretDecCode = 9.toByte
  private[contract] val ReturnSecretDecCode = 10.toByte
  private[contract] val OracleDecCode = 11.toByte
  private[contract] val OracleWithdrawEncCode = 12.toByte
  private[contract] val ClosedEncCode = 13.toByte
  private[contract] val OracleLodgementEncCode = 14.toByte

  implicit class ContractFromBytes(bytes: Array[Byte]) {

    def toEncumbrance: Encumbrance = {
      bytes.head match {
        case SinglePrivateKeyCode =>
          SinglePrivateKeyToFromBytes.fromBytes(bytes)
        case NullEncumberCode => NullEncumbrance
        case SingleIdentityEncCode =>
          SingleIdentityEncToFromBytes.fromBytes(bytes)
        case SaleOrReturnSecretEncCode =>
          SaleOrReturnSecretEncToFromBytes.fromBytes(bytes)
        case OracleWithdrawEncCode =>
          OracleWithdrawEncToFromBytes.fromBytes(bytes)
        case OracleLodgementEncCode =>
          OracleLodgementEncToFromBytes.fromBytes(bytes)
        case ClosedEncCode =>
          ClosedEncumbrance
        case x => throw new Error(s"No such contract known to system $x")
      }
    }

    def toDecumbrance: Decumbrance = {
      bytes.head match {
        case PrivateKeySigCode => PrivateKeySig
        case NullDecumberCode => NullDecumbranceToFromBytes.fromBytes(bytes)
        case CoinBaseDecumbranceCode =>
          CoinbaseDecumbranceToFromBytes.fromBytes(bytes)
        case SingleIdentityDecCode => SingleIdentityDec
        case SaleSecretDecCode => SaleSecretDec
        case ReturnSecretDecCode => ReturnSecretDec
        case OracleDecCode =>
          OracleBalanceDecToFromBytes.fromBytes(bytes)

        case x => throw new Error(s"No such contract known to system $x")
      }
    }
  }

  implicit class ContractToBytes(contract: Contract) {
    def toBytes: Array[Byte] = {
      contract match {
        case a: SinglePrivateKey => SinglePrivateKeyToFromBytes.toBytes(a)
        case PrivateKeySig => Array(PrivateKeySigCode)
        case NullEncumbrance => Array(NullEncumberCode)
        case a: NullDecumbrance => NullDecumbranceToFromBytes.toBytes(a)
        case a: CoinbaseDecumbrance => CoinbaseDecumbranceToFromBytes.toBytes(a)
        case a: SingleIdentityEnc => SingleIdentityEncToFromBytes.toBytes(a)
        case SingleIdentityDec => Array(SingleIdentityDecCode)
        case a: SaleOrReturnSecretEnc =>
          SaleOrReturnSecretEncToFromBytes.toBytes(a)
        case SaleSecretDec => Array(SaleSecretDecCode)
        case ReturnSecretDec => Array(ReturnSecretDecCode)
        case o: OracleDecumbrance =>
          OracleBalanceDecToFromBytes.toBytes(o)
        case o: OracleLodgementEncumbrance =>
          OracleLodgementEncToFromBytes.toBytes(o)
        case o: OracleWithdrawEncumbrance =>
          OracleWithdrawEncToFromBytes.toBytes(o)
        case ClosedEncumbrance => Array(ClosedEncCode)

      }
    }
  }

  object SingleIdentityEncToFromBytes extends Serializer[SingleIdentityEnc] {
    override def toBytes(t: SingleIdentityEnc): Array[Byte] = {
      (ByteSerializer(SingleIdentityEncCode) ++
        StringSerializer(t.identity) ++
        OptionSerializer(t.senderIdentity, StringSerializer) ++
        LongSerializer(t.minBlockHeight)).toBytes
    }

    override def fromBytes(b: Array[Byte]): SingleIdentityEnc = {
      val extracted =
        b.extract(ByteDeSerialize, StringDeSerialize, OptionDeSerialize(StringDeSerialize), LongDeSerialize)
      val headerByte = extracted._1
      require(
        headerByte == SingleIdentityEncCode,
        s"Wrong header byte, expecting $SingleIdentityEncCode, got $headerByte")
      SingleIdentityEnc(extracted._2, extracted._3, extracted._4)
    }
  }

  object NullDecumbranceToFromBytes
    extends Serializer[NullDecumbrance] {
    override def toBytes(t: NullDecumbrance): Array[Byte] = {
      (ByteSerializer(NullDecumberCode) ++ ByteArraySerializer(t.makeUnique)).toBytes
    }

    override def fromBytes(b: Array[Byte]): NullDecumbrance = {
      val extracted = b.extract(ByteDeSerialize, ByteArrayDeSerialize)
      val headerByte = extracted._1
      require(
        headerByte == NullDecumberCode,
        s"Wrong header byte, expecting $NullDecumberCode, got $headerByte")
      NullDecumbrance(extracted._2)
    }
  }

  object CoinbaseDecumbranceToFromBytes
    extends Serializer[CoinbaseDecumbrance] {
    override def toBytes(t: CoinbaseDecumbrance): Array[Byte] = {
      (ByteSerializer(CoinBaseDecumbranceCode) ++ LongSerializer(t.blockHeight)).toBytes
    }

    override def fromBytes(b: Array[Byte]): CoinbaseDecumbrance = {
      val extracted = b.extract(ByteDeSerialize, LongDeSerialize)
      val headerByte = extracted._1
      require(
        headerByte == CoinBaseDecumbranceCode,
        s"Wrong header byte, expecting $CoinBaseDecumbranceCode, got $headerByte")
      CoinbaseDecumbrance(extracted._2)
    }
  }

  object SinglePrivateKeyToFromBytes extends Serializer[SinglePrivateKey] {

    override def toBytes(t: SinglePrivateKey): Array[Byte] = {
      (ByteSerializer(SinglePrivateKeyCode) ++
        LongSerializer(t.minBlockHeight) ++
        StringSerializer(t.keyType) ++
        ByteArraySerializer(t.pKey)).toBytes
    }

    override def fromBytes(b: Array[Byte]): SinglePrivateKey = {
      val extracted =
        b.extract(ByteDeSerialize, LongDeSerialize, StringDeSerialize, ByteArrayDeSerialize)
      val headerByte = extracted._1
      require(
        headerByte == SinglePrivateKeyCode,
        s"Wrong header byte, expecting $SinglePrivateKeyCode, got $headerByte")
      SinglePrivateKey(PublicKey(extracted._4), extracted._3, extracted._2)
    }
  }

  object SaleOrReturnSecretEncToFromBytes
    extends Serializer[SaleOrReturnSecretEnc] {

    override def toBytes(t: SaleOrReturnSecretEnc): Array[Byte] = {
      (ByteSerializer(SaleOrReturnSecretEncCode) ++
        StringSerializer(t.returnIdentity) ++
        SequenceSerializer(t.claimants map StringSerializer) ++
        OptionSerializer(
          t.hashOfSecretOpt,
          (hashedSecret: HashedSecret) => ByteArraySerializer(hashedSecret.bytes)
        ) ++
        LongSerializer(t.returnBlockHeight)).toBytes
    }

    override def fromBytes(b: Array[Byte]): SaleOrReturnSecretEnc = {
      val extracted = b.extract(ByteDeSerialize,
        StringDeSerialize,
        SequenceDeSerialize(_.extract(StringDeSerialize)),
        OptionDeSerialize(ByteArrayDeSerialize),
        LongDeSerialize)

      val headerByte = extracted._1
      require(
        headerByte == SaleOrReturnSecretEncCode,
        s"Wrong header byte, expecting $SaleOrReturnSecretEncCode, got $headerByte")
      SaleOrReturnSecretEnc(extracted._2,
        extracted._3,
        extracted._4 map HashedSecret.apply,
        extracted._5)
    }
  }

  object OracleBalanceDecToFromBytes extends Serializer[OracleDecumbrance] {

    override def toBytes(t: OracleDecumbrance): Array[Byte] = {
      ByteSerializer(OracleDecCode) ++
        ByteSerializer(t.ledgerId.id)
          .toBytes
    }

    override def fromBytes(b: Array[Byte]): OracleDecumbrance = {
      val extracted = b.extract(ByteDeSerialize, ByteDeSerialize)
      require(
        extracted._1 == OracleDecCode,
        s"Wrong header byte, expecting $OracleDecCode, got ${extracted._1}")

      OracleDecumbrance(LedgerId(extracted._2))
    }
  }


  object OracleWithdrawEncToFromBytes extends Serializer[OracleWithdrawEncumbrance] {

    override def toBytes(t: OracleWithdrawEncumbrance): Array[Byte] = {
      ByteSerializer(OracleWithdrawEncCode) ++
        StringSerializer(t.exitInstructions)
          .toBytes
    }

    override def fromBytes(b: Array[Byte]): OracleWithdrawEncumbrance = {
      val extracted = b.extract(ByteDeSerialize, StringDeSerialize)
      require(
        extracted._1 == OracleWithdrawEncCode,
        s"Wrong header byte, expecting $OracleWithdrawEncCode, got ${extracted._1}")

      OracleWithdrawEncumbrance(extracted._2)
    }
  }

  object OracleLodgementEncToFromBytes extends Serializer[OracleLodgementEncumbrance] {

    override def toBytes(t: OracleLodgementEncumbrance): Array[Byte] = {
      ByteSerializer(OracleLodgementEncCode) ++
        ByteArraySerializer(t.globalUniqueIdentifier)
          .toBytes
    }

    override def fromBytes(b: Array[Byte]): OracleLodgementEncumbrance = {
      val extracted = b.extract(ByteDeSerialize, ByteArrayDeSerialize)
      require(
        extracted._1 == OracleLodgementEncCode,
        s"Wrong header byte, expecting $OracleLodgementEncCode, got ${extracted._1}")

      OracleLodgementEncumbrance(extracted._2)
    }
  }

}
