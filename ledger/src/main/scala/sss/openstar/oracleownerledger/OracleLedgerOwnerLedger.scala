package sss.openstar.oracleownerledger

import scorex.crypto.signatures.Signature
import sss.ancillary.Logging
import sss.db.ops.DbOps.DbRunOps
import sss.db.{Db, FutureTx, QueryResults, Row, where}
import sss.openstar.account.KeyFactory
import sss.openstar.balanceledger.OracleBalanceLedger.{OracleBalanceLedgerContext, OracleLedgerOwnerLookupFTx}
import sss.openstar.common.block.BlockId
import sss.openstar.contract.{FindPublicKeyAccFTxOpt, FindPublicKeyAccOpt}
import sss.openstar.identityledger.Identifier
import sss.openstar.ledger.LedgerOps.TryLedgerResultToTryApplied
import sss.openstar.ledger.{Ledger, LedgerErrorCodeGenericException, LedgerId, LedgerItem, LedgerResult, LedgerResultFTx, LedgerTxApplyError, SignedTxEntry, ToTxEntryWithSignatures, TxId, TxSeqApplied}
import sss.openstar.schemamigration.SqlSchemaNames.ColumnNames.{ledgerIdCol, lenCol, oracleCol}
import sss.openstar.schemamigration.SqlSchemaNames.TableNames.oracleOwnerTableName
import sss.openstar.{UniqueNodeIdentifier, contract, hash}

import java.nio.charset.StandardCharsets.UTF_8
import scala.util.{Failure, Success, Try}

class OracleLedgerOwnerLedger(
                               identifier: Identifier,
                               findAccountOptFTx: FindPublicKeyAccFTxOpt,
                               maxAllowedOwnersPerLedger: Int,
                               maxAllowedLedgers: Int,
                               keyFactory: KeyFactory
                             )(implicit db: Db, implicit val ledgerId: LedgerId)
  extends Ledger
    with Logging {

  private val table = db.table(oracleOwnerTableName)

  lazy val findPublicKeyAccOpt: FindPublicKeyAccOpt = (a, b) => findAccountOptFTx(a,b).dbRunSyncGet

  val lookup: OracleLedgerOwnerLookupFTx = getOwnersFTx

  def getOwnersFTx(ledgerId: LedgerId): FutureTx[Seq[String]] = {
    table.filter(
      where(
        ledgerIdCol -> ledgerId.id
      )
    ).map(_.map(_.string(oracleCol)))
  }

  /**
   * Can be used to set the owners as part of chain genesis
   *
   * @param owners
   * @param ledgerId
   * @return
   */
  def setOwners(owners: Seq[UniqueNodeIdentifier], ledgerId: LedgerId): Try[Seq[String]] = Try {

    validateMaxOwnersOrThrow(owners)

    val plan = for {
      currentOwners <- getOwnersFTx(ledgerId)
      _ = require(currentOwners.isEmpty, s"Owners for $ledgerId already set to $currentOwners")
      rows <- table.map(r => r.byte(ledgerIdCol), where())
      _ = validateMaxLedgersOrThrow(rows, ledgerId)
      rows <- insertOwners(owners, ledgerId)
    } yield rows.map(_.string(oracleCol))

    plan.dbRunSyncGet
  }

  //def getOwners(ledgerId: LedgerId): FutureTx[Seq[String]] = getOwnersFTx(ledgerId)

  override def apply(
                      ledgerItem: LedgerItem,
                      blockId: BlockId,
                      txSeqApplied: TxSeqApplied): LedgerResult =  {
    applyFTx(ledgerItem, blockId, txSeqApplied)
      .dbRunSync.unwrap
  }

  private def insertOwners(owners: Seq[UniqueNodeIdentifier], ledgerId: LedgerId): FutureTx[Seq[Row]] = {
    FutureTx.sequence(owners.distinct.map(newOwner => table.insert(Map(
      ledgerIdCol -> ledgerId.id,
      oracleCol -> newOwner
    ))))
  }

  def applyFTx(
                ledgerItem: LedgerItem,
                blockId: BlockId,
                txSeqApplied: TxSeqApplied): LedgerResultFTx = Try {

    (for {
      ste <- FutureTx.lazyUnit {
        require(ledgerItem.ledgerId == ledgerId, s"The ledger id for this (Identity) ledger is $ledgerId but " +
          s"the ledgerItem passed has an id of ${ledgerItem.ledgerId}")
        ledgerItem.txEntryBytes.toSignedTxEntry
      }
      rows <- table.map(r => r.byte(ledgerIdCol), where())

      result <- ste.txEntryBytes.toOracleLedgerTx match {
        case OracleClaim(claimedLedgerId, instigatingIdentity, others) =>
          validateMaxLedgersOrThrow(rows, claimedLedgerId)
          validateMaxOwnersOrThrow(others)
          for {
            b <- verifyChangeRequest(ste, ste.txId, instigatingIdentity)
            _ = require(b, "Failed to verify change request")
            currentOwners <- getOwnersFTx(claimedLedgerId)
            numDeleted <- if (currentOwners.nonEmpty) {
              require(currentOwners.contains(instigatingIdentity),
                s"$instigatingIdentity is attempting to update ownership claim to $ledgerId but is not an owner: ${currentOwners.mkString}")
              table.delete(where(ledgerIdCol -> claimedLedgerId.id))

            } else {
              FutureTx.unit(0)
            }
            updateOwnerRows <- insertOwners(instigatingIdentity +: others, claimedLedgerId)
          } yield OracleOwnerLedgerTxApplied(ste.txId, claimedLedgerId, updateOwnerRows.map(_.string(oracleCol)))

      }

    } yield Right(result))
  } match {
    case Failure(err: LedgerTxApplyError) => FutureTx.unit(Left(err))
    case Failure(e) => FutureTx.unit(Left(LedgerTxApplyError(LedgerErrorCodeGenericException, e.getMessage)))
    case Success(result) => result
  }


  private def validateMaxLedgersOrThrow(rows: QueryResults[Byte], claimedLedgerId: LedgerId): Unit = {
    val notTooManyLedgerOwners = rows.contains(claimedLedgerId.id) || rows.distinct.size <= maxAllowedLedgers
    require(notTooManyLedgerOwners, s"Cannot claim ledger id ${claimedLedgerId.id}, max allowed ledger owners reached $maxAllowedOwnersPerLedger")
  }

  private def validateMaxOwnersOrThrow(owners: Seq[UniqueNodeIdentifier]): Unit = {
    require(owners.forall(identifier.isValidIdentifier), s"one of $owners is not a valid identifier")
    require(owners.size < maxAllowedOwnersPerLedger, s"too many owners, max allowed is $maxAllowedOwnersPerLedger")
  }

  private def verifyChangeRequest(ste: SignedTxEntry, txId: TxId, identity: String): FutureTx[Boolean] = {

    //require(ste.signatures.sigs.nonEmpty && ste.signatures.sigs.head.size == 3, "A id/tag/sig pair must be provided to continue.")
    val tag = new String(ste.signatures.sigs.head(1), UTF_8)
    val sig = Signature(ste.signatures.sigs.head(2))
    findAccountOptFTx(identity, tag).map { accOpt =>
      require(accOpt.isDefined, s"Could not find an account for identity/tag pair ${identity}/$tag provided in signature.")
      require(accOpt.get.verifier.verify(txId, sig), "The signature does not match the txId")
      true
    }
  }

  def ledgerContext(blockId: BlockId): contract.LedgerContext =
    OracleBalanceLedgerContext(blockId.blockHeight, lookup , findAccountOptFTx, keyFactory)

  override def importState(height: Long): Try[Unit] = Failure(new RuntimeException("NOT IMPLEMENTED"))

  override def deleteCheckpoint(height: Long): Try[Unit] = Failure(new RuntimeException("NOT IMPLEMENTED"))

  override def createCheckpoint(id: Long): Try[hash.Digest32] = Failure(new RuntimeException("NOT IMPLEMENTED"))
}
