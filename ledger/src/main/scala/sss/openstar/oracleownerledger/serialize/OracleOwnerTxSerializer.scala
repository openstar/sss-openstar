package sss.openstar.oracleownerledger.serialize

import sss.ancillary.Serialize.{ByteDeSerialize, ByteSerializer, LongDeSerialize, LongSerializer, SequenceDeSerialize, SequenceSerializer, SerializeHelper, Serializer, StringDeSerialize, StringSerializer}
import sss.openstar.ledger.LedgerId
import sss.openstar.oracleownerledger.{OracleClaim, OracleClaimCode}

object OracleOwnerTxSerializer {

  object OracleClaimSerializer extends Serializer[OracleClaim] {
    override def toBytes(t: OracleClaim): Array[Byte] =
      (ByteSerializer(OracleClaimCode) ++
        ByteSerializer(t.ledgerId.id) ++
        LongSerializer(t.uniqueMessage) ++
        StringSerializer(t.instigatingOwner) ++
        SequenceSerializer(t.otherOwners.map(StringSerializer))).toBytes

    override def fromBytes(bytes: Array[Byte]): OracleClaim = {

      val (code, ledIdByte, unique, instigating, othersSeq) = bytes.extract(ByteDeSerialize,
        ByteDeSerialize,
        LongDeSerialize,
        StringDeSerialize,
        SequenceDeSerialize(_.extract(StringDeSerialize)))

      require(code == OracleClaimCode, s"Bad deser code for OracleClaim ${code}")

      new OracleClaim(LedgerId(ledIdByte), instigating, othersSeq) {
        override private[oracleownerledger] val uniqueMessage = unique
      }
    }

  }


}
