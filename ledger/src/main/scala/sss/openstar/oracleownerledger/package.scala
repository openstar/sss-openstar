package sss.openstar

import sss.ancillary.ByteArrayEncodedStrOps.ByteArrayToBase64UrlStr

import java.security.SecureRandom
import sss.openstar.hash.SecureCryptographicHash
import sss.openstar.identityledger.IdentityService
import sss.openstar.ledger.{LedgerId, TxApplied, TxId}
import sss.openstar.oracleownerledger.serialize.OracleOwnerTxSerializer

package object oracleownerledger {

  protected[oracleownerledger] val OracleClaimCode = 1.toByte

  abstract class OracleLedgerTx {

    private[oracleownerledger] val uniqueMessage = SecureRandom.getInstanceStrong.nextLong()

    lazy val txId: Array[Byte] = SecureCryptographicHash.hash(this.toBytes)

  }

  case class OracleClaim(ledgerId: LedgerId,
                         instigatingOwner: UniqueNodeIdentifier,
                         otherOwners: Seq[UniqueNodeIdentifier]) extends OracleLedgerTx

  case class OracleOwnerLedgerTxApplied(
                                         txId: TxId,
                                         changedLedgerId: LedgerId,
                                         updatedOwners: Seq[UniqueNodeIdentifier])(implicit val ledgerId: LedgerId)
    extends TxApplied with BusEvent {
    override def toString: UniqueNodeIdentifier = s"txId: ${txId.toBase64Str}, ${super.toString}"
  }

  implicit class OracleLedgerTxToBytes(tx: OracleLedgerTx) {
    def toBytes: Array[Byte] = tx match {
      case tx: OracleClaim => tx.toBytes
    }
  }

  implicit class OracleLedgerTxFromBytes(bytes: Array[Byte]) {
    def toOracleLedgerTx: OracleLedgerTx = bytes.head match {
      case OracleClaimCode => bytes.toOracleClaim

    }
  }

  implicit class OracleClaimFromBytes(bytes: Array[Byte]) {
    def toOracleClaim: OracleClaim = OracleOwnerTxSerializer.OracleClaimSerializer.fromBytes(bytes)
  }


  implicit class OracleClaimToBytes(tx: OracleClaim) {
    def toBytes: Array[Byte] = OracleOwnerTxSerializer.OracleClaimSerializer.toBytes(tx)
  }

}
