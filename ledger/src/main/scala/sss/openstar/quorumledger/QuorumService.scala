package sss.openstar.quorumledger


import sss.ancillary.Logging
import sss.db.RowSerializer._
import sss.db._
import sss.db.ops.DbOps.{DbRunOps, FutureTxOps}
import sss.openstar.UniqueNodeIdentifier
import sss.openstar.chains.Chains.GlobalChainIdMask
import sss.openstar.hash.Digest32
import sss.openstar.schemamigration.SqlSchemaNames
import sss.openstar.schemamigration.SqlSchemaNames.ColumnNames.identityCol
import sss.openstar.schemamigration.dynamic.GlobalTableNameTag
import sss.openstar.util.StringCheck.SimpleTag
import sss.openstar.util.{HashUtils, TableCopy}

import java.sql.SQLIntegrityConstraintViolationException
import scala.util.Try

/**
  *
  * @param uniqueChainId will be used as part of a db table name, only standard characters allowed.
  * @param db
  */

trait QuorumServiceQuery {
  def candidates(): Set[UniqueNodeIdentifier]
}


object QuorumService {

  def quorumService(uniqueChainId: GlobalChainIdMask)(implicit db: Db): QuorumService =
    new QuorumService(uniqueChainId)

  def create(uniqueChainId: GlobalChainIdMask, owners: UniqueNodeIdentifier*)(implicit db: Db): QuorumService = {

    val result = new QuorumService(uniqueChainId)
    owners foreach result.add

    result
  }

}

class QuorumService private[quorumledger](private[quorumledger] val uniqueChainId: GlobalChainIdMask)(implicit db: Db)
  extends QuorumServiceQuery
    with Logging
    with TableCopy {

  val taggedTableNames = new SqlSchemaNames.TaggedTableNames()(GlobalTableNameTag(uniqueChainId.toString))
  import taggedTableNames.quorumTableName

  private lazy val currentStateTableName: String = quorumTableName
  private lazy val table = db.table(currentStateTableName)
  private lazy val idPkSql: String = s"CONSTRAINT ${currentStateTableName}_pk PRIMARY KEY($identityCol)"
  private lazy val addIdPkSql: String = s"ALTER TABLE $currentStateTableName ADD $idPkSql"
  private lazy val dropIdPkSql: String = s"ALTER TABLE $currentStateTableName DROP CONSTRAINT ${currentStateTableName}_pk"

  def candidatesFTx(): FutureTx[Set[UniqueNodeIdentifier]] = {
    (for {
      r <- table
    } yield r.string(identityCol)).map(_.toSet)
  }

  override def candidates(): Set[UniqueNodeIdentifier] = candidatesFTx().dbRunSyncGet

  def add(newCandidate: UniqueNodeIdentifier): Set[UniqueNodeIdentifier] = addFTx(newCandidate).dbRunSyncGet

  private[quorumledger] def addFTx(newCandidate: UniqueNodeIdentifier): FutureTx[Set[UniqueNodeIdentifier]] = {

    table
      .persist(Map(identityCol -> newCandidate))
      .flatMap(_ => candidatesFTx())
      .recoverWithDb {
        case e: SQLIntegrityConstraintViolationException =>
          log.warn(s"Cannot add existing member as newMember. $newCandidate")
          candidatesFTx()
      }
  }

  def remove(candidate: UniqueNodeIdentifier): Set[UniqueNodeIdentifier] =
    removeFTx(candidate).dbRunSyncGet

  private[quorumledger] def removeFTx(candidate: UniqueNodeIdentifier): FutureTx[Set[UniqueNodeIdentifier]] = (for {
    deleted <- (table delete where(identityCol -> candidate))
    _ = require(deleted == 1 || deleted == 0, s"$deleted rows deleted from quorum ledger $uniqueChainId, should only be 0 or 1")
    result <- candidatesFTx()
  } yield result)

  private def createSnapshotTableName(tag: SimpleTag): String = s"${currentStateTableName}_$tag"


  def importState(height: Long): Try[Unit] = {
    val tableToImport = createSnapshotTableName(height)

    for {
      _ <- db.executeSql(dropIdPkSql)
        _ <- replaceTable(currentStateTableName, tableToImport)
        _ <- db.executeSql(addIdPkSql)
    } yield ()

  }.dbRunSync

  def createCheckpoint(height: Long): Try[Digest32] = {
    val snapshotName = createSnapshotTableName(height)
    for {
      _ <- db.executeSql(dropIdPkSql)
      _ <- copyTable(currentStateTableName, snapshotName)
      _ <- db.executeSql(addIdPkSql)
    } yield snapshotName
  }.dbRunSync.flatMap { snapshotName =>
    val newTable = db.select(s"SELECT $identityCol FROM $snapshotName order by $identityCol ASC")
    HashUtils(
      pageGenerator(newTable, _.toBytes(newTable.columnsMetaInfo), Seq.empty) _
    )
  }


  def deleteCheckpoint(height: Long): Try[Unit] = {
    val name = createSnapshotTableName(height)
    db.executeSql(s"DROP TABLE $name;")
  }.dbRunSync.map(_ => ())
}
