package sss.openstar.quorumledger

import sss.openstar.UniqueNodeIdentifier
import sss.openstar.quorumledger.QuorumPolicyChange.QuorumPolicyChangeType.QuorumPolicyChangeType

object QuorumPolicyChange {

  object QuorumPolicyChangeType extends Enumeration {
    type QuorumPolicyChangeType = Value
    val AllOthersMustSign = Value("AllOthersMustSign")
  }
}

abstract class QuorumPolicyChange {

  val quorumPolicyChangeType: QuorumPolicyChangeType

  def isRemovePolicySatisfied(memberToBeRemoved: UniqueNodeIdentifier,
                              quorumMembersWithSigs: Map[UniqueNodeIdentifier, Option[Boolean]]): Boolean

  def isAddPolicySatisfied(memberToBeAdded: UniqueNodeIdentifier,
                           quorumMembersWithSigs: Map[UniqueNodeIdentifier, Option[Boolean]]): Boolean

  val name: QuorumPolicyChangeType
}
