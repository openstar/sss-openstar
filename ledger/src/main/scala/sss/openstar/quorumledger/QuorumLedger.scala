package sss.openstar.quorumledger

import scorex.crypto.signatures.Signature
import sss.ancillary.ByteArrayEncodedStrOps._
import sss.ancillary.Logging
import sss.db.ops.DbOps.DbRunOps
import sss.db.{Db, FutureTx}
import sss.openstar.account.NodeVerifier
import sss.openstar.chains.Chains.GlobalChainIdMask
import sss.openstar.common.block.BlockId
import sss.openstar.ledger.LedgerOps.TryLedgerResultToTryApplied
import sss.openstar.ledger._
import sss.openstar.quorumledger.QuorumLedger.NewQuorumCandidates
import sss.openstar.{BusEvent, UniqueNodeIdentifier, hash}

import java.nio.charset.StandardCharsets
import scala.util.Try


object QuorumLedger {
  case class NewQuorumCandidates(txId: TxId, uniqueId: GlobalChainIdMask, candidates: Set[UniqueNodeIdentifier])(
    implicit val ledgerId: LedgerId)  extends TxApplied with BusEvent {
    override def toString: UniqueNodeIdentifier =
      s"txId: ${txId.toBase64Str}, ${super.toString}"
  }
}

class QuorumLedger(chainId: GlobalChainIdMask,
                   quorumService: QuorumService,
                   quorumChangePolicy: QuorumPolicyChange,
                   accountOpt: (String, String) => FutureTx[Option[NodeVerifier]]
                  )(implicit val ledgerId: LedgerId, db:Db)  extends  Ledger with Logging {




  require(chainId == quorumService.uniqueChainId,
    s"Mismatched chain Id is quorum service (${quorumService.uniqueChainId}) and ledger {$chainId}")

  override def apply(
                      ledgerItem: LedgerItem,
                      blockId: BlockId,
                      txSeqApplied: TxSeqApplied): LedgerResult = applyFTx(ledgerItem, blockId, txSeqApplied)
    .dbRunSync.unwrap


  private def matchCandidatesToSigs(
                                     candidates: Set[UniqueNodeIdentifier],
                                     seqDeserialized: Seq[DeserializedSig],
                                     msg: Array[Byte]
                                   ): FutureTx[Map[UniqueNodeIdentifier, Option[Boolean]]] = {

    val mapCandidatesToSigs: Map[UniqueNodeIdentifier, Option[DeserializedSig]] = candidates.map(member => {
      member -> seqDeserialized.find(_._1 == member)
    }).toMap

    val mapCandidatesToVerifiedSignatures: Map[UniqueNodeIdentifier, FutureTx[Option[Boolean]]] = mapCandidatesToSigs.map {
      case (k, sigsOpt) => k -> {
        sigsOpt
          .map(sigs => {
            val accFound: FutureTx[Option[NodeVerifier]] = accountOpt(sigs._1, sigs._2)
            accFound.map(_.map(_.verifier.verify(msg, Signature(sigs._3))))
          }) match {
          case None => FutureTx.unit(None)
          case Some(f) => f
        }
      }
    }

    val futTxTuples: Seq[FutureTx[(UniqueNodeIdentifier, Option[Boolean])]] =
      mapCandidatesToVerifiedSignatures
        .map {
          case (k, futTx) => futTx.map(v => k -> v)
        }.toSeq

    FutureTx.sequence(futTxTuples).map(_.toMap)
  }

  def applyFTx(
                ledgerItem: LedgerItem,
                blockId: BlockId,
                txSeqApplied: TxSeqApplied): LedgerResultFTx = FutureTx.lazyUnit {

    require(ledgerItem.ledgerId == ledgerId, s"The ledger id for this (Quorum) ledger is $ledgerId but " +
      s"the ledgerItem passed has an id of ${ledgerItem.ledgerId}")

    ledgerItem.txEntryBytes.toSignedTxEntry
  } flatMap { ste =>
    ste.txEntryBytes.toQuorumLedgerTx match {
      case msg @ AddNodeId(nodeId) =>
        for {
          currentSetOfQuorumMembers <- quorumService.candidatesFTx()
          seqDeserialized = deserializeSigLabels(ste.signatures.sigs)
          candidateSigs <- matchCandidatesToSigs(currentSetOfQuorumMembers, seqDeserialized, msg.txId)

          verified = quorumChangePolicy.isAddPolicySatisfied(nodeId, candidateSigs)
          _ = require(verified, s"To add an id to the quorum " +
            s"members must correctly sign the tx according " +
            s"to the policy ${quorumChangePolicy.name}")
          candidates <- quorumService.addFTx(nodeId)
        } yield Right(NewQuorumCandidates(ledgerItem.txId, chainId, candidates))

      case msg @ RemoveNodeId(nodeId) =>

        for {
          currentMembers <- quorumService.candidatesFTx()
          _ = require(currentMembers != Set(nodeId), s"$nodeId is the last member, cannot remove the last member.")
          currentSetMinusNodeToBeRemoved = currentMembers.filterNot(_ == nodeId)
          seqDeserialized = deserializeSigLabels(ste.signatures.sigs)
          candidateSigs <- matchCandidatesToSigs(currentMembers, seqDeserialized, msg.txId)
          verified = quorumChangePolicy.isRemovePolicySatisfied(nodeId, candidateSigs)
          _ = require(verified, s"To remove an id from the quorum all (other) current members must correctly sign the tx")
          candidates <- quorumService.removeFTx(nodeId)
        } yield Right(NewQuorumCandidates(ledgerItem.txId, chainId, candidates))
    }
  }



  private def deserializeSigLabels(sigs: Seq[Seq[Array[Byte]]]): Seq[DeserializedSig]= {
    for {
      sig <- sigs
      _ = require(sig.size == 3, s"Must have id, tag and signature  - $sig")
    } yield {
      val id = new String(sig(0), StandardCharsets.UTF_8)
      val tag = new String(sig(1), StandardCharsets.UTF_8)
      val sign = sig(2)
      (id, tag, sign)
    }
  }

  override def importState(height: Long): Try[Unit] = quorumService.importState(height)

  override def createCheckpoint(id: Long): Try[hash.Digest32] = quorumService.createCheckpoint(id)

  override def deleteCheckpoint(height: Long): Try[Unit] = quorumService.deleteCheckpoint(height)
}


