package sss.openstar.quorumledger
import sss.openstar.UniqueNodeIdentifier
import sss.openstar.quorumledger.QuorumPolicyChange.QuorumPolicyChangeType
import sss.openstar.quorumledger.QuorumPolicyChange.QuorumPolicyChangeType.QuorumPolicyChangeType

class AllAgreeQuorumChangePolicy
  extends QuorumPolicyChange {

  override val quorumPolicyChangeType: QuorumPolicyChangeType =
    QuorumPolicyChangeType.AllOthersMustSign

  override def isRemovePolicySatisfied(memberToBeRemoved: UniqueNodeIdentifier,
                                       quorumMembersWithSigs: Map[UniqueNodeIdentifier, Option[Boolean]]): Boolean = {

    quorumMembersWithSigs.filterNot(_._1 == memberToBeRemoved)
      .forall(tup => tup._2.contains(true))
  }

  override def isAddPolicySatisfied(memberToBeAdded: UniqueNodeIdentifier,
                                    quorumMembersWithSigs: Map[UniqueNodeIdentifier, Option[Boolean]]): Boolean = {
    quorumMembersWithSigs
      .forall(tup => tup._2.contains(true))
  }

  override val name: QuorumPolicyChangeType = QuorumPolicyChangeType.AllOthersMustSign
}
