package sss.openstar

import sss.ancillary.ByteArrayEncodedStrOps.ByteArrayToBase64UrlStr
import sss.ancillary.Serialize.{ByteDeSerialize, ByteSerializer, LongDeSerialize, LongSerializer, SequenceDeSerialize, SequenceSerializer, SerializeHelper, StringDeSerialize, StringSerializer}
import sss.openstar.hash.SecureCryptographicHash
import sss.openstar.ledger.{LedgerId, TxApplied, TxId}

import java.security.SecureRandom

package object counterledger {

  case class CounterTxApplied(txId: TxId)(implicit val ledgerId: LedgerId)  extends TxApplied with BusEvent {
    override def toString: UniqueNodeIdentifier = s"txId: ${txId.toBase64Str}, ${super.toString}"
  }

  class CounterLedgerException(msg: String) extends RuntimeException(msg)

  sealed trait CounterLedgerTx {

    lazy val txId: Array[Byte] = SecureCryptographicHash.hash(this.toBytes)

    val uniqueMessage: Long = SecureRandom.getInstanceStrong.nextLong()
  }

  private val FailOnTheseNodesCode = 1.toByte
  private val IncrementFailureCode = 2.toByte
  private val PrintoutCode = 3.toByte

  case class FailOnTheseNodes(nodes: Seq[String]) extends CounterLedgerTx
  case class IncrementFailure() extends CounterLedgerTx
  case class Printout() extends CounterLedgerTx

  implicit class CounterLedgerToBytes(val tx: CounterLedgerTx) extends AnyVal {

    def toBytes: Array[Byte] = tx match {
      case e: FailOnTheseNodes => e.toBytes
      case e: IncrementFailure => e.toBytes
      case e: Printout => e.toBytes
    }
  }

  implicit class CounterLedgerFromBytes(val bytes: Array[Byte]) extends AnyVal {

    def toCounterLedgerTx: CounterLedgerTx = bytes.head match {
      case FailOnTheseNodesCode => bytes.toFailOnTheseNodes
      case IncrementFailureCode => bytes.toIncrementFailure
      case PrintoutCode => bytes.toPrintout
      case x => throw new CounterLedgerException(s"Code $x is not a known tx code.")
    }
  }


  implicit class PrintoutFromBytes(bytes: Array[Byte]) {
    def toPrintout: Printout = {
      val (code, unique) = bytes.extract(ByteDeSerialize, LongDeSerialize)
      require(code == PrintoutCode, s"Wrong leading byte for PrintoutCode ${bytes.head} instead of $PrintoutCode")
      new Printout() {
        override val uniqueMessage: Long = unique
      }
    }
  }

  implicit class PrintoutToBytes(obj: Printout) {
    def toBytes: Array[Byte] = (ByteSerializer(PrintoutCode) ++ LongSerializer(obj.uniqueMessage)).toBytes
  }

  implicit class FailOnTheseNodesFromBytes(bytes: Array[Byte]) {
    def toFailOnTheseNodes: FailOnTheseNodes = {
      val (code, unique, nodes) = bytes.extract(ByteDeSerialize, LongDeSerialize, SequenceDeSerialize(_.extract(StringDeSerialize)))
      require(code == FailOnTheseNodesCode, s"Wrong leading byte for FailOnTheseNodesCode ${bytes.head} instead of $FailOnTheseNodesCode")
      new FailOnTheseNodes(nodes) {
        override val uniqueMessage: Long = unique
      }
    }
  }

  implicit class FailOnTheseNodesToBytes(obj: FailOnTheseNodes) {
    def toBytes: Array[Byte] =
      (ByteSerializer(FailOnTheseNodesCode) ++
      LongSerializer(obj.uniqueMessage) ++
      SequenceSerializer(obj.nodes.map(StringSerializer))).toBytes
  }

  implicit class IncrementFailureFromBytes(bytes: Array[Byte]) {
    def toIncrementFailure: IncrementFailure = {
      val (code, unique) = bytes.extract(ByteDeSerialize, LongDeSerialize)
      require(code == IncrementFailureCode, s"Wrong leading byte for IncrementFailureCode ${bytes.head} instead of $IncrementFailureCode")
      new IncrementFailure() {
        override val uniqueMessage: Long = unique
      }
    }
  }

  implicit class IncrementFailureToBytes(obj: IncrementFailure) {
    def toBytes: Array[Byte] = ByteSerializer(IncrementFailureCode) ++ LongSerializer(obj.uniqueMessage).toBytes
  }

}
