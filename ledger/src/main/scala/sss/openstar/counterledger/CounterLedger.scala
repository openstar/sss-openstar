package sss.openstar.counterledger

import sss.ancillary.Logging
import sss.db.ops.DbOps.DbRunOps
import sss.db.{Db, FutureTx}
import sss.openstar.common.block
import sss.openstar.counterledger.CounterLedger.CounterLedgerTableNames
import sss.openstar.ledger.LedgerOps.TryLedgerResultToTryApplied
import sss.openstar.ledger.{Ledger, LedgerId, LedgerResult, LedgerResultFTx, ToTxEntryWithSignatures, TxSeqApplied}
import sss.openstar.schemamigration.SqlSchemaNames
import sss.openstar.schemamigration.SqlSchemaNames.ColumnNames.{failureCol, successCol}
import sss.openstar.schemamigration.dynamic.GlobalTableNameTag
import sss.openstar.{hash, ledger}

import scala.util.{Failure, Success, Try}

class CounterLedger(tableNames: CounterLedgerTableNames, currentNodeIdentity: String)
                   (implicit val ledgerId: LedgerId, db: Db) extends Ledger with Logging {

  import tableNames._
  private val table = db.table(counterLedgerTableName)
  private val successView = db.view(successViewName)
  private val failureView = db.view(failureViewName)

  private def incrementSuccessOrFail(nodes: Seq[String]): FutureTx[Unit] = if (!nodes.contains(currentNodeIdentity)) {
    for {
      result <- increment(Map(successCol -> 1))
    } yield result
  } else {
    FutureTx.failed(new IllegalStateException("Simulated exception!"))
  }

  private def incrementFailure(): FutureTx[Unit] = for {
    result <- increment(Map(failureCol -> 1))
  } yield result

  private def increment(values: Map[String, Int]): FutureTx[Unit] = table.persist(values).map(_ => ())

  def successCounter(): FutureTx[Long] = successView.count
  def failureCounter(): FutureTx[Long] = failureView.count

  override def applyFTx(
                         ledgerItem: ledger.LedgerItem,
                         blockId: block.BlockId,
                         txSeqApplied: TxSeqApplied): LedgerResultFTx = {
    val txId = ledgerItem.txId
    ledgerItem.txEntryBytes.toSignedTxEntry.txEntryBytes.toCounterLedgerTx match {
      case _@FailOnTheseNodes(nodes) =>
        incrementSuccessOrFail(nodes).map(_ => Right(CounterTxApplied(txId)))
      case _ @ IncrementFailure() =>
        incrementFailure().map(_ => Right(CounterTxApplied(txId)))
      case _ @ Printout() =>
        FutureTx.sequence(Seq(successCounter(), failureCounter())).map {
          case Seq(successes, failures) if Math.floorMod(successes, 250) == 0 =>
            log.info(s"Counter ledger $ledgerId printout: success $successes, failure $failures (every 250)")
          case _ =>
        }.map(_ => Right(CounterTxApplied(txId)))

    }
  }

  override def apply(
                      ledgerItem: ledger.LedgerItem,
                      blockId: block.BlockId,
                      txSeqApplied: TxSeqApplied): LedgerResult = applyFTx(ledgerItem, blockId, txSeqApplied).dbRunSync.unwrap
  override def importState(height: Long): Try[Unit] = Success(())
  override def deleteCheckpoint(height: Long): Try[Unit] = Success(())
  override def createCheckpoint(id: Long): Try[hash.Digest32] = Failure(new RuntimeException("Counter ledger not works with checkpoints"))
}

object CounterLedger {
  case class CounterLedgerTableNames(counterLedgerTableName: String, successViewName: String, failureViewName: String)

  def apply(currentNodeIdentity: String)(implicit ledgerId: LedgerId, db: Db, globalTableNameTag: GlobalTableNameTag): CounterLedger = {
    val taggedTableNames = new SqlSchemaNames.TaggedTableNames()
    import taggedTableNames._
    new CounterLedger(CounterLedgerTableNames(counterLedgerTableName, successViewName, failureViewName), currentNodeIdentity)
  }
}
