import sbt.Keys.{libraryDependencies, pomIncludeRepository}

lazy val It = config("It") extend Test

lazy val commonSettings = Seq(
  organization := "com.mcsherrylabs",
  publishMavenStyle := true,
  pomIncludeRepository := { _ => false },
  scalaVersion := "2.13.10",
  // the GITLAB CI build will successfully get this value
  version := sys.env.getOrElse("CI_COMMIT_TAG", Vers.fallbackVersionToPublish),
  javacOptions ++= Seq("-source", "11", "-target", "11"),
  scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature", "-Xfatal-warnings" ),
  Test / testOptions := Seq(Tests.Argument("-l", "org.scalatest.tags.Slow")),
  Compile / doc / scalacOptions ++= Seq(
    "-no-link-warnings" // Suppresses problems with Scaladoc @throws links
  ),
  publishTo := Some {
    val sonaUrl = "https://oss.sonatype.org/"
    if (isSnapshot.value)
      "snapshots" at sonaUrl + "content/repositories/snapshots"
    else
      "releases" at sonaUrl + "service/local/staging/deploy/maven2"
  },
  credentials += sys.env.get("SONA_USER").map(userName => Credentials(
    "Sonatype Nexus Repository Manager",
    "oss.sonatype.org",
    userName,
    sys.env.getOrElse("SONA_PASS", ""))
  ).getOrElse(
    Credentials(Path.userHome / ".ivy2" / ".credentials")
  ),
  Test / packageBin / publishArtifact := true, //Not required for *this* repo but could be required for others.
  pomExtra := pomExtraXml,
  versionScheme := Some("early-semver"),
  updateOptions := updateOptions.value.withGigahorse(false),
  updateOptions := updateOptions.value.withLatestSnapshots(false),
  libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.14" % Test,
  libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.17.0" % Test,
  libraryDependencies += "org.scalatestplus" %% "scalacheck-1-17" % "3.2.14.0" % Test
)


lazy val openstar = (project in file("."))
  .aggregate(schema_migration, common, network, ledger, node_core, node_extra, node_rpc_client, http_seed_config, node_template)

lazy val http_seed_config = (project in file("http-seed-config"))
  .settings(commonSettings)
  .dependsOn(node_core)

lazy val schema_migration = (project in file("schema-migration"))
  .settings(commonSettings)

lazy val common  = (project in file("common"))
  .settings(commonSettings)
  .dependsOn(schema_migration)

lazy val network = (project in file("network"))
  .settings(commonSettings)
  .dependsOn(common % "compile->compile;test->test")

lazy val ledger = (project in file("ledger"))
  .settings(commonSettings)
  .dependsOn(common % "compile->compile;test->test")

lazy val node_core = (project in file("node/node-core"))
  .settings(commonSettings)
  .dependsOn(common % "compile->compile;test->test", network, ledger % "compile->compile;test->test", node_rpc_client % "compile->compile")

lazy val node_extra = (project in file("node/node-extra"))
  .settings(commonSettings)
  .dependsOn(common % "compile->compile;test->test", node_core % "compile->compile;test->test", network, ledger % "compile->compile;test->test", node_rpc_client % "compile->compile")

lazy val node_rpc_client = (project in file("node/node-rpc-client"))
  .settings(commonSettings)

lazy val node_template = (project in file("node/node-template"))
  .configs(It)
  .settings(
    inConfig(It)(Defaults.testTasks),
    It / testOptions := Seq(Tests.Argument("-n", "org.scalatest.tags.Slow"))
  )
  .settings(commonSettings)
  .dependsOn(common % "compile->compile;test->test", node_extra % "compile->compile;test->test", node_core % "compile->compile;test->test" , node_rpc_client)


val pomExtraXml = (
  <url>https://gitlab.com/openstar/sss-openstar</url>
    <licenses>
      <license>
        <name>Apache 2</name>
        <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
        <distribution>repo</distribution>
      </license>
    </licenses>
    <scm>
      <url>https://gitlab.com/openstar/sss-openstar</url>
      <connection>scm:git@gitlab.com:openstar/sss-openstar.git</connection>
    </scm>
    <developers>
      <developer>
        <id>mcsherrylabs</id>
        <name>Alan McSherry</name>
        <url>http://mcsherrylabs.com</url>
      </developer>
    </developers>)
