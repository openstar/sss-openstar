name := "openstar-network"

Test / parallelExecution := false

libraryDependencies += "com.typesafe.akka" %% "akka-testkit"  % Vers.akkaVer % Test

libraryDependencies += "org.scorexfoundation" %% "scrypto" % Vers.scryptoVer

libraryDependencies += "com.mcsherrylabs" %% "sss-ancillary" % Vers.ancillaryVer

libraryDependencies += "com.typesafe.akka" %% "akka-actor" %  Vers.akkaVer

libraryDependencies += "org.bitlet" % "weupnp" % "0.1.+"

libraryDependencies += "ch.qos.logback" % "logback-classic" % Vers.logbackClassicVersion % "runtime"

// https://mvnrepository.com/artifact/com.typesafe.akka/akka-slf4j_2.11
libraryDependencies += "com.typesafe.akka" %% "akka-slf4j" % Vers.akkaVer


