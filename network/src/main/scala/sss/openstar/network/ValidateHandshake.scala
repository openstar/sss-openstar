package sss.openstar.network

import java.net.InetSocketAddress
import java.security.SecureRandom
import java.util.Base64
import akka.util.ByteString
import scorex.crypto.signatures.Signature
import sss.ancillary.Logging
import sss.openstar.network.ConnectionHandler._
import sss.ancillary.Results._
import sss.openstar.hash.SecureCryptographicHash

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

trait IdentityVerification {
  val nodeId: String
  val tag: String

  def sign(msg: Array[Byte]): Future[Array[Byte]]

  def verify(sig: Signature,
             msg: Array[Byte],
             nodeId: String,
             tag: String): OkResult
}

object ValidateHandshake {
  def apply(netInf: NetworkInterface, handshakeVerifier: IdentityVerification)(
    local: InetSocketAddress, remote: InetSocketAddress)(implicit executionContext: ExecutionContext): HandshakeStep = {
    //NOTE new myNonce with every new 'remote'
    new ValidateHandshake(local, remote, netInf, handshakeVerifier).initialStep()
  }
}

class ValidateHandshake(local: InetSocketAddress,
                        remote: InetSocketAddress,
                        netInf: NetworkInterface,
                        handshakeVerifier: IdentityVerification)(implicit ec :ExecutionContext)
  extends Logging {

  private lazy val myNonce = new SecureRandom().nextLong()
  private lazy val myChallengeHandshake =
    ByteString(createHandshake(myNonce).bytes)

  def initialStep(): HandshakeStep = {
    BytesToSend(myChallengeHandshake, WaitForBytes(processReturnedHandshake))
  }

  @volatile
  private var hisHandshakeHasBeenSigned: Option[NodeId] = None
  @volatile
  private var ourHandshakeHasBeenCorrectlySignedByHim: Boolean = false


  private def processReturnedHandshake(data: ByteString): HandshakeStep = {

    log.debug(s"New data -> " +
      s"ourHandshakeHasBeenCorrectlySignedByHim=$ourHandshakeHasBeenCorrectlySignedByHim, " +
      s"hisHandshakeHasBeenSigned=$hisHandshakeHasBeenSigned data len ${data.size}"
    )

      Handshake.parse(data.toArray) match {
        case Success((shake, rest)) if shake.nodeId == handshakeVerifier.nodeId =>
          log.info(s"Cannot connect to self (${shake.nodeId})")
          RejectConnection
        case Success((shake, rest)) =>
          if (shake.fromNonce == myNonce) {
            // this is my nonce returned, check he signed it correctly.
            val handshakeGood: OkResult = verifyHandshake(shake)
            if (handshakeGood.isOk) {

              logDelay(shake)
              ourHandshakeHasBeenCorrectlySignedByHim = true

              if (hisHandshakeHasBeenSigned.isDefined) {
                  ConnectionEstablished(hisHandshakeHasBeenSigned.get, rest)
              } else if (rest.nonEmpty) {
                 processReturnedHandshake(rest)
              } else {
                WaitForBytes(processReturnedHandshake)
              }

            } else {
              handshakeGood.errors.foreach(log.warn(_))
              log.info(s"Got a bad handshake from ${remote}, closing.")
              RejectConnection
            }

        } else {
          val signedHandshakeBytes = signHisHandshake(shake)
          hisHandshakeHasBeenSigned = Option(NodeId(shake.nodeId, remote))

          val nextStep =
            if (ourHandshakeHasBeenCorrectlySignedByHim) {
              ConnectionEstablished(hisHandshakeHasBeenSigned.get, rest)
            } else if (rest.nonEmpty) {
              processReturnedHandshake(rest)
            } else {
                WaitForBytes(processReturnedHandshake)
            }
          log.debug(s"Sending back his handshake correctly signed, ourHandshakeHasBeenCorrectlySignedByHim $ourHandshakeHasBeenCorrectlySignedByHim")
          BytesToSendF(signedHandshakeBytes, nextStep)
        }

        case Failure(e) =>
          log.debug(data.toArray.map(_.toInt).mkString(",") + s"<- Bad bytes, our handshake has been signed $ourHandshakeHasBeenCorrectlySignedByHim, his hankdshake has been signed $hisHandshakeHasBeenSigned")
          log.debug(s"Error parsing a handshake", e)
          RejectConnection
      }
  }

  private def logDelay(shake: Handshake): Unit = log.whenDebugEnabled {
    val delay = currentTimeInSeconds - shake.time
    log.debug(
      s"Got a Handshake from ${shake.nodeId} (${remote}), delay in s is $delay")
  }

  private def currentTimeInSeconds: Long = System.currentTimeMillis() / 1000

  private def verifyHandshake(shake: Handshake) = {
    val nowPlus = currentTimeInSeconds + 5
    val nowMinus = nowPlus - 10
    lazy val shakeHash = SecureCryptographicHash.hash(shake.bytesMinusSig)
    (shake.time < nowPlus) orErrMsg s"Time too far in future ${shake.time - nowPlus} ms" andThen {
      (shake.time > nowMinus) orErrMsg s"Time too far in past ${nowMinus - shake.time} ms" andThen {
        (shake.host == remote) orErrMsg s"Spoof! Actual remote is $remote, handshake contains ${shake.host}" andThen
          handshakeVerifier.verify(Signature(shake.sig.toArray),
            shakeHash,
            shake.nodeId,
            shake.tag) andThen
          isApplicationNameCorrect(shake.applicationName) andThen
          isApplicationVersionCompatible(shake.applicationVersion)
      }
    }
  }

  private def logShakeSig(signedShake: Handshake) = {
    log.whenDebugEnabled {
      val sigStr = Base64.getEncoder.encodeToString(signedShake.sig.toArray)
      log.debug(
        s"Signing ${signedShake.fromNonce} ${signedShake.nodeId}, ${signedShake.tag}, ${sigStr}")
    }
  }

  private[network] def isApplicationNameCorrect(thatAppName: String): OkResult = {
    (netInf.appName == thatAppName).orErrMsg(
      s"Chain names not matching ${thatAppName} != ${netInf.appName}")
  }

  private[network] def isApplicationVersionCompatible(
                                                       thatAppVer: ApplicationVersion): OkResult = {
    (thatAppVer.firstDigit == netInf.appVersion.firstDigit)
      .orErrMsg(
        s"First digits not matching ${thatAppVer.firstDigit} != ${netInf.appVersion.firstDigit}") andThen
      (thatAppVer.secondDigit == netInf.appVersion.secondDigit)
        .orErrMsg(
          s"Second digits not matching ${thatAppVer.secondDigit} != ${netInf.appVersion.secondDigit}")
  }

  private def createHandshake(nonce: Long,
                              mySig: ByteString = ByteString()): Handshake = {
    handshakeTemplate.copy(fromNonce = nonce,
      sig = mySig,
      time = currentTimeInSeconds)
  }

  private def signHisHandshake(shake: Handshake)(implicit ec: ExecutionContext): Future[ByteString] = {
    val shakeWithNonce =
      createHandshake(shake.fromNonce)

    val hashToSign = SecureCryptographicHash.hash(shakeWithNonce.bytesMinusSig)

    handshakeVerifier.sign(hashToSign) map { mySig =>
      val signedShake = shakeWithNonce.copy(sig = ByteString(mySig))
      logShakeSig(signedShake)
      ByteString(signedShake.bytes)
    }

  }

  private lazy val handshakeTemplate = Handshake(
    netInf.appName,
    netInf.appVersion,
    handshakeVerifier.nodeId,
    handshakeVerifier.tag,
    new InetSocketAddress(
      netInf.ownSocketAddress.getHostName,
      local.getPort
    ),
    new SecureRandom().nextLong,
    ByteString(),
    0 // it's a template, no point in using real time.
  )

}
