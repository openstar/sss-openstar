package sss.openstar.network

import java.net.InetAddress

import org.bitlet.weupnp.{GatewayDevice, GatewayDiscover}
import sss.ancillary.Logging

import scala.jdk.CollectionConverters._
import scala.util.{Failure, Success, Try}

trait UPnPSettings {
  val upnpGatewayTimeoutOpt: Option[Int]
  val upnpDiscoverTimeoutOpt: Option[Int]
}

class UPnP(settings:UPnPSettings) extends Logging {


  private def fail(msg: String) = throw new RuntimeException(msg)

  lazy val localAddress = gateway.map(_.getLocalAddress)
  lazy val externalAddress = gateway.map(_.getExternalIPAddress).map(InetAddress.getByName)

  private lazy val gateway: Option[GatewayDevice] = Try {
    log.info("Looking for UPnP gateway device...")
    val defaultHttpReadTimeout = settings.upnpGatewayTimeoutOpt.getOrElse(GatewayDevice.getHttpReadTimeout)
    GatewayDevice.setHttpReadTimeout(defaultHttpReadTimeout)
    val discover = new GatewayDiscover()
    val defaultDiscoverTimeout = settings.upnpDiscoverTimeoutOpt.getOrElse(discover.getTimeout)
    discover.setTimeout(defaultDiscoverTimeout)

    val gatewayMap = Option(discover.discover.asScala).map(_.toMap).getOrElse(Map.empty)
    gatewayMap.foreach { case (addr, _) =>
      log.debug("UPnP gateway device found on " + addr.getHostAddress)
    }

    Option(discover.getValidGateway) match {
      case None => fail("There is no connected UPnP gateway device")
      case Some(device) =>
        log.info("Using UPnP gateway device on " + device.getFriendlyName)
        log.info("External IP address is " + device.getExternalIPAddress)
        device
    }
  } match {
    case Failure(t) =>
      log.error("Unable to discover UPnP gateway devices: " + t.toString)
      None
    case Success(device) =>
      Some(device)
  }

  def addPort(port: Int): Try[Unit] = Try {
    //todo: precise app name + version
    gateway.getOrElse(fail("Can't add port, No uPnp gateway found"))
    if (gateway.get.addPortMapping(port, port, localAddress.get.getHostAddress, "TCP", "Openstar")) {
      log.info("Mapped port [" + externalAddress.get.getHostAddress + "]:" + port)
    } else {
      log.info("Unable to map port " + port)
    }
  }.recover { case t: Throwable =>
    log.error("Unable to map port " + port + ": " + t.toString)
  }

  def deletePort(port: Int): Try[Unit] = Try {
    gateway.getOrElse(fail("Can't remove port, No uPnp gateway found "))
    if (gateway.get.deletePortMapping(port, "TCP")) {
      log.info("Mapping deleted for port " + port)
    } else {
      log.warn("Unable to delete mapping for port " + port)
    }
  }.recover { case t: Throwable =>
    log.error("Unable to delete mapping for port " + port + ": " + t.toString)
  }
}