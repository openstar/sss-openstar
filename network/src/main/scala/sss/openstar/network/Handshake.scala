package sss.openstar.network

import java.net.InetSocketAddress

import akka.util.ByteString
import sss.ancillary.Logging
import sss.ancillary.Serialize._
import sss.openstar.util.SerializeByteString.{ByteStringDeSerialize, ByteStringSerializer}
import sss.openstar.util.SerializeInetAddress._
import sss.openstar.util.serialize.InetSocketAddressSerializer

import scala.util.Try

//TODO ADD PORT AND IP BACK MITM
case class Handshake(applicationName: String,
                     applicationVersion: ApplicationVersion,
                     nodeId: String,
                     tag: String,
                     host: InetSocketAddress,
                     fromNonce: Long,
                     sig: ByteString,
                     time: Long) {

  require(nodeId.nonEmpty, "Handshake node id cannot be an empty string.")

  lazy val byteString: ByteString = ByteString(bytes)

  lazy val bytesMinusSig: Array[Byte] = {
    StringSerializer(applicationName) ++
      ByteArraySerializer(applicationVersion.bytes) ++
      StringSerializer(nodeId) ++
      StringSerializer(tag) ++
      ByteArraySerializer(host.toBytes) ++
      LongSerializer(fromNonce) ++
      LongSerializer(time).toBytes
  }

  lazy val bytes: Array[Byte] = {
    StringSerializer(applicationName) ++
      ByteArraySerializer(applicationVersion.bytes) ++
      StringSerializer(nodeId) ++
      StringSerializer(tag) ++
      ByteArraySerializer(host.toBytes) ++
      LongSerializer(fromNonce) ++
      ByteStringSerializer(sig) ++
      LongSerializer(time).toBytes
  }
}

object Handshake extends Logging {
  def parse(bytes: Array[Byte]): Try[(Handshake, ByteString)] =
    Try {

      val extracted = bytes.extract(
        StringDeSerialize,
        ByteArrayDeSerialize(ApplicationVersion.parse),
        StringDeSerialize,
        StringDeSerialize,
        ByteArrayDeSerialize(InetSocketAddressSerializer.fromBytes),
        LongDeSerialize,
        ByteStringDeSerialize,
        LongDeSerialize,
        ByteArrayRawDeSerialize
      )

      (Handshake(
        extracted._1,
        extracted._2,
        extracted._3,
        extracted._4,
        extracted._5,
        extracted._6,
        extracted._7,
        extracted._8
      ), ByteString(extracted._9))
    }
}
