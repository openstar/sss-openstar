package sss.openstar.network

import sss.openstar.UniqueNodeIdentifier

trait NetConnect {
  def connect(nId: NodeId,reconnectStrategy: ReconnectionStrategy): Unit
  def disconnect(nodeId: UniqueNodeIdentifier): Unit
  def apply(nId: NodeId,reconnectStrategy: ReconnectionStrategy): Unit = connect(nId, reconnectStrategy)
}

