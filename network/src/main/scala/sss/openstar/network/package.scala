package sss.openstar

import java.net.{InetAddress, InetSocketAddress}

import akka.util.ByteString
import sss.openstar.chains.Chains.GlobalChainIdMask
import sss.openstar.network.ConnectionHandler.HandshakeStep


package object network {

  type ReconnectionStrategy = LazyList[Int]

  val NoReconnectionStrategy: ReconnectionStrategy = LazyList.empty

  final case class NodeId(id: String, inetSocketAddress: InetSocketAddress) {

    require(Option(inetSocketAddress.getAddress).isDefined, "Cannot provide an InetSocketAddress without an IP address")
    require(inetSocketAddress.getPort > 0, "Cannot provide an InetSocketAddress without a port")

    def isSameId(nodeId: NodeId) = id == nodeId.id

    def isSameAddress(inetSocketAddress: InetSocketAddress) =
      inetSocketAddress.getAddress == address


    def isSameAddress(nId: NodeId) =
      nId.address == address

    def isSameAddress(inetAddress: InetAddress) =
      address == inetAddress

    val address = inetSocketAddress.getAddress

    override def toString: String = {
      s"NodeId id:$id, address: $address (!=port:${inetSocketAddress.getPort})"
    }

    lazy val hash: ByteString = ByteString(inetSocketAddress.getAddress.getAddress) ++
      ByteString(inetSocketAddress.getPort) ++
      ByteString(id.hashCode)
  }

  type InitialHandshakeStepGenerator = (InetSocketAddress,InetSocketAddress) => HandshakeStep


  final case class PublishConnections() extends BusEvent

  final case class ConnectionLost(nodeId: UniqueNodeIdentifier) extends BusEvent

  final case class Connections(connections: Seq[UniqueNodeIdentifier]) extends BusEvent

  final case class Connection(nodeId: UniqueNodeIdentifier) extends BusEvent

  final case class ConnectionHandshakeTimeout(remote: InetSocketAddress)
      extends BusEvent

  final case class ConnectionFailed(remote: InetSocketAddress,
                                    cause: Option[Throwable])
      extends BusEvent

  final case class ConnectionRejected(remote: InetSocketAddress)
    extends BusEvent

  final case class IncomingSerializedMessage(
      fromNodeId: UniqueNodeIdentifier,
      msg: SerializedMessage
  )

  object SerializedMessage {

    implicit val noChain: GlobalChainIdMask = 0.toByte

    def apply(msgCode: Byte)(implicit chainId: GlobalChainIdMask): SerializedMessage =
      SerializedMessage(chainId, msgCode, Array.emptyByteArray)

    def apply[T](msgCode: Byte, obj: T)(implicit ev: T => Array[Byte], chainId: GlobalChainIdMask): SerializedMessage =
      SerializedMessage(chainId, msgCode, ev(obj))
  }

  val heartbeatCode: Byte = 25

  final case class SerializedMessage (
                                                        chainId: GlobalChainIdMask,
                                                        msgCode: Byte,
                                                        data: Array[Byte])


  def indefiniteReconnectionStrategy(delaysInSeconds: Int): ReconnectionStrategy =
    LazyList.continually(delaysInSeconds)

  def defaultReconnectionStrategy: ReconnectionStrategy = {
    (Seq.fill(10)(2) ++ Seq.fill(10)(10) ++ Seq.fill(10)(30)).to(LazyList) ++  LazyList.continually(60 * 5)
  }

  def reconnectionStrategy(delaysInSeconds: Int*): ReconnectionStrategy =
    delaysInSeconds.to(LazyList)

}
