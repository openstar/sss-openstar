package sss.openstar.network

import akka.actor.{Actor, ActorContext, ActorLogging, ActorRef, Props}
import sss.openstar.chains.Chains.GlobalChainIdMask
import sss.openstar.network.NetworkControllerActor.BlackListAddress

import java.net.InetAddress
import java.util.concurrent.TimeUnit
import scala.collection.mutable
import scala.concurrent.duration.Duration

case class Attempts(firstAttemptTimestampMillis: Long, failedCount: Int) {
  def increment: Attempts = copy(failedCount = failedCount + 1)
}

object Attempts {
  def firstAttempt: Attempts = Attempts(System.currentTimeMillis(), 1)
}

class ConnectionGuardActor(threshold: Int, thresholdDuration: Duration, banDuration: Duration)
                          (implicit events: MessageEventBus, networkRef: NetworkRef) extends Actor with ActorLogging {
  private val failedConnections: mutable.Map[InetAddress, Attempts] = mutable.Map.empty

  log.info("Connection guard started!")

  override def preStart(): Unit = {
    super.preStart()
    events.subscribe(classOf[ConnectionRejected])
    events.subscribe(classOf[ConnectionHandshakeTimeout])
  }

  override def receive: Receive = {
    case ConnectionRejected(address) =>
      log.info(s"Connection rejected for $address")
      banIfNecessary(address.getAddress)
    case ConnectionHandshakeTimeout(address) =>
      log.info(s"Connection handshake timeout for $address")
      banIfNecessary(address.getAddress)
    case msg =>
      log.warning(s"ConnectionGuardActor can not handle $msg")
  }

  private def banIfNecessary(remote: InetAddress): Unit = {
    val attempt = updateAndGetAttempts(remote)
    val thresholdClosed = attempt.firstAttemptTimestampMillis + thresholdDuration.toMillis < System.currentTimeMillis()
    if (thresholdClosed) {
      log.debug("Ignore previous failed attempts...")
      failedConnections.remove(remote)
    } else {
      val failedCount = attempt.failedCount
      if (failedCount > threshold) {
        log.warning(s"Ban $remote because of $failedCount failed attempts")
        failedConnections.remove(remote)
        networkRef.blacklist(remote, banDuration)
      } else {
        log.debug(s"Connection to $remote failed already $failedCount times, waiting...")
      }
    }
  }

  private def updateAndGetAttempts(address: InetAddress): Attempts = {
    if (failedConnections.contains(address)) {
      val attempt = failedConnections(address).increment
      failedConnections.update(address, attempt)
      attempt
    } else {
      val attempt = Attempts.firstAttempt
      failedConnections.put(address, attempt)
      attempt
    }
  }

}

object ConnectionGuardActor {
  def apply(threshold: Int, thresholdDuration: Duration, banDuration: Duration)
           (implicit actorSystem: ActorContext, chainId: GlobalChainIdMask, messageRouter: MessageEventBus, networkRef: NetworkRef): ActorRef = {
    actorSystem.actorOf(
      Props(classOf[ConnectionGuardActor], threshold, thresholdDuration, banDuration, messageRouter, networkRef),
      s"ConnectionGuardActor_$chainId"
    )
  }
}
