package sss.openstar.network

import com.typesafe.config.Config

import java.net.{InetAddress, InetSocketAddress}
import sss.ancillary.Logging
import sss.openstar.network.NetworkInterface.BindControllerSettings

import scala.concurrent.duration._
import scala.util.Try

/**
  * Created by alan on 3/31/16.
  */
class NetworkInterface(settings: BindControllerSettings, upnp: Option[UPnP], findMyIp: () => Option[String]) {

  val appVersion = ApplicationVersion(settings.appVersion)
  val appName = settings.applicationName
  val handshakeTimeoutMs = settings.handshakeTimeoutMs
  val connTimeout = new FiniteDuration(settings.connectionTimeout, SECONDS)
  val maxNumConnections = settings.maxNumConnections
  val writeBufferSizeInBytes: Int = settings.writeBufferSizeInBytes

  val lowestClientPortNumber: Int = settings.lowestClientPortNumber
  val highestClientPortNumber: Int = settings.highestClientPortNumber

  //check own declared address for validity
  // SCALEWAY IP not in list of interfaces.
  //require(NetworkInterface.isAddressValid(settings.declaredAddressOpt), upnp)

  private lazy val externalSocketAddress = settings.declaredAddressOpt
    .flatMap(s => Try(InetAddress.getByName(s)).toOption)
    .orElse(upnp.flatMap(_.externalAddress))
    .orElse(
      findMyIp().flatMap(s => Try(InetAddress.getByName(s)).toOption)
    )
    .map(ia => new InetSocketAddress(ia, settings.port.toInt))

  //an address to send to peers
  lazy val ownSocketAddress =
    externalSocketAddress.getOrElse(localAddress)

  lazy val localAddress = new InetSocketAddress(
    InetAddress.getByName(settings.bindAddress),
    settings.port.toInt)

}

object NetworkInterface extends Logging {

  object BindControllerSettings {
    def apply(conf: Config) : BindControllerSettings = {
      val bindConfig = conf.getConfig("bind")

      def getStringOr(name: String, defaultValue: String): String = {
        if(bindConfig.hasPath(name)) {
          bindConfig.getString(name)
        } else defaultValue
      }

      def getIntOr(name: String, defaultValue: Int): Int = {
        if(bindConfig.hasPath(name)) {
          bindConfig.getInt(name)
        } else defaultValue
      }

      def getStringIfExists(name: String): Option[String] = {
        if(bindConfig.hasPath(name)) {
          Some(bindConfig.getString(name))
        } else None
      }

      BindControllerSettings(
        applicationName = bindConfig.getString("applicationName"),
        bindAddress = getStringOr("bindAddress", "0.0.0.0"),
        // Overriding PORT on commandline is causnig problems as it's a string
        port = getStringOr("port", "8084"),
        declaredAddressOpt = getStringIfExists("declaredAddress"),
        handshakeTimeoutMs = getIntOr("handshakeTimeoutMs", 5000),
        connectionTimeout = getIntOr("connectionTimeout", 60),
        heartbeatIntervalms = bindConfig.getInt("heartbeatIntervalms"),
        heartbeatTimeoutms = bindConfig.getInt("heartbeatTimeoutms"),
        writeBufferSizeInBytes = getIntOr("writeBufferSizeInBytes", 10000000),
        maxNumConnections = bindConfig.getInt("maxNumConnections"),
        appVersion = bindConfig.getString("appVersion"),
        lowestClientPortNumber = getIntOr("lowestClientPortNumber", 50000),
        highestClientPortNumber =getIntOr("highestClientPortNumber", 60000),
      )
    }
  }

  case class BindControllerSettings(
    applicationName: String,
    heartbeatIntervalms: Int,
    heartbeatTimeoutms: Int,
    maxNumConnections: Int,
    appVersion: String,
    bindAddress: String = "0.0.0.0",
    // Overriding PORT on commandline is causnig problems as it's a string
    port: String = "8084",
    declaredAddressOpt: Option[String] = None,
    handshakeTimeoutMs: Int = 5000,
    connectionTimeout: Int = 60,
    writeBufferSizeInBytes: Int = 10000000,
    lowestClientPortNumber: Int = 50000,
    highestClientPortNumber: Int = 60000)

  /*def isAddressValid(declaredAddress: Option[String],
                     upnpOpt: Option[UPnP] = None): Boolean = {
    //check own declared address for validity

    declaredAddress
      .map { myAddress =>
        Try {

          val myAddrs = InetAddress.getAllByName(myAddress)

          JNetworkInterface.getNetworkInterfaces.asScala.exists { intf =>
            intf.getInterfaceAddresses.asScala.exists { intfAddr =>
              val extAddr = intfAddr.getAddress
              myAddrs.contains(extAddr)
            }
          } match {
            case true => true
            case false =>
              upnpOpt
                .map { upnp =>
                  val extAddr = upnp.externalAddress
                  myAddrs.contains(extAddr)
                }
                .getOrElse(false)
          }
        }.recover {
          case t: Throwable =>
            log.error("Declared address validation failed: ", t)
            false
        }
          .getOrElse(false)
      }
      .getOrElse(true)
      .ensuring(_ == true, "Declared address isn't valid")
  }*/

}
