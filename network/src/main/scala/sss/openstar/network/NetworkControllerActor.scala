package sss.openstar.network

import java.net.{InetAddress, InetSocketAddress}
import akka.actor.{Actor, _}
import akka.io.Tcp._
import akka.io.{IO, Tcp}
import sss.openstar.{BusEvent, UniqueNodeIdentifier}
import sss.openstar.network.ConnectionHandler.{Begin, ConnectionRef, HeartbeatConfig}
import sss.openstar.network.NetworkControllerActor._

import scala.concurrent.Promise
import scala.concurrent.duration._
import scala.util.{Failure, Random}

private[network] object NetworkControllerActor {

  case class StartNet(waitForStart: Promise[Unit])

  case class Scheduled(c: ConnectTo)

  case class SendToNodeId(msg: SerializedMessage, nId: UniqueNodeIdentifier)

  case class ConnectTo(nodeId: NodeId,
                       reconnectionStrategy: ReconnectionStrategy)

  private[network] case class UnBlackListAddress(inetAddress: InetAddress) extends BusEvent

  private[network] case class UnBlackListById(nodeId: UniqueNodeIdentifier) extends BusEvent

  case class Disconnect(nodeId: UniqueNodeIdentifier)

  private[network] case class BlackListAddress(inetAddress: InetAddress, duration: Duration) extends BusEvent

  private[network] case class BlackListById(id: UniqueNodeIdentifier, duration: Duration) extends BusEvent

  case object ShutdownNetwork


  private case class ConnectionInProgress(nodeId: NodeId,
                                          handlerRefOpt: Option[ActorRef])

}

private class NetworkControllerActor(

                                      loopbackId: UniqueNodeIdentifier,
                                      netInf: NetworkInterface,
                                      initialStep: InitialHandshakeStepGenerator,
                                      stopPromise: Promise[Unit],
                                      events: MessageEventBus,
                                      heartbeatConfig: HeartbeatConfig
                                    )
  extends Actor
    with ActorLogging {

  import context.{dispatcher, system}


  private var connections = Set[ConnectionRef]()
  private var connectionsInProgress = Seq[ConnectionInProgress]()

  private var strategies = Map[UniqueNodeIdentifier, (InetSocketAddress, ReconnectionStrategy)]()
  private var scheduledConnect: List[UniqueNodeIdentifier] = List.empty

  private val blackList = new BlackListHolder()

  override def preStart(): Unit = {
    super.preStart()
    events.subscribe(classOf[PublishConnections])
  }



  private def nextClientAddress: Option[InetSocketAddress] = {
    val tryThisport = Random.between(netInf.lowestClientPortNumber, netInf.highestClientPortNumber)
    Some(new InetSocketAddress(netInf.localAddress.getAddress, tryThisport))
  }

  private def logEarlyConnect: Receive = {
    case c: ConnectTo =>
      log.error(s"Cannot connect yet! $c")
  }

  private def waitForStart: Actor.Receive = logEarlyConnect orElse {

    case StartNet(waiter) =>
      IO(Tcp) ! Bind(self, netInf.localAddress, pullMode = true)
      context.become(waitForBind(waiter))
  }

  private def waitForBind(waiter: Promise[Unit]): Actor.Receive = logEarlyConnect orElse {

    case b@Bound(localAddr) =>
      log.debug("Successfully bound to " + localAddr)
      context.become(manageNetwork(sender()))
      sender() ! ResumeAccepting(1)
      waiter.success(())

  }

  private def manageNetwork(bindConnectionActor: ActorRef): Actor.Receive = {

    case b@Unbound =>
      log.debug("Successfully unbound, stopped the network.")
      context stop self
      stopPromise.success(())

    case c@CommandFailed(b: Bind) =>
      stopPromise.complete(
        Failure(new RuntimeException(
          s"Network port ${b.localAddress} already in use? (Error:${c.cause})"))
      )

    case BlackListById(id, duration) => blackList.blackListById(id, duration)

    case BlackListAddress(addr, duration) => blackList.blackList(addr, duration)

    case UnBlackListAddress(addr) => blackList.unBlackList(addr)

    case UnBlackListById(id) => blackList.unBlackListById(id)

    case Disconnect(nId) =>
      disconnect(nId)

    case SendToNodeId(nm, nodeId) =>

      if (nodeId == loopbackId) {

        events publish IncomingSerializedMessage(loopbackId, nm)

      } else {
        connections
          .find(_.nodeId.id == nodeId) match {
          case Some(c) => c.handlerRef ! nm
          case None =>
            log.debug("Can't send to {}, no connection", nodeId)

        }
      }

    case Scheduled(c) =>
      scheduledConnect = scheduledConnect filterNot (_ == c.nodeId.id)
      self ! c

    case c@ConnectTo(n@NodeId(nodeId, addr), reconnnectStrategy) =>

      if (!isConnectionInProgress(n.id) &&
        !isConnected(n) &&
        blackList.notBlackListed(n.address) &&
        blackList.notBlackListedById(nodeId)
      ) {

        if (log.isDebugEnabled) {
          log.debug(s"Attempting to IO connect to $nodeId $addr, connections ${connections.map(_.nodeId.id)} inProgress ${connectionsInProgress.map(_.nodeId.id)}")

          log.debug("Connections")
          connections foreach { conn =>
            log.debug(conn.toString)
          }
          log.debug("Connections in Progress")
          connectionsInProgress foreach { conn =>
            log.debug(conn.toString)
          }
        }

        connectionsInProgress = ConnectionInProgress(n, None) +: connectionsInProgress


        if (reconnnectStrategy.nonEmpty) {
          strategies += nodeId -> (addr, reconnnectStrategy)
        }

        IO(Tcp) ! Connect(addr,
          localAddress = nextClientAddress,
          timeout = Option(netInf.connTimeout),
          pullMode = true)
      }

    case conn: ConnectionRef =>
      if (blackList.notBlackListedById(conn.nodeId.id) && blackList.notBlackListed(conn.nodeId.address)) {

        connections.find(_.nodeId.id == conn.nodeId.id) match {
          case Some(alreadyConnected) =>
            log.warning(s"Got a duplicate connRef for $alreadyConnected, killing new conn")
            conn.handlerRef ! PoisonPill
          case None =>
            connections += conn
            connectionsInProgress = connectionsInProgress filterNot (_.nodeId.id == conn.nodeId.id)
            events.publish(Connection(conn.nodeId.id))
        }

      } else {
        conn.handlerRef ! Close
      }

    case t@Terminated(ref) =>
      log.info(s"Connection dead - removing $ref from (only printing first few connections)")
      connections.take(10).foreach(c => log.info(c.toString))

      connectionsInProgress = connectionsInProgress.foldLeft(Seq[ConnectionInProgress]()) { (acc, c) =>
        if (c.handlerRefOpt.contains(ref)) {
          executeReconnectionStrategy(c.nodeId.id)
          acc
        } else c +: acc
      }

      connections = connections.foldLeft(Set[ConnectionRef]()) { (acc, c) =>
        if (c.handlerRef == ref) {
          executeReconnectionStrategy(c.nodeId.id)
          events.publish(ConnectionLost(c.nodeId.id))
          acc
        } else acc + c
      }


    case c@Connected(remote, local) =>
      log.info(s"Got a connection to handle from $c")
      if (blackList.notBlackListed(remote.getAddress) && !maxConnectionsReached()) {

        val connection = sender()
        val handler =
          connectionHandler(connection, remote, netInf.writeBufferSizeInBytes)
        context watch handler
        connection ! Register(handler, keepOpenOnPeerClosed = true)

        connectionsInProgress = connectionsInProgress.foldLeft(Seq[ConnectionInProgress]()) { (acc, c) =>
          // NB if this only compares the address then local testing fails
          // but if a connection from a different port is in progress, this will be missed
          // TODO is there a guard to prevent 2 conns from the same node?
          if (c.nodeId.inetSocketAddress == remote) {
            c.copy(handlerRefOpt = Some(handler)) +: acc
          } else c +: acc
        }


        handler ! Begin(initialStep(local, remote),
          netInf.handshakeTimeoutMs)

      }
      bindConnectionActor ! ResumeAccepting(1)

    case ShutdownNetwork =>
      log.info("Going to shutdown all connections & unbind port")
      bindConnectionActor ! Unbind

    case PublishConnections() =>
      events.publish(Connections(connections.map(_.nodeId.id).toSeq))

    case cf@CommandFailed(c: Connect) =>

      connectionsInProgress = connectionsInProgress.filterNot(_.nodeId.inetSocketAddress == c.remoteAddress)

      events.publish(ConnectionFailed(c.remoteAddress, cf.cause))

      if (blackList.notBlackListed(c.remoteAddress.getAddress)) {
        strategies
          .find { case (id, (addr, strategy)) =>
            addr == c.remoteAddress
          }
          .foreach { case (id, (addr, strategy)) =>
            log.debug(s"executeReconnectionStrategy for $id ( $addr )")
            executeReconnectionStrategy(id)
          }
      }

    case CommandFailed(cmd: Tcp.Command) =>
      log.warning(s"Failed to execute command : {}", cmd)

  }

  private def disconnect(id: UniqueNodeIdentifier): Unit = {
    connectionsInProgress.filter(_.nodeId.id == id) foreach (_.handlerRefOpt foreach (_ ! Close))
    connections.filter(_.nodeId.id == id) foreach (_.handlerRef ! Close)
  }

  private def isConnected(nId: NodeId): Boolean =
    connections.exists(_.nodeId.id == nId.id)

  private def isConnectionInProgress(nId: UniqueNodeIdentifier): Boolean =
    connectionsInProgress.exists(_.nodeId.id == nId)


  private def maxConnectionsReached(): Boolean = {
    connections.size >= netInf.maxNumConnections
  }

  private def executeReconnectionStrategy(nodeId: UniqueNodeIdentifier) = {


    strategies.get(nodeId) foreach { case (addr, strategy) =>
      val delayTime = strategy.head

      if (!scheduledConnect.contains(nodeId)) {
        log.debug("Reconnect strategy for {} in {} s", nodeId, delayTime)
        system.scheduler.scheduleOnce(delayTime.seconds,
          self,
          Scheduled(ConnectTo(NodeId(nodeId, addr), strategy.tail)))
        scheduledConnect = nodeId +: scheduledConnect
        strategies -= nodeId
      } else {
        log.debug(s"$nodeId was already reconnect scheduled.")
      }
    }
  }

  def connectionHandler(
                         connection: ActorRef,
                         remoteAddress: InetSocketAddress,
                         writeBufferSizeInBytes: Int
                       ): ActorRef = {
    context.actorOf(
      Props(classOf[ConnectionHandlerActor],
        connection,
        heartbeatConfig: HeartbeatConfig,
        writeBufferSizeInBytes,
        remoteAddress,
        events).withDispatcher("blocking-dispatcher"))
  }

  override def postStop(): Unit = {
    log.warning("Network controller actor has stopped.")
    super.postStop()
  }

  override def receive: Receive = waitForStart

}
