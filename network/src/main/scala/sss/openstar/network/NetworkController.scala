package sss.openstar.network


import akka.actor.{ActorRef, ActorSystem, Props}
import sss.openstar.UniqueNodeIdentifier
import sss.openstar.network.ConnectionHandler.HeartbeatConfig

import scala.concurrent.Promise

class NetworkController(
                         loopbackId: UniqueNodeIdentifier,
                         initialStepGenerator: InitialHandshakeStepGenerator,
                         networkInterface: NetworkInterface,
                         messageEventBus: MessageEventBus,
                         heartbeatConfig: HeartbeatConfig
                       ) {

  def start()(implicit actorSystem: ActorSystem): NetworkRef = {

    val stopPromise = Promise[Unit]()

    val ref = startActor(loopbackId,
      networkInterface,
      initialStepGenerator,
      stopPromise,
      messageEventBus,
      heartbeatConfig)

    new NetworkRef(ref,stopPromise)

  }

  private def startActor(
                          loopbackId: UniqueNodeIdentifier,
                          networkInterface: NetworkInterface,
                          initialStep: InitialHandshakeStepGenerator,
                          stopPromise: Promise[Unit],
                          messageEventBus: MessageEventBus,
                          heartbeatConfig: HeartbeatConfig
                        )(implicit actorSystem: ActorSystem): ActorRef =
    actorSystem.actorOf(
      Props(classOf[NetworkControllerActor],
        loopbackId,
        networkInterface,
        initialStep,
        stopPromise,
        messageEventBus,
        heartbeatConfig
      ))
}
