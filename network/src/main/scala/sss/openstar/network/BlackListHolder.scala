package sss.openstar.network

import sss.ancillary.Logging
import sss.openstar.UniqueNodeIdentifier

import java.net.InetAddress
import scala.concurrent.duration.Duration

// not thread-safe, use only inside actor
private[network] class BlackListHolder extends Logging {
  private var blackList = Map[InetAddress, Long]()
  private var blackListByIdentity = Map[UniqueNodeIdentifier, Long]()

  def blackListById(id: String, duration: Duration, now: Long = System.currentTimeMillis()): Unit = {
    val banUntil = now + duration.toMillis
    blackListByIdentity += (id -> banUntil)
  }

  def blackList(addr: InetAddress, duration: Duration, now: Long = System.currentTimeMillis()): Unit = {
    val banUntil = now + duration.toMillis
    blackList += (addr -> banUntil)
  }

  def unBlackListById(id: String): Unit = blackListByIdentity -= id

  def unBlackList(addr: InetAddress): Unit = blackList -= addr

  def isBlackListed(remoteIp: InetAddress, now: Long = System.currentTimeMillis()): Boolean = {
    blackList = blackList.filter(_._2 > now)
    blackList.exists(_._1.getAddress sameElements remoteIp.getAddress)
  }

  def notBlackListed(remoteIp: InetAddress, now: Long = System.currentTimeMillis()): Boolean = !isBlackListed(remoteIp, now)

  def isBlackListedById(id: String, now: Long = System.currentTimeMillis()): Boolean = {
    blackListByIdentity = blackListByIdentity.filter(_._2 > now)
    blackListByIdentity.contains(id)
  }

  def notBlackListedById(id: String, now: Long = System.currentTimeMillis()): Boolean = !isBlackListedById(id, now)

}
