package sss.openstar.network

import java.net.InetSocketAddress
import akka.actor.{Actor, ActorLogging, ActorRef, Cancellable, SupervisorStrategy, Terminated}
import akka.io.Tcp
import akka.io.Tcp._
import akka.pattern.pipe
import akka.util.{ByteString, CompactByteString}
import sss.openstar.network.ConnectionHandler.HeartbeatConfig

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.{Failure, Success, Try}

object ConnectionHandler {

  final case class Ack(offset: Int) extends Tcp.Event

  case class HeartbeatConfig(interval: FiniteDuration = 2.seconds, timeout: FiniteDuration = 5.seconds)

  case object NoDataReceivedTimeout

  case class Begin(
                    initialStep: HandshakeStep,
                    totalAllowedHandshakeTimeMs: Int
                  )

  sealed trait HandshakeStep

  final case object RejectConnection
    extends HandshakeStep

  final case class BytesToSend(send: ByteString, nextStep: HandshakeStep)
    extends HandshakeStep

  final case class BytesToSendF(send: Future[ByteString], nextStep: HandshakeStep)
    extends HandshakeStep

  final case class WaitForBytes(handleBytes: ByteString => HandshakeStep)
    extends HandshakeStep

  final case class ConnectionEstablished(nodeId: NodeId, leftOverBytes: ByteString)
    extends HandshakeStep

  final case class ConnectionRef(nodeId: NodeId, handlerRef: ActorRef)

}

class ConnectionHandlerActor(
                              connection: ActorRef,
                              heartbeatConfig: HeartbeatConfig,
                              writeBufferSizeInBytes: Int,
                              remote: InetSocketAddress,
                              eventBus: MessageEventBus
                            ) extends Actor
  with Buffering
  with ActorLogging
  with Protocol {

  private case class TooSlow(totalAllowedHandshakeTimeMs: Int)

  import ConnectionHandler._
  import context.dispatcher

  private var handshakeTimeoutCancellable: Option[Cancellable] = None

  private var receiveTimeout: Option[Cancellable] = None
  private var sendTimeout: Option[Cancellable] = None

  context watch connection

  override def preStart(): Unit = connection ! ResumeReading

  override def postStop(): Unit = {
    handshakeTimeoutCancellable map (_.cancel())
    receiveTimeout map (_.cancel())
    sendTimeout map (_.cancel())
    log.debug("Connection handler for {} down, transferred {} bytes from/to ", remote, transferred)
  }

  // there is not recovery for broken connections
  override val supervisorStrategy = SupervisorStrategy.stoppingStrategy

  override def unhandled(message: Any): Unit = log.warning("ConnectionHandler unhandled {}", message)

  private def resetHeartbeatSchedule(): Unit = {
    sendTimeout map (_.cancel())
    scheduleHeartBeat()
  }


  def isProtocolFormatted(bs: ByteString): Boolean = {
    bs.slice(headerSize,headerSize + Protocol.MagicLength) == ByteString(Protocol.MAGIC)
  }

  private def scheduleHeartBeat(): Unit = {

    sendTimeout = Option(context.system.scheduler.scheduleOnce(
      heartbeatConfig.interval,
      self,
      SerializedMessage(SerializedMessage.noChain, heartbeatCode, Array.emptyByteArray))
    )
  }

  private def resetTimeout(): Unit = {

    receiveTimeout map (_.cancel())

    receiveTimeout = Option(context.system.scheduler.scheduleOnce(
      heartbeatConfig.timeout,
      self,
      NoDataReceivedTimeout)
    )

  }

  private def processErrors: Receive = {

    case cc: ConnectionClosed =>
      log.debug(s"Connection closed to : $remote ${Option(cc.getErrorCause)}")
      context.stop(self)

    case Terminated(ref) =>
      log.debug(s"Connection actorref terminated: $ref ($remote)")
      context.stop(self)

    case CommandFailed(cmd) =>
      log.warning(s"Failed to execute command : $cmd ")
      connection ! ResumeWriting

  }

  private def process(step: HandshakeStep): Unit = {
    step match {

      case BytesToSendF(sendF, nextStep) =>
        sendF.map(bs => process(BytesToSend(bs, nextStep)))

      case BytesToSend(send, nextStep) =>
        connection ! Write(send)
        process(nextStep)

      case n: WaitForBytes =>
        context.become(handshake(n))
      case ConnectionEstablished(nodeId, surplusBytes) =>
        handshakeTimeoutCancellable map (_.cancel())
        context become connectionEstablished(nodeId)
        context.parent ! ConnectionRef(nodeId, self)
        if(isProtocolFormatted(surplusBytes)) {
          log.info("Forwarding correctly surplusBytes to self")
          self ! Received(surplusBytes)
        } else if(surplusBytes.nonEmpty) {
          log.warning(s"Surplus bytes after ConnectionEstablished ${surplusBytes.toString}")
        }

        scheduleHeartBeat()

      case RejectConnection =>
        handshakeTimeoutCancellable map (_.cancel())
        log.info(s"Programmer enforced connection close with: $remote")

        eventBus.publish(ConnectionRejected(remote))

        connection ! Close // this will cause this actor to stop via ConnectionClosed.
    }
  }

  private def closeOnTimeout: Receive = {
    case TooSlow(totalAllowedHandshakeTimeMs) =>
      log.warning(
        "Handshake not completed within {} ms, closing connection with {}",
        totalAllowedHandshakeTimeMs,
        remote)
      connection ! Close
      eventBus.publish(ConnectionHandshakeTimeout(remote))
  }

  override def receive: Receive = closeOnTimeout orElse processErrors orElse {
    case Begin(initialConnectionHandler, totalAllowedHandshakeTimeMs: Int) =>
      handshakeTimeoutCancellable = Option(
        context.system.scheduler.scheduleOnce(
          totalAllowedHandshakeTimeMs milliseconds,
          self,
          TooSlow(totalAllowedHandshakeTimeMs)))
      process(initialConnectionHandler)

    // In the case where we get bytes from the opposite
    // end before we process initial step on this end
    case r: Received => self ! r
  }

  override def postRestart(thr: Throwable): Unit = context stop self

  private def handshake(w: WaitForBytes): Receive = closeOnTimeout orElse processErrors orElse {

    case Received(data) =>

      Try {
        process(w.handleBytes(data))
        connection ! ResumeReading
      }.recover {
        case e: Exception =>
          log.warning(s"Exception processing handshake - ${e.toString}, RejectingConn ")
          process(RejectConnection)
      }
  }

  private var chunksBuffer: ByteString = CompactByteString()

  def receiveMessages(nId: NodeId): Receive = {

    case cc: ConnectionClosed =>
      log.debug(s"Connection closed to : $remote ${Option(cc.getErrorCause)}")
      context.stop(self)

    case Terminated(ref) =>
      log.debug(s"Connection actorref terminated: $ref ($remote)")
      context.stop(self)

    case NoDataReceivedTimeout =>
      log.debug(s"No heartbeat data received in window closing ${nId.id}")
      self ! Close

    case Received(data) =>
      resetTimeout()
      val t = getPacket(chunksBuffer ++ data)
      chunksBuffer = t._2

      t._1.find { packet =>
        fromWire(packet.toByteBuffer) match {
          case Success(SerializedMessage(_, `heartbeatCode`, _)) =>
            false
          case Success(s) =>
            //Shamefully using side effect
            eventBus.publish(
              IncomingSerializedMessage(nId.id, s))
            false

          case Failure(e) =>
            log.warning("Corrupted data from: {} {}", remote, e)
            true
        }
      }
      connection ! ResumeReading

    case Close =>
      connection ! Close
  }

  def connectionEstablished(nId: NodeId): Receive =
    receiveMessages(nId) orElse writing(nId)


  //#writing
  def writing(nId: NodeId): Receive = {

    case m: SerializedMessage =>
      resetHeartbeatSchedule()
      val bytes = toWire(m)
      connection ! Write(bytes, Ack(currentOffset))
      buffer(bytes)

    case Ack(ack) =>
      acknowledge(ack)

    case CommandFailed(Write(_, Ack(ack))) =>
      connection ! ResumeWriting
      log.debug("Write Failed - switch to buffering")
      context become (receiveMessages(nId) orElse buffering(ack, nId))

    case PeerClosed =>
      if (storage.isEmpty) context stop self
      else context become closing
  }

  //#writing

  //#buffering
  def buffering(nack: Int, nId: NodeId): Receive = {
    var toAck = 10
    var peerClosed = false

    {
      case m: SerializedMessage =>
        resetHeartbeatSchedule()
        val bytes = toWire(m)
        buffer(bytes)

      case WritingResumed => writeFirst()
      case PeerClosed => peerClosed = true
      case Ack(ack) if ack < nack => acknowledge(ack)
      case Ack(ack) =>
        acknowledge(ack)
        if (storage.nonEmpty) {
          if (toAck > 0) {
            // stay in ACK-based mode for a while
            writeFirst()
            toAck -= 1
          } else {
            // then return to NACK-based again
            writeAll()
            log.debug("Switch back from buffering to writing")
            context become (if (peerClosed) closing else receiveMessages(nId) orElse writing(nId))
          }
        } else if (peerClosed) context stop self
        else context become (receiveMessages(nId) orElse writing(nId))
    }
  }

  //#buffering

  //#closing
  def closing: Receive = {
    case CommandFailed(_: Write) =>
      connection ! ResumeWriting
      context.become({

        case WritingResumed =>
          writeAll()
          context.unbecome()

        case ack: Int => acknowledge(ack)

      }, discardOld = false)

    case Ack(ack) =>
      acknowledge(ack)
      if (storage.isEmpty) context stop self
  }

  //#closing
  //#storage-omitted
  private var storageOffset = 0
  private var storage = Vector.empty[ByteString]
  private var stored = 0L
  private var transferred = 0L

  val maxStored = writeBufferSizeInBytes
  val highWatermark = maxStored * 5 / 10
  val lowWatermark = maxStored * 3 / 10
  private var suspended = false

  private def currentOffset = storageOffset + storage.size

  //#helpers
  private def buffer(data: ByteString): Unit = {
    storage :+= data
    stored += data.size

    if (stored > maxStored) {
      log.warning(s"drop connection to [$remote] (buffer overrun)")
      context stop self

    } else if (stored > highWatermark) {
      log.debug(s"suspending reading at $currentOffset")
      connection ! SuspendReading
      suspended = true
    }
  }

  private def acknowledge(ack: Int): Unit = {
    require(ack == storageOffset, s"received ack $ack at $storageOffset")
    require(storage.nonEmpty, s"storage was empty at ack $ack")

    val size = storage(0).size
    stored -= size
    transferred += size

    storageOffset += 1
    storage = storage drop 1

    if (suspended && stored < lowWatermark) {
      log.debug(s"resuming reading at $storageOffset")
      connection ! ResumeReading
      suspended = false
    }
  }

  //#helpers

  private def writeFirst(): Unit = {
    connection ! Write(storage(0), Ack(storageOffset))
  }

  private def writeAll(): Unit = {
    for ((data, i) <- storage.zipWithIndex) {
      connection ! Write(data, Ack(storageOffset + i))
    }
  }

  //#storage-omitted
}
