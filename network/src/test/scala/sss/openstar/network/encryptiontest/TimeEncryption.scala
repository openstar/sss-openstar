package sss.openstar.network.encryptiontest

import java.security.SecureRandom
import java.util
import java.util.Base64

import javax.crypto.spec.{GCMParameterSpec, SecretKeySpec}
import javax.crypto.{Cipher, KeyGenerator, SecretKey}

import scala.util.Random

object TimeEncryption {

  val AES_KEY_SIZE = 256
  val GCM_IV_LENGTH = 12
  val GCM_TAG_LENGTH = 16

  def measureCpuTime[T](f: => T): (T, java.time.Duration) = {

    import java.lang.management.ManagementFactory.getThreadMXBean
    if (!getThreadMXBean.isThreadCpuTimeSupported)
      throw new UnsupportedOperationException(
        "JVM does not support measuring thread CPU-time")

    var t: Option[T] = None
    var finalCpuTime: Option[Long] = None
    val thread = new Thread {
      override def run(): Unit = {
        t = Some(f)
        finalCpuTime = Some(getThreadMXBean.getThreadCpuTime(
          Thread.currentThread.getId))
      }
    }
    thread.start()

    while (finalCpuTime.isEmpty && thread.isAlive) {
      Thread.sleep(100)
    }

    (t.get, java.time.Duration.ofNanos(finalCpuTime.getOrElse {
      throw new Exception("Operation never returned, and the thread is dead " +
        "(perhaps an unhandled exception occurred)")
    }))
  }

  def decrypt(key: SecretKey, values: IndexedSeq[(Array[Byte], Array[Byte])]): IndexedSeq[Array[Byte]] = {
    values.map {
      case (iv, enc) => decrypt(enc, key, iv)
    }
  }

  def encrypt(key: SecretKey, values: IndexedSeq[Array[Byte]]): IndexedSeq[(Array[Byte], Array[Byte])] = values map { value =>
    val random = new SecureRandom
    val IV = new Array[Byte](GCM_IV_LENGTH)
    random.nextBytes(IV)
    val cipherText = encrypt(value, key, IV)
    (IV, cipherText)
  }

  def count(values: IndexedSeq[Array[Byte]]): Long = values.foldLeft(0L) { case (acc, e) => acc + e.length }

  def countTuple(values: IndexedSeq[(Array[Byte], Array[Byte])]): Long =
    values.foldLeft(0L) { case (acc, e) => acc + e._1.length + e._2.length }

  def main(args: Array[String]): Unit = {
    val iterations = args(0).toInt


    val keyGenerator = KeyGenerator.getInstance("AES")
    keyGenerator.init(AES_KEY_SIZE)

    // Generate Key
    val key = keyGenerator.generateKey


    val testBytes = (0 to iterations).map(_ => Random.nextBytes(Random.between(1024, 100 * 1024)))
    val numBytes = count(testBytes)

    val (encrypted, durationEnc) = measureCpuTime(encrypt(key, testBytes))
    val numEncBytes = countTuple(encrypted)

    val (decrypted, durationDec) = measureCpuTime(decrypt(key, encrypted))

    val msTimeEncPerKByte = (numBytes.toDouble / 1024D) / durationEnc.toMillis.toDouble
    val msTimeDecPerKByte = (numBytes.toDouble / 1024D) / durationDec.toMillis.toDouble
    val mbPerSec = (numBytes * 1024 * 1024) / (durationEnc.toMillis.toDouble *1000)
    val decMbPerSec = (numBytes * 1024 * 1024) / (durationDec.toMillis.toDouble * 1000)
    println(s"Time to encrypt $durationEnc is $msTimeEncPerKByte ms per KB $mbPerSec M/sec")
    println(s"Time to decrypt $durationDec is $msTimeDecPerKByte ms per Kb $decMbPerSec M/sec")
    val increaseInBytes = numEncBytes - numBytes
    val increaseInBytesPercent = (increaseInBytes.toDouble / numBytes.toDouble) * 100
    println(s"$increaseInBytesPercent % increase in bytes, $increaseInBytes extra bytes")
    testBytes.zip(decrypted).foreach(tup => assert(util.Arrays.equals(tup._1, tup._2)))

  }

  @throws[Exception]
  def encrypt(plaintext: Array[Byte], key: SecretKey, IV: Array[Byte]): Array[Byte] = { // Get Cipher Instance
    val cipher = Cipher.getInstance("AES/GCM/NoPadding")
    // Create SecretKeySpec
    val keySpec = new SecretKeySpec(key.getEncoded, "AES")
    // Create GCMParameterSpec
    val gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, IV)
    // Initialize Cipher for ENCRYPT_MODE
    cipher.init(Cipher.ENCRYPT_MODE, keySpec, gcmParameterSpec)
    // Perform Encryption
    val cipherText = cipher.doFinal(plaintext)
    cipherText
  }

  @throws[Exception]
  def decrypt(cipherText: Array[Byte], key: SecretKey, IV: Array[Byte]): Array[Byte] = {
    val cipher = Cipher.getInstance("AES/GCM/NoPadding")
    val keySpec = new SecretKeySpec(key.getEncoded, "AES")
    val gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, IV)
    // Initialize Cipher for DECRYPT_MODE
    cipher.init(Cipher.DECRYPT_MODE, keySpec, gcmParameterSpec)
    // Perform Decryption
    cipher.doFinal(cipherText)

  }
}
