package sss.openstar.network

import akka.util.ByteString
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.openstar.network.ConnectionHandler.{BytesToSend, BytesToSendF, ConnectionEstablished, WaitForBytes}
import sss.ancillary.Results._
import sss.openstar.common.builders.CpuBoundExecutionContextFixture

class ValidationHandshakeSpec
    extends AnyFlatSpec
    with Matchers
    with NetworkTestFixtures
      with CpuBoundExecutionContextFixture{

  "ValidateHandshake" should "fail if same node id" in {

    val remoteNodeId = "remoteNodeId"
    val sut = new ValidateHandshake(local, remote, networkInterface, handshakeVerifier)
    val BytesToSend(hs, WaitForBytes(handler)) = sut.initialStep()
    val RejectConnection = handler(hs)


  }

  it should "succeed even if handshakes returned in reverse" in {

    val remoteNodeId = "remoteNodeId"
    val sut = new ValidateHandshake(local, remote, networkInterface, handshakeVerifier)
    val BytesToSend(hs, WaitForBytes(handler)) = sut.initialStep()
    val (shake, _) = Handshake.parse(hs.toArray).get
    val sentNonce =
      shake.copy(fromNonce = shake.fromNonce + 1, nodeId = remoteNodeId)
    val BytesToSendF(_, WaitForBytes(handler2)) =
      handler(ByteString(sentNonce.bytes))
    val sentNonce2 = shake.copy(nodeId = remoteNodeId, host = remote)
    val ConnectionEstablished(NodeId(`remoteNodeId`, `remote`), ByteString.empty) = handler2(ByteString(sentNonce2.bytes))

  }

  "ValidationHandshake" should " check application version " in {
    val sut = new ValidateHandshake(local, remote, networkInterface, handshakeVerifier)
    assert(
      sut.isApplicationVersionCompatible(ApplicationVersion(appVer234)).isOk)
    assert(
      !sut.isApplicationVersionCompatible(ApplicationVersion(appVer334)).isOk)
    assert(
      sut.isApplicationVersionCompatible(ApplicationVersion(appVer235)).isOk)
    assert(
      !sut.isApplicationVersionCompatible(ApplicationVersion(appVer111)).isOk)

  }

}
