package sss.openstar.network

import java.net.InetSocketAddress
import scorex.crypto.signatures.Signature
import sss.openstar.network.NetworkInterface.BindControllerSettings
import sss.ancillary.Results.{OkResult, ok}

import scala.concurrent.Future

trait NetworkTestFixtures {

  val appVer111 = "1.1.1"
  val appVer234 = "2.3.4"
  val appVer235 = "2.3.5"
  val appVer334 = "3.3.4"


  val local: InetSocketAddress =
    new InetSocketAddress("localhost", 9870)

  val remote: InetSocketAddress =
    new InetSocketAddress("localhost", 9876)

  val handshakeVerifier = new IdentityVerification {
    override val nodeId: String = "nodeId"
    override val tag: String = "tag"
    override def sign(msg: Array[Byte]): Future[Array[Byte]] = Future.successful(msg)
    override def verify(sig: Signature,
                        msg: Array[Byte],
                        nodeId: String,
                        tag: String): OkResult = ok()
  }

  val settings = BindControllerSettings(
    applicationName = "applicationName",
    appVersion = appVer234,
    heartbeatIntervalms = 2000,
    heartbeatTimeoutms = 3000,
    maxNumConnections = 1000,
    writeBufferSizeInBytes = 10000,
    bindAddress = "127.0.0.1"
  )


  val networkInterface = new NetworkInterface(settings, None, () => Some("testingeror"))
}
