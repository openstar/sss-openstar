package sss.openstar.network

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class NetworkTestsSpec extends AnyFlatSpec with Matchers {


  "The network tests " should " run and print report " in {
    try testserver.TestServerRunner.test(TestActorSystem.actorSystem)
    finally TestActorSystem.actorSystem.terminate()
  }
}
