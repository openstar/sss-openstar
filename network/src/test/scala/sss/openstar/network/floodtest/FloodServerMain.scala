package sss.openstar.network.floodtest

import java.net.InetSocketAddress
import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.util.ByteString
import scorex.crypto.signatures.Signature
import sss.openstar.eventbus.MessageInfo
import sss.openstar.network.ConnectionHandler.HeartbeatConfig
import sss.openstar.network.MessageEventBus.IncomingMessage
import sss.openstar.network.NetworkInterface.BindControllerSettings
import sss.openstar.network.TestActorSystem.{TestMessage, TestMessageInfo}
import sss.openstar.network._
import sss.ancillary.Results._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class FloodServer(isPageServer: Boolean, local: NodeId, remote: NodeId) {

  implicit val actorSystem = ActorSystem()
  val messages = Set(TestMessageInfo)
  val decoder: Byte => Option[MessageInfo] =
    messages.map(b => b.msgCode -> b).toMap.get

  val msgBus = new MessageEventBus(decoder)

  val settings = BindControllerSettings(
    applicationName = "TestServer",
    bindAddress = local.inetSocketAddress.getHostName,
    appVersion = "9.9.9",
    port = local.inetSocketAddress.getPort.toString,
    heartbeatIntervalms = 2000,
    heartbeatTimeoutms = 5000,
    maxNumConnections = 1000,
    writeBufferSizeInBytes = 1000000)

  val networkInterface =
    new NetworkInterface(settings, None, () => Some("TSTERROR"))


  val idVerifier = new IdentityVerification {
    override val nodeId: String = local.id
    override val tag: String = "TAG"

    override def sign(msg: Array[Byte]): Future[Array[Byte]] = Future.successful(Array.fill(50)(4))

    override def verify(sig: Signature, msg: Array[Byte], nodeId: String, tag: String): OkResult = ok()
  }


  //def handshakeGenerator = SimpleTestHandshake(networkInterface, local.id, Random.nextLong()) _

  def handshakeGenerator: InitialHandshakeStepGenerator = ValidateHandshake(
    networkInterface,
    idVerifier
  )

  val netController =
    new NetworkController(local.id, handshakeGenerator, networkInterface, msgBus, HeartbeatConfig())

  val nc = netController.start()
  nc.start() map { _ =>

    if (isPageServer) {
      actorSystem.actorOf(Props(classOf[PageServerActor], nc, msgBus))
    } else {
      actorSystem.actorOf(Props(classOf[PageDownloadActor], nc, msgBus))
      nc.connect(remote, indefiniteReconnectionStrategy(5))
    }

  }

}

object FloodServerMain extends {

  def main(args: Array[String]): Unit = {

    val localIp = args.head
    val remoteIp = args(1)
    val weAreLocal = args(2)

    val local = NodeId("local", new InetSocketAddress(localIp, 8090))
    val remote = NodeId("remote", new InetSocketAddress(remoteIp, 8091))

    val s1 = if (weAreLocal == "local") {
      new FloodServer(false, local, remote)
    } else {
      new FloodServer(true, remote, local)
    }

  }
}

class PageDownloadActor(netRef: NetworkRef)(implicit events: MessageEventBus) extends Actor with ActorLogging {

  import TestMessage._

  override def preStart(): Unit = {
    events subscribe 1.toByte
    events subscribe classOf[Connection]
    events subscribe classOf[ConnectionLost]
  }

  private var downloadProgress: Int = 0


  def getFrom(index: Int) = {
    SerializedMessage(1, TestMessage(downloadCmd, index, ByteString.empty))(TestMessageInfo.toBytes, SerializedMessage.noChain)
  }

  def createReturned(index: Int, bs: ByteString) = {
    SerializedMessage(1, TestMessage(returnedCmd, index, bs))(TestMessageInfo.toBytes, SerializedMessage.noChain)
  }

  override def receive: Receive = {

    case Connection(remote) =>
      netRef.send(getFrom(downloadProgress), Set(remote))
      log.info("Connected remote")

    case ConnectionLost(remote) =>
      log.info(s"Lost Connection to $remote, probably because buffer blown")

    case IncomingMessage(c, _, remote, TestMessage(`pageEndCmd`, index, bs)) =>
      require(index == downloadProgress, s"Index must be correct $index != $downloadProgress")
      netRef.send(getFrom(index), Set(remote))

    case IncomingMessage(c, _, from, TestMessage(`allDoneCmd`, index, bs)) =>
      log.info(s"Success, got and returned $downloadProgress messages in order!")
      netRef.send(createReturned(index, bs), Set(from))
      downloadProgress = 0

    case IncomingMessage(c, _, from, TestMessage(cmd, index, bs)) =>
      require(downloadProgress == index, s"Want $downloadProgress got $index")
      log.info(s"Got index $index")
      downloadProgress += 1
      netRef.send(createReturned(index, bs), Set(from))

  }

}

class PageServerActor(netRef: NetworkRef)(implicit events: MessageEventBus) extends Actor with ActorLogging {

  import TestMessage._

  override def preStart(): Unit = {
    events subscribe 1.toByte
    events subscribe classOf[Connection]
    events subscribe classOf[ConnectionLost]
  }

  val maxMessages = 6000

  var returnedIndex = 0
  val pageSize = 100

  val buffer = (0 to 1000 map (_.toByte)).toArray


  def testMsg(tm: TestMessage) = {
    SerializedMessage(1, tm)(TestMessageInfo.toBytes, SerializedMessage.noChain)
  }

  override def receive: Receive = {

    case Connection(remote) =>
      log.info(s"Connected $remote")

    case ConnectionLost(remote) =>
      log.info(s"DISconnected $remote")

    case IncomingMessage(_, _, from, TestMessage(`returnedCmd`, `maxMessages`, bs)) =>
      log.info(s"Finished ok, got returned for index $maxMessages")

    case IncomingMessage(_, _, from, TestMessage(`returnedCmd`, index, bs)) =>
      require(returnedIndex == index, s"got $index, expected $returnedIndex")
      returnedIndex += 1
      log.info(s"Returned index now $returnedIndex")
      if(returnedIndex == maxMessages) returnedIndex = 0

    case IncomingMessage(_, _, from, TestMessage(`downloadCmd`, `maxMessages`, bs)) =>
      netRef.send(testMsg(TestMessage(allDoneCmd, maxMessages, ByteString())), Set(from))
      log.info(s"Finished sending ok, got request for index $maxMessages")

    case IncomingMessage(_, _, from, TestMessage(`downloadCmd`, index, bs)) =>
      0 until pageSize map (i => TestMessage(downloadedCmd, index + i, ByteString(buffer))) foreach { m =>
        if(m.index < maxMessages) netRef.send(testMsg(m), Set(from))
      }
      if(index + pageSize < maxMessages) {
        netRef.send(testMsg(TestMessage(pageEndCmd, index + pageSize, ByteString(buffer))), Set(from))
      } else {
        netRef.send(testMsg(TestMessage(allDoneCmd, maxMessages, ByteString(buffer))), Set(from))
      }
  }

}
