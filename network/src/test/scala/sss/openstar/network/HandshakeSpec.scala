package sss.openstar.network

import java.net.{InetAddress, InetSocketAddress}

import akka.util.ByteString
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.openstar.crypto.SeedBytes

import scala.util.Success

class HandshakeSpec extends AnyFlatSpec with Matchers {

  "Handshake " should " serialise and deserialize to same " in {

    val host = new InetSocketAddress(InetAddress.getLocalHost, 80)
    val sig = ByteString(SeedBytes.randomSeed(10))
    val time = System.currentTimeMillis()
    val h = Handshake("appName",
                      ApplicationVersion(2, 3, 4),
                      "nodeId",
                      "tag",
                      host,
                      Long.MaxValue,
                      sig,
                      time)

    val Success((deserialized, _)) = Handshake.parse(h.bytes)

    assert(h === deserialized)

  }

}
