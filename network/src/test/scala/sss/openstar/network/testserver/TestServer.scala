package sss.openstar.network.testserver

import java.net.InetSocketAddress

import akka.actor.{Actor, ActorRef, ActorSystem, Cancellable, Props}
import akka.pattern.ask
import sss.openstar.BusEvent
import sss.openstar.network.ConnectionHandler.HeartbeatConfig
import sss.openstar.network.NetworkInterface.BindControllerSettings
import sss.openstar.network.testserver.TestServer._
import sss.openstar.network.{MessageEventBus, _}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.language.postfixOps
import scala.util.{Failure, Random, Success, Try}

object TestServer {

  trait TestRun
  trait TestRunEnd extends TestRun
  case object Stop extends TestRunEnd
  case class End(msg: String) extends TestRunEnd
  case object NoOp extends TestRun
  case class DelayExceeded(howLong: Duration) extends TestRunEnd {
    override def toString: String = s"Failed to receive event in $howLong ... failing."
  }

  case object Drained extends BusEvent

  case class UnExpectedMessage(x: Any) extends TestRunEnd

  case class TimerTriggered(howLong: Duration)
  case class Run(f: (TestServer, NetworkRef) => TestRun) extends TestRun
  case class WaitForNothing(duration: FiniteDuration, next: TestRun)
      extends TestRun
  case class WaitFor[T](t: T => TestRun, duration: FiniteDuration = 3 second)
      extends TestRun

  case class Delay[T <: TestRun](sender: ActorRef, t: T)

}

class TestServer(id: String, newPort: Int)(
    implicit val actorSystem: ActorSystem) {

  import TestServerRunner.{scenarioTimeout, timeout}

  val testServer = this

  val msgBus = new MessageEventBus(TestActorSystem.decoder, Seq.empty)

  val settings = BindControllerSettings(
    applicationName = "TestServer",
    bindAddress = "127.0.0.1",
    appVersion = "9.9.9",
    port = newPort.toString,
    heartbeatIntervalms = 200,
    heartbeatTimeoutms = 300,
    maxNumConnections = 1000
  )


  private var cancellable: Option[Cancellable] = None

  val networkInterface =
    new NetworkInterface(settings, None, () => Some("TESTERROR"))

  val nodeId =
    NodeId(id, new InetSocketAddress("127.0.0.1", settings.port.toInt))

  def handshakeGenerator = SimpleTestHandshake(networkInterface, nodeId.id, Random.nextLong()) _


  val netController =
    new NetworkController(id, handshakeGenerator, networkInterface, msgBus, HeartbeatConfig())

  lazy val startActor = startActorPair._1
  lazy val startFuture = startActorPair._2

  private lazy val startActorPair = {
    val ref = actorSystem.actorOf(Props(InternalActor))
    val nr = netController.start()
    val future = nr.start()
    Await.result(future, scenarioTimeout)
    (ref ? nr) map println
    (ref, Future.successful(nr))
  }

  object InternalActor extends Actor {

    override def preStart(): Unit = {
      super.preStart()
      msgBus.subscribe(Drained.getClass.asInstanceOf[Class[Drained.type]])
    }


    override def receive: Receive = {
      case Stop =>
        context stop self

      case tr: TestRun =>
        context.system.scheduler
          .scheduleOnce(.05 second, self, Delay(sender(), tr))

      case ref: NetworkRef =>
        //println("Started TestServer")
        context become ready(ref)
        val who = sender()
        who ! "Started TestServer"
    }

    def waitForNothing(asker: ActorRef,
                       netRef: NetworkRef,
                       runner: TestRun): Receive = {

      case TimerTriggered(_) =>
        process(asker, netRef, runner)

      case x =>
        cancellable map (_.cancel())
        process(asker, netRef, UnExpectedMessage(x))

    }

    def process(asker: ActorRef, netRef: NetworkRef, runner: TestRun): Unit = {
      runner match {
        case Run(f) =>
          process(asker, netRef, f(testServer, netRef))

        case WaitForNothing(d, r) =>
          context become waitForNothing(asker, netRef, r)
          cancellable = Option(
            context.system.scheduler.scheduleOnce(d, self, TimerTriggered(d))
          )

        case e: UnExpectedMessage =>
          context become ready(netRef)
          context become drain(netRef, asker, e)
          msgBus.publish(Drained)

        case WaitFor(t, d) =>
          cancellable = Option(
            context.system.scheduler.scheduleOnce(d, self, TimerTriggered(d))
          )
          context.become(running(asker, netRef, t))

        case e: End =>
          cancellable map (_.cancel())
          context become drain(netRef, asker, e)
          msgBus.publish(Drained)


        case Stop =>
          netRef.stop() map { _ =>
            context stop self
            context become drain(netRef, asker, Stop)
            msgBus.publish(Drained)
          }

        case r @ DelayExceeded(d) =>
          context become drain(netRef, asker, r)
          msgBus.publish(Drained)

        case NoOp =>
      }
    }

    def drain(netRef: NetworkRef, asker: ActorRef, e: TestRunEnd): Receive = {

      case Drained =>
        context become ready(netRef)
        asker ! e

      case x =>
        println(s"Draining $x")

    }

    def ready(netRef: NetworkRef): Receive = {

      case r: TestRun =>
        process(sender(), netRef, r)

      case Delay(s, r) =>
        process(s, netRef, r)
    }

    def running[T](asker: ActorRef,
                   netRef: NetworkRef,
                   t: T => TestRun): Receive = ready(netRef) orElse {

      case TimerTriggered(d) =>
        process(asker, netRef, DelayExceeded(d))

      case p: T @unchecked =>
        cancellable map (_.cancel())
        Try(t(p)) match {
          case Success(r) => process(asker, netRef, r)
          case Failure(f) => process(asker, netRef, UnExpectedMessage(f))
        }

      case x =>
        println(s"Testserver unexpectedly received $x")
    }
  }
}
