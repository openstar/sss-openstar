package sss.openstar.network

import akka.actor.{Actor, ActorLogging, ActorSystem}
import akka.util.ByteString
import sss.ancillary.Serialize._
import sss.openstar.BusEvent
import sss.openstar.eventbus.MessageInfo

import scala.reflect.runtime.universe.TypeTag


object TestActorSystem {

  implicit val actorSystem: ActorSystem = ActorSystem()

  case class MyEvent(i: Int) extends BusEvent with SuperClass
  case class MyOtherEvent(i: Int) extends BusEvent

  trait SuperClass extends BusEvent

  object TestMessage {
    val downloadCmd = 1
    val downloadedCmd = 2
    val returnedCmd = 3
    val allDoneCmd = 4
    val pageEndCmd = 5

    def apply(data: ByteString): TestMessage = TestMessage(0,0, data)
  }
  case class TestMessage(command: Int,
                         index: Int,
                         data: ByteString) extends BusEvent


  object TestMessageInfo extends MessageInfo {

    override val msgCode: Byte = 1
    override type T = TestMessage

    override def fromBytes(bytes: Array[Byte]) = {
      val extracted = bytes.extract(IntDeSerialize, IntDeSerialize, ByteArrayRawDeSerialize)
      TestMessage(extracted._1, extracted._2, ByteString(extracted._3))
    }



    override val clazz: Class[T] = classOf[TestMessage]

    override def toBytes[A: TypeTag](t: A): Array[Byte] = {
      val m = t.asInstanceOf[TestMessage]
      IntSerializer(m.command) ++ IntSerializer(m.index) ++ ByteArrayRawSerializer(m.data.toArray).toBytes
    }
  }


  val messages = Set(TestMessageInfo)
  val decoder: Byte => Option[MessageInfo] =
    messages.map(b => b.msgCode -> b).toMap.get

  object LogActor extends Actor with ActorLogging {
    override def receive: Receive = {
      case x => log.info(s"Testing default message handler   $x")
    }
  }

  lazy val msgBus = new MessageEventBus(decoder)
  lazy val msgBus2 = new MessageEventBus(decoder)
}
