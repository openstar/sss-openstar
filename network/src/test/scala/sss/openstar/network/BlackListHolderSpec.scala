package sss.openstar.network

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import java.net.InetAddress
import scala.concurrent.duration._

class BlackListHolderSpec extends AnyFlatSpec with Matchers {
  val bl = new BlackListHolder()
  val banDuration = 10.seconds

  "Blacklist" should "ban by node id" in {
    val nodeId = "foo"
    bl.blackListById(nodeId, banDuration, 1)
    bl.isBlackListedById(nodeId, banDuration.toMillis) shouldBe true
    bl.notBlackListedById(nodeId, banDuration.toMillis + 1) shouldBe true
  }

  it should "ban by address" in {
    val addr = InetAddress.getLocalHost
    bl.blackList(addr, banDuration, 1)
    bl.isBlackListed(addr, banDuration.toMillis) shouldBe true
    bl.notBlackListed(addr, banDuration.toMillis + 1) shouldBe true
  }

  it should "unblacklist by node id" in {
    val nodeId = "foo"
    bl.blackListById(nodeId, banDuration)
    bl.isBlackListedById(nodeId) shouldBe true
    bl.unBlackListById(nodeId)
    bl.notBlackListedById(nodeId) shouldBe true
  }

  it should "unblacklist by address" in {
    val addr = InetAddress.getLocalHost
    bl.blackList(addr, banDuration)
    bl.isBlackListed(addr) shouldBe true
    bl.unBlackList(addr)
    bl.notBlackListed(addr) shouldBe true
  }
}