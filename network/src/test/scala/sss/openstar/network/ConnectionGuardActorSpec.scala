package sss.openstar.network

import akka.actor.{ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import sss.openstar.eventbus.MessageInfo
import sss.openstar.network.NetworkControllerActor.BlackListAddress
import sss.openstar.network.TestActorSystem.TestMessageInfo

import java.net.{InetAddress, InetSocketAddress}
import java.util.concurrent.TimeUnit
import scala.concurrent.Promise
import scala.concurrent.duration._

class ConnectionGuardActorSpec() extends TestKit(ActorSystem("ConnectionGuardActorSpec"))
  with ImplicitSender
  with AnyWordSpecLike
  with Matchers
  with BeforeAndAfterAll
  with BeforeAndAfterEach {

  var bus: MessageEventBus = _
  var probe: TestProbe = _
  val threshold = 1
  val thresholdDuration: FiniteDuration = Duration(100, TimeUnit.MILLISECONDS)
  val banDuration: FiniteDuration = Duration(5, TimeUnit.MINUTES)
  implicit val ec = system.dispatcher

  def createGuard() = system.actorOf(
    Props(classOf[ConnectionGuardActor], threshold, thresholdDuration, banDuration, bus, new NetworkRef(probe.ref, Promise())),
    s"ConnectionGuardActor_${System.currentTimeMillis()}"
  )

  override def beforeEach(): Unit = {
    val busAndProbe = initMessageBus()
    bus = busAndProbe._1
    probe = busAndProbe._2
  }
  override def afterEach(): Unit = {
    bus.shutdown()
  }
  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "An Connection guard actor" must {

    "not handle garbage messages" in {
      createGuard() ! "hello"
      probe.expectNoMessage()
    }

    "not send ban message if connection rejected not exceeded threshold duration" in {
      val addr = InetAddress.getLocalHost
      val guard = createGuard()

      system.scheduler.scheduleWithFixedDelay(Duration.Zero, thresholdDuration, guard,
        ConnectionRejected(new InetSocketAddress(addr, 1111)))

      probe.expectNoMessage(2.seconds)
    }

    "not send ban message if connection timeout not exceeded threshold duration" in {
      val addr = InetAddress.getLoopbackAddress
      val guard = createGuard()

      system.scheduler.scheduleWithFixedDelay(Duration.Zero, thresholdDuration, guard,
        ConnectionHandshakeTimeout(new InetSocketAddress(addr, 2222)))

      probe.expectNoMessage(2.seconds)
    }

    "send ban message if connection rejected exceeded threshold" in {
      subscribeBusToBlacklistAddress()
      val addr = InetAddress.getLocalHost
      val guard = createGuard()

      1 to (threshold + 1) foreach { _ =>
        guard ! ConnectionRejected(new InetSocketAddress(addr, 3333))
      }
      val observer = probe.ref
      bus.subscribe(classOf[BlackListAddress])(observer)

      probe.expectMsg(BlackListAddress(addr, banDuration))
    }

    "send ban message if connection timeout exceeded threshold" in {
      subscribeBusToBlacklistAddress()
      val addr = InetAddress.getLoopbackAddress
      val guard = createGuard()

      1 to (threshold + 1) foreach { _ =>
        guard ! ConnectionHandshakeTimeout(new InetSocketAddress(addr, 4444))
      }

      probe.expectMsg(BlackListAddress(addr, banDuration))
    }
  }

  private def initMessageBus(): (MessageEventBus, TestProbe) = {
    val messages = Set(TestMessageInfo)
    val decoder: Byte => Option[MessageInfo] = messages.map(b => b.msgCode -> b).toMap.get
    val bus = new MessageEventBus(decoder, Seq.empty)(system)
    val probe = TestProbe()(system)
    (bus, probe)
  }

  private def subscribeBusToBlacklistAddress(): Unit = {
    val observer = probe.ref
    bus.subscribe(classOf[BlackListAddress])(observer)
  }

}