#! /bin/bash

# inputs
STAGE_FOLDER_1=./node/node-template/target/docker/stage/1/opt/docker/
STAGE_FOLDER_2=./node/node-template/target/docker/stage/2/opt/docker/
ARCHIVE_FOLDER=$(pwd)/autoupdates
ARCHIVE_NAME=${ARCHIVE_FOLDER}/autoupdatelib.zip

mkdir $ARCHIVE_FOLDER

echo "Zip to archive ${ARCHIVE_NAME}"

(cd $STAGE_FOLDER_1 && zip -ur $ARCHIVE_NAME *)
(cd $STAGE_FOLDER_2 && zip -ur $ARCHIVE_NAME *)

echo "Zipped..."
echo | ls -lah $ARCHIVE_NAME

cat $ARCHIVE_NAME | openssl dgst -binary -sha256 | basenc --base64url | xargs -I chk touch ${ARCHIVE_FOLDER}/chk

