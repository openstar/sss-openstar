name := "openstar-schema-migrations"

libraryDependencies += "org.flywaydb" % "flyway-core" % Vers.flywayVersion

libraryDependencies += "com.mcsherrylabs" %% "sss-db" % Vers.sssDbVer % Test

libraryDependencies += "org.hsqldb" % "hsqldb" % Vers.hsqldbVer % Test