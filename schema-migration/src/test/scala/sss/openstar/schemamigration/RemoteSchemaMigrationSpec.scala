package sss.openstar.schemamigration

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers


class RemoteSchemaMigrationSpec extends AnyFlatSpec with Matchers {


  object TestRemotePerformSchemaMigration extends
    RemoteSchemaMigrationBuilder with
    RequireRemoteDatasource {

    lazy override val remoteDataSource: CloseableDataSource =
      sss.db.datasource.DataSource("remote.database.datasource")

    migrateRemoteSqlSchema()
  }


  "Remote Schema migration" should "compile, run and close" in {
    TestRemotePerformSchemaMigration.remoteDataSource.close()
  }
}
