package sss.openstar.schemamigration


import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.openstar.schemamigration.dynamic.GlobalTableNameTag


class SchemaMigrationSpec extends AnyFlatSpec with Matchers {

  object TestPerformSchemaMigration extends
    SchemaMigrationBuilder with
    PerformSchemaMigration with
    RequireDatasource {

    implicit val globalTag = GlobalTableNameTag("TEST")
    override def migrateSqlSchema(): Unit = migrateSqlSchema(globalTag)

    lazy override val dataSource: CloseableDataSource = sss.db.datasource.DataSource()
    override protected val tablePartitionsCount: Int = 100

  }

  "Schema migration" should "compile, run and close" in {
    TestPerformSchemaMigration.dataSource.close()
  }

}
