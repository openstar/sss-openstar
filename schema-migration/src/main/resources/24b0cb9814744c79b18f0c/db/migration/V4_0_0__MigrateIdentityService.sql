ALTER TABLE ${keyTableName} ADD COLUMN ${keyTypeCol} VARCHAR(100) DEFAULT ${DefaultKeyTypeName} NOT NULL;
ALTER TABLE ${keyTableName} ADD COLUMN ${securityLevelCol} INT DEFAULT ${DefaultSecurityLevelValue} NOT NULL;
ALTER TABLE ${keyTableName} ADD COLUMN ${signerTypeCol} VARCHAR(100) DEFAULT ${DefaultSignerTypeName} NOT NULL;
ALTER TABLE ${keyTableName} ADD COLUMN ${interactionTypeCol} VARCHAR(100) DEFAULT ${DefaultInteractionTypeName} NOT NULL;