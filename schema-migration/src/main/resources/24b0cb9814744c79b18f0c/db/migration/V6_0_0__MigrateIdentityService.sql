ALTER TABLE ${identityTableName} ADD COLUMN ${disallowBeforeHeightCol} BIGINT DEFAULT NULL;
ALTER TABLE ${identityTableName} ADD COLUMN ${allowUntilHeightCol} BIGINT DEFAULT NULL;
ALTER TABLE ${identityTableName} ADD COLUMN ${balanceCol} BIGINT DEFAULT 0;