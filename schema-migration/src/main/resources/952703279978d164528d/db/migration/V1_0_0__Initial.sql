------------------------------------ USER KEYS -----------------------------------

CREATE CACHED TABLE IF NOT EXISTS ${usersTableName} (
    ${idCol} BIGINT GENERATED BY DEFAULT AS IDENTITY (START WITH 1, INCREMENT BY 1) UNIQUE,
    ${identityCol} VARCHAR(${MaxLenOfIdentity}) NOT NULL,
    ${statusCol} INT NOT NULL,
    PRIMARY KEY(${identityCol}));

CREATE CACHED TABLE IF NOT EXISTS ${usersKeysTableName} (
    ${idCol} BIGINT GENERATED BY DEFAULT AS IDENTITY (START WITH 1, INCREMENT BY 1) UNIQUE,
    ${identityLnkCol} BIGINT,
    ${tagCol} VARCHAR(${MaxTagLen}) NOT NULL,
    ${encryptedKeyFileCol} VARCHAR(900) NOT NULL,
    CONSTRAINT ${usersKeysTableName}_${identityLnkCol}_fk FOREIGN KEY (${identityLnkCol}) REFERENCES ${usersTableName}(${idCol}),
    PRIMARY KEY(${identityLnkCol}, ${tagCol}));
