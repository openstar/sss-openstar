package sss.openstar

package object schemamigration {
  type CloseableDataSource = javax.sql.DataSource with AutoCloseable
}
