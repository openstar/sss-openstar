package sss.openstar.schemamigration

object Sql {

  def alterColumn(
      tableName: String,
      columnName: String,
      columnType: String
  ): String = {
    s"ALTER TABLE $tableName ALTER COLUMN $columnName $columnType;"
  }

  def alterVarcharColumn(
      tableName: String,
      columnName: String,
      length: Int
  ): String = {
    alterColumn(
      tableName = tableName,
      columnName = columnName,
      columnType = s"VARCHAR($length)"
    )
  }

  def createIndex(
      tableName: String,
      columnNames: List[String],
      maybeIndexName: Option[String] = None,
      unique: Boolean = false
  ): String = {
    require(columnNames.nonEmpty)

    val sep             = "_"
    val indexName       = maybeIndexName.getOrElse(tableName + sep + columnNames.mkString(sep) + sep + "indx")
    val maybeUniqueTerm = if (unique) "UNIQUE" else ""
    val columns         = columnNames.mkString(",")

    s"CREATE $maybeUniqueTerm INDEX IF NOT EXISTS $indexName ON $tableName($columns);"
  }

  def createIndex0(
      tableName: String,
      columnName: String,
      maybeIndexName: Option[String] = None,
      unique: Boolean = false
  ): String = {
    createIndex(
      tableName,
      columnName :: Nil,
      maybeIndexName,
      unique
    )
  }
}
