package sss.openstar.schemamigration

import sss.openstar.schemamigration.Placeholders.getPlaceholders
import sss.openstar.schemamigration.dynamic.GlobalTableNameTag

object SqlSchemaNames {

  val staticRemoteMigrationsResourceDir = "952703279978d164528d/db/migration"
  val staticMigrationsResourceDir = "24b0cb9814744c79b18f0c/db/migration"
  val schemaName                  = "PUBLIC"

  object ColumnNames {

    val idCol               = "id"
    val versionCol          = "version"
    val txIdCol             = "txid"
    val txIdIndxCol         = "txid_indx"
    val statusCol           = "status_col"
    val clientCol           = "client_col"
    val createdAtCol        = "created_at"
    val amountCol           = "amount"
    val encumbranceCol      = "encumbrance"
    val blockHeightCol      = "block_height"
    val ledgerIdCol         = "ledger_id_col"
    val guidCol             = "guid_col"
    val mimeCol             = "mime_col"
    val nameCol             = "name_col"
    val locationUrlCol      = "attachment_col"
    val ivKeyCol            = "ivkey_col"
    val ownerCol            = "owner_col"
    val lenCol              = "len_col"
    val heightCol           = "height"
    val entryCol            = "entry"
    val committedCol        = "committed"
    val rejectedCol         = "rejected"
    val numTxsCol           = "num_txs"
    val prevBlockCol        = "prev_block"
    val merkleRootCol       = "merkle_root"
    val mineTimeCol         = "mine_dt"
    val nodeIdCol           = "nodeId"
    val signatureCol        = "signature"
    val publicKeyCol        = "public_key"
    val metaInfoCol         = "meta_info"
    val toCol               = "to_col"
    val uniqueCol           = "unique_col"
    val parentUniqueCol     = "parent_col"
    val fromCol             = "from_col"
    val messagePayloadCol   = "message"
    val txCol               = "tx_col"
    val pkeyCol             = "pkey"
    val nIdCol              = "nid_col"
    val addrCol             = "addr_col"
    val portCol             = "port_col"
    val capCol              = "cap_col"
    val unreachableCol      = "unreachable_at"
    val indxCol             = "indx"
    val whenCol             = "consumed"
    val parentGuidCol       = "parent_guid_col"
    val messageCol          = "message"
    val updatedAtCol        = "updated_at"
    val providerCol         = "provider_col"
    val uniqueIncreasingCol = "unique_increasing"
    val whoCol              = "userCol"
    val endpointCol         = "endpoint"
    val keyCol              = "key"
    val authCol             = "auth"
    val contactCol          = "contact"
    val localContactLnkCol  = "local_contact_lnk"
    val remoteContactLnkCol = "remote_contact_lnk"
    val localCategoryCol    = "local_cat_col"
    val remoteCategoryCol   = "remote_cat_col"
    val avatarCol           = "avatar_col"
    val identityCol         = "identity_col"
    val keyTypeCol          = "key_type_col"
    val securityLevelCol    = "security_level_col"
    val signerTypeCol       = "signer_type_col"
    val interactionTypeCol  = "interaction_type_col"

    // checkpoints
    val digestCol = "digest_col"

    // identity
    val createdCol     = "created_dt"
    val identityLnkCol = "identity_lnk"
    val tagCol         = "tag_col"
    val domainCol      = "domain_col"
    val storeNodeIdCol = "store_node_col"
    val categoryCol    = "category_col"
    val attrValueCol   = "attr_value_col"
    val keyExternalIdCol = "ext_key_id"
    val disallowBeforeHeightCol = "disallow_before_col"
    val allowUntilHeightCol = "allow_until_col"
    val balanceCol = "balance_col"
    // oracle
    val oracleCol = "oracle_col"

    // user keys
    val encryptedKeyFileCol = "enc_keys_col"

    // COUNTERLEDGER
    val successCol = "success_col"
    val failureCol = "failure_col"

    // System updates
    val updateIdentifierCol: String = "update_id"
    val updatePrepStatusCol: String = "update_status"
    val downloadUrlCol: String      = "download_url"
    val lnkCol: String = "lnk"
    val maxHeightPreUpdateCol: String = "update_height"
    val proposerCol: String = "proposer"
    val preparedNodeCol: String = "prepared_node"
    val updatePrepCompleteCol: String = "update_prep_done"
    val eraIdCol: String = "era_id"


  }

  object Constants {
    // System updates
    val maxLengthUpdateIdentifier: Int = 200
    val maxLengthDownloadUrl: Int      = 1000
    val maxLengthDigest: Int           = 32
    val MaxLenOfIdentity: Int = 400
    val MaxTagLen = 50
    val MaxKeyTypeLen = 50

    //These are the defaults needed to bring previous versions up to default values
    val DefaultKeyTypeName: String = "'Curve25519'"
    val DefaultSecurityLevelValue: Int = 1
    val DefaultSignerTypeName ="'LocalPrivateKeyAccountSigner'"
    val DefaultInteractionTypeName = "'Automatic'"
    val DefaultKeyTagName: String = s"'defaultTag'"
  }

  object TableNames {
    val attachmentsTableName          = "attachments"
    val contactTableName              = "contacts"
    val contactCategoriesTableName    = "contact_categories"
    val subscriptionsTableName        = "html_push_subscriptions"
    val messagesTableName             = "messages"
    val messageDownloadCacheTableName = "message_download_cache"
    val messageInboxTableName         = "message_inbox"
    val utxoWatchTableName            = "utxo_watch"
    val discoveryTableName            = "discovery"
    val clientWalletTableName         = "client_wallet_accounts"
    val publicKeyTrackerTableName     = "track_public_key"
    val walletsTableName              = "wallets"

    // CHECKPOINTS
    val checkpointsTableName = "state_chkpoints"

    // IDENTITY
    val identityTableName           = "identity_tbl"
    val paywallTableName            = "paywall_tbl"
    val keyTableName                = "key_tbl"
    val recoveryIdentitiesTableName = "recovery_tbl"
    val messageStoreTableName       = "msg_store_tbl"
    val attributeTableName          = "identity_ledger_attrs_tbl"
    val ownedAttributeTableName     = "identity_ledger_owner_attrs_tbl"
    val messageStoreViewName        = "msg_store_view"

    // ORACLE
    val oracleOwnerTableName = "oracle_owner"

    // COUNTERLEDGER
    val successCol = "success_col"
    val failureCol = "failure_col"

    val usersTableName = "user_tbl"
    val usersKeysTableName = "user_keys_tbl"
    // System updates
    val systemUpdateTableName: String = "local_system_updates_ledger"
    val systemUpdateNotificationTableName: String = "system_updates_ledger"
    val createSystemUpdatePreparationTableName: String = "system_updates_preparation_ledger"
    val systemEraTableName: String = "local_eras_system_updates_ledger"
  }

  class TaggedTableNames(implicit tag: GlobalTableNameTag) {
    // COUNTERLEDGER
    val counterLedgerTableName = s"counter_tbl_$tag"
    val successViewName        = s"success_view_$tag"
    val failureViewName        = s"failure_view_$tag"

    // QUORUM
    val quorumTableName = s"quorum_tbl_$tag"
  }

  val staticPlaceholders: Map[String, String] =
    getPlaceholders(schemaName)(ColumnNames) ++
      getPlaceholders(schemaName)(TableNames) ++
      getPlaceholders(schemaName)(Constants)

  def taggedPlaceholders(implicit tag: GlobalTableNameTag): Map[String, String] =
    getPlaceholders(schemaName)(new TaggedTableNames)

  def placeholders(implicit tag: GlobalTableNameTag): Map[String, String] = staticPlaceholders ++ taggedPlaceholders

}
