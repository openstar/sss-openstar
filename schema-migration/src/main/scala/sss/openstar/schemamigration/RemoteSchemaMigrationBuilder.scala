package sss.openstar.schemamigration

trait RemoteSchemaMigrationBuilder  {
  self: RequireRemoteDatasource =>

  // this is the schema in the remote db that our migration
  // will attempt to create. In the scenario where the remote schema
  // is a "given" then don't call migrateRemoteSqlSchema().
  lazy val remoteSchemaName: String = "OS_REMOTE"

  def migrateRemoteSqlSchema(): Unit =
    FlywayMigrations.migrateDb(
      FlywayMigrations
        .flywayConfig(
          schemaNameOpt = Some(remoteSchemaName),
          placeholders = SqlSchemaNames.staticPlaceholders,
          dataSource = remoteDataSource,
          SqlSchemaNames.staticRemoteMigrationsResourceDir :: Nil,
          javaMigrations = Nil
        )
        .validateMigrationNaming(true)
    )

}

trait RequireRemoteDatasource {

  val remoteDataSource: CloseableDataSource
}