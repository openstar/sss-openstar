package sss.openstar.schemamigration.dynamic

case class GlobalTableNameTag(tag: String) extends AnyVal {
  override def toString: String = tag
}
