package sss.openstar.schemamigration.dynamic.db.migration

import org.flywaydb.core.api.migration.{BaseJavaMigration, Context}
import sss.openstar.schemamigration.dynamic.UtxoDbStorageMigration.UtxoDbStorageId
import sss.openstar.schemamigration.dynamic._

import java.sql.Connection

class V4_1__DynamicSchemaMigration(partitionTableCount: Int)(implicit tag: GlobalTableNameTag) extends BaseJavaMigration {

  override def migrate(context: Context): Unit = {
    implicit val conn: Connection = context.getConnection
    BlockMigration.migrateV4_1(partitionTableCount)
  }
}
