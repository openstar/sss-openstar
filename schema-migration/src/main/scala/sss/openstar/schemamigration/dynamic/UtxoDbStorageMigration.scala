package sss.openstar.schemamigration.dynamic

object UtxoDbStorageMigration {

  import java.sql.Connection
  import scala.util.Using

  val utxoTableNamePrefix = "utxo_"

  case class UtxoDbStorageId(id: Byte) extends AnyVal {
    override def toString: String = id.toString
  }

  object ColumnNames {
    val txIdCol  = "txid"
    val indxCol  = "indx"
    val entryCol = "entry"
  }

  import ColumnNames._

  def utxoPkConstraintName(tableName: String) = s"${tableName}_pk"
  def utxoPk(tableName: String) =
    s"CONSTRAINT ${utxoPkConstraintName(tableName)} PRIMARY KEY ($txIdCol, $indxCol)"

  def addIndexSql(tableName: String): String =
    s"""CREATE INDEX IF NOT EXISTS
       |${tableName}_indx ON
       |$tableName
       |($txIdCol,
       |$indxCol);
       |""".stripMargin

  def dropIndexSql(tableName: String): String =
    s"""DROP INDEX IF EXISTS
       |${tableName}_indx;
       |""".stripMargin

  private def createUtxoTableSql(tableName: String): String =
    s"""CREATE CACHED TABLE IF NOT EXISTS
       |${tableName}
       |($txIdCol BINARY(32) NOT NULL,
       |$indxCol INT NOT NULL,
       |$entryCol VARBINARY(1048576) NOT NULL,
       |${utxoPk(tableName)});
       |""".stripMargin

  def migrateV1(ids: Set[UtxoDbStorageId])(implicit tag: GlobalTableNameTag, conn: Connection): Unit =
    Using(conn.createStatement()) { stmnt =>
      ids.foreach { id =>
        val tableName = utxoTableName(id)
        stmnt.executeUpdate(createUtxoTableSql(tableName))
        stmnt.executeUpdate(addIndexSql(tableName))
      }
    }

  def utxoTableName(id: UtxoDbStorageId)(implicit tag: GlobalTableNameTag) = s"$utxoTableNamePrefix${id}_$tag"
}
