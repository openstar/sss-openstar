package sss.openstar.schemamigration.dynamic

import sss.openstar.schemamigration.Sql

import java.sql.Connection
import scala.util.Using

object MerkleMigration {

  val merkleTablePrefix = "merkle_"

  object ColumnNames {
    val idCol       = "id"
    val heightCol   = "height"
    val hashCol     = "hash"
    val parentIdCol = "parentId "
  }

  import ColumnNames._

  private def createMerkleTableSqlV1(tablePartitionId: Long): String =
    s"""CREATE CACHED TABLE IF NOT EXISTS ${merkleTableName(tablePartitionId)}
       |($idCol INT NOT NULL,
       |$heightCol BIGINT NOT NULL,
       |$hashCol VARBINARY(1048576),
       |$parentIdCol INT,
       |PRIMARY KEY($heightCol, $idCol));
       |""".stripMargin

  def migrateV1(tableCount: Int, conn: Connection): Unit =
    Using(conn.createStatement()) { stmnt =>
      for (index <- 1 to tableCount) {
        stmnt.executeUpdate(createMerkleTableSqlV1(index))
        stmnt.executeUpdate(Sql.createIndex(merkleTableName(index), List("height", "parentId")))
      }
    }

  def merkleTableName(tablePartitionId: Long) = s"$merkleTablePrefix$tablePartitionId"
}
