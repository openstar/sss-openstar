package sss.openstar.schemamigration.dynamic.db.migration

import org.flywaydb.core.api.migration.{BaseJavaMigration, Context}
import sss.openstar.schemamigration.dynamic.UtxoDbStorageMigration.UtxoDbStorageId
import sss.openstar.schemamigration.dynamic._

import java.sql.Connection

class V1_1__DynamicSchemaMigration(utxoDbStorageIds: Set[UtxoDbStorageId], partitionTableCount: Int)(implicit tag: GlobalTableNameTag) extends BaseJavaMigration {

  override def migrate(context: Context): Unit = {
    implicit val conn: Connection = context.getConnection
    BlockMigration.migrateV1(partitionTableCount)
    MerkleMigration.migrateV1(partitionTableCount, conn)
    UtxoDbStorageMigration.migrateV1(utxoDbStorageIds)
  }
}
