package sss.openstar.schemamigration

object Placeholders {
  def getPlaceholders(schemaName: String)(dbNames: Any): Map[String, String] = dbNames.getClass.getDeclaredFields
    .map { field =>
      field.setAccessible(true)
      field.getName -> field.get(dbNames)
    }
    .filter(_._2 match {
      case  _ :String | _ : Int | _ : Long => true
      case _ => false
    })
    .map { case (k, v) => (k, v.toString.stripPrefix(schemaName + ".")) }
    .toMap
}
