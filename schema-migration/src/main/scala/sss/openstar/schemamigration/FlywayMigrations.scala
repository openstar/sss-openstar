package sss.openstar.schemamigration

import org.flywaydb.core.Flyway
import org.flywaydb.core.api.configuration.FluentConfiguration
import org.flywaydb.core.api.migration.JavaMigration

import java.util.Locale
import javax.sql.DataSource
import scala.jdk.CollectionConverters.MapHasAsJava

/** Default implementation using Flyway framework */
object FlywayMigrations {

  /** @param schemaName
    *   a name space for migrations unique to the calling module
    * @param placeholders
    *   the values for names used oin the sql scripts
    * @param dataSource
    * @param locations
    *   look in the default places below here for migration scripts
    */
  def flywayConfig(
      schemaNameOpt: Option[String],
      placeholders: Map[String, String],
      dataSource: DataSource,
      locations: Seq[String],
      baselineVersion: String = "0.0.0",
      javaMigrations: Seq[JavaMigration] = Seq.empty
  ): FluentConfiguration = {
    val flywayConfig = Flyway.configure
      .baselineVersion(baselineVersion)
      .baselineOnMigrate(true)
      .locations(locations: _*)
      .javaMigrations(javaMigrations: _*)
      .createSchemas(true)
      .placeholders(placeholders.asJava)
      .dataSource(dataSource)

    schemaNameOpt.fold(flywayConfig)(schemaName => {
      require(
        schemaName.toUpperCase(Locale.ROOT) == schemaName,
        "Flyway uses quoted names when creating schemas, " +
          "hence the schema name must be upper case to use them without quotes elsewhere in the module."
      )
      flywayConfig
        .schemas(schemaName)
        .defaultSchema(schemaName)
    })

  }

  def migrateDb(
      schemaNameOpt: Option[String],
      placeholders: Map[String, String],
      dataSource: DataSource,
      locations: String*
  ): Unit = {
    migrateDb(flywayConfig(schemaNameOpt, placeholders, dataSource, locations))
  }

  def migrateDb(flywayConfig: FluentConfiguration): Unit = {
    flywayConfig.load.migrate()
  }

}
