package sss.openstar.schemamigration

import sss.openstar.schemamigration.dynamic.GlobalTableNameTag
import sss.openstar.schemamigration.dynamic.UtxoDbStorageMigration.UtxoDbStorageId
import sss.openstar.schemamigration.dynamic.db.migration.{V1_1__DynamicSchemaMigration, V4_1__DynamicSchemaMigration}

trait SchemaMigrationBuilder extends RequireTablePartitionsCount {
  self: RequireDatasource =>

  def migrateSqlSchema(implicit globalTableNameTag: GlobalTableNameTag): Unit = migrateSqlSchema(Set.empty[UtxoDbStorageId])

  def migrateSqlSchema(utxoDbStorageIds: Set[UtxoDbStorageId])(implicit globalTableNameTag: GlobalTableNameTag): Unit =
    FlywayMigrations.migrateDb(
      FlywayMigrations
        .flywayConfig(
          schemaNameOpt = None,
          placeholders = SqlSchemaNames.placeholders,
          dataSource = dataSource,
          SqlSchemaNames.staticMigrationsResourceDir :: Nil,
          javaMigrations = Seq(
            new V1_1__DynamicSchemaMigration(utxoDbStorageIds, tablePartitionsCount),
            new V4_1__DynamicSchemaMigration(tablePartitionsCount)
          )
        )
        .validateMigrationNaming(true)
    )

  def migrateSqlSchema(): Unit
}

trait RequireTablePartitionsCount {
  protected val tablePartitionsCount: Int
}

trait RequireDatasource {
  val dataSource: CloseableDataSource
}
