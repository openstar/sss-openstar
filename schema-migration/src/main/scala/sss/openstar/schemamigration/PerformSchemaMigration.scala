package sss.openstar.schemamigration

trait PerformSchemaMigration {
  self: SchemaMigrationBuilder =>
  migrateSqlSchema()
}
