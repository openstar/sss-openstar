logLevel := Level.Info

addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.6.1")

// 1.9.9 is the last version before xml dep gets bumped to 2.1.0 causing binary incompatability with Akka
addSbtPlugin("com.github.sbt" % "sbt-native-packager" % "1.9.9" changing())

addSbtPlugin("com.lightbend.akka.grpc" % "sbt-akka-grpc" % "2.1.6")

addSbtPlugin("ch.epfl.scala" % "sbt-scalafix" % "0.10.4")

addSbtPlugin("org.scalameta" % "sbt-scalafmt" % "2.4.6")

addSbtPlugin("com.lightbend.sbt" % "sbt-javaagent" % "0.1.6")

addSbtPlugin("com.github.sbt" % "sbt-pgp" % "2.2.1")