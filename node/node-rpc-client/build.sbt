
name := "openstar-node-rpc-client"

enablePlugins(AkkaGrpcPlugin)

libraryDependencies += "com.thesamet.scalapb" %% "scalapb-runtime" % scalapb.compiler.Version.scalapbVersion % "protobuf"
