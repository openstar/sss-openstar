package sss.openstar.ui.rpc

import java.io.{InputStream, OutputStream}
import java.util.logging.Level
import akka.Done
import akka.actor.ActorSystem
import akka.grpc.GrpcClientSettings
import akka.stream.scaladsl.{Sink, Source, StreamConverters}
import akka.util.ByteString
import com.google.protobuf.{ByteString => GByteString}
import sss.openstar.ui.rpc.BridgeClient.ServiceImpls
import sss.openstar.ui.rpc.ImplicitOps._
import sss.openstar.ui.rpc.ResultImplicits._
import sss.openstar.ui.rpc.SessionPushEventManager.SessionEventFromBridge
import sss.openstar.ui.rpc.UploadFileMessage.Options

import scala.concurrent._
import scala.concurrent.duration._
import scala.util.{Failure, Success}

object BridgeClient {

  object GenericEvents extends Enumeration {
    type GenericEvents = Value

  }

  trait ServiceImpls {
    def bridgeService: BridgeService
    def signerService: SignerService

    def authPlusService: AuthPlusService
  }

  case class RemoteServiceImpls(bridgeClientSettings: GrpcClientSettings)(implicit system: ActorSystem) extends ServiceImpls {

    private lazy val rpcClient = BridgeServiceClient(bridgeClientSettings)
    private lazy val signerRpcClient = SignerServiceClient(bridgeClientSettings)
    private lazy val authPlusRpcClient = AuthPlusServiceClient(bridgeClientSettings)

    override def bridgeService: BridgeService = rpcClient

    override def signerService: SignerService = signerRpcClient

    override def authPlusService: AuthPlusService = authPlusRpcClient
  }

  def apply(bridgeClientConfig: String, maxInboundMessageSize: Int = 1024 * 1024 * 10)(implicit system: ActorSystem = ActorSystem("BridgeClient"),
                                        maxCallWaitDuration: Duration = defaultMaxRpcCallWait): BridgeClient = {
    val settings = GrpcClientSettings
      .fromConfig(bridgeClientConfig)
      .withChannelBuilderOverrides(_.maxInboundMessageSize(maxInboundMessageSize))

    new BridgeClient(RemoteServiceImpls(settings))
  }
}

class BridgeClient(serviceImpls: ServiceImpls)(
                    implicit val system: ActorSystem,
                    val maxCallWaitDuration: Duration) {


  implicit val executionContext: ExecutionContext = system.dispatcher

  private lazy val rpcClient = serviceImpls.bridgeService
  private lazy val signerRpcClient = serviceImpls.signerService
  private lazy val authPlusRpcClient = serviceImpls.authPlusService

  val signerClient = new SignerClient(signerRpcClient)
  val authPlusClient = new AuthPlusClient(authPlusRpcClient)

  def webAuthnSignature(user: String, signature: Array[Byte]) : Future[Result[Unit]] = {
    val in = WebAuthnSigMessage.of(user, GByteString.copyFrom(signature))
    rpcClient.webAuthnSig(in).map {
      case ResultWebAuthnSig(Some(prob), _) => problem(prob)
      case _ => success(())
    }
  }

  def login(
             sessId: String,
             user: String,
             password: String,
             subOpt: Option[SubscriptionMessage]
           ): Future[Result[String]] = {
    login(sessId, user,
      "defaultTag",
      "LocalPrivateKeyAccountSigner",
      Some(password), subOpt)._2.map(success)
  }

  def login(
             sessId: String,
             user: String,
             tag: String,
             authType: String,
             password: Option[String],
             subOpt: Option[SubscriptionMessage]
           ): (Future[Array[Byte]], Future[String]) = {
    val messageToSign = Promise[Array[Byte]]()
    val sessIdBack = Promise[String]()

    def failResult(e: Throwable): Unit = {
      messageToSign.failure(e)
      sessIdBack.failure(e)
    }

    rpcClient.login(
      LoginMessage(
        sessId,
        user,
        password,
        subOpt,
        authType,
        tag,
      )
    ).recover {
      case e => failResult(e)

    }.runWith(Sink.foreach {
      case ResultLogin(ResultLogin.Either.Problem(appError), _) =>
        failResult(new RuntimeException(appError.toString))
      case ResultLogin(ResultLogin.Either.SessId(value), _) =>
        sessIdBack.success(value)
      case ResultLogin(ResultLogin.Either.MessageToSign(m), _) =>
        messageToSign.success(m.toByteArray)
    }).recover {
      case e => failResult(e)
    }
    (messageToSign.future, sessIdBack.future)
  }


  def createUser(sessId: String, user: String, password: String, pass2: String, subOpt: Option[SubscriptionMessage]): Future[Result[String]] =
    rpcClient.createUser(CreateUserMessage(sessId, user, password, pass2, subOpt)).map(massageToResult[String, ResultCreateUser])

  def createUserBasic(in: CreateUserBasicMessage): Future[ResultCreateUser] = {
    rpcClient.createUserBasic(in)
  }

  def countMessages(sessId: String): Future[Result[Int]] =
    rpcClient.countMessages(SessId(sessId)).map(massageToResult[Int, ResultCountMessages])


  def countIdentities(filter: Option[String]): Future[Result[Long]] =
    rpcClient.countIdentities(FilterMessage(filter)).map(massageToResult[Long, ResultCountIdentities])

  def fetchIdentities(filter: Option[String], startIndex: Int, limit: Int): Future[Result[Seq[String]]] =
    rpcClient.fetchIdentities(PageParam(startIndex, limit, filter)).map(massageToResult[Seq[String], ResultFetchIdentities])

  def fetchMessages(sessId: String, startIndex: Int, limit: Int, newestFirst: Boolean = false): Future[Result[Seq[DisplayMessage]]] =
    rpcClient.fetchMessages(FetchMessagesMessage(sessId, startIndex, limit, newestFirst))
      .limit(100)
      .runWith(Sink.seq[DisplayMessage])
      .map(success)
      //.runFold(Seq.empty[DisplayMessage])({ case (acc, m) => acc :+ m })


  def balance(sessId: String): Future[Result[Long]] =
    rpcClient.balance(SessId(sessId)).map(massageToResult[Long, ResultBalance])

  def sendEncryptedMessage(sessId: String, msg: DecryptedMessage): Future[Result[Boolean]] =
    rpcClient.sendEncryptedMessage(DecryptedMessageMessage(sessId, Some(msg))).map(massageToResult[Boolean, ResultSendEncryptedMessage])

  def listProviders(sessId: String): Future[Result[Seq[RegisteredProvider]]] =
    rpcClient.listProviders(SessId(sessId)).map(massageToResult[Seq[RegisteredProvider], ResultRegisteredProviders])

  def removeProvider(sessId: String, provider: String): Future[ResultOk] =
    rpcClient.removeProvider(ProviderMessage(sessId, provider)).map(massageToResult[Boolean, ResultRemoveProvider])

  def addProvider(sessId: String, provider: String): Future[ResultOk] =
    rpcClient.addProvider(ProviderMessage(sessId, provider)).map(massageToResult[Boolean, ResultAddProvider])

  def logout(sessId: String): Future[ResultOk] =
    rpcClient.logout(SessId(sessId)).map(massageToResult[Boolean, ResultLogout])


  def fetchMessage(sessId: String, id: Long): Future[Result[DetailedDisplayMessage]] = {
    rpcClient.fetchMessage(MessageIdMessage(sessId, id)).map(r => massageToResult(r))
  }

  def listPaywallCategories(sessId: String): Future[Result[Seq[PaywallCategory]]] = {
    rpcClient.listPaywallCategories(SessId(sessId)).map(r => massageToResult(r))
  }

  def setPaywallCategory(sessId: String,
                         paywallCategory: PaywallCategory): Future[ResultOk] = {
    rpcClient.setPaywallCategory(SetPaywallCategoryMessage(sessId, Some(paywallCategory))).map(massageToResult[Boolean, ResultSetPaywallCategory])
  }

  def deletePaywallCategory(sessId: String,
                            paywallCategory: String): Future[ResultOk] =
    rpcClient.deletePaywallCategory(DeletePaywallCategoryMessage(sessId, paywallCategory)).map(massageToResult[Boolean, ResultDeletePaywallCategory])

  def sendNewContactMessage(sessId: String, msg: NewContactMessage): Future[ResultOk] =
    rpcClient.sendNewContactMessage(NewContactMessageMessage(sessId, Some(msg))).map(massageToResult[Boolean,ResultSendNewContactMessage])

  def deleteMessage(sessId: String, msgId: Long): Future[ResultOk] =
    rpcClient.deleteMessage(MessageIdMessage(sessId, msgId)).map(massageToResult[Boolean, ResultDeleteMessage])

  def setAvatar(sessId: String, avatar: Option[Array[Byte]]): Future[ResultOk] =
    rpcClient.setAvatar(BytesMessage(sessId, avatar.map(GByteString.copyFrom))).map(massageToResult[Boolean, ResultSetAvatar])

  def avatar(sessId: String, contact: String): Future[Result[Option[Array[Byte]]]] =
    rpcClient.avatar(AvatarMessage(sessId, contact)).map(massageToResult[Option[Array[Byte]], ResultAvatar])

  def contacts(sessId: String): Future[Result[Seq[RpcContact]]] =
    rpcClient.listContacts(SessId(sessId)).map(r => massageToResult(r))

  def getProviderCharge(sessId: String): Future[Result[Option[Option[Long]]]] = {
    rpcClient
      .getProviderCharge(SessId(sessId))
      .map(r => massageToResult(r))

  }

  def setProviderCharge(sessId: String, chargeOpt: Option[Long]): Future[Result[Option[Option[Long]]]] = {
    rpcClient
      .setProviderCharge(
        SetProviderMessage(sessId, chargeOpt))
      .map(r => massageToResult(r))

  }

  def listRegisteredProviders(startIndex: Long, pageSize: Int): Future[Result[Seq[RegisteredProvider]]] = {
    rpcClient
      .listRegisteredProviders(
        PageParam(startIndex, pageSize))
      .map(r => massageToResult(r))
  }

  /**
    *
    * @param sessId
    * @param maxRetries max number of staggered connection retries
    * @param numRetries used internally for retry, always set as 0 to start
    */
  def consumeEvents(sessId: String, maxRetries: Int = 5, numRetries: Int = 0): Unit = {
    rpcClient.getEvents(SessId(sessId)).runForeach { e =>
      if (!e.event.isEmpty) system.eventStream.publish(SessionEventFromBridge(SessionId(sessId), e))
    } onComplete {
      case Success(_) =>
        BridgeLogging.log.log(Level.INFO, "consumeEvents completed normally, probably via logout")
      case Failure(e) if numRetries >= maxRetries =>
        BridgeLogging.log.log(Level.WARNING, s"consumeEvents permanently failed, retry count is $numRetries")
      case Failure(e) =>
        BridgeLogging.log.log(Level.FINE, s"consumeEvents failed, retry count is $numRetries")
        system.scheduler.scheduleOnce(numRetries.seconds)(consumeEvents(sessId, maxRetries, numRetries + 1))

    }
  }

  def costToContact(sessId: String, to: String): Future[Result[Long]] = {
    rpcClient
      .costToContact(CostToContactMessage(sessId, to))
      .map(massageToResult[Long, ResultCostToContact])

  }

  def createAttachmentUploadOutputStream(
                         sessionId: SessionId,
                         guid: Array[Byte],
                         fileName: String,
                         fileType: String):FileUploadOutputStream = {

    val initMessageDetails = Options.Details(UploadFileDetails(sessionId.value, fileName, fileType, GByteString.copyFrom(guid)))
    val initMessage = UploadFileMessage(initMessageDetails)

    val begin = Source.single(initMessage)

    val out : Source[UploadFileMessage, OutputStream] =
      StreamConverters.asOutputStream()
        .map(bs => {
          val chunk = Options.Chunk((Chunk(GByteString.copyFrom(bs.asByteBuffer))))
          UploadFileMessage(chunk)
        })

    val (output, source) = out.prepend(begin).preMaterialize()


    val fut = rpcClient.uploadAttachment(source)
    FileUploadOutputStream(output,fut)
  }

  def downloadAttachment(sessionId: SessionId,
                         guid: Array[Byte],
                         fileName: String
                   ):InputStream = {

    val source = rpcClient.downloadAttachment(
      DownloadFileDetailsMessage(sessionId.value,
        Some(DownloadFileDetails(fileName, GByteString.copyFrom(guid)))))

    val sourceAsByteString = source.map(fum => {
      ByteString(fum.getChunk.part.asReadOnlyByteBuffer())
    })
    sourceAsByteString.runWith(StreamConverters.asInputStream())

  }

  def deleteAttachment(sessionId: SessionId, guid: Array[Byte],
                       fileName: String): Future[Result[Boolean]] = {
    rpcClient.deleteAttachment(DownloadFileDetailsMessage(sessionId.value,
      Some(DownloadFileDetails(fileName, GByteString.copyFrom(guid)))))
      .map(massageToResult[Boolean, ResultDeleteAttachment])
  }

  def hostDecrypt(base64EncodedECEncryption: String): Future[Result[String]] = {
    rpcClient.hostDecrypt(HostEncrypDecryptMessage(base64EncodedECEncryption)).map(massageToResult[String, ResultHostDecrypt])
  }

  def hostEncrypt(stringToEncrypt: String): Future[Result[String]] = {
    rpcClient.hostEncrypt(HostEncrypDecryptMessage(stringToEncrypt)).map(massageToResult[String, ResultHostEncrypt])
  }

  def clientBalance(sessionId: SessionId, client: String): Future[Result[Long]] = {
    rpcClient
      .clientBalance(ClientBalanceMessage(sessionId.value, client))
      .map(massageToResult[Long, ResultClientBalance])
  }

  def withdrawFromClientAccount(sessionId: SessionId, client: String, amount: Int): Future[Result[Boolean]] = {
    rpcClient
      .withdrawFromClientAccount(ClientWithdrawMessage(sessionId.value, client, amount))
      .map(massageToResult[Boolean, ResultWithdrawFromClientAccount])
  }

  def sendToTrustee(sessionId: SessionId, trustee: String, amount: Int): Future[Result[Boolean]] = {
    rpcClient
      .sendToTrustee(ToTrusteeMessage(sessionId.value, trustee, amount))
      .map(massageToResult[Boolean, ResultTrusteeMessage])
  }

  def transferBetweenClients(sessionId: SessionId, amount: Int, fromClient: String, toClient: String): Future[Result[Boolean]] = {
    rpcClient
      .transferBetweenClientAccounts(BetweenClientsTransferMessage(sessionId.value, amount, fromClient, toClient))
      .map(massageToResult[Boolean, ResultTransferBetweenClientAccounts])
 }


  def currencyWithdrawal(sessionId: SessionId, amount: Long, adaAddress: String): Future[Result[Boolean]] = {
    rpcClient
      .currencyWithdrawal(WithdrawCurrencyMessage(sessionId.value, adaAddress, amount))
      .map(massageToResult[Boolean, ResultCurrencyWithdrawal])
  }

  def chargeClientsAccount(service: ApiToken, client: ApiToken, isRefund: Boolean = false): Future[Result[Boolean]] = {
    val in = ChargeClientsAccountMessage(Some(service), Some(client), refund = isRefund)
    rpcClient.chargeClientAccount(in).map(massageToResult[Boolean, ResultChargeClientsAccount])
  }

  def createIdentityBearerAttribute(sessionId: SessionId): Future[Result[String]] = {
    val in = SessId(sessionId.value)
    rpcClient.createIdentityBearerAttribute(in)
      .map(massageToResult[String, ResultCreateIdentityBearerAttribute])
  }

  def getHashedIdentityBearerAttribute(sessionId: SessionId): Future[Result[Option[String]]] = {
    val in = SessId(sessionId.value)
    rpcClient.getHashedIdentityBearerAttribute(in)
      .map(massageToResult[Option[String], ResultGetHashedIdentityBearerAttribute])
  }

  def listClientBalances(sessionId: SessionId, startIndex: Long, pageSize: Int): Future[Result[Seq[ClientBalance]]] = {
    val in = ListClientBalancesMessage(sessionId.value, startIndex, pageSize)
    rpcClient.listClientBalances(in)
      .map(r => massageToResult(r))
  }

  def setEmail(sessionId: SessionId, email: String): Future[Result[String]] = {
    rpcClient
      .setEmail(SetEmailMessage(sessionId.value, email))
      .map(massageToResult[String, ResultSetEmail])
  }

  def createSystemUpdateProposal(
                                  sessionId: SessionId,
                                  uniqueUpdateId: String,
                                  downloadUrl: String,
                                  checksumBase64: String,
                                  thresholdBlockHeight: Long): Future[Result[Boolean]] = {
    rpcClient
      .createSystemUpdateProposal(CreateSystemUpdateProposalMessage(
        sessionId.value,
        uniqueUpdateId,
        downloadUrl,
        checksumBase64,
        thresholdBlockHeight
      ))
      .map(massageToResult[Boolean, ResultCreateSystemUpdateProposal])
  }

  def respondSystemUpdateProposal(sessionId: SessionId,
                                  uniqueUpdateId: String,
                                  preparedOrCancel: Boolean): Future[Result[Boolean]] = {
    rpcClient
      .respondSystemUpdateProposal(SystemUpdateProposalResponseMessage(
        sessionId.value,
        uniqueUpdateId,
        preparedOrCancel
      ))
      .map(massageToResult[Boolean, ResultRespondSystemUpdateProposal])
  }


  def getPreparedForSystemUpdate(sessionId: SessionId, uniqueUpdateId: String): Future[Result[PreparedForSystemUpdate]] = {
    rpcClient
      .getPreparedForSystemUpdate(GetPreparedForSystemUpdateMessage(
        sessionId.value,
        uniqueUpdateId
      ))
      .map(massageToResult[PreparedForSystemUpdate, ResultGetPreparedForSystemUpdate])
  }


  def close():Future[Done] = {
    (rpcClient match {
      case client: BridgeServiceClient => client.close()
      case _ => Future.successful(Done)
    }).flatMap(_ => signerRpcClient match {
      case client: SignerServiceClient => client.close()
      case _ => Future.successful(Done)
    })
  }

}
