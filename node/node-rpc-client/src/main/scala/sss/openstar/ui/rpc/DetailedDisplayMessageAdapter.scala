package sss.openstar.ui.rpc

import java.time.LocalDateTime

import sss.openstar.ui.rpc.MessagePayloadType.MessagePayloadType

trait DetailedDisplayMessage {
  val id: Long
  val author: String
  val subject : String
  val receivedAt: LocalDateTime
  val parentIdOpt: Option[Long]
  val msgPayloadType: MessagePayloadType
}

case object BrokenDetailedMessage extends DetailedDisplayMessage {
  override val id: Long = -1
  override val author: String = ""
  override val subject: String = ""
  override val receivedAt: LocalDateTime = LocalDateTime.MIN
  override val parentIdOpt: Option[Long] = None
  override val msgPayloadType: MessagePayloadType = MessagePayloadType.BrokenMessageType
}

case class DetailedNewContactMessage(id: Long,
                                     author: String,
                                     subject: String,
                                     receivedAt: LocalDateTime,
                                     parentIdOpt: Option[Long],
                                     msgPayloadType: MessagePayloadType,
                                     invitorCategory: String,
                                     invitorCurrentAmount: Long,
                                     invitee: String,
                                     inviteePaywalls: sss.openstar.ui.rpc.RpcDetailedNewContactMessage.InviteePaywalls
                                    ) extends DetailedDisplayMessage

case class DetailedEncryptedMessage(id: Long,
                                    guid: Array[Byte],
                                    author: String,
                                    subject: String,
                                    receivedAt: LocalDateTime,
                                    parentIdOpt: Option[Long],
                                    msgPayloadType: MessagePayloadType,
                                    amountOpt: Option[Long],
                                    tos: Seq[String],
                                    body: String,
                                    files: Seq[FileData],
                                    readReceipts: Seq[ReadReceipt],
                                    children: Seq[Long]
                                   )  extends DetailedDisplayMessage

object DetailedDisplayMessageAdapter {

  def adapt(rpcDetailedDisplayMsg: RpcDetailedDisplayMessage):DetailedDisplayMessage = {

    if(rpcDetailedDisplayMsg.details.isContact) {

      val contact = rpcDetailedDisplayMsg.details.contact.get


      DetailedNewContactMessage(
        rpcDetailedDisplayMsg.id,
        rpcDetailedDisplayMsg.author,
        rpcDetailedDisplayMsg.subject,
        MilliUtils.getFromMilli(rpcDetailedDisplayMsg.receivedAtInternal),
        rpcDetailedDisplayMsg.parentIdOpt,
        MessagePayloadType.NewContactType,
        contact.invitorCategory,
        contact.invitorCurrentAmount,
        contact.invitee,
        contact.inviteePaywalls
      )

    } else if (rpcDetailedDisplayMsg.details.isEncrypted) {

      val enc = rpcDetailedDisplayMsg.details.encrypted.get

      DetailedEncryptedMessage(
        rpcDetailedDisplayMsg.id,
        enc.guid.toByteArray,
        rpcDetailedDisplayMsg.author,
        rpcDetailedDisplayMsg.subject,
        MilliUtils.getFromMilli(rpcDetailedDisplayMsg.receivedAtInternal),
        rpcDetailedDisplayMsg.parentIdOpt,
        MessagePayloadType(rpcDetailedDisplayMsg.msgPayloadTypeInternal),
        enc.amountOpt,
        enc.tos,
        enc.body,
        enc.files,

        enc.readReceipts,
        enc.children
        )

    } else BrokenDetailedMessage

  }
}

