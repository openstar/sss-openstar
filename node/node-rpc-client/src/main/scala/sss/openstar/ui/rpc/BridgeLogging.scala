package sss.openstar.ui.rpc

import java.util.logging.Logger

trait BridgeLogging {
  val log = Logger.getLogger("sss.openstar.ui.rpc.BridgeClient")
}

object BridgeLogging extends BridgeLogging
