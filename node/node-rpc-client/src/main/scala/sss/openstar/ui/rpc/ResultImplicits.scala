package sss.openstar.ui.rpc

import sss.openstar.ui.rpc.ProblemTypes._


object ResultImplicits {

  def massageToResult[T, K <: BaseResult](b: K)(implicit m: Massage[T, K]): Result[T] =
    m.checkForProblem(b)
      .getOrElse(m.toResult(b))

  trait Massage[T, K <: BaseResult] {
    def checkForProblem(result: K): Option[Result[T]] = result.problem.map(_.msg).map {
      case `unknownSession` => problem(UnknownSession)
      case `creditTooLow` => problem(CreditTooLowProblem)
      case msg => (problem[T](msg))
    }

    def toResult(result: K): Result[T]
  }

  private def genericParser[T, S](error: Option[AppError], value: Option[T], mapper: T => S, clazz: Class[_]): Result[S] = (error, value) match {
    case (None, None) => problem(ParsingProblem(msg = s"Empty ${clazz.getName}"))
    case (Some(p), Some(v)) => problem(ParsingProblem(msg = s"Unexpected ${clazz.getName}: both $p and $v"))
    case (Some(p), None) => problem(p)
    case (None, Some(v)) => success(mapper(v))
  }

  implicit val massageGetPreparedForSystemUpdate: Massage[PreparedForSystemUpdate, ResultGetPreparedForSystemUpdate] =
    (r: ResultGetPreparedForSystemUpdate) =>
      genericParser[PreparedForSystemUpdate, PreparedForSystemUpdate](r.either.problem, r.either._value, identity[PreparedForSystemUpdate], ResultGetPreparedForSystemUpdate.getClass)

  implicit val massageSystemUpdateProposalResponse: Massage[Boolean, ResultRespondSystemUpdateProposal] =
    (r: ResultRespondSystemUpdateProposal) => genericParser(r.either.problem, r.either._value, identity[Boolean], ResultRespondSystemUpdateProposal.getClass)

  implicit val massageCreateSystemUpdateProposal: Massage[Boolean, ResultCreateSystemUpdateProposal] =
    (r: ResultCreateSystemUpdateProposal) => genericParser(r.either.problem, r.either._value, identity[Boolean], ResultCreateSystemUpdateProposal.getClass)

  implicit val massageCurrencyWithdrawal: Massage[Boolean, ResultCurrencyWithdrawal] =
    (r: ResultCurrencyWithdrawal) => genericParser(r.either.problem, r.either._value, identity[Boolean], ResultCurrencyWithdrawal.getClass)

  implicit val massageBalance: Massage[Long, ResultBalance] =
    (r: ResultBalance) => genericParser(r.either.problem, r.either._value, identity[Long], ResultBalance.getClass)

  implicit val massageCostToContact: Massage[Long, ResultCostToContact] =
    (r: ResultCostToContact) => genericParser(r.either.problem, r.either._value, identity[Long], ResultCostToContact.getClass)

  implicit val massageClientBalance: Massage[Long, ResultClientBalance] =
    (r: ResultClientBalance) => genericParser(r.either.problem, r.either._value, identity[Long], ResultClientBalance.getClass)

  implicit val massageResultCountIdentities: Massage[Long, ResultCountIdentities] =
    (r: ResultCountIdentities) => genericParser(r.either.problem, r.either._value, identity[Long], ResultCountIdentities.getClass)

  implicit val massageFetchIdentities: Massage[Seq[String], ResultFetchIdentities] =
    (r: ResultFetchIdentities) => genericParser[SeqString, Seq[String]](r.either.problem, r.either._value, _.strs, ResultFetchIdentities.getClass)

  implicit val massageLogout: Massage[Boolean, ResultLogout] =
    (r: ResultLogout) => genericParser(r.either.problem, r.either._value, identity[Boolean], ResultLogout.getClass)

  implicit val massageTrusteeMessage: Massage[Boolean, ResultTrusteeMessage] =
    (r: ResultTrusteeMessage) => genericParser(r.either.problem, r.either._value, identity[Boolean], ResultTrusteeMessage.getClass)

  implicit val massageChargeClientsAccount: Massage[Boolean, ResultChargeClientsAccount] =
    (r: ResultChargeClientsAccount) => genericParser(r.either.problem, r.either._value, identity[Boolean], ResultChargeClientsAccount.getClass)

  implicit val massageTransferBetweenClientAccounts: Massage[Boolean, ResultTransferBetweenClientAccounts] =
    (r: ResultTransferBetweenClientAccounts) => genericParser(r.either.problem, r.either._value, identity[Boolean], ResultTransferBetweenClientAccounts.getClass)

  implicit val massageWithdrawFromClientAccount: Massage[Boolean, ResultWithdrawFromClientAccount] =
    (r: ResultWithdrawFromClientAccount) => genericParser(r.either.problem, r.either._value, identity[Boolean], ResultWithdrawFromClientAccount.getClass)

  implicit val massageSendEncryptedMessage: Massage[Boolean, ResultSendEncryptedMessage] =
    (r: ResultSendEncryptedMessage) => genericParser(r.either.problem, r.either._value, identity[Boolean], ResultSendEncryptedMessage.getClass)

  implicit val massageSendNewContactMessage: Massage[Boolean, ResultSendNewContactMessage] =
    (r: ResultSendNewContactMessage) => genericParser(r.either.problem, r.either._value, identity[Boolean], ResultSendNewContactMessage.getClass)

  implicit val massageRemoveProvider: Massage[Boolean, ResultRemoveProvider] =
    (r: ResultRemoveProvider) => genericParser(r.either.problem, r.either._value, identity[Boolean], ResultRemoveProvider.getClass)

  implicit val massageAddProvider: Massage[Boolean, ResultAddProvider] =
    (r: ResultAddProvider) => genericParser(r.either.problem, r.either._value, identity[Boolean], ResultAddProvider.getClass)

  implicit val massageSetPaywallCategory: Massage[Boolean, ResultSetPaywallCategory] =
    (r: ResultSetPaywallCategory) => genericParser(r.either.problem, r.either._value, identity[Boolean], ResultSetPaywallCategory.getClass)

  implicit val massageDeletePaywallCategory: Massage[Boolean, ResultDeletePaywallCategory] =
    (r: ResultDeletePaywallCategory) => genericParser(r.either.problem, r.either._value, identity[Boolean], ResultDeletePaywallCategory.getClass)

  implicit val massageDeleteMessage: Massage[Boolean, ResultDeleteMessage] =
    (r: ResultDeleteMessage) => genericParser(r.either.problem, r.either._value, identity[Boolean], ResultDeleteMessage.getClass)

  implicit val massageSetAvatar: Massage[Boolean, ResultSetAvatar] =
    (r: ResultSetAvatar) => genericParser(r.either.problem, r.either._value, identity[Boolean], ResultSetAvatar.getClass)

  implicit val massageDeleteAttachment: Massage[Boolean, ResultDeleteAttachment] =
    (r: ResultDeleteAttachment) => genericParser(r.either.problem, r.either._value, identity[Boolean], ResultDeleteAttachment.getClass)

//  implicit val massageLogin: Massage[String, ResultLogin] =
//    (r: ResultLogin) => genericParser(r.either.problem, r.either._value, identity[String], ResultLogin.getClass)

  implicit val massageSetEmail: Massage[String, ResultSetEmail] =
    (r: ResultSetEmail) => genericParser(r.either.problem, r.either._value, identity[String], ResultSetEmail.getClass)

  implicit val massageCreateIdentityBearerAttribute: Massage[String, ResultCreateIdentityBearerAttribute] =
    (r: ResultCreateIdentityBearerAttribute) => genericParser(r.either.problem, r.either._value, identity[String], ResultCreateIdentityBearerAttribute.getClass)

  implicit val massageHostEncrypt: Massage[String, ResultHostEncrypt] =
    (r: ResultHostEncrypt) => genericParser(r.either.problem, r.either._value, identity[String], ResultHostEncrypt.getClass)

  implicit val massageHostDecrypt: Massage[String, ResultHostDecrypt] =
    (r: ResultHostDecrypt) => genericParser(r.either.problem, r.either._value, identity[String], ResultHostDecrypt.getClass)

  implicit val massageCreateUser: Massage[String, ResultCreateUser] =
    (r: ResultCreateUser) => genericParser(r.either.problem, r.either._value, identity[String], ResultCreateUser.getClass)

  implicit val massageCountMessages: Massage[Int, ResultCountMessages] =
    (r: ResultCountMessages) => genericParser(r.either.problem, r.either._value, identity[Int], ResultCountMessages.getClass)

  implicit val massageSeqClientBalance: Massage[Seq[ClientBalance], ResultClientBalancesMessage] = (r: ResultClientBalancesMessage) =>
    genericParser[ClientBalances, Seq[ClientBalance]](r.either.problem, r.either._value, _.clientBalance, ResultClientBalancesMessage.getClass)

  implicit val massageRegisteredProviders: Massage[Seq[RegisteredProvider], ResultRegisteredProviders] = (r: ResultRegisteredProviders) =>
    genericParser[RegisteredProviders, Seq[RegisteredProvider]](r.either.problem, r.either._value, _.providers, ResultRegisteredProviders.getClass)

  implicit val massageSeqDisplayMessages: Massage[Seq[DisplayMessage], ResultDisplayMessages] = (r: ResultDisplayMessages) =>
    genericParser[DisplayMessages, Seq[DisplayMessage]](r.either.problem, r.either._value, _.msgs, ResultDisplayMessages.getClass)

  implicit val massageSeqContacts: Massage[Seq[RpcContact], ResultSeqContacts] =
    (r: ResultSeqContacts) => genericParser[RpcContacts, Seq[RpcContact]](r.either.problem, r.either._value, _.contacts, ResultSeqContacts.getClass)

  implicit val massageDisplayMessage: Massage[DetailedDisplayMessage, ResultDetailedDisplayMessage] = (r: ResultDetailedDisplayMessage) =>
    genericParser[RpcDetailedDisplayMessage, DetailedDisplayMessage](r.either.problem, r.either._value, DetailedDisplayMessageAdapter.adapt, ResultDetailedDisplayMessage.getClass)

  implicit val massageSeqCatPaywall: Massage[Seq[PaywallCategory], SeqPaywallCategoryResult] = (r: SeqPaywallCategoryResult) =>
    genericParser[SeqPaywallCategory, Seq[PaywallCategory]](r.either.problem, r.either._value, _.categories, SeqPaywallCategoryResult.getClass)

  implicit val massageProviderCharge: Massage[Option[Option[Long]], ResultProviderCharge] =
    (r: ResultProviderCharge) => (r.either.problem, r.either._value) match {
      case (Some(p), None) => problem(p)
      case (None, v) => success(v.map(_.charge))
      case (Some(p), Some(v)) => problem(ParsingProblem(msg = s"Unexpected ResultProviderCharge: both $p and $v"))
    }

  implicit val massageAvatar: Massage[Option[Array[Byte]], ResultAvatar] =
    (r: ResultAvatar) => (r.either.problem, r.either._value) match {
      case (Some(p), None) => problem(p)
      case (None, v) => success(v.map(_.toByteArray))
      case (Some(p), Some(v)) => problem(ParsingProblem(msg = s"Unexpected ResultAvatar: both $p and $v"))
    }

  implicit val massageGetHashedIdentityBearerAttribute: Massage[Option[String], ResultGetHashedIdentityBearerAttribute] =
    (r: ResultGetHashedIdentityBearerAttribute) => (r.either.problem, r.either._value) match {
      case (Some(p), None) => problem(p)
      case (None, v) => success(v)
      case (Some(p), Some(v)) => problem(ParsingProblem(msg = s"Unexpected ResultGetHashedIdentityBearerAttribute: both $p and $v"))
    }
}
