package sss.openstar.ui.rpc



//import com.twitter.util.SynchronizedLruMap


class DisplayMessageCache(loadById: (SessionId, Long) => Result[DetailedDisplayMessage]) extends BridgeLogging {

  //private lazy val lruUserCache = new SynchronizedLruMap[String, SynchronizedLruMap[Long, DetailedDisplayMessage]](100)


  private def loadAndHandleErrors(sessId: SessionId, msgId: Long): Result[DetailedDisplayMessage] =
    loadById(
      sessId,
      msgId
    )

  def invalidate(sessId: SessionId, msgId: Long): Unit = () /*{
    lruUserCache.get(sessId) match {
      case None =>
        log.warning(s"Trying to remove msgId $msgId from non existing session $sessId")
      case Some(lruMessageCache) =>
        lruMessageCache.remove(msgId)
    }
  }*/

  def apply(sessId: SessionId, msgId: Long): Result[DetailedDisplayMessage] =
    loadAndHandleErrors(sessId, msgId)
    // Cannot cache as there no way to invalidate a message in the cache as a result of a new child. Doh!
    //
    /*lruUserCache.get(sessId) match {
      case None =>
        lruUserCache.update(sessId, new SynchronizedLruMap[Long, DetailedDisplayMessage](100))
        apply(sessId, msgId)
      case Some(lruMessageCache) =>
        lruMessageCache.get(msgId) match {
          case None =>
            loadAndHandleErrors(sessId, msgId) map { newDm =>
              lruMessageCache.update(msgId, newDm)
              newDm
            }
          case found => found
        }
    }*/

}
