package sss.openstar.ui


import java.io.OutputStream

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.{Failure, Success, Try}

package object rpc {

  val defaultMaxRpcCallWait: Duration = 5.seconds

  object ProblemTypes {
    val unknownSession: String = "unknownsession"
    val creditTooLow: String = "creditlow"
  }

  //must match back end types
  object MessagePayloadType extends Enumeration {
    type MessagePayloadType = Value
    val BrokenMessageType = Value(0.toByte)
    val EncryptedMessageType = Value(1.toByte)
    val ChessMessageType = Value(2.toByte)
    val NewQuorumMemberType = Value(3.toByte)
    val TicTacToeType = Value(4.toByte)
    //val PaywalledMessageType = Value(5.toByte)
    val NewContactType = Value(6.toByte)
    //val CompletedNewContactType = Value(7.toByte)
  }

  case class SessionId(value: String) extends AnyVal
  case class UserId(value: String) extends AnyVal

  case class PaywallCategoryDep(category: String, amount: Long)

  trait BaseResult {
    def getProblem: AppError
    def problem: Option[AppError] = Option.when(getProblem != AppError.defaultInstance)(getProblem)
  }

  type Result[R] = Either[Problem, R]
  // boolean used to invoke type checking, Unit can result in ignoring Futures
  type ResultOk = Either[Problem, Boolean]
  type AppResult[R] = Either[ApplicationProblem, R]

  sealed trait Problem {
    val code: String = ""
    val msg: String

    def toAppError: AppError = AppError(code, msg)
  }

  object CommunicationProblem {
    def apply(dur: Duration): CommunicationProblem = CommunicationProblem(s"Communication Lost, no response in $dur")
  }

  case class CommunicationProblem(msg: String = "Communication Lost") extends Problem

  case class UnExpectedProblem(msg: String = "Apologies, Internal Error - see logs") extends Problem

  case class ApplicationProblem(override val code: String, msg: String) extends Problem

  case class ParsingProblem(override val code: String = "ParsingProblem", msg: String) extends Problem

  case object UnknownSession extends Problem {
    val msg: String = ProblemTypes.unknownSession
  }

  case class FileUploadOutputStream(outputStream: OutputStream, done: Future[UploadFileDetails])

  case object CreditTooLowProblem extends Problem {
    val msg: String = ProblemTypes.creditTooLow
  }

  def problem[T](msg: String): Result[T] = Left(ApplicationProblem("InternalError", msg))

  def problem[T](e: Throwable): Result[T] = problem(e.getMessage)

  def problem[T](p : Problem): Result[T] = Left(p)

  def problem[T](p : AppError): Result[T] = Left(ApplicationProblem(p.code, p.msg))

  def problem[T](problems: Seq[Problem]): Result[T] = problem(
    problems
      .foldLeft("")((acc, p) => s"$acc, ${p.msg}")
  )

  def success[T](t:T): Result[T] = Right(t)

  def ok(): Result[Boolean] = success(true)

  def check[T](chk: Boolean, err: String)(f: => Result[T]): Result[T] = {
    if(chk) f
    else problem(err)
  }


  def tryResult[T](f: => Result[T]): Result[T] = Try(f) match {
    case Failure(e) => problem(e.getMessage)
    case Success(value) => value
  }

}
