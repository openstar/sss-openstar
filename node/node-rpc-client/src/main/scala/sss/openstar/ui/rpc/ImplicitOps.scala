package sss.openstar.ui.rpc


import java.time.LocalDateTime
import java.util.logging.Level

import io.grpc.Status
import sss.openstar.ui.rpc.MessagePayloadType.MessagePayloadType

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future, TimeoutException}
import scala.util.{Failure, Success, Try}

object ImplicitOps {


  import BridgeLogging.log

  implicit class DisplayOps(val d: DisplayMessage) extends AnyVal {
    def receivedAt: LocalDateTime = MilliUtils.getFromMilli(d.receivedAtInternal)
    def msgPayloadType: MessagePayloadType = MessagePayloadType(d.msgPayloadTypeInternal)
  }

  implicit class DetailedDisplayOps(val d: RpcDetailedDisplayMessage) extends AnyVal {
    def msgPayloadType: MessagePayloadType = MessagePayloadType(d.msgPayloadTypeInternal)
    def receivedAt: LocalDateTime = MilliUtils.getFromMilli(d.receivedAtInternal)
  }

  implicit class ReadReceiptOps(val rr: ReadReceipt) extends AnyVal {
    def whenDate: Option[LocalDateTime] = rr.when.map(MilliUtils.getFromMilli)
  }

  implicit class FutureOps[T](awaitable: Future[Result[T]]) {
    def await(implicit dur: Duration): Result[T] = {
      Try(Await.result(awaitable, dur)) match {
        case Failure(e: TimeoutException) =>
          log.log(Level.FINEST, "Bridge lost", e)
          Left(CommunicationProblem(dur))

        case Failure(e: io.grpc.StatusRuntimeException) if e.getStatus.getCode == Status.UNAVAILABLE.getCode =>
          Left(CommunicationProblem("Server unreachable, retry..."))

        case Failure(e: io.grpc.StatusRuntimeException)=>
          log.log(Level.WARNING, "Exception across bridge", e)
          Left(CommunicationProblem(s"Problem communicating with server ${e.getStatus.getCode}..."))

        case Failure(e) =>
          log.log(Level.WARNING, "Exception across bridge", e)
          Left(UnExpectedProblem())
        case Success(t) => t
      }
    }
  }


  implicit class RpcTryOps[T](r: Try[T]) {
    def asResult: Result[T] = r match {
      case Success(result) => success(result)
      case Failure(e) => problem(e)
    }
  }

  implicit class RpcOptOps[T](r: Option[T]) {
    def asResult(err: String): Result[T] = r match {
      case None =>
        problem(err)
      case Some(r) =>
        success(r)
    }

    def asResult(prob: Problem): Result[T] = r match {
      case None =>
        problem(prob)
      case Some(r) =>
        success(r)
    }
  }
}
