package sss.openstar.ui.rpc

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.scaladsl._

import scala.concurrent._
import scala.concurrent.duration._
import scala.util.{Failure, Success, Try}

object TestStreams {

  implicit val actorSystem: ActorSystem =  ActorSystem()

  def tryCol(exOpt: Option[Exception] = None) = Try {
    exOpt match {
      case Some(ex) => throw ex
      case None => List(1,2,3,4,5)
    }
  }

  def main(args: Array[String]): Unit = {

    import actorSystem.dispatcher
    val src1: Source[Int, NotUsed] = Source(tryCol(Some(new RuntimeException())).get)

    val o: Future[Seq[Int]] = src1.runWith(Sink.seq)

    o.onComplete({
      case Failure(e) => println(e)
      case Success(e) => println(e)
    })
    val r = Await.ready(o, 1.second)

    println("End")
    actorSystem.terminate()
  }

}
