package sss.openstar.ui.rpc

import java.time.{Instant, LocalDateTime, ZoneOffset}


object MilliUtils {

  def getNow(): Long = getAsMilli(LocalDateTime.now())
  def getAsMilli(l: LocalDateTime): Long = l.atOffset(ZoneOffset.UTC).toInstant.toEpochMilli
  def getFromMilli(l: Long): LocalDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(l), ZoneOffset.UTC)

}
/*
import java.time.{Instant, LocalDateTime, ZoneOffset}

import sss.openstar.ui.rpc.DisplayMessageDep.{DetailedDisplayMessage, DisplayBasics}
import sss.openstar.ui.rpc.MessagePayloadType.MessagePayloadType



object DisplayMessageDep {

  def getNow(): Long = getAsMilli(LocalDateTime.now())
  def getAsMilli(l: LocalDateTime): Long = l.atOffset(ZoneOffset.UTC).toInstant.toEpochMilli
  def getFromMilli(l: Long): LocalDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(l), ZoneOffset.UTC)


  def apply(id: Long,
            amount: Long,
            from: String,
            subject: String,
            receivedAt: LocalDateTime,
            childIds: Option[Seq[Long]],
            msgPayloadType: MessagePayloadType): DisplayBasics = {
    DisplayMessageDep(id, amount, from, subject, getAsMilli(receivedAt), childIds, msgPayloadType.id)
  }

  sealed trait DisplayBasics {
    val id: Long
    val author: String
    val subject: String
    val msgPayloadTypeInternal: Int
    val receivedAtInternal: Long
    val msgPayloadType: MessagePayloadType = MessagePayloadType(msgPayloadTypeInternal)
    val receivedAt: LocalDateTime = DisplayMessageDep.getFromMilli(receivedAtInternal)
  }

  sealed trait DetailedDisplayMessage extends DisplayBasics {

    override val subject: String = if(body.length > 50) body.substring(0, 50) else body
    val body: String
  }

}

case class DisplayMessageDep(
                           id: Long,
                           amount: Long,
                           author: String,
                           subject: String,
                           receivedAtInternal: Long,
                           childIds: Option[Seq[Long]],
                           msgPayloadTypeInternal: Int
                         ) extends DisplayBasics



case class ReadReceipt(identity: String, internalWhen: Option[Long]) {
  lazy val when: Option[LocalDateTime] = internalWhen map (DisplayMessageDep.getFromMilli)
}

case class DetailedEncryptedMessage(
                                     id:Long,
                                     body:String,
                                     files: Seq[FileData],
                                     amount: Int,
                                     author: String,
                                     tos: Set[String],
                                     readReceipts: Seq[ReadReceipt],
                                     parentIdOpt: Option[Long],
                                     children: Seq[DetailedEncryptedMessage],
                                     msgPayloadTypeInternal: Int,
                                     receivedAtInternal: Long
                                   ) extends DetailedDisplayMessage


case class DetailedNewContactMessage(id: Long,
                                     author: String,
                                     body: String,
                                     msgPayloadTypeInternal: Int,
                                     receivedAtInternal: Long,
                                     invitorCategory: String,
                                     invitorCurrentAmount: Long,
                                     invitee: String,
                                     inviteePaywalls: Either[Seq[PaywallCategory],PaywallCategory],
                                     parentIdOpt: Option[Long]
                                    ) extends DetailedDisplayMessage

*/