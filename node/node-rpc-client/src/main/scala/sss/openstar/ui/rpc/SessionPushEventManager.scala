package sss.openstar.ui.rpc

import java.util.concurrent.atomic.AtomicInteger

import akka.actor.{ Actor, ActorLogging, ActorSystem, Cancellable, PoisonPill, Props }
import sss.openstar.ui.rpc.SessionPushEventManager.PushEventType.PushEventType
import sss.openstar.ui.rpc.SessionPushEventManager._

import scala.concurrent.duration._
import scala.util.Try

object SessionPushEventManager {

  private case class PollInterval(intervalInSeconds: Int)

  private val registrationCount: AtomicInteger = new AtomicInteger(0)

  type PushEventConsumer = PushEvent => Unit

  case class RegistrationId(id: Int)

  case class Registration(id: RegistrationId, consumer: PushEventConsumer)

  object PushEventType extends Enumeration {
    type PushEventType = Value
    val NewBalanceType = Value(0)
    val NewMsgType = Value(1)
    val PollType = Value(2)
    val NewClientBalanceType = Value(3)
    val GenericEventType = Value(4)
  }

  trait PushEvent {
    val eventType: PushEventType
  }

  case class NewBalancePushEvent(balance: Long) extends PushEvent {
    override val eventType: PushEventType = PushEventType.NewBalanceType
  }

  case class NewClientBalancePushEvent(balance: Long, client: String, delta: Long) extends PushEvent {
    override val eventType: PushEventType = PushEventType.NewClientBalanceType
  }

  case class NewMessagePushEvent(msg: Long) extends PushEvent {
    override val eventType: PushEventType = PushEventType.NewMsgType
  }

  case object PollEvent extends PushEvent {
    override val eventType: PushEventType = PushEventType.PollType
  }

  case class GenericPushEvent(evType: String, payload: Array[Byte]) extends PushEvent {
    override val eventType: PushEventType = PushEventType.GenericEventType
  }
  case class SessionEventFromBridge(sessId: SessionId, ev: Event)

  def apply(sessId: SessionId, pollIntervalSeconds: Int = 5)(implicit
    as: ActorSystem
  ): SessionPushEventManager = new SessionPushEventManager(sessId, pollIntervalSeconds)
}

class SessionPushEventManager(sessId: SessionId, pollIntervalSeconds: Int)(implicit as: ActorSystem) {

  private val ref = as.actorOf(Props(SessionPushEventActor))

  import as.dispatcher

  as.eventStream.subscribe(ref, classOf[SessionEventFromBridge])

  def register(t: PushEventType, f: PushEventConsumer): RegistrationId = {
    val nextId = SessionPushEventManager.registrationCount.getAndIncrement()
    val regId = RegistrationId(nextId)
    val reg = Registration(regId, f)
    ref ! (t, reg)
    regId
  }

  def deRegister(id: RegistrationId): Unit = ref ! id

  def stop(): Unit =
    ref ! PoisonPill

  private def initCallbacks: Map[PushEventType, List[Registration]] =
    Map.empty.withDefaultValue(List.empty)

  private object SessionPushEventActor extends Actor with ActorLogging {

    private var pollCancellable: Option[Cancellable] = None

    private var callbacks: Map[PushEventType, List[Registration]] = initCallbacks

    override def receive: Receive = {
      case SessionEventFromBridge(`sessId`, ev) if ev.event.isNewBalance =>
        callbacks(PushEventType.NewBalanceType).foreach { r =>
          Try(r.consumer(NewBalancePushEvent(ev.getNewBalance))).recover { case e =>
            log.warning(s"Event Consumer id={} type {} failed - {}", r.id, PushEventType.NewBalanceType, e)
          }
        }

      case SessionEventFromBridge(`sessId`, ev) if ev.event.isMsg =>
        callbacks(PushEventType.NewMsgType).foreach { r =>
          Try(r.consumer(NewMessagePushEvent(ev.getMsg))).recover { case e =>
            log.warning(s"Event Consumer id={} type {} failed - {}", r.id, PushEventType.NewMsgType, e)
          }
        }

      case SessionEventFromBridge(`sessId`, ev) if ev.event.isNewClientBalance =>
        callbacks(PushEventType.NewClientBalanceType).foreach { r =>
          Try(
            r.consumer(
              NewClientBalancePushEvent(
                ev.getNewClientBalance.balance,
                ev.getNewClientBalance.client,
                ev.getNewClientBalance.delta
              )
            )
          ).recover { case e =>
            log.warning(s"Event Consumer id={} type {} failed - {}", r.id, PushEventType.NewMsgType, e)
          }
        }
      case SessionEventFromBridge(`sessId`, ev) if ev.event.isGenericEvent =>
        callbacks(PushEventType.GenericEventType).foreach { r =>
          Try(
            r.consumer(
              GenericPushEvent(
                ev.getGenericEvent.eventType,
                ev.getGenericEvent.payload.toByteArray
              )
            )
          ).recover { case e =>
            log.warning(s"Event Consumer id={} type {} failed - {}", r.id, PushEventType.GenericEventType, e)
          }
        }

      case (t: PushEventType, reg: Registration) =>
        callbacks += (t -> (reg +: callbacks(t)))

      case idToUnregister: RegistrationId =>
        callbacks =
          callbacks.view.mapValues(_.filterNot(_.id == idToUnregister)).toMap.withDefaultValue(List.empty)

      case p @ PollInterval(interval) =>
        callbacks(PushEventType.PollType).foreach { r =>
          Try(r.consumer(PollEvent)).recover { case e =>
            log.warning(s"Event Consumer id={} type {} failed - {}", r.id, PushEventType.PollType, e)
          }
        }
        pollCancellable = Option(context.system.scheduler.scheduleOnce(interval.seconds, self, p))

      case x =>
        log.debug("Unhandled:{}", x)
    }

    override def preStart(): Unit =
      pollCancellable = Option(
        as.scheduler.scheduleOnce(
          pollIntervalSeconds.seconds,
          ref,
          PollInterval(pollIntervalSeconds)
        )
      )

    override def postStop(): Unit =
      pollCancellable foreach (_.cancel())
  }

}
