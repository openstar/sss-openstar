package sss.openstar.ui.rpc


import akka.actor.ActorSystem
import akka.stream.scaladsl.Sink
import CreateLinkResult.Either._

import java.util.logging.Level
import scala.concurrent.{ExecutionContext, Future, Promise}

class AuthPlusClient(authPlusRpcClient: AuthPlusService)(implicit as: ActorSystem) extends BridgeLogging {

  import as.dispatcher

  def createNewLinkForUser(user: String, tag: String, keyType: String, desc: String): (Future[String], Future[UserTagKey]) = {
    val in = CreateLinkMessage.of(
      Some(UserTagKey.defaultInstance
        .withUser(user)
        .withKeyType(keyType)
        .withTag(tag)),
      desc
    )
    val urlPromise = Promise[String]()
    val userKey = Promise[UserTagKey]()
    val sink = Sink.foreach[CreateLinkResult]{
      case CreateLinkResult(Url(qrCodeUrl), _) => urlPromise.success(qrCodeUrl)
      case CreateLinkResult(WhoTagKey(userTagKey), _) => userKey.success(userTagKey)
      case anythingElse => log.log(Level.FINE, s"Got $anythingElse")
    }

    authPlusRpcClient
      .createLink(in)
      .runWith(sink)
      .recover(e => userKey.failure(e))

    (urlPromise.future, userKey.future)
  }

}
