package sss.openstar.ui.rpc

import scala.concurrent.{ExecutionContext, Future}

class SignerClient(signerRpcClient: SignerService)(implicit ec: ExecutionContext) {

  def allUserSigners(user: String): Future[Seq[ResultSignerDetails]] = {
    signerRpcClient
      .userSigners(UserMessage.of(user))
      .map(_.details)
  }

  def allSignerDescriptors(): Future[Seq[SignerDescriptor]] = {
    signerRpcClient
      .allSigners(AllSignersMessage.of())
      .map(_.descriptors)
  }

}
