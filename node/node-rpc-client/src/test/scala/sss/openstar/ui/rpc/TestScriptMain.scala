package sss.openstar.ui.rpc


import sss.openstar.ui.rpc.ImplicitOps._


object TestScriptMain extends BridgeTestSupport{


  import BridgeTestSupport.{Opts, log, toOpt}


  def main(args: Array[String]): Unit = {

    val alice3 = "alice3"
    val alice2 = "alice2"
    val alice = "alice"

    args.head match {
      case `alice` => doAlice()
      case `alice2` => doAlice2()
      case `alice3` => doAlice3()
    }


    import bridge._

    def doAlice2() = {
      implicit val sessId2 = generateSessionId()
      bridge.createUser(sessId2.value, alice2, "passworD0!", "passworD0!", None).await.asOpt match {
        case None => log(s"$alice2 already created?")
        case Some(_) =>
          toOpt(bridge.balance(sessId2.value).await).map { bal =>
            log(s"balance $bal")
          }
      }

      bridge.login(sessId2.value, alice2, "passworD0!", None).await.asOpt.map { _ =>


        bridge.addProvider(sessId2.value, alice).await.asOpt.map { _ =>
          log(s"Set providers")
        }

        bridge.balance(sessId2.value).await.asOpt.map { bal =>
          log(s"Balance $alice2 $bal")
          bridge.fetchMessages(sessId2.value, 0, 10).await.asOpt.foreach { msgs =>
            msgs.foreach(dm => log(dm))
            msgs
              .find(_.author == alice) foreach { msg =>
              val newParentId = Some(msg.id)
              val enc = DecryptedMessage("Hello Child from Alice 2", 1, Seq(alice), 10, newParentId,
                newGuid())
              bridge.sendEncryptedMessage(sessId2.value, enc).await.asOpt.map { _ =>
                log(s"sendEncryptedMessage2 $enc ok")
              }
            }
          }
        }
      }
    }

    def doAlice3() = {
      implicit val sessId = generateSessionId()
      bridge.createUser(sessId.value, alice3, "passworD0!", "passworD0!", None).await.asOpt match {
        case None => log(s"$alice3 already created?")
        case Some(_) =>
          bridge.balance(sessId.value).await.asOpt.map { bal =>
            log(s"balance $bal")
          }
      }

      bridge.login(sessId.value, alice3, "passworD0!", None).await.asOpt.map { _ =>

        bridge.addProvider(sessId.value, alice).await.asOpt.map { _ =>
          log(s"Set providers")
        }

        bridge.balance(sessId.value).await.asOpt.map { bal =>
          log(s"Balance $alice3 $bal")
          bridge.fetchMessages(sessId.value, 0, 10).await.asOpt.foreach { msgs =>
            msgs.foreach(dm => log(dm))
            msgs
              .find(_.author == alice) foreach { msg =>
              val newParentId = Some(msg.id)
              val enc = DecryptedMessage("Hello Child from Alice 3", 1, Seq(alice), 10, newParentId,
                newGuid())
              bridge.sendEncryptedMessage(sessId.value, enc).await.asOpt.map { _ =>
                log(s"sendEncryptedMessage2 $enc ok")
              }
            }
          }
        }
      }
    }

    def doAlice() = {
      implicit val sessId = generateSessionId()


      bridge.login(sessId.value, alice, "passworD0!", None).await.asOpt.map { _ =>


        bridge.balance(sessId.value).await.asOpt.map { bal =>
          if (bal <= 0) {
            println("No money, game over")
          } else {
            val enc = DecryptedMessage("Hello Alice2", 10, Seq(alice2), 10, None,
              newGuid())
            bridge.sendEncryptedMessage(sessId.value, enc).await.asOpt.map { _ =>
              log(s"sendEncryptedMessage $enc ok")

              log("Messages to follow")
              bridge.fetchMessages(sessId.value, 0, 10).await.asOpt.foreach { msgs =>
                msgs.foreach(dm => log(dm))
                /*msgs.headOption map { msg =>
                  val newParentId = Some(msg.id)
                  val enc = EncryptedMessage("Hello Child", 10, Seq(alice2, alice), 10, newParentId)
                  bridge.sendEncryptedMessage(sessId, enc).await.asOpt.map { _ =>
                    log(s"sendEncryptedMessage2 $enc ok")
                    log("Messages to follow")
                    bridge.fetchMessages(sessId, 0, 10).asOpt.foreach { msgs =>
                      msgs.foreach(dm => log(dm))
                      /*msgs.headOption map { msg =>
                      val newParentId = Some(msg.id)
                      val enc = EncryptedMessage("Hello Child 2", 10, Seq("alice"), 10, newParentId)
                      bridge.sendEncryptedMessage(sessId, enc).await.asOpt.map { _ =>
                        log(s"sendEncryptedMessage3 $enc ok")
                        log("Messages to follow")
                        bridge.fetchMessages(sessId, 0, 10).asOpt.foreach { msgs =>
                          msgs.foreach(dm => log(dm))
                        }
                      }
                    }*/
                    }
                  }
                }*/
              }
            }
          }
        }
      }
    }
  }
}
