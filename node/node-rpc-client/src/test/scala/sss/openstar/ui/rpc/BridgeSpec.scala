package sss.openstar.ui.rpc


import org.scalatest.Ignore
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.openstar.ui.rpc.BridgeTestSupport._
import sss.openstar.ui.rpc.ImplicitOps._

@Ignore
class BridgeSpec extends AnyFlatSpec with Matchers with BridgeTestSupport {

  val userWithMoney = "alice"

  lazy implicit val sessIdMoney = generateSessionId()
  lazy implicit val sessId1 = generateSessionId()
  lazy implicit val user1 = generateUser()

  lazy implicit val sessId2 = generateSessionId()
  lazy implicit val user2 = generateUser()

  //import bridge.maxCallWaitDuration


  it should "send a message and read it" in {
    bridge.login(sessIdMoney.value, userWithMoney, "passworD0!", None).await.asOpt.map { _ =>
      bridge.createUser(sessId2.value, user2, "passworD0!", "passworD0!", None).await.asOpt match {
        case None => log(s"$user2 already created?")
        case Some(_) =>
      }
      bridge.addProvider(sessId2.value, userWithMoney).await

      bridge.login(sessId2.value, user2, "password", None).await.asOpt.map { _ =>

        log(s"Logged in $user2")
        val hiFromMoney = "Hello from alice 2"
        val replyToMoney = "Thanks for the message to 2"
        val msg = DecryptedMessage(hiFromMoney, 2000, Seq(user2), 5, None, newGuid())
        val indexOflastMsgForSessid2 = getLastIndex(sessId2).get
        bridge.sendEncryptedMessage(sessIdMoney.value, msg).await(debugDuration).asOpt map { _ =>
          log(s"Send 1 ok")
          val msgs = fetchUntilSuccessful(sessId2, indexOflastMsgForSessid2).get
          val msg = msgs.last
          assert(msg.subject == hiFromMoney, "Wrong message retrieved?")
        }
      }
    }

  }

  "A user" should "send and receive messages" in {

    bridge.login(sessIdMoney.value, userWithMoney,  "password", None).await.asOpt.map { _ =>
      bridge.createUser(sessId1.value, user1, "password", "password", None).await.asOpt match {
        case None => log(s"$user1 already created?")
        case Some(_) =>
          bridge.balance(sessId1.value).await.asOpt.foreach { bal =>
            log(s"balance $bal")
          }

      }
      bridge.addProvider(sessId1.value, userWithMoney).await

      bridge.login(sessId1.value,  user1,"password", None).await.asOpt.map { _ =>

        log(s"Logged in $user1")
        val hiFromMoney = "Hello from alice"
        val replyToMoney = "Thanks for the message"
        val msg = DecryptedMessage(hiFromMoney, 2000, Seq(user1), 5, None, newGuid())
        bridge.sendEncryptedMessage(sessIdMoney.value, msg).await(debugDuration).asOpt map { _ =>
          log(s"Send 1 ok")
          val indexOflastMsgForMoney = getLastIndex(sessIdMoney).get
          val msgs = fetchUntilSuccessful(sessId1).get
          val msg = msgs.last
          assert(msg.subject == hiFromMoney, "Wrong message retrieved?")
          val reply = DecryptedMessage(replyToMoney, 2, Seq(userWithMoney), 5, Some(msg.id), newGuid())
          bridge.sendEncryptedMessage(sessId1.value, reply).await(debugDuration).asOpt.get
          val msgs2 = fetchUntilSuccessful(sessIdMoney, indexOflastMsgForMoney).get
          val replyReceviedByMoney = msgs2.last
          assert(replyReceviedByMoney.subject == replyToMoney, "Wrong message retrieved?")
          bridge.fetchMessage(sessIdMoney.value, replyReceviedByMoney.id - 1).await.asOpt match {
            case Some(foundParent: DetailedEncryptedMessage) =>
              log("CHILDREN")
              val children = foundParent.children
              children.foreach(println)
              assert(children.contains(replyReceviedByMoney.id))
            case _ => fail("couldn't find child")
          }
        }
      }
    }
  }

  "A user" should "create contacts" in {

    val bMoney = bridge.balance(sessIdMoney.value).await.asOpt.get
    val user1Money = bridge.balance(sessId1.value).await.asOpt.get
    log(s"Money user balance $bMoney, user1 balance $user1Money")

    bridge.listPaywallCategories(sessIdMoney.value).await.asOpt.map { cats =>
      val someCategory = cats.head.category
      val sendThis = NewContactMessage(userWithMoney, user1, someCategory, None)
      val indexOflastMsg = getLastIndex(sessId1).get
      val indexOfMsgBeforeReply = getLastIndex(sessIdMoney).get
      bridge.sendNewContactMessage(sessIdMoney.value, sendThis).await.asOpt.map { _ =>
        log("Contact ok")
        val msgs2 = fetchUntilSuccessful(sessId1, indexOflastMsg).get
        msgs2.last match {
          case d: DisplayMessage if (d.msgPayloadTypeInternal == MessagePayloadType.NewContactType.id)=>
            log("Contact received ok")
            bridge.listPaywallCategories(sessId1.value).await.asOpt.map { cats =>
              val replyCategory = cats.head.category
              val sendReply = NewContactMessage(user1, userWithMoney, replyCategory, Some(d.id))

              bridge.sendNewContactMessage(sessId1.value, sendReply).await.asOpt.map { _ =>
                log(s"Waiting for unpdated $indexOfMsgBeforeReply")
                sleep(s"waiting for updated existing message in inbox")
                val msgsWithReply = fetchUntilSuccessful(sessIdMoney, indexOfMsgBeforeReply).get
                msgsWithReply.foreach(println)
              }
            }
          case _ => ???
          }
      }
    }
  }
}