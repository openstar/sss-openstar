package sss.openstar.ui.rpc

import sss.openstar.ui.rpc.ImplicitOps.FutureOps

import java.io._
import java.util.Date
import scala.concurrent.Await
import scala.concurrent.duration._
object TestFileUploadMain extends BridgeTestSupport {

  import BridgeTestSupport.Opts

  implicit val timeout: Duration = 5.seconds
  def writeToOs(bytes: Array[Byte], os: OutputStream): Unit = {

    try {
         os.write(bytes)
    } finally {
      os.flush()
      os.close()
    }
  }

  def main(args: Array[String]): Unit = {

    implicit val sessId = generateSessionId()
    val someUser = "acrazyuser"
    val testFileName = "testFileOne" + new Date().getTime.toString

    bridge.createUser(sessId.value, someUser, "password", "password", None).await.asOpt match {
      case Some(_) =>
      case None =>
        bridge.login(sessId.value, someUser, "password", None).await.asOpt.get
    }

    val guid = newGuid()
    val os = bridge.createAttachmentUploadOutputStream(sessId, guid.toByteArray, testFileName, "testFileOneType")
    val f = new File("/home/alan/extra2/data/Video/The.Interview.2014.WEB-DL.XviD.MP3-RARBG.avi")
    val fin = new FileInputStream(f)

    val beginUp = new Date().getTime
    transferTo(fin, os.outputStream)
    os.outputStream.close()
    fin.close()


    //val bytes = (0 to 100).map(_.toByte).toArray
    //writeToOs(bytes, os.outputStream)

    val fd = Await.result(os.done, 10.minute)
    val upDone = new Date().getTime



    val in = bridge.downloadAttachment(sessId, guid.toByteArray, testFileName )

    val fout = new File("/home/alan/Downloads/testout.delete")
    val foutOs = new FileOutputStream(fout)
    transferTo(in, foutOs)
    foutOs.close()
    in.close()
    val downDone = new Date().getTime
    println(s"Download done ${downDone - upDone}")
    //inBs.foreach(println)

  }

  val DEFAULT_BUFFER_SIZE = 812

  def transferTo(input: InputStream, out: OutputStream): Long = {
    var transferred: Long = 0
    val buffer: Array[Byte] = new Array[Byte](DEFAULT_BUFFER_SIZE)
    var read: Int = input.read(buffer, 0, DEFAULT_BUFFER_SIZE)
    while (read >= 0) {
      out.write(buffer, 0, read)
      transferred += read
      read = input.read(buffer, 0, DEFAULT_BUFFER_SIZE)
    }
    transferred
  }

}
