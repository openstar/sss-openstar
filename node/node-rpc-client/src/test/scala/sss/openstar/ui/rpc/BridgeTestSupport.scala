package sss.openstar.ui.rpc

import java.util.UUID

import akka.actor.ActorSystem
import com.google.protobuf.ByteString
import com.typesafe.config.ConfigFactory
import sss.openstar.ui.rpc.ImplicitOps._

import scala.annotation.tailrec
import scala.concurrent.duration._
import scala.util.Random

object BridgeTestSupport {

  implicit val dur : Duration = 10.seconds

  def log(msg: Any): Unit = println(msg)

  implicit class Opts[T](val r: Result[T]) extends AnyVal {
    def asOpt: Option[T] = toOpt(r)
  }

  def toOpt[T](result: Result[T]): Option[T] = result match {
    case Left(p) =>
      log(p.toString)
      None
    case Right(r) => Some(r)
  }
}

trait BridgeTestSupport {

  import BridgeTestSupport.{Opts, dur}



  def newGuid(): ByteString  = {
    val ary = new Array[Byte](16)
    Random.nextBytes(ary)
    ByteString.copyFrom(ary)
  }

  def failure(m: String = "TEST FAILED!!!!!!") = throw new RuntimeException(m)

  def generateSessionId(): SessionId = SessionId(Random.nextString(20))


  def sleep(explanatoryMessage: String)(implicit duration: Duration) =
    Thread.sleep(duration.toMillis)

  def generateUser(): String = "bridgetest" + UUID.randomUUID().toString.substring(0, 8)

  lazy val config = ConfigFactory.load()

  val debugDuration = 60.seconds

  lazy implicit val bridge = BridgeClient("openstar.BridgeService")(ActorSystem("TestBridgeSystem"), debugDuration)

  @tailrec
  final def getLastIndex(sessId: SessionId, startIndex: Int = 0): Option[Int] = {
    bridge.fetchMessages(sessId.value, startIndex, 10, false).await.asOpt match {
      case Some(msgs) if (msgs.size != 10) =>
        Some(startIndex + msgs.size)
      case _ =>
        getLastIndex(sessId, startIndex + 10)
    }
  }

  @tailrec
  final def fetchUntilSuccessful(sessId: SessionId, lastCount: Int = 0, retries: Int = 10): Option[Seq[DisplayMessage]] = {
    if (retries == 0) None
    else {
      bridge.fetchMessages(sessId.value, lastCount, 10, false).await.asOpt match {
        case Some(s) if s.nonEmpty => Some(s)
        case _ =>
          Thread.sleep(1000)
          fetchUntilSuccessful(sessId, lastCount, retries - 1)
      }
    }
  }
}
