nodes="nodea nodeb"
for node in $nodes
do
  mkdir -p volume/$node/logs
  mkdir -p volume/$node/three-nodes/attachments
  mkdir -p volume/$node/three-nodes/data
  mkdir -p volume/$node/three-nodes/keys
  cp $node.defaultTag volume/$node/three-nodes/keys
done
chmod -R a+w volume
