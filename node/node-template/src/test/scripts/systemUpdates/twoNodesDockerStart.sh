# 1. Launch createVolume.sh script
# 2. Swap universal/conf/overrides.conf with overrides.conf file from this directory
# 3. Build docker image:
#   "sbt node_template/docker:publishLocal" (requires changing REPOSITORY and VER variables below) OR ALTERNATIVELY
#   "sbt node_template/docker:stage" => "docker build -t node-template ." in sss-openstar/node/node-template/target/docker/stage directory
REPOSITORY=node-template
VER=latest
NAME=nodea
NAME2=nodeb
names="$NAME $NAME2"
docker stop $names
rm -rf ./backup/*
cp -a volume/. ./backup/
for node in $names
do
  rm -rf ./volume/$node/three-nodes/attachments/*
  rm -rf ./volume/$node/three-nodes/data/*
  rm -rf ./volume/$node/three-nodes/logs/*
done
if [ -z "$VER" ]
then
      echo "\$VER is empty"
      docker start $names
else
      docker rm $names
      docker run --restart unless-stopped -d --name $NAME --network=host -e JAVA_OPTS="-DNODE_ID=$NAME -DBIND_PORT=7071 -Dnode.chainName=local-three-nodes -Dnode.chainOriginatorIds=nodea:nodeb" -v $PWD/volume/$NAME:/home/demiourgos728/openstar $REPOSITORY:$VER -password Password10! -startRpc -startHttp
      docker run --restart unless-stopped -d --name $NAME2 --network=host -e JAVA_OPTS="-DNODE_ID=$NAME2 -DBIND_PORT=7072 -Dnode.chainName=local-three-nodes -Dnode.chainOriginatorIds=nodea:nodeb" -v $PWD/volume/$NAME2:/home/demiourgos728/openstar $REPOSITORY:$VER -password Password10!
fi