package sss.openstar

import akka.actor.{ActorSystem, CoordinatedShutdown}
import com.typesafe.config.ConfigFactory
import org.scalatest.concurrent.Eventually
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.scalatest.time.{Minutes, Seconds, Span}
import sss.ancillary.Configure
import sss.openstar.test.{AssertionContext, TestCaseConfig}

import scala.concurrent.Await
import scala.concurrent.duration._

class MultiTxSpec extends AnyFlatSpec with Matchers with KeyFileWriterFixture with Eventually with Configure {

  implicit override val patienceConfig: PatienceConfig =
    PatienceConfig(timeout = scaled(Span(10, Minutes)), interval = scaled(Span(10, Seconds)))

  val defaultTimeout: FiniteDuration = 5.seconds

  private def stopActorSystem(as: ActorSystem): Unit =
    Await.result(CoordinatedShutdown(as).run(CoordinatedShutdown.unknownReason), defaultTimeout)

  private def startNode(testCaseConfig: TestCaseConfig, as: ActorSystem, additionalParams: Array[String] = Array.empty) =
    MultiTxNode.run(nodeParams ++ additionalParams, testCaseConfig, as)

  val nodeParams: Array[String] = Array("-password", "Password10!")

  // TODO fix that test (one of the issues: the same config is passed to all nodes)
  ignore should "execute the correct number of transactions" in {
    var terminating = false
    val assertion = (c: AssertionContext) => {
      if (c.totalProcessed != c.counter) fail(s"Unexpected txs count: $c")
      terminating = true
      stopActorSystem(c.as)
    }

    val testCaseConfig = TestCaseConfig(assertion, Seq("n1"))
    val as1 = ActorSystem("q1")
    val as2 = ActorSystem("q2")
    val as3 = ActorSystem("q3")
    startNode(testCaseConfig, as1, Array("-startHttp"))
    startNode(testCaseConfig, as2)
    startNode(testCaseConfig, as3)
    startNode(testCaseConfig, ActorSystem("n1"))

    eventually {
      terminating shouldBe true
      stopActorSystem(as1)
      stopActorSystem(as2)
      stopActorSystem(as3)
    }
  }

  ignore should "execute the correct number of rejected transactions" in {
    var terminating = false
    val assertion = (c: AssertionContext) => {
      if (c.totalProcessed != c.failures) fail(s"Unexpected txs count: $c")
      terminating = true
      val _ = stopActorSystem(c.as)
    }

    val testCaseConfig = TestCaseConfig(assertion, Seq("n1"), 100, Seq("q1", "q3"))
    val as1 = ActorSystem("q1")
    val as2 = ActorSystem("q2")
    val as3 = ActorSystem("q3")
    startNode(testCaseConfig, as1, Array("-startHttp"))
    startNode(testCaseConfig, as2)
    startNode(testCaseConfig, as3)
    startNode(testCaseConfig, ActorSystem("n1"))

    eventually {
      terminating shouldBe true
      stopActorSystem(as1)
      stopActorSystem(as2)
      stopActorSystem(as3)
    }
  }

}
