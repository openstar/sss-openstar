package sss.openstar

import com.typesafe.config.{Config, ConfigFactory}
import org.scalacheck.Gen
import org.scalacheck.Gen.alphaLowerChar
import org.scalatest.{BeforeAndAfterAll, Ignore}
import org.scalatest.concurrent.Eventually.eventually
import org.scalatest.concurrent.PatienceConfiguration.{Interval, Timeout}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers.{a, convertToAnyShouldWrapper}
import org.scalatest.tagobjects.Slow
import org.scalatest.time.{Seconds, Span}
import sss.ancillary.Configure
import sss.openstar.TemplateMain.TemplateNode
import sss.openstar.constructor.{TestPartialNodeConstructor, TestTemplateNodeConstructor}
import sss.openstar.session.UserSessionFixture
import sss.openstar.ui.rpc.BridgeClient

// TODO find a way to shorten the execution time of this test (it is too long even for an Integration test)
@Ignore //TODO amcs Dec21 '22 Tests borken during reorg
class TemplateNodeSpec extends AnyFlatSpec with KeyFileWriterFixture with Configure with UserSessionFixture {
  override implicit lazy val config: Config = ConfigFactory.load("templateNode/templateNodeTest.conf")
  val bridgeClientConfigName: String = "openstar.BridgeService"
  val username: String = Gen.nonEmptyListOf(alphaLowerChar).map(_.mkString).sample.get

  private val nodeBuilder: TestPartialNodeConstructor[TemplateMain.TemplateNode] = new TestPartialNodeConstructor(TestTemplateNodeConstructor.apply)
  import nodeBuilder.{newWithoutServers, newWithServers, password}

  var q1: TemplateNode = _
  var q2: TemplateNode = _

  "Template node" should "create user and login" taggedAs Slow in {

    implicit val coreBridgeClient: BridgeClient = BridgeClient(bridgeClientConfigName)
    eventually(Timeout(Span(50, Seconds)), Interval(Span(2, Seconds))) {
      createUserAndLogin(username, password)
    }
    // this foreach increases test execution time only by ~10%, thus it is probably worth to have it here
    (1 to 10).foreach(_ => createUserLoginLogoutAndLogin())
  }

  override def beforeAll(): Unit = {
    super.beforeAll()
    q1 = newWithServers(ConfigFactory.load(s"templateNode/$q1Name.conf"))
    q2 = newWithoutServers(ConfigFactory.load(s"templateNode/$q2Name.conf"))
  }

  override def afterAll(): Unit = {
    try super.afterAll()
    finally {
      try q1.shutdown()
      finally q2.shutdown()
    }
  }

  private def createUserLoginLogoutAndLogin()(implicit coreBridgeClient: BridgeClient): Unit = {
    val sess = createRandomUserAndLogin(password)
    coreBridgeClient.logout(sess.value)
    coreBridgeClient.login(sess.value, sess.value, password, None) shouldBe a [Right[_,_]]
  }

}
