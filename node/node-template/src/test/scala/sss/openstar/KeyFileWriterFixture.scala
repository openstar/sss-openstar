package sss.openstar

import org.scalatest.concurrent.Eventually.eventually
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper
import org.scalatest.{BeforeAndAfterAll, Suite}
import sss.ancillary.{Configure, Memento}
import sss.openstar.util.FileUtils

import java.nio.file.Path

trait KeyFileWriterFixture extends BeforeAndAfterAll {
  self: Suite with Configure =>

  val q1Name      = "q1"
  val q2Name      = "q2"
  val q1PulicKey  = "wBDhTyVZfiDE2Q1Kx34u4ji5gqaZi5zy7lylDDZOFU4"
  val q2PublicKey = "MtD_eHm41-htMSXRBJBidu45rVfML0y29KwBQ05X2SU"

  override def beforeAll(): Unit = {
    writeKeyFile(
      "n1",
      "T32lCyvCN18OhtSsXg0VFWWvtQ5K4znTo-HOubcjcFU:::sha1:64000:32:kvHnKm19z4FwYAL+QiCX+FZSp57CR+Vf:j6MC0zARSPoZFPyeicLCk7fpkJJ2uSGo4H5pl2tX2pk=:::ndd0NcUAV8TLnxpge6IwFQ==:::cTJhbUVnSWREQW5VTUFLVkNydUl5M1k4bjJEc2VnS2pWOGZSRndUa2tMclBsNGpWbUwvT2pNcWtGZE5XOXlPMQ"
    )
    writeKeyFile(
      "q1",
      s"$q1PulicKey:::sha1:64000:32:UXOU7OFoZpBSilgyAZzEJwNAVkdJX95k:dFi4wnuWcGV5ES/lyFlCy8B8JBKvOVBR7eogeolj+6g=:::WH70RTXF5YYAsaN4EDmwNQ==:::NjJQQzhVOEw2YnFGVC9GcDIrOWFicXVaYk5CRTJPS1EvS2E0U3ZUNFV1TkVpK1ZzblNudmZTR2pMZ2pyR1I4aQ"
    )
    writeKeyFile(
      "q2",
      s"$q2PublicKey:::sha1:64000:32:67tif+vtbX7LEV9PdtCMGarpPcBhJnPZ:+o4nupd15SNw2ToijVVy9U3qlksvXoghYzGMLb+IfjU=:::JI9dT5EMRK8uC4HaVk9gyQ==:::UXRrcHNlYkF4REhvckE0UzZjUHdjNWEzMldvVkEreFliQ2pIUWVwR2czV3NBMDYyaWwvR0FnYUFIS1VVdWZNMQ"
    )
    val p = writeKeyFile(
      "q3",
      "w6GsjRQOMYqdYuMEHjdaSdjOA7YUyvOB8AnfFdLEgkg:::sha1:64000:32:3Nm7nM7eFiCI+RBUQkFN05wUb93q0U5+:ytIE+GopICA8HU0iZfFCblN2OhwkYxcZc7yKeK17xV0=:::R76xXUDJDcKuxe4VYG9nzg==:::ck5LSXB3QTBOZ1U5UFdUR3ZVdjh4RXZ4NGhxRHZaTkNtUk4rWklZMkNRU3MvV05GUDQrMnRWQTZPSUh6UTF2Vw"
    )
    eventually {
      p.read.isEmpty shouldBe false
    }
    super.beforeAll()
  }

  private def writeKeyFile(nodeName: String, encryptedKeyFileContent: String) = {
    val memento = Memento(s"$nodeName.defaultTag")
    memento.write(encryptedKeyFileContent)
    memento
  }

  override def afterAll(): Unit = {
    try super.afterAll()
    finally deleteFolderFromConfigPath("resourcesDataDir")
  }

  private def deleteFolderFromConfigPath(configPath: String): Unit = {
    val path = Path.of(config.getString(configPath))
    require(
      path.getNameCount >= 7,
      s"Number of segments in the path of the folder which the test tries to delete is ${path.getNameCount}, but 'just in case' it should be >= 7"
    )
    FileUtils.deleteAll(path)
  }
}
