package sss.openstar.constructor

import com.typesafe.config.Config
import sss.openstar.nodebuilder.NodeLayer

class TestPartialNodeConstructor[N <: NodeLayer](nodeConstructor: (Array[String], Config) => N) {

  val password: String = "Password10!"
  private val passwordArgs = Array("-password", password)
  private val startServersArgs = Array("-startRpc", "-startHttp")

  def newWithoutServers(config: Config): N = nodeConstructor(passwordArgs, config)
  def newWithServers(config: Config): N = nodeConstructor(passwordArgs ++ startServersArgs, config)
}
