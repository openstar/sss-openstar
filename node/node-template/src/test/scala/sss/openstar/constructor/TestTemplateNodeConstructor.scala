package sss.openstar.constructor

import akka.actor.ActorSystem
import com.typesafe.config.Config
import sss.openstar.TemplateMain.TemplateNode
import sss.openstar.account.NodeIdentityManagerFixture
import sss.openstar.nodebuilder.TestActorContextBuilder

object TestTemplateNodeConstructor {
  def apply(args: Array[String], configOverride: Config): TemplateNode = {
    val node = new TemplateNode(args) with TestActorContextBuilder with NodeIdentityManagerFixture {
      override implicit lazy val config: Config           = configOverride
      override implicit lazy val actorSystem: ActorSystem = ActorSystem(config.getString("node.nodeId"))

    }
    node.migrateSqlSchema()
    node.startAppLayer()
    node
  }
}
