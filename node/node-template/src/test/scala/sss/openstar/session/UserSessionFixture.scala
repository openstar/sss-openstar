package sss.openstar.session

import org.scalacheck.Gen
import org.scalacheck.Gen.alphaLowerChar
import org.scalatest.matchers.should.Matchers.{a, convertToAnyShouldWrapper}
import sss.openstar.ui.rpc.{BridgeClient, SessionId}

trait UserSessionFixture {

  def createRandomUserAndLogin(password: String)(implicit coreBridgeClient: BridgeClient): SessionId = {
    val name = Gen.nonEmptyListOf(alphaLowerChar).map(_.mkString).sample.get
    createUserAndLogin(name, password)
  }

  def createUserAndLogin(username: String, password: String)(implicit coreBridgeClient: BridgeClient): SessionId = {
    val userSession            = SessionId(username)
    coreBridgeClient.createUser(username, username, password, password, None) shouldBe a [Right[_, _]]
    coreBridgeClient.login(userSession.value, username, password, None) shouldBe a [Right[_,_]]
    userSession
  }
}
