package sss.openstar

import akka.actor.ActorRef
import sss.openstar.StartupHooks.NextHook
import sss.openstar.account.NodeIdentity
import sss.openstar.attachments.Tester.db.syncRunContext
import sss.openstar.nodebuilder.Currencies.Ada
import sss.openstar.nodebuilder._
import sss.openstar.schemamigration.dynamic.UtxoDbStorageMigration.UtxoDbStorageId
import sss.openstar.startuphooks.ClaimLedgerOwnership
import sss.openstar.test.{MultiTx, TestCaseConfig}
import sss.openstar.wallet.UnlockedWallet

import scala.util.Try

/**
 * Created by alan on 6/10/16.
 */
object TemplateMain {

  class TemplateNode(withArgs: Array[String]) extends AllInOneLayer[Ada]
    with AdaMessageQueryHandlerActorBuilder {

    override protected val tablePartitionsCount: Int = 100
    override protected val blocksPerTable: Int = 100
    override val argsArray: Array[String] = withArgs

    override val heartbeatTimeoutWaitFactor: Int = 0
    //override def getAdaAtomicWalletCacheMap(): AtomicWalletCacheMap[Ada] = createAtomicWalletCacheMap

    //override lazy val unlockedApiHostWallet: UnlockedWallet[Ada] = buildHostNodeUnlockedWallet[Ada](ledgerId[Ada])

    //val buildAdaWallet: NodeIdentity => UnlockedWallet[Ada] = nId => buildUnlockedWallet[Ada](nId, ledgerId[Ada])

    val testCaseConfig: TestCaseConfig = TestCaseConfig(_ => (), Nil)

    lazy val claims = Seq(
      (systemLedgerId,
        chainOriginatorIdsAndKeys.nodeIdentifiers.head,
        chainOriginatorIdsAndKeys.nodeIdentifiers.tail
      ),
    ).flatMap {
      case (id, owner, others) => ClaimLedgerOwnership.makeValidClaimLedger(id, owner, nodeIdentity, others)
    }

    lazy override val nextHook: NextHook = new ClaimLedgerOwnership(
      claims,
      oracleOwnerLedgerId,
      super.nextHook
    ).claimLedgerReceive


    private lazy val messageInBoxActors: ActorRef = MessageHandlingActors.startMessageHandlingActors[Ada](
      buildUnlockedWallet[Ada](nodeIdentity, ledgerId[Ada]), nodeIdentity
    )

    override lazy val rpcImpl = createRpcImpl(buildAdaWallet, messageInBoxActors)

    override def migrateSqlSchema(): Unit =
      migrateSqlSchema(
        Set(starzBalanceLedgerId, cardanoLedgerId, testBalanceLedgerId).map(id => UtxoDbStorageId(id.id))
      )


    def startTemplateNode(): Unit = {

      migrateSqlSchema()
      startAppLayer()
      startHtml5PushActor()

      if (nodeIdentity.id == "benny")
        MultiTx(counterLedger, counterLedgerId, 1)

      initializeLedgerOwners(requiredChainOriginatorIds, identityLedgerId).map { owners =>
        log.info(s"Identity Ledger owners set to $owners")
      }

      syncWallets[Ada](users, cardanoLedger)

      startUtxoTracker[Ada](nodeIdentity.id, ledgerId[Ada])

      messageServiceActor
      messageInBoxActors

      //start the RpcServer
      startUiRpcServer()

    }
  }

  def main(withArgs: Array[String]): Unit =
    Try {
        new TemplateNode(withArgs).startTemplateNode()
    } recover {
      case e =>
        e.printStackTrace()
        System.exit(-1)
    }
}
