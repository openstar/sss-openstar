package sss.openstar

import akka.actor.{Actor, ActorContext, ActorLogging, ActorRef, Props}
import sss.openstar.RandomExit.MaybeExit
import sss.openstar.test.CoinToss

import scala.concurrent.duration.DurationInt
import scala.util.Random

object RandomExit {

  def apply(probability: Double)(implicit actorSystem: ActorContext): ActorRef = {
    actorSystem.actorOf(Props(new RandomExit(probability)))
  }

  case object MaybeExit
}

class RandomExit(probability: Double) extends Actor with ActorLogging {

  private val coinToss = CoinToss(probability)
  private val tossingInterval = 30.seconds
  import context.dispatcher

  context.system.scheduler.scheduleAtFixedRate(1.seconds, tossingInterval, self, MaybeExit)

  override def receive: Receive = {

    case MaybeExit if coinToss.flip().isHeads =>
      log.warning(s"Ooops, exiting! (p=$probability, interval=$tossingInterval)")
      System.exit(1)

    case MaybeExit =>
  }
}
