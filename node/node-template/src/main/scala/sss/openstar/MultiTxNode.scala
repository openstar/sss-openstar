package sss.openstar

import akka.actor.{ActorRef, ActorSystem}
import com.typesafe.config.Config
import kamon.Kamon
import org.eclipse.jetty.server.Server
import sss.openstar.account.NodeIdentity
import sss.openstar.nodebuilder.Currencies.TestStarz
import sss.openstar.nodebuilder._
import sss.openstar.schemamigration.dynamic.UtxoDbStorageMigration.UtxoDbStorageId
import sss.openstar.test.{MultiTxCounterTest, TestCaseConfig}
import sss.openstar.wallet.UnlockedWallet

import scala.util.Try

object MultiTxNode {

  class MultiTxNode(withArgs: Array[String], testCaseConfig: TestCaseConfig, as: ActorSystem)
    extends AllInOneLayer[TestStarz]
      with AllInOneLedgersLayer
      with StarzBalanceLedgerBuilder
      with TestBalanceLedgerBuilder
      with CounterLedgerBuilder
      with CardanoBalanceLedgerBuilder
      with TestStarzMessageQueryHandlerActorBuilder {


    override protected val tablePartitionsCount: Int = 100
    override protected val blocksPerTable: Int = 100

    override val argsArray: Array[String] = withArgs



    override implicit lazy val actorSystem: ActorSystem = as

    implicit val runContext = db.syncRunContext

    log.info("RUNNING MULTI TX TEMPLATE")

    startAppLayer()

    if (nodeIdentity.id.contains("n"))
      MultiTxCounterTest(counterLedger, counterLedgerId, 50, testCaseConfig)

    claimAllChainOriginators()

    initializeLedgerOwners(requiredChainOriginatorIds, identityLedgerId).map { owners =>
      log.info(s"Identity Ledger owners set to $owners")
    }

    syncWallets[TestStarz](users, testStarzBalanceLedger)

    lazy override val httpServer: Server = basicHttpServer()

    //Start IF flag set
    Try(startHttpServerIfFlagSet()) recover { case e => log.warn("HttpServer not started {}", e) }

    startUtxoTracker[TestStarz](nodeIdentity.id, ledgerId[TestStarz])


    private val messageInBoxActors: ActorRef = MessageHandlingActors.startMessageHandlingActors[TestStarz](
      buildUnlockedWallet[TestStarz](nodeIdentity, ledgerId[TestStarz]), nodeIdentity
    )


    override val rpcImpl = createRpcImpl(buildTestStarzWallet, messageInBoxActors)

    //start the RpcServer
    startUiRpcServer()

    RandomExit(config.getDouble("chaos.exitProbability"))

    override def migrateSqlSchema(): Unit =
      migrateSqlSchema(
        Set(starzBalanceLedgerId, cardanoLedgerId, testBalanceLedgerId).map(id => UtxoDbStorageId(id.id))
      )

    migrateSqlSchema()
  }

  def run(withArgs: Array[String], testCaseConfig: TestCaseConfig, as: ActorSystem) = {
    Try {
      new MultiTxNode(withArgs, testCaseConfig, as)
    } recover {
      case e =>
        e.printStackTrace()
        System.exit(-1)
    }
  }

  def main(withArgs: Array[String]): Unit = {
    //Kamon.init()

    val config = TestCaseConfig(_ => (), Seq("n1", "n2"),.01, Seq("q1", "q2", "q3"), None, None)
    run(withArgs, config, ActorSystem("actor-system"))
  }
}
