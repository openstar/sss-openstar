package sss.openstar

import sss.ancillary.ByteArrayEncodedStrOps._
import sss.ancillary.FutureOps.AwaitResult
import sss.openstar.account.{AccountKeysFactory, KeyPersister}
import sss.openstar.common.users.MementoKeyPersist
import sss.openstar.crypto.keypairs.{CBCKeyEncryption, Curve25519AccountKeysFactory}
import sss.openstar.identityledger.IdentityService.defaultTag
import sss.openstar.util.IOExecutionContext

import scala.concurrent.ExecutionContext.global

/**
 * This class is used to create local key files for chain originators.
 */
object KeysGenerator extends App {

  implicit val ioEc: IOExecutionContext = new IOExecutionContext(global)

  val keyPersister: KeyPersister = new KeyPersister(Seq(MementoKeyPersist),MementoKeyPersist, CBCKeyEncryption)

  val accountKeysFactory: AccountKeysFactory = Curve25519AccountKeysFactory

  case class IdentityAndPassphrase(identity: String, pass: String)

  val ids = List(
    IdentityAndPassphrase("node1", "Password10!"),
  )

  ids
    .tapEach(id => keyPersister.deleteKey(id.identity, defaultTag).await()) //await ok here, its an app
    .map(id => keyPersister.findOrCreate(id.identity, defaultTag, id.pass, () => accountKeysFactory.createKeyPair()).await())
    .foreach { keys =>
      println("Public Key Base 64  = " + keys.pub.toBase64Str)
    }
}
