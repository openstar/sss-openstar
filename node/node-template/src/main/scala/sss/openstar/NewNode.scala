package sss.openstar

import akka.actor.ActorRef
import sss.openstar.StartupHooks.NextHook
import sss.openstar.attachments.Tester.db.syncRunContext
import sss.openstar.nodebuilder.Currencies.TestStarz
import sss.openstar.nodebuilder._
import sss.openstar.schemamigration.dynamic.UtxoDbStorageMigration.UtxoDbStorageId
import sss.openstar.startuphooks.{ClaimLedgerOwnership, TestFund}
import sss.openstar.test.{MultiTx, TestCaseConfig}

import scala.util.Try

/**
 * Created by alan on 6/10/16.
 */
object NewNode {

  class NewNode(withArgs: Array[String]) extends AllInOneLayer[TestStarz]
    with TestStarzMessageQueryHandlerActorBuilder {

    override val argsArray: Array[String] = withArgs

    val testCaseConfig: TestCaseConfig = TestCaseConfig(_ => (), Nil)

    lazy val claims = Seq(
      (systemLedgerId,
        chainOriginatorIdsAndKeys.nodeIdentifiers.head,
        chainOriginatorIdsAndKeys.nodeIdentifiers.tail
      ),
    ).flatMap {
      case (id, owner, others) => ClaimLedgerOwnership.makeValidClaimLedger(id, owner, nodeIdentity, others)
    }

    lazy val testFundHook: NextHook = new TestFund(super.nextHook, nodeIdentity.id).testFundReceive

    lazy override val nextHook: NextHook = new ClaimLedgerOwnership(
      claims,
      oracleOwnerLedgerId,
      testFundHook
    ).claimLedgerReceive


    private lazy val messageInBoxActors: ActorRef = MessageHandlingActors.startMessageHandlingActors[TestStarz](
      buildUnlockedWallet[TestStarz](nodeIdentity, ledgerId[TestStarz]), nodeIdentity
    )

    override lazy val rpcImpl = createRpcImpl(buildTestStarzWallet, messageInBoxActors)

    override def migrateSqlSchema(): Unit =
      migrateSqlSchema(
        Set(starzBalanceLedgerId, cardanoLedgerId, testBalanceLedgerId).map(id => UtxoDbStorageId(id.id))
      )


    def start(): Unit = {

      startAppLayer()
      startHtml5PushActor()

      syncWallets[TestStarz](users, testStarzBalanceLedger)

      startUtxoTracker[TestStarz](nodeIdentity.id, ledgerId[TestStarz])

      messageServiceActor
      messageInBoxActors

      //start the RpcServer
      startUiRpcServer()

    }
  }

  def main(withArgs: Array[String]): Unit =
    Try {
        new NewNode(withArgs).start()
    } recover {
      case e =>
        e.printStackTrace()
        System.exit(-1)
    }
}
