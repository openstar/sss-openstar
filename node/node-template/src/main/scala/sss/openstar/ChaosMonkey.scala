package sss.openstar

import akka.actor.ActorRef
import sss.openstar.account.NodeIdentity
import sss.openstar.nodebuilder.Currencies.TestStarz
import sss.openstar.nodebuilder._
import sss.openstar.schemamigration.dynamic.UtxoDbStorageMigration.UtxoDbStorageId
import sss.openstar.test.MultiIdentityTestTransactionSenderInternalClaim
import sss.openstar.wallet.UnlockedWallet

import scala.util.Try

/**
 * Created by alan on 6/10/16.
 */
object ChaosMonkey {

  class ChaosMonkeyNode(withArgs: Array[String])
    extends AllInOneLayer[TestStarz]
    with TestStarzMessageQueryHandlerActorBuilder
      with AllInOneLedgersLayer
      with CounterLedgerBuilder
      with StarzBalanceLedgerBuilder
      with CardanoBalanceLedgerBuilder
      with TestBalanceLedgerBuilder {

    override protected val tablePartitionsCount: Int = 100
    override protected val blocksPerTable: Int = 100

    override val argsArray: Array[String] = withArgs



    MultiIdentityTestTransactionSenderInternalClaim[TestStarz](
      nodeIdentityManager,
      findPublicKeyOpt,
      users,
      50,
      100000,
      testBalanceLedgerId,
      nId => buildUnlockedWallet[TestStarz](nId, testBalanceLedgerId),
      50,
      5000,
      toIdentityLedgerOwner,
      nodeIdentity
    )

    claimAllChainOriginators()

    initializeLedgerOwners(requiredChainOriginatorIds, identityLedgerId).map { owners =>
      log.info(s"Identity Ledger owners set to $owners")
    }

    syncWallets[TestStarz](users, testStarzBalanceLedger)

    lazy override val httpServer = basicHttpServer()

    //Start IF flag set
    Try(startHttpServerIfFlagSet()) recover { case e => log.warn("HttpServer not started {}", e) }


    //startUtxoTracker[TestStarz](nodeIdentity.id, ledgerId[TestStarz])

    private val messageInBoxActors: ActorRef = MessageHandlingActors.startMessageHandlingActors[TestStarz](
      buildUnlockedWallet[TestStarz](nodeIdentity, ledgerId[TestStarz]), nodeIdentity
    )

    startAppLayer()

    override val rpcImpl = createRpcImpl(buildTestStarzWallet, messageInBoxActors)

    //start the RpcServer
    startUiRpcServer()

    RandomExit(config.getDouble("chaos.exitProbability"))

    override def migrateSqlSchema(): Unit =
      migrateSqlSchema(
        Set(starzBalanceLedgerId, cardanoLedgerId, testBalanceLedgerId).map(id => UtxoDbStorageId(id.id))
      )
  }

  def main(withArgs: Array[String]): Unit = {

    Try {

      new ChaosMonkeyNode(withArgs)

    } recover {
      case e =>
        e.printStackTrace()
        System.exit(-1)
    }
  }
}
