package sss.openstar

import com.typesafe.config.ConfigFactory

import scala.util.Try

object Runner {
  def main(args: Array[UniqueNodeIdentifier]): Unit = {
    val config = ConfigFactory.load()
    val nodeConfig = config.getConfig("node")

    Try(nodeConfig.getString("nodeToRun")).toOption match {
      case Some("template") => TemplateMain.main(args)
      case Some("newNode") => NewNode.main(args)
      case Some("multi-tx") => MultiTxNode.main(args)
      //fall back to default template
      case _ => TemplateMain.main(args)
    }
  }
}