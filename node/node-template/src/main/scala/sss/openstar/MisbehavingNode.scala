package sss.openstar

import akka.actor.ActorRef
import sss.openstar.account.NodeIdentity
import sss.openstar.nodebuilder.Currencies.Ada
import sss.openstar.nodebuilder._
import sss.openstar.schemamigration.dynamic.UtxoDbStorageMigration.UtxoDbStorageId
import sss.openstar.wallet.UnlockedWallet

import scala.util.Try

object MisbehavingNode {

  class TemplateNode(withArgs: Array[String]) extends AllInOneLayer[Ada]
    with AdaMessageQueryHandlerActorBuilder
    with AllInOneLedgersLayer
    with TestBalanceLedgerBuilder
    with StarzBalanceLedgerBuilder
    with CardanoBalanceLedgerBuilder
    with CounterLedgerBuilder {

    override protected val tablePartitionsCount: Int = 100
    override protected val blocksPerTable: Int = 100

    override val argsArray: Array[String] = withArgs

    override def getAdaAtomicWalletCacheMap(): AtomicWalletCacheMap[Ada] = createAtomicWalletCacheMap

    override lazy val unlockedApiHostWallet: UnlockedWallet[Ada] = buildHostNodeUnlockedWallet[Ada](ledgerId[Ada])


    lazy override val httpServer = basicHttpServer()

    private val messageInBoxActors: ActorRef = MessageHandlingActors.startMessageHandlingActors[Ada](
      buildUnlockedWallet[Ada](nodeIdentity, ledgerId[Ada]), nodeIdentity
    )

    override val rpcImpl = createRpcImpl(buildAdaWallet, messageInBoxActors)

    //start the RpcServer

    override def migrateSqlSchema(): Unit =
      migrateSqlSchema(
        Set(starzBalanceLedgerId, cardanoLedgerId, testBalanceLedgerId).map(id => UtxoDbStorageId(id.id))
      )


    def startMisbehavingNode(): Unit = {
      startAppLayer()
      startUtxoTracker[Ada](nodeIdentity.id, ledgerId[Ada])
      syncWallets[Ada](users, cardanoLedger)
      startHtml5PushActor()
      startUiRpcServer()
    }

  }
  def main(withArgs: Array[String]): Unit = {

    Try {
      new TemplateNode(withArgs).startMisbehavingNode()
    } recover {
      case e =>
        e.printStackTrace()
        System.exit(-1)
    }
  }
}
