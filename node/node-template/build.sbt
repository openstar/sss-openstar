import com.typesafe.sbt.packager.docker.{DockerChmodType, DockerPermissionStrategy}
import com.typesafe.sbt.packager.docker.{Cmd, ExecCmd}

name := "openstar-node-template"

libraryDependencies ++= Seq(
  "io.kamon"      %% "kamon-bundle"       % Vers.kamon,
  "io.kamon"      %% "kamon-apm-reporter" % Vers.kamon
)

Linux / packageSummary := "openstar node template"

enablePlugins(JavaAppPackaging, JavaAgent, DockerPlugin, ClasspathJarPlugin)

javaAgents += "io.kamon" % "kanela-agent" % "1.0.16"

Compile / mainClass := Some("sss.openstar.Runner")

Universal / mappings += {
  val conf = sourceDirectory.value / "main" / "resources" / "application.conf"
  conf -> "conf/application.conf"
}

dockerRepository := Some("registry.gitlab.com")

Docker / packageName := "openstar/sss-openstar/node-template"

dockerBaseImage := "eclipse-temurin:11-jre-alpine"

dockerPermissionStrategy := DockerPermissionStrategy.Run

dockerChmodType := DockerChmodType.Custom("uga+rw")

dockerCommands ++= Seq(
  Cmd("USER", "root"),
  ExecCmd("RUN","apk", "update"),
  ExecCmd("RUN","apk","upgrade"),
  ExecCmd("RUN", "apk", "add", "--no-cache", "bash"),
  Cmd("USER", "demiourgos728"),
  )

Test / publishArtifact := true

Test / compile / coverageEnabled := true
Compile / compile / coverageEnabled := false