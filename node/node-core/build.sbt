name := "openstar-node-core"

Test / parallelExecution := false

Test / publishArtifact := true

libraryDependencies += "nl.martijndwars" % "web-push" % Vers.martinWebPush

libraryDependencies += "org.bouncycastle" % "bcpkix-jdk15on" % Vers.bouncyBcpkix

libraryDependencies += "org.bouncycastle" % "bcprov-jdk15on" % Vers.bouncyBcpkix

libraryDependencies += "com.typesafe.akka" %% "akka-discovery" % Vers.akkaVer

libraryDependencies += "com.typesafe.akka" %% "akka-testkit" % Vers.akkaVer % Test

libraryDependencies += "com.typesafe.akka" %% "akka-actor" % Vers.akkaVer

// https://mvnrepository.com/artifact/com.typesafe.akka/akka-slf4j_2.11
libraryDependencies += "com.typesafe.akka" %% "akka-slf4j" % Vers.akkaVer

libraryDependencies += "com.mcsherrylabs" %% "sss-db" % Vers.sssDbVer

libraryDependencies += "us.monoid.web" % "resty" % Vers.restyVer % Test

// https://mvnrepository.com/artifact/org.hsqldb/hsqldb
libraryDependencies += "org.hsqldb" % "hsqldb" % Vers.hsqldbVer

libraryDependencies += "com.typesafe.akka" %% "akka-http" % Vers.akkaHttp

libraryDependencies += "com.typesafe.akka" %% "akka-http2-support" % Vers.akkaHttp

libraryDependencies += "com.typesafe.akka" %% "akka-stream" % Vers.akkaStream

libraryDependencies += "com.typesafe.akka" %% "akka-http-testkit" % Vers.akkaHttp % Test

libraryDependencies += "org.flywaydb" % "flyway-core" % Vers.flywayVersion

libraryDependencies += "net.lingala.zip4j" % "zip4j" % Vers.zip4j