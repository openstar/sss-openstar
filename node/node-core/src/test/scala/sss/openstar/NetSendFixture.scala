package sss.openstar

import sss.ancillary.Logging
import sss.openstar.network.{NetSend, SerializedMessage}
import sss.openstar.nodebuilder.{RequireDecoder, RequireMessageEventBus}

import java.util.concurrent.atomic.AtomicReference

case class TestIncoming(msg: Any) extends BusEvent

trait NetSendFixture {

  self: RequireDecoder with RequireMessageEventBus with Logging =>

  val receivedNetworkMessages = new AtomicReference[Seq[Any]](Seq.empty)

  val netSend: NetSend = (serMsg: SerializedMessage, targets: Set[UniqueNodeIdentifier]) => {
    decoder(serMsg.msgCode) match {
      case Some(info) =>
        val msg = info.fromBytes(serMsg.data)

        receivedNetworkMessages.getAndUpdate({ msgs: Seq[_] =>
          msgs :+ msg
        })

        messageEventBus publish TestIncoming(msg)
      case None => log.warn(s"No decoding info found for ${serMsg.msgCode}")
    }
    ()
  }

}
