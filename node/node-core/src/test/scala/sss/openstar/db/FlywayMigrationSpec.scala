package sss.openstar.db

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.db.ops.DbOps.DbRunOps
import sss.db.where
import sss.openstar.schemamigration.dynamic.BlockMigration.{blockSigsNonQuorumTablePrefix, blockSigsQuorumTablePrefix, blockTablePrefix}
import sss.openstar.chains.Chains.GlobalChainIdBuilder
import sss.openstar.schemamigration.dynamic.MerkleMigration.merkleTablePrefix
import sss.openstar.nodebuilder.BlockFactoriesAndMerklePersisterBuilder

class FlywayMigrationSpec extends AnyFlatSpec with Matchers with DbWithMigrationFixture with BlockFactoriesAndMerklePersisterBuilder with GlobalChainIdBuilder {

  "FlywayMigration" should "be able to create the required number of tables" in {

    val expectedCount = tablePartitionsCount
    getTableCount(merkleTablePrefix) should equal(expectedCount)
    getTableCount(blockTablePrefix) should equal(expectedCount)
    getTableCount(blockSigsQuorumTablePrefix) should equal(expectedCount)
    getTableCount(blockSigsNonQuorumTablePrefix) should equal(expectedCount)
  }

  private def getTableCount(tableNamePrefix: String): Long = {
    db.select(s"SELECT COUNT(TABLE_NAME) TABLE_COUNT FROM INFORMATION_SCHEMA.SYSTEM_TABLES ")
      .getRow(where(s"REGEXP_MATCHES(TABLE_NAME,'^${tableNamePrefix.toUpperCase}[0-9].*')"))
      .map(_.flatMap(_.longOpt("TABLE_COUNT")))
      .map(_.getOrElse(0L))
      .dbRunSyncGet
  }



}
