package sss.openstar.db

import sss.openstar.nodebuilder.GlobalTableNameTagBuilder
import sss.openstar.schemamigration.dynamic.UtxoDbStorageMigration.UtxoDbStorageId
import sss.openstar.schemamigration.{PerformSchemaMigration, RequireDatasource, SchemaMigrationBuilder}

trait MigrationFixture extends SchemaMigrationBuilder with PerformSchemaMigration {
  self: RequireDatasource with GlobalTableNameTagBuilder =>

  import sss.openstar.MessageKeys._

  override protected lazy val tablePartitionsCount: Int = 100

  def migrateSqlSchema(): Unit =
    migrateSqlSchema(
      Set(StarzBalanceLedger, CardanoLedger, TestBalanceLedger).map(UtxoDbStorageId)
    )

}
