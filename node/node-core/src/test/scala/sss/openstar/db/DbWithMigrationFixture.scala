package sss.openstar.db

import org.scalatest.Suite
import sss.openstar.chains.Chains.GlobalChainIdBuilder
import sss.openstar.common.builders.ConfigBuilder
import sss.openstar.db.Builder.RequireDb
import sss.openstar.nodebuilder.{BlockHeightToTableBuilder, GlobalTableNameTagBuilder}

trait DbWithMigrationFixture
    extends DbFixture
    with RequireDb
    with ConfigBuilder
    with GlobalChainIdBuilder
    with GlobalTableNameTagBuilder
    with BlockHeightToTableBuilder
    with MigrationFixture {
  self: Suite =>

  override val blocksPerTable: Int = 100

}
