package sss.openstar

import sss.openstar.eventbus.MessageInfo
import sss.openstar.nodebuilder.RequireDecoder

trait DecoderFixture extends RequireDecoder {
  val decoder: Byte => Option[MessageInfo] = MessageKeys.messages.find
}
