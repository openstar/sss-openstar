package sss.openstar.peers

import akka.testkit.{ImplicitSender, TestKit}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import sss.ancillary.Logging
import sss.ancillary.Restarter.AttemptRestart
import sss.openstar._
import sss.openstar.common.builders.{ActorSystemBuilder, NodeIdTagFixture}
import sss.openstar.db.DbWithMigrationFixture
import sss.openstar.hash.FastCryptographicHash
import sss.openstar.network._
import sss.openstar.nodebuilder.{MessageEventBusBuilder, NetSendBuilder, RootActorContextBuilder}
import sss.openstar.peers.Discovery.DiscoveredNode
import sss.openstar.peers.PeerManager.ChainQuery

import java.net.{InetAddress, InetSocketAddress}
import scala.concurrent.duration._
import scala.language.postfixOps

object ActorSystemObj extends ActorSystemBuilder
class PeerManagerSpec extends TestKit(ActorSystemObj.actorSystem)
  with AnyWordSpecLike
  with Matchers
  with ImplicitSender
  with ActorSystemBuilder
  with RootActorContextBuilder
  with MessageEventBusBuilder
  with DecoderFixture
  with NetSendFixture
  with Logging
  with NodeIdTagFixture
  with DbWithMigrationFixture {

  lazy override implicit val actorSystem = ActorSystemObj.actorSystem

  private val chainId = 1.toByte

  private case class AttemptedConnection(toNode: UniqueNodeIdentifier) extends BusEvent


  var stopIt = false
  messageEventBus.subscribe(classOf[AttemptedConnection])

  val connect: NetConnect = new NetConnect {
    override def connect(nId: NodeId, reconnectStrategy: ReconnectionStrategy): Unit = {
      if(!stopIt) messageEventBus publish AttemptedConnection(nId.id)

    }

    override def disconnect(nodeId: UniqueNodeIdentifier): Unit = ()
  }

  val bootstrapNodes: Set[DiscoveredNode] = ((0 until 2) map { i => DiscoveredNode(NodeId(s"node$i",
    new InetSocketAddress(
      InetAddress.getByName(s"127.0.0.$i"),
      80 + i)),
    i.toByte)
  }).toSet

  val ourCapabilities: Capabilities = Capabilities(chainId)
  val discoveryInterval: FiniteDuration = 15.seconds
  val discovery: Discovery = new Discovery(FastCryptographicHash.hash)
  implicit val attemptRestart: AttemptRestart = () => ()

  val ourNetAddress: InetSocketAddress = new InetSocketAddress(InetAddress.getLocalHost, 8090)

  lazy val thisNode = DiscoveredNode(NodeId(nodeIdTag.nodeId, ourNetAddress),chainId)

  val sut = new PeerManager(connect,
    netSend,
    bootstrapNodes,
    discoveryInterval,
    discovery,
    thisNode
  )

  "PeerManager" should {

    "attempt to connect to bootstrap when queried" in {
      sut.addQuery(ChainQuery(chainId, 2))
      val t = expectMsgType[AttemptedConnection]
      assert(bootstrapNodes.map(_.nodeId.id)contains(t.toNode))
      messageEventBus.unsubscribe(classOf[AttemptedConnection])
      stopIt = true
    }

  }

}
