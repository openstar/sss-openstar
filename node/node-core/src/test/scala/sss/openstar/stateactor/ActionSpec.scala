package sss.openstar.stateactor

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.openstar.BusEvent

class ActionSpec extends AnyFlatSpec with Matchers {

  object DummyPublish extends BusEvent

  "Actions" should "compose" in {
    val s = (ToPublish(DummyPublish) + ToPublish(DummyPublish)).asSeq
    assert(s === Seq(ToPublish(DummyPublish), ToPublish(DummyPublish)))
  }

  it should "allow NoAction in all positions" in {
    val s = (NoAction + ToPublish(DummyPublish) + NoAction + NoAction + ToPublish(DummyPublish) + NoAction).asSeq
    assert(s === Seq(ToPublish(DummyPublish), ToPublish(DummyPublish)))
  }

  it should "allow NoAction only" in {
    val s = NoAction.asSeq
    assert(s === Seq.empty)
  }

  it should "allow NoAction combined" in {
    val s = (NoAction + NoAction).asSeq
    assert(s === Seq.empty)
  }
}
