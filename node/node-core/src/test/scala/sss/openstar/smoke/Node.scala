package sss.openstar.smoke

import akka.actor.{Actor, Props}
import sss.openstar.account.NodeIdTag
import sss.openstar.chains.QuorumFollowersSyncedMonitor.BlockChainReady
import sss.openstar.eventbus.MessageInfo
import sss.openstar.network.MessageEventBus.EventSubscriptionsClassOps
import sss.openstar.network.NetworkInterface.BindControllerSettings
import sss.openstar.nodebuilder.NodeLayer
import sss.openstar.{BusEvent, MessageKeys}

import scala.concurrent.duration.FiniteDuration
import scala.concurrent.{Future, Promise}

class Node(
            aNodeId: String,
            aPort: Int,
            subscribes: Seq[Class[_ <: BusEvent]],
            maxWaitTime: FiniteDuration) extends NodeLayer {
  override def decoder: Byte => Option[MessageInfo] = MessageKeys.messages.find

  lazy override val nodeIdTag: NodeIdTag = NodeIdTag(nodeId = aNodeId)
  lazy override val bindSettings = BindControllerSettings(prefixedConf)
    .copy(port = aPort.toString, bindAddress = "127.0.0.1")

  override protected val tablePartitionsCount: Int = 100
  override val phrase: Option[String] = Some("Password10!")
  override protected val blocksPerTable: Int = 100

  override val heartbeatTimeoutWaitFactor: Int = 0

  override def migrateSqlSchema(): Unit = {
    migrateSqlSchema(globalTableNameTag)
  }

  private val exitPromise: Promise[Seq[BusEvent]] = Promise()

  def future: Future[Seq[BusEvent]] = exitPromise.future

  object CatchPeers extends Actor {

    case object OutOfTime

    override def preStart(): Unit = {
      super.preStart()
      (subscribes :+ classOf[BlockChainReady]).subscribe

      context.system.scheduler.scheduleOnce(maxWaitTime, self, OutOfTime)
    }

    var events: Seq[BusEvent] = Seq.empty

    override val receive: Receive = {

      case OutOfTime =>
        exitPromise.failure(new RuntimeException("Out of time"))

      case exit: BlockChainReady =>
        events = events :+ exit
        exitPromise.success(events)

      case e: BusEvent =>
        events = events :+ e
    }
  }

  actorContext.actorOf(Props(CatchPeers))
}
