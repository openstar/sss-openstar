package sss.openstar.smoke

import org.scalatest.concurrent.PatienceConfiguration.Timeout
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.time.{Millis, Minutes, Span}
import sss.openstar.chains.QuorumFollowersSyncedMonitor.{BlockChainReady, QuorumSync, SyncedQuorum}
import sss.openstar.peers.PeerManager.PeerConnection

import scala.concurrent.Promise
import scala.concurrent.duration.{DurationInt, FiniteDuration}

/**
 * TODO Move to integration test area (none exists)
 */
class NodeLayerSmokeSpec extends AnyFlatSpec with ScalaFutures {

  implicit override val patienceConfig: PatienceConfig =
    PatienceConfig(timeout = scaled(Span(1, Minutes)), interval = scaled(Span(20, Millis)))

  "Alice and Bob" should "find each other and start the chain" in {

    val maxWaitTime: FiniteDuration = 30.seconds

    val subs = Seq(classOf[PeerConnection], classOf[SyncedQuorum], classOf[BlockChainReady])
    val bob = new Node("bob_test", 7072,  subs, maxWaitTime)
    val alice = new Node("alice_test", 7073, subs, maxWaitTime)

    bob.migrateSqlSchema()
    alice.migrateSqlSchema()

    bob.startNodeLayer()
    alice.startNodeLayer()

    val seqAlice = alice.future.futureValue
    val seqBob = bob.future.futureValue
    assert(seqBob.exists(_.isInstanceOf[BlockChainReady]))
    assert(seqAlice.exists(_.isInstanceOf[BlockChainReady]))

    bob.nodeLayerShutdown()
    alice.nodeLayerShutdown()

  }

}
