package sss.openstar.chains

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.openstar.MessageKeys
import sss.openstar.block.{Synchronized, VoteLeader}
import sss.openstar.chains.LeaderElectionActor.LocalLeader
import sss.openstar.chains.QuorumFollowersSyncedMonitor.{QuorumSync, SyncedQuorum}
import sss.openstar.network.MessageEventBus.IncomingMessage
import sss.openstar.stateactor.{ToDisconnect, ToPublish}

import scala.language.postfixOps

class QuorumFollowersSyncedStateSpec extends AnyFlatSpec with Matchers {

  private val chainId = 1.toByte

  private val otherNodeId = "test"

  private val myNodeId = "myNodeId"

  val sut = WaitForLeader(Map.empty, None)(chainId, myNodeId)

  "QuorumFollowersMonitor" should "not disconnect if badly synced" in {

    val followers = Seq(VoteLeader(otherNodeId))

    val (s, a) = sut(LocalLeader(chainId, myNodeId, 8, 3, followers, 1)
    )(IncomingMessage(chainId,
        MessageKeys.Synchronized,
        otherNodeId,
        Synchronized(chainId, 8, 2, otherNodeId))
    )

    assert(a.asSeq === Seq(ToDisconnect(otherNodeId)))

  }

  it should "emit quorumSynced if correctly synced" in {

    val followers = Seq(VoteLeader(otherNodeId))

    val (s, a) = sut(LocalLeader(chainId, myNodeId, 8, 3, followers, 1)
    )(IncomingMessage(chainId,
      MessageKeys.Synchronized,
      otherNodeId,
      Synchronized(chainId, 8, 3, otherNodeId))
    )

    assert(a.asSeq === Seq(ToPublish(SyncedQuorum(chainId,List(QuorumSync(otherNodeId,8,3)),1))))

  }
}
