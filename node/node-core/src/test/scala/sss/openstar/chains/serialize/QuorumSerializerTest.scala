package sss.openstar.chains.serialize


import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.openstar.common.chains.Quorum
import sss.openstar.common.chains.serialize.QuorumSerializer

/**
  * Created by alan on 2/15/16.
  */
class QuorumSerializerTest extends AnyFlatSpec with Matchers {

  val cId = 3.toByte
  val quorumMembers = (0 to 9).map (index => s"asdasdasd$index").toSet
  val numConf = 900

  "A Quorum" should "be correctly serialised and deserialized" in {
    val q = Quorum(cId, quorumMembers, numConf)
    val asBytes = QuorumSerializer.toBytes(q)
    val reanimated = QuorumSerializer.fromBytes(asBytes)
    assert(q == reanimated, "Reanimated quorum should be the same as original")
  }

}
