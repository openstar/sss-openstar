package sss.openstar.chains

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.openstar.block.{FindLeader, FindLeaderOrdering, GetLatestCommittedBlockId, Leader, VoteLeader}
import sss.openstar.chains.LeaderElectionActor.{LeaderLost, LocalLeader, MakeFindLeader}
import sss.openstar.chains.QuorumMonitor.QuorumLost
import sss.openstar.common.block.BlockId
import sss.openstar.common.chains.Quorum
import sss.openstar.network.MessageEventBus.IncomingMessage
import sss.openstar.stateactor.{Schedule, ToPublish, ToSend}
import sss.openstar.{MessageKeys, UniqueNodeIdentifier}

import scala.collection.SortedSet
import scala.concurrent.duration._


class LeaderElectionStateSpec extends AnyFlatSpec with Matchers {

  implicit private val chainId = 1.toByte

  private val myNodeId = "myNodeId"
  private val betterNode = "betterNode"

  def latestBlockId: GetLatestCommittedBlockId = () => BlockId(9, 5)

  def createFind(nId: UniqueNodeIdentifier, sigIndx: Int): MakeFindLeader = () => {
    FindLeader(9, 5, 4, sigIndx, nId)
  }

  private val otherNodeId = "test"
  val sut = WaitForQuorum()(chainId, createFind(myNodeId, 5), latestBlockId, myNodeId)

  "LeaderActor" should "produce leader if no quorum candidates exist " in {
    val (s, a) = sut(Quorum(chainId, Set.empty, 0))
    assert(a.asSeq.last == ToPublish(LocalLeader(chainId, myNodeId, 9, 5, Seq.empty, 0)))
  }

  it should "produce a leader (us) if another candidate with less credentials is connected" in {

    val (s, a) = sut(Quorum(chainId, Set(betterNode), 1))(
      IncomingMessage[VoteLeader](chainId, MessageKeys.VoteLeader, betterNode, VoteLeader(betterNode))
    )

    assert(a.asSeq === Seq(
      ToSend(MessageKeys.FindLeader,FindLeader(9,5,4,5,myNodeId),Set(betterNode)),
      ToPublish(LocalLeader(chainId,myNodeId,9,5,List(VoteLeader(betterNode)),1)),
      ToSend(MessageKeys.Leader,Leader(myNodeId),Set(betterNode)))
    )

  }

  it should " produce a leader (them) if another candidate with more credentials is connected" in {

    val (s, a) = sut(Quorum(chainId, Set(betterNode), 1))(
      IncomingMessage[VoteLeader](chainId, MessageKeys.VoteLeader, betterNode, VoteLeader(betterNode))
    )

    assert(a.asSeq === Seq(

      ToSend(MessageKeys.FindLeader,FindLeader(9,5,4,5,myNodeId),Set(betterNode)),

      ToPublish(LocalLeader(chainId,myNodeId,9,5,List(VoteLeader(betterNode)),1)),
      ToSend(MessageKeys.Leader,Leader(myNodeId),Set(betterNode)))
    )

  }


  it should "produce Leader Lost when Quorum is lost" in {
    val (s, a) = sut(Quorum(chainId, Set(betterNode), 1))(
      IncomingMessage[VoteLeader](chainId, MessageKeys.VoteLeader, betterNode, VoteLeader(betterNode))
    )(QuorumLost(chainId))

    assert(a.asSeq.last ===
      ToPublish(LeaderLost(chainId,myNodeId)
    ))
  }

  it should "produce FindLeader if Quorum is refound" in {
    val (s, _) = sut(Quorum(chainId, Set(betterNode), 1))(
      IncomingMessage[VoteLeader](chainId, MessageKeys.VoteLeader, betterNode, VoteLeader(betterNode))
    )(QuorumLost(chainId)
    )

    val (_, a) = s(Quorum(chainId, Set(betterNode), 0))

    assert(a.asSeq.contains(
      ToSend(MessageKeys.FindLeader,FindLeader(9,5,4,5,myNodeId),Set(betterNode))
    ))
  }

  it should "handle FindLeader from a 'worse' node" in {
    val (s, a) = sut(Quorum(chainId, Set(betterNode), 1))(
      IncomingMessage[FindLeader](chainId, MessageKeys.FindLeader, betterNode, FindLeader(9, 4, 4, 4, betterNode))
    )

    assert(a.asSeq.contains(
      ToSend(MessageKeys.FindLeader,FindLeader(9,5,4,5,myNodeId),Set(betterNode))
    ))
  }

  it should "fire VoteLeader for remote from a slightly 'better' node" in {
    val (s, a) = sut(Quorum(chainId, Set(betterNode), 1))(
      // sig index is lower
      IncomingMessage[FindLeader](chainId, MessageKeys.FindLeader, betterNode, FindLeader(9, 5, 4, 3, betterNode))
    )

    assert(a.asSeq === Seq(
      ToSend(MessageKeys.FindLeader,FindLeader(9,5,4,5,myNodeId),Set(betterNode)),

      ToSend(MessageKeys.VoteLeader,VoteLeader(myNodeId),Set(betterNode))
    ))
  }

  it should "correctly order FindLeaders" in {
    var sortedSet = SortedSet[FindLeader](FindLeader(1,1,1,1,myNodeId))

    sortedSet = sortedSet.union(Set(FindLeader(2,1,1,1,myNodeId)))
    sortedSet = sortedSet.union(Set(FindLeader(1,1,2,1,myNodeId)))
    sortedSet = sortedSet.union(Set(FindLeader(2,2,1,1,myNodeId)))
    sortedSet = sortedSet.union(Set(FindLeader(1,2,1,1,myNodeId)))
    sortedSet = sortedSet.union(Set(FindLeader(1,2,1,1,myNodeId)))
    sortedSet = sortedSet.union(Set(FindLeader(1,1,1,2,myNodeId)))


    assert(sortedSet.size == 6, "It should remove dups")
    assert(sortedSet.last == FindLeader(2,2,1,1,myNodeId))
  }

  "A 6 member quorum" should "find a leader after 3 external votes" in {
    val quorumMembers = (0 until 6).map(i => s"member$i")

    val minConfirms = QuorumMonitor.calcMinConfirms(quorumMembers.size)

    val (_: FindingLeader, actions) = sut(Quorum(chainId, quorumMembers.toSet, minConfirms))(
      IncomingMessage(`chainId`, MessageKeys.FindLeader, quorumMembers(0), FindLeader(9,5,5,1,quorumMembers(0)))
    )(
      IncomingMessage(`chainId`, MessageKeys.FindLeader, quorumMembers(0), FindLeader(9,5,4,1,quorumMembers(1)))
    )(
      IncomingMessage(`chainId`, MessageKeys.FindLeader, quorumMembers(0), FindLeader(9,5,4,1,quorumMembers(2)))
    )

    assert(actions.asSeq.contains(ToSend(MessageKeys.VoteLeader, VoteLeader(myNodeId), Set(quorumMembers(0)))))

  }

  it should "not find a leader if quorum lost" in {
    val quorumMembers = (0 until 6).map(i => s"member$i")

    val minConfirms = QuorumMonitor.calcMinConfirms(quorumMembers.size)

    val (_: WaitForQuorum, actions) = sut(Quorum(chainId, quorumMembers.toSet, minConfirms))(
      IncomingMessage(`chainId`, MessageKeys.FindLeader, quorumMembers(0), FindLeader(9,5,5,1,quorumMembers(0)))
    )(
      IncomingMessage(`chainId`, MessageKeys.FindLeader, quorumMembers(0), FindLeader(9,5,4,1,quorumMembers(1)))
    )(
      QuorumLost(chainId)
    )(
      IncomingMessage(`chainId`, MessageKeys.FindLeader, quorumMembers(0), FindLeader(9,5,4,1,quorumMembers(2)))
    )
    actions.asSeq.contains(ToSend(MessageKeys.FindLeader,FindLeader(9,5,4,5,myNodeId),quorumMembers.toSet))
  }

  it should "recover to find a leader if bigger quorum found again" in {
    val quorumMembers = (0 until 6).map(i => s"member$i")
    val biggerQuorum = (0 until 8).map(i => s"member$i")

    val minConfirms = QuorumMonitor.calcMinConfirms(quorumMembers.size)

    val (stillFindingLeader: FindingLeader, actions) = sut(Quorum(chainId, quorumMembers.toSet, minConfirms))(
      IncomingMessage(`chainId`, MessageKeys.FindLeader, quorumMembers(0), FindLeader(9,5,5,1,quorumMembers(0)))
    )(
      QuorumLost(chainId)
    )(
      IncomingMessage(`chainId`, MessageKeys.FindLeader, quorumMembers(0), FindLeader(9,5,4,1,quorumMembers(1)))
    )(
      Quorum(chainId, biggerQuorum.toSet ,QuorumMonitor.calcMinConfirms(biggerQuorum.size))
    )(
      IncomingMessage(`chainId`, MessageKeys.FindLeader, quorumMembers(0), FindLeader(9,5,4,1,quorumMembers(2)))
    )

    assert(!actions.asSeq.contains(ToSend(MessageKeys.VoteLeader, VoteLeader(myNodeId), Set(quorumMembers(0)))))

    val (shouldBeStillFindLeader: FindingLeader, actions2) = stillFindingLeader(
      IncomingMessage(`chainId`, MessageKeys.FindLeader, quorumMembers(0), FindLeader(9,5,4,1,quorumMembers(0)))
    )(
      IncomingMessage(`chainId`, MessageKeys.FindLeader, quorumMembers(0), FindLeader(9,5,4,1,quorumMembers(1)))
    )

    assert(actions2.asSeq.isEmpty)

    val (_, actionsLeaderFound) = shouldBeStillFindLeader(
      IncomingMessage(`chainId`, MessageKeys.FindLeader, quorumMembers(0), FindLeader(9,5,5,1,quorumMembers(3)))
    )

    assert(actionsLeaderFound.asSeq.contains(
      ToSend(MessageKeys.VoteLeader, VoteLeader(myNodeId), Set(quorumMembers(3))))
    )

  }

  it should "become leader if it gets the votes" in {
    val quorumMembers = (0 until 6).map(i => s"member$i")

    val ( _: LeaderKnown, actions) = sut(Quorum(chainId, quorumMembers.toSet ,3))(
      IncomingMessage(`chainId`, MessageKeys.VoteLeader, quorumMembers(0), VoteLeader(quorumMembers(0)))
    )(
      IncomingMessage(`chainId`, MessageKeys.VoteLeader, quorumMembers(1), VoteLeader(quorumMembers(1)))
    )(
      IncomingMessage(`chainId`, MessageKeys.VoteLeader, quorumMembers(2), VoteLeader(quorumMembers(2)))
    )(
      IncomingMessage(`chainId`, MessageKeys.VoteLeader, quorumMembers(3), VoteLeader(quorumMembers(3)))
    )(
      IncomingMessage(`chainId`, MessageKeys.VoteLeader, quorumMembers(4), VoteLeader(quorumMembers(4)))
    )(
      IncomingMessage(`chainId`, MessageKeys.VoteLeader, quorumMembers(5), VoteLeader(quorumMembers(5)))
    )
  }
}
