package sss.openstar.chains

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.openstar.DummySeedBytes
import sss.openstar.chains.QuorumMonitor._
import sss.openstar.common.chains.Quorum
import sss.openstar.ledger.LedgerId
import sss.openstar.network.ConnectionLost
import sss.openstar.peers.Capabilities
import sss.openstar.peers.PeerManager.{AddQuery, IdQuery, PeerConnection}
import sss.openstar.quorumledger.QuorumLedger.NewQuorumCandidates
import sss.openstar.stateactor.{Action, ToPublish}

import scala.language.postfixOps

class QuorumStateSpec extends AnyFlatSpec with Matchers {

  private val chainId = 1.toByte

  private val otherNodeId = "test"

  private val myNodeId = "myNodeId"


  "QuorumMonitor" should "not quorum if no one in membership" in {

    val sut = QuorumState(chainId, myNodeId, Set.empty)
    val (newState: QuorumState, events) = sut(CheckStatus)
    assert(!newState.isQuorum, "No candidates, should not have quorum")
    assert(events.isEmpty)
  }

  it should "have a quorum if only this node is a member" in {
    val sut = QuorumState(chainId, myNodeId, Set(myNodeId))
    val (newState: QuorumState, events) = sut(CheckStatus)
    assert(newState.isQuorum, "Only us as candidate, should have quorum")
    assert(events === ToPublish(Quorum(chainId, Set.empty, 0)))
  }

  it should "not have a quorum if only one other node is a member and not connected " in {
    val sut = QuorumState(chainId, myNodeId, Set(myNodeId))
    val fakeTxId = DummySeedBytes(16)
    implicit val ledgerId = LedgerId(1.byteValue())
    val (newState: QuorumState, events) = sut(NewQuorumCandidates(fakeTxId, chainId, Set(myNodeId, otherNodeId)))
    assert(!newState.isQuorum)
    assert(events === ToPublish(AddQuery(IdQuery(Set(otherNodeId))), Some(ToPublish(QuorumLost(chainId)))))
  }

  it should "indicate we are not in the quorum if we query when a member and member not connected" in {
    val (sut, events) = QuorumState(
      chainId, myNodeId, Set(myNodeId, otherNodeId)
    )(CheckStatus)

    assert(events == ToPublish(AddQuery(IdQuery(Set(otherNodeId)))))
  }

  it should "produce correct correct quorum members in a many member scenario" in {
    val (sut, actions) = QuorumState(
      chainId, myNodeId, Set(myNodeId, otherNodeId, "third", "fourth", "fifth")
    )(PeerConnection(otherNodeId, Capabilities(chainId))
    )(PeerConnection("third", Capabilities(chainId)))

    assert(actions == ToPublish(Quorum(chainId, Set(otherNodeId, "third"), 2)))
  }

  it should "indicate we are in the quorum if quorum membership changes but we were already and still are in the quorum" in {
    val (sut, actions) = QuorumState(
      chainId, myNodeId, Set(myNodeId, otherNodeId, "thirdNode")
    )(PeerConnection(otherNodeId, Capabilities(chainId))
    )(PeerConnection("thirdNode", Capabilities(chainId)))

    assert(actions.asSeq.last === ToPublish(Quorum(chainId, Set(otherNodeId, "thirdNode"), 1)))
  }

  it should "maintain a quorum if member gets disconnected" in {
    val (sut, actions) = QuorumState(
      chainId, myNodeId, Set(myNodeId, otherNodeId, "thirdNode")
    )(PeerConnection(otherNodeId, Capabilities(chainId))
    )(PeerConnection("thirdNode", Capabilities(chainId))
    )(ConnectionLost(otherNodeId))

    assert(actions.asSeq.last === ToPublish(Quorum(chainId, Set("thirdNode"), 1)))
  }

  it should "maintain a quorum if non member gets disconnected" in {
    val (sut, actions: Action) = QuorumState(
      chainId, myNodeId, Set(myNodeId, otherNodeId)
    )(PeerConnection(otherNodeId, Capabilities(chainId))
    )(ConnectionLost(otherNodeId + "random")
    )

    assert(actions.asSeq.last == ToPublish(Quorum(chainId, Set(otherNodeId), 1)))

  }

  it should "indicate we have lost quorum if member gets disconnected" in {
    val (sut, actions: Action) = QuorumState(
      chainId, myNodeId, Set(myNodeId, otherNodeId)
    )(PeerConnection(otherNodeId, Capabilities(chainId))
    )(ConnectionLost(otherNodeId)
    )
    assert(actions.asSeq.last == ToPublish(QuorumLost(chainId)))
  }

}
