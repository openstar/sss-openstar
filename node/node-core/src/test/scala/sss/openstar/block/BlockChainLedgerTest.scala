package sss.openstar.block


import org.scalatest.concurrent.ScalaFutures.convertScalaFuture
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.must.Matchers.matchPattern
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper
import sss.db.{Db, FutureTx}
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.block.TestLedger.TestTxApplied
import sss.openstar.common.block.{BlockChainTx, BlockId, BlockTx}
import sss.openstar.db.DbWithMigrationFixture
import sss.openstar.ledger._
import sss.openstar.nodebuilder.BlockFactoriesBuilder
import sss.openstar.ledger.TollLedger
import sss.openstar.{DummySeedBytes, hash}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

/**
  * Created by alan on 2/15/16.
  */

object TestLedger {

  case class TestTxApplied(txId: TxId, ledgerId: LedgerId) extends TxApplied

  type ApplyFTx = (LedgerItem, BlockId, TxSeqApplied) => LedgerResultFTx

  def makeTollLedger(ledgeId: LedgerId, impl: ApplyFTx)(implicit db: Db): TollLedger =
    make(ledgeId, impl)
  def makeLedger(ledgeId: LedgerId, impl: ApplyFTx)(implicit db: Db): Ledger =
    make(ledgeId, impl)

  private def make(ledgeId: LedgerId, impl: ApplyFTx)(implicit db: Db): TollLedger = new TollLedger {
    override def ledgerId: LedgerId = ledgeId

    override def validate(ledgerId: LedgerId, signedTxEntry: SignedTxEntry, blockIndex: Long): FutureTx[Unit] = FutureTx.unit(())
    override def apply(
                        ledgerItem: LedgerItem,
                        blockId: BlockId,
                        txSeqApplied: TxSeqApplied): LedgerResult =
      applyFTx(ledgerItem, blockId, txSeqApplied).dbRunSyncGet

    override def applyFTx(ledgerItem: LedgerItem, blockId: BlockId, txSeqApplied: TxSeqApplied): LedgerResultFTx =
      impl(ledgerItem, blockId, txSeqApplied)
  }

}


class BlockChainLedgerTest extends AnyFlatSpec with DbWithMigrationFixture with BlockFactoriesBuilder {

  val testLedger = TestLedger.makeLedger(LedgerId(99),
    (ledgerItem, _, _) => FutureTx.unit(Right(NoSubEventsTxApplied(ledgerItem.txId)(LedgerId(99))))
  )

  val testTollLedger = TestLedger.makeTollLedger(LedgerId(99),
    (ledgerItem, _, _) => FutureTx.unit(Right(NoSubEventsTxApplied(ledgerItem.txId)(LedgerId(99))))
  )
  implicit val ledgers = new Ledgers(Map(testLedger.ledgerId.id -> testLedger), Map.empty, testTollLedger) {
    override def validate(blockTx: BlockTx)(implicit ec: ExecutionContext, db: Db): Future[Unit] = {
      Future.successful(())
    }
  }

  val ledger = BlockChainLedger(blockFactory, 1).dbRunSyncGet

  def createSeqLedgerItem: SeqLedgerItem  = {
    SeqLedgerItem(Seq(LedgerItem(testLedger.ledgerId, DummySeedBytes(32), DummySeedBytes(12))))
  }

  it should "allow preparing of a tx " in {
    val stx = createSeqLedgerItem
    val ledger = BlockChainLedger(blockFactory, 2).dbRunSyncGet
    val blkChnTx = ledger.prepare(stx).futureValue
    assert(blkChnTx.blockTx.index === 1)
    assert(blkChnTx.blockTx.ledgerItems === stx)
    assert(blkChnTx.height === 2)
  }

  it should "allow repeated journalling of the same tx " in {
    val stx = createSeqLedgerItem
    val ledger = BlockChainLedger(blockFactory, 22).dbRunSyncGet
    val bTx = BlockTx(1, stx)

    ledger.journal(bTx).dbRunSyncGet
    val blkChnTx = ledger.journal(bTx).dbRunSyncGet
    assert(blkChnTx.blockTx.index === 1)
    assert(blkChnTx.blockTx.ledgerItems === stx)
    assert(blkChnTx.height === 22)
  }

  it should "allow journalling of many txs" in {
    val h = 29
    val stx = createSeqLedgerItem
    val ledger = BlockChainLedger(blockFactory, h).dbRunSyncGet
    val bTx = BlockTx(1, stx)
    //val bcTx = BlockChainTx(22, bTx)
    val stx2 = createSeqLedgerItem
    val bTx2 = BlockTx(2, stx2)

    val blkChnTx = ledger.journal(bTx).dbRunSyncGet
    val blkChnTx2 = ledger.journal(bTx2).dbRunSyncGet
    assert(blkChnTx.blockTx.index === 1)
    assert(blkChnTx.blockTx.ledgerItems === stx)
    assert(blkChnTx.height === h)
    assert(blkChnTx2.blockTx.index === 2)
    assert(blkChnTx2.blockTx.ledgerItems === stx2)
    assert(blkChnTx2.height === h)
  }
  it should "allow correct preparing of the tx" in {
    val stx = createSeqLedgerItem
    val ledger = BlockChainLedger(blockFactory, 23).dbRunSyncGet

    val goodAttempt = ledger.prepare(stx).futureValue

    assert(goodAttempt.blockTx.index === 1)
    assert(goodAttempt.blockTx.ledgerItems === stx)
    assert(goodAttempt.height === 23)
  }

  it should "allow repeated preparing in order" in {

    val ledger = BlockChainLedger(blockFactory, 4).dbRunSyncGet
    val all = Future.sequence(1 to 15 map { i =>
      val stx = LedgerItem(testLedger.ledgerId, DummySeedBytes(32), DummySeedBytes(12))

      ledger.prepare(stx).map { case a => {
        (a, i)
      }
      }
    })

    all.futureValue.foreach {
      case (blkTx, i) =>
        assert(blkTx.blockTx.index == i)
    }
  }

  it should "accumulate events generated by ledgers" in {

    val firstLedgerId: LedgerId = LedgerId(10.toByte)
    val secondLedgerId = LedgerId(20.toByte)
    val thirdLedgerId = LedgerId(30.toByte)
    val appLedgerId1 = LedgerId(31.toByte)
    val appLedgerId2 = LedgerId(32.toByte)
    val stxFirst = LedgerItem(firstLedgerId, DummySeedBytes(32), DummySeedBytes(12))
    val stxSecond = LedgerItem(secondLedgerId, DummySeedBytes(32), DummySeedBytes(12))
    val stxThird = LedgerItem(thirdLedgerId, DummySeedBytes(32), DummySeedBytes(12))
    val stxAppFirst = LedgerItem(appLedgerId1, DummySeedBytes(32), DummySeedBytes(12))
    val stxAppSecond = LedgerItem(appLedgerId2, DummySeedBytes(32), DummySeedBytes(12))

    val blockId = BlockId(1,1)
    val firstTxId = stxFirst.txId
    val secondTxId = stxSecond.txId
    val thirdTxId = stxThird.txId
    val firstAppTxId = stxAppFirst.txId
    val firstLedgerOk = TestTxApplied(firstTxId, firstLedgerId)

    val appLedger1Ok = TestTxApplied(firstAppTxId, firstLedgerId)
    val secondLedgerOk = TestTxApplied(secondTxId, secondLedgerId)
    val threeApplied = TestTxApplied(thirdTxId, thirdLedgerId)
    val thirdLedgerOk = TxSeqApplied(stxThird.txId, Seq(threeApplied))(thirdLedgerId)

    val ledger1 = TestLedger.makeLedger(firstLedgerId, {
      case (_, _, _) => FutureTx.unit(Right(firstLedgerOk))
    })
    val ledger2 = TestLedger.makeLedger(secondLedgerId, {
      case (_, _, seqApplied) if seqApplied.values == Seq(firstLedgerOk, threeApplied) => FutureTx.unit(Right(secondLedgerOk))
      case _ => FutureTx.failed(new IllegalArgumentException("FAIL"))
    })

    val ledger3 = TestLedger.makeTollLedger(thirdLedgerId, {
      case (_, _, _) => FutureTx.unit(Right(thirdLedgerOk))
    })

    val appLedger1= TestLedger.makeLedger(appLedgerId1, {
      case (_,_,_) => FutureTx.unit(Right(appLedger1Ok))
    })
    val appLedger2 = TestLedger.makeLedger(appLedgerId2, {
      case (_, _, _) => FutureTx.failed(new IllegalArgumentException("FAIL"))
    })
    val mapLedgers = Map(ledger1.ledgerId.id -> ledger1, ledger2.ledgerId.id -> ledger2, ledger3.ledgerId.id -> ledger3)
    val appLedgers = Map(appLedger1.ledgerId.id -> appLedger1, appLedger2.ledgerId.id -> appLedger2)
    val ledgers = new Ledgers(mapLedgers, appLedgers, ledger3)

    ledgers.applyFTx(stxFirst, blockId, TxSeqApplied(stxFirst.txId)(firstLedgerId)).dbRunSyncGet should matchPattern {
      case r@Right(TxSeqApplied(`firstTxId`, s@Seq(`threeApplied`,`firstLedgerOk`))) =>
    }

    ledgers.applyFTx(stxSecond, blockId, TxSeqApplied(stxSecond.txId, Seq(firstLedgerOk))(secondLedgerId)).dbRunSyncGet should matchPattern {
      case r@Right(TxSeqApplied(`secondTxId`, s@Seq(`firstLedgerOk`,`threeApplied`, `secondLedgerOk`))) =>
    }

    ledgers.applyFTx(stxAppFirst, blockId, TxSeqApplied(stxSecond.txId, Seq(firstLedgerOk))(secondLedgerId)).dbRunSyncGet should matchPattern {
      case r@Right(TxSeqApplied(`secondTxId`, s@Seq(`firstLedgerOk`, `threeApplied`, `appLedger1Ok`))) =>
    }

//    ledgers.applyFTx(stxAppSecond, blockId, TxSeqApplied(stxSecond.txId, Seq(firstLedgerOk))(secondLedgerId)).dbRunSync should matchPattern {
//      case util.Failure(_: IllegalArgumentException) =>
//    }
  }
  /*it should "allow a tx to be committed by BlockId (once)" in {
    val stx = resetUTXOBlockAndCreateTx(2)
    val ledger = BlockChainLedger(2).dbRunSyncGet
    val blkChnTx = ledger.journal(BlockTx(34, stx)).dbRunSyncGet
    ledger.commit(BlockId(2, 1))
    assert(ledger(stx).isFailure)
  }

  it should "allow a tx to be committed (only once) " in {
    val stx = resetUTXOBlockAndCreateTx(2)
    val ledger = BlockChainLedger(2)
    val blkChnTx = ledger.journal(BlockTx(34, stx))
    ledger.commit
    assert(ledger(stx).isFailure)
  }*/


  /*it should " prevent commit non existent block " in {

    val ledger = BlockChainLedger(2).dbRunSyncGet
    val stx = resetUTXOBlockAndCreateTx(2)

    val nonExistentBlockOrNumTxs = 999
    val b = BlockTx(nonExistentBlockOrNumTxs, LedgerItem(TestLedger.ledgerId, DummySeedBytes(32), DummySeedBytes(12)))
    intercept[Exception]( ledger.commit(b).get)

    val blkChnTx = ledger.journal(BlockTx(34, stx)).dbRunSyncGet
    intercept[Exception](ledger.commit(BlockTx(nonExistentBlockOrNumTxs, LedgerItem(TestLedger.ledgerId, DummySeedBytes(32), DummySeedBytes(12)))).get)
  }*/

}
