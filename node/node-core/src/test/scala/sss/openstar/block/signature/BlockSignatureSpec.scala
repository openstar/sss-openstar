package sss.openstar.block.signature


import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import scorex.crypto.signatures.{PublicKey, Signature}
import sss.ancillary.FutureOps.AwaitResult
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.DummySeedBytes
import sss.openstar.chains.Chains.GlobalChainIdBuilder
import sss.openstar.db.DbWithMigrationFixture
import sss.openstar.nodebuilder.{BlockFactoriesBuilder, BlockHeightToTableBuilder}

/**
  * Created by alan on 4/22/16.
  */
class BlockSignatureSpec extends AnyFlatSpec with Matchers with DbWithMigrationFixture with BlockFactoriesBuilder with BlockHeightToTableBuilder with GlobalChainIdBuilder {


  "A Block Sig" should " be persisted " in {
    blockSignaturesFactory.getQuorumSigs(2)
      .addFTx(Signature(DummySeedBytes(50)), PublicKey(DummySeedBytes(90)),"KeyType", "someNodeId")
      .dbRunSyncGet
  }

  it should "prevent a second signature from the same node id " in {
    intercept[Exception] {blockSignaturesFactory.getQuorumSigs(2)
      .addFTx(Signature(DummySeedBytes(50)), PublicKey(DummySeedBytes(90)),"KeyType", "someNodeId")
      .dbRunSyncGet
    }
  }

  it should " not retrieve a non existent sig " in {
    assert(blockSignaturesFactory.getQuorumSigs(2).indexOfBlockSignatureFTx("NEVERADDED").dbRunSyncGet.isEmpty)
    assert(blockSignaturesFactory.getQuorumSigs(22).indexOfBlockSignatureFTx("NEVERADDED").dbRunSyncGet.isEmpty)
  }

  it should " re write a sig correctly " in {
    val sigAdded = blockSignaturesFactory.getQuorumSigs(3).addFTx(
      Signature(DummySeedBytes(50)),
      PublicKey(DummySeedBytes(90)),
      "KeyType",
      "someNodeId").dbRunSyncGet
    val sigRewritten = blockSignaturesFactory.getQuorumSigs(3).write(sigAdded).await()
    assert(sigAdded === sigRewritten)
  }

  it should " retrieve the correct index for signatures " in {
    (0 to 10) foreach  { i =>
      blockSignaturesFactory
        .getQuorumSigs(4)
        .addFTx(
          Signature(DummySeedBytes(50)),
          PublicKey(DummySeedBytes(90)),
          "keyType",
          s"nodeId$i"
        ).dbRunSyncGet
    }

    val sigs = blockSignaturesFactory.getQuorumSigs(4)
    (0 to 10) foreach  { i =>
      assert(blockSignaturesFactory.getQuorumSigs(4).indexOfBlockSignatureFTx(s"nodeId$i").dbRunSyncGet.isDefined)
      assert(blockSignaturesFactory.getQuorumSigs(4).indexOfBlockSignatureFTx(s"nodeId$i").dbRunSyncGet.get  === i+1)
    }

  }

  it should " retrieve only the specified number of signatures but in order " in {
    (0 to 10) foreach  { i =>
      blockSignaturesFactory.getQuorumSigs(5).addFTx(
        Signature(DummySeedBytes(50)),
        PublicKey(DummySeedBytes(90)),
        "KeyType",
        s"nodeId$i").dbRunSyncGet
    }
    val sigs = blockSignaturesFactory.getQuorumSigs(5)
    val returned = sigs.signatures(5).await()
    assert(returned.size === 5)
    for(i <- returned.indices) {assert(returned(i).index === i+1)}
  }
}
