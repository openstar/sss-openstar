package sss.openstar.block

import org.scalatest.concurrent.ScalaFutures
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.scalatest.time.{Millis, Seconds, Span}
import scorex.crypto.signatures.Signature
import sss.ancillary.FutureOps.AwaitResult
import sss.db.FutureTx
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.DummySeedBytes
import sss.openstar.account.{AccountKeysFactoryFixture, KeyGeneratorFixture, NodeIdTag, NodeIdentityManagerFixture}
import sss.openstar.common.block.BlockId
import sss.openstar.common.builders.{CpuBoundExecutionContextFixture, KeyFactoryBuilder, VerifierFactoryBuilder}
import sss.openstar.contract.{FindPublicKeyAccFTxOpt, FindPublicKeyAccOpt}
import sss.openstar.db.DbWithMigrationFixture
import sss.openstar.identityledger.{DefaultIdentifier, IdentityService}
import sss.openstar.ledger.{LedgerId, LedgerItem, Ledgers}
import sss.openstar.nodebuilder.BlockFactoriesAndMerklePersisterBuilder
import sss.openstar.oracleownerledger.{OracleClaim, OracleLedgerOwnerLedger}
import sss.openstar.util.{IOExecutionContext, TxSign}

import java.util.Date
import sss.openstar.ledger.TxNotApplied
/**
  * Created by alan on 2/15/16.
  */

class BlockChainSpec extends AnyFlatSpec
  with Matchers
  with BlockFactoriesAndMerklePersisterBuilder
  with KeyGeneratorFixture
  with DbWithMigrationFixture
  with ScalaFutures
  with KeyFactoryBuilder
  with VerifierFactoryBuilder
  with NodeIdentityManagerFixture
  with CpuBoundExecutionContextFixture
  with AccountKeysFactoryFixture {

  implicit override val patienceConfig: PatienceConfig =
    PatienceConfig(timeout = scaled(Span(3, Seconds)), interval = scaled(Span(15, Millis)))

  val merkleRoot= "12345678123456781234567812345678".getBytes
  val prevHash = "12345678123456781234567812345678".getBytes


  implicit val kf = keysFactoryLookup
  val bc: BlockChainImpl = BlockChainImpl.apply(blockFactory, blockSignaturesFactory, merklePersister)

  bc.lastCommittedBlockHeader.dbRunSyncGet

  var genBlk: BlockHeader = _



  "A block header in the chain " should " have it's hash in the following block header " in {

    val header1 = BlockHeader(1, 0 , prevHash, merkleRoot, false, new Date())
    val header2 = BlockHeader(1, 0, header1.hash, merkleRoot, true, new Date())

    assert(header2.hashPrevBlock === header1.hash)
  }

  "A block chain " should " retrieve a genesis block " in {
    genBlk = bc.blockHeader(1).dbRunSyncGet
    assert(genBlk.height === 1)
    assert(genBlk.numTxs === 0)
  }

  it should " be able to sign a block header " in {

    val someNodeId = "whoareyou"
    val ck = defaultAccountKeysFactory.keySigner()
    val signed = ck.sign(genBlk.hash)
    bc.quorumSigs(1)
      .addSignature(
        signed,
        ck.typedPublicKey.publicKey,
        ck.typedPublicKey.keyType,
        someNodeId
      ).await()
    assert(bc.quorumSigs(1).indexOfBlockSignature(someNodeId).await().isDefined)
    assert(bc.nonQuorumSigs(1).indexOfBlockSignature(someNodeId).await().isEmpty)
    assert(bc.quorumSigs(1).indexOfBlockSignature(someNodeId).await().get == 1)

  }


  it should "refuse a signature for the wrong block" in {

    val someNodeId = "whoareyou"
    //val header2 = BlockHeader(1, 0, genBlk.hash, merkleRoot, new Date())

    val ck = defaultAccountKeysFactory.keySigner()
    val signed = ck.sign(genBlk.hash)

    bc.quorumSigs(1).addSignature( signed, ck.typedPublicKey, someNodeId).await()

    // check non existant block...
    intercept[Exception](bc.quorumSigs(2).addSignature(signed, ck.typedPublicKey, someNodeId).await())

  }

  it should " not find signatures that haven't occurred " in {
    val someNodeId = "totallunknown"
    assert(bc.quorumSigs(1).indexOfBlockSignature(someNodeId).await().isEmpty)
  }

  it should " not find signatures that for another block " in {
    val someNodeId = "whoareyou"
    assert(bc.quorumSigs(99).indexOfBlockSignature(someNodeId).await().isEmpty)
  }

  it should " find the correct last block " in {
    assert(bc.lastCommittedBlockHeader.dbRunSyncGet === genBlk)
  }


  private def matchSecondBlock(block: BlockHeader, lastBlockHash: Array[Byte]): Unit = {
    assert(block.height === 2)
    //assert(block.numTxs === 1)
    assert(new Date().getTime - block.time.getTime < 1000)
    //assert(block.merkleRoot !== merkleRoot)
    assert(block.hashPrevBlock === lastBlockHash)
  }
  it should " close a block correctly " in {

    val lastBlock = bc.lastCommittedBlockHeader.dbRunSyncGet

    val newHeader = bc.closeBlock(lastBlock).await()
    val committedNewHeader = bc.commitBlockHeaderFTx(newHeader.height).dbRunSyncGet

    matchSecondBlock(bc.lastCommittedBlockHeader.dbRunSyncGet, lastBlock.hash)

    bc.blockHeader(newHeader.height).dbRunSyncGet.hashCode() should be (committedNewHeader.hashCode())
    assert(bc.lastCommittedBlockHeader.dbRunSyncGet === committedNewHeader)
    assert(lastBlock !== newHeader)
  }

  it should " lookup up a block correctly " in {

    val firstBlock = bc.blockHeader(1).dbRunSyncGet
    assert(firstBlock === genBlk)
    matchSecondBlock(bc.blockHeader(2).dbRunSyncGet, firstBlock.hash)

  }

  it should " prevent a second genesis block " in {
    intercept[Exception]{bc.genesisBlock().dbRunSyncGet}
  }


  it should "validate in block ok" in {

    val height = 8888

    implicit val ownerLedgerId = LedgerId(5.toByte)
    val maxAllowedOwnedLedgers = 3
    val maxAllowedOwnersPerLedger = 4
    val owner1Id = "sfsdfsd"

    val identifierValidator = new DefaultIdentifier


    val owner1IdNodeId = addToFixtureAndGetNodeIdentity(owner1Id, NodeIdTag.defaultTag, testPassword)

    def findPublicAccountKeyOpt: FindPublicKeyAccFTxOpt = {
      case (`owner1Id`, _) => FutureTx.unit(Some(
        owner1IdNodeId.defaultNodeVerifier
      ))
      case x => FutureTx.unit(None)
    }

    val ledgerForOwners = new OracleLedgerOwnerLedger(
      identifierValidator,
      findPublicAccountKeyOpt,
      maxAllowedOwnersPerLedger,
      maxAllowedOwnedLedgers,
      keysFactoryLookup
    )

    val dummyToll = TestLedger.makeTollLedger(ownerLedgerId, (_, _, _) => FutureTx.unit((Right(TxNotApplied(Array.emptyByteArray)))))
    val ledgersMap = Map(ownerLedgerId.id -> ledgerForOwners)
    implicit val ledgers = new Ledgers(ledgersMap, Map.empty, dummyToll)


    val claim = OracleClaim(
      LedgerId(100),
      owner1Id,
      Seq.empty
    )
    val stx = TxSign.simplySignedTxEntry(owner1IdNodeId, claim.txId, claim.toBytes).await()

    val li = LedgerItem(
      ownerLedgerId,
      claim.txId,
      stx.toBytes
    )
    //assert(block999.entries.isEmpty)
    val blkChain = BlockChainLedger(blockFactory, height).dbRunSyncGet

    val firstTimeValidateOk = blkChain.prepare(li).futureValue

    // Cannot prepare a tx and then run it again.
    blkChain.prepare(li).failed

    val firstTimeOk = ledgerForOwners.apply(
      li,
      BlockId(height, 0)
    )
    assert(firstTimeOk.isRight)

    blkChain.prepare(li).failed.futureValue shouldBe an[Exception]


    val block999 = blockFactory.getBlock(height).dbRunSyncGet
    assert(block999.entries.dbRunSyncGet.isEmpty)
  }



  "A blockchain header" should "get committed" in {
    val header = bc.lastCommittedBlockHeader.dbRunSyncGet
    val retrieved = bc.blockHeader(header.height).dbRunSyncGet
    assert(header == retrieved)
    assert(header.committed)

    val blockId = BlockId(3,0)
    val closed = bc.closeBlock(blockId).await()
    assert(!closed.committed)
    val committed = bc.commitBlockHeaderFTx(3).dbRunSyncGet
    assert(committed.committed)

  }
}
