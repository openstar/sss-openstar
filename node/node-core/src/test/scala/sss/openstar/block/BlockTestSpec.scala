package sss.openstar.block

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.DummySeedBytes
import sss.openstar.chains.Chains.GlobalChainIdBuilder
import sss.openstar.common.block._
import sss.openstar.db.DbWithMigrationFixture
import sss.openstar.ledger._
import sss.openstar.nodebuilder.BlockFactoriesAndMerklePersisterBuilder

/**
  * Created by alan on 2/15/16.
  */
class BlockTestSpec extends AnyFlatSpec with Matchers with DbWithMigrationFixture with BlockFactoriesAndMerklePersisterBuilder with GlobalChainIdBuilder {

  val ledgerId = LedgerId(99)
  import scala.concurrent.ExecutionContext.Implicits.global

  val someBlock = blockFactory.getBlock(99).dbRunSyncGet
  val block10 = blockFactory.getBlock(10).dbRunSyncGet
  val block996 = blockFactory.getBlock(9996).dbRunSyncGet
  val block997 = blockFactory.getBlock(9997).dbRunSyncGet
  val block998 = blockFactory.getBlock(9998).dbRunSyncGet
  val block999 = blockFactory.getBlock(9999).dbRunSyncGet

  lazy val dumbSignedTx = LedgerItem(ledgerId, DummySeedBytes(32), DummySeedBytes(12))

  lazy val dumbSignedTx2 = LedgerItem(ledgerId, DummySeedBytes(32), DummySeedBytes(12))

  lazy val dumbSignedTx3 = LedgerItem(ledgerId, DummySeedBytes(32), DummySeedBytes(12))

  lazy val dumbSignedTx4 = LedgerItem(ledgerId, DummySeedBytes(32), DummySeedBytes(12))

  "A block " should "start off empty" in {
    assert(block999.entries.dbRunSyncGet.isEmpty)
  }

  it should "allow an uncommitted tx to be written " in {
      val index = 1
      assert(block999.entries.dbRunSyncGet.size === 0)
      assert(block999.page(1,100).dbRunSyncGet == Seq())
      val tx = SeqLedgerItem(dumbSignedTx)
      val writtenIndex = block999.journal(index, tx).dbRunSyncGet
      block999.commit(index, tx).dbRunSyncGet
      assert(writtenIndex === index)
      assert(block999.entries.dbRunSyncGet.size === 1)
      assert(block999.page(1,100).dbRunSyncGet.size == 1)
      assert(block999.page(1,100).dbRunSyncGet.head === Left(BlockTx(index, dumbSignedTx)), "Signed tx comparison failed?")
      assert(block999.entries.dbRunSyncGet === Seq(BlockTxId(dumbSignedTx.txId, index)))
      assert(block999.entries.dbRunSyncGet === Seq(BlockTxId(dumbSignedTx.txId, index)))
      assert(block999.getUnCommitted.dbRunSyncGet === Seq())

  }

  it should "allow an uncommitted tx to be committed" in {
    val tx = SeqLedgerItem(dumbSignedTx)
    block999.commit(1, tx).dbRunSyncGet
    assert(block999.getUnCommitted.dbRunSyncGet.size === 0)

  }

  it should "allow a tx written, and retrieved " in {
    val index = block10.journal(0, SeqLedgerItem(dumbSignedTx2)).dbRunSyncGet
    val got = block10.get(dumbSignedTx2.txId).dbRunSyncGet
    assert(block10(dumbSignedTx2.txId).dbRunSyncGet ===  BlockTx(index, dumbSignedTx2))
    assert(block10.getUnCommitted.dbRunSyncGet.size === 1)

  }

  it should "not retrieve non existent txid " in {
    assert(block999.get(DummySeedBytes(TxIdLen)).dbRunSyncGet.isEmpty)
  }


  it should " allow standard ledger entries to be persisted " in {

    val stx = LedgerItem(ledgerId, DummySeedBytes(32), DummySeedBytes(12))

    intercept[NoSuchElementException] {
      someBlock(stx.txId).dbRunSyncGet
    }

    someBlock.commit(BlockTx(someBlock.getNextIndex, SeqLedgerItem(stx))).dbRunSyncGet
    val retrieved = someBlock(stx.txId).dbRunSyncGet
    assert(retrieved.ledgerItems.value.head == stx)
  }


  it should "reject a non previously journalled tx" in {
    val blkTxId = BlockTxId(dumbSignedTx3.txId, 1)
    block996.reject(blkTxId).dbRunSyncGet
    val s1 = block996.page(1, 100).dbRunSyncGet.toSet
    s1 shouldBe Set(Right(blkTxId))
  }

  it should "reject a previously journalled tx" in {
    val index = 1
    block997.journal(index, SeqLedgerItem(dumbSignedTx3)).dbRunSyncGet
    block997.reject(BlockTxId(dumbSignedTx3.txId, index)).dbRunSyncGet

    assert(block997.getLastRejected.dbRunSyncGet === index)
  }

  it should "prevent a rejected tx from being returned as a committed tx" in {
    val index = 1
    block998.journal(index, SeqLedgerItem(dumbSignedTx4)).dbRunSyncGet
    block998.reject(BlockTxId(dumbSignedTx4.txId, index)).dbRunSyncGet

    assert(block998.getLastRejected.dbRunSyncGet === index)
    assert(block998.getLastCommitted.dbRunSyncGet === 0)
  }

}
