package sss.openstar.systemupdates

import akka.actor.ActorSystem
import akka.http.scaladsl.model.Uri
import org.scalatest.RecoverMethods.recoverToSucceededIf
import org.scalatest.concurrent.PatienceConfiguration.Timeout
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.db.Db
import sss.openstar.UniqueNodeIdentifier
import sss.openstar.db.DbWithMigrationFixture
import sss.openstar.hash.Digest32
import sss.openstar.systemledger.SystemService.UpdateProposal
import sss.openstar.systemupdates.SystemUpdateTracker._

import java.nio.file.Path
import java.util.Base64
import scala.concurrent.Future
import scala.concurrent.duration.DurationInt


abstract class SystemUpdateTrackerFixture(implicit val db: Db)  {
  val uniqueProposal = "V1"
  val otherUniqueProposal = "V2"
  val chkSum = Digest32(Array.fill(32)(9.toByte))
  val otherChkSum = Digest32(Array.fill(32)(8.toByte))

  val url: String = "https://asdasdasdasdad.com?here"
  val otherUrl: String = "https://asdasdasdasdad.com?here"

  lazy val updateDestinationFolder: String = "."

  lazy val folderContainingAutoUpdates: Path = Path.of(".")


  def getDownloadUpdate: DownloadUpdate = DownloadUpdate

  lazy val systemUpdateTracker = new SystemUpdateTracker(
    getDownloadUpdate,
    folderContainingAutoUpdates,
    updateDestinationFolder
  )

  val proposingNodeId: UniqueNodeIdentifier = "sdfsdfsdfs"

  val requiredFromBlockHeight: Long = 9999
  val updatePreparationComplete: Boolean = false

  val newProposal: UpdateProposal = UpdateProposal(proposingNodeId,
    uniqueProposal,
    url,
    chkSum,
    requiredFromBlockHeight,
    updatePreparationComplete
  )

  val otherProposal: UpdateProposal = UpdateProposal(proposingNodeId,
    otherUniqueProposal,
    otherUrl,
    otherChkSum,
    requiredFromBlockHeight,
    updatePreparationComplete
  )

  val otherNoted = NotedProposal(otherProposal.uniqueUpdateIdentifier,
    otherProposal.url, otherProposal.checksum)
  val otherCompleted = CompletedProposal(otherProposal.uniqueUpdateIdentifier, otherProposal.url, otherProposal.checksum)

  val noted = NotedProposal(newProposal.uniqueUpdateIdentifier,
    newProposal.url, newProposal.checksum)

  val prepared = PreparedForProposal(
    newProposal.uniqueUpdateIdentifier,
    newProposal.url,
    newProposal.checksum
  )

  val completed = CompletedProposal(
    newProposal.uniqueUpdateIdentifier,
    newProposal.url,
    newProposal.checksum
  )

}

class SystemUpdateTrackerSpec extends AnyFlatSpec with Matchers with ScalaFutures with DbWithMigrationFixture {

  "A systemUpdatetracker" should "initially have no updates" in new SystemUpdateTrackerFixture {
    systemUpdateTracker.findLast().futureValue shouldBe NoUpdates
    systemUpdateTracker.findLastCompleted().futureValue shouldBe None
  }

  it should "allow the addition of a proposal" in new SystemUpdateTrackerFixture {

    systemUpdateTracker.note(newProposal).futureValue shouldBe noted
    systemUpdateTracker.findLast().futureValue shouldBe noted
  }

  it should "prevent the addition of a proposal with the same id" in new SystemUpdateTrackerFixture {

    val sameId = otherProposal.copy(uniqueUpdateIdentifier = newProposal.uniqueUpdateIdentifier)
    systemUpdateTracker.note(sameId).failed.futureValue shouldBe an[Exception]
    systemUpdateTracker.findLast().futureValue shouldBe noted
  }

  it should "download the proposal url and fail if checksum bad" in new SystemUpdateTrackerFixture {


    implicit val as = ActorSystem()
    import as.dispatcher
    val realUrl = "https://github.com/input-output-hk/psg-cardano-wallet-api/releases/download/v0.2.4/psg-cardano-wallet-api-assembly.zip"

    val notedWithRealUrl = noted.copy(url = realUrl)

    override def getDownloadUpdate: DownloadUpdate = new DownloadUpdate {
      override def download(uri: Uri, pathToFolder: Path)(implicit ac: ActorSystem): Future[Digest32] =
        Future.successful(notedWithRealUrl.checksum)
    }
    recoverToSucceededIf[IllegalStateException] {
      systemUpdateTracker.download(notedWithRealUrl)
    }

  }

  it should "download the proposal url and update the status" in new SystemUpdateTrackerFixture {

    implicit val as = ActorSystem()
    val realUrl = "https://github.com/input-output-hk/psg-cardano-wallet-api/releases/download/v0.2.4/psg-cardano-wallet-api-assembly.zip"

    val byteAry = Base64.getDecoder.decode("IKJc++GYTeVFIf3Y0sg3enAGwA5nASPxdYCJxvvLzoQ=")

    val actualChecksum = Digest32(byteAry)
    val notedWithRealUrl = noted.copy(url = realUrl, checksum = actualChecksum)

    val downloadedShouldBe = DownloadedProposal(
      newProposal.uniqueUpdateIdentifier,
      newProposal.url,
      newProposal.checksum
    )


    whenReady (
      systemUpdateTracker.download(notedWithRealUrl)
      , Timeout(1.minute)) { result =>
      result shouldBe downloadedShouldBe
    }
  }


  it should "allow the proposal to be marked prepared" in new SystemUpdateTrackerFixture {

    val latest = systemUpdateTracker.findLast().futureValue

    systemUpdateTracker.setPrepared(noted.uniqueProposalId).futureValue shouldEqual prepared

  }

  it should "allow the proposal to be marked completed" in new SystemUpdateTrackerFixture {

    systemUpdateTracker
      .setLastCompleted()
      .futureValue

    val latest = systemUpdateTracker.findLast().futureValue
    latest shouldBe completed
  }

  it should "allow the addition of a second proposal" in new SystemUpdateTrackerFixture {

    systemUpdateTracker.note(otherProposal).futureValue shouldBe otherNoted
    systemUpdateTracker.findLast().futureValue shouldBe otherNoted
  }

  it should "find last completed proposal" in new SystemUpdateTrackerFixture {
    systemUpdateTracker.findLastCompleted().futureValue shouldBe Some(completed)

    systemUpdateTracker.setPrepared(otherProposal.uniqueUpdateIdentifier).futureValue
    systemUpdateTracker.setLastCompleted().futureValue
    systemUpdateTracker.findLastCompleted().futureValue shouldBe Some(otherCompleted)
  }

}
