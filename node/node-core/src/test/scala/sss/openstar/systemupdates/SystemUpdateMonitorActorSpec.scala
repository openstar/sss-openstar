package sss.openstar.systemupdates

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import sss.ancillary.ByteArrayEncodedStrOps._
import sss.ancillary.FutureOps.AwaitResult
import sss.db.Db
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.account.NodeIdentityManagerFixture
import sss.openstar.chains.Chains.GlobalChainIdBuilder
import sss.openstar.common.builders.{ActorSystemBuilder, CpuBoundExecutionContextFixture, NodeIdentityBuilder}
import sss.openstar.db.DbWithMigrationFixture
import sss.openstar.hash.Digest32
import sss.openstar.nodebuilder._
import sss.openstar.schemamigration.SqlSchemaNames.TableNames.systemUpdateTableName
import sss.openstar.systemledger.SystemService
import sss.openstar.systemledger.SystemService.UpdateProposal
import sss.openstar.systemledger.SystemServiceTestHelper.Ops
import sss.openstar.systemupdates.SystemUpdateMonitorActor.CheckForUpdates
import sss.openstar.systemupdates.SystemUpdateTracker._
import sss.openstar.{DecoderFixture, UniqueNodeIdentifier}

import java.nio.file.Path
import scala.concurrent.duration.DurationInt


abstract class SystemUpdateMonitorActorFixture(implicit val db: Db) extends MessageEventBusBuilder
  with DecoderFixture
  with SendTxBuilder
  with NodeIdentityManagerFixture
  with NodeIdentityBuilder
  with GlobalChainIdBuilder
  with AutoUpdateConfig
  with ActorSystemBuilder
  with RootActorContextBuilder
  with CpuBoundExecutionContextFixture {

  val currentBlockHeight = () => 1L
  val systemService: SystemService = new SystemService()

  lazy val updateDownloadFolder: String = "."

  override val rootAutoUpdateFolder: Path = Path.of(".")
  override val rootTargetFolder: Path = Path.of(".")

  val systemLedgerOwners: () => Set[UniqueNodeIdentifier] = () => Set()

  lazy val systemUpdateTracker = new SystemUpdateTracker(DownloadUpdate, folderContainingAutoUpdates, updateDownloadFolder)

  override lazy val markUpdateComplete: MarkUpdateComplete = () =>
    systemUpdateTracker.setLastCompleted()

  val proposingNodeId: UniqueNodeIdentifier = "sdfsdfsdfszzxzc"

  val requiredFromBlockHeight: Long = 9999
  val updatePreparationComplete: Boolean = false

  val realUrl = "https://github.com/input-output-hk/psg-cardano-wallet-api/releases/download/v0.2.4/psg-cardano-wallet-api-assembly.zip"

  val byteAry = "IKJc--GYTeVFIf3Y0sg3enAGwA5nASPxdYCJxvvLzoQ=".fromBase64Str

  val actualChecksum = Digest32(byteAry)

  val uniqueProposal = "V1"

  val newProposal: UpdateProposal = UpdateProposal(proposingNodeId,
    uniqueProposal,
    realUrl,
    actualChecksum,
    requiredFromBlockHeight,
    updatePreparationComplete
  )

  lazy val probe: TestProbe = TestProbe()

  lazy val startSystemUpdateMonitorActor: ActorRef =
    SystemUpdateMonitorActor(
      Props(new SystemUpdateMonitorActor(
        systemUpdateTracker,
        currentBlockHeight,
        systemLedgerOwners,
        systemService,
        1000.minutes // i.e. never
      ) {
        override def receive: Receive = message => {
          probe.testActor ! message
          super.receive(message)
        }
      })
    )

  def deleteAllUpdates(): Unit = {
    db.executeSql(s"TRUNCATE TABLE $systemUpdateTableName;").dbRunSyncGet
  }
}
class SystemUpdateMonitorActorSpec extends TestKit(ActorSystem("SystemUpdateMonitorActorSpec"))
  with ImplicitSender
  with AnyWordSpecLike
  with Matchers
  with ScalaFutures
  with DbWithMigrationFixture {


  override def afterAll(): Unit = {
    try super.afterAll()
    finally TestKit.shutdownActorSystem(system)
  }

  "SystemUpdateMonitorActor" must {

    "succeed on happy path" in new SystemUpdateMonitorActorFixture {
      try {
        systemService.insertUpdateProposal(newProposal)
        startSystemUpdateMonitorActor ! CheckForUpdates

        probe.expectMsgAllOf(CheckForUpdates, NoUpdates, Some(newProposal))
        probe.expectMsgAllConformingOf(100.seconds, classOf[NotedProposal], classOf[DownloadedProposal], classOf[PreparedForProposal])
        probe.expectNoMessage()

        // bump to completed, this role is done by the Autoupdater
        markUpdateComplete().futureValue

        startSystemUpdateMonitorActor ! CheckForUpdates

        probe.expectMsgAllOf(CheckForUpdates)
        probe.expectMsgClass(classOf[CompletedProposal])
        probe.expectMsg(None)
        probe.expectNoMessage()
      } finally {
        systemUpdateTracker.deleteDownloads(newProposal.uniqueUpdateIdentifier)
        deleteAllUpdates()
        systemService.deleteExistingProposals
      }
    }

    "don't download already supported proposals" in new SystemUpdateMonitorActorFixture() {
      val proposal: UpdateProposal = newProposal.copy(uniqueUpdateIdentifier = "alphaera")
      systemService.insertUpdateProposal(proposal)
      systemService.listProposals().futureValue shouldEqual Seq(proposal)
      startSystemUpdateMonitorActor ! CheckForUpdates
      probe.expectMsg(CheckForUpdates)
      probe.expectMsg(NoUpdates)
      probe.expectMsg(None)
      probe.expectNoMessage()
    }

  }
}
