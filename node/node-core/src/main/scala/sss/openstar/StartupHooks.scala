package sss.openstar

import akka.actor.Actor.Receive
import akka.actor.{Actor, ActorContext, ActorLogging, ActorRef, Props}
import sss.openstar.StartupHooks.{DoNextHook, HookDone, NextHook}
import sss.openstar.block.Synchronized
import sss.openstar.chains.Chains.GlobalChainIdMask
import sss.openstar.chains.QuorumFollowersSyncedMonitor.BlockChainReady
import sss.openstar.network.MessageEventBus

import scala.concurrent.duration.DurationInt

/**
 * Reacts to the BlockchainReady event and sets up any attributes thaqt a node might
 * need the running chain to set. Than exits.
 */
object StartupHooks {

  type NextHook = (ActorRef, ActorContext) => Receive

  def terminatingHook(actorRef: ActorRef, context: ActorContext): Receive = {

    case DoNextHook =>
      context.stop(actorRef)

  }

  trait NextHookImpl {
    val nextHook: NextHook

    def handleHookDone(self: ActorRef,
                       context: ActorContext): Receive = {

      case HookDone =>
        context become nextHook(self, context)
        self ! DoNextHook

    }
  }

  def props(firstHook: NextHook)(implicit eventBus: MessageEventBus, chainId: GlobalChainIdMask) : Props =
    Props(
      new StartupHooks(
      firstHook: NextHook)
    )

  def apply(props: Props)(implicit actorContext: ActorContext): ActorRef = {
    actorContext.actorOf(props, "StartupHooks")
  }

  case object HookDone extends BusEvent

  case object DoNextHook

}

class StartupHooks(firstHook: NextHook)(implicit events: MessageEventBus, chainId: GlobalChainIdMask)
                   extends Actor with ActorLogging {


  override def preStart(): Unit = {
    super.preStart()
    events.subscribe(classOf[BlockChainReady])
    events.subscribe(classOf[Synchronized])

  }

  override def postStop(): Unit = {
    super.postStop()
    events.publish(HookDone)
  }

  private var alreadyDone:Boolean = false

  import context.dispatcher


  override def receive: Receive = {

    case Synchronized(`chainId`, _, _, _) if !alreadyDone =>
      //Synchronized causes the tx processing to Begin, but it's not guarenteed to be ready
      //It's a race if the process begins off this message, hence the delay.
      context.system.scheduler.scheduleOnce(5.seconds, self, DoNextHook)
      alreadyDone = true

    case BlockChainReady(`chainId`, _) if !alreadyDone =>
      self ! DoNextHook
      alreadyDone = true

    case DoNextHook =>
      context become firstHook(self, context)
      self ! DoNextHook

  }


}
