package sss.openstar

import sss.openstar.eventbus.MessageInfo
import sss.openstar.nodebuilder.NodeLayer
import sss.openstar.schemamigration.dynamic.GlobalTableNameTag

trait Node extends NodeLayer {

  val globalTagTableName: GlobalTableNameTag = GlobalTableNameTag("1")

  override def migrateSqlSchema(): Unit = migrateSqlSchema(globalTagTableName)

  override protected val blocksPerTable: Int = 100
  override protected val tablePartitionsCount: Int = 500

  //override val heartbeatTimeoutWaitFactor: Int = 0 //TODO REMOVE BEFORE PROD

  override def decoder: Byte => Option[MessageInfo] = MessageKeys.messages.find

  def startNode(): Unit = {
    migrateSqlSchema()

    initializeLedgerOwners(requiredChainOriginatorIds, identityLedgerId).recover {
      case e =>
        log.info(s"Claiming ledgerId $identityLedgerId for $requiredChainOriginatorIds results")
        log.info(e.getMessage)
    }

    startNodeLayer()
  }
}


