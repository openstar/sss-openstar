package sss.openstar.network

import sss.ancillary.Serialize.ToBytes
import sss.openstar.UniqueNodeIdentifier
import sss.openstar.chains.Chains.GlobalChainIdMask


object TestMessageEventBusOps {

  implicit class TestMessageEventBus(val msgBus: MessageEventBus) extends AnyVal {

    def simulateNetworkMessage(serializedNetworkMessage: IncomingSerializedMessage): Unit = {
      msgBus.publish(serializedNetworkMessage)
    }


    def simulateNetworkMessage[T](from: UniqueNodeIdentifier, msgKey: Byte, networkMessage: T)
                                  (implicit ev: T => ToBytes, chainId: GlobalChainIdMask): Unit = {

      implicit val toB: T => Array[Byte] = m => ev(networkMessage).toBytes

      val serializedNetworkMessage = IncomingSerializedMessage(from,
        SerializedMessage(msgKey, networkMessage))

      simulateNetworkMessage(serializedNetworkMessage)
    }

  }
}
