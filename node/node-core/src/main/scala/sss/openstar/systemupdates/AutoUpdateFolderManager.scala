package sss.openstar.systemupdates


import sss.ancillary.Logging
import sss.openstar.util.FileUtils
import net.lingala.zip4j.ZipFile
import java.nio.file.Path
import scala.util.Try

case class AutoUpdateFolderManager(autoUpdateConfig: AutoUpdateConfig) extends Logging {

  val updateFolderAsFile = autoUpdateConfig.folderContainingAutoUpdates.toFile

  val hasUpdate: Try[Boolean] = Try {
    updateFolderAsFile.exists &&
      updateFolderAsFile.isDirectory &&
        updateFolderAsFile.list().nonEmpty
  }

  def makeChanges(): Try[Unit] = {

    for {
      _ <- backupFolder(autoUpdateConfig.libFolder)
      _ <- backupFolder(autoUpdateConfig.binFolder)
      updateZip = autoUpdateConfig.folderContainingAutoUpdates.toFile.listFiles().head
      _ = new ZipFile(updateZip).extractAll(autoUpdateConfig.rootTargetFolder.toFile.toString)
      _ = log.info(s"update exists ${updateFolderAsFile.exists()}")
    } yield ()
  }

  def removeUpdateFolder(): Try[Unit] = Try {
    FileUtils.deleteAll(autoUpdateConfig.folderContainingAutoUpdates)
  }

  def backupFolder(folder: Path): Try[Unit] = Try {
    val oldBackFolder = autoUpdateConfig.updateBackupFolderPath.resolve(FileUtils.pathLast(folder))
    val deleteOld = FileUtils.deleteAll(oldBackFolder)
    FileUtils.copyFolder(folder, autoUpdateConfig.updateBackupFolderPath)
    val remove = FileUtils.deleteContents(folder)
    log.info(s"${folder} was deleted? $remove")
    log.info(s"${oldBackFolder} was deleted? $deleteOld")
  }
}
