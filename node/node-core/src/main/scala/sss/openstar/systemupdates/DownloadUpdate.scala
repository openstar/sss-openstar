package sss.openstar.systemupdates

import akka.actor.ActorSystem
import akka.http.scaladsl.model.Uri
import akka.stream.IOResult
import akka.stream.scaladsl.{FileIO, Keep, Sink, StreamConverters}
import akka.util.ByteString
import sss.openstar.hash.Digest32
import sss.openstar.systemupdates.DigestStream.sha2Sink
import us.monoid.web.Resty

import java.nio.file.Path
import java.security.MessageDigest
import scala.concurrent.Future

trait DownloadUpdate {
  val uriSeparator = '/'
  def download(uri: Uri, pathToFolder: Path)(implicit ac: ActorSystem): Future[Digest32]
}

object DownloadUpdate extends DownloadUpdate {

  private lazy val httpClient = new Resty()

  private def makeFileSink(pathToFile: Path): Sink[ByteString, Future[IOResult]] = {
    FileIO.toPath(pathToFile)
  }

  def download(uri: Uri, pathToFile: Path)(implicit ac: ActorSystem): Future[Digest32] = {

    import ac.dispatcher

    val sink = makeFileSink(pathToFile)

    val inputStreamSource = StreamConverters.fromInputStream(
      () => httpClient.bytes(uri.toString()).stream()
    )

    val (ioFut: Future[IOResult], messageDigestFut: Future[MessageDigest]) = inputStreamSource
      .alsoToMat(sink)(Keep.right)
      .toMat(sha2Sink)(Keep.both)
      .run()

    for {
      _ <- ioFut //the IOResult is deprecated
      md <- messageDigestFut
      digest = md.digest()
    } yield Digest32(digest)

  }

}
