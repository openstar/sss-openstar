package sss.openstar.systemupdates

import akka.stream.scaladsl.Sink
import akka.util.ByteString

import java.security.MessageDigest
import java.util.Base64
import scala.concurrent.Future

object DigestStream {

  private val hashType: String = "SHA-256"

  def emptySHA256Digest: MessageDigest = MessageDigest getInstance hashType

  def digestToBase64UrlString(bytes: Array[Byte]): String =
    Base64.getUrlEncoder.encodeToString(bytes)

  private val updateDigest: (MessageDigest, ByteString) => MessageDigest =
    (messageDigest, byteBuffer) => {
      messageDigest update byteBuffer.toByteBuffer
      messageDigest
    }

  def sha2Sink: Sink[ByteString, Future[MessageDigest]] = Sink.fold(emptySHA256Digest)(updateDigest)
}
