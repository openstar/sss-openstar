package sss.openstar.systemupdates

import akka.actor.{Actor, ActorContext, ActorLogging, ActorRef, Props, Status, Timers}
import akka.pattern.pipe
import sss.ancillary.FutureOps.AwaitResult
import sss.db.Db
import sss.openstar.UniqueNodeIdentifier
import sss.openstar.account.NodeIdentity
import sss.openstar.actor.SystemPanic
import sss.openstar.chains.TxWriterActor.InternalCommit
import sss.openstar.controller.Utils
import sss.openstar.network.MessageEventBus
import sss.openstar.systemledger.SystemService.UpdateProposal
import sss.openstar.systemledger.{PreparedToUpdate, SystemServiceQuery}
import sss.openstar.systemupdates.SystemUpdateMonitorActor.CheckForUpdates
import sss.openstar.systemupdates.SystemUpdateTracker._
import sss.openstar.tools.SendTxSupport.SendTx

import scala.concurrent.Future
import scala.concurrent.duration.FiniteDuration

object SystemUpdateMonitorActor {

  case object CheckForUpdates

  def props(systemUpdateTracker: SystemUpdateTracker,
            currentBlockHeight: () => Long,
            systemLedgerOwners: () => Set[UniqueNodeIdentifier],
            systemServiceQuery: SystemServiceQuery,
            checkInterval: FiniteDuration,
           )(implicit eventBus: MessageEventBus,
             sendTx: SendTx,
             nodeIdentity: NodeIdentity,
             db: Db): Props = Props(
    new SystemUpdateMonitorActor(
      systemUpdateTracker,
      currentBlockHeight,
      systemLedgerOwners,
      systemServiceQuery,
      checkInterval: FiniteDuration)
  )

  def apply(props:Props)(implicit ac: ActorContext): ActorRef = {
    ac.actorOf(props)
  }
}

class SystemUpdateMonitorActor(systemUpdateTracker: SystemUpdateTracker,
                               currentBlockHeight: () => Long,
                               systemLedgerOwners: () => Set[UniqueNodeIdentifier],
                               systemServiceQuery: SystemServiceQuery,
                               checkInterval: FiniteDuration,
                              )(implicit eventBus: MessageEventBus,
                                                                       sendTx: SendTx,
                                                                       nodeIdentity: NodeIdentity,
                                                                       db:Db) extends Actor
  with ActorLogging
  with Timers
  with SystemPanic {

  override def preStart(): Unit = {
    super.preStart()
    timers.startSingleTimer(classOf[SystemUpdateMonitorActor], CheckForUpdates, checkInterval)
    log.info("SystemUpdateMonitorActor started...checking for updates every {}", checkInterval)
  }

  import context.{dispatcher, system}

  override def unhandled(message: Any): Unit = {
    log.info("SystemUpdateMonitorActor unhandled {}", message)
  }

  private def nodeIsSystemLedgerOwner(): Boolean =
    systemLedgerOwners().contains(nodeIdentity.id)

  private def hasAlreadySigned(
                                  uniqueProposalId: String): Future[Boolean] = {
    systemServiceQuery
      .getSignatories(uniqueProposalId).map(_.contains(nodeIdentity.id))
  }

  override def receive: Receive = {

    case CheckForUpdates =>
      systemUpdateTracker.findLast().map {  last =>
        log.info("Current last is {}", last)
        last
      } pipeTo self

    case NoUpdates =>
      (for {
        proposals <- systemServiceQuery.listProposals()
        supportedEras <- systemUpdateTracker.listSupportedEras
        supportedErasSet = supportedEras.toSet
        unsupportedProposals = proposals.dropWhile(proposal => supportedErasSet.contains(proposal.uniqueUpdateIdentifier))
      } yield unsupportedProposals.headOption) pipeTo self

    case Some(p:UpdateProposal) =>
      systemUpdateTracker.note(p) pipeTo self

    case None =>
      timers.startSingleTimer(classOf[SystemUpdateMonitorActor], CheckForUpdates, checkInterval)

    case n : NotedProposal =>
      systemUpdateTracker.download(n) pipeTo self

    case d : DownloadedProposal if nodeIsSystemLedgerOwner() =>
      hasAlreadySigned(d.uniqueProposalId).flatMap {
        case true =>
          systemUpdateTracker.setPrepared(d.uniqueProposalId)  pipeTo self

        case false =>
          val replayDefeating = currentBlockHeight()
          val tx = PreparedToUpdate(replayDefeating, d.uniqueProposalId)
          Utils.signedSystemLedgerItem(nodeIdentity, tx) flatMap { ledgerItem =>
            sendTx(ledgerItem).map(res => (res, d))
          } pipeTo self
      }

    case d : DownloadedProposal =>
      systemUpdateTracker.setPrepared(d.uniqueProposalId)  pipeTo self

    case Status.Failure(f) =>
      log.error("Future failure: {}", f)
      timers.startSingleTimer(classOf[SystemUpdateMonitorActor], CheckForUpdates, checkInterval)

    case (_: InternalCommit, d: DownloadedProposal) =>
      systemUpdateTracker.setPrepared(d.uniqueProposalId) pipeTo self

    case p: PreparedForProposal =>
      systemServiceQuery
        .isUpdatePreparationComplete(p.uniqueProposalId)
        .map {
          case true =>
            if(systemUpdateTracker.copyToAutoUpdateFolder(p.uniqueProposalId)) {
              log.info(s"System exiting to trigger auto update ${p.uniqueProposalId}")
              systemExit() // Exit so auto update is executed
            }
          case false =>
        }
      timers.startSingleTimer(classOf[SystemUpdateMonitorActor], CheckForUpdates, checkInterval)

    case c: CompletedProposal =>
      systemServiceQuery.nextProposal(c.uniqueProposalId) pipeTo self
  }
}
