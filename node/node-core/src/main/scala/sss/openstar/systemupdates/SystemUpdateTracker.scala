
package sss.openstar.systemupdates

import akka.actor.ActorSystem
import akka.http.scaladsl.model.Uri
import sss.ancillary.Logging
import sss.db.ops.DbOps.DbRunOps
import sss.db.{Db, Row, where}
import sss.openstar.hash.Digest32
import sss.openstar.schemamigration.SqlSchemaNames.ColumnNames._
import sss.openstar.schemamigration.SqlSchemaNames.TableNames.{systemEraTableName, systemUpdateTableName}
import sss.openstar.systemledger.SystemService.UpdateProposal
import sss.openstar.systemupdates.SystemUpdateTracker._
import sss.openstar.util.FileUtils

import java.nio.file.{Files, Path}
import scala.concurrent.Future
import scala.util.{Failure, Try}

object SystemUpdateTracker {

  sealed trait SystemUpdate

  case object NoUpdates extends SystemUpdate

  private val noted: Byte = 0
  private val downloaded: Byte = 1
  private val preparedFor: Byte = 2
  private val completed: Byte = 3


  def toUpdateProposalStatus(row: Row): UpdateProposalStatus = {

    val pId = row.string(updateIdentifierCol)
    val url = row.string(downloadUrlCol)
    val digest = Digest32(row.arrayByte(digestCol))

    row.byte(updatePrepStatusCol) match {
      case `noted` => NotedProposal(
        pId, url, digest
      )

      case `downloaded` => DownloadedProposal(
        pId, url, digest
      )

      case `preparedFor` => PreparedForProposal(
        pId, url, digest
      )

      case `completed` => CompletedProposal(
        pId, url, digest
      )

      case x =>
        throw new IllegalStateException(s"Invalid SystemUpdateStatus column value: $x")
    }
  }

  def toCompletedProposalStatus(row: Row): CompletedProposal = {
    val completedProposal = toUpdateProposalStatus(row)
    require(completedProposal.isInstanceOf[CompletedProposal], s"$completedProposal should be an instance of CompletedProposal")
    completedProposal.asInstanceOf[CompletedProposal]
  }

  sealed trait UpdateProposalStatus extends SystemUpdate {
    val uniqueProposalId: String
    val url: String
    val checksum: Digest32
  }

  case class NotedProposal(
                            uniqueProposalId: String,
                            url: String,
                            checksum: Digest32) extends UpdateProposalStatus

  case class DownloadedProposal(
                                  uniqueProposalId: String,
                                  url: String,
                                  checksum: Digest32) extends UpdateProposalStatus

  case class PreparedForProposal(
                            uniqueProposalId: String,
                            url: String,
                            checksum: Digest32) extends UpdateProposalStatus


  case class CompletedProposal(
                            uniqueProposalId: String,
                            url: String,
                            checksum: Digest32) extends UpdateProposalStatus
}

class SystemUpdateTracker(
                         downloadUpdate: DownloadUpdate,
                         autoUpdateFolder: Path,
                           updateDestinationFolder: String)(implicit db:Db) extends Logging {
  import db.asyncRunContext.ec

  lazy val updateDestinationPath: Path = Path.of(updateDestinationFolder)

  lazy private val table = db.table(systemUpdateTableName)
  lazy private val erasTable = db.table(systemEraTableName)

  def note(newProposal: UpdateProposal): Future[NotedProposal] =
    table.insert(Map(
      updateIdentifierCol -> newProposal.uniqueUpdateIdentifier,
      downloadUrlCol -> newProposal.url,
      digestCol -> newProposal.checksum.digest,
      updatePrepStatusCol -> noted
    )).dbRun.map(toUpdateProposalStatus(_).asInstanceOf[NotedProposal])

  def download(newProposal: NotedProposal)(implicit ac: ActorSystem): Future[DownloadedProposal] = {

    val downloadPath = updateDestinationPath.resolve(newProposal.uniqueProposalId)
    Files.createDirectories(downloadPath)
    val pathToDownloadFile = downloadPath.resolve("system-update.zip")
    val uri = Uri(newProposal.url)

    (for {
      digest <- downloadUpdate.download(uri, pathToDownloadFile)
      download <- if(digest == newProposal.checksum)
        updateStatus[DownloadedProposal](newProposal.uniqueProposalId, downloaded)
      else Future.failed(new IllegalStateException("Checksum mismatch"))
    } yield download) andThen {
      case Failure(exception) =>
        log.info("Problem with download of auto update ", exception)
        delete(newProposal.uniqueProposalId)
        deleteDownloads(newProposal.uniqueProposalId)
    }
  }

  private[systemupdates] def deleteDownloads(proposalId: String): Unit =
    FileUtils.deleteAll(updateDestinationPath.resolve(proposalId))

  private def delete(proposalId: String): Int = {
    table.delete(
      where(updateIdentifierCol -> proposalId)
    ).dbRunSyncGet
  }

  private def updateStatus[T <: UpdateProposalStatus](proposalId: String, newStatus: Byte): Future[T] = {
    (for {
      rows <- table.filter(where(updateIdentifierCol -> proposalId))
      newRow = rows.head.asMap + (updatePrepStatusCol -> newStatus)
      newRowUpdated <- table.updateRow(newRow)
    } yield toUpdateProposalStatus(newRowUpdated)
      .asInstanceOf[T])
      .dbRun
  }

  def setPrepared(newProposalId: String): Future[PreparedForProposal] = {
    updateStatus[PreparedForProposal](newProposalId, preparedFor)
  }

  private def setCompleted(newProposalId: String): Future[Unit] =
    updateStatus[CompletedProposal](newProposalId, completed).map(_ => ())

  def setLastCompleted(): Future[Unit] = {
    findLast().flatMap {
      case p: PreparedForProposal =>
        setCompleted(p.uniqueProposalId)
      case x =>
        Future.failed(new IllegalStateException(s"Expected PreparedForProposal, got $x"))
    }
  }

  def findLast(): Future[SystemUpdate] =
    (for {
      rows <- table.map(identity, where() orderAsc(idCol))
      lastStatus = rows.lastOption.map(toUpdateProposalStatus).getOrElse(NoUpdates)
    } yield lastStatus)
      .dbRun

  def findLastCompleted(): Future[Option[CompletedProposal]] =
    (for {
      rows <- table.filter(where(updatePrepStatusCol -> completed) orderAsc(idCol))
      lastCompleted = rows.lastOption.map(toCompletedProposalStatus)
    } yield lastCompleted).dbRun

  def listSupportedEras: Future[Seq[String]] =
    erasTable.map(_.string(eraIdCol), where() orderAsc(idCol)).dbRun

  def copyToAutoUpdateFolder(uniqueProposalId: String): Boolean = {
    val downloadPath = updateDestinationPath.resolve(uniqueProposalId)
    Try(FileUtils.copyFolderContents(downloadPath, autoUpdateFolder)).recover {
      case e =>
        log.warn(s"Failed to copy to $autoUpdateFolder", e)
        FileUtils.deleteContents(autoUpdateFolder)
        Failure(e)
    }.isSuccess
  }

}
