package sss.openstar.systemupdates

import sss.ancillary.FutureOps.AwaitResult
import sss.ancillary.Logging

import java.nio.file.Path
import scala.concurrent.Future
import scala.util.{Failure, Success}

trait AutoUpdateConfig {

  type MarkUpdateComplete = () => Future[Unit]

  val rootAutoUpdateFolder: Path
  val rootTargetFolder: Path // the folder where ths new application will go
  lazy val libFolder: Path = rootTargetFolder.resolve(libFolderName)
  lazy val binFolder: Path =  rootTargetFolder.resolve(binFolderName)

  val libFolderName = "lib"
  val binFolderName = "bin"

  val markUpdateComplete: MarkUpdateComplete
  lazy val folderContainingAutoUpdates: Path = rootAutoUpdateFolder.resolve("autoupdates")
  lazy val updateBackupFolderPath: Path = rootAutoUpdateFolder.resolve("backup")
}

trait AutoUpdate extends AutoUpdateConfig with Logging {

  def autoUpdateOk(): Boolean = {

    lazy val update: AutoUpdateFolderManager = AutoUpdateFolderManager(this)

    update.hasUpdate.flatMap {

      case true => for {
        _ <- update.makeChanges()
        _ <- update.removeUpdateFolder()
        _ <- markUpdateComplete().toTry()
      } yield true

      case false =>
        Success(false)

    } match {
      case Failure(ex) =>
        log.error("Autoupdate problem", ex)
        false
      case Success(value) => value
    }

  }
}
