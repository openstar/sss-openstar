package sss.openstar


import sss.ancillary.Logging
import sss.openstar.block._
import sss.openstar.block.serialize._
import sss.openstar.block.signature.BlockSignatures.BlockSignature
import sss.openstar.common.block._
import sss.openstar.common.block.serialize.{BlockChainTxIdSerializer, BlockChainTxSerializer}
import sss.openstar.eventbus._
import sss.openstar.peers._
import sss.openstar.peers.serialize.{CapabilitiesSerializer, PeerPageSerializer, SeqPeerPageResponseSerializer}

trait MessageKeys extends PublishedMessageKeys with Logging {

  val HeartbeatCode: Byte = network.heartbeatCode

  val FindLeader: Byte = 30
  val Leader: Byte = 31
  val VoteLeader: Byte = 32

  val Capabilities: Byte = 33
  val QueryCapabilities: Byte = 34
  val Synchronized: Byte = 35
  val PeerPage: Byte = 36
  val SeqPeerPageResponse: Byte = 37


  val GetPageTx: Byte = 40
  val PagedTx: Byte = 41
  val EndPageTx: Byte = 42
  val PagedCloseBlock: Byte = 43
  val CloseBlock: Byte = 44
  val Synced: Byte = 45
  val BlockSig: Byte = 46
  val BlockNewSig: Byte = 47
  val SouthBlockNewSig: Byte = 48
  val SouthCloseBlock: Byte = 49
  val RejectedPagedTx: Byte = 50

  val NotSynced: Byte = 52

  val messages: MessageInfos =
    (MessageInfoComposite[GetTxPage](NotSynced, classOf[GetTxPage], GetTxPageSerializer) :+
    MessageInfoComposite[BlockSignature](BlockNewSig, classOf[BlockSignature], BlockSignatureSerializer) :+
    MessageInfoComposite[BlockSignature](SouthBlockNewSig, classOf[BlockSignature], BlockSignatureSerializer) :+
    MessageInfoComposite[BlockSignature](BlockSig, classOf[BlockSignature], BlockSignatureSerializer) :+
    MessageInfoComposite[GetTxPage](Synced, classOf[GetTxPage], GetTxPageSerializer) :+
    MessageInfoComposite[DistributeClose](PagedCloseBlock, classOf[DistributeClose], DistributeCloseSerializer) :+
    MessageInfoComposite[DistributeClose](CloseBlock, classOf[DistributeClose],DistributeCloseSerializer) :+
    MessageInfoComposite[DistributeClose](SouthCloseBlock, classOf[DistributeClose], DistributeCloseSerializer) :+
    MessageInfoComposite[GetTxPage](EndPageTx, classOf[GetTxPage], GetTxPageSerializer) :+
    MessageInfoComposite[GetTxPage](GetPageTx, classOf[GetTxPage], GetTxPageSerializer) :+
    MessageInfoComposite[BlockChainTxId](RejectedPagedTx, classOf[BlockChainTxId], BlockChainTxIdSerializer) :+
    MessageInfoComposite[BlockChainTx](PagedTx, classOf[BlockChainTx], BlockChainTxSerializer) :+
    MessageInfoComposite[VoteLeader](VoteLeader, classOf[VoteLeader], VoteLeaderSerializer) :+
    MessageInfoComposite[FindLeader](FindLeader, classOf[FindLeader],FindLeaderSerializer) :+
    MessageInfoComposite[Leader](Leader, classOf[Leader], LeaderSerializer) :+
    MessageInfoComposite[Capabilities](Capabilities, classOf[Capabilities], CapabilitiesSerializer) :+
    MessageInfoComposite[Synchronized](Synchronized, classOf[Synchronized], SynchronizedSerializer) :+
    MessageInfoComposite[PureEvent](QueryCapabilities, classOf[PureEvent], new PureEventSerializer(QueryCapabilities)) :+
    MessageInfoComposite[PeerPage](PeerPage, classOf[PeerPage], PeerPageSerializer) :+
    MessageInfoComposite[SeqPeerPageResponse](SeqPeerPageResponse, classOf[SeqPeerPageResponse], SeqPeerPageResponseSerializer)) ++ publishedMsgs

}

object MessageKeys extends MessageKeys