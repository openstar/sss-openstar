package sss.openstar.chains


import akka.actor.{Actor, ActorContext, ActorLogging, ActorRef, Cancellable, Props, Stash, Terminated}
import sss.ancillary.ByteArrayEncodedStrOps._
import sss.ancillary.Logging
import sss.db.Db
import sss.db.ops.DbOps.DbRunOps
import sss.openstar._
import sss.openstar.account.NodeIdentity
import sss.openstar.actor.{Stasher, SystemPanic}
import sss.openstar.block.BlockChainLedger.NewBlockId
import sss.openstar.block.signature.BlockSignaturesFactory
import sss.openstar.block.{BlockChain, BlockChainLedger, BlockChainSignaturesAccessor, BlockClosedEvent, BlockFactory}
import sss.openstar.chains.BlockCloseDistributorActor.BlockCloseDistributed
import sss.openstar.chains.Chains.{Checkpointer, GlobalChainIdMask}
import sss.openstar.chains.QuorumFollowersSyncedMonitor.{BlockChainReady, QuorumSync, SyncedQuorum}
import sss.openstar.chains.QuorumMonitor.QuorumLost
import sss.openstar.chains.TxDistributorActor._
import sss.openstar.chains.TxWriterActor._
import sss.openstar.common.block.{TxMessage, _}
import sss.openstar.controller.Send
import sss.openstar.ledger.LedgerId.NoLedgerId
import sss.openstar.ledger.LedgerOps.LedgerExceptionOps
import sss.openstar.ledger.{LedgerItem, _}
import sss.openstar.network.MessageEventBus.IncomingMessage
import sss.openstar.network._

import scala.collection.SortedSet
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.{Failure, Success, Try}

/**
  * Created by alan on 3/18/16.
  */
object TxWriterActor {

  final private case class CloseBlock(height: Long)

  final private case object BlockCloseTrigger

  final private case class PostJournalConfirm(bcTx: BlockChainTx)

  sealed trait InternalTxUpdate extends BusEvent {
    val chainId: GlobalChainIdMask
  }

  sealed trait InternalTxResult extends BusEvent {
    val chainId: GlobalChainIdMask
  }

  case class InternalLedgerItem(chainId: GlobalChainIdMask,
                                le: SeqLedgerItem,
                                responseListener: Option[ActorRef]) extends BusEvent {
    override def toString: String = {
      s"InternalLedgerItem(ChainId:$chainId, $le, listener:${responseListener.map(_.path.name)})"
    }
  }

  case class InternalCommit(chainId: GlobalChainIdMask, blTxId: BlockChainTxId)
    extends InternalTxResult

  case class InternalAck(chainId: GlobalChainIdMask, blTxId: BlockChainTxId)
    extends InternalTxUpdate

  case class InternalTempNack(chainId: GlobalChainIdMask, txMsg: TxMessage)
    extends InternalTxResult

  case class InternalNack(chainId: GlobalChainIdMask, txMsg: TxMessage)
    extends InternalTxResult


  def apply(checkedProps: CheckedProps)(implicit actorSystem: ActorContext): Unit = {
    actorSystem.actorOf(checkedProps.value.withDispatcher("blocking-dispatcher"), checkedProps.name)
  }

  sealed trait Response {
    def tempNack(txMsg: TxMessage): Unit

    def nack(txMsg: TxMessage): Unit

    def nack(chainId: Byte, ledgerId: LedgerId, errorCode: LedgerErrorCode, msg: String, txId: TxId): Unit

    def ack(bTx: BlockChainTxId): Unit

    def confirm(bTx: BlockChainTxId): Unit
  }

  case class InternalResponse(listener: Option[ActorRef])(
    implicit chainId: GlobalChainIdMask)
    extends Response with Logging {

    override def tempNack(txMsg: TxMessage): Unit =
      listener match {
        case None => log.warn(s"Internal tx has been temp nacked -> ${txMsg.msg}")
        case Some(listener) => listener ! InternalTempNack(chainId, txMsg)
      }

    override def nack(txMsg: TxMessage): Unit =
      listener match {
        case None => log.warn(s"Internal tx has been nacked -> ${txMsg.msg}")
        case Some(listener) => listener ! InternalNack(chainId, txMsg)
      }

    override def nack(chainId: GlobalChainIdMask, ledgerId: LedgerId, code: LedgerErrorCode, msg: String, txId: TxId): Unit =
      nack(TxMessage(chainId, ledgerId, code, txId, msg))

    override def ack(bTx: BlockChainTxId): Unit =
      listener foreach (_ ! InternalAck(chainId, bTx))

    override def confirm(bTx: BlockChainTxId): Unit =
      listener foreach (_ ! InternalCommit(chainId, bTx))
  }

  case class NetResponse(nodeId: UniqueNodeIdentifier, send: Send)(
    implicit chainId: GlobalChainIdMask)
    extends Response {

    override def tempNack(txMsg: TxMessage): Unit =
      send(MessageKeys.TempNack,
        txMsg,
        nodeId)

    override def nack(txMsg: TxMessage): Unit =
      send(MessageKeys.SignedTxNack,
        txMsg,
        nodeId)

    override def nack(chainId: Byte, ledgerId: LedgerId, code: LedgerErrorCode, msg: String, txId: TxId): Unit =
      nack(TxMessage(chainId, ledgerId, code, txId, msg))


    override def ack(bTx: BlockChainTxId): Unit = {
      send(MessageKeys.SignedTxAck, bTx, nodeId)
    }

    override def confirm(bTx: BlockChainTxId): Unit =
      send(MessageKeys.SignedTxConfirm, bTx, nodeId)
  }


  case class CheckedProps(value: Props, name: String)

  def props(blockFactory: BlockFactory,
            blockChainSettings: BlockChainSettings,
            thisNodeId: UniqueNodeIdentifier,
            bc: BlockChain with BlockChainSignaturesAccessor,
            nodeIdentity: NodeIdentity,
            checkpoint: Checkpointer
           )(implicit db: Db,
                                        chainId: GlobalChainIdMask,
                                        send: Send,
                                        connect: NetConnect,
                                        messageEventBus: MessageEventBus,
                                        ledgers: Ledgers
           ): CheckedProps = {
    CheckedProps(
      Props(new TxWriterActor(
        blockFactory,
        blockChainSettings,
        thisNodeId,
        bc,
        nodeIdentity,
        checkpoint)
      ),
      s"TxWriterActor_$chainId"
    )
  }

}

private class TxWriterActor(blockFactory: BlockFactory,
                            blockChainSettings: BlockChainSettings,
                            thisNodeId: UniqueNodeIdentifier,
                            bc: BlockChain with BlockChainSignaturesAccessor,
                            nodeIdentity: NodeIdentity,
                            checkpoint: Checkpointer
                           )(implicit val db: Db,
                             chainId: GlobalChainIdMask,
                             send: Send,
                             netConnect: NetConnect,
                             messageEventBus: MessageEventBus,
                             ledgers: Ledgers)
  extends Actor
    with SystemPanic
    with Stasher
    with Stash
    with ActorLogging {

  log.debug("TxWriter actor has started...")

  override def preStart(): Unit = {
    super.preStart()
    messageEventBus.subscribe(classOf[BlockClosedEvent])
    messageEventBus.subscribe(classOf[SyncedQuorum])
    messageEventBus.subscribe(classOf[QuorumSync])
    messageEventBus.subscribe(classOf[QuorumLost])
  }

  import context.dispatcher

  private var lastHeightClosed: Long = 0
  private var lastValidatedIndex: Long = 0

  private var responseMap = Map[BlockId, Response]()

  private var blockTxsToDistribute: Map[Long, Set[ActorRef]] =
    Map().withDefaultValue(Set.empty)

  private var blockCloseDistribute: Map[Long, ActorRef] = Map.empty

  private var blockCloseTimer: Option[Cancellable] = None

  private var blocksToClose: SortedSet[Long] = SortedSet[Long]()


  private def respond(blockId: BlockId, f: Response => Unit): Unit = {
    f(responseMap(blockId))
    responseMap -= blockId
  }
  private def setTimer(): Unit = {

    import context.dispatcher

    log.debug(s"Timer setup to close block after ${blockChainSettings.maxBlockOpenSecs}")
    blockCloseTimer = Option(
      context.
        system.
        scheduler.
        scheduleOnce(blockChainSettings.maxBlockOpenSecs seconds,
          self,
          BlockCloseTrigger
        )
    )
  }

  private def waitForUp: Receive = reset orElse {

    case sq@SyncedQuorum(`chainId`, _, _) =>
      //start listening for
      messageEventBus.subscribe(MessageKeys.SignedTx)
      messageEventBus.subscribe(MessageKeys.SeqSignedTx)
      messageEventBus.subscribe(classOf[InternalLedgerItem])

      setTimer()

      stashWhileDoing((for {
        header <- bc.lastBlockHeader
        lastHeaderIsCommitted = header.committed
        l <- BlockChainLedger(blockFactory, header.height + 1)
        currentBlock <- blockFactory.getBlock(l.blockHeight)
        lastCommitted <- currentBlock.getLastCommitted
        lastRecorded <- currentBlock.getLastRecorded
        uncommitted <- currentBlock.getUnCommitted
      } yield (l, lastCommitted, lastRecorded , lastHeaderIsCommitted, uncommitted)).dbRun, {
        case StasherFutureSuccess((l: BlockChainLedger, lastCommitted: Long, lastRecorded , lastHeaderIsCommitted: Boolean, uncommitted: Seq[BlockTx] @unchecked)) =>

          if(log.isInfoEnabled) {
            log.info(s"This node, $thisNodeId, is leader and accepting transactions (at height ${l.blockHeight}, committed index ${lastCommitted}, recordedIndex ${lastRecorded}, lastHeaderIsCommitted ${lastHeaderIsCommitted}) ...")
          }

          pop()

          if(lastHeaderIsCommitted) {
            lastHeightClosed = l.blockHeight - 1
            lastValidatedIndex = lastCommitted

            messageEventBus.publish(BlockChainReady(chainId, thisNodeId))

            uncommitted foreach (leftover => {

              log.info("Found uncommitted tx, redistributing... BlockId(h:{},index:{}) ",
                l.blockHeight,
                leftover.index
              )

              self ! PostJournalConfirm(BlockChainTx(l.blockHeight, leftover))

            })
            context become acceptTxs(l, sq)

          } else {
            val notClosedHeight = l.blockHeight - 1
            val lastGoodTxIndex = (for {
              currentBlock <- blockFactory.getBlock(notClosedHeight - 1)
              lastCommitted <- currentBlock.getLastCommitted
            } yield lastCommitted).dbRunSyncGet

            lastHeightClosed =  notClosedHeight - 1
            lastValidatedIndex = lastGoodTxIndex

            log.
              info(
                "Found uncommitted block header: {}, closing...(last closed block and index is {}, {})",
                notClosedHeight,
                lastHeightClosed,
                lastValidatedIndex
              )


            self ! CloseBlock(notClosedHeight)
            context become waitForBlockClose(l, sq)
          }


        case StasherFutureFailure(e) =>
          systemPanic(e)
      })

    case x => log.debug("Ignoring {}", x)

  }

  private def reset: Receive = {

    case QuorumLost(`chainId`) =>
      log.info(s"Quorum lost {} is not accepting transactions :( ", thisNodeId)
      messageEventBus.unsubscribe(MessageKeys.SignedTx)
      messageEventBus.unsubscribe(MessageKeys.SeqSignedTx)
      messageEventBus.unsubscribe(classOf[InternalLedgerItem])

      context become waitForUp

    case Terminated(ref) =>
    //blockTxsToDistribute = blockTxsToDistribute map { case (k, v) => (k, v.filterNot(_ == ref)) }
    //blockCloseDistribute = blockCloseDistribute.filterNot { case (k, v) => v == ref}

  }


  override def receive: Receive = waitForUp

  private def getOrCreateBlockCloseDistributingActor(
                                         height: Long
                                       ): ActorRef = {

    blockCloseDistribute.getOrElse(height, {

      val ref = BlockCloseDistributorActor(
        BlockCloseDistributorActor.props(
          height,
          bc,
          nodeIdentity,
          checkpoint
        )
      )

      blockCloseDistribute += height -> ref
      ref

    })

  }


  def createTxDistributingActor(bTx: BlockChainTx): ActorRef =
    TxDistributorActor(
      TxDistributorActor.props(bTx)
    )

  private def checkConditionsForBlockClose(): Unit = {
    blocksToClose
      .headOption
      .foreach(checkConditionsForBlockClose)
  }

  private def checkConditionsForBlockClose(heightOfBlockToClose: Long): Unit = {

    log.info("Checking if we can close block {}, left to close {}", heightOfBlockToClose, blocksToClose)

    if (blockTxsToDistribute.nonEmpty) log.info("Block Txs to distribute {}", blockTxsToDistribute)

    if (blockTxsToDistribute(heightOfBlockToClose).isEmpty) {
      blockTxsToDistribute -= heightOfBlockToClose
      log.info(s"checkConditionsForBlockClose SUCCESS for $heightOfBlockToClose")
      blocksToClose = blocksToClose.tail
      self ! CloseBlock(heightOfBlockToClose)

    } else {
      log.info(s"checkConditionsForBlockClose failed for $heightOfBlockToClose")
    }
  }

  private def waitForBlockClose(blockLedger: BlockChainLedger, sq: SyncedQuorum): Receive = reset orElse feedBackFromChildren orElse {

    case InternalLedgerItem(`chainId`, le, responseListener) =>
      val msg = TxMessage(chainId, NoLedgerId, "", le.txId, s"Waiting for block ${blockLedger.blockHeight} to close, retry")
      InternalResponse(responseListener).tempNack(msg)

    case IncomingMessage(`chainId`,
                        MessageKeys.SignedTx,
                        clientNodeId,
                        le: LedgerItem) =>

      val msg = TxMessage(chainId, le.ledgerId, "", le.txId, s"Waiting for block ${blockLedger.blockHeight} to close, retry")
      NetResponse(clientNodeId, send).tempNack(msg)

    case IncomingMessage(`chainId`,
    MessageKeys.SeqSignedTx,
    clientNodeId,
    _) =>
      val msg = TxMessage(chainId, LedgerId(0), "", Array.emptyByteArray, s"Waiting for block ${blockLedger.blockHeight} to close, retry")
      NetResponse(clientNodeId, send).tempNack(msg)

    case CloseBlock(height) =>
      val ref = getOrCreateBlockCloseDistributingActor(height)
      ref ! sq

    case BlockCloseDistributed(height) if height == blockLedger.blockHeight =>

      val newLedger = (for {
        committed <- bc.commitBlockHeaderFTx(height)
        _ = log.info("BlockHeader {} committed", committed)
        newLedger <- BlockChainLedger(blockFactory, height + 1)
      } yield newLedger).dbRunSyncGet


      blockCloseDistribute = blockCloseDistribute - height
      context become acceptTxs(newLedger, sq)
      setTimer()

      lastValidatedIndex = 0
      lastHeightClosed = height

      messageEventBus publish NewBlockId(chainId, BlockId(newLedger.blockHeight, 0))

      val coinbaseTxs = ledgers.coinbase(newLedger.blockHeight).toSeq
      if(coinbaseTxs.nonEmpty) {
        log.debug(s"There are ${coinbaseTxs.size} txs")
        val ledgerItems = SeqLedgerItem(coinbaseTxs)

        validateAndJournalTx(blockChainSettings.maxTxPerBlock,
          newLedger,
          ledgerItems,
          createTxDistributingActor,
          InternalResponse(None),
          sq
        )
      }

    case BlockCloseDistributed(height) =>
      log.debug("Got a BlockCloseDistributed for height {} but height is now {}", height, blockLedger.blockHeight)

    case updatedQuorum: SyncedQuorum =>
      blockTxsToDistribute.values.flatten foreach (_ ! updatedQuorum)
      blockCloseDistribute.values foreach (_ ! updatedQuorum)
      context become waitForBlockClose(blockLedger, updatedQuorum)

    case qs@QuorumSync(newNode, height, index) =>
      val currentHeight = lastHeightClosed + 1
      val currentIndex = lastValidatedIndex
      if (currentHeight == height && index == currentIndex) {
        val newSyncs = qs +: sq.syncs.filterNot(_.nodeId == newNode)
        val newSq = sq.copy(syncs = newSyncs)
        blockTxsToDistribute.values.flatten foreach (_ ! newSq)
        blockCloseDistribute.values foreach (_ ! newSq)
        context become waitForBlockClose(blockLedger, newSq)
      } else {
        log.info(s"$newNode is at h $height index $index, Quorum writer is at $currentHeight, $currentIndex - disconnecting")
        netConnect.disconnect(newNode)
      }

  }


  private def acceptTxs(blockLedger: BlockChainLedger, sq: SyncedQuorum): Receive = reset orElse feedBackFromChildren orElse {

    case updatedQuorum: SyncedQuorum =>
      blockTxsToDistribute.values.flatten foreach (_ ! updatedQuorum)
      blockCloseDistribute.values foreach (_ ! updatedQuorum)
      context become acceptTxs(blockLedger, updatedQuorum)

    case qs@QuorumSync(newNode, height, index) =>
      val currentHeight = lastHeightClosed + 1
      val currentIndex = lastValidatedIndex
      if (currentHeight == height && index == currentIndex) {
        val newSyncs = qs +: sq.syncs.filterNot(_.nodeId == newNode)
        val newSq = sq.copy(syncs = newSyncs)
        blockTxsToDistribute.values.flatten foreach (_ ! newSq)
        blockCloseDistribute.values foreach (_ ! newSq)
        context become acceptTxs(blockLedger, newSq)
      } else {
        log.debug(s"$newNode is at h $height index $index, Quorum writer is at $currentHeight, $currentIndex - disconnecting")
        netConnect.disconnect(newNode)
      }


    case PostJournalConfirm(bcTx) =>

      postJournalConfirm(blockChainSettings.maxTxPerBlock,
        createTxDistributingActor,
        InternalResponse(None),
        sq,
        bcTx)

    case BlockCloseTrigger =>
      blockCloseTimer foreach (_.cancel())
      blockCloseTimer = None
      blocksToClose = blocksToClose.union(SortedSet(blockLedger.blockHeight))

      context become waitForBlockClose(blockLedger, sq)
      checkConditionsForBlockClose()

      log.info(s"Last height closed $lastHeightClosed Current block height ${blockLedger.blockHeight}")


    case IncomingMessage(`chainId`,
    MessageKeys.SeqSignedTx,
    clientNodeId,
    stxs: SeqLedgerItem) if(sq.members.size < 2 || sq.members.contains(clientNodeId)) =>

      Try {
        validateAndJournalTx(blockChainSettings.maxTxPerBlock,
          blockLedger,
          stxs,
          createTxDistributingActor,
          NetResponse(clientNodeId, send),
          sq
        )
      } match {
        case Failure(exception) => log.error(exception.getMessage)
        case Success(_) => log.info("validateAndJournalTx OK")
      }

      //accept txs only from quorum members to prevent edge case
      //where a non quorum syncing node gets off a tx before it gets disconnected
    case IncomingMessage(`chainId`,
    MessageKeys.SignedTx,
    clientNodeId,
    stx: LedgerItem) if(sq.members.size < 2 || sq.members.contains(clientNodeId)) =>

      log.info("ASDFSDFSDFSDFSDFSDFSDF ?????????????????")
      validateAndJournalTx(blockChainSettings.maxTxPerBlock,
        blockLedger,
        SeqLedgerItem(stx),
        createTxDistributingActor,
        NetResponse(clientNodeId, send),
        sq
      )


    case InternalLedgerItem(`chainId`, signedTx, responseListener) =>
      validateAndJournalTx(blockChainSettings.maxTxPerBlock,
        blockLedger,
        signedTx,
        createTxDistributingActor,
        InternalResponse(responseListener),
        sq
      )

  }

  def updateBlockTxsToDistribute(height: Long, refOfTxDistributor: ActorRef) = {

    val others = blockTxsToDistribute(height)

    if (others == Set(refOfTxDistributor)) {

      // A block has all it's TXs distributed to the quorum, it may be waiting on this to close.
      blockTxsToDistribute -= height
      checkConditionsForBlockClose()

    } else {
      blockTxsToDistribute += (height -> (others filterNot (_ == refOfTxDistributor)))
    }
  }

  def feedBackFromChildren: Receive = {

    case BlockClosedEvent(`chainId`, BlockId(heightClosed, index)) =>
      log.info("{} (index : {}) now closed, previous was {}", heightClosed, index, lastHeightClosed)

    case nack: TxTimeout => // couldn't get replicate confirms within 'reasonable' time
      systemPanic(s"Couldn't replicate confirms - timeout $nack")

    case nack: TxNackReplicated => //the quorum has rejected a tx the leader has accepted

      val refOfTxDistributor = sender()
      val height = nack.bTx.height
      val plan = for {
        blockLedger <- BlockChainLedger(blockFactory, height)
        rejected <- blockLedger.rejected(nack.bTx)
      } yield rejected

      stashWhileDoing(plan.dbRun, {

        case StasherFutureSuccess(_) =>

          val blockId = BlockId(height, nack.bTx.blockTxId.index)
          respond(blockId,
            _.nack(chainId, NoLedgerId, "",
            "Tx couldn't replicate (possibly time out)", nack.bTx.blockTxId.txId)
          )

          refOfTxDistributor ! TxRejected(nack.bTx, nack.rejectors)
          updateBlockTxsToDistribute(height, refOfTxDistributor)
          messageEventBus.publish(NewBlockId(chainId, blockId))
          pop()

        case StasherFutureFailure(e) =>
          log.error("Could not reject tx ")
          systemPanic(e)
      })



    case TxReplicated(bTx) =>

      val refOfTxDistributor = sender()

      def rejectAndNack(e: Throwable): Unit = {
        log.error(s"Could not apply tx $bTx after confirm {}", e)
        refOfTxDistributor ! TxRejected(bTx.toId, Set.empty)
        updateBlockTxsToDistribute(bTx.height, refOfTxDistributor)
        val bId = BlockId(bTx.height, bTx.blockTx.index)
        respond(bId, _.nack(e.toTxMessage(chainId, bTx.toId.blockTxId.txId)))
      }

      import context.dispatcher

      stashWhileDoing(BlockChainLedger(blockFactory, bTx.height).dbRun, {

        case StasherFutureSuccess(blockLedger: BlockChainLedger) =>
          stashWhileDoing(blockLedger.commitFTx(bTx.blockTx)
            .dbRun, {

            case StasherFutureSuccess(result: LedgerResult @unchecked) =>
              self ! (blockLedger, result)

            case (blockChainLedger: BlockChainLedger, Right(txApplied: TxApplied)) =>

              refOfTxDistributor ! TxCommitted(bTx.toId)
              updateBlockTxsToDistribute(blockChainLedger.blockHeight, refOfTxDistributor)
              val bId = BlockId(bTx.height, bTx.blockTx.index)
              log.info(s"Confirm $bId ${bTx.blockTx.ledgerItems.txId.toBase64Str}")
              respond(bId, _.confirm(bTx.toId))
              txApplied.events.foreach(messageEventBus.publish)
              messageEventBus.publish(NewBlockId(chainId, bId))
              pop()

            case (_, Left(applyError: LedgerTxApplyError)) =>
              stashWhileDoing(blockLedger.rejected(bTx.toId).dbRun, {
                case StasherFutureSuccess(_) =>
                  log.info("Tx Not Applied successfully at leader")
                  rejectAndNack(applyError)
                  pop()

                case StasherFutureFailure(failedToReject) =>
                  log.error("Blockledger failed to reject {}, {}", bTx, failedToReject)
                  rejectAndNack(applyError)
                  pop()

              })

            case StasherFutureFailure(e) =>

              stashWhileDoing(blockLedger.rejected(bTx.toId).dbRun, {
                case StasherFutureSuccess(_) =>
                  rejectAndNack(e)
                  pop()

                case StasherFutureFailure(failedToReject) =>
                  log.error("Blockledger failed to reject {}, {}", bTx, failedToReject)
                  rejectAndNack(e)
                  pop()

              })


          })

        case StasherFutureFailure(e) =>
            systemPanic(e)

      })

  }

  override def postStop(): Unit = log.warning(s"Tx Writer is down. ($self)")


  private def validateAndJournalTx(maxTxPerBlock: Int,
                                   blockLedger: BlockChainLedger,
                                   signedTxs: SeqLedgerItem,
                                   createConfirmingActor: BlockChainTx => ActorRef,
                                   responder: Response,
                                   sq: SyncedQuorum): Unit = {

    log.info(s"validateAndJournalTx ${signedTxs.txIdHexStr}")
    val txsInFlightForBlock = blockTxsToDistribute.get(blockLedger.blockHeight) map (_.size) getOrElse(0)

    if (txsInFlightForBlock < blockChainSettings.maxThroughput) {

      stashWhileDoing(blockLedger.prepare(signedTxs), {
          case StasherFutureSuccess(blkChainTx: BlockChainTx) =>
            Try {
              lastValidatedIndex = blkChainTx.blockTx.index

              log.info(s"Last index $lastValidatedIndex")
              //log.info(s"Valid ${signedTx.txIdHexStr} Last Block is ${bcTx.blockTx}")
              //assert(t.blockTx == bcTx.blockTx, "Journal-ed blockTx did not equal validated blockTx")
              //assert(t.height == blockLedger.blockHeight, "Sanity check for block heights failed")
              responder.ack(BlockChainTxId(blockLedger.blockHeight, BlockTxId(signedTxs.txId, lastValidatedIndex)))

              postJournalConfirm(
                maxTxPerBlock,
                createConfirmingActor,
                responder,
                sq: SyncedQuorum,
                blkChainTx)
            } recover {
              case e => log.warning(e.getMessage)
            }
            pop()

          case StasherFutureFailure(e) =>
            Try {
              log.info(s"Failed to prepare ledger tx! ${signedTxs.txIdHexStr} ${e.getMessage}")
              responder.nack(e.toTxMessage(chainId, signedTxs.txId))
            } recover {
              case e => log.warning(e.getMessage)
            }
            pop()
        }
      )

    } else {
        log.debug(s"Max throughput ${blockChainSettings.maxThroughput} reached!")
        responder.tempNack(TxMessage(0, NoLedgerId, "", signedTxs.txId, s"Max throughput  (${blockChainSettings.maxThroughput}) reached, retry... "))
    }
  }

  override def unhandled(message: Any): Unit = {
    super.unhandled(message)
    log.warning(s"Unhandled! $message")
  }

  private def postJournalConfirm(maxTxPerBlock: Long,
                                 createConfirmingActor: BlockChainTx => ActorRef,
                                 responder: Response,
                                 sq: SyncedQuorum,
                                 bcTx: BlockChainTx) = {

    val confirmingRefs = blockTxsToDistribute(bcTx.height)

    val confirmingActor = createConfirmingActor(bcTx)
    context watch confirmingActor
    //TODO there is no guarantee that the bcTx will definitely be sent
    // by the confirming actor before the next transaction comes in,
    // potential race condition under load.
    confirmingActor ! sq
    val height = bcTx.height
    val index = bcTx.blockTx.index

    blockTxsToDistribute += height -> (confirmingRefs + confirmingActor)
    responseMap += BlockId(height, index) -> responder
    val responseMapSize = responseMap.size
    if(responseMapSize % 100 == 0) {
      log.info("Response map size now {}", responseMapSize)
    }

    if (index >= maxTxPerBlock) {
      self ! BlockCloseTrigger
    }
  }
}
