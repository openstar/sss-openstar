package sss.openstar.chains

import akka.actor.{ActorContext, ActorRef}
import sss.ancillary.FutureOps.AwaitResult
import sss.db.Db
import sss.db.ops.DbOps.DbRunOps
import sss.openstar._
import sss.openstar.block._
import sss.openstar.chains.Chains.GlobalChainIdMask
import sss.openstar.chains.LeaderElectionActor._
import sss.openstar.chains.QuorumMonitor.QuorumLost
import sss.openstar.common.chains.Quorum
import sss.openstar.controller.Send
import sss.openstar.network.MessageEventBus.IncomingMessage
import sss.openstar.network.{MessageEventBus, _}
import sss.openstar.stateactor.StateActor.Disconnect
import sss.openstar.stateactor._

import scala.collection.SortedSet
import scala.concurrent.duration._


/**
  * Created by alan on 4/1/16.
  */

class LeaderElection

object LeaderElectionActor {

  type LeaderElectionState = State[LeaderElection]

  type MakeFindLeader = () => FindLeader



  sealed trait LeaderFound extends BusEvent {
    val chainId: GlobalChainIdMask
    val leader: UniqueNodeIdentifier
  }

  case class LocalLeader(chainId: GlobalChainIdMask,
                         leader: UniqueNodeIdentifier,
                         height: Long,
                         index: Long,
                         followers: Seq[VoteLeader],
                         minConfirms: Int
                        ) extends LeaderFound

  case class RemoteLeader(chainId: GlobalChainIdMask,
                          leader: String,
                          members: Set[UniqueNodeIdentifier]) extends LeaderFound

  case class LeaderLost(chainId: GlobalChainIdMask,
                        leader: String) extends BusEvent


  private def createFindLeaderMsg(thisNodeId: UniqueNodeIdentifier,
                                  bc: BlockChain with BlockChainSignaturesAccessor)(implicit db: Db): FindLeader = {

    // Note that if a node is synchronizing, dirty reads mean these values are not consistent.
    // Not all nodes can be synchronising at the same time meaning at least one node will get fully
    // stable reads here.
    val recordedId = bc.getLatestRecordedBlockId().dbRunSyncGet
    val committedId = bc.getLatestCommittedBlockId()


    val sigIndex = bc.quorumSigs(recordedId.blockHeight)
      .indexOfBlockSignature(thisNodeId)
      .await(500.millis)
      .getOrElse(Int.MaxValue)

    FindLeader(recordedId.blockHeight,
      recordedId.txIndex,
      committedId.txIndex,
      sigIndex,
      thisNodeId)
  }

  private def apply(thisNodeId: UniqueNodeIdentifier,
            createFindLeader: MakeFindLeader,
            getLatestCommittedBlockId: GetLatestCommittedBlockId)
           (implicit chainId: GlobalChainIdMask,
            messageBus: MessageEventBus,
            send: Send,
            netConnect: NetConnect,
            actorSystem: ActorContext): ActorRef = {

    implicit val disconnect: Disconnect = netConnect.disconnect

    val init = WaitForQuorum()(chainId, createFindLeader, getLatestCommittedBlockId, thisNodeId)

    StateActor(
      init,
      s"LeaderElectionActor_$chainId",
      Seq(classOf[Quorum], classOf[QuorumLost], classOf[ConnectionLost]),
      Seq(MessageKeys.FindLeader, MessageKeys.Leader, MessageKeys.VoteLeader)
    )
  }


  def apply(thisNodeId: UniqueNodeIdentifier,
            bc: BlockChain with BlockChainSignaturesAccessor)
           (implicit db: Db,
            chainId: GlobalChainIdMask,
            send: Send,
            netConnect: NetConnect,
            messageBus: MessageEventBus,
            actorSystem: ActorContext): ActorRef = {

    def createFindLeader: MakeFindLeader = () => createFindLeaderMsg(thisNodeId, bc)


    apply(thisNodeId, createFindLeader, () => bc.getLatestCommittedBlockId())
  }
}

case class WaitForQuorum()(implicit chainId: GlobalChainIdMask,
                           createFindLeader: MakeFindLeader,
                           getLatestCommittedBlockId: GetLatestCommittedBlockId,
                           thisNodeId: UniqueNodeIdentifier) extends LeaderElectionState {


  override def handle: Handler = {

    case Quorum(`chainId`, connected, minConfirms) =>

      if (connected.isEmpty) {
        //Special case where there is a quorum and it's just us.
        val latestBlockId = getLatestCommittedBlockId()
        val localLeaderAction = ToPublish(LocalLeader(chainId,
          thisNodeId,
          latestBlockId.blockHeight,
          latestBlockId.txIndex,
          Seq.empty,
          minConfirms
        ))
        log.debug(s"Connected q members is empty, min confirms is  ${minConfirms}")
        (LeaderKnown(
          connected,
          thisNodeId,
          minConfirms,
        ),
          localLeaderAction)

      } else {
        val findMsg = createFindLeader()
        log.debug(s"Sending FindLeader to network ${findMsg}")

        (FindingLeader(
          connected,
          Set.empty,
          minConfirms,
          SortedSet(findMsg)
        ),
          ToSend(MessageKeys.FindLeader, findMsg, connected)

        )

      }
  }
}

case class LeaderKnown(
                        connectedMembers: Set[UniqueNodeIdentifier],
                        leader: UniqueNodeIdentifier,
                        minConfirms: Int,
                      )(implicit chainId: GlobalChainIdMask,
                        createFindLeader: MakeFindLeader,
                        getLatestCommittedBlockId: GetLatestCommittedBlockId,
                        thisNodeId: UniqueNodeIdentifier) extends LeaderElectionState {

  override def handle: Handler = {

    case ConnectionLost(`leader`) =>
      val connectedNow = connectedMembers - leader
      log.info(s"Lost conn with leader $leader connectedNow $connectedNow")
      val actions = ToSend(MessageKeys.FindLeader, createFindLeader(), connectedNow) +
        ToPublish(LeaderLost(chainId, leader))
      (FindingLeader(connectedNow, Set.empty, minConfirms, SortedSet(createFindLeader())), actions)

    case IncomingMessage(`chainId`, MessageKeys.FindLeader, from, _) =>

      log.debug(s"Find leader from {} and leader already known {}", from, leader)
      if (leader == thisNodeId && connectedMembers.contains(from))
        onlyAction(ToSend(MessageKeys.Leader, Leader(leader), Set(from)))
      else
        NoChange

    case IncomingMessage(`chainId`, MessageKeys.VoteLeader, from, vote: VoteLeader) =>
      log.debug(
        s"Got a surplus vote for this node from $from (${vote.nodeId}), leader is $leader")
      NoChange

    case IncomingMessage(`chainId`, MessageKeys.Leader, from, saying) =>
      log.whenDebugEnabled(
        if(saying != leader) {
          log.debug(
            s"Got an unneeded leader indicator from $from, saying $saying but leader is $leader")
        }
      )
      NoChange

    case Quorum(`chainId`, connected, minConfirms) =>
      (copy(connectedMembers = connected, minConfirms = minConfirms), NoAction)

    case QuorumLost(`chainId`) =>
      (WaitForQuorum(),
        ToPublish(LeaderLost(chainId, leader)))

  }
}

case class FindingLeader(
                          connectedMembers: Set[UniqueNodeIdentifier],
                          leaderVotes: Set[VoteLeader],
                          minConfirms: Int,
                          var findLeaders: SortedSet[FindLeader]

                        )(implicit chainId: GlobalChainIdMask,

                          createFindLeader: MakeFindLeader,
                          getLatestCommittedBlockId: GetLatestCommittedBlockId,
                          thisNodeId: UniqueNodeIdentifier) extends LeaderElectionState {

  private def checkAmLeader(): (LeaderElectionState, Action) = {

    if (leaderVotes.size == connectedMembers.size && leaderVotes.forall(v => connectedMembers.contains(v.nodeId))) {
      val latestBlockId = getLatestCommittedBlockId()
      // I am the leader.
      val actions = ToPublish(LocalLeader(chainId,
        thisNodeId,
        latestBlockId.blockHeight,
        latestBlockId.txIndex,
        leaderVotes.toSeq,
        minConfirms
      )) +
        ToSend(MessageKeys.Leader, Leader(thisNodeId), connectedMembers)

      (LeaderKnown(
        connectedMembers,
        thisNodeId,
        minConfirms,
      ),
        actions)

    } else {
      log.debug(s"LeaderVotes $leaderVotes")
      log.debug(s"Connected   $connectedMembers")
      log.debug(s"minConfirm  $minConfirms")
      NoChange
    }
  }

  override def handle: Handler = {

    case QuorumLost(`chainId`) =>
      (WaitForQuorum(), NoAction)

    case IncomingMessage(`chainId`, MessageKeys.FindLeader, qMember, his: FindLeader) =>

      findLeaders = findLeaders.union(SortedSet(his))
      val actions = if(findLeaders.size > minConfirms) {
        //we have enough to make a decision
        val votingFor = findLeaders.last
        if(votingFor.nodeId != thisNodeId) {
          log.info(s"I'm voting for $votingFor")
          log.debug(s"(Losers - ${findLeaders.take(findLeaders.size - 1)})")
          ToSend(MessageKeys.VoteLeader, VoteLeader(thisNodeId), Set(votingFor.nodeId))
        } else {
          NoAction
        }
      } else {
        log.info(s"Got ${findLeaders.size} FindLeaders, need over $minConfirms")
        NoAction
      }

      onlyAction(actions)

    case IncomingMessage(`chainId`, MessageKeys.VoteLeader, qMember, vl: VoteLeader) =>
      copy(leaderVotes = leaderVotes + vl).checkAmLeader()


    case IncomingMessage(`chainId`, MessageKeys.Leader, _, Leader(leader)) =>

      (LeaderKnown(
        connectedMembers,
        leader,
        minConfirms,
      ),
        ToPublish(RemoteLeader(`chainId`, leader, connectedMembers)))


    case Quorum(`chainId`, connected, minConfirms) =>
      val findMsg = createFindLeader()
      val actions = ToSend(MessageKeys.FindLeader, findMsg, connected)
      (copy(connectedMembers = connected, minConfirms = minConfirms), actions)


  }
}
