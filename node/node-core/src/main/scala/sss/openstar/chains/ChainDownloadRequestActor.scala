package sss.openstar.chains

import akka.actor.{Actor, ActorContext, ActorLogging, ActorRef, Props}
import akka.pattern.pipe
import sss.ancillary.ByteArrayEncodedStrOps._
import sss.db.Db
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.account.AccountKeys.AccountVerifyAndDets
import sss.openstar.account.{AccountKeysFactory, KeyFactory, NodeVerifier}
import sss.openstar.actor.SystemPanic
import sss.openstar.block._
import sss.openstar.block.signature.BlockSignaturesFactory
import sss.openstar.chains.ChainDownloadRequestActor.PostCheckpointClose
import sss.openstar.chains.Chains.{Checkpointer, GlobalChainIdMask}
import sss.openstar.common.block._
import sss.openstar.controller.Send
import sss.openstar.eventbus.EventPublish
import sss.openstar.ledger.LedgerCheckpoints.Checkpoint
import sss.openstar.ledger.{LedgerCheckpoints, Ledgers}
import sss.openstar.network.ConnectionLost
import sss.openstar.network.MessageEventBus.{EventSubscriptions, EventSubscriptionsByteOps, IncomingMessage}
import sss.openstar.peers.Capabilities
import sss.openstar.peers.PeerManager.PeerConnection
import sss.openstar.{MessageKeys, UniqueNodeIdentifier}

import scala.util.{Failure, Success}

object ChainDownloadRequestActor {

  def createStartSyncer(
                         blockFactory: BlockFactory,
                         blockSignaturesFactory: BlockSignaturesFactory,
                         send: Send,
                         messageEventBus: EventSubscriptions,
                         bc: BlockChain,
                         keyFactory: KeyFactory,
                         checkpoint: Checkpointer,
                         db: Db,
                         ledgers: Ledgers,
                         chainId: GlobalChainIdMask)(context: ActorContext): ActorRef = {

    ChainDownloadRequestActor(
      props(blockFactory, blockSignaturesFactory, bc, checkpoint)(db, chainId, send, messageEventBus, ledgers,keyFactory))(
      context)
  }

  case class CheckedProps(p: Props, name: String)

  case class PostCheckpointClose(blockId: BlockId)

  def props(blockFactory: BlockFactory,
            blockSignaturesFactory: BlockSignaturesFactory,
            bc: BlockChain,
            checkpoint: Checkpointer)(
             implicit db: Db,
             chainId: GlobalChainIdMask,
             send: Send,
             messageEventBus: EventSubscriptions,
             ledgers: Ledgers,
             keyFactory: KeyFactory,
           ): CheckedProps =
    CheckedProps(
      Props(classOf[ChainDownloadRequestActor],
        blockFactory: BlockFactory,
        blockSignaturesFactory: BlockSignaturesFactory,
        bc,
        checkpoint,
        db,
        chainId,
        send,
        messageEventBus,
        ledgers,
        keyFactory),
      s"ChainDownloadRequestActor_${chainId}"
    )

  def apply(props: CheckedProps)(implicit context: ActorContext): ActorRef = {
    context.actorOf(props.p, props.name)
  }

}

private class ChainDownloadRequestActor(blockFactory: BlockFactory,
                                        blockSignaturesFactory: BlockSignaturesFactory,
                                        bc: BlockChain,
                                        checkpoint: Checkpointer)(
                                         implicit db: Db,
                                         chainId: GlobalChainIdMask,
                                         send: Send,
                                         messageEventBus: EventSubscriptions with EventPublish,
                                         ledgers: Ledgers,
                                         keyFactory: KeyFactory
                                       ) extends Actor
  with ActorLogging
  with SystemPanic {

  private val messageKeysOfInterest =
    Seq(
      MessageKeys.NotSynced,
      MessageKeys.Synced,
      MessageKeys.PagedTx,
      MessageKeys.EndPageTx,
      MessageKeys.PagedCloseBlock,
      MessageKeys.RejectedPagedTx)

  override def preStart(): Unit = {
    super.preStart()
    messageKeysOfInterest.subscribe
    messageEventBus.subscribe(classOf[ConnectionLost])
  }

  import context.dispatcher

  var resultsToReport: Option[Any] = None

  private def withPeer(syncingWithNode: UniqueNodeIdentifier, caps: Capabilities): Receive = {

    case ConnectionLost(`syncingWithNode`) =>
      finish(s"Lost connection with synching node ${syncingWithNode}, finishing...")

    case _: ConnectionLost =>

    case IncomingMessage(`chainId`,
    MessageKeys.RejectedPagedTx,
    `syncingWithNode`,
    bTxId: BlockChainTxId) =>
      val blockLedger = BlockChainLedger(blockFactory, bTxId.height).dbRunSyncGet
      blockLedger.rejected(bTxId).dbRunSync match {
        case Success(_) => log.debug("RejectedPagedTx")
        case Failure(e) => systemPanic(e)
      }


    case IncomingMessage(`chainId`,
    MessageKeys.PagedTx,
    `syncingWithNode`,
    BlockChainTx(height, bTx@BlockTx(index, le))) =>

      val blockLedger = BlockChainLedger(blockFactory, height).dbRunSyncGet

      blockLedger(bTx) match {
        case Failure(e) => {
          log.info("Couldn't apply tx {}", e)
          systemPanic(e)

        }
        case Success(txApplied) =>
          txApplied.events foreach messageEventBus.publish
      }

    case IncomingMessage(`chainId`,
    MessageKeys.EndPageTx,
    `syncingWithNode`,
    getTxPage: GetTxPage) =>
      send(MessageKeys.GetPageTx, getTxPage, syncingWithNode)

    case IncomingMessage(`chainId`,
    MessageKeys.PagedCloseBlock,
    `syncingWithNode`,
    DistributeClose(blockSigs, blockId)) =>

      for {
        sigs <- blockSignaturesFactory.getQuorumSigs(blockId.blockHeight).write(blockSigs)
        _ = sigs.foreach(sig => log.debug("DistributeClose block sig {}", sig))

        blockHeader <- bc.closeBlock(blockId, committed = true) andThen {
          case Failure(e) =>
            log.error(e, s"Ledger cannot sync close block $blockId.")
            systemPanic()
        }

        height = blockHeader.height

        _ = if(blockSigs.isEmpty) {
          systemPanic(s"No sig found for blockheader, ${blockHeader} there must be always one")
        }

        allSigsGood = blockSigs.forall { blockSig =>
            val sigGood = keyFactory(blockSig.keyType).verify(blockSig.publicKey, blockHeader.hash, blockSig.signature)
              if (!sigGood) {
                log.warning(s"DistributeClose hash: ${blockHeader.hash.toBase64Str} signatures: ${blockSigs.map(_.toString)}")
              }
            sigGood
          }

        _ = if (!allSigsGood) {
          systemPanic(s"Our block header h ${blockHeader.height} num ${blockHeader.numTxs} did not match the sig")
        }

        _ = log.info(
            s"Synching - committed block height ${blockHeader.height}, num txs  ${blockHeader.numTxs}")

        _ =  require(blockHeader.height == blockId.blockHeight,
            s"How can ${blockHeader} differ from ${blockId}")

        _ =  if (LedgerCheckpoints.isCheckpointHeight(height)) {
            LedgerCheckpoints.getLastCheckpoint match {
              case Some(Checkpoint(`height`, _)) =>
                self ! PostCheckpointClose(blockHeader.blockId)
              case _ =>
                log.info(s"Chain checkpoint {} begins", height)
                checkpoint(height) map {
                  case Success(digest) =>
                    log.info(s"Checkpoint $height done ok")
                    PostCheckpointClose(blockHeader.blockId)
                  case Failure(e) =>
                    log.warning("Checkpoint {} failed {}", height, e)
                    PostCheckpointClose(blockHeader.blockId)

                } pipeTo self
            }
          } else self ! PostCheckpointClose(blockHeader.blockId)

      } yield ()

    case PostCheckpointClose(blockId: BlockId) =>
      messageEventBus publish BlockClosedEvent(chainId, blockId)

      send(MessageKeys.GetPageTx,
        GetTxPage(blockId.blockHeight + 1, 1),
        syncingWithNode)

    case IncomingMessage(`chainId`,
    MessageKeys.NotSynced,
    `syncingWithNode`,
    getTxPage: GetTxPage) =>
      finish(
        s"Downloader is synced to tx page $getTxPage but not fully synced",
        Some(NotSynchronized(chainId)))

    case IncomingMessage(`chainId`,
    MessageKeys.Synced,
    `syncingWithNode`,
    p@GetTxPage(h, i, pageSize)) =>
      finish(
        s"Downloader is fully synced to tx page $p",
        Some(Synchronized(chainId, h, i, syncingWithNode)))

  }

  private def withoutPeer: Receive = {

    case PeerConnection(nodeId, caps) =>
      log.info(s"Syncer now has peer $nodeId, starting to sync.")
      context become withPeer(nodeId, caps)
      val getTxs = createGetTxPage()
      send(MessageKeys.GetPageTx, getTxs, nodeId)
  }

  override def receive: Receive = withoutPeer


  private def getLastCommitted(): BlockId = {
    for {
      lb <- bc.lastCommittedBlockHeader
      currentBlockHeight = lb.height + 1
      blockStorage <- blockFactory.getBlock(currentBlockHeight)
      indexOfLastRow <- blockStorage.getLastCommitted
    } yield BlockId(currentBlockHeight, indexOfLastRow)
  }.dbRunSyncGet

  private def createGetTxPage(): GetTxPage = {
    val BlockId(height, index) = getLastCommitted()
    GetTxPage(height, index + 1, 50)
  }

  private def finish(msg: String, result: Option[Any] = None): Unit = {
    log.info(msg)
    this.resultsToReport = result
    context stop self
  }

  //this is async after actor stop
  override def postStop(): Unit = {
    resultsToReport.foreach(context.parent ! _)
  }

}
