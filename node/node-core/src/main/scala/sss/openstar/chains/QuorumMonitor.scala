package sss.openstar.chains

import akka.actor.{ActorContext, ActorRef}
import sss.openstar._
import sss.openstar.chains.Chains.GlobalChainIdMask
import sss.openstar.chains.QuorumMonitor._
import sss.openstar.common.chains.Quorum
import sss.openstar.controller.Send
import sss.openstar.network.{ConnectionLost, MessageEventBus, NetConnect}
import sss.openstar.peers.PeerManager.{AddQuery, IdQuery, PeerConnection, UnQuery}
import sss.openstar.quorumledger.QuorumLedger.NewQuorumCandidates
import sss.openstar.stateactor.StateActor.Disconnect
import sss.openstar.stateactor._



object QuorumMonitor {

  type QuorumMonitorEvent = Event
  type QuorumMonitorState = State[QuorumMonitor]

  def calcMinConfirms(totalCandidateCount: Int): Int = totalCandidateCount / 2

  case object CheckStatus extends QuorumMonitorEvent

  object QuorumLost {
    def apply(chainId: GlobalChainIdMask): QuorumLost = new QuorumLost(chainId)

    def unapply(arg: QuorumLost): Option[GlobalChainIdMask] = Option(arg.chainId)
  }

  class QuorumLost(val chainId: GlobalChainIdMask) extends BusEvent {
    override def equals(o: Any): Boolean = o match {
      case q: QuorumLost if q.chainId == chainId => true
      case _ => false
    }

    override def hashCode(): Int = chainId.hashCode()
  }

  case class NotQuorumCandidate(override val chainId: GlobalChainIdMask, nodeId: UniqueNodeIdentifier) extends QuorumLost(chainId)

  def apply(

             myNodeId: UniqueNodeIdentifier,
             initialCandidates: Set[UniqueNodeIdentifier]

           )(implicit actorSystem: ActorContext,
             eventMessageBus: MessageEventBus,
             netConnect: NetConnect,
             chainId: GlobalChainIdMask, send:Send): QuorumMonitor = {
    new QuorumMonitor(
      myNodeId: UniqueNodeIdentifier,
      initialCandidates)
  }

}

class QuorumMonitor private(

                             myNodeId: UniqueNodeIdentifier,
                             initialCandidates: Set[UniqueNodeIdentifier],
                           )(implicit actorSystem: ActorContext,
                             eventMessageBus: MessageEventBus,
                             chainId: GlobalChainIdMask,
                             netConnect: NetConnect,
                             send:Send
) {

  implicit val disconnect: Disconnect = netConnect.disconnect

  private val ref: ActorRef = StateActor(
    QuorumState(chainId, myNodeId, initialCandidates),
    s"QuorumMonitorActor_$chainId",
    Seq(
      classOf[PeerConnection],
      classOf[ConnectionLost],
      classOf[NewQuorumCandidates]
    ))


  def checkInitialStatus: Unit = ref ! CheckStatus

}

case class QuorumState(chainId: GlobalChainIdMask,
                       myNodeId: UniqueNodeIdentifier,
                       candidates: Set[UniqueNodeIdentifier],
                       connectedCandidates: Set[UniqueNodeIdentifier] = Set.empty
                      ) extends QuorumMonitorState {


  private val minConfirms: Int = calcMinConfirms(candidates.size)

  lazy val isQuorum = weAreMember && connectedMemberCount() >= minConfirms
  lazy val weAreMember = candidates.contains(myNodeId)

  private def connectedMemberCount(): Int = connectedCandidates.size


  private def removeThisNodeId(nodes: Set[UniqueNodeIdentifier]) =
    nodes.filterNot(_ == myNodeId)


  override def handle: Handler = {

    case CheckStatus =>
      val events = if (isQuorum) {
        ToPublish(Quorum(chainId, connectedCandidates, minConfirms))
      } else NoAction

      val quorumCands = removeThisNodeId(candidates)
      val addQueryEvents = if (quorumCands.isEmpty) NoAction
      else ToPublish(AddQuery(IdQuery(quorumCands)))

      (this, addQueryEvents + events)

    case NewQuorumCandidates(_, `chainId`, newCandidates) =>

      val newState = copy(candidates = newCandidates)

      val newEvents = makeEvents(newState)

      val unQ = candidates diff newState.candidates
      val qCand = newState.candidates diff candidates
      val unQueryCandidates = removeThisNodeId(unQ)
      val addQueryCandidates = removeThisNodeId(qCand)

      val seqUnQuery = if (unQueryCandidates.isEmpty) NoAction else ToPublish(UnQuery(IdQuery(unQueryCandidates)))

      val seqAddQuery = if (addQueryCandidates.isEmpty) NoAction else ToPublish(AddQuery(IdQuery(addQueryCandidates)))

      (newState, seqUnQuery + seqAddQuery + newEvents)


    case PeerConnection(nodeId, _) =>
      if (candidates.contains(nodeId)) {
        val newState = copy(connectedCandidates = connectedCandidates + nodeId)
        val newEvents = makeEvents(newState)
        (newState, newEvents)
      } else NoChange


    case ConnectionLost(nodeId: UniqueNodeIdentifier) =>

      if (candidates.contains(nodeId)) {
        val newState = copy(connectedCandidates = connectedCandidates - nodeId)
        val newEvents = makeEvents(newState)

        (newState, newEvents)
      } else NoChange

  }

  private def makeEvents(newState: QuorumState) = {
    val newEvents = (isQuorum, newState.isQuorum) match {
      case (true, false) if !newState.weAreMember => ToPublish(NotQuorumCandidate(chainId, myNodeId))
      case (true, false) => ToPublish(QuorumLost(chainId))
      case (false, true) => ToPublish(Quorum(chainId, newState.connectedCandidates, minConfirms))
      case (true, true)  if connectedCandidates != newState.connectedCandidates =>
        ToPublish(Quorum(chainId, newState.connectedCandidates, minConfirms))
      case _ => NoAction
    }
    newEvents
  }
}



