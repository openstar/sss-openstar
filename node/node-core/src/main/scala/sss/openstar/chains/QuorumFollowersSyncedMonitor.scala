package sss.openstar.chains


import akka.actor.{ActorContext, ActorRef}
import sss.ancillary.Logging
import sss.openstar._
import sss.openstar.block.BlockChainLedger.NewBlockId
import sss.openstar.block.{Synchronized, VoteLeader}
import sss.openstar.chains.Chains.GlobalChainIdMask
import sss.openstar.chains.LeaderElectionActor.{LeaderFound, LeaderLost, LocalLeader, RemoteLeader}
import sss.openstar.chains.QuorumFollowersSyncedMonitor.{BlockChainReady, QuorumFollowersSyncedMonitorState, QuorumSync, SyncedQuorum}
import sss.openstar.common.block.BlockId
import sss.openstar.controller.Send
import sss.openstar.network.MessageEventBus.IncomingMessage
import sss.openstar.network._
import sss.openstar.stateactor.StateActor.Disconnect
import sss.openstar.stateactor.{Action, NoAction, State, StateActor, ToDisconnect, ToPublish, ToSend}

import scala.language.postfixOps

/**
  * Created by alan on 3/18/16.
  */
object QuorumFollowersSyncedMonitor {

  type QuorumFollowersSyncedMonitorState = State[QuorumFollowersSyncedMonitor.type]

  type QuorumSyncs = Seq[QuorumSync]

  case class SyncedQuorum(chainId: GlobalChainIdMask, syncs: QuorumSyncs, minConfirms: Int) extends BusEvent {
    val members: Set[UniqueNodeIdentifier] = syncs map (_.nodeId) toSet
    val membersSize = members.size

  }

  case class QuorumSync(nodeId: UniqueNodeIdentifier, height: Long, index: Long) extends BusEvent

  /* Issued by every node *except* the leader to indicate
     the node has found a leader and is ready to process txs
   */
  case class BlockChainReady(chainId: GlobalChainIdMask,
                             leader: UniqueNodeIdentifier) extends BusEvent

  def apply(
             thisNodeId: UniqueNodeIdentifier,
            )(implicit actorSystem: ActorContext,
              chainId: GlobalChainIdMask,
              messageEventBus: MessageEventBus,
              netConnect: NetConnect,
              send: Send
  ): ActorRef = {

    implicit val disconnect: Disconnect = netConnect.disconnect

    val init = WaitForLeader(Map.empty, None)(chainId, thisNodeId)
      StateActor(init,
        s"QuorumFollowersSyncedMonitor_$chainId",
        Seq(classOf[NewBlockId], classOf[LeaderLost], classOf[Synchronized], classOf[ConnectionLost], classOf[LeaderFound]),
        Seq(MessageKeys.Synchronized),
       )
  }
}


trait CheckForSyncedQuorum {

  self : Logging =>

  var syncedQuorum: Boolean = false

  val syncedFollowers: Map[UniqueNodeIdentifier, Synchronized]

  def checkForSyncedQuorum(followers: Seq[VoteLeader],
                           minConfirms: Int,
                           areWeSynced: Option[Synchronized]
                          )
                          (implicit chainId: GlobalChainIdMask,
                          thisNodeId: UniqueNodeIdentifier): Action = {

    val withLocalNodeId: Map[UniqueNodeIdentifier, Synchronized] =
      areWeSynced
        .map(s => syncedFollowers + (thisNodeId -> s))
        .getOrElse(syncedFollowers)

    val allVotersMustBeSynced = followers.forall(follower => withLocalNodeId.contains(follower.nodeId))

    log.whenInfoEnabled {
      log.info(s"All followers synced? $allVotersMustBeSynced")
      log.info(s"min confirms          $minConfirms")
      log.info(s"Synced Followers      $syncedFollowers")
      log.info(s"Followers             $followers")
    }

    if (allVotersMustBeSynced && syncedFollowers.size >= minConfirms) {
      val sq = SyncedQuorum(chainId, syncedFollowers map (follower => QuorumSync(follower._1, follower._2.height, follower._2.index)) toSeq, minConfirms)
      syncedQuorum = true

      ToPublish(sq)
    } else NoAction
  }
}

case class WaitForLeader(
                          syncedFollowers: Map[UniqueNodeIdentifier, Synchronized],
                          areWeSynced: Option[Synchronized]
                        )(implicit chainId: GlobalChainIdMask, thisNodeId: UniqueNodeIdentifier)
  extends QuorumFollowersSyncedMonitorState with CheckForSyncedQuorum {

  override def handle: Handler = {

    case IncomingMessage(`chainId`, MessageKeys.Synchronized,  nodeId,  s@Synchronized(`chainId`, _, _,_)) =>
      (copy(syncedFollowers = syncedFollowers + (nodeId -> s)), NoAction)

    case s@Synchronized(`chainId`, _, _, _) =>
      (copy(areWeSynced = Option(s)), NoAction)

    case RemoteLeader(`chainId`, leader, members) =>

      val actions = areWeSynced map { s =>
        ToSend(MessageKeys.Synchronized, s, Set(leader)) +
          ToPublish(BlockChainReady(chainId, leader))
      }
      (RemoteLeading(leader, areWeSynced), actions.getOrElse(NoAction))

    case LocalLeader(`chainId`, `thisNodeId`, height, index, followers, min) =>
      val actions = checkForSyncedQuorum(followers, min, areWeSynced)

      (LocalLeading(syncedFollowers, syncedQuorum, areWeSynced, followers, height, index, min), actions)
  }
}


case class RemoteLeading(leader: UniqueNodeIdentifier,
                         areWeSynced: Option[Synchronized])(implicit chainId: GlobalChainIdMask, thisNodeId: UniqueNodeIdentifier) extends QuorumFollowersSyncedMonitorState {


  override def handle: Handler = {

    case s@Synchronized(`chainId`, height, index, _) =>
      val actions = ToSend(MessageKeys.Synchronized, s, Set(leader)) + ToPublish(BlockChainReady(chainId, leader))
      (copy(areWeSynced = Option(s)), actions)


    case LeaderLost(`chainId`, `leader`) =>
      (WaitForLeader(Map.empty, None), NoAction)

  }
}

case class LocalLeading(syncedFollowers: Map[UniqueNodeIdentifier, Synchronized],
                        initSyncedQuorum: Boolean,
                        areWeSynced: Option[Synchronized],
                        followers: Seq[VoteLeader],
                        height: Long,
                        index: Long,
                        minConfirms: Int)
                       (implicit chainId: GlobalChainIdMask,
                        thisNodeId: UniqueNodeIdentifier)
  extends QuorumFollowersSyncedMonitorState
    with CheckForSyncedQuorum {

  syncedQuorum = initSyncedQuorum


  override def handle: Handler = {

    // it possible a connection is lost during wait for sync
    // but this might not result in QuorumLost or Leader lost.
    case ConnectionLost(nodeId) =>

      val syncedFollowersNow = syncedFollowers filterNot (_._1 == nodeId)
      val followersNow = followers.filterNot(_.nodeId == nodeId)
      val newState = copy(syncedFollowers = syncedFollowersNow, followers = followersNow)
      val actions = newState.checkForSyncedQuorum(newState.followers, newState.minConfirms, areWeSynced)
      (newState.copy(initSyncedQuorum = newState.syncedQuorum), actions)

    case NewBlockId(`chainId`, BlockId(blockHeight, txIndex)) =>
      (copy(height = blockHeight, index = txIndex), NoAction)

    case s@Synchronized(`chainId`, _, _, _) =>
      val newState = copy(areWeSynced = Option(s))
      val actions = checkForSyncedQuorum(newState.followers, newState.minConfirms, areWeSynced)
      (newState.copy(initSyncedQuorum = syncedQuorum), actions)


    case IncomingMessage(`chainId`,
              MessageKeys.Synchronized,
              nodeId,
              s@Synchronized(`chainId`, followerHeight, followerIndex, _)) =>

      if (followerHeight != height || followerIndex != index) {

        log.warn(
          s"Follower $nodeId is 'synchronized' but height {} and index {} don't match leaders {} {} - disconnect!",
          followerHeight, followerIndex, height, index)
        onlyAction(ToDisconnect(nodeId))

      } else {
        val newState = copy(syncedFollowers = syncedFollowers + (nodeId -> s))

        val actions = if (!syncedQuorum) {
          newState.checkForSyncedQuorum(newState.followers, newState.minConfirms, areWeSynced)
        } else {
          ToPublish(QuorumSync(nodeId, followerHeight, followerIndex))
        }
        (newState.copy(initSyncedQuorum = newState.syncedQuorum), actions)
      }

    case LeaderLost(`chainId`, `thisNodeId`) =>
      (WaitForLeader(Map.empty, areWeSynced), NoAction)
  }

}