package sss.openstar.chains


import akka.actor.{Actor, ActorContext, ActorLogging, ActorRef, Props, Stash, SupervisorStrategy}
import sss.ancillary.FutureOps.AwaitResult
import sss.db.Db
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.account.{KeyFactory, NodeIdentity}
import sss.openstar.actor.{Stasher, SystemPanic}
import sss.openstar.block.BlockChainLedger.NewBlockId
import sss.openstar.block.signature.BlockSignatures.BlockSignature
import sss.openstar.block.signature.BlockSignaturesFactory
import sss.openstar.block.{BlockChain, BlockChainLedger, BlockChainSignaturesAccessor, BlockClosedEvent, BlockFactory, DistributeClose, NotSynchronized}
import sss.openstar.chains.Chains.{Checkpointer, GlobalChainIdMask}
import sss.openstar.chains.LeaderElectionActor.{LeaderLost, LocalLeader}
import sss.openstar.chains.SouthboundTxDistributorActor._
import sss.openstar.common.block._
import sss.openstar.controller.Send
import sss.openstar.ledger.{CheckpointSupport, Ledgers}
import sss.openstar.network.MessageEventBus.IncomingMessage
import sss.openstar.network._
import sss.openstar.util.ThrowUtil
import sss.openstar.{BusEvent, MessageKeys, UniqueNodeIdentifier}

import java.util.Date
import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import scala.concurrent.impl.Promise
import scala.util.{Failure, Success, Try}

/**
 * This actor coordinates the distribution of tx's across the connected peers
 * - Making sure a local tx has been written on remote peers.
 * - Adding peers to the upToDate list when TxPageActor says they are synced
 * - Forward the confirms from the remote peers back to the original client.
 * - when a quorum of peers are up to date the 'Synced' event is raised with the State Machine
 *
 * @param quorum
 * @param maxTxPerBlock
 * @param maxSignatures
 * @param stateMachine
 * @param bc
 * @param messageRouter
 * @param db
 */

object SouthboundTxDistributorActor {

  type QuorumCandidates = () => Set[UniqueNodeIdentifier]

  private case class TryClose(nodeId: UniqueNodeIdentifier, dc: DistributeClose, retryCount: Int = 10)

  case class SouthboundRejectedTx(blockChainTxId: BlockChainTxId) extends BusEvent

  case class SouthboundTx(blockChainTx: BlockChainTx) extends BusEvent

  case class SouthboundClose(close: DistributeClose) extends BusEvent

  case class SynchronizedConnection(chainId: GlobalChainIdMask,
                                    nodeId: UniqueNodeIdentifier,
                                    height: Long,
                                    index: Long
  ) extends BusEvent

  case class CheckedProps(value: Props, name: String)

  def props(
             blockFactory: BlockFactory,
             blockSignaturesFactory: BlockSignaturesFactory,
             nodeIdentity: NodeIdentity,
             q: QuorumCandidates,
             bc: BlockChainSignaturesAccessor,
             disconnect: UniqueNodeIdentifier => Unit,
             checkpoint: Checkpointer,
             keysFactoryLookup: KeyFactory,
  )(implicit
    db: Db,
    chainId: GlobalChainIdMask,
    send: Send,
    messageEventBus: MessageEventBus,
    ledgers: Ledgers
  ): CheckedProps =
    CheckedProps(
      Props(
        classOf[SouthboundTxDistributorActor],
        blockFactory,
        blockSignaturesFactory,
        nodeIdentity,
        q,
        bc,
        checkpoint,
        disconnect,
        db,
        chainId,
        send,
        messageEventBus,
        ledgers,
        keysFactoryLookup
      ),
      s"SouthboundTxDistributorActor_$chainId"
    )

  def apply(p: CheckedProps)(implicit actorSystem: ActorContext): ActorRef =
    actorSystem.actorOf(p.value.withDispatcher("blocking-dispatcher"), p.name)
}

private class SouthboundTxDistributorActor(
  blockFactory: BlockFactory,
  blockSignaturesFactory: BlockSignaturesFactory,
  nodeIdentity: NodeIdentity,
  quorumCandidates: QuorumCandidates,
  bc: BlockChain with BlockChainSignaturesAccessor,
  val checkpoint: Checkpointer,
  disconnect: UniqueNodeIdentifier => Unit
                                          )(implicit db: Db, chainId: GlobalChainIdMask, send: Send, messageEventBus: MessageEventBus, ledgers: Ledgers, keysFactory: KeyFactory)
  extends Actor
    with Stash
    with Stasher
    with SystemPanic
    with ActorLogging {

  override val supervisorStrategy = SupervisorStrategy.stoppingStrategy

  private def updateIfNecessary(height: Long, index: Long): Unit = {
    val newBlockId  = BlockId(height, index)

    if(!lastBlockId.contains(newBlockId)) {
      log.info(s"lastBlockId was $lastBlockId , updating to $newBlockId ")
      lastBlockId = Option(newBlockId)
    }
  }

  override def preStart(): Unit = {
    super.preStart()
    messageEventBus.subscribe(classOf[NewBlockId])
    messageEventBus.subscribe(classOf[SouthboundTx])
    messageEventBus.subscribe(classOf[SouthboundRejectedTx])
    messageEventBus.subscribe(classOf[SouthboundClose])
    messageEventBus.subscribe(classOf[SynchronizedConnection])
    messageEventBus.subscribe(classOf[ConnectionLost])
    messageEventBus.subscribe(classOf[NotSynchronized])
    messageEventBus.subscribe(classOf[LocalLeader])
    messageEventBus.subscribe(classOf[LeaderLost])
    messageEventBus.subscribe(MessageKeys.SouthCommittedTx)
    messageEventBus.subscribe(MessageKeys.SouthBlockNewSig)
    messageEventBus.subscribe(MessageKeys.SouthCloseBlock)
    messageEventBus.subscribe(MessageKeys.SouthQuorumRejectedTx)

  }

  log.info("SouthboundTxDistributor actor has started...")

  override def postStop(): Unit =
    log.debug("SouthboundTxDistributor actor stopped ")

  private var weAreLeader = false

  private var syncedNodes: Set[UniqueNodeIdentifier] = Set()
  private var lastBlockId: Option[BlockId] = None

  private def isSynced(lastWrittenHeight: Long, lastWrittenIndex: Long): Boolean =
    lastBlockId.forall { lastId =>
      lastWrittenHeight == lastId.blockHeight &&
      lastWrittenIndex == lastId.txIndex
    }

  import context.dispatcher

  override def receive: Receive = {

    case c @ IncomingMessage(
          `chainId`,
          MessageKeys.SouthCloseBlock,
          nodeId,
          dc: DistributeClose
        ) =>
      self ! TryClose(nodeId, dc)

    case TryClose(nodeId, dc @ DistributeClose(blockSigs, blockId), 0) =>
      log.error("Cannot close block {} and stopped trying ", blockId)

    case t @ TryClose(nodeId, dc @ DistributeClose(blockSigs, blockId), retryCount) =>

      bc.blockHeaderOpt(blockId.blockHeight).dbRunSyncGet match {
          case None =>
            Try(bc.closeBlock(blockId, committed = true).await()) match {
              case Failure(e) =>
                log.error("Couldn't close block {} {} will retry ...", blockId, e)
                import context.dispatcher

                import concurrent.duration._
                context.system.scheduler.scheduleOnce(50.millis, self, t.copy(retryCount = retryCount - 1))

              case Success(header) =>
                log.info("Close block h:{} numTxs: {}", header.height, header.numTxs)
                messageEventBus.publish(BlockClosedEvent(chainId, header.blockId))

                val sigsOk = blockSigs.forall { sig =>
                  keysFactory.get(sig.keyType).fold(ThrowUtil.throwRuntime(s"Sig key type unsupported! ${sig.keyType}")) { kf =>
                    val isOk = kf.verify(sig.publicKey, header.hash, sig.signature)
                    log.info("Post close block, verify sig from {} is {}", sig.nodeId, isOk)
                    isOk
                  }
                }

                require(sigsOk, "Cannot continue block sig from quorum doesn't match that of local header hash")

                val blockSignatures = blockSignaturesFactory.getQuorumSigs(blockId.blockHeight)
                blockSignatures.write(blockSigs)

                if (
                  blockSignatures
                    .indexOfBlockSignatureFTx(nodeIdentity.id)
                    .dbRunSyncGet
                    .isEmpty
                ) {
                  bc.blockHeader(blockId.blockHeight).dbRun flatMap { blockHeader =>
                    nodeIdentity.defaultNodeVerifier.signer.sign(blockHeader.hash) map { sig =>

                      val newSig = BlockSignature(
                        0,
                        new Date(),
                        blockHeader.height,
                        nodeIdentity.id,
                        nodeIdentity.publicKey,
                        nodeIdentity.defaultKeyType,
                        sig
                      )

                      send(MessageKeys.SouthBlockNewSig, newSig, nodeId)
                    }
                  } recover {
                    case e => log.warning(s"Problem signing and sending: $e")
                  }
                }

                self ! SouthboundClose(dc)

            }
          case Some(header) =>
            log.error("BlockHeader {} already exists, we are redistributing?", header)
        }


    case IncomingMessage(`chainId`, MessageKeys.SouthQuorumRejectedTx, leader, blkChnTxId: BlockChainTxId) =>
      BlockChainLedger(blockFactory, blkChnTxId.height).flatMap(_.rejected(blkChnTxId)).dbRunSync match {
        case Failure(e) =>
          log.info("Blockchain sync failed to reject  tx {}", e)

        case Success(_) => log.debug("SouthQuorumRejectedTx")
      }
      self ! SouthboundRejectedTx(blkChnTxId)

    case IncomingMessage(
          `chainId`,
          MessageKeys.SouthCommittedTx,
          leader,
          blkChnTx @ BlockChainTx(height, blockTx)
        ) =>
      val bc: BlockChainLedger = BlockChainLedger(blockFactory, height).dbRunSyncGet
        bc.apply(blockTx) match {
        case Failure(e) =>
          systemPanic(e)

        case Success(txApplied) =>
          txApplied.events foreach messageEventBus.publish
          messageEventBus.publish(NewBlockId(chainId, BlockId(height, blockTx.index)))
      }
      self ! SouthboundTx(blkChnTx)

    case NotSynchronized(`chainId`) =>
      syncedNodes foreach disconnect

    case ConnectionLost(someNode) =>
      syncedNodes -= someNode

    //TODO quorumCandidates() <- this is probably a race condition.
    case SynchronizedConnection(`chainId`, someNode, lastWrittenHeight, lastWrittenIndex)
        if !quorumCandidates().contains(someNode) =>
      //we owe these the new txs and closes *if* we are not leader

      if (weAreLeader && quorumCandidates().size > 1) {
        log.info("We are leader, {} is not in the quorum - disconnect", someNode)
        disconnect(someNode)
      } else if (isSynced(lastWrittenHeight, lastWrittenIndex)) {
        log.debug(s"$someNode synced to $lastWrittenHeight $lastWrittenIndex properly")
        syncedNodes += someNode
      } else {
        log.warning(
          s"Disconnecting node $someNode, was at $lastWrittenHeight $lastWrittenIndex and Southbound distributor is at $lastBlockId"
        )
        disconnect(someNode)
      }

    case _: LocalLeader => weAreLeader = true
    case _: LeaderLost  => weAreLeader = false

    case IncomingMessage(
          `chainId`,
          MessageKeys.SouthBlockNewSig,
          nodeId,
          bs @ BlockSignature(index, savedAt, height, sigNodeId, publicKey, keyType, signature)
        ) =>
      // do something.
      log.info("Got new sig from Southside client {}", bs)
      bc.nonQuorumSigs(height).addSignature(signature, publicKey, keyType, nodeId).recover { case e =>
        log.error(e, "Failed to add southside client sig")
      }

    case NewBlockId(`chainId`, bId) =>
      lastBlockId = Option(bId)

    case SouthboundRejectedTx(blkChnTxId: BlockChainTxId) =>

      updateIfNecessary(blkChnTxId.height, blkChnTxId.blockTxId.index)

      val minusQuorumCandidates = syncedNodes.diff(quorumCandidates())

      if (minusQuorumCandidates.nonEmpty) {
        send(MessageKeys.SouthQuorumRejectedTx, blkChnTxId, minusQuorumCandidates)
      }

    case SouthboundTx(blkChnTx: BlockChainTx) =>
      updateIfNecessary(blkChnTx.height, blkChnTx.blockTx.index)
      val minusQuorumCandidates = syncedNodes.diff(quorumCandidates())

      if (minusQuorumCandidates.nonEmpty) {
        send(MessageKeys.SouthCommittedTx, blkChnTx, minusQuorumCandidates)
      }

    case SouthboundClose(dc) =>
      //TODO Will this work if many txs? Needed to bump in empty block case.
      updateIfNecessary(dc.blockId.blockHeight + 1, 0)
      val minusQuorumCandidates = syncedNodes.diff(quorumCandidates())

      if (minusQuorumCandidates.nonEmpty)
        send(MessageKeys.SouthCloseBlock, dc, minusQuorumCandidates)

  }

}
