package sss.openstar.chains

trait BlockChainSettings {
  val inflationRatePerBlock: Int
  val maxTxPerBlock: Int
  val maxBlockOpenSecs: Int
  val maxSignatures: Int
  val spendDelayBlocks: Int
  val numBlocksCached: Int
  val maxThroughput: Int
}
