package sss.openstar.chains

import akka.actor.{Actor, ActorContext, ActorLogging, Props, Stash}
import sss.ancillary.ByteArrayComparisonOps
import sss.db.ops.DbOps.DbRunOps
import sss.db.{Db, FutureTx}
import sss.openstar.account.AccountKeys.AccountVerifyAndDets
import sss.openstar.account.{AccountKeysFactory, NodeIdentity, NodeVerifier}
import sss.openstar.actor.{Stasher, SystemPanic}
import sss.openstar.block.BlockChainLedger.NewBlockId
import sss.openstar.block._
import sss.openstar.block.signature.BlockSignatures.BlockSignature
import sss.openstar.block.signature.{BlockSignatures, BlockSignaturesFactory}
import sss.openstar.chains.Chains.{Checkpointer, GlobalChainIdMask}
import sss.openstar.chains.LeaderElectionActor.{LeaderLost, LocalLeader, RemoteLeader}
import sss.openstar.chains.SouthboundTxDistributorActor.{SouthboundClose, SouthboundRejectedTx, SouthboundTx}
import sss.openstar.common.block._
import sss.openstar.common.chains.Quorum
import sss.openstar.controller.Send
import sss.openstar.ledger.LedgerOps.TryLedgerResultToTryApplied
import sss.openstar.ledger.{CheckpointSupport, Ledgers, SeqLedgerItem}
import sss.openstar.network.MessageEventBus._
import sss.openstar.network._
import sss.openstar.{MessageKeys, UniqueNodeIdentifier}

import scala.util.{Failure, Success}
import java.util.Date
import scala.concurrent.Future
import sss.openstar.chains.TxDistributeeActor.RemoveFromCache
/*

https://stackoverflow.com/questions/1691179/is-tcp-guaranteed-to-arrive-in-order

 */
object TxDistributeeActor {

  case class RemoveFromCache(id: BlockId) extends AnyVal
  case class CheckedProp(value: Props, name: String)

  def props(
             blockFactory: BlockFactory,
             blockSignaturesFactory: BlockSignaturesFactory,
             bc: BlockChain with BlockChainSignaturesAccessor,
             nodeIdentity: NodeIdentity,
             checkpoint: Checkpointer
           )
           (implicit db: Db,
            chainId: GlobalChainIdMask,
            send: Send,
            messageEventBus: MessageEventBus,
            ledgers: Ledgers,
            accountKeyFactory: AccountKeysFactory
           ): CheckedProp =
    CheckedProp(Props(classOf[TxDistributeeActor],
      blockFactory,
      blockSignaturesFactory,
      bc,
      nodeIdentity,
      checkpoint,
      db,
      chainId,
      send,
      messageEventBus,
      ledgers,
      accountKeyFactory.accountKeysImp
    ),
      s"TxDistributeeActor_$chainId"
    )


  def apply(p: CheckedProp)(implicit actorSystem: ActorContext): Unit = {
    actorSystem.actorOf(p.value.withDispatcher("blocking-dispatcher"), p.name)
  }
}

private class TxDistributeeActor(
                                  blockFactory: BlockFactory,
                                  blockSignaturesFactory: BlockSignaturesFactory,
                                  bc: BlockChain with BlockChainSignaturesAccessor,
                                  nodeIdentity: NodeIdentity,
                                  val checkpoint: Checkpointer
                                )(implicit db: Db,
                                  chainId: GlobalChainIdMask,
                                  send: Send,
                                  messageEventBus: MessageEventBus,
                                  ledgers: Ledgers,
                                  acVerify: AccountVerifyAndDets
                                )
  extends Actor
    with Stash
    with Stasher
    with ActorLogging
    with ByteArrayComparisonOps
    with SystemPanic {


  override def preStart(): Unit = {
    super.preStart()
    messageEventBus.subscribe(classOf[IsSynced])
    messageEventBus.subscribe(classOf[LocalLeader])
    messageEventBus.subscribe(classOf[RemoteLeader])
    messageEventBus.subscribe(classOf[LeaderLost])
    messageEventBus.subscribe(classOf[Quorum])
  }

  val distributeeNetMsgCodes = Seq(
    MessageKeys.CloseBlock,
    MessageKeys.BlockSig,
    MessageKeys.ConfirmTx,
    MessageKeys.CommittedTxId,
    MessageKeys.QuorumRejectedTx
  )

  log.info("TxDistributee actor has started...")

  import context.dispatcher

  private var txCache: Map[BlockId, SeqLedgerItem] = Map()
  private var remoteLeaderId: UniqueNodeIdentifier = ""
  private var quorumMembers: Seq[UniqueNodeIdentifier] = Seq.empty
  private var quorumMembersSize: Int = 0


  private def getBlockChainLedger(height: Long): FutureTx[BlockChainLedger] = {
    BlockChainLedger(blockFactory, height)
  }

  override def receive: Receive = {

    case Synchronized(`chainId`, _, _, _) =>
      distributeeNetMsgCodes.subscribe

    case NotSynchronized(`chainId`) =>
      distributeeNetMsgCodes.unsubscribe

    case _: LocalLeader =>
      distributeeNetMsgCodes.unsubscribe

    case Quorum(`chainId`, qMembers, _) =>
      quorumMembers = qMembers.toSeq
      quorumMembersSize = qMembers.size

    //case QuorumLost(`chainId`, quorumMembers, _) =>

    case remoteLeader: RemoteLeader =>
      remoteLeaderId = remoteLeader.leader

    case _: LeaderLost =>
      distributeeNetMsgCodes.subscribe

    case IncomingMessage(`chainId`, MessageKeys.BlockSig, msgSender, blkSig: BlockSignature) if (msgSender == remoteLeaderId) =>
      for {
        header <- bc.blockHeaderOpt(blkSig.height).dbRun
        _ = header match {
          case None =>
            log.error("Got a BlockSig for a non existent header .... {}", blkSig)
          case Some(header) =>
            val isOk = acVerify.verify(blkSig.publicKey, header.hash, blkSig.signature)
            if (isOk) {
              log.info("In BlockSig, verify sig from {} is {}", blkSig.nodeId, isOk)
              val qSigs = blockSignaturesFactory.getQuorumSigs(blkSig.height)
              for {
                _ <- qSigs.write(blkSig)
                _ =  require(quorumMembersSize != 0, "quorumMembersSize is 0")
                allSigs <- qSigs.signatures(quorumMembersSize)
                _ = if (allSigs.size >= quorumMembersSize) {
                  for(committedBlockHeader <- bc.commitBlockHeaderFTx(blkSig.height).dbRun)
                    yield messageEventBus.publish(SouthboundClose(DistributeClose(allSigs, committedBlockHeader.blockId)))
                }
              } yield ()
            } else {
              log.error("FATAL In BlockSig, verify sig from {} is {}", blkSig.nodeId, isOk)
            }
        }
      } yield ()

    case IncomingMessage(`chainId`, MessageKeys.ConfirmTx, msgSender, bTx: BlockChainTx) if (msgSender == remoteLeaderId) =>

      stashWhileDoing(
        (for {
          blockLedger <- getBlockChainLedger(bTx.height)
          _ <- blockLedger.journal(bTx.blockTx)
        } yield ()).dbRun
      , {

        case StasherFutureSuccess(_) =>
          txCache += BlockId(bTx.height, bTx.blockTx.index) -> bTx.blockTx.ledgerItems
          send(MessageKeys.AckConfirmTx, bTx.toId, msgSender)

          pop()

        case StasherFutureFailure(e) =>
          log.error(s"Failed to journal tx!? {}", bTx)
          systemPanic(e)
      })


    case IncomingMessage(`chainId`, MessageKeys.QuorumRejectedTx, msgSender, bTx: BlockChainTxId) if (msgSender == remoteLeaderId) =>

      BlockChainLedger(blockFactory, bTx.height).flatMap(_.rejected(bTx)).dbRun.map {
        case _ =>
          log.info("Rejected tx h: {} i: {}", bTx.height, bTx.blockTxId.index)
          messageEventBus publish SouthboundRejectedTx(bTx)
          self ! RemoveFromCache(BlockId(bTx.height, bTx.blockTxId.index))
      } recover {
        case e => systemPanic(e)
      }

    case IncomingMessage(`chainId`, MessageKeys.CommittedTxId, msgSender,
    bTx: BlockChainTxId) if (msgSender == remoteLeaderId) =>
      val bId = BlockId(bTx.height, bTx.blockTxId.index)
      val retrieved = txCache(bId)
      val blockTx = BlockTx(bId.txIndex, retrieved)

      BlockChainLedger(blockFactory, bId.blockHeight).flatMap(_.commitFTx(blockTx)).dbRun.map {
        case Left(e) => systemPanic(e)

        case Right(txApplied) =>
          log.info("Commit tx h: {} i: {} ledgerId: {} txId: {} ",
            bId.blockHeight, blockTx.index, blockTx.ledgerItems.value.headOption, blockTx.ledgerItems.txIdHexStr)

          messageEventBus publish SouthboundTx(BlockChainTx(bTx.height, blockTx))
          messageEventBus publish NewBlockId(chainId, bId)
          txApplied.events foreach messageEventBus.publish
          self ! RemoveFromCache(bId)

      } recover {
        case e => systemPanic(e)
      }


    case RemoveFromCache(blockId) =>
      txCache -= blockId

    case c@IncomingMessage(`chainId`, MessageKeys.CloseBlock, msgSender, dc@DistributeClose(blockSigs, blockId)) if (msgSender == remoteLeaderId) =>

      stashWhileDoing(
        for {
          headerOpt <- bc.blockHeaderOpt(blockId.blockHeight).dbRun
          _ = headerOpt match {
            case Some(header) =>
              log.info("Already Closed!! block h:{} numTxs: {}", header.height, header.numTxs)
              val blockSignatures = blockSignaturesFactory.getQuorumSigs(blockId.blockHeight)
              for {
                sigOpt <- findOurSignature(blockSignatures)
                _ = sigOpt match {
                  case Some(sig) =>
                    send(MessageKeys.BlockNewSig, sig, msgSender)
                  case None =>
                    createOurSignature(blockId) foreach { sig =>
                      send(MessageKeys.BlockNewSig, sig, msgSender)
                    }
                }
              } yield ()
            case None =>
              for {
                header <- bc.closeBlock(blockId) andThen { case Failure(e) =>
                  log.error("Couldn't close block {} {}", blockId, e)
                  systemPanic(e)
                }
                _ = log.info("Close block h:{} numTxs: {}", header.height, header.numTxs)
                sigsOk = blockSigs.forall {
                  sig =>
                    val isOk = acVerify.verify(sig.publicKey, header.hash, sig.signature)
                    log.info("Post close block, verify sig from {} is {}", sig.nodeId, isOk)
                    isOk
                }
                _ = require(sigsOk, "Cannot continue block sig from quorum doesn't match that of local header hash")

                blockSignatures = blockSignaturesFactory.getQuorumSigs(blockId.blockHeight)

                sigF <- findOurSignature(blockSignatures)
                sig <- sigF match {
                  case Some(value) => Future.successful(value)
                  case None => createOurSignature(blockId)
                }
                _ = send(MessageKeys.BlockNewSig, sig, msgSender)
                _ = require(quorumMembersSize != 0, s"quorumMembersSize is 0 (blockHeaderOpt) $blockId")
                allSigs <- blockSignatures.write(blockSigs)
                _ = if (allSigs.size >= quorumMembersSize) {
                  for {
                    _ <- bc.commitBlockHeaderFTx(header.height).dbRun
                  } yield messageEventBus.publish(SouthboundClose(dc))
                }
              } yield ()
          }
        } yield ()
        , {
          case _: StasherFutureSuccess[_] => pop()
          case e: StasherFutureFailure =>
            log.error("Failure while closing block? {}", e)
            pop()
        })
  }

  private def findOurSignature(blockSignatures: BlockSignatures): Future[Option[BlockSignature]] = {
    blockSignatures
      .signaturesFTx(Int.MaxValue)
      .dbRun
      .map(_.find(_.nodeId == nodeIdentity.id))
  }

  private def createOurSignature(blockId: BlockId): Future[BlockSignature] = {
    for {
      blockHeader <- bc.blockHeader(blockId.blockHeight).dbRun
      sig <- nodeIdentity.defaultNodeVerifier.signer.sign(blockHeader.hash)
    } yield
      BlockSignature(0,
        new Date(),
        blockHeader.height,
        nodeIdentity.id,
        nodeIdentity.publicKey,
        nodeIdentity.defaultKeyType,
        sig)
  }

}
