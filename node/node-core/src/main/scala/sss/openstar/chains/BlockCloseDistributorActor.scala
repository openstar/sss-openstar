package sss.openstar.chains

import akka.actor.{Actor, ActorContext, ActorLogging, ActorRef, Cancellable, PoisonPill, Props, Stash, Status, SupervisorStrategy}
import akka.pattern.pipe
import sss.ancillary.ByteArrayComparisonOps
import sss.db.Db
import sss.openstar.account.NodeIdentity
import sss.openstar.actor.{Stasher, SystemPanic}
import sss.openstar.block.signature.BlockSignatures.BlockSignature
import sss.openstar.block.{BlockChain, BlockChainSignaturesAccessor, BlockClosedEvent, DistributeClose}
import sss.openstar.chains.BlockChainHelper.{DistributeCloseResult, RetryClose}
import sss.openstar.chains.BlockCloseDistributorActor.{BlockCloseDistributed, SigsNotArrivingInTime}
import sss.openstar.chains.Chains.{Checkpointer, GlobalChainIdMask}
import sss.openstar.chains.QuorumFollowersSyncedMonitor.SyncedQuorum
import sss.openstar.controller.Send
import sss.openstar.hash.Digest32
import sss.openstar.ledger.LedgerCheckpoints
import sss.openstar.ledger.LedgerCheckpoints.{Checkpoint, listObsoleteCheckpoints}
import sss.openstar.network.MessageEventBus.IncomingMessage
import sss.openstar.network._
import sss.openstar.{MessageKeys, UniqueNodeIdentifier}

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.util.Success

object BlockCloseDistributorActor {

  case object SigsNotArrivingInTime

  case class BlockCloseDistributed(height: Long)

  case class CheckedProps(value: Props, name: String)

  case object WaitForClose

  def props(height: Long,
            bc: BlockChain with BlockChainSignaturesAccessor,
            nodeIdentity: NodeIdentity,
            checkpoint: Checkpointer
           )(
             implicit db: Db,
             chainId: GlobalChainIdMask,
             send: Send,
             messageEventBus: MessageEventBus
  ): CheckedProps =
    CheckedProps(
      Props(new BlockCloseDistributorActor(
        height,
        bc,
        nodeIdentity,
        checkpoint)
      ),
      s"BlockCloseDistributorActor_${chainId}_${height}"
    )

  def apply(p: CheckedProps)(implicit context: ActorContext): ActorRef = {
    context.actorOf(p.value.withDispatcher("blocking-dispatcher"), p.name)
  }
}

private class BlockCloseDistributorActor(
                                          height: Long,
                                          bc: BlockChain with BlockChainSignaturesAccessor,
                                          nodeIdentity: NodeIdentity,
                                          checkpoint: Checkpointer,
                                        )(implicit db: Db,
                                          chainId: GlobalChainIdMask,
                                          send: Send,
                                          messageEventBus: MessageEventBus)
  extends Actor
    with ActorLogging
    with ByteArrayComparisonOps
    with Stasher
    with Stash
    with SystemPanic {

  import context.dispatcher

  override val supervisorStrategy = SupervisorStrategy.stoppingStrategy

  override def postStop(): Unit = {
    log.debug("BlockCloseDistributor actor for {} has stopped", height)
  }

  override def preStart(): Unit = {
    super.preStart()
    messageEventBus.subscribe(MessageKeys.BlockNewSig)
  }

  private var currentBlockSignatories: Set[UniqueNodeIdentifier] = Set.empty
  private var sigsTimeOut: Option[Cancellable] = None
  private var checkpointingInProgress = false

  private val blockChainHelper = new BlockChainHelper(bc, nodeIdentity, log)

  private def waitForClose(sq: SyncedQuorum, closeToDist: DistributeClose): Receive = {

    case SigsNotArrivingInTime =>
      stopIfConfirmed(sq.members)

    case Success(a: Digest32) =>
      checkpointingInProgress = false
      log.info(s"Checkpoint {} complete", height)
      stopIfConfirmed(sq.members)

    case Status.Failure(e) =>
      log.error(s"Checkpoint {} failed, {} ", height, e)
      checkpointingInProgress = false
      stopIfConfirmed(sq.members)

    case newSq: SyncedQuorum =>
      val newMembers = newSq.members diff sq.members
      send(MessageKeys.CloseBlock,
        closeToDist,
        newMembers)
      stopIfConfirmed(newSq.members)
      context become waitForClose(newSq, closeToDist)


    case IncomingMessage(`chainId`,
    MessageKeys.BlockNewSig,
    nodeId,
    bs@BlockSignature(_,
    savedAt,
    `height`,
    sigNodeId,
    publicKey,
    keyType,
    signature)) =>

      log.info("Add new sig, height {}, from {}", height, sigNodeId)

      val localSq = sq
      stashWhileDoing(
        for {
          _ <- bc.quorumSigs(height).addSignature(signature, publicKey, keyType, nodeId)
          allSigs <- bc.quorumSigs(height).signatures(Int.MaxValue)
        } yield allSigs.toSeq, {

          case StasherFutureSuccess(allNodesSigs: Seq[BlockSignature] @unchecked) =>
            send(MessageKeys.BlockSig, allNodesSigs.filter(_.nodeId == nodeId).head, localSq.members)
            currentBlockSignatories = allNodesSigs.map(_.nodeId).toSet
            stopIfConfirmed(localSq.members)
            pop()

          case StasherFutureFailure(e) =>
            log.error("bc.addSignature failure {}", e)
            pop()
        })

  }

  private def waitForSq: Receive = {
    case sq: SyncedQuorum =>

      import context.dispatcher
      sigsTimeOut = Some(context.system.scheduler.scheduleOnce(15.second, self, SigsNotArrivingInTime))
      //as soon as we map the future , the actor is free to process another SyncedQuorum message
      val localQuorum = sq
      stashWhileDoing(blockChainHelper.closeBlock(height), {

          case StasherFutureSuccess(DistributeCloseResult(closeToDistribute, isRedistribute)) =>

            pop()

            log.info(s"Dist $closeToDistribute to ${localQuorum.members}")
            if(!isRedistribute) messageEventBus.publish(BlockClosedEvent(chainId, closeToDistribute.blockId))

            stopIfConfirmed(localQuorum.members)

            send(MessageKeys.CloseBlock,
              closeToDistribute,
              localQuorum.members)

            context become waitForClose(sq, closeToDistribute)

          case StasherFutureSuccess(RetryClose) =>
            log.info("Cannot close block {}, previous not yet closed ... retrying ", height)
            context.system.scheduler.scheduleOnce(1.second, self, localQuorum)
            pop()

          case StasherFutureFailure(requireFailed: IllegalArgumentException) =>
            systemPanic(requireFailed)

          case StasherFutureFailure(e) =>
            log.error("Failed to close block, now what? {}", e)
            pop()

        })

  }

  private def stopIfConfirmed(expectedConfirmers: Set[UniqueNodeIdentifier] ) = {

    log.info(s"Stop if Confirmed {}", expectedConfirmers)

    implicit val ec: ExecutionContext = context.dispatcher

    val shouldWeExit: Boolean =
      if (LedgerCheckpoints.isCheckpointHeight(height)) {
        LedgerCheckpoints.getLastCheckpoint match {
          case Some(Checkpoint(`height`, _)) =>
            true
          case _ =>
            if (checkpointingInProgress) {
              false
            } else {
              log.info(s"Chain checkpoint {} begins", height)
              checkpointingInProgress = true
              checkpoint(height) pipeTo self
              false
            }
        }
    } else true

    if (shouldWeExit && expectedConfirmers.diff(currentBlockSignatories).isEmpty) {

      log.debug(
        "stop block close distributor height:{} expected confirmers : {} current signatories: {}",
        height, expectedConfirmers, currentBlockSignatories
      )

      context.parent ! BlockCloseDistributed(height)
      sigsTimeOut foreach (_.cancel())
      self ! PoisonPill
    }
  }


  override def receive: Receive = waitForSq

}
