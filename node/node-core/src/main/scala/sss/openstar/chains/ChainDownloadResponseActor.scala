package sss.openstar.chains

import akka.actor.{Actor, ActorContext, ActorLogging, Props}
import sss.db.Db
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.MessageKeys
import sss.openstar.MessageKeys._
import sss.openstar.block.signature.BlockSignaturesFactory
import sss.openstar.block.{BlockChain, DistributeClose, GetTxPage, IsSynced, NotSynchronized, Synchronized}
import sss.openstar.chains.Chains.GlobalChainIdMask
import sss.openstar.chains.SouthboundTxDistributorActor.SynchronizedConnection
import sss.openstar.common.block._
import sss.openstar.controller.Send
import sss.openstar.network.MessageEventBus
import sss.openstar.network.MessageEventBus.IncomingMessage


/**
  * Sends pages of txs to a client trying to download the whole chain.
  *
  *
  * Created by alan on 3/24/16.
  */

object ChainDownloadResponseActor {

  def apply(blockSignaturesFactory: BlockSignaturesFactory,
            maxSignatures: Int,
            bc: BlockChain)
           (implicit actorSystem: ActorContext,
            db: Db,
            chainId: GlobalChainIdMask,
            send: Send,
            messageEventBus: MessageEventBus)

  : Unit = {

    actorSystem.actorOf(
      Props(classOf[ChainDownloadResponseActor],
        blockSignaturesFactory,
        maxSignatures,
        bc,
        db,
        chainId,
        send,
        messageEventBus)
      , s"ChainDownloadResponseActor_$chainId")
  }
}

private class ChainDownloadResponseActor(blockSignaturesFactory: BlockSignaturesFactory,
                                         maxSignatures: Int,
                                         bc: BlockChain)
                                        (implicit db: Db,
                                         chainId: GlobalChainIdMask,
                                         send: Send,
                                         messageEventBus: MessageEventBus) extends Actor with ActorLogging {


  override def preStart(): Unit = {
    super.preStart()
    messageEventBus.subscribe(classOf[IsSynced])
    messageEventBus.subscribe(MessageKeys.GetPageTx)

  }

  private var canIssueSyncs = false

  log.info("ChainDownloadResponse actor has started...")

  override def receive: Receive = {
    case Synchronized(`chainId`, _, _, _) =>
      canIssueSyncs = true


    case NotSynchronized(`chainId`) =>
      canIssueSyncs = false

    case IncomingMessage(`chainId`, GetPageTx, someClientNode, getTxPage@GetTxPage(blockHeight, index, pageSize)) =>

      log.debug(s"${someClientNode} asking me for $getTxPage")

      lazy val nextPage = bc.block(blockHeight).flatMap(_.page(index, pageSize)).dbRunSyncGet
      val lastBlockHeader = bc.lastCommittedBlockHeader.dbRunSyncGet

      val localCurrentBlockHeight = lastBlockHeader.height + 1

      if (localCurrentBlockHeight >= blockHeight) {

        val pageIncremented = GetTxPage(blockHeight, index + nextPage.size, pageSize)
        val indexMinusOne = pageIncremented.index - 1

        nextPage foreach {
          case Left(blockTx) =>
            val bctx = BlockChainTx(blockHeight, blockTx)
            send(MessageKeys.PagedTx, bctx, someClientNode)
          case Right(blockTxId) =>
            send(MessageKeys.RejectedPagedTx, BlockChainTxId(blockHeight, blockTxId), someClientNode)
        }


        if (nextPage.size == pageSize) {
          send(MessageKeys.EndPageTx, pageIncremented, someClientNode)
        } else if (localCurrentBlockHeight == blockHeight) {
          if (canIssueSyncs) {
            send(MessageKeys.Synced, pageIncremented.copy(index = indexMinusOne), someClientNode)
            messageEventBus.publish(SynchronizedConnection(chainId, someClientNode, blockHeight, indexMinusOne))
          } else {
            send(MessageKeys.NotSynced, pageIncremented, someClientNode)
          }
        } else {
          val blockId = BlockId(blockHeight, indexMinusOne)

          import context.dispatcher

          for {
            sigs <- blockSignaturesFactory.getQuorumSigs(blockHeight).signatures(maxSignatures)
            closeBytes = DistributeClose(sigs, blockId)
          } yield send(PagedCloseBlock, closeBytes, someClientNode)

        }

      } else {
        log.debug(s"${someClientNode} asking for block height of $getTxPage, but current block height is $localCurrentBlockHeight")
        send(MessageKeys.NotSynced, getTxPage, someClientNode)
      }


  }
}
