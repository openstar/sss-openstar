package sss.openstar.chains

import akka.event.LoggingAdapter
import sss.db.Db
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.account.NodeIdentity
import sss.openstar.block.{BlockChain, BlockChainSignaturesAccessor, BlockHeader, DistributeClose}
import sss.openstar.chains.BlockChainHelper.{CloseBlockResult, DistributeCloseResult, RetryClose}
import sss.openstar.chains.Chains.GlobalChainIdMask
import sss.openstar.common.block.BlockId

import scala.concurrent.{ExecutionContext, Future}


object BlockChainHelper {
  trait CloseBlockResult
  case class DistributeCloseResult(dc: DistributeClose, isRedistribute: Boolean) extends CloseBlockResult
  case object RetryClose extends CloseBlockResult
}
class BlockChainHelper(
                        bc: BlockChain with BlockChainSignaturesAccessor,
                        nodeIdentity: NodeIdentity,
                        log: LoggingAdapter
                      )(implicit db: Db,
                        ec:ExecutionContext
                      ) {


  def closeBlock(height: Long): Future[CloseBlockResult] = {
    for {
      lastBlockOpt <- bc.blockHeaderOpt(height).dbRun
      prevBlockHeader <- bc.blockHeaderOpt(height - 1).dbRun
      result <- lastBlockOpt match {
        case None =>
          makeDistributeCloseOrRetry(prevBlockHeader, false)
        case Some(header) =>
          log.info("BlockHeader {} already exists, we are redistributing.",
            header)
          makeDistributeCloseOrRetry(prevBlockHeader, true)
      }
    } yield result

  }

  def makeDistributeClose(blockHeader: BlockHeader, isRedistribute: Boolean): Future[DistributeCloseResult] = for {
    newLastBlock <- bc.closeBlock(blockHeader)
    existingSigs <- bc.quorumSigs(newLastBlock.height).signatures(Int.MaxValue)
    allSigs <- if(existingSigs.exists(_.nodeId == nodeIdentity.id)) Future.successful(existingSigs) else {
      bc.
        quorumSigs(newLastBlock.height)
        .sign(nodeIdentity, newLastBlock.hash)
        .map(existingSigs.toSeq :+ _)
    }
  } yield DistributeCloseResult(DistributeClose(
    allSigs.toSeq,
    BlockId(newLastBlock.height, newLastBlock.numTxs)
  ), isRedistribute)

  def makeDistributeCloseOrRetry(blockHeaderOpt: Option[BlockHeader], isRedistribute: Boolean): Future[CloseBlockResult] =

    blockHeaderOpt match {
      case Some(blockHeader) =>
        makeDistributeClose(blockHeader, isRedistribute)
      case None =>
        Future.successful(RetryClose)
    }


}
