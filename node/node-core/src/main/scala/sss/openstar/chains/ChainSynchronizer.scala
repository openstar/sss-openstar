package sss.openstar.chains

import akka.actor.{Actor, ActorContext, ActorRef, Cancellable, PoisonPill, Props, SupervisorStrategy}
import sss.ancillary.Logging
import sss.openstar._
import sss.openstar.block.{GetLatestCommittedBlockId, GetLatestRecordedBlockId, IsSynced, NotSynchronized, Synchronized}
import sss.openstar.chains.ChainSynchronizer.{StartSyncer, log}
import sss.openstar.chains.Chains.GlobalChainIdMask
import sss.openstar.chains.LeaderElectionActor.{LeaderFound, LeaderLost, LocalLeader, RemoteLeader}
import sss.openstar.chains.QuorumMonitor.QuorumLost
import sss.openstar.network.{ConnectionLost, MessageEventBus}
import sss.openstar.peers.PeerManager.PeerConnection

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._


object ChainSynchronizer extends Logging {


  def apply(chainQuorumCandidates: Set[UniqueNodeIdentifier],
            myNodeId: UniqueNodeIdentifier,
            startSyncer: StartSyncer,
            getLatestCommitted: GetLatestCommittedBlockId,
            getLatestRecorded: GetLatestRecordedBlockId,
            name: String = "ChainSynchronizer"
           )(implicit actorSystem: ActorContext,
             chainId: GlobalChainIdMask,
             eventMessageBus: MessageEventBus
           ) = new ChainSynchronizer(

    chainQuorumCandidates,
    myNodeId,
    startSyncer,
    getLatestCommitted,
    getLatestRecorded,
    s"${name}_${chainId}"
  )

  type StartSyncer = ActorContext => ActorRef

}

class ChainSynchronizer private(chainQuorumCandidates: Set[UniqueNodeIdentifier],
                                myNodeId: UniqueNodeIdentifier,
                                startSyncer: StartSyncer,
                                getLatestCommitted: GetLatestCommittedBlockId,
                                getLatestRecorded: GetLatestRecordedBlockId,
                                name: String
                               )(implicit actorSystem: ActorContext,
                                 chainId: GlobalChainIdMask,
                                 eventMessageBus: MessageEventBus
                               ) {

  final protected val ref = actorSystem.actorOf(Props(SynchronizationActor),
    name)

  final private case object StartSync

  val maxWaitInterval: Long = 3 * 1000

  private lazy val stream: LazyList[Long] = {
    10L #:: 20L #:: stream.zip(stream.tail).map { n =>
      val fib = n._1 + n._2
      if (fib > maxWaitInterval) maxWaitInterval
      else fib
    }
  }

  private case class PeerConnectionTimestamped(peer: PeerConnection, whenLastUsed: Long = 0, noJoyCount: Int = 0) {
    def reset(): PeerConnectionTimestamped = copy(whenLastUsed = 0, noJoyCount = 0)

    def update(): PeerConnectionTimestamped = this.copy(noJoyCount = noJoyCount + 1, whenLastUsed = System.currentTimeMillis())

    def timeLeftToWait(nowMs: Long): Long = {
      val delay = stream(noJoyCount)
      whenLastUsed + delay - nowMs
    }
  }

  def startSync: Unit = ref ! StartSync

  private[chains] def shutdown = {
    eventMessageBus.unsubscribe(ref)
    ref ! PoisonPill
  }

  private object SynchronizationActor extends Actor {

    private def startSyncInMillis(timeToWaitMs: Long): Unit = {

      startSyncTimer.map(_.cancel())

      startSyncTimer = Option(context
        .system
        .scheduler
        .scheduleOnce(timeToWaitMs.millis,
          self,
          StartSync
        )
      )
    }
    private def syncerRef = startSyncer(context)

    override val supervisorStrategy = SupervisorStrategy.stoppingStrategy

    override def preStart(): Unit = {
      super.preStart()
      eventMessageBus.subscribe(classOf[ConnectionLost])
      eventMessageBus.subscribe(classOf[QuorumLost])
      eventMessageBus.subscribe(classOf[LeaderFound])
      eventMessageBus.subscribe(classOf[LeaderLost])
      eventMessageBus.subscribe(classOf[PeerConnection])
    }

    private var inProgress = false

    private var startSyncTimer: Option[Cancellable] = None

    private var synchronised: IsSynced = {
      val bId = getLatestCommitted()
      if (chainQuorumCandidates == Set(myNodeId)) Synchronized(chainId, bId.blockHeight, bId.txIndex, myNodeId)
      else NotSynchronized(chainId)
    }

    private var connectedPeers: Seq[PeerConnectionTimestamped] = Seq()
    private var synchronizingPeerOpt: Option[PeerConnection] = None

    private def reset() = {

      if (synchronised.isSynced) {
        synchronised = NotSynchronized(chainId)
        eventMessageBus publish synchronised
      }
      synchronizingPeerOpt = None
      log.debug("inProgress now false")
      inProgress = false
      startSyncInMillis(3000)
    }

    private def waitForleaderLost: Receive = {

      case ConnectionLost(nodeId) =>
        log.info("(waitForLeaderLost)Connected was {} removing {}, sync peer is {}", connectedPeers, nodeId, synchronizingPeerOpt)
        connectedPeers = connectedPeers.filterNot(_.peer.nodeId == nodeId)


      case LeaderLost(`chainId`, _) =>
          context become receive
          reset()

    }

    override def receive: Receive = {

      case ConnectionLost(nodeId) =>
        log.info("Connected was {} removing {}, sync peer is {}", connectedPeers, nodeId, synchronizingPeerOpt)
        connectedPeers = connectedPeers.filterNot(_.peer.nodeId == nodeId)
        if (synchronizingPeerOpt.exists(_.nodeId == nodeId)) {
          reset()
        }

      case NotSynchronized(`chainId`) => //sent from child
        synchronizingPeerOpt foreach { peer =>
          val (removed, rest) = connectedPeers.partition(_.peer.nodeId == peer.nodeId)
          removed.headOption foreach { found =>
            connectedPeers = rest :+ found.update()
          }
        }
        reset()

      case s@Synchronized(`chainId`, _, _, _) => //sent from child
        // this comes from child actor
        log.info("Now synchronized to {} with {}", s, synchronizingPeerOpt)
        synchronised = s
        inProgress = false
        eventMessageBus.publish(synchronised)
        //in the case a conn is lost before processing this message.
        synchronizingPeerOpt.getOrElse(reset())

      case _: QuorumLost | _: LeaderLost => reset()

      case LocalLeader(`chainId`, leader, height, index, _, _) =>
        inProgress = false
        synchronised = Synchronized(chainId, height, index, leader)
        log.info("Local Leader {} hence synchronized to {}", myNodeId, synchronised)
        context become waitForleaderLost
        eventMessageBus publish synchronised


      case RemoteLeader(`chainId`, leader, _) =>
        //always try to sync with the leader.
        val (leaderRemoved, rest) = connectedPeers.partition(_.peer.nodeId == leader)
        connectedPeers = leaderRemoved.headOption
          .map {
            startSyncInMillis(3000)
            _.reset() +: rest
          }.getOrElse(connectedPeers)


      case QueryStatus =>
        val status = Status(synchronised)
        eventMessageBus.publish(status)


      case StartSync =>

        log.debug(s"start sync, ${inProgress}, ${synchronised.isSynced} connected peers are $connectedPeers")

        (inProgress, synchronised.isSynced) match {

          case (false, false) =>

            connectedPeers.headOption foreach { tracker =>
              val timeToWait = tracker.timeLeftToWait(System.currentTimeMillis())
              if (timeToWait <= 0) {
                syncerRef ! tracker.peer
                inProgress = true
                synchronizingPeerOpt = Option(tracker.peer)
              } else {
                startSyncTimer = Option(context
                  .system
                  .scheduler
                  .scheduleOnce(timeToWait.millis,
                    self,
                    StartSync
                  )
                )
              }
            }
          case (_, _) =>
        }


      case p@PeerConnection(nodeId, caps) if caps.contains(chainId) =>

        if(!connectedPeers.exists(_.peer.nodeId == nodeId)) {
          connectedPeers = connectedPeers :+ PeerConnectionTimestamped(p)
          log.debug(s"New PeerConnection $nodeId, now $connectedPeers")
          startSyncTimer map (_.cancel())
          self ! StartSync
        }

    }
  }

}


