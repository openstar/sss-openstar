package sss.openstar.startuphooks

import akka.actor.Actor.Receive
import akka.actor.{ActorContext, ActorRef, Status}
import akka.pattern.pipe
import scorex.crypto.signatures.Signature
import sss.ancillary.Logging
import sss.openstar.StartupHooks.{DoNextHook, HookDone, NextHook, NextHookImpl}
import sss.openstar.UniqueNodeIdentifier
import sss.openstar.account.NodeIdentity
import sss.openstar.account.Ops.MakeTxSigArys
import sss.openstar.chains.TxWriterActor.InternalTxResult
import sss.openstar.ledger.{LedgerId, LedgerItem, SignedTxEntry}
import sss.openstar.oracleownerledger.OracleClaim
import sss.openstar.startuphooks.ClaimLedgerOwnership.{ClaimLedgerFor, ClaimNextLedger}
import sss.openstar.tools.SendTxSupport.SendTx

import java.nio.charset.StandardCharsets

object ClaimLedgerOwnership {
  case object ClaimNextLedger

  case class ClaimLedgerFor(ledgerId: LedgerId, owner: NodeIdentity, otherOwners: Seq[UniqueNodeIdentifier])

  def makeValidClaimLedger(
                            ledgerId: LedgerId,
                            ownerName: UniqueNodeIdentifier,
                            nodeIdentity: NodeIdentity,
                            otherOwners: Seq[UniqueNodeIdentifier]
                          ): Option[ClaimLedgerFor] = {
    if (ownerName == nodeIdentity.id) {
      Some(ClaimLedgerFor(ledgerId, nodeIdentity, otherOwners))
    } else None
  }

}

class ClaimLedgerOwnership(claims: Seq[ClaimLedgerFor],
                           oracleOwnerLedgerId: LedgerId,
                           val nextHook: NextHook)(implicit
                                                   sendTx: SendTx)
  extends Logging
    with NextHookImpl {



  private def claimEachLedger(claims: Seq[ClaimLedgerFor], self: ActorRef, context: ActorContext): Receive =
    handleHookDone(self, context) orElse {


      case ClaimNextLedger if claims.isEmpty =>
        self ! HookDone


      case ClaimNextLedger =>
        import context.dispatcher
        val nextClaim = claims.head
        val claim = OracleClaim(nextClaim.ledgerId, nextClaim.owner.id, nextClaim.otherOwners)
        nextClaim.owner.defaultNodeVerifier.signer.sign(claim.txId) flatMap { sig =>
          val sigs = Seq(nextClaim.owner.idBytes, nextClaim.owner.tagBytes, sig)
          val sTx = SignedTxEntry(claim.toBytes, Seq(sigs).txSig)
          val ledgerItem = LedgerItem(oracleOwnerLedgerId, sTx.txId, sTx.toBytes)
          sendTx(ledgerItem)
        } pipeTo self

      case Status.Failure(e) =>
        log.warn("Failed to claim ledger.{}, {}", claims.head, e)
        context.become(claimEachLedger(claims.tail, self, context))
        self ! ClaimNextLedger

      case result: InternalTxResult =>
        log.info(s"Claim ledger ${claims.head} result follows")
        log.info(result.toString)
        context.become(claimEachLedger(claims.tail, self, context))
        self ! ClaimNextLedger

    }

  def claimLedgerReceive(self: ActorRef, context: ActorContext): Receive = {
    case DoNextHook =>
      context.become(claimEachLedger(claims, self, context))
      self ! ClaimNextLedger

  }
}
