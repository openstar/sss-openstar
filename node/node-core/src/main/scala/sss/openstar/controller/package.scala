package sss.openstar

import sss.openstar.chains.TxWriterActor.{InternalCommit, InternalNack, InternalTempNack, InternalTxResult}
import sss.openstar.ui.rpc.{Result, ResultOk, problem, success}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

package object controller {

  implicit class SendTxOps(val futTxResult: Future[InternalTxResult]) extends AnyVal {
    def toOkResult: Future[ResultOk] = toResult[Boolean](true)

    def toResult[T](f : => T): Future[Result[T]] = futTxResult map {
      case _: InternalCommit => success(f)
      case _: InternalTempNack => problem("System temporarily busy, try again.")
      case e: InternalNack =>
        problem(e.txMsg.msg)
    }
  }
}
