package sss.openstar.controller

import sss.openstar.MessageKeys
import sss.openstar.account.{NodeIdentity, NodeSigner}
import sss.openstar.identityledger.IdentityLedgerTx
import sss.openstar.ledger.{LedgerItem, SignedTxEntry, TxId}
import sss.openstar.oracleownerledger.OracleLedgerTx
import sss.openstar.systemledger.SystemLedger.toSignedSystemLedgerTxEntry
import sss.openstar.systemledger.SystemLedgerTx
import sss.openstar.util.TxSign

import scala.concurrent.{ExecutionContext, Future}

object Utils {

  def simplySignedTxEntry(user: NodeIdentity, txId: TxId, txBytes: Array[Byte])(implicit executionContext: ExecutionContext):Future[SignedTxEntry] = {
    TxSign.simplySignedTxEntry(user, txId, txBytes)
  }

  def simplySignedIdentityLedgerItem(signer: NodeSigner, tx: IdentityLedgerTx)(implicit ec: ExecutionContext): Future[LedgerItem] =
    TxSign.simplySignedTxEntry(signer, tx.txId, tx.toBytes).map(ste =>
      LedgerItem(MessageKeys.IdentityLedger, tx.txId, ste.toBytes)
    )


  def simplySignedIdentityLedgerItem(user: NodeIdentity, tx: IdentityLedgerTx)(implicit ec: ExecutionContext): Future[LedgerItem] = {
    simplySignedTxEntry(user, tx.txId, tx.toBytes)map { ste =>
      LedgerItem(MessageKeys.IdentityLedger, tx.txId, ste.toBytes)
    }
  }

  def simplySignedOwnerLedgerItem(user: NodeIdentity, tx: OracleLedgerTx)(implicit ec: ExecutionContext): Future[LedgerItem] = {
    simplySignedTxEntry(user, tx.txId, tx.toBytes)map { ste =>
      LedgerItem(MessageKeys.OracleLedgerOwnerLedger, tx.txId, ste.toBytes)
    }
  }

  def signedSystemLedgerItem(user: NodeIdentity, tx: SystemLedgerTx)(implicit ec: ExecutionContext): Future[LedgerItem] = {
    toSignedSystemLedgerTxEntry(user, tx) map (ste =>
      LedgerItem(MessageKeys.SystemLedger, tx.txId, ste.toBytes)
      )
  }

}
