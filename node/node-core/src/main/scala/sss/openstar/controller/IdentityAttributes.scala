package sss.openstar.controller

import sss.openstar.UniqueNodeIdentifier
import sss.openstar.account.NodeIdentity
import sss.openstar.chains.TxWriterActor.InternalTxResult
import sss.openstar.identityledger.{AttributeCategory, BearerAuthorizationAttribute, IdentityServiceQuery, SystemAttributeCategory, ProviderAttachmentUrlAttribute, SetAttribute}
import sss.openstar.tools.SendTxSupport.SendTx

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

/**
  * Created by alan on 6/7/16.
  */
object IdentityAttributes {

  /**
    *
    * @param identity
    * @param baseUrl
    * @param identityQueryService
    * @param sendTx
    * @param ec
    * @return None if the urls already match and Some(InternalTxResult) if the tx was needed to change the url
    */
  def setAttachmentBaseUrl(
                   identity: NodeIdentity,
                   baseUrl: String)(implicit identityQueryService: IdentityServiceQuery,
                                                           sendTx: SendTx,
                                                           ec: ExecutionContext
  ): Future[Option[InternalTxResult]] = Future.fromTry(Try {
    identityQueryService.listAttributes(identity.id)
      .collectFirst {
        case ProviderAttachmentUrlAttribute(currentBaseUrl) if currentBaseUrl == baseUrl => currentBaseUrl
      }
  }).flatMap {
    case None =>
      val tx = SetAttribute(identity.id, ProviderAttachmentUrlAttribute(baseUrl))
      Utils.simplySignedIdentityLedgerItem(identity, tx) flatMap { le =>
        sendTx(le).map(Some(_))
      }

    case Some(_) => Future.successful(None)
  }

  def findBearerAuthAttr(name: UniqueNodeIdentifier)(implicit identityQueryService: IdentityServiceQuery): Option[BearerAuthorizationAttribute] = {
    identityQueryService
      .attribute(name, SystemAttributeCategory.BearerAuthorization)
      .collect { case result: BearerAuthorizationAttribute => result }
  }

  def validateSecret(name: UniqueNodeIdentifier, secret: String)(implicit identityQueryService: IdentityServiceQuery): Boolean = {
    identityQueryService
      .attribute(name, SystemAttributeCategory.BearerAuthorization)
      .collect {
        case BearerAuthorizationAttribute(hashOfSecret) if hashOfSecret.matches(secret) => hashOfSecret
      }
      .isDefined
  }
}
