package sss.openstar.controller

import sss.openstar.UniqueNodeIdentifier
import sss.openstar.chains.Chains.GlobalChainIdMask
import sss.openstar.controller.Send.ToSerializedMessage
import sss.openstar.eventbus.{MessageInfo, PureEvent}
import sss.openstar.network.{NetSend, SerializedMessage}

import scala.reflect.runtime.universe._


object Send {

  def apply(ns: NetSend,
            makeSerializedMessage: ToSerializedMessage): Send =
    new Send(ns, makeSerializedMessage)

  class ToSerializedMessage(decoder: Byte => Option[MessageInfo]) {

    def apply[T: TypeTag](msgCode: Byte, t: T)
                (implicit chainId: GlobalChainIdMask): SerializedMessage = {
      val info = decoder(msgCode)
        .getOrElse(
          throw new IllegalArgumentException(s"$msgCode not supported, no messageInfo found"))

      SerializedMessage(chainId, msgCode, info.toBytes(t))

    }


    def apply(msgCode: Byte)(implicit chainId: GlobalChainIdMask): SerializedMessage =
      SerializedMessage(chainId, msgCode, Array.empty)
        .ensuring(decoder(msgCode).get.isInstanceOf[PureEvent],
        s"No bytes were provided but code $msgCode does not map to a PureEvent class")
  }

}


class Send private(
                    ns: NetSend,
                    makeSerializedMessage: ToSerializedMessage) {


  def apply(msgCode: Byte, nId: UniqueNodeIdentifier)
           (implicit chainId: GlobalChainIdMask): Unit = {
    apply(msgCode, Set(nId))
  }

  def apply(msgCode: Byte, nIds: Set[UniqueNodeIdentifier])
           (implicit chainId: GlobalChainIdMask): Unit = {
    ns(makeSerializedMessage(msgCode), nIds)
  }

  def apply[T: TypeTag](msgCode: Byte, a: T, nId: UniqueNodeIdentifier)
              (implicit chainId: GlobalChainIdMask): Unit = {
    apply(msgCode, a, Set(nId))
  }

  def apply[T: TypeTag](msgCode: Byte, a: T, nIds: Set[UniqueNodeIdentifier])
              (implicit chainId: GlobalChainIdMask): Unit = {

    ns(makeSerializedMessage[T](msgCode, a), nIds)
  }
}
