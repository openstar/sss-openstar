package sss.openstar.nodebuilder

import sss.db.ops.DbOps.DbRunOps
import sss.openstar.account.NodeIdentity
import sss.openstar.chains._
import sss.openstar.common.builders._
import sss.openstar.common.users.FolderBasedUserDirectoryBuilder
import sss.openstar.crypto.keypairs.DefaultAccountKeysFactoryBuilder
import sss.openstar.network.ConnectionGuardActor
import sss.openstar.peers.DiscoveryActor
import sss.openstar.peers.PeerManager.{ChainQuery, IdQuery}
import sss.openstar.schemamigration.SchemaMigrationBuilder
import sss.openstar.systemupdates.AutoUpdate

import java.nio.file.Path
import java.util.logging.{Level, Logger}
import scala.concurrent.Await
import scala.concurrent.duration.DurationInt
import scala.jdk.DurationConverters.JavaDurationOps
import scala.util.{Failure, Success, Try}


trait NodeLayer extends RequirePhrase
  with AutoUpdate
  with SchemaMigrationBuilder
  with FolderBasedUserDirectoryBuilder
  with DefaultAccountKeysFactoryBuilder
  with CpuBoundExecutionContextFromActorSystemBuilder
  with GetSignerDetailsBuilder
  with BootstrapIdentitiesBuilder
  with DefaultIdentityVerificationBuilder
  with ChainLayer
  with MandatoryLedgersLayer
  with NodeIdentityLayer
  with NetworkLayer {

  override lazy val rootAutoUpdateFolder: Path = Path.of(config.getString("datafolder"))

  override lazy val rootTargetFolder = Path.of(".")

  def performAutoUpdateChecks(): Unit = {
    requireSoftwareErasToMatchDatabaseErasAndCompletedProposalIds()
    if (autoUpdateOk()) {
      log.info("AutoUpdate completed successfully, exiting!")
      System.exit(0)
    }
  }

  def startPeerManagerQueries(): Unit = {

    peerManager.addQuery(IdQuery(nodeConfig.peersList ++ seedNodesFromDns map (_.nodeId.id) filterNot (_ == nodeIdentity.id)))

    if (chain.quorumCandidates() contains nodeIdentity.id) {
      peerManager.addQuery(IdQuery(chain.quorumCandidates() - nodeIdentity.id))
    } else {
      peerManager.addQuery(ChainQuery(globalChainId, 15))
    }
  }

  def nodeLayerShutdown(): Unit = {

    log.info("Openstar node layer shutdown")

    uPnpDeletePort()

    val waitMs = config.getInt("actorSystem.terminateWaitInMs")

    Try(Await.ready(actorSystem.terminate(), waitMs.millis)) recover {
      case e => log.info("ActorSystem terminate Error!", e)
    }
    log.info("Attempt to shutdown Db...")
    val shutdownResult = Try(db.shutdown.dbRunSyncGet) //no problem waiting here (!?)
    log.info(s"Db shutdown result: ${shutdownResult}, Openstar shutdown hook exits normally...")
  }

  val claimUrl = fromConfigOrDownload("claimUrl")

  Logger.getLogger("hsqldb.db").setLevel(Level.OFF)

  def startNodeLayer(): Unit = {

    claimAllChainOriginators()
    performAutoUpdateChecks()
    startSystemUpdateMonitorActor

    nodeIdentity //blocking call, get it done deterministically here

    startNetworkLayer()

    LeaderElectionActor(nodeIdentity.id, bc)

    ChainDownloadResponseActor(blockSignaturesFactory, nodeConfig.blockChainSettings.maxSignatures, bc)

    import chain.ledgers

    TxWriterActor(TxWriterActor.props(
      blockFactory,
      nodeConfig.blockChainSettings,
      nodeIdentity.id,
      bc,
      nodeIdentity,
      chain.checkpoint
    ))

    TxDistributeeActor(
      TxDistributeeActor.props(
        blockFactory,
        blockSignaturesFactory,
        bc,
        nodeIdentity,
        chain.checkpoint)
    )

    QuorumFollowersSyncedMonitor(nodeIdentity.id)

    synchronization // Init this to allow it to register for events before QuorumMonitor starts.

    peerManager // Quorum monitor depends on peermanager. (update, does it?)

    quorumMonitor

    TxForwarderActor(nodeIdentity.id, 1000)

    SouthboundTxDistributorActor(
      SouthboundTxDistributorActor.props(blockFactory, blockSignaturesFactory, nodeIdentity, () => chain.quorumCandidates(),
        bc, net.disconnect, chain.checkpoint, keysFactoryLookup)
    )


    DiscoveryActor(DiscoveryActor.props(discovery))


    ConnectionGuardActor(
      config.getInt("connection-guard.threshold"),
      config.getDuration("connection-guard.thresholdDuration").toScala,
      config.getDuration("connection-guard.blackListDuration").toScala
    )(actorContext, globalChainId, messageEventBus, net)

    startPeerManagerQueries()

    quorumMonitor.checkInitialStatus
  }
}




