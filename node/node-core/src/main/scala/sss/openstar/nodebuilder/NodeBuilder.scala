package sss.openstar.nodebuilder
import akka.actor.ActorRef
import com.typesafe.config.Config
import scorex.crypto.signatures._
import sss.ancillary.FutureOps.AwaitResult
import sss.ancillary._
import sss.db.Db
import sss.db.datasource.DataSource
import sss.db.datasource.DataSource.CloseableDataSource
import sss.db.ops.DbOps.DbRunOps
import sss.openstar._
import sss.openstar.account.NodeIdentityManager.{GetSignerDetails, SignerDetails}
import sss.openstar.account.SignerFactory.SignerDescriptor
import sss.openstar.account._
import sss.openstar.balanceledger.OracleBalanceLedger.OracleLedgerOwnerLookupFTx
import sss.openstar.block._
import sss.openstar.block.signature.BlockSignaturesFactory
import sss.openstar.chains.ChainSynchronizer.StartSyncer
import sss.openstar.chains.Chains.{Chain, GlobalChainIdBuilder, GlobalChainIdMask}
import sss.openstar.chains._
import sss.openstar.common.builders._
import sss.openstar.common.users._
import sss.openstar.contract.{FindPublicKeyAccFTxOpt, FindPublicKeyAccOpt}
import sss.openstar.controller.Send
import sss.openstar.controller.Send.ToSerializedMessage
import sss.openstar.crypto.keypairs.DefaultAccountKeysFactoryBuilder
import sss.openstar.db.BlockHeightToTablePartitionId.BlockHeightToTablePartitionId
import sss.openstar.db.Builder.RequireDb
import sss.openstar.eventbus.MessageInfo
import sss.openstar.hash.FastCryptographicHash
import sss.openstar.identityledger.IdentityRolesOps._
import sss.openstar.identityledger.{DefaultIdentifier, Identifier, IdentityLedger, IdentityRoleAttribute, IdentityService}
import sss.openstar.ledger.LedgerCheckpoints.Checkpoint
import sss.openstar.ledger.{Ledger, LedgerCheckpoints, LedgerId, Ledgers}
import sss.openstar.merkle.MerklePersister
import sss.openstar.network.ConnectionHandler.HeartbeatConfig
import sss.openstar.network.NetworkInterface.BindControllerSettings
import sss.openstar.network._
import sss.openstar.oracleownerledger.OracleLedgerOwnerLedger
import sss.openstar.peers.Discovery.DiscoveredNode
import sss.openstar.peers.{Discovery, PeerManager, PeerQuery}
import sss.openstar.quorumledger.{AllAgreeQuorumChangePolicy, QuorumLedger, QuorumService}
import sss.openstar.schemamigration.dynamic.GlobalTableNameTag
import sss.openstar.schemamigration.{RequireDatasource, RequireTablePartitionsCount}
import sss.openstar.systemledger.{SystemLedger, SystemService}
import sss.openstar.systemupdates._
import sss.openstar.tollledger.{TollLedger, TollService}
import sss.openstar.tools.SendTxSupport.SendTx
import sss.openstar.tools.{DownloadSeedNodes, SendTxSupport}
import sss.openstar.util.ConfigHelper
import us.monoid.web.Resty

import java.util
import scala.annotation.tailrec
import scala.concurrent.duration._
import scala.concurrent.{Future, blocking}
import scala.jdk.CollectionConverters._
import scala.jdk.DurationConverters._
import scala.util.{Failure, Success, Try}

/**
 * This is area is used to create 'components' that can be wired up for test or different applications
 *
 * A component should be created only if it is reusable across applications or tests. The final wiring
 * can be done in the 'main' function, not everything has to be buildable.
 *
 * There are a few options for making a component available.
 *
 * 1. Declare as a requirement. Declare the name of the val and the type in a trait . e.g.
 *
 * trait RequireMyThing { val myThing: MyThing }
 *
 * This 'RequireMyThing' trait is now stackable and usable across applications and tests.
 * If myThing is a simple instanciation it can read
 *
 * trait RequireMyThing { val myThing: MyThing = new MyThing() }
 *
 * If it is more complicated and needs construction use a Builder
 *
 * 2.
 *
 * trait MyThingBuilder extends RequireMyThing with RequireOtherThing { val myThing: MyThing = new MyThing(otherThing) }
 *
 * Note the MyThingBuilder depends only on other 'Require'ments. This means in tests or elsewhere one can use the Builder
 * but the dependencies can be wired up as test dependencies.
 *
 * 3.
 * Just add a Builder (no 'Require' trait) , in this case extending this trait means instantly depending on the components
 * used to build it.
 *
 * So a component could have
 *
 * a. no building trait, to be instanciated in the 'main' function
 * b. a Require trait
 * c. a RequireTrait and extending Builder
 * d. a Builder only
 *
 */


trait RequireChainOriginators {
  type ChainIdsKeys = Set[(UniqueNodeIdentifier, PublicKey, Long, Long)]


  case class ChainOriginators(value: ChainIdsKeys) {
    def nodeIdentifiers: Seq[UniqueNodeIdentifier] = value.map(_._1).toSeq
  }
  def getChainOriginators(): Try[ChainIdsKeys]
}

trait ChainOriginatorsBuilder extends RequireChainOriginators {
  self: RequireConfig
    with Logging
    with BootstrapDetailsBuilder
    with NodeIdentityBuilder
    with IdentityServiceBuilder =>

  import sss.ancillary.ByteArrayEncodedStrOps._


  val chainOriginatorIdsAndKeysConfigName = "chainOriginatorIdsAndKeys"
  lazy private val listOfChainOriginatorIdsAndKeys =
    ConfigHelper.toListOfStringLists(prefixedConf, chainOriginatorIdsAndKeysConfigName)

  def requiredChainOriginatorIds: Seq[String] = listOfChainOriginatorIdsAndKeys.map(_.head)

  private val delim = ":"

  private def getChainOriginatorsAndMissing: (ChainIdsKeys, List[String]) = {
    listOfChainOriginatorIdsAndKeys.foldLeft((Set.empty[(String, PublicKey, Long, Long)], List.empty[String])) {
      case ((acc, accMissing), List(chainOriginator)) =>
        identityService.accountOpt(chainOriginator).map(identityService.toVerifier) match {
          case Some(ac) => (acc + ((chainOriginator, ac.verifier.typedPublicKey.publicKey, 0, 0)), accMissing)
          case None =>  (acc, accMissing :+ chainOriginator)
        }
      case ((acc, accMissing), List(chainOriginator, key, balance, licensedUntil)) =>
        val publicKeyFromConfig = PublicKey(key.fromBase64Str)
        val balAsLong = balance.toLong
        val licensedAsLong = licensedUntil.toLong
        identityService.accountOpt(chainOriginator).map(identityService.toVerifier).foreach { foundAc =>
          if(!util.Arrays.equals(publicKeyFromConfig, foundAc.verifier.typedPublicKey.publicKey)) {
            log.warn(
              "Key (default tag) for chain originator {} is {}, this does not match the key in the identity ledger {}",
              chainOriginator,
              key.fromBase64Str,
              foundAc.verifier.typedPublicKey.publicKey.toBase64Str
            )
          }
        }
        (acc + ((chainOriginator, publicKeyFromConfig, balAsLong, licensedAsLong)), accMissing)

      case x => throw new IllegalStateException(s"Expected list with name and key or just name, got $x")
    }
  }

  def getChainOriginators(): Try[ChainIdsKeys] = {
    getChainOriginatorsAndMissing match {
      case (chainIdAndKeys, missing) if missing.isEmpty => Success(chainIdAndKeys)
      case (chainIdAndKeys, missing) =>
        //post and get

        val payload = requiredChainOriginatorIds.find(_ == nodeIdentity.id)
          .map(_ => nodeIdentity.id -> s"${nodeIdentity.id}:${nodeIdentity.publicKey.toBase64Str}")

        val numRetries = 10000
        val msDelay = 1000
        log.info(s"Looking for missing chain originators $missing , max delay $numRetries * $msDelay ms")
        loop(missing, Set.empty, payload, numRetries, msDelay).map(_ ++ chainIdAndKeys)
    }
  }

  private def toTuples(asString: String): ChainIdsKeys = {
    if(asString.isEmpty) {
      Set.empty[(String, PublicKey, Long, Long)]
    } else {
      asString.split(delim).grouped(4).map(tup => (tup.head, PublicKey(tup(1).fromBase64Str), tup(2).toLong, tup(3).toLong)).toSet
    }
  }

  @tailrec
  private def loop(targets: Seq[String],
                   found: ChainIdsKeys,
                   idPayload: Option[(String, String)],
                   numRetries: Int,
                   msDelay: Long): Try[ChainIdsKeys] = {

    def targetsAllFound(current: ChainIdsKeys): Boolean = targets.forall(t => current.map(_._1).contains(t))

    if(targetsAllFound(found)) Success(found)
    else if (numRetries == 0)  Failure(new RuntimeException("Failed to get the chain originator ids and keys"))
    else {
      getFromBootstrapUrl(chainOriginatorIdsAndKeysConfigName, idPayload) match {
        case Success(result) =>
          val chainIdsAndKeys = toTuples(result)
          if(!targetsAllFound(chainIdsAndKeys)) Thread.sleep(msDelay)
          loop(targets, chainIdsAndKeys, idPayload, numRetries - 1, msDelay)
        case Failure(exception) => Failure[ChainIdsKeys](exception)
      }
    }
  }

}

trait BootstrapDetailsBuilder {
  self: RequireConfig with Logging  =>

  private lazy val baseConfigUrl = {

    val s = prefixedConf.getString("bootstrapUrl")
    if (s.endsWith("/")) s else s"$s/"

  }

  private lazy val bootstrapUrl = {
    val chain = prefixedConf.getString("chainName")
    val version = prefixedConf.getString("chainVersion")
    s"$baseConfigUrl$chain/$version"
  }

  lazy val myExternalIp: Option[String] = {
    getFromUrl(s"${baseConfigUrl}cgi-bin/externalIp").toOption
  }

  private def urlWithParams(url: String, params: Map[String, String]): String = {
    if (params.isEmpty) url
    else {
      params.foldLeft(s"${url}?") {
        case (acc, (n, v)) => s"$acc&$n=$v"
      }
    }
  }

  def getFromUrl(url: String, idPayload: Option[(String,String)] = None): Try[String] = {

    val params = idPayload.map(idPayload =>
      Map("id" -> idPayload._1, "payload" -> idPayload._2)
    ).getOrElse(Map.empty)

    Try(new Resty().text(
      urlWithParams(url, params))
    ) recover {
      case e =>
        log.warn(s"Failed to get bootstrap detail for $url")
        throw e
    } map (_.toString.filter(_ >= ' '))
  }


  def getFromBootstrapUrl(attrName: String, idPayload: Option[(String,String)] = None): Try[String] =
    getFromUrl(s"$bootstrapUrl/$attrName", idPayload)

  def fromConfigOrDownload[T](attributeName: String, idPayload: Option[(String,String)] , f: String => T): T =
    if (prefixedConf.hasPath(attributeName)) f(prefixedConf.getString(attributeName))
    else f(getFromBootstrapUrl(attributeName, idPayload).get)


  def fromConfigOrDownload(attributeName: String, idPayload: Option[(String, String)] = None): String =
    fromConfigOrDownload[String](attributeName, idPayload, s => s)

}

trait RequireNodeConfig {
  def bindSettings: BindControllerSettings
  def nodeConfig: NodeConfig

  trait NodeConfig {
    val conf: Config
    def settings: BindControllerSettings
    val uPnp: Option[UPnP]
    val blockChainSettings: BlockChainSettings
    val peersList: Set[DiscoveredNode]
    val discoveryInterval: FiniteDuration
    val initialReportIntervalSeconds: Int
    val telemetryHostVerificationOff: Boolean
    val reportUrlOpt: Option[String]
  }

}

trait NodeConfigBuilder extends RequireNodeConfig {

  self: RequireConfig with Logging  =>

  def bindSettings: BindControllerSettings = BindControllerSettings(prefixedConf)

  lazy val useUpnpFlag: Boolean = true

  lazy val nodeConfig: NodeConfig = NodeConfigImpl(prefixedConf)


  case class NodeConfigImpl(conf: Config) extends NodeConfig with Configure {

    def settings: BindControllerSettings = bindSettings

    lazy val uPnp = if (conf.hasPath("upnp") && useUpnpFlag)
      Some(new UPnP(DynConfig[UPnPSettings](conf.getConfig("upnp")))) else None

    lazy val blockChainSettings: BlockChainSettings =
      DynConfig[BlockChainSettings](conf.getConfig("blockchain"))

    lazy val peersList: Set[DiscoveredNode] = conf
      .getStringList("peers")
      .asScala
      .toSet
      .map(Discovery.toDiscoveredNode)


    import concurrent.duration._

    lazy val discoveryInterval: FiniteDuration = Duration(conf.getString("discoveryInterval")).asInstanceOf[FiniteDuration]

    lazy val initialReportIntervalSeconds: Int = conf.getInt("reportIntervalSeconds")

    lazy val reportUrlOpt: Option[String] =
      if (conf.hasPath("reportUrl")) Some(conf.getString("reportUrl"))
      else None

    lazy val telemetryHostVerificationOff: Boolean = conf.getBoolean("telemetryHostVerificationOff")
  }

  def uPnpDeletePort(port: Try[Int] = Try(nodeConfig.settings.port.toInt)) =
    Try {
      nodeConfig.uPnp.foreach { pnp =>
        port foreach { tcpPort =>
          pnp.deletePort(tcpPort)
        }
      }
    } recover {
      case e => log.info("PnP delete port error", e)
    }
}



trait DbBuilder extends RequireDb with RequireDatasource {

  self: RequireConfig with IOExecutionContextBuilder =>

  override implicit lazy val dataSource: CloseableDataSource =
    DataSource(
      prefixedConf.getConfig("database.datasource")
    )

  override lazy implicit val db: Db =
    Db(prefixedConf.getConfig("database"))(
      dataSource,
      ioExecutionContext.ec
    )

}


trait RequireChain {
  val chain: Chain
}

trait LedgersBuilder {

  self: RequireDb =>

  def allLedgers: Ledgers = new Ledgers(
    coreLedgers.map(l => (l.ledgerId.id -> l)).toMap,
    applicationLedgers.map(l => (l.ledgerId.id -> l)).toMap,
    tollLedger
  )

  def applicationLedgers: Seq[Ledger]  = Seq.empty
  def coreLedgers: Seq[Ledger]

  def tollLedger: TollLedger

  def initializeLedgers(): Seq[Try[String]] = {
    allLedgers.initializeLedgers()
  }
}


trait ChainBuilder extends RequireChain {

  self: BlockChainBuilder
    with Logging
    with IdentityServiceBuilder
    with MessageEventBusBuilder
    with RequireNodeConfig
    with GlobalChainIdBuilder
    with QuorumLedgerBuilder
    with QuorumServiceBuilder
    with IdentityLedgerBuilder
    with OracleLedgerOwnerLedgerBuilder
    with IOExecutionContextBuilder
    with LedgersBuilder
    with RequireDb
    with BlockFactoriesAndMerklePersisterBuilder =>

  lazy val chain: Chain = new Chain {
    implicit override val id: GlobalChainIdMask = globalChainId

    lazy override implicit val ledgers: Ledgers = {
      log.info(s"Initializing ${allLedgers.ordered.size} ledgers...")
      allLedgers.initializeLedgers().foreach {
        case Failure(exception) =>
          // ledgers must initialize successfully
          throw exception
        case Success(msg) =>
          log.info(msg)
      }
      allLedgers
    }

    override def quorumCandidates(): Set[UniqueNodeIdentifier] = quorumService.candidates()

    override val quorumChangePolicyType: Int = changePolicy.quorumPolicyChangeType.id

    override def importState(height: Long): Try[Unit] = {
      ledgers.importState(height).map { u =>
        LedgerCheckpoints.deleteCheckpoint(height)
      }
    }

    override def mostRecentCheckpoint(): Option[Checkpoint] = {
      LedgerCheckpoints.getLastCheckpoint
    }

    override def deleteRecentBlocks(num: Int): Unit = {
      val last = bc.lastCommittedBlockHeader.dbRunSyncGet
      (last.height until (last.height - num) by -1) foreach { h =>
        bc.drop(h)
        merklePersister.drop(h)
      }
    }

    override def checkpoint(height: Long): Future[Try[hash.Digest32]] = Future {
      blocking {
        ledgers.checkpoint(height).flatMap { digest =>
          Try {
            val newChkPoint = LedgerCheckpoints.recordCheckpoint(height, digest)
            LedgerCheckpoints.listObsoleteCheckpoints() foreach { obsolete =>
              log.info(obsolete.toString)
              ledgers.deleteCheckpoint(obsolete.height).map { _ =>
                LedgerCheckpoints.deleteCheckpoint(obsolete.height)
              }
            }
            digest
          }
        }
      }
    }(ioExecutionContext.ec)
  }


}

trait RequireDecoder {
  def decoder: Byte => Option[MessageInfo]
}

trait RequirePeerQuery {
  val peerQuery: PeerQuery
}

trait PeerQueryBuilder extends RequirePeerQuery {
  self: RequirePeerManager =>

  lazy override val peerQuery: PeerQuery = peerManager

}

trait RequirePeerManager {
  val peerManager: PeerManager
}

trait SeedNodesBuilder {

  self: RequireNodeConfig with BootstrapDetailsBuilder =>

  val seedNodesFromDns: Set[DiscoveredNode]

  def getSeedNodesFromDns(thisNode: DiscoveredNode): Set[DiscoveredNode] = {
    val str    = DownloadSeedNodes.discoveredNodeToString(thisNode)
    val params = Some((thisNode.nodeId.id, str))
    DownloadSeedNodes.toDiscoveredNode(Try(fromConfigOrDownload("dnsSeedUrl", params)))
  }
}


trait PeerManagerBuilder extends RequirePeerManager {
  self: NetworkControllerBuilder with
    RequireActorContext with
    RequireDb with
    GlobalChainIdBuilder with
    RequireNodeConfig with
    RequireNetSend with
    SeedNodesBuilder with
    NetworkInterfaceBuilder with
    NodeIdTagBuilder with
    MessageEventBusBuilder =>


  lazy val discovery: Discovery = new Discovery(FastCryptographicHash.hash)

  lazy val thisNode = DiscoveredNode(NodeId(nodeIdTag.nodeId, networkInterface.ownSocketAddress),globalChainId)

  lazy val seedNodesFromDns: Set[DiscoveredNode] = getSeedNodesFromDns(thisNode)

  lazy val peerManager: PeerManager = new PeerManager(
    net,
    net,
    nodeConfig.peersList ++ seedNodesFromDns,
    nodeConfig.discoveryInterval,
    discovery,
    thisNode

  )
}

trait RequireMessageEventBus {
  implicit val messageEventBus: MessageEventBus
}

trait MessageEventBusBuilder extends RequireMessageEventBus {
  self: ActorSystemBuilder with
    RequireDecoder =>

  implicit lazy val messageEventBus: MessageEventBus = new MessageEventBus(
    decoder,
    loggingSuppressedClasses
  )

  def loggingSuppressedClasses: Seq[Class[_]]  = Seq.empty

}


trait DbUserServiceBuilder {

  // this may or may not be the same as the remote schema schema name
  lazy val schemaName: String = "OS_REMOTE"

  // expose this to allow use of remote db
  val dbUserServiceDb: Db
  lazy val dbUserService: DbUserService = new DbUserService(schemaName)(dbUserServiceDb)
}


trait SystemLedgerBuilder {
  self: RequireDb with
    RequireConfig with
    RootActorContextBuilder with
    OracleLedgerOwnerLedgerBuilder with
    RequireIdentityLookup with
    SendTxBuilder with
    NodeIdentityBuilder with
    MessageEventBusBuilder with
    BlockChainBuilder with
    OracleLedgerOwnerLedgerBuilder with
    AutoUpdateConfig =>

  lazy val systemLedgerId = LedgerId(MessageKeys.SystemLedger)

  lazy val systemService: SystemService = new SystemService()

  lazy val systemLedger =
    new SystemLedger(systemService, nodeIdTag => findPublicKeyFTxOpt(nodeIdTag.nodeId, nodeIdTag.tag), ownersLookup)(systemLedgerId, db)

  lazy val updateDownloadFolder: String = rootAutoUpdateFolder.toString

  lazy val systemLedgerOwners: () => Set[UniqueNodeIdentifier] = () => ownersLookup(systemLedgerId).dbRunSyncGet.toSet

  lazy val systemUpdateTracker = new SystemUpdateTracker(
    DownloadUpdate,
    folderContainingAutoUpdates, updateDownloadFolder)

  lazy val updateCheckInterval: FiniteDuration = prefixedConf.getDuration("system.updateCheckInterval").toScala

  override lazy val markUpdateComplete: MarkUpdateComplete = () =>
    systemUpdateTracker.setLastCompleted()


  lazy val startSystemUpdateMonitorActor: ActorRef =
    SystemUpdateMonitorActor(
      SystemUpdateMonitorActor.props(
        systemUpdateTracker,
        currentBlockHeight,
        systemLedgerOwners,
        systemService,
        updateCheckInterval
      )
    )

  private lazy val softwareSupportedEras =
    prefixedConf.getStringList("system.supportedEras").asScala

  private lazy val dbSupportedEras = systemUpdateTracker.listSupportedEras.await()

  private lazy val lastCompletedProposalId: Option[UniqueNodeIdentifier] = systemUpdateTracker.findLastCompleted().await().map(_.uniqueProposalId)

  // call this function at startup to prevent the
  // scenario whereby software from era X is run against data
  // from era Y.
  @throws[IllegalStateException]
  def requireSoftwareErasToMatchDatabaseErasAndCompletedProposalIds(): Unit = {
    requireSoftwareErasToMatchDatabaseEras()
    requireSoftwareErasToSupportRecentlyCompletedProposal()
  }

  private def requireSoftwareErasToMatchDatabaseEras(): Unit = {
    if(softwareSupportedEras != dbSupportedEras) {
      val msg = s"Wrong data for this software! Software eras: ${softwareSupportedEras.mkString(",")}, Db Eras: ${dbSupportedEras.mkString(",")}"
      throw new IllegalStateException(msg)
    }
  }

  private def requireSoftwareErasToSupportRecentlyCompletedProposal(): Unit = {
    if(!lastCompletedProposalId.forall(softwareSupportedEras.contains))
      throw new IllegalStateException(s"Id of last completed proposal ($lastCompletedProposalId) is not present in supported eras (${dbSupportedEras.toList}).")
  }
}

trait TollLedgerBuilder {

  self: RequireDb
    with IdentityServiceBuilder
    with QuorumServiceBuilder
    with ConfigBuilder =>
  def tollService = new TollService(identifierValidator.unsafe)

  val tollLedgerId = LedgerId(MessageKeys.TollLedger)

  def tollAmount: Long = prefixedConf.getLong("blockchain.tollAmount")
  def inflationRatePerBlock: Long = prefixedConf.getLong("blockchain.inflationRatePerBlock")

  lazy val tollLedger = new TollLedger(
    tollService,
    identityService,
    () => quorumService.candidates(),
    tollAmount,
    inflationRatePerBlock,
    tollLedgerId
  )
}
trait IdentityLedgerBuilder {
  self: IdentityServiceBuilder with RequireDb with OracleLedgerOwnerLedgerBuilder =>

  lazy val identityLedgerId = LedgerId(MessageKeys.IdentityLedger)

  lazy val identityLedger =
    new IdentityLedger(identityService, ownersLookup)(identityLedgerId, db)

  def toIdentityLedgerOwner(nodeIdentity: NodeIdentity): Option[NodeIdentity] =
    Option.when(ownersLookup(identityLedgerId).dbRunSyncGet.contains(nodeIdentity.id))(nodeIdentity)
}

trait QuorumLedgerBuilder {

  self: IdentityServiceBuilder
    with MessageEventBusBuilder
    with RequireDb
    with GlobalChainIdBuilder with QuorumServiceBuilder =>

  protected lazy val changePolicy = new AllAgreeQuorumChangePolicy

  lazy val quorumLedger: QuorumLedger = new QuorumLedger(
    globalChainId,
    quorumService,
    changePolicy,
    identityService.accountOptFTx(_,_).map(_.map(identityService.toVerifier))
  )(LedgerId(MessageKeys.QuorumLedger), db)
}



trait OracleLedgerOwnerLedgerBuilder {

  self: RequireDb with IdentityServiceBuilder with KeyFactoryBuilder =>

  val numOwnerLedgersAllowed : Int = 1000

  val numOwnersPerLedgerAllowed : Int = 100

  val oracleOwnerLedgerId = LedgerId(MessageKeys.OracleLedgerOwnerLedger)

  implicit lazy val ownersLookup: OracleLedgerOwnerLookupFTx = oracleLedgerOwnerLedger.lookup


  lazy val oracleLedgerOwnerLedger =
    new OracleLedgerOwnerLedger(
      identifierValidator,
      findPublicKeyFTxOpt,
      numOwnersPerLedgerAllowed,
      numOwnerLedgersAllowed,
      keysFactoryLookup)(db, oracleOwnerLedgerId)

  def initializeLedgerOwners(initialOwners: Seq[String], ledgerId: LedgerId): Try[Seq[String]] = {
    oracleLedgerOwnerLedger.setOwners(initialOwners, ledgerId)
  }
}

trait RequireCardanoLedgerId {
  lazy val cardanoLedgerId = LedgerId(MessageKeys.CardanoLedger)
}



trait RequireStarzBalanceLedgerId {
  lazy val starzBalanceLedgerId = LedgerId(MessageKeys.StarzBalanceLedger)
}





trait RequireIdentityLookup {
  val findPublicKeyFTxOpt: FindPublicKeyAccFTxOpt
  val findPublicKeyOpt: FindPublicKeyAccOpt
}

trait GetSignerDetailsBuilder {
  self: NodeIdentityManagerBuilder
    with CpuBoundExecutionContextBuilder
    with IdentityServiceBuilder =>

  def getSignerDetails: GetSignerDetails = nodeId =>
    for {
      localCreds <- getLocalKeyFileSignerDetails(nodeId)
      idServiceCreds <- getSignerDetailsFromIdentityService(nodeId)
      dupsRemoved = localCreds.filterNot(cred => idServiceCreds.exists(_.nodeIdTag == cred.nodeIdTag))

    } yield {
      idServiceCreds ++ dupsRemoved
    }

}

trait IdentityServiceBuilder extends RequireIdentityLookup {
  self: RequireDb
    with VerifierFactoryBuilder
    with CpuBoundExecutionContextBuilder =>

  lazy val identifierValidator: Identifier = new DefaultIdentifier()

  lazy val findPublicKeyOpt: FindPublicKeyAccOpt = (id, tag) =>
    identityService.accountOpt(id, tag).map(identityService.toVerifier)

  lazy val findPublicKeyFTxOpt: FindPublicKeyAccFTxOpt = (id, tag) =>
    identityService.accountOptFTx(id, tag).map(_.map(identityService.toVerifier))

  implicit lazy val identityService: IdentityService =
    IdentityService()(db, verifierFactory)

  def getSignerDetailsFromIdentityService: GetSignerDetails = nodeId => identityService.accountsFTx(nodeId)
    .dbRun
    .map(_.map(acDetails => {

      val signerDescriptor: SignerDescriptor = SignerDescriptor(
        keyType = acDetails.typedPublicKey.keyType,
        acDetails.securityLevel,
        acDetails.signerType,
        acDetails.interactionType
      )
      SignerDetails(
        acDetails.nodeIdTag,
        acDetails.typedPublicKey.publicKey,
        acDetails.externalKeyIdOpt,
        signerDescriptor
      )
    }))
}

case class BootstrapIdentity(nodeId: String,
                             localKeyVerify: Verifier) {
  def verify(sig: Signature, msg: Array[Byte]): Boolean =
    localKeyVerify.verify(msg, sig)
}


trait RequireBootstrapIdentities {
  def bootstrapIdentities: List[BootstrapIdentity]
}

trait BootstrapIdentitiesBuilder extends RequireBootstrapIdentities {

  self: RequireConfig
    with RequireChainOriginators
    with DefaultAccountKeysFactoryBuilder =>

  lazy val bootstrapIdentities: List[BootstrapIdentity] =
    getChainOriginators()
      .map(_.toList.map {
        case (s, pKey, _, _) =>
          //assumption is the key is of the same type as the default AccountKeysFactory key type
          BootstrapIdentity(s, defaultAccountKeysFactory.keyVerifier(pKey))
      })
      .getOrElse(List.empty)

}

trait QuorumMonitorBuilder {

  self: RequireNodeIdentity
    with GlobalChainIdBuilder
    with RequireChain
    with RequireActorContext
    with RequireNetSend
    with RequireNetConnect
    with RequireMessageEventBus =>

  lazy val quorumMonitor = QuorumMonitor(nodeIdentity.id, chain.quorumCandidates())
}


trait QuorumServiceBuilder {

  self: RequireDb
    with Logging
    with GlobalChainIdBuilder
    with BootstrapDetailsBuilder
    with IdentityServiceBuilder
    with RequireChainOriginators =>

  lazy val chainOriginatorIdsAndKeys = ChainOriginators(getChainOriginators().get)


  lazy val quorumService = {
    val ids = chainOriginatorIdsAndKeys.value.map(_._1).toSeq
    val created = QuorumService.create(globalChainId, ids: _*)
    created.candidates()
    log.info("Quorum candidates")
    created.candidates().foreach(c => log.info(s"Candidate $c"))
    created

  }

  import sss.openstar.identityledger.BackwardsCompatibilityOps.PublicKeyOps

  def claimAllChainOriginators(): Unit = {
    //put the node id of the chain starting node in the id ledger from the beginning.
    chainOriginatorIdsAndKeys.value foreach { chainOriginator =>
      val withAuditorRole = Seq(IdentityRoleAttribute().setNetworkAuditor())
      identityService.claim(
        chainOriginator._3,
        Some(chainOriginator._4),
        chainOriginator._2.toDetails(chainOriginator._1),
        withAuditorRole)
        .dbRunSync
    }
  }
}

trait GlobalTableNameTagBuilder {
  self: GlobalChainIdBuilder =>
  implicit lazy val globalTableNameTag: GlobalTableNameTag = GlobalTableNameTag(globalChainId.toString)
}

trait RequireBlockChain {
  val bc: BlockChain with BlockChainSignaturesAccessor

  type GetCurrentBlockHeight = () => Long

  def currentBlockHeight(): Long
}

trait BlockHeightToTableBuilder {
  self: RequireTablePartitionsCount =>
  protected val blocksPerTable: Int

  protected def blockHeightTooSmallMsg(height: Long): String = s"Min block height is 1, but got $height"
  protected def blockHeightTooBigMsg(height: Long): String =
    s"Max block height is ${tablePartitionsCount * blocksPerTable}, but got $height"

  protected def blockHeightToTablePartitionId: BlockHeightToTablePartitionId = (height: Long) => {
      require(height > 0, blockHeightTooSmallMsg(height))
      (height - 1) / blocksPerTable + 1
    }.ensuring(_ <= tablePartitionsCount, blockHeightTooBigMsg(height))
}

trait BlockFactoriesBuilder {
  self: BlockHeightToTableBuilder with GlobalChainIdBuilder with GlobalTableNameTagBuilder with RequireTablePartitionsCount =>
  protected lazy val blockFactory: BlockFactory = BlockFactory.usingPartitionedTable(blockHeightToTablePartitionId)
  protected lazy val blockSignaturesFactory: BlockSignaturesFactory =
    BlockSignaturesFactory.usingPartitionedTable(blockHeightToTablePartitionId)
}

trait MerklePersisterBuilder {
  self: BlockHeightToTableBuilder with RequireTablePartitionsCount =>
  protected lazy val merklePersister: MerklePersister = MerklePersister(blockHeightToTablePartitionId)
}

trait BlockFactoriesAndMerklePersisterBuilder
    extends MerklePersisterBuilder
    with BlockFactoriesBuilder
    with BlockHeightToTableBuilder {
  self: GlobalChainIdBuilder with GlobalTableNameTagBuilder with RequireTablePartitionsCount =>
}

trait BlockChainBuilder extends RequireBlockChain {

  self: RequireDb
    with GlobalChainIdBuilder
    with BlockFactoriesAndMerklePersisterBuilder
    with KeyFactoryBuilder =>

  lazy val bc: BlockChain with BlockChainSignaturesAccessor =
    BlockChainImpl(blockFactory, blockSignaturesFactory, merklePersister)(db, globalChainId, db.asyncRunContext.ec, keysFactoryLookup)

  def currentBlockHeight(): Long = bc.lastCommittedBlockHeader.dbRunSyncGet.height + 1
}


trait HandshakeGeneratorBuilder {

  self: NetworkInterfaceBuilder with CpuBoundExecutionContextBuilder =>

  lazy val initialStepGenerator: InitialHandshakeStepGenerator =
    ValidateHandshake(
      networkInterface,
      idVerifier
    )
}

trait RequireNetSend {
  implicit val send: Send
}


trait RequireNetConnect {
  implicit val netConnect: NetConnect
}

trait NetConnectBuilder extends RequireNetConnect {

  self: NetworkControllerBuilder =>

  implicit override lazy val netConnect: NetConnect = net
}


trait SendTxBuilder {

  self: MessageEventBusBuilder
    with RequireActorContext
    with GlobalChainIdBuilder =>

  implicit val sendTx: SendTx = SendTxSupport(actorContext, globalChainId, messageEventBus)
}

trait NetSendBuilder extends RequireNetSend {

  self: NetworkControllerBuilder with RequireDecoder =>

  def makeToSerializedDecoder = new ToSerializedMessage(decoder)
  implicit override lazy val send: Send = Send(net, makeToSerializedDecoder)

}

trait NetworkControllerBuilder {

  self: ActorSystemBuilder
    with NodeConfigBuilder
    with MessageEventBusBuilder
    with NetworkInterfaceBuilder
    with NodeIdTagBuilder
    with HandshakeGeneratorBuilder =>

  private lazy val netController =
    new NetworkController(nodeIdTag.nodeId,
      initialStepGenerator,
      networkInterface,
      messageEventBus,
      HeartbeatConfig(bindSettings.heartbeatIntervalms.milliseconds,
        bindSettings.heartbeatTimeoutms.milliseconds
      )
    )

  val heartbeatTimeoutWaitFactor: Int = 2

  implicit lazy val net = netController.start()

}

trait ChainSynchronizerBuilder {

  self: RequireActorContext
    with GlobalChainIdBuilder
    with RequireDb
    with RequireNodeIdentity
    with RequireChain
    with RequireNetSend
    with RequireBlockChain
    with MessageEventBusBuilder
    with BlockFactoriesAndMerklePersisterBuilder
    with KeyFactoryBuilder =>

  lazy val startSyncer: StartSyncer = ChainDownloadRequestActor.createStartSyncer(
    blockFactory,
    blockSignaturesFactory,
    send,
    messageEventBus,
    bc,
    keysFactoryLookup,
    chain.checkpoint, db, chain.ledgers, chain.id)


  lazy val synchronization =
    ChainSynchronizer(chain.quorumCandidates(),
      nodeIdentity.id,
      startSyncer,
      () => bc.getLatestCommittedBlockId(),
      () => bc.getLatestRecordedBlockId().dbRunSyncGet,
    )
}

trait RequireArgumentParser {

  val apiHostPasswordParam: String = "-apiHostPassword"
  val apiHostParam: String = "-apiHost"
  val passwordParam: String = "-password"
  val hammerNumUsers: String = "-hammerNumUsers"
  val hammerIdRoot: String = "-hammerIdRoot"
  val hammerMaxInFlight: String = "-hammerMaxInFlight"
  val hammerMaxTotal: String = "-hammerMaxTotal"
  val startHttpServiceArgName: String = "-startHttp"
  val startRpcServerArgName: String = "-startRpc"
  val noPnpArgName: String = "-noPnp"
  val startHtml5PushArgName: String = "-startHtml5Push"

  def args: ArgumentParser
}















