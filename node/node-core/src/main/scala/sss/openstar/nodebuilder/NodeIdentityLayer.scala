package sss.openstar.nodebuilder

import sss.openstar.common.builders._
import sss.openstar.crypto.keypairs.DefaultAccountKeysFactoryBuilder

trait NodeIdentityLayer extends KeyFactoryBuilder
  with VerifierFactoryBuilder
  with RequirePhrase
  with SignerFactoryBuilder
  with NodeIdentityManagerBuilder
  with DefaultAccountKeysFactoryBuilder
  with KeyPersisterBuilder
  with NodeIdentityBuilder {

  self: BaseLayer with NetworkLayer =>
}
