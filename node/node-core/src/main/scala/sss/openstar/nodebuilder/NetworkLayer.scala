package sss.openstar.nodebuilder

import sss.openstar.chains.Chains.GlobalChainIdBuilder

import scala.concurrent.Await
import scala.concurrent.duration.DurationInt

trait NetworkLayer extends UnsubscribedMessageHandlerBuilder
  with NetworkInterfaceBuilder
  with NodeConfigBuilder
  with RequireDecoder
  with MessageEventBusBuilder
  with HandshakeGeneratorBuilder
  with NetworkControllerBuilder
  with NetSendBuilder
  with NetConnectBuilder
  with SeedNodesBuilder
  with GlobalChainIdBuilder
  with PeerManagerBuilder
  with RequireBootstrapIdentities
  with RequireChainOriginators
  with BaseLayer {

  def startNetworkLayer(): Unit = {

    startUnsubscribedHandler
    peerManager
    //Sleep for twice the heartbeat timeout to prevent rapid restarts
    //not triggering the ConnectionLost event.
    log.info("Wait for Net start ...")
    Thread.sleep(bindSettings.heartbeatTimeoutms * heartbeatTimeoutWaitFactor)
    //Bind to the TCP port
    Await.result(net.start(), 10.seconds)
    log.info("Net bound, begin query....")
  }
}
