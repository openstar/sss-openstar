package sss.openstar.nodebuilder

import akka.actor.{Actor, ActorContext, ActorLogging, ActorRef, Props}
import sss.openstar.chains.Chains.GlobalChainIdMask
import sss.openstar.common.builders.NodeIdentityBuilder
import sss.openstar.controller.Send
import sss.openstar.eventbus.StringMessage
import sss.openstar.network.MessageEventBus
import sss.openstar.network.MessageEventBus.{IncomingMessage, Unsubscribed, UnsubscribedIncomingMessage}
import sss.openstar.{MessageKeys, UniqueNodeIdentifier}

trait UnsubscribedMessageHandlerBuilder {

  self: RequireNetSend with
    RequireActorContext with
    MessageEventBusBuilder =>

  lazy val startUnsubscribedHandler: ActorRef = {
    DefaultMessageHandlerActor(send, messageEventBus)
  }

  private object DefaultMessageHandlerActor {

    def apply(send: Send,
              messageEventBus: MessageEventBus,
             )(implicit actorSystem: ActorContext): ActorRef = {

      actorSystem.actorOf(
        Props(classOf[DefaultMessageHandlerImpl],
          send,
          messageEventBus)
        , "DefaultMessageHandlerActor")

    }
  }

}


private class DefaultMessageHandlerImpl(send: Send,
                                        messageEventBus: MessageEventBus)
  extends Actor with
    ActorLogging {

  override def preStart(): Unit = {
    super.preStart()
    messageEventBus.subscribe(classOf[Unsubscribed])
  }


  override def receive: Receive = {

    case UnsubscribedIncomingMessage(IncomingMessage(chainId: GlobalChainIdMask, msgCode, clientNodeId, _))
      if msgCode != MessageKeys.GenericErrorMessage =>

    implicit val c = chainId

    send(MessageKeys.GenericErrorMessage,
      StringMessage(s"Received your msg (code=$msgCode) " +
        "but is not processing at this time")
      , clientNodeId)

    case x =>
      log.warning("Event has no sink! {} ", x)

  }
}
