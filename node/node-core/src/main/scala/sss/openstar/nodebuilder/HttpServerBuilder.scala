package sss.openstar.nodebuilder

import org.eclipse.jetty.server.Server
import sss.ancillary.{DynConfig, Logging, ServerConfig}
import sss.openstar.common.builders._
import sss.openstar.db.Builder.RequireDb


import scala.util.Try

/**
 * Created by alan on 6/16/16.
 */
trait HttpServerBuilder {

  self: NodeConfigBuilder with
    NodeIdTagBuilder with
    ConfigBuilder with
    Logging =>

  implicit lazy val httpConfig = DynConfig[ServerConfig](nodeConfig.conf.getConfig("httpServerConfig"))

  val httpServer: Server

  lazy val startHttpServiceFlag: Boolean = true

  def stopHttpServer(): Unit = {
    if (startHttpServiceFlag) {
      log.info(s"Stopping HttpServer...")
      httpServer.stop()
      uPnpDeletePort(Try(httpConfig.httpPort))
    }
  }

  def startHttpServerIfFlagSet(): Unit = {
    //addClaimServlet
    if (startHttpServiceFlag) {
      httpServer.start()
      log.info(s"HttpServer started on " +
        s"${httpConfig.httpPort}, ssl? " +
        s"${httpConfig.useSslConnector} ${httpConfig.httpsPort}"
      )

    } else {
      log.info(s"No startHttpServiceFlag false, not starting http server")
    }
  }

}

