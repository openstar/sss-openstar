package sss.openstar.nodebuilder

trait ChainLayer
  extends ChainSynchronizerBuilder
    with ChainBuilder
    with QuorumMonitorBuilder
    with ChainOriginatorsBuilder {

  self: BaseLayer
    with NetworkLayer
    with NodeIdentityLayer
    with MandatoryLedgersLayer =>
}
