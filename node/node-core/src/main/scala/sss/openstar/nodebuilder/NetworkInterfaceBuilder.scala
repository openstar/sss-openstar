package sss.openstar.nodebuilder

import scorex.crypto.signatures.Signature
import sss.ancillary.Logging
import sss.ancillary.Results._
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.account.{NodeIdentity, Signer, Verifier}
import sss.openstar.common.builders.NodeIdentityBuilder
import sss.openstar.db.Builder.RequireDb
import sss.openstar.identityledger.IdentityRolesOps.ReservedIdentityRolesAccessor
import sss.openstar.network.{IdentityVerification, NetworkInterface}

import scala.concurrent.Future
import scala.util.Try

trait NetworkInterfaceBuilder {

  self:
    BootstrapDetailsBuilder with
    NodeConfigBuilder =>

  lazy val networkInterface: NetworkInterface = buildNetworkInterface


  def buildNetworkInterface = {
    val result = new NetworkInterface(nodeConfig.settings, nodeConfig.uPnp, () => myExternalIp)
    nodeConfig.uPnp.foreach( pnp => {
      pnp.addPort(nodeConfig.settings.port.toInt)
    })
    result
  }

  def idVerifier: IdentityVerification

}

trait DefaultIdentityVerificationBuilder {

  self: RequireBootstrapIdentities
    with BootstrapDetailsBuilder
    with Logging
    with NodeConfigBuilder
    with NodeIdentityBuilder
    with IdentityServiceBuilder
    with RequireDb =>


  def idVerifier: IdentityVerification = createIdVerifier(nodeIdentity, bootstrapIdentities)

  protected def createIdVerifier(nodeIdentity: NodeIdentity,
                                 bootstrapIdentities: List[BootstrapIdentity]
                                ): IdentityVerification = {

    new IdentityVerification {

      override def verify(sig: Signature,
                          msg: Array[Byte],
                          nodeId: String,
                          tag: String): OkResult = {

        val ac = identityService.accountOptFTx(nodeId, tag).map(_.map(identityService.toVerifier)).dbRunSyncGet

        lazy val nodeTagStr = s"$nodeId, $tag"
        ac orErrMsg s"Could not find $nodeTagStr in identity ledger" andThen {

          ac.exists(_.verifier.verify(msg, sig) &&
            identityService.roleOwnedAttribute(nodeId).isNetworkAuditor) orErrMsg s"Could not verify signature of $nodeTagStr"

        } ifNotOk {
          //try the bootstrap
          bootstrapIdentities.find(_.nodeId == nodeId)
            .orErrMsg(s"NodeId $nodeId not in bootstrap identities")
            .ifOk { bi =>
              bi.verify(sig, msg)
                .orErrMsg(s"Could not verify sig $sig from NodeId $nodeId")
            }
        }
      }

      override def sign(msg: Array[Byte]): Future[Array[Byte]] = nodeIdentity.defaultNodeVerifier.signer.sign(msg)

      override val nodeId: String = nodeIdentity.id
      override val tag: String = nodeIdentity.tag

    }

  }
}