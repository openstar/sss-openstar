package sss.openstar.nodebuilder

import sss.ancillary.Logging
import sss.openstar.common.builders._

trait BaseLayer extends ActorSystemBuilder
  with RootActorContextBuilder
  with NodeIdTagBuilder
  with ConfigBuilder
  with Logging
  with CpuBoundExecutionContextFromActorSystemBuilder
  with IOExecutionContextBuilder
  with DbBuilder
  with BootstrapDetailsBuilder

