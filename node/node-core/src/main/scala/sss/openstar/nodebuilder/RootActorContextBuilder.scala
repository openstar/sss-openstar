package sss.openstar.nodebuilder

import akka.actor.{Actor, ActorContext, ActorLogging, Props, SupervisorStrategy}
import akka.util.Timeout
import sss.openstar.actor.AllStop
import sss.openstar.common.builders.ActorSystemBuilder

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.reflect.classTag


trait RequireActorContext {

  implicit def actorContext: ActorContext

}

class RootActor extends Actor with ActorLogging {

  override def postStop(): Unit = {

    //Will call the shutdown hooks....
    if(exitAfterStop) {
      log.info("Root Actor stopped, calling system exit...")
      System.exit(1)
    } else {
      log.info("Root Actor stopped.")
    }
  }

  override def preStart(): Unit = {
    log.info("Root Actor starting up...")
    context.system.eventStream
      .subscribe(self, classTag[AllStop.type].runtimeClass)
  }

  private var exitAfterStop = false

  override val supervisorStrategy = SupervisorStrategy.stoppingStrategy


  override def receive: Receive = {
    case AllStop => {
      exitAfterStop = true
      context stop self
    }
    case _ => sender() ! context
  }

}

trait RootActorContextBuilder extends RequireActorContext {
  self: ActorSystemBuilder =>

  import akka.pattern.ask

  lazy implicit val actorContext: ActorContext = {
    implicit val timeOut: Timeout = Timeout(1, SECONDS)
    Await.result(
      actorSystem
        .actorOf(Props(classOf[RootActor])) ? "GIVE ME CONTEXT", 1.second)
      .asInstanceOf[ActorContext]
  }
}
