package sss.openstar.nodebuilder

import sss.openstar.ledger.{Ledger, Ledgers}
import sss.openstar.schemamigration.RequireTablePartitionsCount
import sss.openstar.systemupdates.AutoUpdateConfig

trait MandatoryLedgersLayer extends LedgersBuilder
  with IdentityServiceBuilder
  with OracleLedgerOwnerLedgerBuilder
  with IdentityLedgerBuilder
  with TollLedgerBuilder
  with SendTxBuilder
  with AutoUpdateConfig
  with SystemLedgerBuilder
  with QuorumServiceBuilder
  with QuorumLedgerBuilder
  with RequireTablePartitionsCount
  with BlockFactoriesAndMerklePersisterBuilder
  with GlobalTableNameTagBuilder
  with BlockChainBuilder {

  self: BaseLayer
    with NetworkLayer
    with NodeIdentityLayer =>

  def coreLedgers: Seq[Ledger] = Seq(
    identityLedger,
    quorumLedger,
    oracleLedgerOwnerLedger,
    systemLedger
  )


}
