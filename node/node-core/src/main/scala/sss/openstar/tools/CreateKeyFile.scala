package sss.openstar.tools

import sss.ancillary.FutureOps.AwaitResult
import sss.openstar.common.builders.{IOExecutionContextBuilder, KeyPersisterBuilder}
import sss.ancillary.ByteArrayEncodedStrOps._
import sss.openstar.crypto.keypairs.DefaultAccountKeysFactoryBuilder

object CreateKeyFile extends KeyPersisterBuilder
  with DefaultAccountKeysFactoryBuilder
  with IOExecutionContextBuilder {

  def main(args: Array[String]): Unit = {
    val identity = args(0)
    val tag = args(1)
    val pass = System.getenv("OS_NODE_PHRASE")
    val kpGen = () => defaultAccountKeysFactory.createKeyPair()
    val kp = keyPersister.findOrCreate(identity, tag, pass, kpGen).await()

    println(kp.pub.toBase64Str)
  }
}
