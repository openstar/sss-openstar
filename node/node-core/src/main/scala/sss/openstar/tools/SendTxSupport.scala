package sss.openstar.tools


import akka.actor.{Actor, ActorContext, ActorLogging, Props, ReceiveTimeout}
import sss.ancillary.ByteArrayEncodedStrOps._
import sss.openstar.chains.Chains.GlobalChainIdMask
import sss.openstar.chains.TxWriterActor._
import sss.openstar.ledger.{LedgerItem, SeqLedgerItem, TxApplied}
import sss.openstar.network.MessageEventBus
import sss.openstar.tools.SendTxSupport.{SendTx, TxTracker}

import java.util.concurrent.atomic.AtomicInteger
import scala.concurrent.duration.{Duration, _}
import scala.concurrent.{Future, Promise}
import scala.util.Random

object SendTxSupport {

  private val count = new AtomicInteger(1)

  private case class TxTracker(
                                ledgerItem: SeqLedgerItem,
                                p: Promise[InternalTxResult],
                                retryCount: Int,
                                timeout: Duration,
                                completeWhenWrittenLocally: Boolean
                              )

  trait SendTx extends ((SeqLedgerItem, Int, Duration, Boolean) => Future[InternalTxResult]) {
    def apply(le: LedgerItem): Future[InternalTxResult] = apply(SeqLedgerItem(le), 10, 15.seconds, false)

    def apply(lItems: SeqLedgerItem, completeWhenWrittenLocally: Boolean): Future[InternalTxResult] =
      apply(lItems, 10, 15.seconds, completeWhenWrittenLocally)

    def apply(le: LedgerItem, completeWhenWrittenLocally: Boolean = false): Future[InternalTxResult] =
      apply(SeqLedgerItem(le), 10, 15.seconds, completeWhenWrittenLocally)
  }


  def apply(implicit actorSystem: ActorContext,
            chainId: GlobalChainIdMask,
            messageEventBus: MessageEventBus): SendTx = {
    new SendTxSupport()
  }
}

class SendTxSupport(implicit actorSystem: ActorContext,
                    chainId: GlobalChainIdMask,
                    messageEventBus: MessageEventBus) extends SendTx {

  private val ref = actorSystem.actorOf(Props(SendTxActor),
    s"SendTxActor_${chainId}_${SendTxSupport.count.getAndIncrement()}")

  override def apply(le: SeqLedgerItem,
                     retryCount: Int,
                     timeout: Duration,
                     completeWhenWrittenLocally: Boolean): Future[InternalTxResult] = {
    val p = Promise[InternalTxResult]()

    ref ! TxTracker(le, p, retryCount, timeout, completeWhenWrittenLocally)
    p.future
  }

  private object SendTxActor extends Actor with ActorLogging {

    override def receive = {

      case TxTracker(le, p, retryCount, timeout, completeWhenWrittenLocally) =>
        context.actorOf(
          Props(
            classOf[TxTrackerActor],
            le,
            p,
            retryCount,
            timeout,
            completeWhenWrittenLocally,
            chainId,
            messageEventBus
          ),
          s"TxTrackerActor_${chainId}_${le.txId.toBase64Str}_RND${Random.nextLong()}"
        )

    }
  }

}

private class TxTrackerActor(
                              le: SeqLedgerItem,
                              result: Promise[InternalTxResult],
                              var retryCount: Int,
                              timeout: Duration,
                              completeWhenWrittenLocally: Boolean,
                              chainId: GlobalChainIdMask,
                              events: MessageEventBus) extends Actor with ActorLogging {

  context.setReceiveTimeout(timeout)


  override def preStart(): Unit = {
    super.preStart()
    //this is useful on nodes that must wait for
    //sync before proceeding.
    if (completeWhenWrittenLocally) {
      events subscribe classOf[TxApplied]
    }
  }

  private var txAppliedOpt: Option[TxApplied] = None
  private var txGoodResultOpt: Option[InternalCommit] = None
  private var txStartTime: Option[Long] = Some(System.currentTimeMillis())

  val item = InternalLedgerItem(chainId, le, Some(self))
  self ! item

  override def unhandled(message: Any): Unit =
    log.error("Tx Tracker received unexpected message {}", message)

  override def receive: Receive = {

    case txApplied: TxApplied if txApplied.txId.sameElements(le.txId) && txGoodResultOpt.isDefined =>
      result.success(txGoodResultOpt.get)
      context stop self

    case txApplied: TxApplied =>
      val found = txApplied.txId sameElements le.txId
      if (found) {
        txAppliedOpt = Some(txApplied)
      }

    case ReceiveTimeout =>
      result.failure(
        new RuntimeException(
          s"${self.path.name} received no response in $timeout, " +
            s"txAppliedOpt $txAppliedOpt t" +
            s"xResult $txGoodResultOpt, " +
            s"retries left $retryCount")
      )

      context stop self

    case item: InternalLedgerItem =>
      events.publish(item)

    case _: InternalAck =>
      log.debug(s"${self.path.name} GOT ACK")
    //that's great, but ignore until it commits or rolls back.

    case InternalTempNack(_, msg) if retryCount > 0 =>
      import context.dispatcher
      log.debug(s"InternalTempNack (retries left:$retryCount)  ${msg.toString}")
      retryCount -= 1
      context.system.scheduler.scheduleOnce(10.milliseconds, self, item)


    case noMoreRetries: InternalTempNack =>
      result.success(noMoreRetries)
      log.info(s"got nack, no more retry after ${txStartTime.map(System.currentTimeMillis() - _)} ms")
      context stop self

    case txBadResult: InternalNack =>
      log.debug("{}", txBadResult)
      result.success(txBadResult)
      log.info(s"internal nack after ${txStartTime.map(System.currentTimeMillis() - _)} ms")
      context stop self

    case txResult: InternalCommit if txAppliedOpt.isDefined =>
      result.success(txResult)
      log.info(s"success tx applied after ${txStartTime.map(System.currentTimeMillis() - _)} ms")
      context stop self

    case txResult: InternalCommit if !completeWhenWrittenLocally =>
      result.success(txResult)
      log.info(s"success InternalCommit after ${txStartTime.map(System.currentTimeMillis() - _)} ms")
      context stop self

    case txResult: InternalCommit =>
      txGoodResultOpt = Some(txResult)
  }

}
