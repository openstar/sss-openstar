package sss.openstar.tools

import sss.ancillary.Logging
import sss.openstar.network.NodeId
import sss.openstar.peers.Discovery.DiscoveredNode

import java.net.InetSocketAddress
import scala.util.{Failure, Success, Try}

object DownloadSeedNodes extends Logging {

  val delim = ":"

  private def toInetSocketAddress(hostName: String, port: String): InetSocketAddress =
    new InetSocketAddress(hostName, port.toInt)

  def discoveredNodeToString(discoveredNode: DiscoveredNode): String = {
    (discoveredNode.nodeId.id ::
      discoveredNode.nodeId.inetSocketAddress.getHostString ::
      discoveredNode.nodeId.inetSocketAddress.getPort.toString ::
      discoveredNode.capabilities.toString ::
      Nil).mkString(delim)
  }

  def toDiscoveredNode(attrDownloadResult: Try[String]): Set[DiscoveredNode] = {

    attrDownloadResult map {
      _.split(delim).grouped(4)
        .map (ary => Try(DiscoveredNode(NodeId(ary(0), toInetSocketAddress(ary(1), ary(2))), ary(3).toByte)))
        .collect { case Success(n) => n }
        .toSet
    } match {
      case Success(s) =>
        s
      case Failure(e) =>
        log.warn(e.toString)
        Set.empty
    }

  }

}
