package sss.openstar.http

import akka.actor.ActorSystem
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.http.scaladsl.{ConnectionContext, Http, HttpsConnectionContext, ServerBuilder}
import com.typesafe.config.Config
import sss.ancillary.Logging
import sss.openstar.http.RpcServer.{Bindings, HostPort, RpcServerConfig, RpcTlsConfig}

import java.io.{File, FileInputStream, InputStream}
import java.security.{KeyStore, SecureRandom}
import javax.net.ssl._
import scala.concurrent.duration.FiniteDuration
import scala.concurrent.{ExecutionContext, Future}

object RpcServer {

  case class Bindings(
    http: Future[Http.ServerBinding],
    https: Option[Future[Http.ServerBinding]]
  )

  case class HostPort(host: String, port: Int)

  case class RpcTlsConfig(hostPort: HostPort, keyStorePass: String, keyStore: String)

  case class RpcServerConfig(
    hostPort: HostPort = HostPort("127.0.0.1", 2000),
    tls: Option[RpcTlsConfig] = None
  )

  object RpcServerConfig {

    def from(cfg: Config): RpcServerConfig = {
      def getHostPort(c: Config): HostPort =
        HostPort(c.getString("host"), c.getInt("port"))

      RpcServerConfig(
        hostPort = getHostPort(cfg),
        tls = Option.when(cfg.hasPath("tls")) {
          val tls = cfg.getConfig("tls")

          RpcTlsConfig(
            hostPort = getHostPort(tls),
            keyStorePass = tls.getString("keyStorePass"),
            keyStore = tls.getString("keyStore"),
          )
        }
      )
    }
  }

}

class RpcServer(
  config: RpcServerConfig,
  httpRoute: PartialFunction[HttpRequest, Future[HttpResponse]],
  tlsRoute: PartialFunction[HttpRequest, Future[HttpResponse]]
)(implicit actorSystem: ActorSystem, ec: ExecutionContext) extends Logging {

  private def http2Server(hostPort: HostPort): ServerBuilder = {
    Http()
      .newServerAt(interface = hostPort.host, port = hostPort.port)
      .adaptSettings(s => s.withPreviewServerSettings(s.previewServerSettings.withEnableHttp2(true)))
  }

  private lazy val httpBinding: Future[Http.ServerBinding] = {
    http2Server(config.hostPort).bind(httpRoute)
  }

  private lazy val httpsBindingOpt: Option[Future[Http.ServerBinding]] = {
    config.tls.map { tlsConfig =>
      http2Server(tlsConfig.hostPort)
        .enableHttps(createHttpsContext(tlsConfig))
        .bind(tlsRoute)
    }
  }

  def start(): Bindings = {
    httpsBindingOpt.foreach(_.foreach { binding =>
      log.info(s"gRPC server bound (TLS) to: ${binding.localAddress}")
    })

    httpBinding.foreach { binding =>
      log.info(s"gRPC server bound to: ${binding.localAddress}")
    }

    Bindings(httpBinding, httpsBindingOpt)
  }

  def stop(maxWait: FiniteDuration): Future[List[Http.HttpTerminated]] = {
    import cats.implicits._

    for {
      httpsTerm <- httpsBindingOpt.traverse(_.flatMap(_.terminate(maxWait)))
      httpTerm  <- httpBinding.flatMap(_.terminate(maxWait))
    } yield httpTerm :: httpsTerm.toList
  }

  private def createHttpsContext(tlsConfig: RpcTlsConfig): HttpsConnectionContext = {
    import scala.util.chaining._

    val password: Array[Char] = tlsConfig.keyStorePass.toCharArray // do not store passwords in code, read them from somewhere safe!

    val keystoreInputStream: InputStream = {
      Option(this.getClass.getClassLoader.getResourceAsStream(tlsConfig.keyStore)).getOrElse {
        log.warn(s"Failed to load ${tlsConfig.keyStore} as stream, try as file")
        new FileInputStream(new File(tlsConfig.keyStore))
      }
    }

    val ks: KeyStore =
      KeyStore
        .getInstance("PKCS12")
        .tap(_.load(keystoreInputStream, password))

    val keyManagers: Array[KeyManager] =
      KeyManagerFactory
        .getInstance("SunX509")
        .tap(_.init(ks, password))
        .getKeyManagers

    val trustManagers: Array[TrustManager] =
      TrustManagerFactory
        .getInstance("SunX509")
        .tap(_.init(ks))
        .getTrustManagers

    val sslContext: SSLContext =
      SSLContext
        .getInstance("TLS")
        .tap(_.init(keyManagers, trustManagers, new SecureRandom))

    ConnectionContext.httpsServer(sslContext)
  }
}

