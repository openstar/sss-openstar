package sss.openstar


import sss.ancillary.Logging
import sss.openstar.network.MessageEventBus.BusEventClass

import scala.concurrent.duration.{Duration, FiniteDuration}
import scala.reflect.runtime.universe._

package object stateactor {


  val NoActions = Seq.empty[Action]

  sealed trait Action {

    val isEmpty: Boolean = false

    protected def shimCopy(actionOpt: Option[Action]): Action

    protected val next: Option[Action]

    private def append(other: Action): Action = {
      if (next.isDefined) shimCopy(Option(next.get.append(other)))
      else if (other.isEmpty) this
      else shimCopy(Option(other))
    }

    def asSeq: Actions = {
      if (next.isDefined) shimCopy(None) +: next.get.asSeq
      else if (isEmpty) Seq.empty
      else Seq(this)
    }

    def +(other: Action): Action = append(other)

  }

  case object NoAction extends Action {

    override val isEmpty: Boolean = true

    override protected def shimCopy(actionOpt: Option[Action]): Action = actionOpt match {
      case None => this
      case Some(action) => action
    }

    override protected val next: Option[Action] = None
  }

  type Actions = Seq[Action]

  case class PublishedEvent[A](value: BusEvent) extends Event


  case class Schedule(duration: FiniteDuration, a: Event, next: Option[Action] = None) extends Action {
    override def shimCopy(actionOpt: Option[Action]): Action = this.copy(next = actionOpt)
  }

  case class SetReceiveTimeout(duration: Duration, next: Option[Action] = None) extends Action {
    override def shimCopy(actionOpt: Option[Action]): Action = this.copy(next = actionOpt)
  }

  case class ToDisconnect(nodeId: UniqueNodeIdentifier, next: Option[Action] = None) extends Action {
    override def shimCopy(actionOpt: Option[Action]): Action = this.copy(next = actionOpt)
  }

  case class ToUnSubscribeCode(value: Seq[Byte], next: Option[Action] = None) extends Action {
    override def shimCopy(actionOpt: Option[Action]): Action = this.copy(next = actionOpt)
  }

  case class ToSubscribeCode(value: Seq[Byte], next: Option[Action] = None) extends Action {
    override def shimCopy(actionOpt: Option[Action]): Action = this.copy(next = actionOpt)
  }

  case class ToUnSubscribeClass(value: Seq[BusEventClass], next: Option[Action] = None) extends Action {
    override def shimCopy(actionOpt: Option[Action]): Action = this.copy(next = actionOpt)
  }

  case class ToSubscribeClass(value: Seq[BusEventClass], next: Option[Action] = None) extends Action {
    override def shimCopy(actionOpt: Option[Action]): Action = this.copy(next = actionOpt)
  }

  case class ToSend[A: TypeTag](code: Byte, a: A, to: Set[UniqueNodeIdentifier], next: Option[Action] = None) extends Action {
    override def shimCopy(actionOpt: Option[Action]): Action = this.copy(next = actionOpt)
  }

  case class ToPublish(value: BusEvent, next: Option[Action] = None) extends Action {
    override def shimCopy(actionOpt: Option[Action]): Action = this.copy(next = actionOpt)
  }



  trait State[A] extends Logging {

    type Handler = PartialFunction[Event, (State[A], Action)]

    def apply(e: Event): (_ <: State[A], Action) = (handle orElse defaultHanlder)(e)
    val NoChange: (State[A], Action) = (this, NoAction)
    def onlyAction(action: Action): (State[A], Action) = (this, action)

    def handle: Handler = defaultHanlder

    val defaultHanlder: Handler = {
      case e =>
        log.debug(s"state not handling ${e.toString}")
        (this, NoAction)
    }
  }


  implicit class OpenstarEventOps[A](p: BusEvent) {
    def toPublish: Action = ToPublish(p)
  }

  implicit class StateOps[A](p: (_ <: State[A], Action)) {
    def apply(e: Event): (_ <: State[A], Action) = {
      val (s: State[A], es) = p._1.apply(e)
      (s, p._2 + es)
    }
  }

}
