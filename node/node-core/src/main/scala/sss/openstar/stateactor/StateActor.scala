package sss.openstar.stateactor

import akka.actor.{Actor, ActorContext, ActorLogging, ActorRef, Props, ReceiveTimeout}
import sss.openstar.actor.SystemPanic
import sss.openstar.chains.Chains.GlobalChainIdMask
import sss.openstar.controller.Send
import sss.openstar.network.MessageEventBus
import sss.openstar.network.MessageEventBus._
import sss.openstar.stateactor.StateActor.{Disconnect, ReceiveTimeoutEvent}
import sss.openstar.{Event, UniqueNodeIdentifier}


object StateActor {

  case object ReceiveTimeoutEvent extends Event

  type Disconnect = UniqueNodeIdentifier => Unit

  def apply[A](
                initialState: State[A],
                name: String,
                classEventsToSubscribeTo: Seq[Class[_]] = Seq.empty,
                codeEventsToSubscribeTo: Seq[Byte] = Seq.empty)(implicit actorSystem: ActorContext, eventMessageBus: MessageEventBus,
                                                                send: Send,
                                                                disconnect: Disconnect,
                                                                chainIdMask: GlobalChainIdMask): ActorRef = {
    actorSystem.actorOf(
      Props(
        classOf[StateActor[A]],
        initialState,
        classEventsToSubscribeTo,
        codeEventsToSubscribeTo,
        eventMessageBus,
        send,
        disconnect,
        chainIdMask
      ), name
    )
  }
}

class StateActor[A](
                     protected var state: State[A],
                     classEventsToSubscribeTo: Seq[BusEventClass],
                     codeEventsToSubscribeTo: Seq[Byte],
                   )(implicit eventMessageBus: MessageEventBus,
                     send: Send,
                     disconnect: Disconnect,
                     chainIdMask: GlobalChainIdMask
                   )
  extends Actor
    with ActorLogging
    with SystemPanic
{

  log.debug("a StateActor Starts up ")

  override def preStart(): Unit = {
    super.preStart()
    classEventsToSubscribeTo.subscribe
    codeEventsToSubscribeTo.subscribe

  }

  override def receive: Receive = {

    case ReceiveTimeout =>
      self !  ReceiveTimeoutEvent

    case e: Event =>

      val (newState, action: Action) = state(e)
      val actions = action.asSeq
      state = newState

      log.debug(s"state ${self.path.name} now $newState after event $e, next actions $actions")

      actions.foreach {
        case ToPublish(p, _) => eventMessageBus.publish(p)
        case ToSend(code, a, tos, _) => send(code, a, tos)
        case ToSubscribeClass(classes, _) => classes.subscribe
        case ToSubscribeCode(codes, _) => codes.subscribe
        case ToUnSubscribeClass(classes, _) => classes.unsubscribe
        case ToUnSubscribeCode(codes, _) => codes.unsubscribe
        case ToDisconnect(nId, _) => disconnect(nId)
        case SetReceiveTimeout(d, _) => context.setReceiveTimeout(d)
        case Schedule(d, a, _) =>
          import context.dispatcher
          context.system.scheduler.scheduleOnce(d, self, a)

        case NoAction =>

      }

  }

  override def unhandled(message: Any): Unit = log.warning(s"Unhandled in StateActor: {}", message)
}
