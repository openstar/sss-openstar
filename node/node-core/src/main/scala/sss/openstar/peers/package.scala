package sss.openstar

import java.net.{InetAddress, InetSocketAddress}

import akka.util.ByteString
import sss.ancillary.SeqSerializer
import sss.ancillary.Serialize._
import sss.openstar.chains.Chains.GlobalChainIdMask
import sss.openstar.peers.serialize._
import sss.openstar.util.IntBitSet

package object peers {

  case class PeerPage(start: Int, end: Int, fingerprint: ByteString)
  case class PeerPageResponse(nodeId: String, sockAddr:InetSocketAddress, capabilities: Capabilities)

  case class SeqPeerPageResponse(val value: Seq[PeerPageResponse]) extends AnyVal

  implicit class SeqPeerPageResponseToBytes(v: SeqPeerPageResponse) extends ToBytes {
    override def toBytes: Array[Byte] = SeqSerializer.toBytes(v.value map (_.toBytes))
  }

  implicit class SeqPeerPageResponseFromBytes(bytes: Array[Byte]) {
    def toSeqPeerPageResponse: SeqPeerPageResponse =
      SeqPeerPageResponse(
        SeqSerializer.fromBytes(bytes) map (_.toPeerPageResponse)
      )
  }

  case class Capabilities(supportedChains: GlobalChainIdMask) {
    def contains(chainIdMask: GlobalChainIdMask): Boolean = {
      IntBitSet(supportedChains).contains(chainIdMask)
    }
  }

  implicit class CapabilitiesToBytes(c: Capabilities) extends ToBytes {

    override def toBytes: Array[GlobalChainIdMask] =
      CapabilitiesSerializer.toBytes(c)

  }

  implicit class CapabilitiesFromBytes(val bs: Array[Byte]) extends AnyVal {
    def toCapabilities: Capabilities = CapabilitiesSerializer.fromBytes(bs)
  }

  implicit class PeerPageToBytes(val p:PeerPage) extends ToBytes {
    override def toBytes: Array[Byte] = PeerPageSerializer.toBytes(p)
  }

  implicit class PeerPageFromBytes(val bs: Array[Byte])  {
    def toPeerPage: PeerPage = PeerPageSerializer.fromBytes(bs)
  }

  implicit class PeerPageResponseToBytes(val p:PeerPageResponse) extends ToBytes {
    override def toBytes: Array[Byte] = PeerPageResponseSerializer.toBytes(p)
  }

  implicit class PeerPageResponseFromBytes(val bs: Array[Byte])  {
    def toPeerPageResponse: PeerPageResponse = PeerPageResponseSerializer.fromBytes(bs)
  }

}
