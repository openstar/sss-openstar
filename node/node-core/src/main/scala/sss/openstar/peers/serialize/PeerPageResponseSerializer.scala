package sss.openstar.peers.serialize

import sss.ancillary.SeqSerializer
import sss.ancillary.Serialize._
import sss.openstar.peers._
import sss.openstar.util.SerializeInetAddress._

object PeerPageResponseSerializer extends Serializer[PeerPageResponse] {

  override def toBytes(p: PeerPageResponse): Array[Byte] = {
    StringSerializer(p.nodeId) ++
      ByteArraySerializer(p.sockAddr.toBytes) ++
      ByteArraySerializer(p.capabilities.toBytes)
        .toBytes
  }

  override def fromBytes(bs: Array[Byte]): PeerPageResponse = {
    PeerPageResponse.tupled (bs.extract(
      StringDeSerialize,
      ByteArrayDeSerialize(_.toInetSocketAddress),
      ByteArrayDeSerialize(_.toCapabilities)
    ))

  }
}

object SeqPeerPageResponseSerializer extends Serializer[SeqPeerPageResponse] {
  override def toBytes(t: SeqPeerPageResponse): Array[Byte] =
    SeqSerializer.toBytes(t.value map (_.toBytes))

  override def fromBytes(bytes: Array[Byte]): SeqPeerPageResponse =
    SeqPeerPageResponse(
      SeqSerializer.fromBytes(bytes) map (_.toPeerPageResponse)
    )
}
