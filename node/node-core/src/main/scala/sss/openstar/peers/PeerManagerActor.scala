package sss.openstar.peers

import java.net.InetSocketAddress

import akka.actor.{Actor, ActorLogging, Cancellable}
import sss.ancillary.Restarter.AttemptRestart
import sss.openstar.actor.SystemPanic
import sss.openstar.chains.Chains.GlobalChainIdMask
import sss.openstar.network.MessageEventBus.IncomingMessage
import sss.openstar.network.{MessageEventBus, _}
import sss.openstar.peers.Discovery.DiscoveredNode
import sss.openstar.peers.DiscoveryActor.Discover
import sss.openstar.peers.PeerManager._
import sss.openstar.peers.serialize.CapabilitiesSerializer
import sss.openstar.util.LogOnChange
import sss.openstar.{MessageKeys, UniqueNodeIdentifier}

import scala.concurrent.duration.FiniteDuration

private class PeerManagerActor(connect: NetConnect,
                               send: NetSend,
                               ourCapabilities: Capabilities,
                               discoveryInterval: FiniteDuration,
                               discovery: Discovery,
                               thisNodesId: UniqueNodeIdentifier
                              )
                              (implicit events: MessageEventBus)
  extends Actor with ActorLogging with SystemPanic {

  private case class KnownConnection(c: Connection, cabs: Capabilities)

  private case object WakeUp

  private val ourCapabilitiesNetworkMsg: SerializedMessage =
    SerializedMessage(MessageKeys.Capabilities, ourCapabilities)(CapabilitiesSerializer.toBytes, SerializedMessage.noChain)

  private var wakeTimer: Option[Cancellable] = None

  // below this number of connections each new connection is asked for it's connections
  private val minSizeOfDiscoverPool = 10

  private val skippedWakeUpIntervalsPlusOne = 3
  private var resetFailCount: Int = skippedWakeUpIntervalsPlusOne
  private var queries: Set[Query] = Set()

  private var knownConns: Map[UniqueNodeIdentifier, Capabilities] = Map()
  private var failedConns: Seq[InetSocketAddress] = Seq.empty
  private var reverseNodeInetAddrLookup: Seq[NodeId] = Seq.empty

  log.info(s"This node is $thisNodesId")

  val logInfoConnections = new LogOnChange[Set[String]](conns =>
    log.info(
      s"""Known conns in PeerManager
         | ${conns}
         | """.stripMargin)
  )

  override def preStart(): Unit = {
    super.preStart()
    events.subscribe(MessageKeys.Capabilities)
    events.subscribe(MessageKeys.QueryCapabilities)
    events.subscribe(classOf[ConnectionLost])
    events.subscribe(classOf[Connection])
    events.subscribe(classOf[ConnectionFailed])
    events.subscribe(classOf[ConnectionHandshakeTimeout])
    events.subscribe(classOf[UnQuery])
    events.subscribe(classOf[AddQuery])

  }

  private def minusThisNode(nodes: Seq[DiscoveredNode]): Seq[DiscoveredNode] = {
    nodes filterNot (_.nodeId.id == thisNodesId)
  }

  private def matchWithCapabilities(nodeId: UniqueNodeIdentifier,
                                    otherNodesCaps: Capabilities)(q: Query): Boolean = {
    q match {
      case ChainQuery(chainId: GlobalChainIdMask, _) =>
        otherNodesCaps.contains(chainId)

      case IdQuery(ids: Set[String]) =>
        ids contains nodeId

      case _ => false
    }
  }




  override def receive: Receive = {

    case WakeUp =>

      resetFailCount -= 1
      if (resetFailCount == 0) {
        //Recycle failed conns to give them a chance to come up.
        failedConns = Seq.empty
        resetFailCount = skippedWakeUpIntervalsPlusOne
      }

      queries foreach (self ! _)
      import context.dispatcher
      wakeTimer map (_.cancel())
      wakeTimer = Option(context.system.scheduler.scheduleOnce(discoveryInterval, self, WakeUp))
      events publish Discover(knownConns.keys.toSet)

    case UnQuery(q@IdQuery(ids)) =>
      queries -= q

      ids foreach (n => {
        log.debug("UnQuery disconnecting {}", n)
        connect.disconnect(n)
      })

    case UnQuery(q) => queries -= q

    case AddQuery(q) =>
      queries += q
      self ! WakeUp

    case q@IdQuery(ids) if ids.isEmpty =>
      queries -= q

    case q@IdQuery(ids) =>
      val notAlreadyConnected = ids diff knownConns.keySet
      val nodeIds = discovery.lookup(notAlreadyConnected)
      minusThisNode(nodeIds) foreach (n => connect(n.nodeId, defaultReconnectionStrategy))

      logInfoConnections(knownConns.keys.map(_.toString).toSet)

      ids foreach (id => knownConns.get(id) foreach { c =>
        val pc = PeerConnection(id, c)
        events.publish(pc)
      })

    case q@ChainQuery(cId, numRequestedConns) =>
      val goodConns = knownConns.filter {
        case (nId, caps) => matchWithCapabilities(nId, caps)(q)
      }.keySet

      if (goodConns.size < numRequestedConns) {
        val allIgnoredConns = goodConns ++
          reverseNodeInetAddrLookup.filter(ni => failedConns.map(_.getAddress).contains(ni.address)).map(_.id)

        val newNodes = discovery.find(numRequestedConns, cId, allIgnoredConns)
        minusThisNode(newNodes) foreach { n =>
          connect(n.nodeId, defaultReconnectionStrategy)
          reverseNodeInetAddrLookup = n.nodeId +: reverseNodeInetAddrLookup
        }
      }


    case Connection(nodeId) =>
      failedConns = reverseNodeInetAddrLookup.find(_.id == nodeId)
        .map(found => failedConns.filterNot(_ == found.inetSocketAddress))
        .getOrElse(failedConns)

      log.debug(s"QueryCapabilities of $nodeId")
      send(SerializedMessage(0.toByte, MessageKeys.QueryCapabilities, Array.emptyByteArray), Set(nodeId))

    case ConnectionLost(lost) =>
      knownConns -= lost

    case ConnectionFailed(remote, _) =>
      failedConns = remote +: failedConns
      discovery.unreachable(remote)

    case IncomingMessage(_, MessageKeys.QueryCapabilities, nodeId, _) =>
      // TODO if spamming, blacklist
      log.debug(s"Sending $nodeId our capabilities.")
      send(ourCapabilitiesNetworkMsg, Set(nodeId))

    case IncomingMessage(_, MessageKeys.Capabilities, nodeId, otherNodesCaps: Capabilities) =>

      val alreadyKnown = knownConns.get(nodeId)

      log.debug(s"$nodeId has returned capabilities.")
      knownConns += nodeId -> otherNodesCaps

      reverseNodeInetAddrLookup.find(_.id == nodeId) foreach { found =>
        discovery.reachable(found.inetSocketAddress)
      }

      if (queries.exists(matchWithCapabilities(nodeId, otherNodesCaps))) {
        events.publish(PeerConnection(nodeId, otherNodesCaps))
      }

      alreadyKnown match {
        case None if (knownConns.size < minSizeOfDiscoverPool) =>
          //discover from this now
          events publish Discover(Set(nodeId))
        case _ =>
      }
  }
}
