package sss.openstar.peers


import akka.actor.{ActorContext, Props}
import sss.ancillary.Logging
import sss.openstar.chains.Chains.GlobalChainIdMask
import sss.openstar.network.{MessageEventBus, _}
import sss.openstar.peers.Discovery.DiscoveredNode
import sss.openstar.peers.PeerManager.{AddQuery, Query, UnQuery}
import sss.openstar.{BusEvent, UniqueNodeIdentifier}

import scala.concurrent.duration.FiniteDuration


trait PeerQuery {
  def addQuery(q:Query): Unit
  def removeQuery(q:Query): Unit
}

object PeerManager {

  case class PeerConnection(nodeId: UniqueNodeIdentifier, c: Capabilities) extends BusEvent
  case class UnQuery(q: Query) extends BusEvent
  case class AddQuery(q: Query) extends BusEvent

  trait Query
  case class ChainQuery(chainId: GlobalChainIdMask, numConns: Int) extends Query
  case class IdQuery(ids: Set[UniqueNodeIdentifier]) extends Query


}

class PeerManager(connect: NetConnect,
                  send: NetSend,
                  bootstrapNodes: Set[DiscoveredNode],
                  discoveryInterval: FiniteDuration,
                  discovery: Discovery,
                  thisNode: DiscoveredNode
                  )
                 (implicit actorSystem: ActorContext,
                  events: MessageEventBus
                 ) extends PeerQuery with Logging {



  discovery.persist(thisNode.nodeId, thisNode.capabilities) recover {
    case e => log.error(e.toString)
  }

  log.info(s"Our address to the world is ${thisNode.nodeId.inetSocketAddress} with capabilities ${Capabilities(thisNode.capabilities)}")

  bootstrapNodes foreach { dn =>
    discovery.persist(dn.nodeId, dn.capabilities) recover {
      case e => log.error(e.toString)
    }
  }

  log.whenDebugEnabled(discovery.query(0, 10)._1.foreach(discovered => log.debug(discovered.toString)))

  override def addQuery(q: Query): Unit = {
    ref ! AddQuery(q)
  }

  override def removeQuery(q: Query): Unit = ref ! UnQuery(q)

  private val ref = actorSystem.actorOf(Props(classOf[PeerManagerActor],
    connect,
    send,
    Capabilities(thisNode.capabilities),
    discoveryInterval,
    discovery,
    thisNode.nodeId.id,
    events
    ), "PeerManagerActor")

}

