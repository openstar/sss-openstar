package sss.openstar.peers

import java.net.InetSocketAddress
import akka.util.ByteString
import sss.ancillary.Logging
import sss.db.NullOrder._
import sss.db._
import sss.openstar.chains.Chains.GlobalChainIdMask
import sss.openstar.network.NodeId
import sss.openstar.peers.Discovery.{DiscoveredNode, Hash}
import sss.openstar.{BusEvent, UniqueNodeIdentifier}
import sss.openstar.util.SerializeInetAddress._

import scala.util.Try
import sss.openstar.schemamigration.SqlSchemaNames.ColumnNames._
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.schemamigration.SqlSchemaNames.TableNames.discoveryTableName
import java.util.Date
import java.util.TimeZone

object Discovery {

  private val peerPattern = """(.*):(.*):(\d\d\d\d):([0-9]{1,3})""".r

  def toDiscoveredNode(pattern: String): DiscoveredNode = pattern match {
    case peerPattern(id, ip, port, caps) =>
      DiscoveredNode(NodeId(id, new InetSocketAddress(ip, port.toInt)), caps.toByte)
  }

  type Hash = Array[Byte] => Array[Byte]

  case class DiscoveredNode(nodeId: NodeId, capabilities: GlobalChainIdMask) extends BusEvent {
    lazy val hash: ByteString = nodeId.hash ++ ByteString(capabilities)
  }

}

class Discovery(hasher: Hash)(implicit db: Db) extends Logging {

  private lazy val discoveryTable = db.table(discoveryTableName)
  TimeZone.setDefault( TimeZone.getTimeZone("UTC"))

  def find(numConns: Int, caps: GlobalChainIdMask, nodesToIgnore: Set[UniqueNodeIdentifier] = Set.empty): Seq[DiscoveredNode] = {
    val whereNotInNodesToIgnore =
      if (nodesToIgnore.nonEmpty) where(nIdCol) notIn nodesToIgnore
      else where()

    viewToDiscoveredNodes(
      where(capCol -> caps) and whereNotInNodesToIgnore limit numConns
    )
  }

  def lookup(names: Set[UniqueNodeIdentifier]): Seq[DiscoveredNode] = {

    if (names.nonEmpty) {
      viewToDiscoveredNodes(
        where(nIdCol) in names
      )
    } else Seq.empty

  }

  private def viewToDiscoveredNodes(filter: Where): Seq[DiscoveredNode] = {
    discoveryTable
      .map(
        rowToDiscoveredNode, filter
      )
  }.dbRunSyncGet

  private def rowToDiscoveredNode(r: Row): DiscoveredNode = {
    val inet = r.arrayByte(addrCol).toInetAddress
    val sock = new InetSocketAddress(inet, r.int(portCol))

    DiscoveredNode(NodeId(r.string(nIdCol), sock), r.byte(capCol))
  }

  private[peers] def purge(supportedChains: GlobalChainIdMask): Int = {
    discoveryTable delete where(capCol -> supportedChains)
  }.dbRunSyncGet


  private[peers] def persist(nodeId: NodeId, supportedChains: GlobalChainIdMask): Try[DiscoveredNode] = {

    val nId = nodeId.id
    val addr = nodeId.inetSocketAddress.getAddress.toBytes
    val port = nodeId.inetSocketAddress.getPort

    val columnMap = Map(
      nIdCol -> nId,
      addrCol -> addr,
      portCol -> port,
      capCol -> supportedChains)

    for {
    _ <- discoveryTable delete
        where(s"($nIdCol = ?) OR ($addrCol = ? AND $portCol = ?)", nId, addr, port)

      row <- discoveryTable insert columnMap

    } yield(rowToDiscoveredNode(row))
  }.dbRunSync

  def query(start: Int, pageSize: Int): (Seq[DiscoveredNode], ByteString) = {
    val nodes =
      viewToDiscoveredNodes(
        where(s"$idCol > ?", start)
          .orderAsc(addrCol, portCol)
          .limit(pageSize)
      )
    val hashed = nodes.foldLeft(Array.emptyByteArray)((acc, e) => hasher(acc ++ e.hash))
    (nodes, ByteString(hashed))
  }

  import sss.db.ops.DbOps.OptFutureTxOps

  def reachable(addr: InetSocketAddress): Unit = (for {
    rowOpt <- discoveryTable.find(
      where(
        addrCol -> addr.getAddress.toBytes,
        portCol -> addr.getPort
      )
    )
    _ <- rowOpt.map(r =>
      discoveryTable.updateRow(
        Map(idCol -> r.id, unreachableCol -> None)
      )
    ).toFutureTxOpt
  } yield ()).dbRunSyncGet


  def unreachable(addr: InetSocketAddress): Unit = (for {

    rowOpt <- discoveryTable.find(
      where(
        addrCol -> addr.getAddress.toBytes,
        portCol -> addr.getPort
      )
    )

    _ <- rowOpt match {
      case None =>
        log.debug("Addr {} is being marked as unreachable, but it's not in the discoveray table", addr)
        FutureTx.unit(())
      case Some(row) if row.longOpt(unreachableCol).isDefined => FutureTx.unit(())
      case _ => discoveryTable.updateRow(
        Map(
          idCol -> rowOpt.get.id,
          unreachableCol -> new Date()
        )
      )
    }

  } yield ()).dbRunSyncGet

  def prune(maxDbRows: Long): Int = (for {
    total <- discoveryTable.count

    result <- if (total > maxDbRows) {

      val maxRowstoDelete = total - maxDbRows

      for {
        idRows <- discoveryTable filter (
          where()
            orderBy OrderAsc(unreachableCol, NullsLast)
            limit maxRowstoDelete.toInt
          )
        ids = idRows.map(_.id)
        num <- discoveryTable delete {
          where(idCol) in ids.toSet
        }
      } yield num

    } else FutureTx.unit(0)

  } yield result).dbRunSyncGet

}
