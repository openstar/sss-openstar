package sss.openstar.peers.serialize

import sss.ancillary.Serialize._
import sss.openstar.peers.Capabilities

object CapabilitiesSerializer extends Serializer[Capabilities]{

  override def toBytes(t: Capabilities): Array[Byte] =
    ByteSerializer(t.supportedChains).toBytes

  override def fromBytes(b: Array[Byte]): Capabilities =
    Capabilities(b.extract(ByteDeSerialize))

}
