package sss.openstar.block.serialize

import sss.ancillary.Serialize._
import sss.openstar.block.VoteLeader

/**
  * Copyright Stepping Stone Software Ltd. 2016, all rights reserved. 
  * mcsherrylabs on 3/3/16.
  */
object VoteLeaderSerializer extends Serializer[VoteLeader]{

  override def toBytes(l: VoteLeader): Array[Byte] =
    StringSerializer(l.nodeId)
        .toBytes

  override def fromBytes(b: Array[Byte]): VoteLeader =
    VoteLeader(
      b.extract(
        StringDeSerialize
      )
    )

}
