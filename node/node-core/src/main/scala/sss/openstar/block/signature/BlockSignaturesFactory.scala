package sss.openstar.block.signature

import sss.db.Db
import sss.openstar.db.BlockHeightToTablePartitionId.BlockHeightToTablePartitionId
import sss.openstar.schemamigration.dynamic.{BlockMigration, GlobalTableNameTag}

trait BlockSignaturesFactory {
  def getQuorumSigs(height: Long)(implicit db: Db): BlockSignatures

  def getNonQuorumSigs(height: Long)(implicit db: Db): BlockSignatures
}

object BlockSignaturesFactory {

  def usingPartitionedTable(
      blockHeightToTablePartitionId: BlockHeightToTablePartitionId
  )(implicit globalTableNameTag: GlobalTableNameTag): BlockSignaturesFactory = new BlockSignaturesFactory {
    override def getQuorumSigs(height: Long)(implicit db: Db): BlockSignatures =
      getSigs(BlockMigration.blockSigsQuorumTableName(blockHeightToTablePartitionId(height)), height)

    override def getNonQuorumSigs(height: Long)(implicit db: Db): BlockSignatures =
      getSigs(BlockMigration.blockSigsNonQuorumTableName(blockHeightToTablePartitionId(height)), height)

    private def getSigs(tableName: String, height: Long)(implicit db: Db): BlockSignatures = {
      new BlockSignatures.BlockSignaturePersister(tableName, height)
    }
  }

}
