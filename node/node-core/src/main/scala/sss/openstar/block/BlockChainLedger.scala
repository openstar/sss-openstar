package sss.openstar.block

import sss.ancillary.ByteArrayEncodedStrOps._
import sss.ancillary.Logging
import sss.db.ops.DbOps.DbRunOps
import sss.db.{Db, FutureTx}
import sss.openstar.BusEvent
import sss.openstar.chains.Chains.GlobalChainIdMask
import sss.openstar.common.block._
import sss.openstar.ledger.LedgerId.NoLedgerId
import sss.openstar.ledger.LedgerOps._
import sss.openstar.ledger._

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Try}

object BlockChainLedger {

  case class NewBlockId(chainId: GlobalChainIdMask, newId: BlockId) extends BusEvent

  def apply(blockFactory: BlockFactory, height: Long)
           (implicit db: Db,
            ledgers: Ledgers,
            chainId: GlobalChainIdMask,
            ec: ExecutionContext): FutureTx[BlockChainLedger] =
    blockFactory.getBlock(height).map(blk => new BlockChainLedger(blk, ledgers))

}

class BlockChainLedger(block: Block, ledgers: Ledgers)(implicit db:Db, ec: ExecutionContext) extends Logging {

  val blockHeight = block.height


  /**
   * Write the tx to the block storage, but don't apply it to the UTXO db.
   * Used for distributing the blocks across the network.
   *
   * @param blockTx
   * @return
   */
  def journal(blockTx: BlockTx): FutureTx[BlockChainTx] = {

    block.get(blockTx.ledgerItems.txId) flatMap {
      case Some(blck) if (blck.index == blockTx.index) =>
        FutureTx.unit(
          BlockChainTx(block.height, BlockTx(blck.index, blck.ledgerItems))
        )
      case None =>
        block.journal(blockTx.index, blockTx.ledgerItems).map(index => {
          require(index == blockTx.index, "???? Index not the same")
          BlockChainTx(block.height, BlockTx(index, blockTx.ledgerItems))
        })
      case Some(blck) =>
        FutureTx.failed(new IllegalArgumentException(
          s"Cannot journal Block Txid ${blck.ledgerItems.txId.toBase64Str}, with index ${blck.index} to new index ${blockTx.index}"
        ))
    }
  }

  /**
   * A tx that has been
   *
   * @param blockId
   * @return
   */
  def rejected(bTxId: BlockChainTxId): FutureTx[Unit] = {
    for {
      _ <- FutureTx.lazyUnit(require(bTxId.height == block.height, s"Cannot reject tx from block ${bTxId.height} in block ${block.height}"))
      _ <- block.reject(bTxId.blockTxId)
    } yield ()
  }

  def commitFTx(entry: BlockTx, commitToBlock: Boolean = true): LedgerResultFTx = {

    entry
      .ledgerItems
      .value
      .foldLeft[LedgerResultFTx](FutureTx.unit(Right(TxSeqApplied.empty(entry.ledgerItems.txId)))) {
        case (acc, ledgerEntry) =>
          acc.flatMap {
            case Right(events: TxSeqApplied) =>
              for {
                result <- ledgers.applyFTx(ledgerEntry, BlockId(block.height, entry.index), events)
                _ <- if (commitToBlock) block.commit(entry.index, entry.ledgerItems) else FutureTx.unit(())
              } yield result
            case shortCircuit =>
              FutureTx.unit(shortCircuit)

          }
      }
  } map {
    case result@Right(txApplied) =>
      log.debug(s"txApplied successfully applied ")
      log.debug(txApplied.toString)
      result
    case result@Left(err) =>
      log.info(s"Tx failed -> ${err.toString}")
      result
  }

  def prepare(stx: LedgerItem): Future[BlockChainTx] = prepare(SeqLedgerItem(stx))

  def prepare(stxs: SeqLedgerItem): Future[BlockChainTx] = {
    val nextIndex = block.getNextIndex
    log.info(s"${stxs.value.size} LedgerItems, next index is $nextIndex")
    val blkTx = BlockTx(nextIndex, stxs)
    import db.asyncRunContext

    (for {
      _ <- ledgers.validate(blkTx)
      bcTx <- journal(blkTx).run
    } yield bcTx) andThen {
      case Failure(e) =>
        log.debug(s"Exception while prepare ${e.getMessage}")
        block.setCurrentIndex(nextIndex - 1)
      case _ =>
    }

  }

  private def prepare(bTx: BlockChainTx): Future[(BlockChainTx, TxApplied)] = {

    Future {
      require(bTx.height == blockHeight, s"${bTx.height} vs ${blockHeight}")
      bTx.blockTx
    }.flatMap(block => {
      tryAndRollback(block).map {
        case res@(bcTx, _) =>
          require(bcTx.blockTx.index == bTx.blockTx.index, s"Original index ${bcTx.blockTx.index} must equal ${bTx.blockTx.index}")
          res
      }

    })
  }

  private def tryAndRollback(blkTx: BlockTx): Future[(BlockChainTx, TxApplied)] = {
    import db.asyncRunContext

    val rolledBackFut =
      commitFTx(blkTx, commitToBlock = false)
        .toFTxApplied
        .runRollback

    rolledBackFut.flatMap {
      applied =>
        journal(blkTx)
          .run
          .map { b =>
            require(blkTx.index == b.blockTx.index,
              s"The journalled index ${b.blockTx.index} is not equal to the provided nextIndex ${blkTx.index}")
            (b, applied)
          }
    }
  }


  def apply(blockTx: BlockTx): Try[TxApplied] = applyFTx(blockTx).dbRunSync

  def applyFTx(blockTx: BlockTx): FutureTx[TxApplied] = {

    for {
      wasAlreadyCommitted <- block.commit(blockTx)

      result <- if (!wasAlreadyCommitted) {
        commitFTx(blockTx, commitToBlock = false).toFTxApplied
      } else {
        val lid = blockTx.ledgerItems.value.headOption.map(_.ledgerId).getOrElse(NoLedgerId)
        FutureTx.unit(TxNotApplied(blockTx.ledgerItems.txId)(lid))
      }
    } yield result
  }


}
