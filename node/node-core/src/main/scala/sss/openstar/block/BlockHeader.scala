package sss.openstar.block

import java.util
import java.util.Date

import com.google.common.primitives.Longs
import sss.ancillary.ByteArrayComparisonOps
import sss.ancillary.ByteArrayEncodedStrOps._
import sss.db.Row
import sss.openstar.common.block.BlockId
import sss.openstar.schemamigration.SqlSchemaNames.ColumnNames._
import sss.openstar.hash.SecureCryptographicHash

/**
  * Copyright Stepping Stone Software Ltd. 2016, all rights reserved. 
  * mcsherrylabs on 3/14/16.
  */

case class BlockHeader(
  height: Long,
  numTxs: Int,
  hashPrevBlock: Array[Byte],
  merkleRoot: Array[Byte],
  committed: Boolean,
  time: Date) extends ByteArrayComparisonOps {

  lazy val asMap: Map[String, Any] = Map(
    heightCol -> height,
    numTxsCol -> numTxs,
    prevBlockCol -> hashPrevBlock,
    merkleRootCol -> merkleRoot,
    committedCol -> committed,
    mineTimeCol -> time)

  lazy val hash: Array[Byte] = SecureCryptographicHash.hash(Longs.toByteArray(height) ++ hashPrevBlock ++ merkleRoot)

  val blockId: BlockId = BlockId(height, numTxs)

  override def equals(obj: scala.Any): Boolean = obj match {
    case header: BlockHeader =>
      header.numTxs == numTxs &&
      header.height == height &&
      header.hashPrevBlock.isSame(hashPrevBlock) &&
      header.merkleRoot.isSame(merkleRoot) &&
      header.committed == committed &&
      header.time == time
    case _ => false
  }

  override def toString: String = {
    s"Height: $height, " +
      s"numTxs: $numTxs, " +
      s"hashPrev: ${hashPrevBlock.toBase64Str}, " +
      s"merkle: ${merkleRoot.toBase64Str}, " +
      s"committed: $committed" +
      s"time: $time"
  }
  override def hashCode(): Int = {
    (17 + Longs.hashCode(height)) *
      (numTxs + util.Arrays.hashCode(hashPrevBlock) +
        util.Arrays.hashCode(merkleRoot) +
        committed.hashCode +
        time.hashCode)
  }
}

object BlockHeader {

  def apply(row: Row): BlockHeader =
    BlockHeader(
      row.long(heightCol),
      row.int(numTxsCol),
      row.arrayByte(prevBlockCol),
      row.arrayByte(merkleRootCol),
      row.boolean(committedCol),
      new Date(row.long(mineTimeCol)))
}


