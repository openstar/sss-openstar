package sss.openstar.block.serialize


import java.nio.charset.StandardCharsets
import com.google.common.primitives.{Ints, Longs}
import scorex.crypto.signatures.{PublicKey, Signature}
import sss.ancillary.Serialize.{ByteArrayDeSerialize, ByteArraySerializer, IntDeSerialize, IntSerializer, LongDeSerialize, LongSerializer, SerializeHelper, Serializer, StringDeSerialize, StringSerializer}
import sss.openstar.block.signature.BlockSignatures.BlockSignature

import java.util.Date


/**
  * Copyright Stepping Stone Software Ltd. 2016, all rights reserved. 
  * mcsherrylabs on 3/3/16.
  */
object BlockSignatureSerializer extends Serializer[BlockSignature]{

  override def toBytes(blockSig: BlockSignature): Array[Byte] = {

    LongSerializer(blockSig.height) ++
      StringSerializer(blockSig.nodeId) ++
      StringSerializer(blockSig.keyType) ++
      ByteArraySerializer(blockSig.publicKey) ++
      ByteArraySerializer(blockSig.signature) ++
      IntSerializer(blockSig.index) ++
      LongSerializer(blockSig.savedAt.getTime).toBytes

  }

  override def fromBytes(b: Array[Byte]): BlockSignature = {
    val (h, nId, kType, pk, sig, index, time) = b.extract(
      LongDeSerialize,
      StringDeSerialize,
      StringDeSerialize,
      ByteArrayDeSerialize,
      ByteArrayDeSerialize,
      IntDeSerialize,
      LongDeSerialize
    )

    BlockSignature(
      index,
      new Date(time),
      h,
      nId,
      PublicKey(pk),
      kType,
      Signature(sig)
    )
  }

}
