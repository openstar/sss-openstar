package sss.openstar.block

import sss.ancillary.ByteArrayEncodedStrOps._
import sss.ancillary.Logging
import sss.db.ops.DbOps.FutureTxOps
import sss.db.{OrderAsc, _}
import sss.openstar.common.block._
import sss.openstar.schemamigration.SqlSchemaNames.ColumnNames._
import sss.openstar.ledger._
import sss.openstar.util.SynchronizedLruMap

import scala.collection.mutable
import scala.concurrent.ExecutionContext
import scala.util.Success


object Block extends Logging {

  private val lruMap: mutable.Map[Long, FutureTx[Block]]
  = SynchronizedLruMap[Long, FutureTx[Block]](100)

  def apply(height: Long, tableName: String)(implicit db: Db, ec: ExecutionContext): FutureTx[Block] = {
    def createNewBlock(): FutureTx[Block] = {
      val view = db.insertableView(tableName, where(blockHeightCol -> height))
      for {
        blockIndex <- view.maxId()
      } yield new Block(height, view, blockIndex)
    }

    lruMap.getOrElse(height, {
      val blockFTx = createNewBlock()
      blockFTx.andAfter {
        case Success(block) =>
          lruMap.put(height, FutureTx.unit(block))
        case _ =>
      }
    })
  }
}

class Block private(val height: Long,
                    private val txView: InsertableView,
                    @volatile private var blockIndex: Long)(implicit db: Db, ec: ExecutionContext) extends Logging {


  def entries: FutureTx[Seq[BlockTxId]] = {
    txView.map(toBlockTxId,
      where(s"($committedCol = ? OR $rejectedCol = ?)", true, true) orderAsc (idCol))
  }

  private[block] def toBlockTx(r: Row): BlockTx = BlockTx(r.long(idCol), r.arrayByte(entryCol).toSeqLedgerItem)

  private def toBlockTxId(r: Row): BlockTxId = BlockTxId(r.arrayByte(txIdCol), r.long(idCol))

  def page(index: Long, pageSize: Int): FutureTx[Seq[Either[BlockTx, BlockTxId]]] = {
    var count = 0
    assert(index != 0, "1 based index for pages")
    txView.filter(
      where(s"$idCol >= ? AND ($committedCol = ? OR $rejectedCol = ?)", index, true, true)
        .orderBy(OrderAsc(idCol))
        .limit(pageSize))
      .map { rows =>
        rows.map { r =>
          require(r.long(idCol) == index + count, s"row: $r, index: $index count: $count")
          count += 1
          if (r.boolean(rejectedCol)) Right(toBlockTxId(r))
          else Left(toBlockTx(r))
        }
      }

  }

  def apply(k: TxId): FutureTx[BlockTx] = get(k).map(_.get)

  def count(): FutureTx[Long] = txView.count

  def getNextIndex: Long = synchronized {
    blockIndex += 1
    blockIndex
  }

  def setCurrentIndex(current: Long): Unit = synchronized {
    blockIndex = current
  }

  def get(id: TxId): FutureTx[Option[BlockTx]] =
    txView.find(
      where(txIdCol -> id))
      .map(_.map(toBlockTx))

  def journal(blockTx: BlockTx): FutureTx[Long] = {
    journal(blockTx.index, blockTx.ledgerItems)
  }

  def journal(index: Long, le: SeqLedgerItem): FutureTx[Long] = {
    val bs = le.toBytes
    insert(height, index, Map(idCol -> index, txIdCol -> le.txId, entryCol -> bs)).map(_.id)
  }

  private def insert(height: Long, index: Long, others: Map[String, Any]) = {

    txView
      .insertNoIdentity(Map(blockHeightCol -> height, idCol -> index) ++ others)
      .map(rs => if (rs == 1) () else throw new DbError(
        s"Failed to insert the row with id as $index and height as $height into table ${txView.name}"))
      .flatMap(_ => txView(index))
  }

  private[block] def reject(bTxId: BlockTxId): FutureTx[Unit] = {

    txView.update(
      Map(entryCol -> null,
        rejectedCol -> true,
        txIdCol -> bTxId.txId
      ), where(blockHeightCol -> height, idCol -> bTxId.index))
      .flatMap(_ => txView(bTxId.index))
      .map(_ => ())
      .recoverWith {
        case e =>
          log.debug(s"No such row on reject, (will insert) ${e.getMessage}")
          insert(height, bTxId.index, Map(
            txIdCol -> bTxId.txId,
            rejectedCol -> true)
          ).map(_ => ())
      }
  }

  def getUnCommitted: FutureTx[Seq[BlockTx]] = txView.filter(
    where(committedCol -> false, rejectedCol -> false)
      .orderBy(OrderAsc(idCol)))
    .map(_.map(toBlockTx))

  def getLastRecorded: FutureTx[Long] = txView.maxId()

  def getLastRejected: FutureTx[Long] = txView.find(
    where(rejectedCol -> true)
      .orderBy(OrderDesc(idCol))
      .limit(1)
  )
    .map(_.map(_.long(idCol))
    .getOrElse(0))

  def getLastCommitted: FutureTx[Long] = {
    txView.find(
      where(committedCol -> true)
        .orderBy(OrderDesc(idCol))
        .limit(1)
    )
      .map(_.map(_.long(idCol))
      .getOrElse(0))
  }

  //TODO Unify the different versions of commit
  //the "test and fix" cycle has resulted in some entropy here.
  //there are now a few ways of doing the same thing.
  def commit(index: Long, ledgerItem: SeqLedgerItem): FutureTx[Row] = {
    txView.update(
      Map(
        txIdCol -> ledgerItem.txId,
        entryCol -> ledgerItem.toBytes,
        committedCol -> true
      ), where(blockHeightCol -> height, idCol -> index))
      .flatMap(_ => txView(index))
  }

  /*
      Has this index been committed already?
      Avoid actually committing the item to avoid
      increasing the insert sequence
   */
  def isCommitted(index: Long, txId: TxId): FutureTx[Boolean] = for {
    txRowOpt <- txView.get(index)
    wasCommitted = txRowOpt match {
      case None => false
      case Some(row) =>
        val isCommitted = row.boolean(committedCol)
        if(isCommitted) {
          val existingTxId = row.arrayByte(txIdCol)
          require(
            existingTxId sameElements txId,
            s"A committed tx at index ${index} has different txId ${txId.toBase64Str}, but existing is ${existingTxId.toBase64Str}"
          )
        }
        isCommitted
    }

  } yield wasCommitted

  /**
    *
    * @param index
    * @param le
    * @return true if the tx was already committed
    */
  def commit(blockTx: BlockTx): FutureTx[Boolean] = {
    val le = blockTx.ledgerItems
    val index = blockTx.index
    val bs = le.toBytes
    val txId = le.txId

    insert(height, index, Map(
          txIdCol -> txId,
          entryCol -> bs,
          committedCol -> true))
      .map(_ => false)
      .recoverWith {
        case x => for {
          wasCommittedPreviously <- isCommitted(index, le.txId)
          _ = log.info(s"Ledger index $index already exists was committed = $wasCommittedPreviously, try commit...${x.getMessage}")
          _ <- if(!wasCommittedPreviously) commit(index, le) else FutureTx.unit(())
        } yield wasCommittedPreviously
      }

  }

  def drop(): FutureTx[Unit] = (txView delete where(blockHeightCol -> height)).map(_ => ())

}
