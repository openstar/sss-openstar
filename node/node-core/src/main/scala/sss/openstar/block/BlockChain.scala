package sss.openstar.block

import scorex.crypto.signatures.{PublicKey, Signature}
import sss.ancillary.Logging
import sss.db.WhereOps.toWhere
import sss.db.ops.DbOps.DbRunOps
import sss.db.{where, _}
import sss.openstar.account.AccountKeys.AccountVerifyAndDets
import sss.openstar.account.NodeSigner.KeyType
import sss.openstar.account.{AccountKeyDets, AccountKeysVerify, KeyFactory, NodeIdentity, NodeVerifier, TypedPublicKey}
import sss.openstar.block.signature.BlockSignatures.BlockSignature
import sss.openstar.block.signature.{BlockSignatures, BlockSignaturesFactory}
import sss.openstar.chains.Chains.GlobalChainIdMask
import sss.openstar.common.block._
import sss.openstar.hash.{Digest32, FastCryptographicHash, SecureCryptographicHash}
import sss.openstar.schemamigration.SqlSchemaNames.ColumnNames._
import sss.openstar.merkle.MerklePersister._
import sss.openstar.merkle.{MerklePersister, MerkleTree}
import sss.openstar.util.StringCheck.SimpleTag

import java.nio.charset.StandardCharsets
import java.util.Date
import scala.collection.immutable
import scala.collection.immutable.ArraySeq
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

trait BlockChainSignaturesAccessor {

  this: BlockChainImpl =>

  implicit def keyFactory: KeyFactory
  implicit def ec: ExecutionContext

  def quorumSigs(height: Long): BlockChainSignatures =
    new BlockChainSigsImpl(this.blockSignaturesFactory.getQuorumSigs(height),
      blockHeaderOpt(height))

  def nonQuorumSigs(height: Long): BlockChainSignatures =
    new BlockChainSigsImpl(this.blockSignaturesFactory.getNonQuorumSigs(height),
      blockHeaderOpt(height))

}

trait BlockChainSignatures {

  def addSignature(signature: Signature, signersTypedPublicKey: TypedPublicKey, nodeId: String): Future[BlockSignature] =
    addSignature(signature, signersTypedPublicKey.publicKey, signersTypedPublicKey.keyType, nodeId)

  def addSignature(signature: Signature, signersPublicKey: PublicKey, keyType: KeyType, nodeId: String): Future[BlockSignature]

  def indexOfBlockSignature(nodeId: String): Future[Option[Int]]

  def signatures(maxToReturn: Int): Future[Iterable[BlockSignature]]

  def sign(nodeIdentity: NodeIdentity, hash: Array[Byte]): Future[BlockSignature]
}

trait BlockChain {

  def getLatestRecordedBlockId(): FutureTx[BlockId] = for {
    blockHeader <- lastCommittedBlockHeader
    block <- block(blockHeader.height + 1)
    biggestRecordedTxIndex <- block.getLastRecorded
  } yield BlockId(blockHeader.height + 1, biggestRecordedTxIndex)

  def getLatestCommittedBlockId(): BlockId

  def commitBlockHeaderFTx(blockHeight: Long): FutureTx[BlockHeader]

  implicit val chainId: GlobalChainIdMask

  def block(blockHeight: Long): FutureTx[Block]

  def lastCommittedBlockHeader: FutureTx[BlockHeader]

  def lastBlockHeader: FutureTx[BlockHeader]

  def blockHeaderOpt(height: Long): FutureTx[Option[BlockHeader]]

  final def blockHeader(height: Long): FutureTx[BlockHeader] = blockHeaderOpt(height).map(_.get)

  def closeBlock(blockId: BlockId, committed: Boolean = false): Future[BlockHeader]

  def closeBlockFTx(prevHeader: BlockHeader, committed: Boolean = false): FutureTx[BlockHeader]

  def closeBlock(prevHeader: BlockHeader): Future[BlockHeader]

  //def commit(blockHeader: BlockHeader): Try[BlockHeader]

  def dropFTx(height: Long): FutureTx[Unit]

  def drop(height: Long): Unit
}

object BlockChainImpl {
  //private lazy val blockHeaderCache = new SynchronizedLruMap[Long, BlockHeader](100)

  def apply(blockFactory: BlockFactory, blockSignaturesFactory: BlockSignaturesFactory, merklePersister: MerklePersister)(
    implicit db: Db, chainId: GlobalChainIdMask, ec: ExecutionContext, keyFactory: KeyFactory): BlockChainImpl = {

    val tag: SimpleTag = chainId

    val blockHeaderTable = db.table(s"blockchain_$tag")

    new BlockChainImpl(blockFactory, blockSignaturesFactory, keyFactory, blockHeaderTable)
  }
}

class BlockChainImpl private(blockFactory: BlockFactory,
                             protected val blockSignaturesFactory: BlockSignaturesFactory,
                             implicit val keyFactory: KeyFactory,
                             blockHeaderTable: Table)(
  implicit val db: Db, val ec: ExecutionContext, val chainId: GlobalChainIdMask) extends BlockChain
  with BlockChainSignaturesAccessor
  with Logging {

  private def lookupBlockHeader(height: Long): FutureTx[Option[BlockHeader]] = {
    blockHeaderTable.find(where(heightCol, height)).map(_ map (BlockHeader(_)))
  }

  private def lookupBlockHeader(sql: Where): FutureTx[Option[BlockHeader]] = {
    blockHeaderTable.find(sql).map(_ map (BlockHeader(_)))
  }

  //NB this creates the genises block if it doesn't exist.
  //lastBlockHeader

  private[block] def genesisBlock(prevHash: String = "GENESIS".padTo(32, "8").toString, merkleRoot: String = "GENESIS".padTo(32, "8").toString): FutureTx[BlockHeader] = for {
    count <- blockHeaderTable.count
    _ = require(count == 0, "Cannot create genesis block, headers exists")
    genesisHeader = BlockHeader(1, 0, prevHash.substring(0, 32)
      .getBytes(StandardCharsets.UTF_8),
      merkleRoot.substring(0, 32).getBytes(StandardCharsets.UTF_8),
      true,
      new Date())
    row <- blockHeaderTable.insert(genesisHeader.asMap)

  } yield BlockHeader(row)

  def block(blockHeight: Long): FutureTx[Block] = blockFactory.getBlock(blockHeight)

  def commitBlockHeaderFTx(blockHeight: Long): FutureTx[BlockHeader] = for {
    rowOpt <- blockHeaderTable
      .find(where(heightCol -> blockHeight))
    header <- rowOpt.fold(FutureTx.failed[Row](new RuntimeException(s"No block header row at height $blockHeight"))) { row =>
      blockHeaderTable.updateRow(row.asMap + (committedCol -> true))
    }
  } yield BlockHeader(header)


  def getLatestCommittedBlockId(): BlockId = (for {
    blockHeader <- lastCommittedBlockHeader
    hiBlk <- block(blockHeader.height + 1)
    biggestCommittedTxIndex <- hiBlk.getLastCommitted
  } yield BlockId(blockHeader.height + 1, biggestCommittedTxIndex)).dbRunSyncGet


  def lastBlockHeader: FutureTx[BlockHeader] = for {
    lookupOpt <- lookupBlockHeader(OrderDesc(heightCol) limit(1))
    blockHeader <- lookupOpt
      .map(FutureTx.unit)
      .getOrElse(genesisBlock())
  } yield blockHeader

  def lastCommittedBlockHeader: FutureTx[BlockHeader] = for {
    lookupOpt <- lookupBlockHeader(where(committedCol -> true) orderDesc(heightCol) limit(1))
    blockHeader <- lookupOpt
      .map(FutureTx.unit)
      .getOrElse(genesisBlock())
  } yield blockHeader

  def blockHeaderOpt(height: Long): FutureTx[Option[BlockHeader]] = {
    lookupBlockHeader(where(heightCol -> height))
  }

  override def closeBlock(blockId: BlockId, committed: Boolean): Future[BlockHeader] = ((for {

    blk <- block(blockId.blockHeight)
    lst <- lastCommittedBlockHeader
    lstCommitted <- blk.getLastCommitted
    lstRejected <- blk.getLastRejected
    lstRecorded <- blk.getLastCommitted
    _ = require(lst.height + 1 == blockId.blockHeight, s"Trying to close ${blockId.blockHeight} but last closed header is ${lst.height}")
    _ = require(lstCommitted == blockId.txIndex || lstRejected == blockId.txIndex,
      s"Expected number of Txs is ${blockId.txIndex}, last rejected, committed, recorded is ${lstRejected} ${lstRecorded} ${lstCommitted}")

    _ = log.info("Closeblock ok {}", blockId)

  } yield lst) flatMap(closeBlockFTx(_, committed))).dbRun

  override def closeBlock(prevHeader: BlockHeader): Future[BlockHeader] = closeBlockFTx(prevHeader).dbRun

  override def closeBlockFTx(prevHeader: BlockHeader, committed: Boolean): FutureTx[BlockHeader] = {

    val height = prevHeader.height + 1

    for {
      block <- blockFactory.getBlock(height)
      hashPrevBlock = prevHeader.hash
      txs <- block.entries
      newBlock = {
        val txIds = txs.map(_.txId)
        val hash = FastCryptographicHash(txIds)
        new BlockHeader(height, txs.size, hashPrevBlock, hash, committed, new Date())
      }
      newRow <- insertBlockHeader(newBlock)
    } yield BlockHeader(newRow)

  }

  private def insertBlockHeader(header: BlockHeader): FutureTx[Row] = {
    val whereHeight = where(heightCol -> header.height)
    for {
      rowOpt <- blockHeaderTable.find(whereHeight)
      updatedRow <- rowOpt match {

        case None =>
          blockHeaderTable.insert(header.asMap)

        case Some(row) =>
          require(!row.boolean(committedCol), s"Cannot update a committed block header $row")
          blockHeaderTable
            .updateRow(header.asMap + (idCol -> header.height))
      }
    } yield updatedRow
  }

  override def drop(height: Long): Unit = dropFTx(height).dbRunSyncGet

  override def dropFTx(height: Long): FutureTx[Unit] = for {
      _ <- blockHeaderTable.delete(
        where(heightCol -> height)
      )
      block <- blockFactory.getBlock(height)
      _ <- block.drop()
    } yield ()
}

class BlockChainSigsImpl(impl: BlockSignatures, blockHeaderOptFTx: => FutureTx[Option[BlockHeader]])(implicit keyCrypto: KeyFactory, ec: ExecutionContext, db:Db)
  extends Logging
    with BlockChainSignatures {

  def sign(nodeIdentity: NodeIdentity, hash: Array[Byte]): Future[BlockSignature] = {
    nodeIdentity.defaultNodeVerifier.signer.sign(hash) flatMap { sig =>
      impl.addFTx(
        sig,
        nodeIdentity.publicKey,
        nodeIdentity.defaultKeyType,
        nodeIdentity.id).dbRun
    }
  }

  def addSignature(signature: Signature, signersPublicKey: PublicKey, keyType: KeyType, nodeId: String): Future[BlockSignature] = Future.fromTry(Try {

    (for {
      blockHeaderOpt <- blockHeaderOptFTx
      _ = blockHeaderOpt match {
        case None =>
          throw new IllegalArgumentException(s"No block exists of height ${impl.height}")
        case Some(bh) =>
          if (!keyCrypto(keyType).verify(signersPublicKey, bh.hash, signature)) {
            throw new IllegalArgumentException(s"The signature does not match the blockheader (height=${impl.height})")
          }
      }
      index <- impl.indexOfBlockSignatureFTx(nodeId)
      blockSig <- index match {
        case None =>
          impl.addFTx(signature, signersPublicKey, keyType, nodeId)
        case Some(indx) =>
          log.warn(s"Already have sig from ${nodeId} at index $indx")
          impl.signaturesFTx(indx).map(_.last)

      }
    } yield blockSig).dbRun
  }).flatten

  def indexOfBlockSignature(nodeId: String): Future[Option[Int]] = impl.indexOfBlockSignatureFTx(nodeId).dbRun

  def signatures(maxToReturn: Int): Future[Iterable[BlockSignature]] = {
    impl.signatures(maxToReturn)
  }
}
