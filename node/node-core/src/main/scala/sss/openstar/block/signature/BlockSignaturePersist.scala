package sss.openstar.block.signature


import scorex.crypto.signatures._
import sss.ancillary.ByteArrayEncodedStrOps._
import sss.ancillary.Logging
import sss.db._
import sss.db.ops.DbOps.{DbRunOps, FutureTxOps}
import sss.openstar.account.NodeSigner.KeyType
import sss.openstar.schemamigration.SqlSchemaNames.ColumnNames._

import java.sql.SQLIntegrityConstraintViolationException
import java.util
import java.util.Date
import scala.concurrent.Future

trait BlockSignatures {
  val height: Long
  import BlockSignatures._
  def signatures(maxToReturn: Int): Future[Seq[BlockSignature]]
  def signaturesFTx(maxToReturn: Int): FutureTx[Seq[BlockSignature]]
  def write(blkSigs: Seq[BlockSignature]): Future[Seq[BlockSignature]]
  def write(blkSig: BlockSignature): Future[BlockSignature]
  def addFTx(signature: Signature, signersPublicKey: PublicKey, keyType: KeyType, nodeId: String): FutureTx[BlockSignature]
  //def add(signature: Signature, signersPublicKey: PublicKey, keyType: KeyType, nodeId: String): BlockSignature
  def indexOfBlockSignatureFTx(nodeId: String): FutureTx[Option[Int]]
  //def indexOfBlockSignature(nodeId: String): Option[Int]
}

object BlockSignatures {

  import sss.ancillary.ByteArrayComparisonOps._

  //TODO what are the implications of not checking the pKey against the ledger?
  //Can we be sure the BlockSig came from an honest party
  case class BlockSignature(index: Int,
                            savedAt: Date,
                            height: Long,
                            nodeId: String,
                            publicKey: PublicKey,
                            keyType: KeyType,
                            signature: Signature)  {
    val asMap = Map(idCol -> index,
      blockHeightCol -> height,
      createdAtCol -> savedAt.getTime,
      signatureCol -> signature,
      publicKeyCol -> publicKey,
      nodeIdCol -> nodeId,
      keyTypeCol -> keyType
      )

    override def equals(obj: scala.Any): Boolean = obj match {
      case blkSig: BlockSignature => blkSig.height == height &&
        blkSig.nodeId == nodeId &&
        blkSig.index == index &&
        blkSig.savedAt == savedAt &&
        blkSig.keyType == keyType &&
        blkSig.publicKey.isSame(publicKey) &&
        blkSig.signature.isSame(signature)
      case _ => false
    }
    override def toString: String = s"BlockSignature Index: $index, " +
      s"SavedAt: $savedAt, " +
      s"Height: $height, NodeId: $nodeId, " +
      s"PK:${publicKey.toBase64Str}, type: ${keyType}" +
      s"Sig: ${signature.toBase64Str}"


    override def hashCode(): Int =
      height.hashCode() +
        nodeId.hashCode +
        index.hashCode() +
        savedAt.hashCode() +
        keyType.hashCode() +
        util.Arrays.hashCode(publicKey) +
        util.Arrays.hashCode(signature)
  }

  private[signature] class BlockSignaturePersister(tableName: String, val height: Long)(implicit db: Db)
    extends BlockSignatures with Logging {

    private val view = db.insertableView(tableName, where(blockHeightCol -> height))

    override def write(blkSig: BlockSignature): Future[BlockSignature] = writeFTx(blkSig).dbRun
    /**
      *
      * @param blkSig
      * @return
      */
    private def writeFTx(blkSig: BlockSignature): FutureTx[BlockSignature] = {

      val findByNodeId = where(nodeIdCol -> blkSig.nodeId)
      def toRowOrError(r: Option[Row]): Row = r.getOrElse(DbException(s"BlockSignature row cannot be found by nodeId after insert: $r"))

      (for {
        _ <- FutureTx.lazyUnit(require(height == blkSig.height, "Trying to save a signature into the wrong block."))
        rowOpt <- view.insertNoIdentity(blkSig.asMap)
          .flatMap(_ => view.find(findByNodeId))
      } yield apply(toRowOrError(rowOpt))).recoverWithDb {
        case e : SQLIntegrityConstraintViolationException =>
          view.find(findByNodeId).map(rowOpt => apply(toRowOrError(rowOpt)))
      }
    }

    override def write(blkSigs: Seq[BlockSignature]): Future[Seq[BlockSignature]] = {
      FutureTx.sequence(blkSigs.map(writeFTx))
    }.dbRun

    override def addFTx(signature: Signature, signersPublicKey: PublicKey, keyType: KeyType, nodeId: String): FutureTx[BlockSignature] = {
      view.maxId().map(_ + 1L).flatMap(index => view.insertNoIdentity(Map(
          idCol -> index,
          blockHeightCol -> height,
          nodeIdCol -> nodeId,
          createdAtCol -> new Date().getTime,
          signatureCol -> signature,
          keyTypeCol -> keyType,
          publicKeyCol -> signersPublicKey
        )).map {
          case 1 => index
          case _ => throw new DbError(
            s"Failed to insert the row with id as $index and height as $height into table ${view.name}")
        }).flatMap(view(_)).map(apply)

    }

    override def indexOfBlockSignatureFTx(nodeId: String): FutureTx[Option[Int]] = {
      view.find(nodeIdCol -> nodeId).map(_.map(r => r.int(idCol)))
    }

    def signaturesFTx(maxToReturn: Int): FutureTx[Seq[BlockSignature]] = {
      view.page(0, maxToReturn, Seq(OrderAsc(idCol))).map(_.map(apply))
    }

    override def signatures(maxToReturn: Int): Future[Seq[BlockSignature]] = {
      signaturesFTx(maxToReturn)
    }.dbRun

    def apply(row: Row): BlockSignature =
      BlockSignature(row.int(idCol),
        new Date(row.long(createdAtCol)),
        height,
        row.string(nodeIdCol),
        PublicKey(row.arrayByte(publicKeyCol)),
        row.string(keyTypeCol),
        Signature(row.arrayByte(signatureCol))
      )


  }
}
