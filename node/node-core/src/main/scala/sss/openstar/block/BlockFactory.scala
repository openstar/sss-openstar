package sss.openstar.block

import sss.db.{Db, FutureTx}
import sss.openstar.db.BlockHeightToTablePartitionId.BlockHeightToTablePartitionId
import sss.openstar.schemamigration.dynamic.{BlockMigration, GlobalTableNameTag}

import scala.concurrent.ExecutionContext

trait BlockFactory {
  def getBlock(height: Long)(implicit db: Db, ec: ExecutionContext): FutureTx[Block]
}

object BlockFactory {
  def usingPartitionedTable(blockHeightToTablePartitionId: BlockHeightToTablePartitionId)(implicit globalTableNameTag: GlobalTableNameTag): BlockFactory = new BlockFactory {
    override def getBlock(height: Long)(implicit db: Db, ec: ExecutionContext): FutureTx[Block] = {
      val tableName = BlockMigration.blockTableName(blockHeightToTablePartitionId(height))
       Block(height, tableName)
    }
  }
}

