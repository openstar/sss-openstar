package sss.openstar

import scala.util.Try

object RunNode extends Node {

  def main(args: Array[UniqueNodeIdentifier]): Unit = {
    Try {
      startNode()
    } recover {
      case e =>
        e.printStackTrace()
        System.exit(-1)
    }
  }


}
