name := "openstar-node-extra"

Test / parallelExecution := false

libraryDependencies += "nl.martijndwars" % "web-push" % Vers.martinWebPush

libraryDependencies += "io.spray" %%  "spray-json" % "1.3.5"

libraryDependencies += "org.scalatra" %% "scalatra" % Vers.scalatraVer

// https://mvnrepository.com/artifact/org.jsoup/jsoup
libraryDependencies += "org.jsoup" % "jsoup" % "1.15.3"

libraryDependencies += "iog.psg" %% "message-signing-3rd-party-client" % Vers.messageSigningClient

resolvers += ("maven-public" at "https://nexus.mcsherrylabs.com/repository/maven-public/")

resolvers += ("maven-snapshot" at "https://nexus.mcsherrylabs.com/repository/maven-snapshots/")