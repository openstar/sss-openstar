package sss.openstar

import com.google.protobuf.{ByteString => GByteString}
import sss.openstar.ui.rpc._
import cats.syntax.either._
import sss.openstar.ui.rpc.ResultProviderCharge.OptionOptionLong

package object rpc {

  def massage[K, T <: BaseResult](r: Result[K])(implicit m: Massge[K, T]): T = m.massage(r)

  trait Massge[K, T <: BaseResult] {
    def massage(r: Result[K]): T
  }


  implicit val massageResultGetPreparedForSystemUpdate: Massge[PreparedForSystemUpdate, ResultGetPreparedForSystemUpdate] = (r: Result[PreparedForSystemUpdate]) => ResultGetPreparedForSystemUpdate.of(
    r.leftMap(_.toAppError).fold(ResultGetPreparedForSystemUpdate.Either.Problem, ResultGetPreparedForSystemUpdate.Either.Value)
  )

  implicit val massageRespondSystemUpdateProposal: Massge[Boolean, ResultRespondSystemUpdateProposal] = (r: Result[Boolean]) => ResultRespondSystemUpdateProposal.of(
    r.leftMap(_.toAppError).fold(ResultRespondSystemUpdateProposal.Either.Problem, ResultRespondSystemUpdateProposal.Either.Value)
  )

  implicit val massageCreateSystemUpdateProposal: Massge[Boolean, ResultCreateSystemUpdateProposal] = (r: Result[Boolean]) => ResultCreateSystemUpdateProposal.of(
    r.leftMap(_.toAppError).fold(ResultCreateSystemUpdateProposal.Either.Problem, ResultCreateSystemUpdateProposal.Either.Value)
  )

  implicit val massageCountIdentities: Massge[Long, ResultCountIdentities] = (r: Result[Long]) => ResultCountIdentities.of(
    r.leftMap(_.toAppError).fold(ResultCountIdentities.Either.Problem, ResultCountIdentities.Either.Value)
  )

  implicit val massageCostToContact: Massge[Long, ResultCostToContact] = (r: Result[Long]) => ResultCostToContact.of(
    r.leftMap(_.toAppError).fold(ResultCostToContact.Either.Problem, ResultCostToContact.Either.Value)
  )

  implicit val massageFetchIdentities: Massge[Seq[String], ResultFetchIdentities] = (r: Result[Seq[String]]) => ResultFetchIdentities.of(
    r.bimap(_.toAppError, SeqString(_)).fold(ResultFetchIdentities.Either.Problem, ResultFetchIdentities.Either.Value)
  )

  implicit val massageListContacts: Massge[Seq[RpcContact], ResultSeqContacts] = (r: Result[Seq[RpcContact]]) => ResultSeqContacts.of(
    r.bimap(_.toAppError, RpcContacts(_)).fold(ResultSeqContacts.Either.Problem, ResultSeqContacts.Either.Value)
  )

  implicit val massageListProviders: Massge[Seq[RegisteredProvider], ResultRegisteredProviders] = (r: Result[Seq[RegisteredProvider]]) => ResultRegisteredProviders.of(
    r.bimap(_.toAppError, RegisteredProviders(_)).fold(ResultRegisteredProviders.Either.Problem, ResultRegisteredProviders.Either.Value)
  )

  implicit val massageLogout: Massge[Boolean, ResultLogout] = (r: Result[Boolean]) => ResultLogout.of(
    r.leftMap(_.toAppError).fold(ResultLogout.Either.Problem, ResultLogout.Either.Value)
  )

  implicit val massageTrusteeMessage: Massge[Boolean, ResultTrusteeMessage] = (r: Result[Boolean]) => ResultTrusteeMessage.of(
    r.leftMap(_.toAppError).fold(ResultTrusteeMessage.Either.Problem, ResultTrusteeMessage.Either.Value)
  )

  implicit val massageChargeClientsAccount: Massge[Boolean, ResultChargeClientsAccount] = (r: Result[Boolean]) => ResultChargeClientsAccount.of(
    r.leftMap(_.toAppError).fold(ResultChargeClientsAccount.Either.Problem, ResultChargeClientsAccount.Either.Value)
  )

  implicit val massageTransferBetweenClientAccounts: Massge[Boolean, ResultTransferBetweenClientAccounts] = (r: Result[Boolean]) => ResultTransferBetweenClientAccounts.of(
    r.leftMap(_.toAppError).fold(ResultTransferBetweenClientAccounts.Either.Problem, ResultTransferBetweenClientAccounts.Either.Value)
  )

  implicit val massageWithdrawFromClientAccount: Massge[Boolean, ResultWithdrawFromClientAccount] = (r: Result[Boolean]) => ResultWithdrawFromClientAccount.of(
    r.leftMap(_.toAppError).fold(ResultWithdrawFromClientAccount.Either.Problem, ResultWithdrawFromClientAccount.Either.Value)
  )

  implicit val massageSendEncryptedMessage: Massge[Boolean, ResultSendEncryptedMessage] = (r: Result[Boolean]) => ResultSendEncryptedMessage.of(
    r.leftMap(_.toAppError).fold(ResultSendEncryptedMessage.Either.Problem, ResultSendEncryptedMessage.Either.Value)
  )

  implicit val massageSendNewContactMessage: Massge[Boolean, ResultSendNewContactMessage] = (r: Result[Boolean]) => ResultSendNewContactMessage.of(
    r.leftMap(_.toAppError).fold(ResultSendNewContactMessage.Either.Problem, ResultSendNewContactMessage.Either.Value)
  )

  implicit val massageRemoveProvider: Massge[Boolean, ResultRemoveProvider] = (r: Result[Boolean]) => ResultRemoveProvider.of(
    r.leftMap(_.toAppError).fold(ResultRemoveProvider.Either.Problem, ResultRemoveProvider.Either.Value)
  )

  implicit val massageAddProvider: Massge[Boolean, ResultAddProvider] = (r: Result[Boolean]) => ResultAddProvider.of(
    r.leftMap(_.toAppError).fold(ResultAddProvider.Either.Problem, ResultAddProvider.Either.Value)
  )

  implicit val massageSetPaywallCategory: Massge[Boolean, ResultSetPaywallCategory] = (r: Result[Boolean]) => ResultSetPaywallCategory.of(
    r.leftMap(_.toAppError).fold(ResultSetPaywallCategory.Either.Problem, ResultSetPaywallCategory.Either.Value)
  )

  implicit val massageDeletePaywallCategory: Massge[Boolean, ResultDeletePaywallCategory] = (r: Result[Boolean]) => ResultDeletePaywallCategory.of(
    r.leftMap(_.toAppError).fold(ResultDeletePaywallCategory.Either.Problem, ResultDeletePaywallCategory.Either.Value)
  )

  implicit val massageDeleteMessage: Massge[Boolean, ResultDeleteMessage] = (r: Result[Boolean]) => ResultDeleteMessage.of(
    r.leftMap(_.toAppError).fold(ResultDeleteMessage.Either.Problem, ResultDeleteMessage.Either.Value)
  )

  implicit val massageSetAvatar: Massge[Boolean, ResultSetAvatar] = (r: Result[Boolean]) => ResultSetAvatar.of(
    r.leftMap(_.toAppError).fold(ResultSetAvatar.Either.Problem, ResultSetAvatar.Either.Value)
  )

  implicit val massageDeleteAttachment: Massge[Boolean, ResultDeleteAttachment] = (r: Result[Boolean]) => ResultDeleteAttachment.of(
    r.leftMap(_.toAppError).fold(ResultDeleteAttachment.Either.Problem, ResultDeleteAttachment.Either.Value)
  )

//  implicit val massageLogin: Massge[String, ResultLogin] = (r: Result[String]) => ResultLogin.of(
//    r.leftMap(_.toAppError).fold(ResultLogin.Either.Problem, ResultLogin.Either.Value)
//  )

  implicit val massageSetEmail: Massge[String, ResultSetEmail] = (r: Result[String]) => ResultSetEmail.of(
    r.leftMap(_.toAppError).fold(ResultSetEmail.Either.Problem, ResultSetEmail.Either.Value)
  )

  implicit val massageCreateIdentityBearerAttribute: Massge[String, ResultCreateIdentityBearerAttribute] = (r: Result[String]) => ResultCreateIdentityBearerAttribute.of(
    r.leftMap(_.toAppError).fold(ResultCreateIdentityBearerAttribute.Either.Problem, ResultCreateIdentityBearerAttribute.Either.Value)
  )

  implicit val massageHostEncrypt: Massge[String, ResultHostEncrypt] = (r: Result[String]) => ResultHostEncrypt.of(
    r.leftMap(_.toAppError).fold(ResultHostEncrypt.Either.Problem, ResultHostEncrypt.Either.Value)
  )

  implicit val massageHostDecrypt: Massge[String, ResultHostDecrypt] = (r: Result[String]) => ResultHostDecrypt.of(
    r.leftMap(_.toAppError).fold(ResultHostDecrypt.Either.Problem, ResultHostDecrypt.Either.Value)
  )

  implicit val massageCreateUser: Massge[String, ResultCreateUser] = (r: Result[String]) => ResultCreateUser.of(
    r.leftMap(_.toAppError).fold(ResultCreateUser.Either.Problem, ResultCreateUser.Either.Value)
  )

  implicit val massageCountMessages: Massge[Int, ResultCountMessages] = (r: Result[Int]) => ResultCountMessages.of(
    r.leftMap(_.toAppError).fold(ResultCountMessages.Either.Problem, ResultCountMessages.Either.Value)
  )

  implicit val massageBalance: Massge[Long, ResultBalance] = (r: Result[Long]) => ResultBalance.of(
    r.leftMap(_.toAppError).fold(ResultBalance.Either.Problem, ResultBalance.Either.Value)
  )

  implicit val massageClientBalance: Massge[Long, ResultClientBalance] = (r: Result[Long]) => ResultClientBalance.of(
    r.leftMap(_.toAppError).fold(ResultClientBalance.Either.Problem, ResultClientBalance.Either.Value)
  )

  implicit val massageCurrencyWithdrawal: Massge[Boolean, ResultCurrencyWithdrawal] = (r: Result[Boolean]) => ResultCurrencyWithdrawal.of(
    r.leftMap(_.toAppError).fold(ResultCurrencyWithdrawal.Either.Problem, ResultCurrencyWithdrawal.Either.Value)
  )

  implicit val massageSeqClientBalanceMessages: Massge[Seq[ClientBalance], ResultClientBalancesMessage] =
    (r: Result[Seq[ClientBalance]]) => ResultClientBalancesMessage.of(
      r.bimap(_.toAppError, ClientBalances(_)).fold(ResultClientBalancesMessage.Either.Problem, ResultClientBalancesMessage.Either.Value)
    )

  implicit val massageDisplayMessages: Massge[Seq[DisplayMessage], ResultDisplayMessages] =
    (r: Result[Seq[DisplayMessage]]) => ResultDisplayMessages.of(
      r.bimap(_.toAppError, DisplayMessages(_)).fold(ResultDisplayMessages.Either.Problem, ResultDisplayMessages.Either.Value)
    )

  implicit val massageDetailedDisplayMessage: Massge[RpcDetailedDisplayMessage, ResultDetailedDisplayMessage] =
    (r: Result[RpcDetailedDisplayMessage]) => ResultDetailedDisplayMessage.of(
      r.leftMap(_.toAppError).fold(ResultDetailedDisplayMessage.Either.Problem, ResultDetailedDisplayMessage.Either.Value)
    )

  implicit val massageSeqPaywallCategory: Massge[Seq[PaywallCategory], SeqPaywallCategoryResult] =
    (r: Result[Seq[PaywallCategory]]) => SeqPaywallCategoryResult.of(
      r.bimap(_.toAppError, SeqPaywallCategory(_)).fold(SeqPaywallCategoryResult.Either.Problem, SeqPaywallCategoryResult.Either.Value)
    )

  implicit val massageProviderCharge: Massge[Option[Option[Long]], ResultProviderCharge] = {
    case Left(p) => ResultProviderCharge(ResultProviderCharge.Either.Problem(p.toAppError))
    case Right(Some(v)) => ResultProviderCharge(ResultProviderCharge.Either.Value(OptionOptionLong(v)))
    case Right(None) => ResultProviderCharge(ResultProviderCharge.Either.Empty)
  }

  implicit val massageAvatar: Massge[Option[Array[Byte]], ResultAvatar] = {
    case Left(p) => ResultAvatar(ResultAvatar.Either.Problem(p.toAppError))
    case Right(Some(v)) => ResultAvatar(ResultAvatar.Either.Value(GByteString.copyFrom(v)))
    case Right(None) => ResultAvatar(ResultAvatar.Either.Empty)
  }

  implicit val massageGetEmail: Massge[Option[String], ResultGetEmail] = {
    case Left(p) => ResultGetEmail(ResultGetEmail.Either.Problem(p.toAppError))
    case Right(Some(v)) => ResultGetEmail(ResultGetEmail.Either.Value(v))
    case Right(None) => ResultGetEmail(ResultGetEmail.Either.Empty)
  }

  implicit val massageGetHashedIdentityBearerAttribute: Massge[Option[String], ResultGetHashedIdentityBearerAttribute] = {
    case Left(p) => ResultGetHashedIdentityBearerAttribute(ResultGetHashedIdentityBearerAttribute.Either.Problem(p.toAppError))
    case Right(Some(v)) => ResultGetHashedIdentityBearerAttribute(ResultGetHashedIdentityBearerAttribute.Either.Value(v))
    case Right(None) => ResultGetHashedIdentityBearerAttribute(ResultGetHashedIdentityBearerAttribute.Either.Empty)
  }

}
