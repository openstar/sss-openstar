package sss.openstar.rpc

import akka.stream.scaladsl.Source
import sss.openstar.account.impl.authplus.RemoteKeyApi
import sss.openstar.ui.rpc._

class AuthPlusRpcImpl(
                      remoteKeyApi: RemoteKeyApi,
                      ) extends AuthPlusService {

  def createLink(in: CreateLinkMessage): Source[CreateLinkResult,akka.NotUsed] = {
    remoteKeyApi.remoteCall.createLink(
      in.userTag.get.user,
      in.userTag.get.tag,
      in.userTag.get.keyType,
      "Openstar Identity Key",
      in.description,

    )
  }

}
