package sss.openstar.rpc

import sss.openstar.ui.rpc.AppError

object RpcErrors {
  val noSessionError = AppError("NoSession", "No session found, not logged in")
  val noTokensError = AppError("NoTokens", "Bad request, tokens needed for proxy and client")
  val tempNackError = AppError("InternalTempNack", "Try again, system busy")

  def transferError(failMsg: String): AppError = AppError("TransferFailure", failMsg)

}
