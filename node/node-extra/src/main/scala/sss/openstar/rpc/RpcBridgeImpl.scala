package sss.openstar.rpc

import akka.NotUsed
import akka.actor.{ActorContext, ActorRef, ActorSystem}
import akka.stream.IOResult
import akka.stream.scaladsl.StreamConverters.asInputStream
import akka.stream.scaladsl.{Sink, Source, StreamConverters}
import akka.util.ByteString
import com.google.protobuf.{ByteString => GByteString}
import sss.ancillary.ByteArrayEncodedStrOps._
import sss.ancillary.{Guid, Logging}
import sss.db.Db
import sss.openstar.Currency
import sss.openstar.account.NodeIdentityManager.PasswordCredentials
import sss.openstar.account._
import sss.openstar.attachments.AttachmentsService
import sss.openstar.balanceledger.OracleBalanceLedger.OracleWithdrawEncumbrance
import sss.openstar.balanceledger.TxOutput
import sss.openstar.chains.Chains.GlobalChainIdMask
import sss.openstar.chains.TxWriterActor.{InternalCommit, InternalNack, InternalTempNack, InternalTxResult}
import sss.openstar.common.users.UserDirectory
import sss.openstar.contacts.ContactService
import sss.openstar.controller.SendMessage.SubmitMessage
import sss.openstar.controller.{ApiHostHelper, ChargeHelper, ContactHelper, EncryptedMessageHelper, PaywallHelper, ProviderHelper, Send, Utils}
import sss.openstar.crypto.ECEncryption.CreateNewLocalKeySign
import sss.openstar.crypto.{AESDetails, CBCStreamEncryption, ECEncryption, SeedBytes}
import sss.openstar.hash.Digest32
import sss.openstar.html5push.Subscriptions
import sss.openstar.html5push.Subscriptions.Subscription
import sss.openstar.identityledger.SystemAttributeCategory.BearerAuthorization
import sss.openstar.identityledger.{BearerAuthorizationAttribute, EmailAttribute, IdentityService, SetAttribute, SignerVerifierDetails, SystemAttributeCategory}
import sss.openstar.message.MessageInBox.MessageQueryType
import sss.openstar.message.MessagePaywall.MessagePaywallGenerator
import sss.openstar.message.payloads.MessageEcryption.AttachmentDetails
import sss.openstar.message.{MessageDownloadPersistCache, OutgoingMessageProcessorUtil, UtxoQuery, UtxoWatch}
import sss.openstar.network.MessageEventBus
import sss.openstar.quorumledger.QuorumService
import sss.openstar.rpc.DisplayMessageAdapter.Ops._
import sss.openstar.rpc.RpcErrors._
import sss.openstar.systemledger.{CancelUpdate, PreparedToUpdate, SystemServiceQuery, UpdateNotification}
import sss.openstar.tools.ClaimIdentity
import sss.openstar.tools.SendTxSupport.SendTx
import sss.openstar.ui.rpc.ImplicitOps._
import sss.openstar.ui.rpc.{ResultLogin, _}
import sss.openstar.util.HashedSecret
import sss.openstar.wallet.UnlockedWallet
import sss.openstar.wallet.Wallet.{CreditTooLow, Transfer}
import RpcOps.ResultF
import scorex.crypto.signatures.{PublicKey, Signature}
import sss.openstar.account.impl.EncryptedKeyFileAccountSignerCreator
import sss.openstar.crypto.password.PasswordRules

import java.io.InputStream
import java.nio.charset.StandardCharsets
import java.util.concurrent.TimeUnit
import scala.annotation.nowarn
import scala.concurrent.duration.FiniteDuration
import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.util.{Failure, Success, Try}
import iog.psg.client.messagesigning.NeedHashMessageType
import iog.psg.messagesigning.keys.KeyType
import iog.psg.messagesigning.util.CreateSourceActorRef
import sss.openstar.account.impl.authplus.{AuthPlusAccountSignerCreator, RemoteKeyApi}
import sss.openstar.account.impl.webauthn.{WebAuthnAccountSignerCreator, WebAuthnSignatureEvent, WebAuthnSignatures}


class RpcBridgeImpl[BASECURRENCY <: Currency](
                                               claimUrl: String,
                                               systemServiceQuery: SystemServiceQuery,
                                               getCurrentBlockHeight: () => Long,
                                               //buildStarzWallet: NodeIdentity => UnlockedWallet[Starz],
                                               buildBaseCurrencyWallet: NodeIdentity => UnlockedWallet[BASECURRENCY],
                                               toIdentityLedgerOwner: NodeIdentity => Option[NodeIdentity],
                                               users: UserDirectory,
                                               hostingNode: NodeIdentity,
                                               welcomeMessage: String,
                                               welcomeAmount: Long,
                                               userSessionFactory: UserSessionFactory[BASECURRENCY],
                                               cpuEc: ExecutionContext,
                                               messageInBoxActorGroup: ActorRef,
                                               keyPersister: KeyPersister,
                                               remoteKeyApi: RemoteKeyApi,
                                               webAuthnSigs: WebAuthnSignatures

)(implicit

  apiHostHelper: ApiHostHelper[BASECURRENCY],
  attachmentsService: AttachmentsService,
  nodeIdentityManager: NodeIdentityManager,
  persistCache: MessageDownloadPersistCache,
  outgoingMessageProcessorUtil: OutgoingMessageProcessorUtil,
  submitMessage: SubmitMessage,
  sendTx: SendTx,
  events: MessageEventBus,
  as: ActorSystem,
  actorContext: ActorContext,
  send: Send,
  chainId: GlobalChainIdMask,
  identityService: IdentityService,
  quorumService: QuorumService,
  contactService: ContactService,
  chargeHelper: ChargeHelper,
  utxoQuery: UtxoQuery,
  utxoWatch: UtxoWatch,
  messagePaywallGenerator: MessagePaywallGenerator[BASECURRENCY],
  subscriptions: Subscriptions,
  db: Db,
  createNewCryptoKeys: CreateNewLocalKeySign,
  defaultAccountKeysFactory: AccountKeysFactory
) extends BridgeService with Logging {

  implicit private val implicitCpuEc: ExecutionContext = cpuEc

  private def userSession(sessId: String): Result[UserSession[BASECURRENCY]] = userSessionFactory(sessId).asResult(UnknownSession)

  private val unlockedHostWallet: UnlockedWallet[Currency] = buildBaseCurrencyWallet(hostingNode)

  val brokenDisplayMessage = DisplayMessage.defaultInstance.copy(id = BrokenDetailedMessage.id)

  private def futureProblem[T](msg: String): Future[Result[T]] =
    Future.successful(problem(msg))

  private def validateLoginInputs(
                                   user: String,
                                   password: String,
                                   sessId: String): Result[Unit] = {
    require(Option(sessId).isDefined, "sessId cannot be null")
    require(sessId.nonEmpty, "sessId cannot be empty")

    if (Option(user).isEmpty || user.isEmpty) problem("User cannot be empty")
    else if (Option(password).isEmpty || password.isEmpty) problem("Password cannot be empty")
    else Right(())
  }

  def webAuthnSig(in: WebAuthnSigMessage): Future[ResultWebAuthnSig] = Future {
    if(webAuthnSigs.respondToSignatureRequest(in.user, Signature(in.signature.toByteArray))) {
      ResultWebAuthnSig.defaultInstance
    } else {
      ResultWebAuthnSig(Some(AppError("BadSigRequest", "No request pending for that user")))
    }

  }

  def login(
             sessId: String,
             user: String,
             tag: String,
             authType: String,
             passwordOpt: Option[String],
             subscription: Option[SubscriptionMessage]
           ): Source[ResultLogin, NotUsed] = {

    val (ref, resultSource) = CreateSourceActorRef[Any]()

    authType match {

      case WebAuthnAccountSignerCreator.SignerType =>

        events.subscribe(classOf[WebAuthnSignatureEvent])(ref)

        val challenge = SeedBytes.secureSeed(64)
        (for {
          n <- nodeIdentityManager.nodeVerifier(NodeIdTag(user, tag))
          sig <- webAuthnSigs.requestSignature(user, challenge)
          _ = log.info(s"Got sig back ${sig.length} ")
          isGood = n.verifier.verify(challenge, sig)
          _ = log.info(s"Got sig back and its $isGood ")
          nodeIdOpt <- if (isGood) {
            nodeIdentityManager(user, tag, Seq.empty).map(Some(_))
          } else {
            Future.successful(None)
          }
        } yield nodeIdOpt match {
          case Some(nodeIdentity) =>
            val adaUserWallet = buildBaseCurrencyWallet(nodeIdentity)
            val subOpt = subscription.map(rpcSub => Subscription(rpcSub.endpoint, rpcSub.key, rpcSub.auth))
            userSessionFactory.register(sessId, nodeIdentity, hostingNode.id, adaUserWallet, subOpt, messageInBoxActorGroup)
            ref ! success(sessId)
          case None =>
            ref ! problem("Sig Verification failure")
        }).andThen {
          case Failure(exception) =>
            log.warn("Problem getting remote signature", exception)
            ref ! exception
        }


      case AuthPlusAccountSignerCreator.SignerType =>

        val remoteApiCall = remoteKeyApi.remoteCall

        val challenge = SeedBytes.secureSeed(64)

        (for {
          n <- nodeIdentityManager.nodeVerifier(NodeIdTag(user, tag))

          sig <- remoteApiCall.requestSignature(
            n.verifier.typedPublicKey.publicKey,
            KeyType.withName(n.verifier.typedPublicKey.keyType),
            NeedHashMessageType(GByteString.copyFrom(challenge)),
            user,
            tag,
            "Authenticate to Openstar Social"
          )

          isGood = n.verifier.verify(challenge, Signature(sig.signature.toByteArray))
          nodeIdOpt <- if(isGood) {
            nodeIdentityManager(user, tag, Seq.empty).map(Some(_))
          } else {
            Future.successful(None)
          }
        } yield {
          nodeIdOpt match {
            case Some(nodeIdentity) =>
              val adaUserWallet = buildBaseCurrencyWallet(nodeIdentity)
              val subOpt = subscription.map(rpcSub => Subscription(rpcSub.endpoint, rpcSub.key, rpcSub.auth))
              userSessionFactory.register(sessId, nodeIdentity, hostingNode.id, adaUserWallet, subOpt, messageInBoxActorGroup)
              ref ! success(sessId)
            case None =>
              ref ! problem("Sig Verification failure")
          }

        }).andThen {
          case Failure(exception) =>
            log.warn("Problem getting remote signature", exception)
            ref ! exception
        }

      case EncryptedKeyFileAccountSignerCreator.SignerType if passwordOpt.isEmpty =>
        ref ! problem("You need a password to un encrypt a key file")

      case EncryptedKeyFileAccountSignerCreator.SignerType if passwordOpt.isDefined =>
        val password = passwordOpt.get
        (for {
          nodeId <- nodeIdentityManager(user, tag, Seq(PasswordCredentials(NodeIdTag(user, tag), password)))
          isSignerUnlocked = nodeId.defaultNodeVerifier.signerOpt.isDefined
          result = validateLoginInputs(user, password, sessId)
        } yield {
          if (isSignerUnlocked) {
            result.map { _ =>
              val adaUserWallet = buildBaseCurrencyWallet(nodeId)
              val subOpt = subscription.map(rpcSub => Subscription(rpcSub.endpoint, rpcSub.key, rpcSub.auth))
              userSessionFactory.register(sessId, nodeId, hostingNode.id, adaUserWallet, subOpt, messageInBoxActorGroup)
              ref ! success(sessId)
            }
          } else {
            ref ! problem("Could not unlock users signer")
          }
        }) recover (ref ! _)
    }

    resultSource.map[ResultLogin] {
      case WebAuthnSignatureEvent(mToSign) =>
        ResultLogin.of(ResultLogin.Either.MessageToSign(GByteString.copyFrom(mToSign)))
      case Right(sessId: String) =>
        ResultLogin.of(ResultLogin.Either.SessId(sessId))
      case Left(prob: Problem) =>
        ResultLogin.of(ResultLogin.Either.Problem(prob.toAppError))
      case e: Throwable =>
        ResultLogin.of(ResultLogin.Either.Problem(AppError("Exception", e.getMessage)))
    }
  }


  def logout(sessId: String): ResultOk = tryResult {
    userSessionFactory.remove(sessId)
    ok()
  }

  def createUserBasic(
                  sessId: String,
                  details: SignerVerifierDetails,
                  subOpt: Option[SubscriptionMessage],
                ): Future[Result[String]] = {

    require(Option(sessId).isDefined, "sessId cannot be null")
    require(sessId.nonEmpty, "sessId cannot be empty")
    val user = details.nodeIdTag.nodeId

    if (Option(user).isEmpty || user.isEmpty) Future.successful(problem("User cannot be empty"))
    else if (details.nodeIdTag.tag != NodeIdTag.defaultTag) Future.successful(problem("Must use defaultTag"))
    else if (toIdentityLedgerOwner(hostingNode).isDefined) {
      log.warn("CLAIMING REMOTE using only public key, all other signerverifier details will use defaults")

      Future.fromTry(ClaimIdentity.claimRemote(
        1,
        claimUrl,
        details.nodeIdTag.nodeId,
        details.typedPublicKey.publicKey
      ).map(_ => Right(sessId)))
    } else {

      ClaimIdentity.claim(
        claimUrl,
        details,
        toIdentityLedgerOwner,
        hostingNode,
      ).flatMap { ni =>

        val userWallet = buildBaseCurrencyWallet(ni)
        userSessionFactory.register(sessId, ni, hostingNode.id, userWallet,
          subOpt.map(rpcSub => Subscription(rpcSub.endpoint, rpcSub.key, rpcSub.auth)), messageInBoxActorGroup)

        implicit val unlockedHostWalletImp: UnlockedWallet[Currency] = unlockedHostWallet
        implicit val hostingNodeImpicit: NodeIdentity = hostingNode

        val sendingKeyType = hostingNode.defaultNodeVerifier.verifier.typedPublicKey.keyType
        if(ni.defaultKeyType == sendingKeyType) {
          EncryptedMessageHelper.sendMessageToMany(
            Guid(),
            welcomeMessage,
            None,
            welcomeAmount,
            Seq(user),
            5,
            None
          ) map (r => r.map(_ => sessId))
        } else Future.successful(success(sessId))

      }
    }.recover {
      case CreditTooLow(msg) =>
        problem("Identity created but could not be funded, get starz elsewhere.")
      case e: IllegalArgumentException =>
        problem(e.getMessage)
      case u: Throwable =>
        log.warn("Failed to crate user", u)
        problem(s"User $user may already be taken")
    }
  }
  def createUser(
    sessId: String,
    user: String,
    password: String,
    pass2: String,
    subOpt: Option[SubscriptionMessage]
  ): Future[Result[String]] = {

    require(Option(sessId).isDefined, "sessId cannot be null")
    require(sessId.nonEmpty, "sessId cannot be empty")

    if (Option(user).isEmpty || user.isEmpty) Future.successful(problem("User cannot be empty"))
    else if (Option(password).isEmpty || password.isEmpty || password != pass2 || !PasswordRules.isValidPassword(password)) {
      Future.successful(problem(s"Passwords must match and ${PasswordRules.descriptiveMessage}"))
    } else {

      ClaimIdentity.claimAndSetup(
        user,
        password,
        quorumService.candidates(),
        toIdentityLedgerOwner,
        hostingNode,
        keyPersister,
        defaultAccountKeysFactory,
        claimUrl
      ).flatMap { ni =>
        //val starzUserWallet = buildStarzWallet(ni)
        val adaUserWallet = buildBaseCurrencyWallet(ni)
        userSessionFactory.register(sessId, ni, hostingNode.id, adaUserWallet,
          subOpt.map(rpcSub => Subscription(rpcSub.endpoint, rpcSub.key, rpcSub.auth)), messageInBoxActorGroup)

        implicit val unlockedHostWalletImp: UnlockedWallet[Currency] = unlockedHostWallet
        implicit val hostingNodeImpicit: NodeIdentity = hostingNode

        EncryptedMessageHelper.sendMessageToMany(
          Guid(),
          welcomeMessage,
          None,
          welcomeAmount,
          Seq(user),
          5,
          None
        ) map (r => r.map(_ => sessId))

      }
    }.recover {
      case CreditTooLow(msg) =>
        problem("Identity created but could not be funded, get starz elsewhere.")
      case e:IllegalArgumentException =>
        problem(e.getMessage)
      case _ =>
        problem(s"User $user may already be taken")
    }
  }

  def countMessages(sessId: String): Result[Int] = tryResult {
    userSession(sessId) map {
      us =>
        val c = us.messages.count(MessageQueryType.DefaultOrder)
        val countInt: Int = if (c > Int.MaxValue) Int.MaxValue else c.toInt
        countInt
    }
  }

  def deleteMessage(sessId: String, msgId: Long): ResultOk = tryResult {
    userSession(sessId).map { us =>

      us.messages.idToGuid(msgId)
        .map(us.messages.delete)
        .get
    }
  }


  def fetchMessages(sessId: String, page: Int, limit: Int, newestFirst: Boolean = false): Future[Result[Seq[DisplayMessage]]] = tryResult {
    userSession(sessId).map { us =>
      import us.nodeId

      us.messageAttachments.clear()

      val p = if (newestFirst) {
        us.messages.page(page, limit, MessageQueryType.NewestFirst)
      } else {
        us.messages.page(page, limit, MessageQueryType.DefaultOrder)
      }

      val rs = p.map {
        savedMessage =>

          val adapted = MessageAdapter.adapt(savedMessage)
          adapted.map(_.toDisplay(us.messages.guidToId)).recover {
            case e =>
              log.warn("Failed to adapt message", e)
              brokenDisplayMessage
          }

      }
      Future.sequence(rs)
    }
  }.toFutureResult

  def countIdentities(filter: Option[String]): Result[Long] = tryResult {
    //val adjusted = if (identityService.count > Int.MaxValue) Int.MaxValue else identityService.count.toInt
    success(identityService.count(filter))
  }

  def fetchIdentities(startsWith: Option[String], page: Long, limit: Int): Result[Seq[String]] = tryResult {
    success(identityService.list(startsWith, page, limit))
  }

  def balance(sessId: String): Result[Long] = tryResult {
    userSession(sessId) map {
      us =>
        us.userAdaWallet.w.balance()
          .getOrElse(unlockedHostWallet.w.amount(0)).value
    }
  }

  def sendEncryptedMessage(sessId: String, msg: DecryptedMessage): Future[Result[Boolean]] = {
    Future.fromTry(Try(userSessionFactory(sessId))).flatMap {
      case Some(us) =>

        import us._

        val parentGuid = msg.parentIdOpt.map(id => {
          messages.idToGuid(id)
            .getOrElse {
              log.info(s"There is no guid for id $id in message box")
              throw new IllegalArgumentException(s"There is no guid for id $id in message box")
            }
        })

        /*log.info(s"${us.nodeId.id} sending msg ${newMessageGuid} with parentGuid ${parentGuid} to ${msg.tos} ")
        msg.parentIdOpt.foreach(id => {
          Try(messages.get(id)) match {
            case Failure(e) =>
              log.info(s"Failed to lookup msg $id")
            case Success(msg) =>
              log.info(s"Sending message whose parent has parent ${msg.msg.parentGuid}")
          }
        })*/

        val newMessageGuid = Guid(msg.guid.toByteArray)
        val messageAttachmentsOpt = us.messageAttachments.getAttachments(newMessageGuid)


        implicit val w: UnlockedWallet[Currency] = userAdaWallet
        EncryptedMessageHelper.sendMessageToMany(
          newMessageGuid,
          msg.text,
          messageAttachmentsOpt,
          msg.amount,
          msg.tos,
          msg.numBlocksToLive,
          parentGuid)
          .recover {
            case _: CreditTooLow =>
              problem(CreditTooLowProblem)
          }
      case None =>
        Future.successful(problem(UnknownSession))
    }
  }

  def listProviders(sessId: String): Result[Seq[RegisteredProvider]] = {

    userSession(sessId) map {
      us =>
        ProviderHelper
          .getProviders(us.nodeId.id)
          .toSeq
          .map(p =>
            RegisteredProvider(p.name, p.amount)
          )
    }
  }

  def removeProvider(sessId: String, provider: String): Future[ResultOk] = {
    Future.fromTry(Try(userSessionFactory(sessId))).flatMap {
      case Some(us) =>
        ProviderHelper.removeProvider(provider, us.nodeId)
      case None => Future.successful(problem(UnknownSession))
    }
  }

  def addProvider(sessId: String, provider: String): Future[ResultOk] = {
    Future.fromTry(Try(userSessionFactory(sessId))).flatMap {
      case Some(us) =>
        ProviderHelper.addProvider(provider, us.nodeId)
      case None => Future.successful(problem(UnknownSession))
    }
  }

  def fetchMessage(sessId: String, id: Long): Future[Result[RpcDetailedDisplayMessage]] = tryResult {
    userSession(sessId).map { implicit us =>
      if (id != brokenDisplayMessage.id) {
        val savedMessage = us.messages.get(id)
        import us.guidToSavedMessage
        MessageExpander.expand(savedMessage)
      } else {
        Future.successful(RpcDetailedDisplayMessage.defaultInstance)
      }
    }
  }.toFutureResult

  def listPaywallCategories(sessId: String): Result[Seq[PaywallCategory]] =
    userSession(sessId).map { us =>
      PaywallHelper.getPaywallCategories(us.nodeId.id)
    }


  def setPaywallCategory(sessId: String, paywallCategory: PaywallCategory): Future[ResultOk] = {
    Future.fromTry(Try(userSessionFactory(sessId))).flatMap {
      case Some(us) =>
        PaywallHelper.setPaywallCategory(us.nodeId, paywallCategory)
      case None => Future.successful(problem(UnknownSession))
    }
  }

  def deletePaywallCategory(sessId: String, paywallCategory: String): Future[ResultOk] = {
    Future.fromTry(Try(userSessionFactory(sessId))).flatMap {
      case Some(us) =>
        PaywallHelper.deletePaywallCategory(us.nodeId, paywallCategory)
      case None => Future.successful(problem(UnknownSession))
    }
  }

  def sendNewContactMessage(sessId: String,
                            msg: NewContactMessage): Future[ResultOk] = {
    Future.fromTry(Try(userSessionFactory(sessId))).flatMap {
      case Some(us) =>
        ContactHelper.sendRequest(us, msg)
      case None => Future.successful(problem(UnknownSession))
    }
  }

  /**
   * This cheekily will return the contact avatar of anyone 'on' this machine.
   * You don't have to be a contact of the person to get their avatar.
   *
   * @param sessId ignored in this instance
   * @param contact
   * @return
   */
  def avatar(sessId: String, contact: String): Result[Option[Array[Byte]]] = tryResult {
    val r = contactService.findContact(contact).flatMap(_.avatar)
    success(r.map(_.toArray))
  }

  def setAvatar(sessId: String, avatar: Option[Array[Byte]]): ResultOk = {
    userSession(sessId).map { us =>

      val updatedLoggedInUser = us.loggedInUser.update(avatar.map(ByteString.apply))
      val updatedUs = us.update(updatedLoggedInUser)
      userSessionFactory.update(sessId, updatedUs)
      true
    }
  }


  override def listContacts(in: SessId): Future[ResultSeqContacts] = Future {
    userSession(in.sessId).map { us =>
      us.loggedInUser.contacts.map { contact =>

        val details = us.loggedInUser.getCategoryDetails(contact.name).get

        val amountContactHasToPay =
          identityService
            .getCurrentCharge(
              contact.name,
              details.nameCategoryForContact
            )

        val amountUserHasToPay =
          identityService
            .getCurrentCharge(
              us.nodeId.id,
              details.contactCategoryForName
            )

        RpcContact(
          contact.name,
          contact.avatar.map(bs => GByteString.copyFrom(bs.toArray)),
          details.contactCategoryForName,
          amountUserHasToPay,
          details.nameCategoryForContact,
          amountContactHasToPay
        )
      }
    }
  } map massage[Seq[RpcContact], ResultSeqContacts]

  override def countIdentities(in: FilterMessage): Future[ResultCountIdentities] = Future.fromTry(Try {
    massage[Long, ResultCountIdentities](countIdentities(in.filter))
  })

  override def fetchIdentities(in: PageParam): Future[ResultFetchIdentities] = Future.fromTry(Try {
    massage[Seq[String], ResultFetchIdentities](fetchIdentities(in.filter, in.startIndex, in.limit))
  })

  override def logout(in: SessId): Future[ResultLogout] = Future.successful {
    massage[Boolean, ResultLogout](logout(in.sessId))
  }

  override def login(in: LoginMessage): Source[ResultLogin, NotUsed] = {
    login(in.sessId,
      in.user,
      in.tag,
      in.authType,
      in.password,
      in.subscription)
  }


  override def createUser(in: CreateUserMessage): Future[ResultCreateUser] =
    createUser(in.sessId, in.user, in.password, in.password2, in.subscription).map(massage[String, ResultCreateUser])

  override def createUserBasic(in: CreateUserBasicMessage): Future[ResultCreateUser] =
    createUserBasic(
      sessId = in.sessId,
      details = SignerVerifierDetails(
        NodeIdTag(in.user),
        in.publicKeyId.map(_.getBytes(StandardCharsets.UTF_8)),
        TypedPublicKey( PublicKey(in.publicKey.toByteArray), in.keyType),
        in.securityLevel,
        in.signerType,
        in.interactionType,
      ),
      subOpt = in.subscription,
    ).map(massage[String, ResultCreateUser])

  override def countMessages(in: SessId): Future[ResultCountMessages] = Future.successful {
    massage[Int, ResultCountMessages](countMessages(in.sessId))
  }

  override def fetchMessages(in: FetchMessagesMessage): Source[DisplayMessage, NotUsed] =
    Source.future(fetchMessages(in.sessId, in.page, in.limit, in.newestFirst) flatMap {
      case Right(msgs) => Future.successful(msgs.toList)
      case Left(problem) => Future.failed(new RuntimeException(problem.msg))
    }).mapConcat(identity)

  override def fetchMessage(in: MessageIdMessage): Future[ResultDetailedDisplayMessage] = {
    fetchMessage(in.sessId, in.id).map(massage[RpcDetailedDisplayMessage, ResultDetailedDisplayMessage])
  }

  override def balance(in: SessId): Future[ResultBalance] = Future.successful {
    massage[Long, ResultBalance](balance(in.sessId))
  }

  override def sendEncryptedMessage(in: DecryptedMessageMessage): Future[ResultSendEncryptedMessage] =
    sendEncryptedMessage(in.sessId, in.decryptedMsg.get).map(massage[Boolean, ResultSendEncryptedMessage])

  override def sendNewContactMessage(in: NewContactMessageMessage): Future[ResultSendNewContactMessage] =
    sendNewContactMessage(in.sessId, in.newContact.get).map(massage[Boolean, ResultSendNewContactMessage])

  override def listProviders(in: SessId): Future[ResultRegisteredProviders] = Future.successful {
    listProviders(in.sessId)
  } map massage[Seq[RegisteredProvider], ResultRegisteredProviders]

  override def removeProvider(in: ProviderMessage): Future[ResultRemoveProvider] = removeProvider(in.sessId, in.provider).map(massage[Boolean, ResultRemoveProvider])

  override def addProvider(in: ProviderMessage): Future[ResultAddProvider] = addProvider(in.sessId, in.provider).map(massage[Boolean, ResultAddProvider])

  override def listPaywallCategories(in: SessId): Future[SeqPaywallCategoryResult] = Future.successful {
    massage(listPaywallCategories(in.sessId))
  }

  override def setPaywallCategory(in: SetPaywallCategoryMessage): Future[ResultSetPaywallCategory] =
    setPaywallCategory(in.sessId, in.category.get).map(massage[Boolean, ResultSetPaywallCategory])

  override def deletePaywallCategory(in: DeletePaywallCategoryMessage): Future[ResultDeletePaywallCategory] =
    deletePaywallCategory(in.sessId, in.category).map(massage[Boolean, ResultDeletePaywallCategory])

  override def deleteMessage(in: MessageIdMessage): Future[ResultDeleteMessage] = Future.successful {
    massage[Boolean, ResultDeleteMessage](deleteMessage(in.sessId, in.id))
  }

  override def avatar(in: AvatarMessage): Future[ResultAvatar] = Future.successful {
    massage[Option[Array[Byte]], ResultAvatar](avatar(in.sessId, in.contact))
  }

  override def setAvatar(in: BytesMessage): Future[ResultSetAvatar] = Future.successful {
    massage[Boolean, ResultSetAvatar](setAvatar(in.sessId, in.bs.map(_.toByteArray)))
  }

  override def getProviderCharge(in: SessId): Future[ResultProviderCharge] = Future.successful {
    userSession(in.sessId) map { us =>
      chargeHelper.getProviderCharge(us.loggedInUser.name)
    }
  } map massage[Option[Option[Long]], ResultProviderCharge]

  override def setProviderCharge(in: SetProviderMessage): Future[ResultProviderCharge] = {
    userSession(in.sessId) match {
      case Left(p) => Future.successful(problem(p.toAppError))
      case Right(us) => chargeHelper.setProviderCharge(us.nodeId, in.chargeOpt)
    }
  } map massage[Option[Option[Long]], ResultProviderCharge]

  override def listRegisteredProviders(in: PageParam): Future[ResultRegisteredProviders] = Future {
    ProviderHelper.listRegisteredProviders(in.startIndex, in.limit) match {
      case Failure(e) => ResultRegisteredProviders(ResultRegisteredProviders.Either.Problem(AppError("RegisteredProvidersError", e.getMessage)))
      case Success(providers) => ResultRegisteredProviders(ResultRegisteredProviders.Either.Value(RegisteredProviders(
        providers map { provider =>
          RegisteredProvider(provider.name, provider.amount)
        }
      )))
    }
  }

  override def getEvents(in: SessId): Source[Event, akka.NotUsed] = {
    userSession(in.sessId) match {
      case Left(_) => Source.empty
      case Right(us) => us.createEventSource
    }

  }

  override def costToContact(in: CostToContactMessage): Future[ResultCostToContact] = Future {
    userSession(in.sessId) flatMap { us =>
      chargeHelper.getTotalChargeFromTo(us.nodeId.id, in.user).asResult
    }
  } map massage[Long, ResultCostToContact]

  override def uploadAttachment(in: Source[UploadFileMessage, NotUsed]): Future[UploadFileDetails] = {

    val inputStreamSink: Sink[ByteString, InputStream] =
      asInputStream(FiniteDuration(30, TimeUnit.SECONDS))

    val headerFut = Promise[UploadFileDetails]()

    val minusHeaderSource = in.prefixAndTail(1).flatMapConcat({
      case (headerSeq: Seq[UploadFileMessage], remainingStream: Source[UploadFileMessage, NotUsed]) =>
        //deal with header
        val newHeader = headerSeq.head.getDetails
        headerFut.success(newHeader)

        remainingStream
          .map(gbs => ByteString(gbs.options.chunk.get.part.asReadOnlyByteBuffer()))

    })

    val input: InputStream = minusHeaderSource.runWith(inputStreamSink)

    headerFut.future.map { newHeader =>
      userSessionFactory(newHeader.sessId) match {
        case None =>
          throw new IllegalArgumentException("Bad session Id, not loggged in")

        case Some(us) =>
          val attachments = attachmentsService.localAttachmentsService.forUser(us.loggedInUser.name)
          val guid = Guid(newHeader.guid.toByteArray)

          val sessionAttachments = us.messageAttachments(guid)
          val size = attachments.saveEncrypted(guid, newHeader.fileName, newHeader.fileType, sessionAttachments.key, input).get
          val newDetails = AttachmentDetails(newHeader.fileName, newHeader.fileType, size)
          us.messageAttachments.addDetails(guid, newDetails)

          newHeader
      }
    }

  }

  override def downloadAttachment(inMsg: DownloadFileDetailsMessage): Source[UploadFileMessage, NotUsed] = {

    userSessionFactory(inMsg.sessId) match {
      case None => Source.failed(new IllegalArgumentException("Bad session Id, not loggged in"))
      case Some(us) =>
        val in = inMsg.details.get

        //val attachments = attachmentsService.forUser(us.loggedInUser.name)
        val guid = Guid(in.guid.toByteArray)

        us.messageAttachments.getAttachments(guid) match {
          case Some(sessionAttachments) if sessionAttachments.attachmentDetails.exists(_.name == in.fileName) =>

            attachmentsService.find(
              sessionAttachments.attachmentProvider,
              guid,
              in.fileName) match {

              case None =>
                Source.failed(new IllegalArgumentException(s"No such file ${in.fileName} ${guid}"))

              case Some(encryptedAttachment) =>
                val encryptedInputStream = encryptedAttachment.input

                val clearInputStream = CBCStreamEncryption.decrypt(
                  encryptedInputStream,
                  sessionAttachments.key,
                  encryptedAttachment.iv
                )


                val byteStringSource: Source[ByteString, Future[IOResult]] = StreamConverters.fromInputStream(() => clearInputStream)
                val uploadFileMessageSource: Source[UploadFileMessage, Future[IOResult]] = byteStringSource.map(bs => {

                  UploadFileMessage(
                    UploadFileMessage.Options.Chunk(Chunk(GByteString.copyFrom(bs.asByteBuffer))))
                })

                uploadFileMessageSource.mapMaterializedValue(_ => NotUsed)

            }
          case Some(_) =>
            Source.failed(new IllegalArgumentException(s"No such file in session ${in}"))
          case None =>
            Source.failed(new IllegalArgumentException(s"No such guid in session ${in.guid} ${in.fileName}"))

        }
    }
  }


  override def deleteAttachment(in: DownloadFileDetailsMessage): Future[ResultDeleteAttachment] = Future {
    userSessionFactory(in.sessId) match {
      case None => problem(noSessionError)
      case Some(us) =>

        val attachments = attachmentsService.localAttachmentsService.forUser(us.loggedInUser.name)
        val details = in.details.get
        val guid = Guid(details.guid.toByteArray)

        val numDeleted = attachments.delete(
          guid,
          details.fileName
        ).get
        success(numDeleted == 1)
    }
  } map massage[Boolean, ResultDeleteAttachment]

  override def hostDecrypt(in: HostEncrypDecryptMessage): Future[ResultHostDecrypt] = {
    hostDecrypt(in.messageToEnDecrypt).map(massage[String, ResultHostDecrypt])
  }

  override def hostEncrypt(in: HostEncrypDecryptMessage): Future[ResultHostEncrypt] =
    hostEncrypt(in.messageToEnDecrypt).map(massage[String, ResultHostEncrypt])

  import NodeIdentityOps._

  def hostDecrypt(base64EncodedECEncryption: String): Future[Result[String]] = for {
    ec <- Future.fromTry(Try(ECEncryption.fromBase64Encoded(base64EncodedECEncryption)))
    decrypted <- ECEncryption.decrypt(ec, hostingNode.sharedSecretCreator(SignerSecurityLevels.SecurityLevel1))
  } yield success(new String(decrypted, StandardCharsets.UTF_8))


  def hostEncrypt(stringToEncrypt: String): Future[Result[String]] = for {
    asBytes <- Future.fromTry(Try(stringToEncrypt.getBytes(StandardCharsets.UTF_8)))
    ec = ECEncryption.encrypt(hostingNode.defaultNodeVerifier.verifier.typedPublicKey, asBytes)
    ecEnc = success(ec.encodedAsBase64String)
  } yield ecEnc

  override def clientBalance(in: ClientBalanceMessage): Future[ResultClientBalance] = Future {
    userSessionFactory(in.sessId) match {
      case None => problem(noSessionError)
      case Some(us) =>
        us.userAdaWallet.w.clientBalance(in.client) match {
          case None => success(0L)
          case Some(bal) => success(bal.value)
        }
    }
  } map massage[Long, ResultClientBalance]

  override def sendToTrustee(in: ToTrusteeMessage): Future[ResultTrusteeMessage] = {
    userSessionFactory(in.sessId) match {
      case None => Future.successful(problem(noSessionError))
      case Some(us) =>
        val failMsg = s"Failed to transfer ${in.amount} to ${in.trustee}"
        us.userAdaWallet.payTrustee(in.amount, in.trustee) match {
          case Failure(e) =>
            log.error(failMsg, e)
            Future.successful(problem(transferError(failMsg)))
          case Success(f: Future[InternalTxResult]) => f map {
            case _: InternalCommit => success(true)
            case _ => problem(transferError(failMsg))
          }
          case _ => Future.successful(problem(transferError(failMsg)))
        }
    }
  } map massage[Boolean, ResultTrusteeMessage]

  override def withdrawFromClientAccount(in: ClientWithdrawMessage): Future[ResultWithdrawFromClientAccount] = {
    userSessionFactory(in.sessId) match {
      case None => Future.successful(problem(noSessionError))
      case Some(us) =>
        val failMsg = s"Failed to transfer ${in.amount} to ${in.client}"

        val enc = us.userAdaWallet.w.encumberToIdentity(0, in.client)
        us.userAdaWallet.withdrawClientMoney(in.amount, in.client, enc) match {
          case Failure(e) =>
            log.error(failMsg, e)
            Future.successful(problem(transferError(failMsg)))
          case Success(f: Future[InternalTxResult]) => f map {
            case _: InternalCommit => success(true)
            case _ => problem(transferError(failMsg))
          }
          case _ => Future.successful(problem(transferError(failMsg)))
        }
    }
  } map massage[Boolean, ResultWithdrawFromClientAccount]

  override def chargeClientAccount(in: ChargeClientsAccountMessage): Future[ResultChargeClientsAccount] = Future {
    (in.service, in.client, in.refund) match {
      case (Some(proxyToken: ApiToken), Some(clientToken: ApiToken), false) =>
        apiHostHelper.charge(proxyToken, clientToken)
      case (Some(proxyToken: ApiToken), Some(clientToken: ApiToken), true) =>
        apiHostHelper.refund(proxyToken, clientToken)
      case _ => problem(noTokensError)
    }
  } map massage[Boolean, ResultChargeClientsAccount]

  override def transferBetweenClientAccounts(in: BetweenClientsTransferMessage): Future[ResultTransferBetweenClientAccounts] = Future {
    userSessionFactory(in.sessId) match {
      case None => problem(noSessionError)
      case Some(us) =>

        us.userAdaWallet.transferBetweenClients(in.amount, in.fromClient, in.toClient) match {
          case Failure(e) =>
            val failMsg = s"Failed to transfer ${in.amount} from ${in.fromClient} to ${in.toClient}"
            log.error(failMsg, e)
            problem(transferError(failMsg))
          case Success(_) => success(true)
        }
    }
  } map massage[Boolean, ResultTransferBetweenClientAccounts]

  /**
   * Used by pinned or 'wrapped' currencies to withdraw to whatever the external system is
   * e.g. to the ADA blockchain, or to a bank ac.
   * @param in
   * @return
   */
  override def currencyWithdrawal(in: WithdrawCurrencyMessage): Future[ResultCurrencyWithdrawal] = {
    userSessionFactory(in.sessId) match {

      case None =>
        Future.successful(problem(noSessionError))

      case Some(us) =>
        val exitInstructionsPlusUser = s"${in.exitInstructions}:${us.nodeId.id}"
        val out = TxOutput(in.amount, OracleWithdrawEncumbrance(exitInstructionsPlusUser))
        //todo should MaxValue be None or -1????
        val transfer = Transfer(in.amount, Int.MaxValue, Seq(out))
        us.userAdaWallet.payAsync(transfer).map {
          case _: InternalCommit => success(true)
          case _: InternalTempNack => problem(tempNackError)
          case error: InternalNack =>  problem(AppError("InternalError", error.txMsg.msg))
        }
    }
  } map massage[Boolean, ResultCurrencyWithdrawal]

  override def getHashedIdentityBearerAttribute(in: SessId): Future[ResultGetHashedIdentityBearerAttribute] = Future {
    userSession(in.sessId) map {
      us =>
        identityService
          .attribute(us.loggedInUser.name, BearerAuthorization)
          .map(_.asInstanceOf[BearerAuthorizationAttribute])
          .map(_.hashOfSecret.hashedSecretAsString)
    }
  }.map(massage[Option[String], ResultGetHashedIdentityBearerAttribute])


  override def createIdentityBearerAttribute(in: SessId): Future[ResultCreateIdentityBearerAttribute] = {
    userSessionFactory(in.sessId) match {
      case None =>
        Future.successful(problem("No session found, not logged in"))
      case Some(us) =>
        val secret = AESDetails.generateAESKey()
        val hashed = HashedSecret.hashSecret(secret.value)
        // only one attribute 'live' at a time.
        val tx = SetAttribute(us.loggedInUser.name, BearerAuthorizationAttribute(hashed))
        Utils.simplySignedIdentityLedgerItem(us.nodeId, tx) flatMap { le =>
          sendTx(le).map {
            case _: InternalCommit =>
              success(HashedSecret.asUrlSafeBase64String(secret.value))
            case x =>
              log.debug("failed to set attribute {}", x)
              problem("Failed to set attribute. Try again.")
          }
        }
      }
  }.map(massage[String, ResultCreateIdentityBearerAttribute])

  override def listClientBalances(in: ListClientBalancesMessage): Future[ResultClientBalancesMessage] = Future {

    userSession(in.sessId).map { us =>
      us.userAdaWallet
        .w
        .clientBalances(in.startIndex, in.pageSize)
        .map(cb => ClientBalance(cb.balance.value, cb.client, 0))
    }
  }.map(r => massage(r))

  override def getEmail(in: SessId): Future[ResultGetEmail] = {
    Future {
      userSession(in.sessId).map {
        us =>
          identityService
            .attribute(us.loggedInUser.name, SystemAttributeCategory.Email)
            .collect { case EmailAttribute(email) => email }
      }
    }.map(massage[Option[String], ResultGetEmail])
  }

  override def setEmail(in: SetEmailMessage): Future[ResultSetEmail] = {
    userSession(in.sessId).map {
      us =>
        val tx = SetAttribute(us.loggedInUser.name, EmailAttribute(in.email))
        Utils.simplySignedIdentityLedgerItem(us.nodeId, tx) flatMap { le =>
          sendTx(le).map {
            case _: InternalCommit =>
              success(in.email)
            case x =>
              log.debug("failed to set attribute {}", x)
              problem("Failed to set attribute (email). Try again.")
          }
        }
    }.fold(p => Future.successful(problem(p)), identity)
      .map(massage[String, ResultSetEmail])
  }

  override def getPreparedForSystemUpdate(in: GetPreparedForSystemUpdateMessage): Future[ResultGetPreparedForSystemUpdate] = {
    userSession(in.sessId).map { someLoggedInUser =>
      Future.sequence(Seq(
        systemServiceQuery.getSignatories(in.uniqueUpdateId),
        systemServiceQuery.isUpdatePreparationComplete(in.uniqueUpdateId)
      )).map {
        case Seq(signatories: Seq[String] @nowarn, isPrepared: Boolean) =>
          success(PreparedForSystemUpdate(Option(SeqString(signatories)), isPrepared))
      }
    }.fold(p => Future.successful(problem(p)), identity)
      .map(massage[PreparedForSystemUpdate, ResultGetPreparedForSystemUpdate])
  }

  override def respondSystemUpdateProposal(in: SystemUpdateProposalResponseMessage): Future[ResultRespondSystemUpdateProposal] = {
    userSession(in.sessId).map {
      us =>
        val currentHeight = getCurrentBlockHeight()

        // Set prepared should happen automatically
        // so this is really for test
        val tx = if(in.preparedOrCancel) {
          PreparedToUpdate(
            currentHeight,
            in.uniqueUpdateId)
        } else {
          CancelUpdate(
            currentHeight,
            in.uniqueUpdateId)
        }

        Utils.signedSystemLedgerItem(us.nodeId, tx) flatMap { ledgerItem =>
          sendTx(ledgerItem).map {
            case _: InternalCommit =>
              success(true)
            case nack: InternalNack =>
              problem(AppError(nack.txMsg.code, nack.txMsg.msg))
            case tmpNack: InternalTempNack =>
              problem(AppError(tmpNack.txMsg.code, tmpNack.txMsg.msg))
          }
        }
    }.fold(p => Future.successful(problem(p)), identity)
      .map(massage[Boolean, ResultRespondSystemUpdateProposal])

  }

  override def createSystemUpdateProposal(
                                           in: CreateSystemUpdateProposalMessage
                                         ): Future[ResultCreateSystemUpdateProposal] = {
    userSession(in.sessId).map {
      us =>
        val currentHeight = getCurrentBlockHeight()

        Try(in.checksumBase64.fromBase64Str) match {
          case Failure(ex) =>
            log.debug(s"Failed to decode checksum", ex)
            Future.successful(problem(s"Cannot base 64url decode ${in.checksumBase64} to array"))
          case Success(chksumAry) =>

            val tx = UpdateNotification(
              currentHeight,
              in.thresholdBlockHeight,
              in.uniqueUpdateId,
              in.downloadUrl,
              Digest32(chksumAry))

            Utils.signedSystemLedgerItem(us.nodeId, tx) flatMap { ledgerItem =>
              sendTx(ledgerItem).map {
                case _: InternalCommit =>
                  success(true)
                case nack: InternalNack =>
                  problem(AppError(nack.txMsg.code, nack.txMsg.msg))
                case tmpNack: InternalTempNack =>
                  problem(AppError(tmpNack.txMsg.code, tmpNack.txMsg.msg))
              }
            }
        }
    }.fold(p => Future.successful(problem(p)), identity)
      .map(massage[Boolean, ResultCreateSystemUpdateProposal])

  }
}
