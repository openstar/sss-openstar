package sss.openstar.rpc

import sss.ancillary.Guid
import sss.openstar.rpc.MessageAdapter.AdaptedMessage
import sss.openstar.ui.rpc.{DisplayMessage, MilliUtils, SeqLong}

object DisplayMessageAdapter {

  object Ops {
    implicit class ToMsgDisplay(val adapted: AdaptedMessage) extends AnyVal {
      def toDisplay(guidToId: Guid => Long): DisplayMessage = DisplayMessage(
        adapted.index,
        adapted.coins,
        adapted.from,
        adapted.description,
        MilliUtils.getAsMilli(adapted.receivedAt),
        adapted.msgType.id,
        DisplayMessage.Either.ChildIds(SeqLong(adapted.children.map(guidToId)))
      )
    }
  }

}



