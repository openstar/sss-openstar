package sss.openstar.rpc

import sss.openstar.ui.rpc.Result

import scala.concurrent.{ExecutionContext, Future}

object RpcOps {

  implicit class ResultF[T](val result: Result[Future[T]]) extends AnyVal {
    def toFutureResult(implicit ec: ExecutionContext): Future[Result[T]] =
      result match {
        case Left(p) => Future.successful(Left(p))
        case Right(valueF) =>
          valueF.map(Right(_))
      }
  }
}
