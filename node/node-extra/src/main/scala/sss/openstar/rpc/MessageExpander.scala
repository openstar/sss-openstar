package sss.openstar.rpc

import com.google.protobuf.ByteString
import sss.ancillary.FailWithException.fail
import sss.ancillary.Logging
import sss.openstar.account.{NodeIdTag, NodeVerifier}
import sss.openstar.Currency
import sss.openstar.identityledger.IdentityServiceQuery
import sss.openstar.message.MessageInBox.SavedMessage
import sss.openstar.message.payloads.MessageEcryption.EncryptedMessage
import sss.openstar.message.payloads.{CompletedNewContactMessage, MessageEcryption, NewContactMessage}
import sss.openstar.message.{DecodedMessageCache, UtxoQuery}
import sss.openstar.rpc.UserSessionFactory.LookUpMessageByGuid
import sss.openstar.ui.rpc.RpcDetailedNewContactMessage.InviteePaywalls
import sss.openstar.ui.rpc.RpcDetailedNewContactMessage.InviteePaywalls.PaywallCategories
import sss.openstar.ui.rpc._

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

object MessageExpander extends Logging {


  def expand(savedMessage: SavedMessage)(
    implicit us: UserSession[_ <: Currency],
    identityServiceQuery: IdentityServiceQuery,
    txIndexQuery: UtxoQuery,
    lookup: LookUpMessageByGuid,
    ec: ExecutionContext): Future[RpcDetailedDisplayMessage] = Future {
    DecodedMessageCache(savedMessage.msg).getOrElse(fail(s"Can't decode message ${savedMessage.msg.msgPayload.payloadType}"))
  } flatMap {
    case m: EncryptedMessage => expand(savedMessage, m)
    case m: NewContactMessage => expand(savedMessage, m)
    case m: CompletedNewContactMessage => expand(savedMessage, m)
    //case m: TicTacToeGameMessage => expand(savedMessage, m)
    //case m: NewQuorumMemberMessage => expand(savedMessage, m)

  }


  def expand(savedMessage: SavedMessage,
             ncm: CompletedNewContactMessage)(implicit lookupMsgByGuid: LookUpMessageByGuid,
                                              userSession: UserSession[_ <: Currency],
                                              identityServiceQuery: IdentityServiceQuery,
                                              ec: ExecutionContext): Future[RpcDetailedDisplayMessage] = Future {

    val parentIdOpt = savedMessage
      .msg
      .parentGuid
      .flatMap(lookupMsgByGuid)
      .map(_.index)

    val invitorCurrentAmount =
      identityServiceQuery
        .getCurrentCharge(
          ncm.invitor,
          ncm.invitorCategory

        )

    val inviteeCurrentAmount =
      identityServiceQuery
        .getCurrentCharge(
          ncm.invitee,
          ncm.inviteeCategory
        )


    val subMessage = RpcDetailedNewContactMessage(
      ncm.invitorCategory,
      invitorCurrentAmount,
      ncm.invitee,
      InviteePaywalls.PaywallCategory(PaywallCategory(ncm.inviteeCategory, inviteeCurrentAmount))
    )

    val details = RpcDetailedDisplayMessage.Details.Contact(subMessage)
    RpcDetailedDisplayMessage(
      savedMessage.index,
      ncm.invitor,
      "",
      MilliUtils.getAsMilli(savedMessage.savedAt),
      parentIdOpt,
      savedMessage.msg.msgPayload.payloadType,
      details)
  }

  def expand(savedMessage: SavedMessage,
             ncm: NewContactMessage)(implicit lookupMsgByGuid: LookUpMessageByGuid,
                                     userSession: UserSession[_ <: Currency],
                                     identityServiceQuery: IdentityServiceQuery,
                                     ec: ExecutionContext): Future[RpcDetailedDisplayMessage] = Future {

    val userId = userSession.nodeId
    val parentIdOpt = savedMessage.msg.parentGuid.flatMap(lookupMsgByGuid).map(_.index)
    val myPaywalls = identityServiceQuery.paywalls(userId.id).map(pw => PaywallCategory(pw.category, pw.amount.get))

    val currentAmount = identityServiceQuery.getCurrentCharge(
      ncm.invitor,
      ncm.category
    )


    val subMessage = RpcDetailedNewContactMessage(
      ncm.category,
      currentAmount,
      ncm.invitee,
      PaywallCategories(SeqPaywallCategory(myPaywalls)),
    )

    val details = RpcDetailedDisplayMessage.Details.Contact(subMessage)


      RpcDetailedDisplayMessage(
        savedMessage.index,
        ncm.invitor,
        "",
        MilliUtils.getAsMilli(savedMessage.savedAt),
        parentIdOpt,
        savedMessage.msg.msgPayload.payloadType,
        details)

  }

  def expand(savedMessage: SavedMessage,
             enc: EncryptedMessage)(implicit userSession: UserSession[_ <: Currency],
                                    identityServiceQuery: IdentityServiceQuery,
                                    txIndexQuery: UtxoQuery,
                                    lookupMsgByGuid: LookUpMessageByGuid,
                                    ec: ExecutionContext
            ): Future[RpcDetailedDisplayMessage] = Future {

    val userId = userSession.nodeId
    val lookupId: NodeIdTag => NodeVerifier = MessageEcryption.createLookupId(identityServiceQuery)

    val textWithSecretF = enc.decrypt(userId, lookupId)

    textWithSecretF.map { textWithSecret =>
      textWithSecret.attachmentsOpt.foreach { attachments =>
        userSession.messageAttachments(savedMessage.msg.guid, attachments)
      }

      val files: Seq[FileData] = textWithSecret
        .attachmentsOpt
        .map(details => details.attachmentDetails.map(detail => FileData(detail.name, detail.mimeType, detail.size)))
        .getOrElse(Seq.empty)


      val children = enc.childGuids
        .map(lookupMsgByGuid)
        .collect { case Some(goodChild) => goodChild.index }


      val readReceipts = enc
        .receivers
        .map(_.identity)
        .zip(enc.watch.map(txIndexQuery))
        .map(MessageAdapter.toReadReceipt(enc.bountyOpt.map(_.bountyReturnBlockHeight)))

      require(savedMessage.msg.guid == enc.guid, s"${enc.guid} nmust equal ${savedMessage.msg.guid}, developer error")

      val parentIdOpt = savedMessage.msg.parentGuid.flatMap(lookupMsgByGuid).map(_.index)
      //log.debug(s"msg i: ${savedMessage.index} g ${savedMessage.msg.guid} ${savedMessage.msg.parentGuid} id : $parentIdOpt")

      val tos = enc.receivers.map(_.identity).toSet
      require(enc.receivers.size == tos.size, s"${enc.receivers.map(_.identity)} has duplicates!")


      val subMessage = RpcDetailedEncryptedMessage(
        enc.bountyOpt.map(_.amountOfBounty),
        tos.toSeq,
        textWithSecret.text,
        files,
        readReceipts,
        children,
        ByteString.copyFrom(enc.guid.value)
      )

      val details = RpcDetailedDisplayMessage.Details.Encrypted(subMessage)
      RpcDetailedDisplayMessage(
        savedMessage.index,
        enc.author,
        if (textWithSecret.text.length > 50) textWithSecret.text.substring(0, 50) else textWithSecret.text,
        MilliUtils.getAsMilli(savedMessage.savedAt),
        parentIdOpt,
        savedMessage.msg.msgPayload.payloadType,
        details)
    }
  }.flatten


}
