package sss.openstar.rpc

import sss.openstar.account.{NodeIdentityManager, SignerFactory}
import sss.openstar.ui.rpc
import sss.openstar.ui.rpc._

import java.nio.charset.StandardCharsets
import scala.concurrent.{ExecutionContext, Future}


class SignerRpcImpl(implicit signerFactory: SignerFactory,
                    nodeIdentityManager: NodeIdentityManager,
                    ec: ExecutionContext) extends SignerService {


  override def userSigners(in: UserMessage): Future[ResultUserSignersDetails] = {
    for {
      details <- nodeIdentityManager.signerDetails(in.user)
      resultDetails = details.map(detail => ResultSignerDetails.of(
        detail.nodeIdTag.nodeId,
        detail.nodeIdTag.tag, Some(rpc.SignerDescriptor.of(
          detail.signerDescriptor.keyType,
          detail.signerDescriptor.securityLevel,
          detail.signerDescriptor.signerType,
          detail.signerDescriptor.interactionType
        )),
        detail.externalKeyId.map(new String(_, StandardCharsets.UTF_8)).getOrElse(""))
      )
    } yield ResultUserSignersDetails.of(resultDetails)
  }

  override def allSigners(in: AllSignersMessage): Future[ResultAllSignerDescriptors] = Future {
    val descs = signerFactory.signerDescriptors
    val descriptors = descs.map { detail =>
      rpc.SignerDescriptor.of(
        detail.keyType,
        detail.securityLevel,
        detail.signerType,
        detail.interactionType
      )
    }
    ResultAllSignerDescriptors.of(descriptors)
  }


}
