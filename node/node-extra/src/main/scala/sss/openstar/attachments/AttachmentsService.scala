package sss.openstar.attachments

import java.io.InputStream

import sss.ancillary.{Guid, Logging}
import sss.openstar.UniqueNodeIdentifier
import sss.openstar.attachments.AttachmentsService.EncryptedAttachment
import sss.openstar.crypto.CBCEncryption.InitVector

import scala.util.Try



object AttachmentsService {
  case class EncryptedAttachment(input: InputStream,
                                 guid: Guid,
                                 name: String,
                                 mimeType: String,
                                 encryptedLen: Long,
                                 iv: InitVector)
}

class AttachmentsService(
                        localProviderName: String,
                        val localAttachmentsService: LocalAttachmentsService,
                        remoteAttachmentsService: RemoteAttachmentsService) extends Logging {

  require(localProviderName.nonEmpty, s"Local provider name '$localProviderName' must be a valid node id")

  def find(
            provider: UniqueNodeIdentifier,
            guid: Guid,
            name: String): Option[EncryptedAttachment] = Try {

    if(provider == localProviderName) {
      localAttachmentsService.find(guid, name)
    } else {
      remoteAttachmentsService.find(provider, guid, name)
    }
  }.recover {
    case e =>
      log.error(s"Error getting file $provider, $name, $guid", e)
      None
  }.get

}
