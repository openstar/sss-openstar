package sss.openstar.attachments

import java.io.InputStream

import scala.util.Try

case class UniqueLocator(location: String, size: Option[Long] = None)

trait StreamPersister {

  def saveStream(in: InputStream, path: String*): Try[UniqueLocator]
  def getStream(location: UniqueLocator): Try[InputStream]

}

