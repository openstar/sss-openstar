package sss.openstar.attachments

import org.scalatra.{NotFound, ScalatraServlet}
import sss.ancillary.{Guid, Logging}
import sss.openstar.attachments.RemoteAttachmentsServlet._


object RemoteAttachmentsServlet {

  val servletContext = "/attachments"
  val osMimeHeader = "os_mime"
  val osIvHeader = "os_iv"
  val osEncLenHeader = "os_enc_len"
}
class RemoteAttachmentsServlet(
                                localAttachmentsService: LocalAttachmentsService
                              ) extends ScalatraServlet with Logging {

  get("/list") {
    val s = localAttachmentsService.table.map(_.toString())
    s
  }

  get("/:guid/:name") {

    val guidStr = params("guid")
    val guid = Guid(guidStr)
    val nameStr = params("name")

    val attachmentOpt = localAttachmentsService.find(guid, nameStr)

    try {
      attachmentOpt match {
        case None =>
          NotFound(s"$guid/$nameStr not found on this server")
        case Some(attachment) =>

          response.setHeader(osEncLenHeader, attachment.encryptedLen.toString)
          response.setHeader(osIvHeader, attachment.iv.asString)
          response.setHeader(osMimeHeader, attachment.mimeType)
          val wrote = attachment.input.transferTo(response.getOutputStream)
          log.debug(s"wrote $wrote for guid/name $guid $nameStr")
          () // don't handle response twice
      }
    } finally attachmentOpt.foreach(_.input.close())

  }
}
