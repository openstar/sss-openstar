package sss.openstar.attachments

import java.io.{File, FileOutputStream, InputStream}
import java.net.URL
import java.nio.file.Files
import java.util.Date
import com.google.common.io.ByteStreams
import org.eclipse.jetty.server.Server
import sss.ancillary._
import sss.openstar.attachments.RemoteAttachmentsService.GetProviderPrefix
import sss.openstar.common.builders.{ConfigBuilder, IOExecutionContextBuilder, NodeIdTagBuilder, SeedBytesBuilder}
import sss.openstar.nodebuilder._



object Tester extends RequireNodeConfig
  with Logging
  with NodeConfigBuilder
  with ConfigBuilder
  with Configure
  with DbBuilder
  with IOExecutionContextBuilder
  with NodeIdTagBuilder
  with RequireGetProviderPrefix
  with AttachmentsServiceBuilder
  with SeedBytesBuilder {

  override val getProviderPrefix: GetProviderPrefix = _ => Some("http://localhost:8080/")

  val all = Seq (
    "file:///home/alan/run_cc_miner",
    "https://sourceforge.net/p/hsqldb/discussion/73674/"
  )
  def main(args: Array[String]): Unit = {

    val f = new File("~")
    f.getAbsolutePath
    f.getCanonicalPath

    /*val pKey = seedBytes.randomSeed(16)

    val testStr = (0 to 1000).map(i => s"$i").mkString
    val bytes = testStr.getBytes(StandardCharsets.UTF_8)
    val guid = GuidFromByteString()
    val name = "name"
    val mimeType = "application/something"
    val someData = new ByteArrayInputStream(bytes)
    val a = attachmentsService.forUser("hello2")
    val res = a.saveEncrypted(
      guid, name, mimeType, AESEncodedKey(pKey), someData
    )*/
    val s = makeHttp()
    s.start()

    val guid = Guid("m0fuM0UNQ1SSQFThRjOkng")

    attachmentsService.find("remote", guid, "name").foreach { found =>
      val f = Files.createTempFile("out", "txt")
      val fos = new FileOutputStream(f.toFile)
      val n = found.input.transferTo(fos)
      println(s"$n wrote, $found ")
      fos.close()
      found.input.close()
    }
    s.join()

  }

  def makeHttp(): Server = {

    implicit val httpConfig = DynConfig[ServerConfig](nodeConfig.conf.getConfig("httpServerConfig"))


    ServerLauncher(
      remoteAttachmentsServlet.toHandler
      //BEWARE $configName IS ACTUALLY the name you want here
      //ServletContext(s"/db", "", InitServlet(buildDbAccessServlet.get, s"/*")).toHandler,
      //ServletContext("/claim", "", InitServlet(buildClaimServlet.get, "/*")).toHandler
    )
  }

  def testOne(): Unit = {
    all.foreach { u =>
      val f = new URL(u)
      val conn = f.openConnection()
      val fields = conn.getHeaderFields
      fields.forEach((a,b) => {
        println(s"$a , $b")
      })
      readToAry(conn.getInputStream, new Date().getTime.toString)
    }
  }

  def readToAry(in: InputStream, name: String): Unit = {
    val f = new File(name)
    val os = new FileOutputStream(f)
    ByteStreams.copy(in, os)
  }
}
