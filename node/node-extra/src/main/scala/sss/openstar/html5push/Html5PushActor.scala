package sss.openstar.html5push

import akka.actor.{Actor, ActorContext, ActorRef, Props}
import sss.openstar.message.MessageDownloadActor.NewMessageDownloaded
import sss.openstar.network.MessageEventBus

object Html5PushActor {
  def props(html5Push: Html5Push,
            events: MessageEventBus): Props = Props(classOf[Html5PushActor], html5Push, events)

  def apply(props: Props)(implicit actorContext: ActorContext): ActorRef = {
    actorContext.actorOf(props, "Html5PushActor")
  }

}

class Html5PushActor(html5Push: Html5Push,
                     events: MessageEventBus) extends Actor {

  override def preStart(): Unit = {
    super.preStart()
    events subscribe classOf[NewMessageDownloaded]
  }


  override def receive: Receive = {

    case NewMessageDownloaded(who, _) =>
      html5Push.pushAndLog(who, "New Encrypted Message Received")

  }
}
