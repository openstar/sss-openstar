package sss.openstar.html5push

import sss.ancillary.Logging
import sss.db._
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.UniqueNodeIdentifier
import sss.openstar.schemamigration.SqlSchemaNames.ColumnNames._
import sss.openstar.html5push.Subscriptions._
import sss.openstar.schemamigration.SqlSchemaNames

import scala.util.Try

object Subscriptions {

  case class Subscription(endpoint:String, key: String, auth: String)
}

class Subscriptions(implicit db:Db) extends Logging {

  lazy private val table = db.table(SqlSchemaNames.TableNames.subscriptionsTableName)

  def persist(who: UniqueNodeIdentifier, subOpt: Option[Subscription]): Try[Unit] = {

    val dbPlan = subOpt match {
      case Some(sub) =>
        val update = Map(
          whoCol -> who,
          endpointCol -> sub.endpoint,
          keyCol -> sub.key,
          authCol -> sub.auth
        )

        table.find(where(whoCol -> who)) flatMap {
          case Some(row) =>
            table.update(update, where(idCol -> row.id))
          case None =>
            table.insert(update)
        }

      case None =>
        table.delete(where(whoCol -> who))
    }

    dbPlan.dbRunSync.map(_ => ())
  }

  def retrieve(who: UniqueNodeIdentifier): Option[Subscription] = {
    table.find(where(whoCol -> who))
      .dbRunSyncGet
      .map(row =>
        Subscription(row.string(endpointCol), row.string(keyCol), row.string(authCol))
      )
  }

}
