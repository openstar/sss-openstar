package sss.openstar.nodebuilder

import sss.openstar.Currency
import sss.openstar.controller.ApiHostHelper
import sss.openstar.db.Builder.RequireDb
import sss.openstar.wallet.UnlockedWallet

trait ApiHostHelperBuilder[C <: Currency] {
  self: IdentityServiceBuilder
    with RequireDb =>

  val unlockedApiHostWallet: UnlockedWallet[C]

  lazy implicit val apiHostHelper: ApiHostHelper[C] = new ApiHostHelper[C](unlockedApiHostWallet)
}
