package sss.openstar.nodebuilder

import sss.openstar.db.Builder.RequireDb
import sss.openstar.message.{UtxoMonitor, UtxoQuery, UtxoWatch}

trait UtxoMonitorBuilder {

  self: RequireDb
    with RequireNodeConfig
    with RequireActorContext
    with RequireMessageEventBus =>

  lazy val maxRowBeforePrune: Int = nodeConfig.conf.getInt("utxomonitor.maxRowsBeforePrune")
  lazy val utxoMonitor = new UtxoMonitor(maxRowBeforePrune)

  lazy implicit val utxoQuery: UtxoQuery = utxoMonitor.query
  lazy implicit val utxoWatch: UtxoWatch = utxoMonitor.utxoWatch
}
