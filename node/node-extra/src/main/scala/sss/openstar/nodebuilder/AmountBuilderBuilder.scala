package sss.openstar.nodebuilder

import sss.openstar.Currency
import sss.openstar.nodebuilder.Currencies._
import sss.openstar.util.{Amount, AmountBuilder}
import scala.reflect.runtime.universe._

trait AmountBuilderBuilder {

  case class AdaAmount(value: Long) extends Amount {
    override type C = Ada

    override def newAmount(value: Long): Amount = AdaAmount(value)
  }

  case class StarzAmount(value: Long) extends Amount {
    override type C = Starz

    override def newAmount(value: Long): Amount = StarzAmount(value)
  }

  case class TestStarzAmount(value: Long) extends Amount {
    override type C = TestStarz

    override def newAmount(value: Long): Amount = TestStarzAmount(value)
  }


  implicit def amountBuilder[C <: Currency : TypeTag]: AmountBuilder[C] = (value: Long) => typeOf[C] match {
    case t if t =:= typeOf[Ada] => AdaAmount(value)
    case t if t =:= typeOf[Starz] => StarzAmount(value)
    case t if t =:= typeOf[TestStarz] => TestStarzAmount(value)

  }
}
