package sss.openstar.nodebuilder

import sss.openstar.message.{MessageDownloadPersistCache, OutgoingMessageProcessorUtil}

trait MessagingLayer extends UtxoMonitorBuilder
  with ContactServiceBuilder
  with ChargeHelperBuilder
  with RequireMessageQueryHandlerActor
  with GetProviderPrefixBuilder
  with AttachmentsServiceBuilder
  with SendMessageBuilder {

  self: BaseLayer
    with NetworkLayer
    with NodeIdentityLayer
    with MandatoryLedgersLayer =>

  implicit val persistCache: MessageDownloadPersistCache = new MessageDownloadPersistCache
  implicit val outgoingMessageProcessorUtil = new OutgoingMessageProcessorUtil()

}
