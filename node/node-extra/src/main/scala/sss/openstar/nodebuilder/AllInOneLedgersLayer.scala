package sss.openstar.nodebuilder

import sss.openstar.Currency
import sss.openstar.ledger.{LedgerId, Ledgers}

trait AllInOneLedgersLayer
  extends StarzBalanceLedgerBuilder
    with CardanoBalanceLedgerBuilder
    with TestBalanceLedgerBuilder
    with CounterLedgerBuilder
    with MandatoryLedgersLayer {

  self: AppLayer with WalletLayer =>

  import Currencies._

  def currency[C <: Currency](implicit ev: C): C = ev

  def ledgerId[C <: Currency](implicit ev: C): LedgerId = ev match {
    case Ada => cardanoLedger.ledgerId
    case TestStarz => testStarzBalanceLedger.ledgerId
    case Starz => starzBalanceLedger.ledgerId
  }


  override lazy val applicationLedgers  = Seq(
      testStarzBalanceLedger,
      cardanoLedger,
      counterLedger
  )

}
