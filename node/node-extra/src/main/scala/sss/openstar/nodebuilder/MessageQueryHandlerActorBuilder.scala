package sss.openstar.nodebuilder

import akka.actor.ActorRef
import sss.openstar.Currency
import sss.openstar.balanceledger.BalanceLedgerQuery
import sss.openstar.chains.Chains.GlobalChainIdBuilder
import sss.openstar.common.builders.{ConfigBuilder, CpuBoundExecutionContextBuilder, NodeIdentityBuilder}
import sss.openstar.db.Builder.RequireDb
import sss.openstar.ledger.LedgerId
import sss.openstar.message.MessagePaywall
import sss.openstar.message.MessagePaywall.MessagePaywallGenerator
import sss.openstar.message.storeservice.{MessagePersist, MessageQueryHandlerActor}
import sss.openstar.wallet.UnlockedWallet

trait RequireMessageQueryHandlerActor {
  def messageServiceActor: ActorRef
}

trait MessageQueryHandlerActorBuilder[C <: Currency] extends RequireMessageQueryHandlerActor {
  self: RequireDb
    with MessageEventBusBuilder
    with RequireActorContext
    with NodeIdentityBuilder
    with BlockChainBuilder
    with ConfigBuilder
    with IdentityServiceBuilder
    with RequireNetSend
    with WalletBuilder
    with ChargeHelperBuilder
    with GlobalChainIdBuilder
    with CpuBoundExecutionContextBuilder =>

  lazy val minNumBlocksInWhichToClaim =
    prefixedConf.getInt("messagebox.minNumBlocksInWhichToClaim")
  //lazy val chargePerMessage = conf.getInt("messagebox.chargePerMessage")

  implicit lazy val messagePaywallGenerator: MessagePaywallGenerator[C] = MessagePaywall[C](
    () => currentBlockHeight(),
    getLedgerId(),
    getBalanceLedgerQuery(),
    chargeHelper
  )

  def getLedgerId(): LedgerId

  def getBalanceLedgerQuery(): BalanceLedgerQuery[C]

  def getHostUnlockedWallet(): UnlockedWallet[C]

  override lazy val messageServiceActor =
    MessageQueryHandlerActor(
      MessageQueryHandlerActor.props(
        new MessagePersist(),
        messagePaywallGenerator(getHostUnlockedWallet()).validateProviderCharge,
        db,
        messageEventBus,
        send,
        globalChainId).withDispatcher("blocking-dispatcher")
    )
}
