package sss.openstar.nodebuilder

import sss.openstar.contacts.ContactService
import sss.openstar.db.Builder.RequireDb

trait ContactServiceBuilder {
  self: RequireDb =>

  lazy implicit val contactService: ContactService = new ContactService()
}
