package sss.openstar.nodebuilder

import sss.openstar.account.NodeIdentity
import sss.openstar.balanceledger.BalanceLedgerQuery
import sss.openstar.chains.Chains.GlobalChainIdBuilder
import sss.openstar.common.builders.{ConfigBuilder, CpuBoundExecutionContextBuilder, NodeIdentityBuilder}
import sss.openstar.db.Builder.RequireDb
import sss.openstar.ledger.LedgerId
import sss.openstar.nodebuilder.Currencies.{Ada, Starz}
import sss.openstar.wallet.UnlockedWallet

trait StarzMessageQueryHandlerActorBuilder extends MessageQueryHandlerActorBuilder[Starz] {
  self: RequireDb
    with MessageEventBusBuilder
    with RequireActorContext
    with NodeIdentityBuilder
    with BlockChainBuilder
    with ConfigBuilder
    with IdentityServiceBuilder
    with RequireNetSend
    with WalletBuilder
    with ChargeHelperBuilder
    with GlobalChainIdBuilder
    with AllInOneLedgersLayer
    with StarzBalanceLedgerBuilder
    with CpuBoundExecutionContextBuilder =>

  override def getLedgerId(): LedgerId = starzBalanceLedgerId

  override def getBalanceLedgerQuery(): BalanceLedgerQuery[Starz] = starzBalanceLedger

  def getStarzAtomicWalletCacheMap(): AtomicWalletCacheMap[Starz] = createAtomicWalletCacheMap

  lazy implicit val evStarz: AtomicWalletCacheMap[Starz] = getStarzAtomicWalletCacheMap()

  override def getHostUnlockedWallet(): UnlockedWallet[Starz] =
    buildHostNodeUnlockedWallet[Starz](starzBalanceLedgerId)

  lazy val buildStarzWallet: NodeIdentity => UnlockedWallet[Starz] = nId => buildUnlockedWallet[Starz](nId, ledgerId[Starz])

  lazy val unlockedApiHostWallet: UnlockedWallet[Starz] = buildHostNodeUnlockedWallet[Starz](ledgerId[Starz])
}
