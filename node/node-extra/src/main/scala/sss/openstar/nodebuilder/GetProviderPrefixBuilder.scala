package sss.openstar.nodebuilder

import sss.openstar.attachments.RemoteAttachmentsService.GetProviderPrefix
import sss.openstar.identityledger.ProviderAttachmentUrlAttribute


trait RequireGetProviderPrefix {
  val getProviderPrefix: GetProviderPrefix
}

trait GetProviderPrefixBuilder
  extends RequireGetProviderPrefix {

  self: IdentityServiceBuilder =>

  override lazy val getProviderPrefix: GetProviderPrefix = remoteNode => {
    identityService.listAttributes(remoteNode)
      .collectFirst {
        case ProviderAttachmentUrlAttribute(baseUrl) => baseUrl
      }
  }
}
