package sss.openstar.nodebuilder

import akka.actor.{ActorContext, ActorRef}
import sss.ancillary.Logging
import sss.db.Db
import sss.openstar.account.NodeIdentity
import sss.openstar.chains.Chains.GlobalChainIdMask
import sss.openstar.contacts.ContactService
import sss.openstar.controller.Send
import sss.openstar.identityledger.IdentityServiceQuery
import sss.openstar.message.MessageInBoxActors.{CreateMessageInBoxActor, StopMessageInBoxActor}
import sss.openstar.message.MessagePaywall.MessagePaywallGenerator
import sss.openstar.message.{IncomingMessageProcessorUtil, MessageDownloadActor, MessageDownloadPersistCache, MessageInBoxActor, MessageInBoxActorConstructor, MessageInBoxActors, OutgoingMessageProcessorUtil, PaywallChargeRedeemer, UtxoQuery, UtxoWatch}
import sss.openstar.network.MessageEventBus
import sss.openstar.tools.SendTxSupport.SendTx
import sss.openstar.wallet.UnlockedWallet
import sss.openstar.{Currency, UniqueNodeIdentifier}

class MessageHandlingActors(val downloadRef: ActorRef, val inBoxGroupRef: ActorRef, val user: UniqueNodeIdentifier)
    extends Logging {
  def stop(): Unit = {
    log.info("Stop user  messagehandling actors except MessageDownloadActor, needed to deliver notifications")
    // downloadRef ! PoisonPill
    inBoxGroupRef ! StopMessageInBoxActor(user)
  }
}

object MessageHandlingActors {

  def startMessageHandlingActors[C <: Currency](userWallet: UnlockedWallet[C], nodeIdentity: NodeIdentity)(implicit
      db: Db,
      persistCache: MessageDownloadPersistCache,
      utxoWatch: UtxoWatch,
      utxoQuery: UtxoQuery,
      personalPaywallGenerator: MessagePaywallGenerator[C],
      outgoingMessageProcessorUtil: OutgoingMessageProcessorUtil,
      events: MessageEventBus,
      identityService: IdentityServiceQuery,
      contactService: ContactService,
      actorContext: ActorContext,
      send: Send,
      sendTx: SendTx,
      chainId: GlobalChainIdMask
  ): ActorRef = {
    val messageInBoxActorGroup: ActorRef = MessageInBoxActors(
      MessageInBoxActors.props(newMessageInBoxActorWithTrivialValidateBounty)
    )
    addMessageHandlingActors(userWallet, nodeIdentity, messageInBoxActorGroup)
    messageInBoxActorGroup
  }

  def addMessageHandlingActors[C <: Currency](
      userWallet: UnlockedWallet[C],
      nodeIdentity: NodeIdentity,
      messageInBoxActorGroup: ActorRef
  )(implicit
      persistCache: MessageDownloadPersistCache,
      events: MessageEventBus,
      actorContext: ActorContext,
      send: Send,
      chainId: GlobalChainIdMask
  ): MessageHandlingActors = {

    val messageDownloadActorRef = MessageDownloadActor(nodeIdentity.id, persistCache)
    messageInBoxActorGroup ! CreateMessageInBoxActor(userWallet = userWallet, nodeIdentity = nodeIdentity)
    new MessageHandlingActors(messageDownloadActorRef, messageInBoxActorGroup, nodeIdentity.id)
  }

  private def newMessageInBoxActorWithTrivialValidateBounty[C <: Currency](implicit
      db: Db,
      events: MessageEventBus,
      identityService: IdentityServiceQuery,
      contactService: ContactService,
      utxoWatch: UtxoWatch,
      personalPaywallGenerator: MessagePaywallGenerator[C],
      utxoQuery: UtxoQuery,
      sendTx: SendTx,
      outgoingMessageProcessorUtil: OutgoingMessageProcessorUtil
  ): MessageInBoxActorConstructor[C] =
    (userWallet: UnlockedWallet[C], nodeIdentity: NodeIdentity) => { implicit context: ActorContext =>
      val trivialValidateBounty: (Long, UniqueNodeIdentifier) => Boolean = (_, _) => true

      import context.dispatcher

      val messageProcessor =
        IncomingMessageProcessorUtil.createIncomingProcessor(trivialValidateBounty, nodeIdentity, userWallet)

      val personalPaywall: PaywallChargeRedeemer = personalPaywallGenerator(userWallet).validate

      MessageInBoxActor(MessageInBoxActor.props(messageProcessor, nodeIdentity.id, utxoWatch, personalPaywall))
    }
}
