package sss.openstar.nodebuilder

import nl.martijndwars.webpush.PushService
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter
import org.bouncycastle.openssl.{PEMKeyPair, PEMParser}
import sss.openstar.common.builders.{IOExecutionContextBuilder, RequireConfig}
import sss.openstar.db.Builder.RequireDb
import sss.openstar.html5push.{Html5Push, Html5PushActor, Subscriptions}

import java.io.{FileInputStream, InputStreamReader}
import java.security.{KeyPair, Security}

trait Html5PushServiceBuilder {

  self: RequireConfig
    with IOExecutionContextBuilder
    with RequireDb
    with RequireActorContext
    with MessageEventBusBuilder =>

  private lazy val html5PushServer = {
    if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) Security.addProvider(new BouncyCastleProvider)
    val vapid_pem_file = prefixedConf.getString("vapid.file")
    new PushService(load(vapid_pem_file))
  }


  implicit lazy val html5Subscriptions = new Subscriptions()

  private def load(from: String): KeyPair = {
    if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) Security.addProvider(new BouncyCastleProvider)
    val inputStreamReader: InputStreamReader = new InputStreamReader(new FileInputStream(from))
    val pEMParser = new PEMParser(inputStreamReader);
    val pEMKeyPair: PEMKeyPair = pEMParser.readObject().asInstanceOf[PEMKeyPair]
    new JcaPEMKeyConverter().getKeyPair(pEMKeyPair);
  }

  lazy val html5Push: Html5Push = new Html5Push(html5PushServer, html5Subscriptions)

  lazy val startHtml5PushFlag: Boolean = false

  def startHtml5PushActor(): Unit = {
    if (startHtml5PushFlag) {
      // sends push message to registered subscribers on new message
      Html5PushActor(Html5PushActor.props(html5Push, messageEventBus))
    }
  }
}
