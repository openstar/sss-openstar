package sss.openstar.nodebuilder

import scorex.crypto.signatures.PublicKey
import sss.openstar.StartupHooks
import sss.openstar.StartupHooks.NextHook
import sss.openstar.account.NodeIdTag
import sss.openstar.common.builders.ShutdownHookBuilder
import sss.openstar.message.{EndMessageQuery, MessageQuery, UtxoMonitorActor}
import sss.openstar.network.ConnectionFailed
import sss.openstar.tools.ClaimIdentity

import scala.util.{Failure, Success, Try}
import sss.openstar.Node

trait AppLayer extends HttpServerBuilder
  with UtxoMonitorBuilder
  with RequireArgumentParser
  with ShutdownHookBuilder
  with MessagingLayer
  with Node {

  override def loggingSuppressedClasses: Seq[Class[_]] =
    Seq(classOf[ConnectionFailed], classOf[EndMessageQuery], classOf[MessageQuery])

  //will be called by the shutdown hook on exit
  override def shutdown(): Unit = {

    Try(stopHttpServer()) recover {
      case e => log.info("HttpServer Stop Error!", e)
    }

    nodeLayerShutdown()
  }

  def nextHook: NextHook = StartupHooks.terminatingHook

  def startAppLayer(): Unit = {

    startNode()
    claimThisNodeIfNecessary()
    UtxoMonitorActor(utxoMonitor)
    StartupHooks(StartupHooks.props(nextHook))
    //Start IF flag set
    startHttpServerIfFlagSet()
  }

  /**
   * Override this to have a non trivial claim process.
   * @param nodeIdTag
   * @param publicKey
   */
  protected def claimIdentityFromLedgerOwner(nodeIdTag: NodeIdTag, publicKey: PublicKey): Unit = {
    require(nodeIdTag.tag == NodeIdTag.defaultTag, s"First key claimed *must* be ${NodeIdTag.defaultTag}, not ${nodeIdTag.tag}")
    ClaimIdentity.claimRemote(200, claimUrl, nodeIdTag.nodeId, publicKey) match {
      case Failure(e) =>
        log.info(s"Claiming ${nodeIdTag.nodeId} at $claimUrl did not complete successfully.", e)
      case Success(_) =>
        log.info(s"Claiming ${nodeIdTag.nodeId} at $claimUrl completed.")
    }
  }

  protected def claimThisNodeIfNecessary(): Try[Unit] = Try {
    if (chainOriginatorIdsAndKeys.value.forall(_._1 != nodeIdTag.nodeId)) { // not a chain originator
      if(findPublicKeyOpt(nodeIdTag.nodeId, nodeIdTag.tag).isEmpty) { //not already in the ledger
        claimIdentityFromLedgerOwner(nodeIdTag, nodeIdentity.publicKey)
      }
    }
  } recover {
    case e => log.warn(e.getMessage)
  }
}