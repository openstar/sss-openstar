package sss.openstar.nodebuilder

import akka.actor.ActorRef
import org.eclipse.jetty.server.Server
import sss.ancillary.{ArgumentParser, InitServlet, ServerLauncher, ServletContext}
import sss.openstar.StartupHooks.NextHook
import sss.openstar.account.impl.authplus.{AuthPlusAccountSignerCreator, AuthPlusSignerFactory, RemoteKeyApi}
import sss.openstar.account.impl.webauthn.WebAuthnSignatures
import sss.openstar.account.impl.{DefaultSignerFactory, EncryptedKeyFileAccountSignerCreator}
import sss.openstar.account.{NodeIdentity, NodeIdentityManager, NodeVerifierSigner, SignerFactory}
import sss.openstar.common.builders.SeedBytesBuilder
import sss.openstar.common.users.UserDirectory
import sss.openstar.crypto.SeedBytes
import sss.openstar.quorumledger.QuorumService
import sss.openstar.rpc.{AuthPlusRpcImpl, RpcBridgeImpl, SignerRpcImpl, UserSessionFactory}
import sss.openstar.startuphooks.SetProviderChargeHook
import sss.openstar.wallet.{UnlockedWallet, Wallet}
import sss.openstar.{Currency, UniqueNodeIdentifier}

import scala.concurrent.{ExecutionContext, Future}

trait AllInOneLayer[BASECURRENCY <: Currency] extends AppLayer
  with UIRpcServerBuilder[BASECURRENCY]
  with ConsoleServletBuilder
  with Html5PushServiceBuilder
  with ApiHostHelperBuilder[BASECURRENCY]
  with DecoderBuilder
  with ChargeHelperBuilder
  with WalletLayer
  with AllInOneLedgersLayer
  with SignerFactoryWithAuthPlusBuilder
  with MessageQueryHandlerActorBuilder[BASECURRENCY] {

  val argsArray: Array[String]

  lazy val users: UserDirectory = folderBaseUserDirectory

  lazy implicit val confImp = prefixedConf
  implicit val currentBlockHeightImp = () => currentBlockHeight()

  lazy val userSessionFactory: UserSessionFactory[BASECURRENCY] = new UserSessionFactory[BASECURRENCY]

  override def nextHook: NextHook = new SetProviderChargeHook(
    chargeHelper, prefixedConf, nodeIdentity, super.nextHook).setProviderChargeHook

  override def args = new ArgumentParser(argsArray)

  override lazy val useUpnpFlag: Boolean = !args.contains(noPnpArgName)

  override lazy val startHttpServiceFlag: Boolean = args.contains(startHttpServiceArgName)

  override lazy val startUiRpcServerFlag: Boolean = args.contains(startRpcServerArgName)

  override lazy val startHtml5PushFlag: Boolean = args.contains(startHtml5PushArgName)

  lazy override val httpServer = basicHttpServer()

  def basicHttpServer(): Server = {
    ServerLauncher(
      ServletContext("/console", "", InitServlet(buildConsoleServlet.get, "/*")).toHandler,
      remoteAttachmentsServlet.toHandler,
    )
  }


  def logAllWalletBalances(buildWallet: UniqueNodeIdentifier => Wallet[BASECURRENCY])(implicit atomicWalletCacheMap: AtomicWalletCacheMap[BASECURRENCY]): Unit = {

    log.info("List wallet owners and balances")
    users.listUsers().foreach(u => {
      val w = buildWallet(u.identity)
      log.info(s"Who ${w.walletOwner} Bal ${w.balance()} Status: ${u.status}")
    })
    log.info("Done wallets")
  }

  lazy implicit val extendedNodeIdentityManager: NodeIdentityManager =
    new NodeIdentityManager(
      verifierFactory,
      getSignerDetails,
      extendedSignerFactory)

  def createSignerRpcImpl: SignerRpcImpl = {
    implicit val s = extendedSignerFactory
    new SignerRpcImpl
  }

  def createAuthPlusRpcImpl: AuthPlusRpcImpl = {
    new AuthPlusRpcImpl(remoteKeyApi)
  }

  lazy val webAuthnSigs: WebAuthnSignatures =
    new WebAuthnSignatures(5 * 60 * 1000)

  def createRpcImpl(buildWallet: NodeIdentity => UnlockedWallet[BASECURRENCY], messageInboxActorGroup: ActorRef)(implicit atomicWalletCacheMap: AtomicWalletCacheMap[BASECURRENCY]): RpcBridgeImpl[BASECURRENCY] = {
    implicit val quorumSvc: QuorumService = quorumService

    new RpcBridgeImpl[BASECURRENCY](
      claimUrl,
      systemService,
      () => currentBlockHeight(),
      buildWallet,
      toIdentityLedgerOwner,
      users,
      nodeIdentity,
      config.getString("welcome.message"),
      config.getInt("welcome.amount"),
      userSessionFactory,
      actorContext.dispatcher,
      messageInboxActorGroup,
      keyPersister,
      remoteKeyApi,
      webAuthnSigs
    )
  }

}
