package sss.openstar.nodebuilder

import sss.openstar.Currency
import sss.openstar.ui.rpc.{AuthPlusService, BridgeService, SignerService}
import sss.openstar.ui.rpc.BridgeClient.ServiceImpls

trait LocalServiceImplBuilder[BASECURRENCY <: Currency] {

  self: RequireActorContext with UIRpcServerBuilder[BASECURRENCY] =>

  lazy val localServiceImpl: ServiceImpls = new ServiceImpls {
    override def bridgeService: BridgeService = rpcImpl

    override def signerService: SignerService = createSignerRpcImpl

    override def authPlusService: AuthPlusService = createAuthPlusRpcImpl
  }
}
