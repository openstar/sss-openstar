package sss.openstar.nodebuilder

trait WalletLayer extends WalletBuilder
  with AmountBuilderBuilder
  with WalletSyncer
  with UtxoTrackerBuilder {

  self: BaseLayer
    with NetworkLayer
    with NodeIdentityLayer
    with MandatoryLedgersLayer =>

}
