package sss.openstar.nodebuilder

import sss.openstar.account.NodeIdentity
import sss.openstar.balanceledger.BalanceLedgerQuery
import sss.openstar.chains.Chains.GlobalChainIdBuilder
import sss.openstar.common.builders.{ConfigBuilder, CpuBoundExecutionContextBuilder, NodeIdentityBuilder}
import sss.openstar.db.Builder.RequireDb
import sss.openstar.ledger.LedgerId
import sss.openstar.nodebuilder.Currencies.{Starz, TestStarz}
import sss.openstar.wallet.UnlockedWallet

trait TestStarzMessageQueryHandlerActorBuilder extends MessageQueryHandlerActorBuilder[TestStarz] {
  self: RequireDb
    with MessageEventBusBuilder
    with RequireActorContext
    with NodeIdentityBuilder
    with BlockChainBuilder
    with ConfigBuilder
    with IdentityServiceBuilder
    with RequireNetSend
    with WalletBuilder
    with ChargeHelperBuilder
    with GlobalChainIdBuilder
    with AllInOneLedgersLayer
    with TestBalanceLedgerBuilder
    with CpuBoundExecutionContextBuilder =>

  override def getLedgerId(): LedgerId = testBalanceLedgerId

  override def getBalanceLedgerQuery(): BalanceLedgerQuery[TestStarz] = testStarzBalanceLedger

  def getTestStarzAtomicWalletCacheMap(): AtomicWalletCacheMap[TestStarz] = createAtomicWalletCacheMap

  lazy implicit val evTestStarz: AtomicWalletCacheMap[TestStarz] = getTestStarzAtomicWalletCacheMap()

  override def getHostUnlockedWallet(): UnlockedWallet[TestStarz] =
    buildHostNodeUnlockedWallet[TestStarz](testBalanceLedgerId)

  lazy val buildTestStarzWallet: NodeIdentity => UnlockedWallet[TestStarz] = nId => buildUnlockedWallet[TestStarz](nId, ledgerId[TestStarz])

  lazy val unlockedApiHostWallet: UnlockedWallet[TestStarz] = buildHostNodeUnlockedWallet[TestStarz](ledgerId[TestStarz])
}
