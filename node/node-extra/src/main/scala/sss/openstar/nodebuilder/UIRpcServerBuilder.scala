package sss.openstar.nodebuilder

import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import sss.ancillary.{Configure, Logging}
import sss.openstar.Currency
import sss.openstar.common.builders.ActorSystemBuilder
import sss.openstar.http.RpcServer
import sss.openstar.http.RpcServer.RpcServerConfig
import sss.openstar.rpc._
import sss.openstar.ui.rpc.{AuthPlusService, AuthPlusServiceHandler, BridgeServiceHandler, SignerServiceHandler}

import scala.concurrent.Future

trait UIRpcServerBuilder[BASECURRENCY <: Currency] {

  self: Logging
    with Configure
    with ActorSystemBuilder =>

  lazy val startUiRpcServerFlag: Boolean = true

  def rpcImpl: RpcBridgeImpl[BASECURRENCY]
  def createSignerRpcImpl: SignerRpcImpl

  def createAuthPlusRpcImpl: AuthPlusRpcImpl

  lazy val configRpc: RpcServerConfig = RpcServerConfig.from(config.getConfig("bridge"))

  lazy val rpcServiceHandlers: HttpRequest => Future[HttpResponse] =
    BridgeServiceHandler.partial(rpcImpl)
      .orElse(SignerServiceHandler.partial(createSignerRpcImpl))
      .orElse(AuthPlusServiceHandler.partial(createAuthPlusRpcImpl))

  lazy val uiRpcServer: RpcServer = {
    import actorSystem.dispatcher

    new RpcServer(
      configRpc,
      PartialFunction.fromFunction(rpcServiceHandlers),
      PartialFunction.fromFunction(rpcServiceHandlers)
    )
  }

  def startUiRpcServer(): Unit = {
    if (startUiRpcServerFlag) {
      uiRpcServer.start()
      log.info(s"ui rpc server started ${configRpc.hostPort}:${configRpc.tls}")
    } else {
      log.info(s"startUiRpcServerFlag false, not starting rpc server")
    }
  }
}
