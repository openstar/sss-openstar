package sss.openstar.nodebuilder

import sss.openstar.Currency

/**
 * Node is aware of concrete currencies, it would have been nice to
 * make node deal with currencies generically but it's too much effort
 * at this point.
 */
object Currencies {


  sealed trait Ada extends Currency
  implicit final object Ada extends Ada

  sealed trait Starz extends Currency
  implicit final object Starz extends Starz

  sealed trait TestStarz extends Currency
  implicit final object TestStarz extends TestStarz


}
