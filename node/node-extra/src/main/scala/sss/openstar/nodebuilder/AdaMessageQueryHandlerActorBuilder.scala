package sss.openstar.nodebuilder

import sss.openstar.account.NodeIdentity
import sss.openstar.balanceledger.BalanceLedgerQuery
import sss.openstar.chains.Chains.GlobalChainIdBuilder
import sss.openstar.common.builders.{ConfigBuilder, CpuBoundExecutionContextBuilder, NodeIdentityBuilder}
import sss.openstar.db.Builder.RequireDb
import sss.openstar.ledger.LedgerId
import sss.openstar.nodebuilder.Currencies.Ada
import sss.openstar.wallet.UnlockedWallet

trait AdaMessageQueryHandlerActorBuilder extends MessageQueryHandlerActorBuilder[Ada] {
  self: RequireDb
    with MessageEventBusBuilder
    with RequireActorContext
    with NodeIdentityBuilder
    with BlockChainBuilder
    with ConfigBuilder
    with IdentityServiceBuilder
    with RequireNetSend
    with WalletBuilder
    with ChargeHelperBuilder
    with GlobalChainIdBuilder
    with AllInOneLedgersLayer
    with CardanoBalanceLedgerBuilder
    with CpuBoundExecutionContextBuilder =>

  override def getLedgerId(): LedgerId = cardanoLedgerId

  override def getBalanceLedgerQuery(): BalanceLedgerQuery[Ada] = cardanoLedger

  def getAdaAtomicWalletCacheMap(): AtomicWalletCacheMap[Ada] = createAtomicWalletCacheMap

  lazy implicit val evAda: AtomicWalletCacheMap[Ada] = getAdaAtomicWalletCacheMap()

  override def getHostUnlockedWallet(): UnlockedWallet[Ada] =
    buildHostNodeUnlockedWallet[Ada](cardanoLedgerId)

  lazy val buildAdaWallet: NodeIdentity => UnlockedWallet[Ada] = nId => buildUnlockedWallet[Ada](nId, ledgerId[Ada])

  lazy val unlockedApiHostWallet: UnlockedWallet[Ada] = buildHostNodeUnlockedWallet[Ada](ledgerId[Ada])

}
