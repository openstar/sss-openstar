package sss.openstar.nodebuilder

import sss.ancillary.{Configure, InitServlet, ServletContext}
import sss.openstar.attachments.{AttachmentsService, FileStreamPersister, LocalAttachmentsService, RemoteAttachmentsService, RemoteAttachmentsServlet}
import sss.openstar.common.builders.NodeIdTagBuilder
import sss.openstar.db.Builder.RequireDb

import java.io.File

trait AttachmentsServiceBuilder {
  self: RequireDb
    with Configure
    with RequireGetProviderPrefix
    with NodeIdTagBuilder =>

  lazy val attachmentsFolder = {
    val attFolder = config.getString("attachments.folder")
    val folder = new File(attFolder)
    folder.mkdirs()
    folder
  }

  lazy val localAttachmentsService =
    new LocalAttachmentsService(
      new FileStreamPersister(
        attachmentsFolder
      ))


  lazy val remoteAttachmentsService =
    new RemoteAttachmentsService(
      getProviderPrefix
    )

  lazy implicit val attachmentsService =
    new AttachmentsService(
      nodeIdTag.nodeId,
      localAttachmentsService,
      remoteAttachmentsService
    )

  lazy val remoteAttachmentsServlet =
    ServletContext(RemoteAttachmentsServlet.servletContext, "",
      InitServlet(new RemoteAttachmentsServlet(localAttachmentsService), "/*")
    )
}
