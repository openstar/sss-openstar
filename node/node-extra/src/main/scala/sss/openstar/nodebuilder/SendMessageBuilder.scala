package sss.openstar.nodebuilder

import sss.openstar.chains.Chains.GlobalChainIdBuilder
import sss.openstar.controller.SendMessage
import sss.openstar.controller.SendMessage.SubmitMessage
import sss.openstar.db.Builder.RequireDb
import sss.openstar.message.{PaywallCharges, ProviderStoreCharges}

trait SendMessageBuilder {

  self: ChargeHelperBuilder
    with RequireDb
    with RequireMessageEventBus
    with RequireNetSend
    with IdentityServiceBuilder
    with GlobalChainIdBuilder
    with RequireBlockChain =>

  val serviceProviderPayment: ProviderStoreCharges = chargeHelper.getIdentitiesProviderCharge
  val paywallCharges: PaywallCharges = chargeHelper.getPaywallChargeFromTo

  lazy val sendMessage = new SendMessage(
    () => currentBlockHeight(),
    serviceProviderPayment,
    paywallCharges
  )(db,
    messageEventBus,
    send,
    identityService,
    globalChainId
  )

  implicit val submitMessage: SubmitMessage = sendMessage.sendToRecipients

}
