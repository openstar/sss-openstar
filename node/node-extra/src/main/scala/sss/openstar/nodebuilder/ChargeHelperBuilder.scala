package sss.openstar.nodebuilder

import sss.openstar.common.builders.NodeIdentityBuilder
import sss.openstar.controller.ChargeHelper

trait ChargeHelperBuilder {
  self: ContactServiceBuilder
    with NodeIdentityBuilder
    with IdentityServiceBuilder =>

  lazy implicit val chargeHelper = new ChargeHelper(Set(nodeIdentity.id))
}
