package sss.openstar.nodebuilder

import sss.openstar.MessageKeys
import sss.openstar.balanceledger.TestBalanceLedger
import sss.openstar.chains.Chains.GlobalChainIdBuilder
import sss.openstar.common.builders.KeyFactoryBuilder
import sss.openstar.db.Builder.RequireDb
import sss.openstar.ledger.LedgerId
import Currencies.TestStarz

trait TestBalanceLedgerBuilder {
  self: NodeConfigBuilder
    with RequireDb
    with GlobalChainIdBuilder
    with GlobalTableNameTagBuilder
    with BlockChainBuilder
    with IdentityServiceBuilder
    with AmountBuilderBuilder
    with KeyFactoryBuilder =>

  lazy val testBalanceLedgerId = LedgerId(MessageKeys.TestBalanceLedger)

  lazy val testStarzBalanceLedger: TestBalanceLedger[TestStarz] = TestBalanceLedger(
    testBalanceLedgerId,
    findPublicKeyFTxOpt,
    keysFactoryLookup
  )
}
