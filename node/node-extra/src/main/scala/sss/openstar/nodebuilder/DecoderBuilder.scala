package sss.openstar.nodebuilder

import sss.openstar.eventbus.MessageInfo
import sss.openstar.ExtraMessageKeys


trait DecoderBuilder extends RequireDecoder {
  private lazy val m = ExtraMessageKeys.extraMessages
  override def decoder: Byte => Option[MessageInfo] = m.find
}
