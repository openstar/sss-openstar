package sss.openstar.nodebuilder

import sss.ancillary.Logging
import sss.openstar.Currency
import sss.openstar.balanceledger.UtxoLedgerImpl
import sss.openstar.common.users.UserDirectory

import scala.util.{Failure, Success, Try}
import sss.openstar.wallet.UtxoTracker.TrackWallet
import sss.openstar.wallet.Wallet
import scala.reflect.runtime.universe._

trait WalletSyncer {

  self: WalletBuilder
    with MessageEventBusBuilder
    with Logging =>

  def syncWallets[C <: Currency : TypeTag](
                                            users: UserDirectory,
                                            balanceLedger: UtxoLedgerImpl[C])(implicit walletCacheMap: AtomicWalletCacheMap[C]): Unit = Try {

    val balanceLedgerWallets: Seq[Wallet[C]] = users.listUsers().map { u =>
      val w = buildWallet[C](u.identity, balanceLedger.ledgerId)
      w.syncWalletWithLedger[C](balanceLedger)
      w
    }

    //log.debug(s"starting syncLedgerWithWallet for all wallets")
    Wallet.syncLedgerWithWallet[C](balanceLedger, balanceLedgerWallets, log)

    var bal = 0L

    balanceLedgerWallets foreach { w =>
      w.balance() match {
        case Success(balance) =>
          bal += balance.value
          messageEventBus publish TrackWallet[C](w, balance)
          log.info(s"Wallet for ${w.walletOwner}, ledgerId ${w.ledgerId}, balance ${w.balance()}, cash in client wallet ${w.clientBalanceTotal()} ")
        case Failure(e) =>
          log.warn(s"Failed to get balance for ${w.walletOwner} could be Long overflow.", e)
      }

    }

    log.info(s"Total balance for ${balanceLedgerWallets.size} wallets is $bal")

  }.recover {
    case e => log.warn("Failed to sync wallets ", e)
  }
}
