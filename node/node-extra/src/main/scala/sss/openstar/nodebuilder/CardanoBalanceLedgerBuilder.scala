package sss.openstar.nodebuilder

import sss.openstar.balanceledger.OracleBalanceLedger
import sss.openstar.chains.Chains.GlobalChainIdBuilder
import sss.openstar.common.builders.KeyFactoryBuilder
import sss.openstar.db.Builder.RequireDb
import Currencies.Ada

trait CardanoBalanceLedgerBuilder extends RequireCardanoLedgerId {

  self: OracleLedgerOwnerLedgerBuilder
    with RequireDb
    with GlobalChainIdBuilder
    with IdentityServiceBuilder
    with AmountBuilderBuilder
    with GlobalTableNameTagBuilder
    with KeyFactoryBuilder =>

  lazy val cardanoLedger = OracleBalanceLedger[Ada](cardanoLedgerId, keysFactoryLookup)

}
