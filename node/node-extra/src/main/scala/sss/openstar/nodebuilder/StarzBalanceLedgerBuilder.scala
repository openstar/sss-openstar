package sss.openstar.nodebuilder

import sss.openstar.balanceledger.{BalanceLedgerQuery, CoinbaseBalanceLedger}
import sss.openstar.chains.Chains.GlobalChainIdBuilder
import sss.openstar.common.builders.KeyFactoryBuilder
import sss.openstar.contract.CoinbaseValidator
import sss.openstar.db.Builder.RequireDb
import sss.openstar.quorumledger.QuorumService
import Currencies.Starz

trait StarzBalanceLedgerBuilder extends RequireStarzBalanceLedgerId {
  self: NodeConfigBuilder
    with RequireDb
    with GlobalChainIdBuilder
    with GlobalTableNameTagBuilder
    with BlockChainBuilder
    with IdentityServiceBuilder
    with AmountBuilderBuilder
    with KeyFactoryBuilder =>

  private lazy val starzBalanceLedgerQuery: BalanceLedgerQuery[Starz] = starzBalanceLedger

  lazy val starzBalanceLedger: CoinbaseBalanceLedger[Starz] = CoinbaseBalanceLedger(
    starzBalanceLedgerId,
    CoinbaseValidator(
      nodeConfig.blockChainSettings.inflationRatePerBlock,
      nodeConfig.blockChainSettings.spendDelayBlocks,
      () => QuorumService.quorumService(globalChainId).candidates()
    ),
    findPublicKeyFTxOpt,
    keysFactoryLookup
  )
}
