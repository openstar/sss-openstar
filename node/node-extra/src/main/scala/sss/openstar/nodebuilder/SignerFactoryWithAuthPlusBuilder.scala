package sss.openstar.nodebuilder

import iog.psg.client.messagesigning.ThirdPartyMessageSigningClient.ThirdPartyDetails
import iog.psg.messagesigning.keys.KeyType
import sss.openstar.account.impl.authplus.{AuthPlusAccountSignerCreator, AuthPlusSignerFactory, RemoteKeyApi}
import sss.openstar.account.{NodeIdentityManager, SignerFactory}
import sss.openstar.account.impl.EncryptedKeyFileAccountSignerCreator
import sss.openstar.account.impl.webauthn.{WebAuthnAccountSignerCreator, WebAuthnSignatures, WebAuthnSignerFactory}
import sss.openstar.common.builders.{ActorSystemBuilder, CpuBoundExecutionContextBuilder, CpuBoundExecutionContextFromActorSystemBuilder, KeyFactoryBuilder, KeyPersisterBuilder, NodeIdentityBuilder, RequireConfig, SignerFactoryBuilder}

trait SignerFactoryWithAuthPlusBuilder extends SignerFactoryBuilder {

  self: KeyPersisterBuilder
    with CpuBoundExecutionContextFromActorSystemBuilder
    with ActorSystemBuilder
    with KeyFactoryBuilder
    with RequireActorContext
    with RequireConfig
    with RequireMessageEventBus
    with NodeIdentityBuilder =>


  def sourceUrl = prefixedConf.getString("authplus.sourceUrl")

  def thirdPartyDetails: ThirdPartyDetails = ThirdPartyDetails(
    nodeIdentity.id,
    nodeIdentity.publicKey,
    KeyType.withName(nodeIdentity.defaultKeyType),
    bytes => nodeIdentity.defaultNodeVerifier.signer.sign(bytes)
  )

  def remoteKeyApi: RemoteKeyApi = new RemoteKeyApi(thirdPartyDetails, sourceUrl)(actorContext.system)

  lazy val extendedSignerFactory: SignerFactory = signerFactory.merge(
    AuthPlusSignerFactory(
      new AuthPlusAccountSignerCreator(keysFactoryLookup, remoteKeyApi)
    ).merge(
      WebAuthnSignerFactory(
        new WebAuthnAccountSignerCreator(
          new WebAuthnSignatures(5 * 60 * 1000)
        )
      )
    )
  )
}
