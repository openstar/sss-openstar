package sss.openstar.nodebuilder

import sss.openstar.MessageKeys
import sss.openstar.chains.Chains.GlobalChainIdBuilder
import sss.openstar.common.builders.NodeIdentityBuilder
import sss.openstar.counterledger.CounterLedger
import sss.openstar.db.Builder.RequireDb
import sss.openstar.ledger.LedgerId

trait CounterLedgerBuilder {
  self: RequireDb with GlobalChainIdBuilder with NodeIdentityBuilder with GlobalTableNameTagBuilder =>

  lazy val counterLedgerId: LedgerId = LedgerId(MessageKeys.CounterLedger)

  lazy val counterLedger = CounterLedger(nodeIdentity.id)(counterLedgerId, db, globalTableNameTag)
}
