package sss.openstar.nodebuilder

import sss.openstar.common.builders.{IOExecutionContextBuilder, RequireNodeIdentity}
import sss.openstar.console.ConsoleServlet
import sss.openstar.db.Builder.RequireDb

trait ConsoleServletBuilder {

  self: RequireNodeIdentity
    with SendTxBuilder
    with RequireDb
    with IOExecutionContextBuilder
    with IdentityLedgerBuilder =>

  def buildConsoleServlet: Option[ConsoleServlet] = {
    Option(new ConsoleServlet(nodeIdentity, sendTx, toIdentityLedgerOwner)(ioExecutionContext))
  }

}
