package sss.openstar.message.serialize

import sss.ancillary.Serialize._
import sss.openstar.balanceledger._
import sss.openstar.message._
/**
  * Created by alan on 6/8/16.
  */
object PaywallEnvelopeSerializer extends Serializer[PaywallEnvelope] {
  def toBytes(o: PaywallEnvelope): Array[Byte] =
    (
      OptionSerializer[TxIndex](o.indexOfPayment, index => ByteArraySerializer(index.toBytes)) ++
        StringSerializer(o.to) ++
        ByteArraySerializer(o.msg.toBytes)
      ).toBytes

  def fromBytes(bs: Array[Byte]): PaywallEnvelope = {
    PaywallEnvelope.tupled(
      bs.extract(
        OptionDeSerialize(ByteArrayDeSerialize(_.toTxIndex)),
        StringDeSerialize,
        ByteArrayDeSerialize(_.toMessage)
      )
    )

  }

}
