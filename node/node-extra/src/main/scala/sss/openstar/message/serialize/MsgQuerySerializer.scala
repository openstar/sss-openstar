package sss.openstar.message.serialize

import sss.ancillary.Serialize._
import sss.ancillary.ShortSessionKey
import sss.openstar.message.{EndMessagePage, EndMessageQuery, MessageQuery}

/**
  * Created by alan on 6/8/16.
  */
object MsgQuerySerializer extends Serializer[MessageQuery]{
  def toBytes(o: MessageQuery): Array[Byte] =
    (StringSerializer(o.who) ++
      LongSerializer(o.mostRecentUniqueIncreasing) ++
      IntSerializer(o.pageSize) ++
      ShortSerializer(o.sessionKey.value)).toBytes

  def fromBytes(bs: Array[Byte]): MessageQuery = {

    MessageQuery.tupled(
      bs.extract(
        StringDeSerialize,
        LongDeSerialize,
        IntDeSerialize,
        ShortDeSerialize(ShortSessionKey.apply)
      )
    )
  }

}

object EndMessageQuerySerializer extends Serializer[EndMessageQuery] {
  override def toBytes(t: EndMessageQuery): Array[Byte] = t.sessKey.toBytes

  override def fromBytes(b: Array[Byte]): EndMessageQuery =
    EndMessageQuery(b.extract(ShortDeSerialize(ShortSessionKey.apply )))
}

object EndMessagePageSerializer extends Serializer[EndMessagePage] {
  override def toBytes(t: EndMessagePage): Array[Byte] = t.sessKey.toBytes

  override def fromBytes(b: Array[Byte]): EndMessagePage =
    EndMessagePage(b.extract(ShortDeSerialize(ShortSessionKey.apply )))
}