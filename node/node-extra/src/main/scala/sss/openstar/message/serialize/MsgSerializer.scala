package sss.openstar.message.serialize

import sss.ancillary.Guid
import sss.ancillary.Serialize._
import sss.openstar.message._

/**
  * Created by alan on 6/8/16.
  */
object MsgSerializer extends Serializer[Message] {

  def toBytes(o: Message): Array[Byte] =

    ByteArraySerializer(o.msgPayload.toBytes) ++
      ByteArraySerializer(o.guid.value) ++
        OptionSerializer[Guid](o.parentGuid, g => ByteArraySerializer(g.value))
        .toBytes

  def fromBytes(bs: Array[Byte]): Message = {
    Message.tupled(
      bs.extract(
        ByteArrayDeSerialize(_.toMessagePayload),
        ByteArrayDeSerialize(Guid.apply),
        OptionDeSerialize(
          ByteArrayDeSerialize(Guid.apply)
        )
      )
    )

  }

}
