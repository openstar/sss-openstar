package sss.openstar.message

import sss.db.Db
import sss.openstar.UniqueNodeIdentifier
import sss.openstar.contacts.ContactService
import sss.openstar.identityledger.IdentityServiceQuery
import sss.openstar.message.MessageProcessorUtils._
import sss.openstar.message.OutgoingMessageProcessorUtil._
import sss.openstar.message.payloads.NewContactOutgoingMessageConsumer
import sss.openstar.network.MessageEventBus

import scala.concurrent.ExecutionContext
import scala.util.Try

object OutgoingMessageProcessorUtil {

  def defaultAsyncOutgoingMessageConsumer(implicit ec: ExecutionContext):AsyncMessageProcessor =
    syncToAsync(defaultOutgoingMessageConsumer)

  val defaultOutgoingMessageConsumer: MessageProcessor = {
    case ExpandedMessage(ExpandedComposite(_, guid, payload), None) =>  {
      MessageProcessorUtils.comsumeMessageSent(
        Message(payload, guid, None),
        MessageProcessorUtils.messageDecoder)
    }

    case ExpandedMessage(ExpandedComposite(_, guid, payload), Some(ExpandedComposite(_, parentGuid, parentPayload))) =>

      MessageProcessorUtils.comsumeMessageWithParent(
        Message(parentPayload, parentGuid, None),
        Message(payload, guid, Some(parentGuid)),
        MessageProcessorUtils.messageDecoder
      )

  }

}


class OutgoingMessageProcessorUtil(implicit db: Db,
                                   contactService: ContactService,
                                   events: MessageEventBus,
                                   identityServiceQuery: IdentityServiceQuery,
                                   utxoWatch: UtxoWatch
                                   ) {

  def processOutgoing(from: UniqueNodeIdentifier, msg: Message)(implicit box: MessageInBox): Try[Unit] = Try {


    val expandedParent = msg.parentGuid
      .flatMap(box.find)
      .map(_.msg)
        .map(msg => ExpandedComposite(messageDecoder(msg), msg.guid, msg.msgPayload))

    val expandedChileMessage = ExpandedComposite(messageDecoder(msg), msg.guid, msg.msgPayload)

    val outgoing = ExpandedMessage(expandedChileMessage, expandedParent)

    createOutgoingProcessors(from).lift((outgoing))
      .map(results => defaultHandleConsumeResults(results))

  }

  private def createOutgoingProcessors(userId : UniqueNodeIdentifier): MessageProcessor = {
    val messageDecoder = MessageProcessorUtils.messageDecoder
      new NewContactOutgoingMessageConsumer(userId,
        messageDecoder).outgoing
  } orElse defaultOutgoingMessageConsumer

}
