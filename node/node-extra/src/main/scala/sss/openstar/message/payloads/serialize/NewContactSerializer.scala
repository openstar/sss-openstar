package sss.openstar.message.payloads.serialize

import scorex.crypto.signatures.Signature
import sss.ancillary.Serialize._
import sss.openstar.message.payloads.NewContactMessage
import sss.openstar.util.SerializeByteString.{ByteStringDeSerialize, ByteStringSerializer}


object NewContactSerializer extends Serializer[NewContactMessage]{
  override def toBytes(t: NewContactMessage): Array[Byte] = {
    val g = Signature(t.sig)
    StringSerializer(t.invitor) ++
    StringSerializer(t.invitee) ++
     OptionSerializer(t.invitorAvatar, ByteStringSerializer) ++
      StringSerializer(t.category) ++
      ByteArraySerializer(g)
        .toBytes
  }

  override def fromBytes(b: Array[Byte]): NewContactMessage = {
    val extracted = b.extract(
      StringDeSerialize,
      StringDeSerialize,
      OptionDeSerialize(ByteStringDeSerialize),
      StringDeSerialize,
      ByteArrayDeSerialize
    )

    NewContactMessage(
      extracted._1,
      extracted._2,
      extracted._3,
      extracted._4,
      Signature(extracted._5)
    )
  }

}
