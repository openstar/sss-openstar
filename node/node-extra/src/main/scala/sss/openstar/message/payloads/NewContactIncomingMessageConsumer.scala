package sss.openstar.message.payloads


import sss.openstar.account.NodeIdentity
import sss.openstar.contacts.ContactService
import sss.openstar.identityledger.IdentityServiceQuery
import sss.openstar.message.{Add, AsyncMessageProcessor, Delete, ExpandedComposite, ExpandedMessage, IncomingMessageConsumer, Message, MessageDecoder, MessagePayloadDecoder, Update}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class NewContactIncomingMessageConsumer(userId: NodeIdentity,
                                        messageDecoder: MessageDecoder)
                                       (implicit identityServiceQuery: IdentityServiceQuery,
                                contactService: ContactService) extends IncomingMessageConsumer {

  override val incoming: AsyncMessageProcessor = {

    case (ExpandedMessage(ExpandedComposite(msg@NewContactMessage(invitor, invitee, _, _, sig), guid, payload), None)) =>  Future {
      if (invitor == invitee) {
        //inviting oneself for some reason, ignore
        Seq.empty
      } else {
        val sigIsGood = identityServiceQuery
          .toVerifier(
            identityServiceQuery.account(invitor)
          )
          .verifier
          .verify(msg.bytesToSign(guid), sig)

        if (sigIsGood) Seq(Add(Message(payload, guid, None)))
        else throw new RuntimeException(s"Contact request from ${invitor} failed signature verification")
      }
    }

    case (ExpandedMessage(
                          ExpandedComposite(
                                            msg@NewContactMessage(invitor, invitee, _, _, sig), guid, _
                          ),
                          Some(ExpandedComposite(parentMsg, parentGuid, _))
    )) =>  Future {

      if (invitor == invitee) Seq.empty
      else {
        val sigIsGood = identityServiceQuery.toVerifier(identityServiceQuery.account(invitor)).verifier.verify( msg.bytesToSign(guid), sig)
        if (sigIsGood) {
          parentMsg(msg) match {
            case parentNewContact: CompletedNewContactMessage =>

              val newPayload = MessagePayloadDecoder.toPayload(parentNewContact)
              val newMsg = Message(newPayload, parentGuid, None)
              //save new contact

              require(parentNewContact.invitor == userId.id, s"Invitor (${parentNewContact.invitor}) should be logged in user ${userId.id}")

              val user  = contactService.findUser(userId.id).get
              user.setCategoryDetails(
                parentNewContact.invitee,
                parentNewContact.invitorCategory,
                parentNewContact.inviteeCategory,
                parentNewContact.inviteeAvatar)
              Seq(Update(guid, parentGuid), Delete(guid))
          }

        } else throw new RuntimeException(s"Contact request from ${invitor} failed signature verification")
      }
    }
  }
}
