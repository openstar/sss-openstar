package sss.openstar.message

import sss.openstar.{BusEvent, UniqueNodeIdentifier}

import scala.concurrent.duration.Duration

//TODO expose the network version of this properly and
// make something react to it.
case class ToDoBlackList(node: UniqueNodeIdentifier, d: Duration) extends BusEvent

