package sss.openstar.message.payloads.serialize

import sss.ancillary.Serialize._
import sss.openstar.message.payloads.TicTacToeGameMessage
import sss.openstar.message.payloads.TicTacToeGameMessage.GameState

object GameStateSerializer extends Serializer[GameState] {

  override def toBytes(t: GameState): Array[Byte] = {
    SequenceSerializer(
      t.boardState map (OptionSerializer(_, ByteSerializer))
    ).toBytes
  }

  override def fromBytes(b: Array[Byte]): GameState = {
    GameState(
      b.extract(SequenceDeSerialize(_.extract(OptionDeSerialize(ByteDeSerialize))))
    )
  }
}

object TicTacToeGameSerializer extends Serializer[TicTacToeGameMessage]{

  override def toBytes(t: TicTacToeGameMessage): Array[Byte] = {
    StringSerializer(t.originator) ++
      StringSerializer(t.opponent) ++
      ByteArraySerializer(t.moverSig) ++
      StringSerializer(t.nextMover) ++
      ByteArraySerializer(GameStateSerializer.toBytes(t.gameState)).toBytes
  }

  override def fromBytes(b: Array[Byte]): TicTacToeGameMessage = {

    val extracted = b.extract(
      StringDeSerialize,
      StringDeSerialize,
      ByteArrayDeSerialize,
      StringDeSerialize,
      ByteArrayDeSerialize(GameStateSerializer.fromBytes)
    )

    TicTacToeGameMessage(extracted._1, extracted._2, extracted._3, extracted._4, extracted._5)

  }
}
