package sss.openstar.message.payloads.serialize

import sss.ancillary.Guid
import sss.ancillary.Serialize._
import sss.openstar.balanceledger._
import sss.openstar.crypto.AESDetails.AESEncodedKey
import sss.openstar.crypto.CBCEncryption
import sss.openstar.message.payloads.MessageEcryption._
import sss.openstar.util.SerializeByteString.{ByteStringDeSerialize, ByteStringSerializer}

object EmbeddedBountySerializer extends Serializer[EmbeddedBounty] {
  override def toBytes(t: EmbeddedBounty): Array[Byte] =
    LongSerializer(t.amountOfBounty) ++
      LongSerializer(t.bountyReturnBlockHeight) ++
      ByteArraySerializer(t.addrOfBounty.toBytes).toBytes

  override def fromBytes(b: Array[Byte]): EmbeddedBounty =
    EmbeddedBounty.tupled(
      b.extract(LongDeSerialize,
        LongDeSerialize,
        ByteArrayDeSerialize(_.toTxIndex))
    )
}
object MessageEncryptionSerializer extends Serializer[EncryptedMessage] {

  override def toBytes(m: EncryptedMessage): Array[Byte] = {
      OptionSerializer[EmbeddedBounty](m.bountyOpt, b => ByteArraySerializer(EmbeddedBountySerializer.toBytes(b))) ++
      StringSerializer(m.author) ++
      SequenceSerializer(m.receivers.map(EncryptedSessionKeySerializer.toBytes)) ++
      ByteStringSerializer(m.encrypted) ++
      StringSerializer(m.iv.asString) ++
      ByteArraySerializer(m.guid.value) ++
      SequenceSerializer(m.childGuids.map(g => ByteArraySerializer(g.value)))
    .toBytes
  }

  override def fromBytes(b: Array[Byte]): EncryptedMessage = {
    EncryptedMessage.tupled(
      b.extract(
        OptionDeSerialize(ByteArrayDeSerialize(EmbeddedBountySerializer.fromBytes)),
        StringDeSerialize,
        SequenceDeSerialize(EncryptedSessionKeySerializer.fromBytes),
        ByteStringDeSerialize,
        StringDeSerialize(CBCEncryption.initVector),
        ByteArrayDeSerialize(Guid(_)),
        SequenceDeSerialize(_.extract(ByteArrayDeSerialize(Guid.apply)))
      )
    )
  }
}


object MessageAttachmentsSerializer extends Serializer[MessageAttachments] {
  override def toBytes(t: MessageAttachments): Array[Byte] = {
    StringSerializer(t.attachmentProvider) ++
    SequenceSerializer(t.attachmentDetails.map(AttachmentDetailsSerializer.toBytes)) ++
      ByteArraySerializer(t.key.value)
        .toBytes
  }

  override def fromBytes(b: Array[Byte]): MessageAttachments = MessageAttachments.tupled(
    b.extract(
      StringDeSerialize,
      SequenceDeSerialize(AttachmentDetailsSerializer.fromBytes),
      ByteArrayDeSerialize(AESEncodedKey)
  ))
}

object AttachmentDetailsSerializer extends Serializer[AttachmentDetails] {

  override def toBytes(ad: AttachmentDetails): Array[Byte] = {
    StringSerializer(ad.name) ++
      StringSerializer(ad.mimeType) ++
      LongSerializer(ad.size)
        .toBytes
  }

  override def fromBytes(b: Array[Byte]): AttachmentDetails = AttachmentDetails.tupled(
    b.extract(
      StringDeSerialize,
      StringDeSerialize,
      LongDeSerialize
    )
  )

}

object BodyToBeEncryptedSerializer extends Serializer[BodyToBeEncrypted] {
  override def toBytes(t: BodyToBeEncrypted): Array[Byte] = {
    (StringSerializer(t.text) ++
      ByteArraySerializer(t.secret) ++
      OptionSerializer[MessageAttachments](t.attachmentsOpt,
        attachments => ByteArraySerializer(MessageAttachmentsSerializer.toBytes(attachments))))
      .toBytes
  }

  override def fromBytes(b: Array[Byte]): BodyToBeEncrypted = {
    BodyToBeEncrypted.tupled(
      b.extract(StringDeSerialize,
        ByteArrayDeSerialize,
        OptionDeSerialize(ByteArrayDeSerialize(MessageAttachmentsSerializer.fromBytes))
      )
    )
  }
}

object EncryptedSessionKeySerializer extends Serializer[EncryptedSessionKey] {

  override def toBytes(e: EncryptedSessionKey): Array[Byte] = {
    StringSerializer(e.identity) ++
      ByteStringSerializer(e.iv) ++
      ByteStringSerializer(e.encryptedSessionKey).toBytes
  }

  override def fromBytes(b: Array[Byte]): EncryptedSessionKey = EncryptedSessionKey.tupled(
    b.extract(
      StringDeSerialize,
      ByteStringDeSerialize,
      ByteStringDeSerialize
    )
  )

}

