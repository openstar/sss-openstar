package sss.openstar.message

import sss.ancillary.{Guid, Logging}
import sss.db._
import sss.openstar.UniqueNodeIdentifier
import sss.openstar.util.{IOExecutionContext, LoggingFutureSupport}

import scala.concurrent.Future
import sss.openstar.schemamigration.SqlSchemaNames.ColumnNames._
import sss.db.ops.DbOps.DbRunOps

object MessageDownloadPersistCache {

  object MessageStatus extends Enumeration {
    type MessageStatus = Value
    val unprocessed = Value(0)
    val processed = Value(1)
  }

}

class MessageDownloadPersistCache(implicit db: Db, ioExecutionContext: IOExecutionContext)
    extends Logging
    with LoggingFutureSupport {

  import MessageDownloadPersistCache._
  import ioExecutionContext.ec

  private lazy val table = db.table("message_download_cache")

  private def toMsg(r: Row): Message =
    r.arrayByte(messageCol).toMessage

  def mostRecentUniqueIncreasing(forNodeId: UniqueNodeIdentifier,
                                 fromProvider: UniqueNodeIdentifier
                                ): Future[Long] = {
    table.find(
      where(
        providerCol -> fromProvider,
        toCol -> forNodeId
      )
        orderDesc uniqueIncreasingCol
        limit 1
    )
    .dbRun
    .map(r => r.map(_.long(uniqueIncreasingCol)).getOrElse(0))

  }

  def saveMessage(forNodeId: UniqueNodeIdentifier,
                  fromProvider: UniqueNodeIdentifier,
                  uniqueIncreasing: Long,
                  msg: Message
  ): Future[Unit] = {

      table.insert(
        Map(
          guidCol -> msg.guid.value,
          providerCol -> fromProvider,
          toCol -> forNodeId,
          statusCol -> MessageStatus.unprocessed.id,
          messageCol -> msg.toBytes,
          uniqueIncreasingCol -> uniqueIncreasing
        )
      ).dbRun.map(_ => ())
     /*recover { case e =>
      mostRecentUniqueIncreasing(forNodeId, fromProvider).onComplete({
        case Success(l) =>
          log.warn(s"ERROR Provider: $fromProvider Index: $uniqueIncreasing but l is $l")
        case Failure(exception) => println("EXCEPTION IN MESSAGE DOWNLOAD " + exception)
      })
      table.find(
        where(
          guidCol -> msg.guid.value,
          providerCol -> fromProvider,
          toCol -> forNodeId,
          messageCol -> msg.toBytes
        )
      ) match {
        case None                                => log.info("No such ROW????")
        case Some(r) if r.id == uniqueIncreasing => log.info(s"IS good row $r")
        case Some(r)                             => log.info(r.toString())
      }
      log.warn(s"Failed to insert message - ${e.toString}")
    }*/

  }

  def messageProcessed(who: String, guid: Guid): Future[Unit] = {

    //val before = findUnprocessed(who)
    table
      .update(
        Map(statusCol -> MessageStatus.processed.id),
        where(
          guidCol -> guid.value,
          toCol -> who
        )
      ).map(_ => ())
    //val after = findUnprocessed(who)
    //log.info(s"Processed $who, $guid ")
    //log.info(s"$before")
    //log.info(s"$after")
  }.dbRun

  def findUnprocessed(who: String): Seq[Message] =
    table
      .filter(
        where(
          statusCol -> MessageStatus.unprocessed.id,
          toCol -> who
        )
          orderAsc uniqueIncreasingCol
          limit 100 // TODO warn?
      ).dbRunSyncGet
      .map(toMsg)

}
