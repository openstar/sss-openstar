package sss.openstar.message.payloads

import java.nio.charset.StandardCharsets
import java.util
import akka.util.ByteString
import scorex.crypto.signatures.Signature
import sss.ancillary.Guid
import sss.openstar.account.NodeIdentity
import sss.openstar.message.MessageComposite
import sss.openstar.{UniqueNodeIdentifier, message}

import scala.concurrent.{ExecutionContext, Future}

object NewContactMessage {

  def bytesToSign(invitor: UniqueNodeIdentifier,
                  invitee: UniqueNodeIdentifier,
                  category: String,
                  guid: Guid): Array[Byte] = {
    s"${invitor}:${invitee}:${category}:${guid.toString}".getBytes(StandardCharsets.UTF_8)
  }

  def create(invitor: UniqueNodeIdentifier,
            invitee: UniqueNodeIdentifier,
            invitorAvatar: Option[ByteString],
            category: String,
            guid: Guid,
            nodeIdentity: NodeIdentity)(implicit ec: ExecutionContext): Future[NewContactMessage] = {

    val bytes = s"${invitor}:${invitee}:${category}:${guid.toString}".getBytes(StandardCharsets.UTF_8)
    nodeIdentity.defaultNodeVerifier.signer.sign(bytes).map { sig =>
      NewContactMessage(invitor, invitee, invitorAvatar, category, sig)
    }
  }
}

case class CompletedNewContactMessage(invitor: UniqueNodeIdentifier,
                                      invitorAvatar: Option[ByteString],
                                      invitee: UniqueNodeIdentifier,
                                      invitorCategory: String,
                                      inviteeCategory: String,
                                      inviteeAvatar: Option[ByteString]
                                     ) extends MessageComposite {

  override def apply[C >: MessageComposite](m: message.MessageUpdater): C = throw new IllegalArgumentException(s"Cannot add to CompletedNewContactMessage")

}

case class NewContactMessage(invitor: UniqueNodeIdentifier,
                             invitee: UniqueNodeIdentifier,
                             invitorAvatar: Option[ByteString],
                             category: String,
                             sig: Signature
                            ) extends MessageComposite {


  def bytesToSign(guid: Guid): Array[Byte] = NewContactMessage.bytesToSign(invitor, invitee, category, guid)

  override def apply[C >: MessageComposite](m: message.MessageUpdater): C = m match {
    case n: NewContactMessage =>
      CompletedNewContactMessage(invitor, invitorAvatar, invitee, category, n.category, n.invitorAvatar)

    case _ =>
      throw new IllegalArgumentException(s"Can only add NewContactMessage to NewContactMessage")

  }

  override def hashCode(): Int = {
    val h1 = Seq(invitor, invitee, category)
      .filterNot(_ == null)
      .foldLeft(0)((acc,e) => acc + e.hashCode)

    h1 + sig.hashCode() + invitorAvatar.hashCode()
  }

  override def equals(o: Any): Boolean = o match {
    case that: NewContactMessage =>
      that.invitor == this.invitor &&
      that.category == this.category &&
      that.invitee == this.invitee &&
      that.invitorAvatar == this.invitorAvatar &&
      util.Arrays.equals(that.sig,this.sig)

    case _ => false
  }
}
