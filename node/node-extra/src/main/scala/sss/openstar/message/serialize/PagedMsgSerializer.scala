package sss.openstar.message.serialize

import sss.ancillary.Serialize._
import sss.ancillary.ShortSessionKey._
import sss.openstar.message._

/**
  * Created by alan on 6/8/16.
  */
object PagedMsgSerializer extends Serializer[PagedMessage] {

  def toBytes(o: PagedMessage): Array[Byte] =

    ByteArraySerializer(o.sessKey.toBytes) ++
    LongSerializer(o.mostRecentUniqueIncreasing) ++
      ByteArraySerializer(o.msg.toBytes)
        .toBytes

  def fromBytes(bs: Array[Byte]): PagedMessage = {
    PagedMessage.tupled(
      bs.extract(
        ByteArrayDeSerialize(_.toShortSessionKey),
        LongDeSerialize,
        ByteArrayDeSerialize(_.toMessage)
      )
    )

  }

}
