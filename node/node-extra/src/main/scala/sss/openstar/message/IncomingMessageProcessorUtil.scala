package sss.openstar.message

import sss.openstar.account.NodeIdentity
import sss.openstar.Currency
import sss.openstar.contacts.ContactService
import sss.openstar.identityledger.IdentityServiceQuery
import sss.openstar.message.MessageProcessorUtils._
import sss.openstar.message.payloads.{IncomingMessageEncryptionConsumer, NewContactIncomingMessageConsumer}
import sss.openstar.network.MessageEventBus
import sss.openstar.wallet.UnlockedWallet

import scala.concurrent.ExecutionContext

object IncomingMessageProcessorUtil {


  def defaultAsyncIncomingMessageConsumer(implicit ec: ExecutionContext):AsyncMessageProcessor =
    syncToAsync(defaultIncomingMessageConsumer)


  private val defaultIncomingMessageConsumer: MessageProcessor = {
    case ExpandedMessage(ExpandedComposite(_, guid, payload), None) =>  {
      MessageProcessorUtils.comsumeMessage(
        Message(payload, guid, None),
        MessageProcessorUtils.messageDecoder)
    }

    case ExpandedMessage(ExpandedComposite(_, guid, payload), Some(ExpandedComposite(_, parentGuid, parentPayload))) =>

        MessageProcessorUtils.comsumeMessageWithParent(
          Message(parentPayload, parentGuid, None),
          Message(payload, guid, Some(parentGuid)),
          MessageProcessorUtils.messageDecoder
        )

  }

  def createIncomingProcessor[C <: Currency](validateBounty: ValidateBounty,
                              userId: NodeIdentity,
                              userWallet: UnlockedWallet[C]
                             )(
                               implicit identityServiceQuery: IdentityServiceQuery,
                               events: MessageEventBus,
                               watch: UtxoWatch,
                               contactService: ContactService,
                               ec: ExecutionContext): AsyncMessageProcessor = {

    new IncomingMessageEncryptionConsumer(validateBounty, userId, userWallet, messageDecoder)
      .incoming
      .orElse(new NewContactIncomingMessageConsumer(userId, messageDecoder)
        .incoming
      )
  }

}


