package sss.openstar.message

import sss.openstar.{Currency, MessageKeys}
import sss.openstar.balanceledger._
import sss.openstar.contract._
import sss.openstar.controller.ChargeHelper
import sss.openstar.ledger.{LedgerId, LedgerItem}
import sss.openstar.nodebuilder.Currencies.{Ada, Starz}
import sss.openstar.wallet.UnlockedWallet

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}


/**
  * Created by alan on 6/24/16.
  */

object MessagePaywall {

  type MessagePaywallGenerator[C <: Currency] = UnlockedWallet[C] => MessagePaywall[C]

  def apply[C <: Currency](currentBlockHeight: () => Long,
                           ledgerId: LedgerId,
            balanceLedger: BalanceLedgerQuery[C],
            chargeHelper: ChargeHelper)(implicit ec :ExecutionContext): MessagePaywallGenerator[C] = unLockedWallet =>
    new MessagePaywall[C](currentBlockHeight,
      ledgerId,
      unLockedWallet,
      balanceLedger,
      chargeHelper
    )
}

class MessagePaywall[+C <: Currency](
                     currentBlockHeight: () => Long,
                     ledgerId: LedgerId,
                     wallet: UnlockedWallet[C],
                     balanceLedger: BalanceLedgerQuery[C],
                     chargeHelper: ChargeHelper
                     )(implicit ec :ExecutionContext) {

  class MessagePaywallException(msg: String) extends Exception(msg)

  @throws[MessagePaywallException]
  def require(f: => Boolean, msg:String) = if(!f) throw new MessagePaywallException(msg)

  @throws[MessagePaywallException]
  def require(msg:String) = throw new MessagePaywallException(msg)

  private def getMinAmount(fromOpt: Option[String], to: String): Try[Long] = {
    fromOpt match {
      case None => Try(chargeHelper.getDefaultPaywallCharge(to))
      case Some(from) =>
        chargeHelper.getPaywallChargeFromTo(from, to)
    }
  }

  def validate(fromOpt: Option[String], to: String)(txIndxOpt: Option[TxIndex]): Try[Option[Future[LedgerItem]]] = {
    getMinAmount(fromOpt, to).flatMap { amount =>
      validate(amount)(txIndxOpt)
    }
  }

  def validateProviderCharge(txIndxOpt: Option[TxIndex]): Try[Option[Future[LedgerItem]]] = {
    //.get will fail if no longer providing service
    chargeHelper.getProviderCharge(wallet.w.walletOwner).map(Success(_))
      .getOrElse(Failure(new RuntimeException(s"${wallet.w.walletOwner} is not a provider"))) flatMap {
      case Some(charge) => validate(charge)(txIndxOpt)
      case None =>
        // no charge
        validate(0)(txIndxOpt)
    }
  }

  private def validate(minimumExpectedAmount: Long)(txIndxOpt: Option[TxIndex]): Try[Option[Future[LedgerItem]]] = Try {

    val nowBlock = currentBlockHeight()

    require(minimumExpectedAmount >= 0, s"Minimum expected $minimumExpectedAmount cannot be negative?")

    if(minimumExpectedAmount > 0) {
      val txIndx = txIndxOpt.getOrElse(throw new RuntimeException(s"Expected min was $minimumExpectedAmount, bit no index provided."))
      val servicePayment = balanceLedger
        .entry(txIndx)
        .getOrElse(require(s"TxIndx $txIndx gone from balance ledger, already claimed?"))

      require(servicePayment.amount >= minimumExpectedAmount,
        s"Message paywall charge is $minimumExpectedAmount. (${servicePayment.amount})")

      val decumbrance: Decumbrance = servicePayment.encumbrance match {

        case SaleOrReturnSecretEnc(_, claimants, _, returnBlockHeight) if claimants.contains(wallet.w.walletOwner) && nowBlock < returnBlockHeight =>
          SaleSecretDec

        case SingleIdentityEnc(id, _, blockHeight) if wallet.w.walletOwner == id && blockHeight < nowBlock =>
          SingleIdentityDec

        case x =>
          require(s"Payment to must be VALID SingleIdentityEnc or SaleOrReturnSecretEnc, is ${x}")
      }

      val in = TxInput(txIndx, servicePayment.amount, decumbrance)
      val out = TxOutput(servicePayment.amount, SingleIdentityEnc(wallet.w.walletOwner, None, 0))
      val tx = StandardTx(Seq(in), Seq(out))
      Some(
        wallet.sign(tx) map { sTx =>
          LedgerItem(ledgerId, sTx.txId, sTx.toBytes)
        }
      )
    } else None

  }
}
