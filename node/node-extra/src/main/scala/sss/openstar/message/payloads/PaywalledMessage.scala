package sss.openstar.message.payloads

import sss.openstar.balanceledger.TxIndex
import sss.openstar.message.{MessageComposite, MessagePayload}
import sss.openstar.{UniqueNodeIdentifier, message}

case class PaywalledMessage(
                             from: Option[UniqueNodeIdentifier],
                             txIndex: Option[TxIndex],
                             messagePayload: MessagePayload) extends MessageComposite {

  override def apply[C >: MessageComposite](m: message.MessageUpdater): C =
    throw new Exception("Can't apply anything to PaywalledMessage")

}
