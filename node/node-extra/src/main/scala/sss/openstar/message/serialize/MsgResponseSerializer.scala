package sss.openstar.message.serialize

import sss.ancillary.Serialize._
import sss.openstar.message.{FailureResponse, MessageResponse, SuccessResponse}

/**
  * Created by alan on 6/8/16.
  */
object ob {

  implicit object MsgResponseSerializer extends Serializer[MessageResponse] {

    def toBytes(o: MessageResponse): Array[Byte] = {
      o match {
        case SuccessResponse(responseId) => (BooleanSerializer(true) ++ OptionSerializer(o.txIdOpt, ByteArraySerializer)).toBytes
        case FailureResponse(responseId, info) => (BooleanSerializer(false) ++
          OptionSerializer(o.txIdOpt, ByteArraySerializer) ++
          StringSerializer(info)).toBytes
      }
    }

    def fromBytes(bs: Array[Byte]): MessageResponse = {

      bs.extract(BooleanDeSerialize, ByteArrayRawDeSerialize) match {
        case (true, rest) =>
          SuccessResponse(rest.extract(OptionDeSerialize(ByteArrayDeSerialize)))
        case (_, rest) =>
          val (txIdOpt, str) = rest.extract(OptionDeSerialize(ByteArrayDeSerialize), StringDeSerialize)
          FailureResponse(txIdOpt, str)
      }
    }

  }

}
