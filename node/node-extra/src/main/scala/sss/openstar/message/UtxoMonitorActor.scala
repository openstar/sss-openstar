package sss.openstar.message

import akka.actor.{Actor, ActorContext, ActorLogging, ActorRef, Props}
import sss.db.Db
import sss.openstar.balanceledger.{ConsumedUtxoChange, UtxoChanges}
import sss.openstar.message.UtxoMonitorActor.Prune
import sss.openstar.network.MessageEventBus

import scala.concurrent.duration._

object UtxoMonitorActor {

  private case object Prune

  def apply(utxoMonitor: UtxoMonitor)(
    implicit actorSystem: ActorContext,
    db: Db,
    events: MessageEventBus): ActorRef = {
    actorSystem.actorOf(props(utxoMonitor))
  }

  def props(utxoMonitor: UtxoMonitor)(implicit db: Db,
                                      events: MessageEventBus): Props =
    Props(classOf[UtxoMonitorActor], utxoMonitor, db, events)
}

class UtxoMonitorActor(implicit val utxoMonitor: UtxoMonitor,
                       db: Db,
                       events: MessageEventBus) extends Actor with ActorLogging {

  override def preStart(): Unit = {
    super.preStart()
    events subscribe classOf[UtxoChanges]
  }

  import context.dispatcher

  private val cancellable = context.system.scheduler.scheduleAtFixedRate(1.hour, 1.hour, self, Prune)

  override def postStop(): Unit = cancellable.cancel()

  override def receive: Receive = {
    case UtxoChanges(ledgerId, _, atHeight, consumed, _) =>
      consumed.collect {
        case c:ConsumedUtxoChange => c
      }.foreach(self ! (atHeight, _))

    case (atHeight: Long, ConsumedUtxoChange(index)) =>
      utxoMonitor.markConsumed(index, atHeight) recover {
        case e => log.error(e, "Failed to mark index consumed")
      }

    case Prune =>
      utxoMonitor.prune()
  }
}
