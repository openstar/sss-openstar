package sss.openstar.message.payloads

import sss.ancillary.Logging
import sss.openstar.UniqueNodeIdentifier
import sss.openstar.message.{MessageComposite, MessageUpdater}
import sss.openstar.quorumledger.AddNodeId


final case class NewQuorumMemberMessage(tx: AddNodeId,
                                  requiredSigs: Set[UniqueNodeIdentifier],
                                  sigs: Map[UniqueNodeIdentifier, Array[Byte]]
                                 ) extends MessageComposite with Logging {

  def addSig(node: UniqueNodeIdentifier, sig: Array[Byte]): NewQuorumMemberMessage = {
    copy(sigs = sigs + (node -> sig))
  }

  override def apply[F >: MessageComposite](m: MessageUpdater): F = m match {

    case NewQuorumMemberMessage(_, _, otherSigs) =>

      val newSigs = otherSigs.filterNot {
        case (who, sig) => sigs.keys.exists(_ == who)
      }

      this.copy(sigs = sigs ++ newSigs)

    case x =>
      log.error(s"NewQuorumMember could not merge ${x.getClass}")
      this
  }
}