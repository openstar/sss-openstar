package sss.openstar.message.payloads


import sss.ancillary.Guid
import sss.openstar.message

object ThreadedMessage {

  trait Threadable extends message.MessageUpdater {
    val guid: Guid
  }

  trait Threaded {
    val childGuids: Seq[Guid]
  }

}
