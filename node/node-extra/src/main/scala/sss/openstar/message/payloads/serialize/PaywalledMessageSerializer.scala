package sss.openstar.message.payloads.serialize

import sss.ancillary.Serialize._
import sss.openstar.balanceledger.TxIndex
import sss.openstar.balanceledger.serialize.TxIndexSerializer
import sss.openstar.message.payloads.PaywalledMessage
import sss.openstar.message.serialize.MsgPayloadSerializer

object PaywalledMessageSerializer extends Serializer[PaywalledMessage]{
  override def toBytes(t: PaywalledMessage): Array[Byte] = {
    OptionSerializer(t.from, StringSerializer) ++
      OptionSerializer[TxIndex](t.txIndex, index => ByteArraySerializer(TxIndexSerializer.toBytes(index))) ++
      ByteArraySerializer(t.messagePayload.toBytes)
        .toBytes
  }

  override def fromBytes(b: Array[Byte]): PaywalledMessage = PaywalledMessage.tupled(
    b.extract(
      OptionDeSerialize(StringDeSerialize),
      OptionDeSerialize(ByteArrayDeSerialize(TxIndexSerializer.fromBytes)),
      ByteArrayDeSerialize(MsgPayloadSerializer.fromBytes))
  )


}
