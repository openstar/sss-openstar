package sss.openstar

import akka.actor.{ActorContext, ActorRef}

import java.util
import java.util.concurrent.atomic.AtomicReference
import sss.ancillary.ByteArrayEncodedStrOps._
import sss.ancillary.Serialize._
import sss.ancillary.{Guid, ShortSessionKey}
import sss.openstar.account.NodeIdentity
import sss.openstar.balanceledger._
import sss.openstar.ledger._
import sss.openstar.message.payloads.MessageEcryption.BodyToBeEncrypted
import sss.openstar.message.payloads.serialize.BodyToBeEncryptedSerializer
import sss.openstar.message.serialize._
import sss.openstar.message.serialize.ob.MsgResponseSerializer
import sss.openstar.util.UniqueIncreasing.UniqueIncreasing
import sss.openstar.wallet.UnlockedWallet

import scala.concurrent.Future
import scala.util.Try

/**
  * Created by alan on 6/6/16.
  */
package object message {

  val `1MB` = 1048576
  val maxSizeOfMessage = `1MB` * 20

  type AtomicNodeActorRefMap = AtomicReference[Set[UniqueNodeIdentifier]]

  type ValidateBounty = (Long, UniqueNodeIdentifier) => Boolean
  type PaywallChargeRedeemer =  (Option[UniqueNodeIdentifier],UniqueNodeIdentifier) => Option[TxIndex] => Try[Option[Future[LedgerItem]]]
  type PaywallCharges = (UniqueNodeIdentifier,UniqueNodeIdentifier)  => Try[Long]
  type ProviderStoreCharges = UniqueNodeIdentifier => Try[Long]
  type MessageInBoxActorConstructor[C <: Currency] = (UnlockedWallet[C], NodeIdentity) => ActorContext => ActorRef

  case class MessageQuery(who: UniqueNodeIdentifier,
                          mostRecentUniqueIncreasing: UniqueIncreasing,
                          pageSize: Int,
                          sessionKey: ShortSessionKey)

  object MessageCompositeFailure {
    def apply(str: String) = throw new IllegalArgumentException(str)
  }

  trait MessageUpdater // marker interface for objects used to update messages

  trait MessageComposite extends MessageUpdater {
    private[message] def apply[C >: MessageComposite](m: MessageUpdater): C
  }

  type MessageDecoder = Message => MessageComposite

  type ConsumeResults = Seq[ConsumeResult]

  type MessageProcessor = PartialFunction[ExpandedMessage, ConsumeResults]
  type AsyncMessageProcessor = PartialFunction[ExpandedMessage, Future[ConsumeResults]]
  type IncomingMessageSink = ExpandedMessage => Future[Unit]


  sealed trait ConsumeResult

  case class Add(msg: Message) extends ConsumeResult
  case class AddSent(msg: Message) extends ConsumeResult

  case object TempRejected extends ConsumeResult

  case class Rejected(msg: Message) extends ConsumeResult

  case class Update(msgGuid: Guid, parentMessageGuid: Guid) extends ConsumeResult
  //case class Update(msg: Message) extends ConsumeResult

  case class Delete(guid: Guid) extends ConsumeResult

  case class Watch(txIndex: TxIndex) extends ConsumeResult


  trait IncomingMessageConsumer {
    val incoming: AsyncMessageProcessor
  }

  trait OutgoingMessageConsumer {
    val outgoing: MessageProcessor
  }

  trait MessageResponse {
    val success: Boolean
    val txIdOpt: Option[TxId]
    lazy val txIdOptStr: String = txIdOpt.map(_.toBase64Str).getOrElse("none")

    override def equals(obj: scala.Any): Boolean = {
      obj match {
        case that: MessageResponse =>
          (that.success == success) &&
            util.Arrays.equals(that.txIdOpt.getOrElse(Array.emptyByteArray),txIdOpt.getOrElse(Array.emptyByteArray))
        case _ => false
      }
    }

    override def hashCode(): Int = success.hashCode() + txIdOpt.map(_.hash).getOrElse(0)

    override def toString: UniqueNodeIdentifier = {
      s"MessageResponse success=$success txIdOpt=$txIdOptStr"
    }
  }

  object SuccessResponse {
    def apply(txId: TxId): SuccessResponse = new SuccessResponse(Some(txId))
  }

  case class SuccessResponse(txIdOpt: Option[TxId]) extends MessageResponse {
    final val success = true
  }

  object FailureResponse {
    def apply(txId: TxId, info: String): FailureResponse = new FailureResponse(Some(txId), info)
  }

  case class FailureResponse(txIdOpt: Option[TxId], info: String) extends MessageResponse {
    final val success = false

    override def toString: UniqueNodeIdentifier =  s"${super.toString} info: $info"
  }

  case class EndMessagePage(sessKey: ShortSessionKey)

  case class EndMessageQuery(sessKey: ShortSessionKey)

  case class PaywallEnvelope(indexOfPayment: Option[TxIndex], to: UniqueNodeIdentifier, msg: Message)

  case class MessagePayload(payloadType: Byte, payload: Array[Byte]) {
    override def equals(obj: scala.Any): Boolean = {
      obj match {
        case that: MessagePayload =>
          (that.payloadType == payloadType) && (that.payload isSame payload)
        case _ => false
      }
    }

    override def hashCode(): Int = payloadType.hashCode() + payload.hash
  }

  case class PagedMessage(
                          sessKey: ShortSessionKey,
                          mostRecentUniqueIncreasing: UniqueIncreasing,
                          msg:Message)

  case class Message(
                      msgPayload: MessagePayload,
                      guid: Guid,
                      parentGuid: Option[Guid] = None
                    )

  case class ExpandedComposite(msg: MessageComposite, guid: Guid, payload: MessagePayload)
  case class ExpandedMessage(msg: ExpandedComposite, parentOpt: Option[ExpandedComposite])


  implicit class ToEndMessageQuery(bs: Array[Byte]) {
    def toEndMessageQuery: EndMessageQuery = EndMessageQuerySerializer.fromBytes(bs)
  }

  implicit class ToEndMessagePage(bs: Array[Byte]) {
    def toEndMessagePage: EndMessagePage = EndMessagePage(bs.extract(ShortDeSerialize(ShortSessionKey.apply )))
  }

  implicit class ToMsgPayload(bs: Array[Byte]) {
    def toMessagePayload: MessagePayload = MsgPayloadSerializer.fromBytes(bs)
  }

  implicit class MsgPayloadToBytes(o : MessagePayload) extends ToBytes {
    override def toBytes: Array[Byte] = MsgPayloadSerializer.toBytes(o)
  }

  implicit class ToMsgResponse(bs: Array[Byte]) {
    def toMessageResponse: MessageResponse = MsgResponseSerializer.fromBytes(bs)
  }

  implicit class MsgResponseToBytes(o : MessageResponse) extends ToBytes {
    override def toBytes: Array[Byte] = MsgResponseSerializer.toBytes(o)
  }

  implicit class ToMsg(bs: Array[Byte]) {
    def toMessage: Message = MsgSerializer.fromBytes(bs)
  }

  implicit class MsgToBytes(o: Message) extends ToBytes {
    override def toBytes: Array[Byte] =  MsgSerializer.toBytes(o)
  }

  implicit class ToPageMsg(bs: Array[Byte]) {
    def toPagedMessage: PagedMessage = PagedMsgSerializer.fromBytes(bs)
  }

  implicit class MsgPagedToBytes(o: PagedMessage) extends ToBytes {
    override def toBytes: Array[Byte] =  PagedMsgSerializer.toBytes(o)
  }

  implicit class ToMsgQuery(bs: Array[Byte]) {
    def toMessageQuery: MessageQuery = MsgQuerySerializer.fromBytes(bs)
  }

  implicit class MsgQueryToBytes(o: MessageQuery) extends ToBytes {
    override def toBytes: Array[Byte] =  MsgQuerySerializer.toBytes(o)
  }

  implicit class EnvelopeFromBytes(bs: Array[Byte]) {
    def toEnvelope: PaywallEnvelope = PaywallEnvelopeSerializer.fromBytes(bs)
  }

  implicit class EnvelopeToBytes(o: PaywallEnvelope) extends ToBytes {
    override def toBytes: Array[Byte] = PaywallEnvelopeSerializer.toBytes(o)
  }

  implicit class BodyToBeEncryptedToBytes(b: BodyToBeEncrypted) extends ToBytes {
    override def toBytes: Array[Byte] = BodyToBeEncryptedSerializer.toBytes(b)
  }

  implicit class BodyToBeEncryptedFromBytes(b: Array[Byte]) extends {
     def toBodyToBeEncrypted: BodyToBeEncrypted = BodyToBeEncryptedSerializer.fromBytes(b)
  }
}
