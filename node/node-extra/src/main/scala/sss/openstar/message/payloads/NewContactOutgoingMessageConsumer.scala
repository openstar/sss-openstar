package sss.openstar.message.payloads


import sss.openstar.UniqueNodeIdentifier
import sss.openstar.contacts.ContactService
import sss.openstar.identityledger.IdentityServiceQuery
import sss.openstar.message.{Delete, ExpandedComposite, ExpandedMessage, Message, MessageDecoder, MessagePayloadDecoder, MessageProcessor, OutgoingMessageConsumer, Update}

class NewContactOutgoingMessageConsumer(userId: UniqueNodeIdentifier,
                                        messageDecoder: MessageDecoder)
                                       (implicit identityServiceQuery: IdentityServiceQuery,
                                contactService: ContactService) extends OutgoingMessageConsumer {


  override val outgoing: MessageProcessor = {

    case (ExpandedMessage(
                          ExpandedComposite(
                                            msg@NewContactMessage(invitor, invitee, _, _, sig), guid, _
                          ),
                          Some(ExpandedComposite(parentMsg, parentGuid, _))
    )) =>  {

      if (invitor == invitee) Seq.empty
      else {


          parentMsg(msg) match {
            case parentNewContact: CompletedNewContactMessage =>

              val newPayload = MessagePayloadDecoder.toPayload(parentNewContact)
              val newMsg = Message(newPayload, parentGuid, None)
              //save new contact

              require(parentNewContact.invitee == userId, s"Invitor (${parentNewContact.invitor}) should be logged in user ${userId}")

              val user  = contactService.findUser(userId).get
              user.setCategoryDetails(
                parentNewContact.invitor,
                parentNewContact.inviteeCategory,
                parentNewContact.invitorCategory,
                parentNewContact.invitorAvatar)
              Seq(Update(guid, parentGuid), Delete(guid))
          }
      }
    }
  }
}
