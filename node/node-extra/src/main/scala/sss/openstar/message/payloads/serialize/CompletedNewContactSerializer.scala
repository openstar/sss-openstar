package sss.openstar.message.payloads.serialize

import sss.ancillary.Serialize._
import sss.openstar.message.payloads.CompletedNewContactMessage
import sss.openstar.util.SerializeByteString.{ByteStringDeSerialize, ByteStringSerializer}


object CompletedNewContactSerializer extends Serializer[CompletedNewContactMessage]{
  override def toBytes(t: CompletedNewContactMessage): Array[Byte] =
    StringSerializer(t.invitor) ++
    OptionSerializer(t.invitorAvatar, ByteStringSerializer) ++
    StringSerializer(t.invitee) ++
      StringSerializer(t.invitorCategory) ++
      StringSerializer(t.inviteeCategory) ++
      OptionSerializer(t.inviteeAvatar, ByteStringSerializer)
        .toBytes

  override def fromBytes(b: Array[Byte]): CompletedNewContactMessage = CompletedNewContactMessage.tupled(
    b.extract(StringDeSerialize,
      OptionDeSerialize(ByteStringDeSerialize),
      StringDeSerialize,
      StringDeSerialize,
      StringDeSerialize,
      OptionDeSerialize(ByteStringDeSerialize),
    )
  )

}
