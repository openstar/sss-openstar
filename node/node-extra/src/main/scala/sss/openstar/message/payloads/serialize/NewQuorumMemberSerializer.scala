package sss.openstar.message.payloads.serialize

import sss.ancillary.Serialize._
import sss.openstar.message.payloads.NewQuorumMemberMessage
import sss.openstar.quorumledger._

object NewQuorumMemberSerializer extends Serializer[NewQuorumMemberMessage] {

  override def toBytes(m: NewQuorumMemberMessage): Array[Byte] = {
    ByteArraySerializer(m.tx.toBytes) ++
    SequenceSerializer(m.requiredSigs.toSeq map StringSerializer) ++
      MapSerializer(m.sigs, StringSerializer, ByteArraySerializer)
      .toBytes
  }

  override def fromBytes(b: Array[Byte]): NewQuorumMemberMessage = {
    val extracted = b.extract(
      ByteArrayDeSerialize(_.toAddNode),
      SequenceDeSerialize(_.extract(StringDeSerialize)),
        MapDeSerialize(_.extract(StringDeSerialize), _.extract(ByteArrayDeSerialize))
    )

    NewQuorumMemberMessage(extracted._1, extracted._2.toSet, extracted._3)

  }
}
