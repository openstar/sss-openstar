package sss.openstar.message

import sss.ancillary.Guid
import sss.openstar.util.SynchronizedLruMap

object DecodedMessageCache {

  private lazy val decodedCache = SynchronizedLruMap[Guid, Option[MessageComposite]](5000)

  def remove(guid: Guid): Unit = decodedCache.remove(guid)

  def update(m: Message): Unit = {
    decodedCache
      .update(m.guid,
        MessagePayloadDecoder.fromPayload(m.msgPayload)
      )
  }

  def apply(m: Message): Option[MessageComposite] = {
    decodedCache.getOrElseUpdate(
      m.guid,
      MessagePayloadDecoder.fromPayload(m.msgPayload)
    )
    MessagePayloadDecoder.fromPayload(m.msgPayload)
  }

}
