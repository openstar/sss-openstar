package sss.openstar.message.payloads

import sss.openstar.balanceledger.TxIndex


trait UtxoWatcher {
  def watch: Seq[TxIndex]
}
