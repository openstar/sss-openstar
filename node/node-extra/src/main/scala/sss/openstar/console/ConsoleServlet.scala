package sss.openstar.console


import scorex.crypto.signatures.PublicKey
import sss.ancillary.ByteArrayEncodedStrOps._
import sss.ancillary.FutureOps.AwaitResult
import sss.ancillary.Logging
import sss.db._
import sss.openstar.account.NodeIdentity
import sss.openstar.chains.TxWriterActor.InternalCommit
import sss.openstar.controller.Utils
import sss.openstar.identityledger.IdentityRolesOps._
import sss.openstar.identityledger.{Claim0, IdentityRoleAttribute}
import sss.openstar.tools.SendTxSupport.SendTx
import sss.openstar.util.IOExecutionContext


/**
  * Copyright Stepping Stone Software Ltd. 2016, all rights reserved.
  * mcsherrylabs on 3/9/16.
  */
class ConsoleServlet(
                      nodeIdentity: NodeIdentity,
                      sendTx: SendTx,
                      toIdentityLedgerOwner: NodeIdentity => Option[NodeIdentity]
                    )
                    (implicit ioExecutionContext: IOExecutionContext)
  extends BaseConsoleServlet
    with Logging {

  val cmds: Map[String, Cmd] = Map(
    "claim" -> new Cmd {
      override def help: String = s"Claim an identity with public key "

      override def apply(params: Seq[String]): Seq[String] = {
        val claim = params.head
        val pKey = PublicKey(params(1).fromBase64Str)
        toIdentityLedgerOwner(nodeIdentity) match {
          case None =>
            log.warn(s"This node is ${nodeIdentity.id} and can not claim $claim")
            Seq("fail")
          case Some(identityLedgerOwner) =>
            val tx = Claim0(identityLedgerOwner.id, claim, pKey, IdentityRoleAttribute().setNetworkAuditor())

            import ioExecutionContext.ec
            Seq(
              Utils
                .simplySignedIdentityLedgerItem(nodeIdentity, tx)
                .flatMap(sendTx(_))
                .await() match {
                case _: InternalCommit => "ok"
                case p =>
                  log.warn(p.toString)
                  "fail"
              }
            )
        }
      }
    }

  )

}
