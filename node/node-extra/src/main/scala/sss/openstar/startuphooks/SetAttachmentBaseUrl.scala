package sss.openstar.startuphooks

import akka.actor.Actor.Receive
import akka.actor.{ActorContext, ActorRef}
import akka.pattern.pipe
import com.typesafe.config.Config
import sss.ancillary.{Logging, ServerConfig}
import sss.openstar.StartupHooks.{DoNextHook, HookDone, NextHook, NextHookImpl}
import sss.openstar.account.NodeIdentity
import sss.openstar.attachments.RemoteAttachmentsServlet
import sss.openstar.chains.TxWriterActor.InternalCommit
import sss.openstar.controller.IdentityAttributes
import sss.openstar.identityledger.IdentityServiceQuery
import sss.openstar.network.{NetworkInterface, UPnP}
import sss.openstar.tools.SendTxSupport.SendTx

import scala.concurrent.Future

class SetAttachmentBaseUrl(
                            nodeIdentity: NodeIdentity,
                            httpConfig: ServerConfig,
                            networkInterface: NetworkInterface,
                            config: Config,
                            uPnPOpt: Option[UPnP],
                            override val nextHook: NextHook)(
  implicit identityServiceQuery: IdentityServiceQuery,
  sendTx: SendTx)

  extends NextHookImpl
    with Logging {

  def setAttachmentUrl(self: ActorRef, context: ActorContext): Receive = handleHookDone(self, context) orElse {

    case DoNextHook =>
      //do the setting of attributes.
      import context.dispatcher
      Future {
        val prefixPath = "provider.attachments.urlprefix"
        if (!config.hasPath(prefixPath)) {
          val host = networkInterface.ownSocketAddress.getHostString
          val port = httpConfig.httpPort
          uPnPOpt.foreach(uPnP => uPnP.addPort(port))

          val context = if (RemoteAttachmentsServlet.servletContext.startsWith("/")) {
            RemoteAttachmentsServlet.servletContext
          } else {
            "/" + RemoteAttachmentsServlet.servletContext
          }
          s"http://${host}:${port}${context}"
        } else {
          log.info(s"Using config file prefix url at $prefixPath")
          config.getString(prefixPath)
        }
      }.flatMap { baseUrl =>
        IdentityAttributes.setAttachmentBaseUrl(nodeIdentity, baseUrl).map {
          case None =>
            log.info(s"No change required ${nodeIdentity.id} url is already $baseUrl")
          case Some(_: InternalCommit) =>
            log.info(s"${nodeIdentity.id} url set to $baseUrl")
          case Some(x) =>
            log.warn(s"{} url not set to {}: Error {}", nodeIdentity.id, baseUrl, x)
        }.map(_ => HookDone)
      } pipeTo self
  }

}
