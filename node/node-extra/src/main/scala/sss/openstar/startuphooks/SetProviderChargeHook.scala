package sss.openstar.startuphooks

import akka.actor.Actor.Receive
import akka.actor.{ActorContext, ActorRef, Status}
import akka.pattern.pipe
import com.typesafe.config.Config
import sss.ancillary.Logging
import sss.openstar.StartupHooks._
import sss.openstar.account.NodeIdentity
import sss.openstar.controller.ChargeHelper
import sss.openstar.tools.SendTxSupport.SendTx


class SetProviderChargeHook(chargeHelper: ChargeHelper,
                            config: Config,
                            nodeIdentity: NodeIdentity,
                             override val nextHook: NextHook = terminatingHook)(implicit sendTx: SendTx)
  extends NextHookImpl with Logging {

  def setProviderChargeHook(self: ActorRef, context: ActorContext): Receive = handleHookDone(self, context) orElse {

    case Left(problem) =>
      log.warn(s"SetProviderCharge problem $problem")
      self ! HookDone

    case Right(result) =>
      log.info(s"SetProviderCharge result $result")
      self ! HookDone

    case Status.Failure(e) =>
      log.error("Startuphooks runSetProviderChargeHook problem", e)
      self ! HookDone

    case DoNextHook =>
      //do the setting of attributes.
      import context.dispatcher
      val chargePath = "provider.charge"
      if (config.hasPath(chargePath)) {
        config.getAnyRef(chargePath) match {
          case charge: Integer =>
            log.info(s"Setting provider charge for ${nodeIdentity.id} to ${charge}.")
            chargeHelper.setProviderCharge(nodeIdentity, Some(charge.toLong)) pipeTo self
          case s: String if "none".equalsIgnoreCase(s) =>
            //remove attribute
            log.info(s"$chargePath is 'none', removing provider charge attribute for ${nodeIdentity.id} ")
            chargeHelper.setProviderCharge(nodeIdentity, None) pipeTo self

          case badSetting =>
            log.warn(s"Couldn't understand setting provider.charge ${badSetting}")
            self ! HookDone
        }

      } else self ! HookDone

  }

}
