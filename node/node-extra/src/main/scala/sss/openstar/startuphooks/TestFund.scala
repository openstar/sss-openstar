package sss.openstar.startuphooks

import akka.actor.Actor.Receive
import akka.actor.{ActorContext, ActorRef, Status}
import akka.pattern.pipe
import sss.ancillary.Logging
import sss.openstar.StartupHooks.{DoNextHook, HookDone, NextHook, NextHookImpl}
import sss.openstar.account.NodeIdentity
import sss.openstar.account.Ops.MakeTxSigMaker
import sss.openstar.balanceledger.TestBalanceLedger
import sss.openstar.chains.TxWriterActor.InternalTxResult
import sss.openstar.contract.SingleIdentityEnc
import sss.openstar.ledger.{LedgerId, LedgerItem}
import sss.openstar.tools.SendTxSupport.SendTx
import sss.openstar.{MessageKeys, UniqueNodeIdentifier}
import sss.openstar.balanceledger.TxOutput

class TestFund(
                val nextHook: NextHook,
                who: UniqueNodeIdentifier)(implicit sendTx: SendTx, nodeIdentity: NodeIdentity) extends Logging with NextHookImpl {

  def testFundReceive(self: ActorRef, context: ActorContext): Receive = handleHookDone(self, context) orElse {
    case DoNextHook =>
      import context.dispatcher
      val out = TxOutput(1000, SingleIdentityEnc(who))

      TestBalanceLedger.createTestCurrencyTx(
        nodeIdentity.txSigMaker,
        out,
        LedgerId(MessageKeys.TestBalanceLedger)
      ) pipeTo self

    case li : LedgerItem =>
      import context.dispatcher
      sendTx(li) pipeTo self

    case Status.Failure(e) =>
      log.warn("Failed to test fund.{}", e)
      self ! HookDone

    case result: InternalTxResult =>
      log.warn("Test fund result.{}", result)
      self ! HookDone

  }

}
