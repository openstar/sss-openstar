package sss.openstar.wallet

import java.util.Date
import scorex.crypto.signatures.PublicKey
import sss.ancillary.ByteArrayEncodedStrOps._
import sss.db._
import sss.openstar.UniqueNodeIdentifier
import sss.openstar.schemamigration.SqlSchemaNames.ColumnNames._
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.schemamigration.SqlSchemaNames.TableNames.publicKeyTrackerTableName

/**
  * Created by alan on 6/28/16.
  */
class PublicKeyTracker(nodeId :UniqueNodeIdentifier)(implicit db: Db) {

  def isTracked(pKey: PublicKey): Boolean = isTrackedBase64(pKey.toBase64Str)

  def isTrackedBase64(pKey: String): Boolean = {
    table
      .find(
        where(
          nodeIdCol -> nodeId,
          pkeyCol -> pKey
        )
      ).dbRunSyncGet.isDefined
  }

  def keys: Seq[String] = table
    .filter(
      where(nodeIdCol -> nodeId)
    ).dbRunSyncGet.map (_.string(pkeyCol))

  def track(pKey: PublicKey): Unit = trackBase64(pKey.toBase64Str)

  def unTrack(pKey: PublicKey): Unit = unTrackBase64(pKey.toBase64Str)

  def unTrackBase64(pKey: String): Boolean = table
    .delete(
      where(
        nodeIdCol -> nodeId,
        pkeyCol -> pKey
      )
    ).dbRunSyncGet == 1

  def trackBase64(pKey: String): Unit = table
    .insert(Map(
      nodeIdCol -> nodeId,
      pkeyCol -> pKey,
      createdAtCol -> new Date().getTime
    )).dbRunSyncGet

  private val table = db.table(publicKeyTrackerTableName)
}
