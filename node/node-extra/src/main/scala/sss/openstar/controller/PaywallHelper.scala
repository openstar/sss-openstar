package sss.openstar.controller

import sss.openstar.UniqueNodeIdentifier
import sss.openstar.account.NodeIdentity
import sss.openstar.controller.ProviderHelper.log
import sss.openstar.identityledger.{IdentityServiceQuery, SetPaywall}
import sss.openstar.tools.SendTxSupport.SendTx
import sss.openstar.ui.rpc.{PaywallCategory, ResultOk, problem}

import scala.concurrent.{ExecutionContext, Future}

object PaywallHelper {


  def setPaywallCategory(user: NodeIdentity,
                         paywallCategory: PaywallCategory)(implicit identityServiceQuery: IdentityServiceQuery,
                                                           sendTx: SendTx,
                                                           ec: ExecutionContext
                        ): Future[ResultOk] = {
    Utils.simplySignedIdentityLedgerItem(user, SetPaywall(user.id, paywallCategory.category, Some(paywallCategory.amount)))
  } flatMap { le =>
    sendTx(le).toOkResult
  } recover {
    case e =>
      log.error("Failed to create PaywallCategory tx", e)
      problem("Internal error - failed to create tx, see logs")
  }

  def deletePaywallCategory(user: NodeIdentity,
                            paywallCategory: String)(implicit identityServiceQuery: IdentityServiceQuery,
                                                     sendTx: SendTx, ec: ExecutionContext
                           ): Future[ResultOk] = {
    Utils.simplySignedIdentityLedgerItem(user, SetPaywall(user.id, paywallCategory, None))
  } flatMap { le =>
    sendTx(le).toOkResult
  } recover {
    case e =>
      log.error("Failed to create PaywallCategory tx", e)
      problem("Internal error - failed to create tx, see logs")
  }


  def getPaywallCategories(uniqueNodeIdentifier: UniqueNodeIdentifier)(
    implicit identityService: IdentityServiceQuery
  ): Seq[PaywallCategory] = {
    identityService
      .paywalls(uniqueNodeIdentifier)
      .map(pw => PaywallCategory(pw.category, pw.amount.get))
  }

}
