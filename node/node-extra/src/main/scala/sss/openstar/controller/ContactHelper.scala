package sss.openstar.controller

import akka.util.ByteString
import sss.openstar.Currency
import sss.openstar.controller.SendMessage.SubmitMessage
import sss.openstar.message.payloads.NewContactMessage
import sss.openstar.rpc.UserSession
import sss.openstar.ui.rpc.{NewContactMessage => RpcNewContactMessage, _}
import sss.openstar.util.GuidFromByteString
import sss.openstar.wallet.Wallet.CreditTooLow

import scala.concurrent.Future
import scala.util.Try
//TODO clean up use of global ec
import scala.concurrent.ExecutionContext.Implicits.global

object ContactHelper {


  //This is the number of blocks after which the tx used to pay for delivery etc
  // will become available for reclaiming.
  val defaultTimeToLiveForContactTxs = 168

  def sendRequest(session: UserSession[_ <: Currency],
                  msgFromUi: RpcNewContactMessage)(implicit
                                                   submitMessage: SubmitMessage
  ): Future[ResultOk] = Future.fromTry(Try {

    val lookedupContactAvatar =
      if (msgFromUi.contactAvatar.isDefined) msgFromUi.contactAvatar.map(bs => ByteString(bs.toByteArray))
      else {
        session.loggedInUser.avatar
      }
    (GuidFromByteString(), lookedupContactAvatar)

  }).flatMap { case (guid, lookedupContactAvatar) =>
    NewContactMessage.create(
      msgFromUi.from,
      msgFromUi.to,
      lookedupContactAvatar,
      msgFromUi.paywallCategory,
      guid,
      session.nodeId) map ((guid, _))
  } flatMap { case (guid, msg) =>
    submitMessage(session.nodeId.id,
      session.userAdaWallet,
      defaultTimeToLiveForContactTxs,
      Set(msgFromUi.to),
      msg,
      msgFromUi.parentIdOpt.flatMap(session.messages.idToGuid),
      guid
    )
  } recover {
    case _: CreditTooLow => problem(CreditTooLowProblem)
  } map(_ => ok())

}
