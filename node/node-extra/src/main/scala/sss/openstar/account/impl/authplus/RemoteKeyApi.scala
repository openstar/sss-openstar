package sss.openstar.account.impl.authplus

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.scaladsl.Source
import iog.psg.client.messagesigning.ThirdPartyMessageSigningClient.ThirdPartyDetails
import iog.psg.client.messagesigning.{MessageType, ThirdPartyMessageSigningClient}
import iog.psg.messagesigning.keys.KeyType.KeyType
import iog.psg.service.messagesigning.CreateLinkResponse.Either.{NewLink, Problem, QrUrl}
import iog.psg.service.messagesigning.{CreateLinkResponse, LinkMessage, UserSig}
import sss.openstar.ui.rpc.{AppError, CreateLinkResult, UserTagKey}

import scala.concurrent.{ExecutionContext, Future}


class RemoteKeyApiCall(private val client: ThirdPartyMessageSigningClient,
                       sourceUrl: String)(implicit private val thirdPartyDetails: ThirdPartyDetails) {

  def createLink(userIdentifier: String,
                 keyTag: String,
                 preferedKeyType: String,
                 nickName: String,
                 description: String): Source[CreateLinkResult, NotUsed] = {
    client.createLinkSource(
      userIdentifier,
      keyTag,
      sourceUrl,
      preferedKeyType,
      Seq.empty,
      nickName: String,
      description: String
    ).map{
      case CreateLinkResponse(Problem(value), _) =>
        CreateLinkResult.of(CreateLinkResult.Either.Problem(AppError.of(value.code, value.msg)))
      case CreateLinkResponse(QrUrl(value), _) =>
        CreateLinkResult.of(CreateLinkResult.Either.Url(value))
      case CreateLinkResponse(NewLink(value: LinkMessage), _) =>
        CreateLinkResult.of(CreateLinkResult.Either.WhoTagKey(UserTagKey.of(
          value.userIdentifier,
          keyTag,
          value.publicKey,
          value.keyType)))

      case CreateLinkResponse(_, _) => // sends Beats and unknowns
        CreateLinkResult.of(CreateLinkResult.Either.Empty)
    }
  }

  def requestSignature(clientPublicKey: Array[Byte],
                       keyType: KeyType,
                       msgHash: MessageType,
                       userIdentifier: String,
                       tagForSig: String,
                       description: String)(implicit ec: ExecutionContext): Future[UserSig] =
    client.requestSignature(
      clientPublicKey,
      keyType,
      // This is a messageHash only in that we don't want a sig of the hash of this message
      // If we pass this f a Right(message) we will get back a sig of the message hash.
      msgHash,
      userIdentifier,
      tagForSig,
      description
    )

}

class RemoteKeyApi(thirdPartyDetails: ThirdPartyDetails,
                   val sourceUrl: String,
                   grpcClientConfigName: String = "message.SigningService")(implicit as: ActorSystem) {

  implicit val thirdParty: ThirdPartyDetails = thirdPartyDetails

  private lazy val client = ThirdPartyMessageSigningClient(grpcClientConfigName)
  lazy val remoteCall: RemoteKeyApiCall = new RemoteKeyApiCall(client, sourceUrl)

}
