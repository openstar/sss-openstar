package sss.openstar.account.impl.authplus

import com.google.protobuf.ByteString
import iog.psg.client.messagesigning.{NeedHashMessageType, SharedSecretMessageType}
import iog.psg.messagesigning.keys.KeyType
import scorex.crypto.signatures.{MessageToSign, PublicKey, SharedSecret, Signature}
import sss.openstar.account._
import sss.openstar.account.impl.authplus.AuthPlusAccountSignerCreator.mapKeyType
import supertagged.PostfixSugar

import scala.concurrent.duration.Duration
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Using


class AuthPlusAccountSigner(
                             signerDetails: NodeIdentityManager.SignerDetails,
                             keyFactory: KeyFactory,
                             remoteApi: RemoteKeyApi)(implicit ec:ExecutionContext) extends NodeVerifierSigner {

  require(keyFactory.supportedKeyTypes.contains(mapKeyType(signerDetails.signerDescriptor.keyType)),
    s"Keytype ${signerDetails.signerDescriptor.keyType} not supported: ${keyFactory.supportedKeyTypes.mkString}")

  override def signerOpt: Option[NodeSigner] = Some {
    new NodeSigner {

      override def maxExpectedSignatureWaitDuration: Duration =
        AuthPlusAccountSignerCreator.MaxExpectedSignatureWaitDuration

      override def verifier: NodeVerifierSigner = AuthPlusAccountSigner.this

      override def sign(msg: MessageToSign, timeoutAfter: Duration): Future[Signature] = {

        remoteApi.remoteCall.requestSignature(
            signerDetails.publicKey,
            KeyType.withName(mapKeyType(signerDetails.signerDescriptor.keyType)),
            NeedHashMessageType(ByteString.copyFrom(msg)),
            nodeIdTag.nodeId,
            nodeIdTag.tag,
            "Openstar requests signature"
        ).map(_.signature.toByteArray @@ Signature)
      }

      override def createSharedSecret(publicKey: PublicKey, timeoutAfter: Duration): Future[SharedSecret] =

        remoteApi.remoteCall.requestSignature(
            signerDetails.publicKey,
            KeyType.withName(mapKeyType(signerDetails.signerDescriptor.keyType)),
            SharedSecretMessageType(ByteString.copyFrom(publicKey)),
            nodeIdTag.nodeId,
            nodeIdTag.tag,
            "Openstar requests shared secret"
        ).map(_.signature.toByteArray @@ SharedSecret)
    }
  }

  override def nodeIdTag: NodeIdTag = signerDetails.nodeIdTag

  override def verifier: Verifier = new Verifier {
    override def typedPublicKey: TypedPublicKey =
      TypedPublicKey(signerDetails.publicKey, signerDetails.signerDescriptor.keyType)

    override def verify(message: MessageToSign, signature: Signature): Boolean = {
      keyFactory(signerDetails.signerDescriptor.keyType)
        .verify(signerDetails.publicKey, message, signature)
    }
  }

  override def signerDescriptor: SignerFactory.SignerDescriptor = signerDetails.signerDescriptor
}
