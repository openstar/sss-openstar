package sss.openstar.account.impl.webauthn

import com.google.protobuf.ByteString
import iog.psg.client.messagesigning.NeedHashMessageType
import iog.psg.messagesigning.keys.KeyType
import scorex.crypto.signatures.{MessageToSign, PublicKey, SharedSecret, Signature}
import sss.openstar.account._
import sss.openstar.account.impl.authplus.AuthPlusAccountSignerCreator.mapKeyType
import sss.openstar.account.impl.authplus.RemoteKeyApi
import sss.openstar.crypto.keypairs.ECDSA256AccountKeys
import supertagged.PostfixSugar

import scala.concurrent.duration.Duration
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Using


class WebAuthnAccountSigner(
                             signerDetails: NodeIdentityManager.SignerDetails,
                             webAuthnSigs: WebAuthnSignatures)(implicit ec:ExecutionContext) extends NodeVerifierSigner {

  require(signerDetails.signerDescriptor.keyType == ECDSA256AccountKeys.keysType,
    s"Only ${ECDSA256AccountKeys.keysType} supported not ${signerDetails.signerDescriptor.keyType}")

  override def signerOpt: Option[NodeSigner] = Some {
    new NodeSigner {

      override def maxExpectedSignatureWaitDuration: Duration = WebAuthnAccountSignerCreator.MaxExpectedSignatureWaitDuration

      override def verifier: NodeVerifierSigner = WebAuthnAccountSigner.this

      override def sign(msg: MessageToSign, timeoutAfter: Duration): Future[Signature] = {
        webAuthnSigs.requestSignature(signerDetails.nodeIdTag.nodeId, msg)
      }

      override def createSharedSecret(publicKey: PublicKey, timeoutAfter: Duration): Future[SharedSecret] =
        Future.failed(new IllegalArgumentException(s"${this.getClass.getName} doesn't support creating shared secrets"))
    }
  }

  override def nodeIdTag: NodeIdTag = signerDetails.nodeIdTag

  override def verifier: Verifier = new Verifier {
    override def typedPublicKey: TypedPublicKey =
      TypedPublicKey(signerDetails.publicKey, signerDetails.signerDescriptor.keyType)

    override def verify(message: MessageToSign, signature: Signature): Boolean = {
      ECDSA256AccountKeys
        .verify(signerDetails.publicKey, message, signature)
    }
  }

  override def signerDescriptor: SignerFactory.SignerDescriptor = signerDetails.signerDescriptor
}
