package sss.openstar.account.impl.authplus

import sss.ancillary.Logging
import sss.openstar.account.NodeIdentityManager.{Credentials, SignerDetails}
import sss.openstar.account.SignerFactory.SignerDescriptor
import sss.openstar.account.{NodeVerifierSigner, SignerFactory, SignerInteractionTypes}
import sss.openstar.crypto.keypairs.{Curve25519AccountKeys, Ed25519AccountKeys}

import scala.concurrent.Future

case class AuthPlusSignerFactory(
                                            authPlusAccountSignerCreator: AuthPlusAccountSignerCreator,
                                            seqCredentials: Seq[Credentials] = Seq.empty,
                               ) extends SignerFactory with Logging {

  override def lookupMatcher: PartialFunction[SignerDetails, Future[NodeVerifierSigner]] = {
    case s if authPlusAccountSignerCreator.signerMatcher.isDefinedAt(s.signerDescriptor) =>
      authPlusAccountSignerCreator.signerMatcher(s.signerDescriptor).createSigner(s, seqCredentials)
  }

  protected override def withMergedCredentials(seqMergedCredentials: Seq[Credentials]): SignerFactory =
    copy(seqCredentials = seqMergedCredentials)

  override def signerDescriptors: Seq[SignerDescriptor] = Seq(
    SignerDescriptor(
      Curve25519AccountKeys.keysType,
      AuthPlusAccountSignerCreator.SecurityLevel,
      AuthPlusAccountSignerCreator.SignerType,
      SignerInteractionTypes.ManualSignerInteractionType
    ),
    SignerDescriptor(
      Ed25519AccountKeys.keysType,
      AuthPlusAccountSignerCreator.SecurityLevel,
      AuthPlusAccountSignerCreator.SignerType,
      SignerInteractionTypes.ManualSignerInteractionType
    )
  )

}
