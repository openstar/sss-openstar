package sss.openstar.account.impl.authplus

import iog.psg.messagesigning.keys.KeyUtilFactory
import sss.openstar.account.NodeSigner.{InteractionType, SecurityLevel, SignerType}
import sss.openstar.account.SignerFactory.SignerDescriptor
import sss.openstar.account._
import sss.openstar.account.impl.authplus.AuthPlusAccountSignerCreator._

import scala.concurrent.duration.{Duration, DurationInt}
import scala.concurrent.{ExecutionContext, Future}


object AuthPlusAccountSignerCreator {
  val SignerType: SignerType = "AuthPlusAccountSigner"
  val SecurityLevel: SecurityLevel = SignerSecurityLevels.SecurityLevel3
  val InteractionType: InteractionType = SignerInteractionTypes.ManualSignerInteractionType
  val MaxExpectedSignatureWaitDuration: Duration = 1.minute

  def mapKeyType(osKeyType: String): String = osKeyType match {
    case x => x // they currently support identical keys and names...
  }

}
class AuthPlusAccountSignerCreator(keyFactory: KeyFactory, remoteKeyApi: RemoteKeyApi)(implicit ec: ExecutionContext) extends CreatesSigner {

  override def signerMatcher: PartialFunction[SignerFactory.SignerDescriptor, CreatesSigner] = {
    case SignerDescriptor(
    keyType,
    secLevel,
    `SignerType`,
    `InteractionType`
    ) if SecurityLevel >= secLevel && KeyUtilFactory.supportsKeyType(mapKeyType(keyType)) => this
  }

  override def createSigner(signerDetails: NodeIdentityManager.SignerDetails, seqCreds: Seq[NodeIdentityManager.Credentials]): Future[NodeVerifierSigner] = Future {
    new AuthPlusAccountSigner(signerDetails,keyFactory, remoteKeyApi)
  }
}
