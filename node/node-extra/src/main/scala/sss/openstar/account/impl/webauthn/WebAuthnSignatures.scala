package sss.openstar.account.impl.webauthn

import scorex.crypto.signatures.Signature
import sss.ancillary.Logging
import sss.openstar.BusEvent
import sss.openstar.crypto.SeedBytes
import sss.openstar.network.MessageEventBus
import sss.openstar.rpc.RpcBridgeEventSource.RpcBridgeEvent
import sss.openstar.ui.rpc.BridgeClient.GenericEvents

import scala.collection.concurrent.TrieMap
import scala.concurrent.{Future, Promise}


case class WebAuthnSignatureEvent(messageToSign: Array[Byte]) extends BusEvent

class WebAuthnSignatures(sigDelayToleranceMs: Long)(implicit messageEventBus: MessageEventBus) extends Logging {

  private val challengeCache = new TrieMap[String, (Long, Promise[Signature])]()

  def respondToSignatureRequest(id: String, signature: Signature): Boolean = {
    val now = System.currentTimeMillis()
    challengeCache.filterInPlace {
      case (k, (ts, _)) => ts + sigDelayToleranceMs >= now
    }.remove(id).map {
      case (_, p) =>
        log.info("Completing sig promise")
        p.success(signature)
    }.isDefined
  }

  def requestSignature(id: String, message: Array[Byte]): Future[Signature] = {
    val now = System.currentTimeMillis()
    val p = Promise[Signature]()
    challengeCache.put(id, (now, p))
    messageEventBus.publish(WebAuthnSignatureEvent(message))
    p.future
  }

}
