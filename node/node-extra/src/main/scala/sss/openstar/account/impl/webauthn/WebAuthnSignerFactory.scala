package sss.openstar.account.impl.webauthn

import sss.ancillary.Logging
import sss.openstar.account.NodeIdentityManager.{Credentials, SignerDetails}
import sss.openstar.account.SignerFactory.SignerDescriptor
import sss.openstar.account.{NodeVerifierSigner, SignerFactory, SignerInteractionTypes}
import sss.openstar.crypto.keypairs.{ECDSA256AccountKeys, Ed25519AccountKeys}

import scala.concurrent.Future

case class WebAuthnSignerFactory(
                                  webAuthnAccountSignerCreator: WebAuthnAccountSignerCreator,
                                            seqCredentials: Seq[Credentials] = Seq.empty,
                               ) extends SignerFactory with Logging {

  override def lookupMatcher: PartialFunction[SignerDetails, Future[NodeVerifierSigner]] = {
    case s if webAuthnAccountSignerCreator.signerMatcher.isDefinedAt(s.signerDescriptor) =>
      webAuthnAccountSignerCreator.signerMatcher(s.signerDescriptor).createSigner(s, seqCredentials)
  }

  protected override def withMergedCredentials(seqMergedCredentials: Seq[Credentials]): SignerFactory =
    copy(seqCredentials = seqMergedCredentials)

  override def signerDescriptors: Seq[SignerDescriptor] = Seq(
    SignerDescriptor(
      ECDSA256AccountKeys.keysType,
      WebAuthnAccountSignerCreator.SecurityLevel,
      WebAuthnAccountSignerCreator.SignerType,
      SignerInteractionTypes.ManualSignerInteractionType
    )
  )

}
