package sss.openstar.account.impl.webauthn


import sss.openstar.account.NodeSigner.{InteractionType, SecurityLevel, SignerType}
import sss.openstar.account.SignerFactory.SignerDescriptor
import sss.openstar.account._

import sss.openstar.crypto.keypairs.ECDSA256AccountKeys

import scala.concurrent.duration.{Duration, DurationInt}
import scala.concurrent.{ExecutionContext, Future}


object WebAuthnAccountSignerCreator {
  val SignerType: SignerType = "WebAuthnAccountSigner"
  val SecurityLevel: SecurityLevel = SignerSecurityLevels.SecurityLevel3
  val InteractionType: InteractionType = SignerInteractionTypes.ManualSignerInteractionType
  val MaxExpectedSignatureWaitDuration: Duration = 1.minute

  def mapKeyType(osKeyType: String): String = osKeyType match {
    case x => x // they currently support identical keys and names...
  }

}
class WebAuthnAccountSignerCreator(
                                   webAuthnSigs: WebAuthnSignatures)(implicit ec: ExecutionContext) extends CreatesSigner {

  override def signerMatcher: PartialFunction[SignerFactory.SignerDescriptor, CreatesSigner] = {
    case SignerDescriptor(
    ECDSA256AccountKeys.`keysType`,
    secLevel,
    WebAuthnAccountSignerCreator.`SignerType`,
    WebAuthnAccountSignerCreator.`InteractionType`
    ) if WebAuthnAccountSignerCreator.SecurityLevel >= secLevel => this
  }

  override def createSigner(signerDetails: NodeIdentityManager.SignerDetails, seqCreds: Seq[NodeIdentityManager.Credentials]): Future[NodeVerifierSigner] = Future {
    new WebAuthnAccountSigner(signerDetails, webAuthnSigs)
  }
}
