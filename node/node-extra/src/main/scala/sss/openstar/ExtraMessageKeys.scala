package sss.openstar

import sss.ancillary.Logging
import sss.openstar.block._
import sss.openstar.block.serialize._
import sss.openstar.block.signature.BlockSignatures.BlockSignature
import sss.openstar.common.block._
import sss.openstar.common.block.serialize.{BlockChainTxIdSerializer, BlockChainTxSerializer}
import sss.openstar.eventbus._
import sss.openstar.message._
import sss.openstar.message.serialize._
import sss.openstar.message.serialize.ob.MsgResponseSerializer
import sss.openstar.peers._
import sss.openstar.peers.serialize.{CapabilitiesSerializer, PeerPageSerializer, SeqPeerPageResponseSerializer}

object ExtraMessageKeys extends MessageKeys with Logging {

  val MessageQuery: Byte = 60
  val PaywallEnvelope: Byte = 61
  val EndMessagePage: Byte = 62
  val EndMessageQuery: Byte = 63
  val MessageResponse: Byte = 64
  val PagedMessageMsg: Byte = 65

  private val localMessages: MessageInfos =
    MessageInfoComposite[MessageResponse](MessageResponse, classOf[MessageResponse], MsgResponseSerializer) +:
    MessageInfoComposite[EndMessageQuery](EndMessageQuery, classOf[EndMessageQuery], EndMessageQuerySerializer) +:
    MessageInfoComposite[EndMessagePage](EndMessagePage, classOf[EndMessagePage], EndMessagePageSerializer) :+
    MessageInfoComposite[PaywallEnvelope](PaywallEnvelope, classOf[PaywallEnvelope], PaywallEnvelopeSerializer) :+
    MessageInfoComposite[PagedMessage](PagedMessageMsg, classOf[PagedMessage], PagedMsgSerializer) :+
    MessageInfoComposite[MessageQuery](MessageQuery, classOf[MessageQuery], MsgQuerySerializer)

  val extraMessages = localMessages ++ messages

}
