package sss.openstar.tools

import scorex.crypto.signatures.PublicKey
import sss.ancillary.ByteArrayEncodedStrOps._
import sss.ancillary.Logging
import sss.openstar.account.{AccountKeysFactory, NodeVerifier}
import sss.openstar.common.builders.{ConfigBuilder, IOExecutionContextBuilder, NodeIdTagBuilder, NodeIdentityManagerBuilder, SignerFactoryBuilder}
import sss.openstar.crypto.keypairs.DefaultAccountKeysFactoryBuilder
import sss.openstar.nodebuilder._
import sss.openstar.wallet.PublicKeyTracker


/**
  * Created by alan on 6/7/16.
  */
object AddStandalonePublicKeyToNodeWallet extends DefaultAccountKeysFactoryBuilder {

  class LoadDb(val confName: String) extends
    DbBuilder with
    NodeConfigBuilder with
    ConfigBuilder with
    Logging with
    IOExecutionContextBuilder {
    override lazy val configName: String = confName
  }


  def main(args: Array[String]): Unit = {

    if(args.length == 3) {

      val dbLoader = new LoadDb(args(0))
      import dbLoader.db

      val identity = args(1)
      val tracker = new PublicKeyTracker(identity)
      val pKey64Str = args(2)
      val pKey = PublicKey(pKey64Str.fromBase64Str)

      if(tracker.isTracked(pKey)) {
        println("Key already in nodes list.")
      } else {
        tracker.trackBase64(pKey64Str)
      }
      println(s"Keys for $identity now ")
      tracker.keys foreach (println(_))

    } else println("Provide the node config string, the " +
      "identity who's wallet the key should appear in, and " +
      "the base 64 Hex representation of the public key")
  }
}
