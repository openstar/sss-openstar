package sss.openstar.tools

import sss.ancillary.Logging

import scala.concurrent.duration.Duration
import scala.util.{Failure, Try}

object Retry extends Logging {
  def retry[T](n: Int, interval:Duration)(f: => T): Try[T] = {
    val tries = LazyList.fill(n)(Try {
      //todo basic impl for testing
      Thread.sleep(interval.toMillis)
      f
    } recoverWith {
      case e =>
        log.warn(e.getMessage)
        Failure(e)
    })
    tries
      .find(_.isSuccess)
      .getOrElse(tries.head)
  }
}
