package sss.openstar.test

import sss.openstar.test.CoinToss.Flip

import scala.util.Random

object CoinToss {
  case class Flip(coinToss: CoinToss, isHeads: Boolean) {
    def opt[A](a: A): Option[A] =
      if(isHeads) Some(a)
      else None
  }

}
case class CoinToss(probabilityOfHead: Double = .5) {

  private val probabilityAsInt: Int = (probabilityOfHead * 1000).toInt

  if(probabilityOfHead != 0.0) {
    require(probabilityOfHead >= 0.001, s"Probability must precise to a max of 3 digits ${probabilityOfHead}")
  }

  require(probabilityOfHead >= 0.0 && probabilityOfHead <= 1.0, "Probability must be between 0 and 1")

  def flip(): Flip = {
    Flip(this, Random.between(1, 1000) <= probabilityAsInt)
  }

}

