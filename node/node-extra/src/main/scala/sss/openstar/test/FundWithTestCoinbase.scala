package sss.openstar.test

import akka.actor.Status.{Failure => FutureFailure}
import akka.actor.{Actor, ActorLogging}
import akka.pattern.pipe
import sss.openstar.balanceledger.{TestBalanceLedger, TxOutput}
import sss.openstar.chains.TxWriterActor.{InternalAck, InternalCommit, InternalNack, InternalTempNack}
import sss.openstar.contract.SingleIdentityEnc
import sss.openstar.ledger.{LedgerId, LedgerItem}
import sss.openstar.test.MultiIdentityTestTransactionSenderInternalClaim.Funded
import sss.openstar.tools.SendTxSupport.SendTx

class FundWithTestCoinbase(newId: String,
                           amount: Long,
                           testLedgerId: LedgerId)(implicit sendTx: SendTx) extends Actor with ActorLogging {

  import context.dispatcher

  private def createCoinBaseForTestLedger(newIdentity: String, ledgerId: LedgerId): LedgerItem = {
    val output = TxOutput(amount, SingleIdentityEnc(newIdentity))
    //TestBalanceLedger.createTestCurrencyTx(output, ledgerId)
    throw new IllegalStateException("CANT CALL this without updating to include a node id to pay toll")
  }

  val item = createCoinBaseForTestLedger(newId, testLedgerId)
  sendTx(item, true) pipeTo self

  override def receive: Receive = {

    case com: InternalCommit =>
      context.parent ! Funded(newId, amount)
      context.stop(self)

    case r: InternalNack =>
      log.info(r.toString + s"Fund Nack ${r.txMsg.msg}")
      context.stop(self)

    case ack: InternalAck =>
    //log.info(s"Got ack: $ack")

    case r: InternalTempNack =>

    case FutureFailure(e) =>
      log.warning("Failed " + e.toString)
      context.stop(self)


  }
}
