package sss.openstar.contact

import akka.util.ByteString
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.openstar.DummySeedBytes
import sss.openstar.contacts.ContactService
import sss.openstar.db.DbWithMigrationFixture

import scala.language.postfixOps
import scala.util.Try

class ContactServiceSpec extends AnyFlatSpec with Matchers with DbWithMigrationFixture{

  val sut = new ContactService()
  val userName = "newuser"
  val otherUserName = "newuser2"
  val category1 = "asdasdasd"
  val category2 = "asdasdasd2"
  val avatarOpt = Some(ByteString(DummySeedBytes.apply(2048)))

  "Contact Service" should "support repeated addition of contact" in {

    val user = sut.addContact(userName, None)
    val foundUser = sut.findContact(userName).get
    assert(user === foundUser)

    val userAgain = sut.addContact(userName, avatarOpt)
    val foundUserAgain = sut.findContact(userName).get
    assert(userAgain === foundUserAgain)

  }

  it should "support setting contact avatar" in {
    val foundUser = sut.findContact(userName).get
    assert(foundUser.avatar == avatarOpt)
    foundUser.setAvatar(None)
    val foundUserAgain = sut.findContact(userName).get
    assert(foundUserAgain.avatar.isEmpty)
    foundUser.setAvatar(avatarOpt)

  }

  it should "support setting category details" in {
    val foundUser = sut.findUser(userName).get
    assert(foundUser.getCategoryDetails(otherUserName).isEmpty)
    foundUser.setCategoryDetails(otherUserName, category1, category2, avatarOpt)
    assert(sut.findContact(otherUserName).isDefined)
    val foundDetails = foundUser.getCategoryDetails(otherUserName).get
    assert(foundDetails.contact == otherUserName)
    assert(foundDetails.nameCategoryForContact == category1)
    assert(foundDetails.contactCategoryForName == category2)
  }

  it should "prevent self setting and getting category details" in {
    val foundUser = sut.findUser(userName).get

    assert(Try(foundUser.getCategoryDetails(userName)).isFailure)
    assert(Try(foundUser.setCategoryDetails(userName, category1, category2, avatarOpt)).isFailure)
  }

  it should "support setting reverse category details" in {
    val foundUser = sut.findUser(otherUserName).get
    assert(foundUser.getCategoryDetails(userName).isEmpty)

    foundUser.setCategoryDetails(userName, category1, category2, None)

    val foundDetails = foundUser.getCategoryDetails(userName).get
    assert(foundDetails.contact == userName)
    assert(foundDetails.nameCategoryForContact == category1)
    assert(foundDetails.contactCategoryForName == category2)
  }

  it should "support listing contacts" in {
    val foundUser = sut.findUser(userName).get
    assert(foundUser.contacts == Seq(sut.findContact(otherUserName).get))
  }

}
