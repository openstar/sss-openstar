package sss.openstar.tools

import org.scalatest.concurrent.ScalaFutures
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers
import org.scalatest.time.{Minutes, Seconds, Span}
import sss.ancillary.FutureOps.AwaitResult
import sss.openstar.account.{AccountKeysFactoryFixture, KeyPersisterFixtureWithScalaFutures, NodeIdentity, NodeIdentityManagerFixture}
import sss.openstar.chains.TxWriterActor._
import sss.openstar.common.builders.{ConfigBuilder, CpuBoundExecutionContextFixture, PhraseFixture}
import sss.openstar.identityledger.IdentityService
import sss.openstar.tools.SendTxSupport.SendTx

import scala.concurrent.Future
import scala.util.Random

class ClaimIdentitySpec
  extends AnyFunSuite
    with Matchers
    with ScalaFutures
    with KeyPersisterFixtureWithScalaFutures
    with NodeIdentityManagerFixture
    with AccountKeysFactoryFixture
    with CpuBoundExecutionContextFixture
    with PhraseFixture
    with ConfigBuilder {

  implicit override val patienceConfig: PatienceConfig =
    PatienceConfig(timeout = scaled(Span(1, Minutes)), interval = scaled(Span(1, Seconds)))

  private val identity = {
    "identity" + Random.alphanumeric.take(5).mkString
  }

  lazy val hostingNodeIdentity: NodeIdentity = addToFixtureAndGetNodeIdentity("hostingFaker")

  test("claim()") {

    assert(!keyPersister.keyExists(identity, IdentityService.defaultTag).futureValue)

    assert(
      claim(
        hostingNodeIdentity,
        sendTxBuilder(InternalTempNack(1, null), identity)
      ).failed.futureValue.getMessage.contains("System temporarily busy, try again.")
    )

    assert(!keyPersister.keyExists(identity, IdentityService.defaultTag).futureValue)

    assert(
      claim(
        hostingNodeIdentity,
        sendTxBuilder(InternalNack(1, null), identity)
      ).failed.futureValue.getMessage.contains("Failed to claim identity")
    )

    assert(!keyPersister.keyExists(identity, IdentityService.defaultTag).futureValue)


    assert(
      claim(
        hostingNodeIdentity,
        sendTxBuilder(InternalCommit(1, null), identity)
      ).futureValue.id === identity
    )

    assert(keyPersister.keyExists(identity, IdentityService.defaultTag).futureValue)

    assert(
      claim(
        hostingNodeIdentity,
        sendTxBuilder(InternalCommit(1, null), identity)
      ).failed.futureValue.getMessage.contains(s"Identifier=`$identity` is already taken")
    )

    assert(keyPersister.keyExists(identity, IdentityService.defaultTag).futureValue)
  }

  private def claim(
                     hostingNodeIdentity: NodeIdentity,
                     sendTx: SendTx
                   ): Future[NodeIdentity] = {
    implicit val stx: SendTx = sendTx

    ClaimIdentity.claimAndSetup(
      identity = identity,
      phrase = testPassword,
      rescuers = Set.empty,
      toIdentityLedgerOwner = x => Some(x),
      hostingNodeIdentity,
      keyPersister = keyPersister,
      keyGenerator = defaultAccountKeysFactory,
      claimUrl = "",
    )
  }

  private def sendTxBuilder(res: InternalTxResult, id: String): SendTx = (_, _, _, _) => {
    res match {
      case InternalCommit(chainId, blTxId) =>
        addNodeIdentityToFixture(identity, IdentityService.defaultTag, testPassword)
      case _ =>
    }
    Future.successful(res)
  }

  private def identityAlreadyExists(flag: Boolean): (String, String) => Boolean = (_, _) => flag
}
