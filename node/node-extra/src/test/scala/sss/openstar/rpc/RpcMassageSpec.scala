package sss.openstar.rpc

import cats.implicits._
import org.scalatest.EitherValues
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.openstar.rpc
import sss.openstar.ui.rpc.{Problem, ResultImplicits}

class RpcMassageSpec extends AnyFlatSpec with Matchers with EitherValues {

  "ProviderCharge" should "round-trip correctly" in {
    val totallyEmpty = None.asRight[Problem]
    val empty = Option(None).asRight[Problem]
    val data = Option(Option(0L)).asRight[Problem]

    ResultImplicits.massageProviderCharge.toResult(rpc.massageProviderCharge.massage(totallyEmpty)) shouldBe totallyEmpty
    ResultImplicits.massageProviderCharge.toResult(rpc.massageProviderCharge.massage(empty)) shouldBe empty
    ResultImplicits.massageProviderCharge.toResult(rpc.massageProviderCharge.massage(data)) shouldBe data
  }

  "Avatar" should "round-trip correctly" in {
    def err = fail("Test failed!")

    val empty = None.asRight[Problem]
    val array = Array.empty[Byte]
    val emptyData = Option(array).asRight[Problem]
    val bytes = Array[Byte](0, 42)
    val data = Option(bytes).asRight[Problem]

    ResultImplicits.massageAvatar.toResult(rpc.massageAvatar.massage(empty)) shouldBe empty
    ResultImplicits.massageAvatar.toResult(rpc.massageAvatar.massage(emptyData)).getOrElse(err).getOrElse(err) shouldBe array
    ResultImplicits.massageAvatar.toResult(rpc.massageAvatar.massage(data)).getOrElse(err).getOrElse(err) shouldBe bytes
  }

  "GetHashedIdentityBearerAttribute" should "round-trip correctly" in {
    val empty = None.asRight[Problem]
    val emptyData = Option("").asRight[Problem]
    val data = Option("foo").asRight[Problem]

    ResultImplicits.massageGetHashedIdentityBearerAttribute.toResult(rpc.massageGetHashedIdentityBearerAttribute.massage(empty)) shouldBe empty
    ResultImplicits.massageGetHashedIdentityBearerAttribute.toResult(rpc.massageGetHashedIdentityBearerAttribute.massage(emptyData)) shouldBe emptyData
    ResultImplicits.massageGetHashedIdentityBearerAttribute.toResult(rpc.massageGetHashedIdentityBearerAttribute.massage(data)) shouldBe data
  }

}