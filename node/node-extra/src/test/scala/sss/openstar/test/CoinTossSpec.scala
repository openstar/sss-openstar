package sss.openstar.test

import akka.stream.TLSClientAuth.None
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.ancillary.ByteArrayComparisonOps

class CoinTossSpec extends AnyFlatSpec with Matchers with ByteArrayComparisonOps {

  "A CoinToss with 0 probability" should "never come up heads" in {
    0 to 100 foreach { _ =>
      assert(!CoinToss(0).flip().isHeads, "No possibility of coming up")
    }
  }

  "A CoinToss with 1 probability" should "always come up heads" in {
    0 to 100 foreach { _ =>
      assert(CoinToss(1).flip().isHeads, "Always coming up")
    }
  }

  "A CoinToss" should "only accept between 0 and 1" in {
    an [IllegalArgumentException] should be thrownBy CoinToss(-0.1)
    an [IllegalArgumentException] should be thrownBy CoinToss(1.1)
    an [IllegalArgumentException] should be thrownBy CoinToss(.0009)
  }

  "A CoinToss with 50% probability" should "come up heads eventually" in {
    def tossUntil(count: Int, result: Boolean, coinToss: CoinToss): Unit = {
      if(!result) {
        tossUntil(count + 1, coinToss.flip().isHeads, coinToss)
      }
    }
    tossUntil(0, false, CoinToss())
  }

  "A CoinToss" should "map to Option" in {
    CoinToss(0).flip().opt("").isEmpty shouldBe true
    CoinToss(1).flip().opt("YES") shouldBe Some("YES")
    CoinToss(1).flip().opt(999) shouldBe Some(999)
  }

}
