package sss.openstar.nodebuilder

import akka.actor.{ActorContext, Props}
import akka.util.Timeout
import sss.openstar.common.builders.ActorSystemBuilder

import scala.concurrent.Await
import scala.concurrent.duration.{DurationInt, SECONDS}

class TestRootActor extends RootActor {

  override def postStop(): Unit =
    log.info("Test Root Actor stopped.")
}

trait TestActorContextBuilder extends RequireActorContext {
  self: ActorSystemBuilder =>

  import akka.pattern.ask

  override lazy implicit val actorContext: ActorContext = {
    implicit val timeOut: Timeout = Timeout(1, SECONDS)
    Await.result(
      actorSystem
        .actorOf(Props(classOf[TestRootActor])) ? "GIVE ME CONTEXT", 1.second)
      .asInstanceOf[ActorContext]
  }
}