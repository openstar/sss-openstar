package sss.openstar.nodebuilder

import com.typesafe.config.Config
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.ancillary.FutureOps.AwaitResult
import sss.ancillary.{ByteArrayComparisonOps, ConfigureFactory, Logging}
import sss.openstar.DummySeedBytes
import sss.openstar.account.{KeyPersisterFixture, NodeIdTag, NodeIdentity, NodeIdentityManagerFixture}
import sss.ancillary.Results.ResultOps
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.chains.Chains.GlobalChainIdBuilder
import sss.openstar.common.builders.{CpuBoundExecutionContextBuilder, CpuBoundExecutionContextFixture, IOExecutionContextBuilder, KeyPersisterBuilder, NodeIdTagBuilder, NodeIdentityBuilder, NodeIdentityManagerBuilder, PhraseFixture, RequireConfig, RequirePhrase, SeedBytesBuilder}
import sss.openstar.crypto.keypairs.DefaultAccountKeysFactoryBuilder
import sss.openstar.db.DbFixture
import sss.openstar.identityledger.IdentityRoleAttribute
import sss.openstar.identityledger.IdentityRolesOps._
import sss.openstar.schemamigration.dynamic.UtxoDbStorageMigration.UtxoDbStorageId
import sss.openstar.schemamigration.{PerformSchemaMigration, SchemaMigrationBuilder}
import sss.openstar.identityledger.BackwardsCompatibilityOps.PublicKeyOps

import scala.util.{Success, Try}

class IdentityVerifierSpec extends AnyFlatSpec with Matchers with
  ByteArrayComparisonOps with
  NetworkInterfaceBuilder with
  KeyPersisterFixture with
  NodeIdentityManagerFixture with
  NodeIdentityBuilder with
  PhraseFixture with
  SeedBytesBuilder with
  NodeIdTagBuilder with
  RequireConfig with
  IdentityServiceBuilder with
  DbFixture with
  IOExecutionContextBuilder with
  BootstrapIdentitiesBuilder with
  ChainOriginatorsBuilder with
  BootstrapDetailsBuilder with
  Logging with
  NodeConfigBuilder with
  GlobalTableNameTagBuilder with
  GlobalChainIdBuilder with
  RequireStarzBalanceLedgerId with
  RequireCardanoLedgerId with
  BlockHeightToTableBuilder with
  SchemaMigrationBuilder with
  PerformSchemaMigration with
  DefaultAccountKeysFactoryBuilder with
  DefaultIdentityVerificationBuilder with
  CpuBoundExecutionContextFixture {


  override protected val tablePartitionsCount: Int = 100

  override lazy val prefixedConf: Config = ConfigureFactory.config

  override def getChainOriginators(): Try[ChainIdsKeys] = Success(Set.empty)

  val nodeId = "somenode"
  lazy val ni = addToFixtureAndGetNodeIdentity(nodeId)

  identityService
    .claim(ni.publicKey.toDetails(nodeId, NodeIdTag.defaultTag), Seq(IdentityRoleAttribute()))
    .dbRunSyncGet

  val msg = DummySeedBytes(320)
  val k = nodeIdentityManager(nodeId, NodeIdTag.defaultTag, "Password10!").await()
  lazy val sig  = ni.defaultNodeVerifier.signer.sign(msg).await()


  "A id verifier" should "not pass if not network auditor" in {
    assert(!identityService.roleOwnedAttribute(nodeId).isNetworkAuditor)
    val r = idVerifier.verify(sig, msg, nodeId, ni.tag)
    assert(r.isError, "Cannot pass without auditor role")
  }

  it should "pass if it is a network auditor" in {
    identityService.setOwnedAttribute(nodeId, IdentityRoleAttribute().setNetworkAuditor()).dbRunSyncGet
    val r = idVerifier.verify(sig, msg, nodeId, ni.tag)
    assert(r.isOk, "Network Id has auditor role?")
  }

  override protected val blocksPerTable: Int = 10

  override def migrateSqlSchema(): Unit = migrateSqlSchema(Set.empty[UtxoDbStorageId])

}
