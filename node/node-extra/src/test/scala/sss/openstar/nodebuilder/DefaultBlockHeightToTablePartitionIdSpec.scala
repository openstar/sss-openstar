package sss.openstar.nodebuilder

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper
import sss.openstar.chains.Chains.GlobalChainIdBuilder
import sss.openstar.schemamigration.RequireTablePartitionsCount

class DefaultBlockHeightToTablePartitionIdSpec
    extends AnyFlatSpec
    with BlockHeightToTableBuilder
    with GlobalChainIdBuilder
    with RequireTablePartitionsCount {

  override val tablePartitionsCount: Int = 100
  override protected val blocksPerTable: Int = 100

  "Default BlockHeightToTablePartitionId " should "calculate correct partition ids" in {
    blockHeightToTablePartitionId(1) shouldEqual 1
    blockHeightToTablePartitionId(blocksPerTable) shouldEqual 1
    blockHeightToTablePartitionId(1 + blocksPerTable) shouldEqual 2
    blockHeightToTablePartitionId(blocksPerTable * tablePartitionsCount / 2) shouldEqual tablePartitionsCount / 2
    blockHeightToTablePartitionId(blocksPerTable * tablePartitionsCount - 1) shouldEqual tablePartitionsCount
    blockHeightToTablePartitionId(blocksPerTable * tablePartitionsCount) shouldEqual tablePartitionsCount
  }

  it should "throw illegal argument exception if block height is too small" in {
    intercept[IllegalArgumentException](
      blockHeightToTablePartitionId(-1)
    ).getMessage shouldEqual "requirement failed: " + blockHeightTooSmallMsg(-1)
  }

  it should "throw assertion error if block height is too big" in {
    val tooBigHeight: Int = blocksPerTable * 100 + 1
    intercept[AssertionError](
      blockHeightToTablePartitionId(tooBigHeight)
    ).getMessage shouldEqual "assertion failed: " + blockHeightTooBigMsg(tooBigHeight)
  }
}
