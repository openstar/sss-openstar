package sss.openstar.message

import akka.testkit.{ImplicitSender, TestKit}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import sss.ancillary.ShortSessionKey
import sss.openstar._
import sss.openstar.identityledger.BackwardsCompatibilityOps.PublicKeyOps

class MessageDownloadActorSpec extends TestKit(TestUtils.actorSystem)
  with AnyWordSpecLike with Matchers with ImplicitSender {

  private val chainId = 1.toByte

  private object TestSystem extends TestSystem {

    val key = createPrivateAc()

    identityService.claim(nodeIdentity.publicKey.toDetails(nodeId))

  }

  private val sessKey: ShortSessionKey = ShortSessionKey()

  def validateBounty(amount: Long, from: UniqueNodeIdentifier): Boolean = amount > 0

  /*val sut = MessageDownloadActor(
    TestSystem.nodeIdentity.id,

    sessKey
  )

  messageEventBus.subscribe(classOf[InternalLedgerItem])

  "MessageDownloadActor" should {

    "react to a PagedMessage" in {

      val g = Guid()

      val index = TxIndex(TestSystem.seedBytes.apply(TxIndexLen), 9)

      val enc = MessageEcryption.encryptWithEmbeddedSecret(90,
        index,
        TestSystem.nodeIdentity,
        TestSystem.nodeIdentity.publicKey,
        "Some message",
        TestSystem.seedBytes.secureSeed(16)
      )

      val payLoad = MessagePayloadDecoder.toPayload(enc)

      val msg = Message(payLoad.get, g)
      sut ! IncomingMessage(TestSystem.globalChainId, MessageKeys.PagedMessageMsg, "provider", PagedMessage(sessKey, 0, msg))

      expectMsgType[InternalLedgerItem]

    }
  }*/
}