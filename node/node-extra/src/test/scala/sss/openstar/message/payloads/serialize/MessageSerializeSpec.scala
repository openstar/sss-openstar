package sss.openstar.message.payloads.serialize

import akka.util.ByteString
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.ancillary.{ByteArrayComparisonOps, Guid, ShortSessionKey}
import sss.openstar.DummySeedBytes
import sss.openstar.balanceledger.TxIndex
import sss.openstar.crypto.CBCEncryption
import sss.openstar.ledger.{LedgerItem, TxIdLen}
import sss.openstar.message._
import sss.openstar.message.payloads.MessageEcryption.{EmbeddedBounty, EncryptedMessage, EncryptedSessionKey}
import sss.openstar.message.payloads.{CompletedNewContactMessage, NewContactMessage, PaywalledMessage}
import sss.openstar.util.GuidFromByteString
/**
  * Created by alan on 2/15/16.
  */

class MessageSerializeSpec extends AnyFlatSpec with Matchers with ByteArrayComparisonOps {

  import sss.openstar.TestUtils.toSig

  def encryptedSessionKey(s:String) = EncryptedSessionKey(s, ByteString(DummySeedBytes(16)), ByteString(DummySeedBytes(16)))

  "A EncryptedSessionKey" should "serialize and de" in {
    val sut = encryptedSessionKey("asdasdasd")
    val rehydrated = EncryptedSessionKeySerializer.fromBytes(EncryptedSessionKeySerializer.toBytes(sut))
    assert(sut == rehydrated)
  }

  "A EncryptedMessage" should "serialize and de" in {
    val sut = EncryptedMessage(
      Some(EmbeddedBounty(23, 99, TxIndex(DummySeedBytes(TxIdLen), 9))),
      "asdasdasd",
      Seq(encryptedSessionKey("sdfsdfs")),
      ByteString(DummySeedBytes(45)),
      CBCEncryption.newInitVector(DummySeedBytes),
      GuidFromByteString(),
      Seq.fill(4)(GuidFromByteString())
    )

    val rehydrated = MessageEncryptionSerializer.fromBytes(MessageEncryptionSerializer.toBytes(sut))
    assert(sut == rehydrated)
  }

  "A PaywalledMessageSerializer" should " ser and deser" in {
    val index = TxIndex(DummySeedBytes(TxIdLen), 99)
    val payload = MessagePayload(3.toByte, DummySeedBytes(25))
    val sut = PaywalledMessage(Some("from"), Some(index), payload)
    val dehrdrated = PaywalledMessageSerializer.toBytes(sut)
    val reHydrated = PaywalledMessageSerializer.fromBytes(dehrdrated)
    assert(sut == reHydrated)
  }

  "A NewContactMessageSerializer" should " ser and deser" in {

    val sut = NewContactMessage("asdad", "adada", Some(ByteString(DummySeedBytes(25))), "asdfafsdf",  DummySeedBytes(25))
    val dehrdrated = NewContactSerializer.toBytes(sut)
    val reHydrated = NewContactSerializer.fromBytes(dehrdrated)
    assert(sut == reHydrated)
  }

  "A CompletedNewContactMessageSerializer" should " ser and deser" in {

    val sut = CompletedNewContactMessage(
      "asdad",
      Some(ByteString(DummySeedBytes(25))),
      "adada",
      "asdfafsdf",
      "drhdhdgfh",
      Some(ByteString(DummySeedBytes(25)))
    )

    val dehrdrated = CompletedNewContactSerializer.toBytes(sut)
    val reHydrated = CompletedNewContactSerializer.fromBytes(dehrdrated)
    assert(sut == reHydrated)
  }


  val le = LedgerItem(3.toByte, DummySeedBytes(32), DummySeedBytes(32))

  "A message payload " should " serialize and deserialize " in {
    val test = MessagePayload(2.toByte, DummySeedBytes(32))
    val asBytes = test.toBytes
    val hydrated = asBytes.toMessagePayload
    assert(hydrated.payloadType == test.payloadType)
    assert(hydrated.payload isSame test.payload)
    assert(hydrated.hashCode == test.hashCode)
    assert(hydrated == test)
  }

  "A Message Query " should " serialize and deserialize " in {

    val test = MessageQuery("from", 4444L, 567, ShortSessionKey())
    val asBytes = test.toBytes
    val hydrated = asBytes.toMessageQuery

    assert(hydrated.pageSize == test.pageSize)
    assert(hydrated.hashCode == test.hashCode)
    assert(hydrated == test)
  }

  "A Success Message response " should " serialize and deserialize " in {
    val test: MessageResponse = SuccessResponse(DummySeedBytes(32))
    val asBytes = test.toBytes
    val hydrated = asBytes.toMessageResponse
    assert(hydrated.success == test.success)
    assert(hydrated.txIdOptStr == test.txIdOptStr)
    assert(hydrated.hashCode == test.hashCode)
    assert(hydrated == test)
  }

  "A Failure Message response " should " serialize and deserialize " in {
    val test: MessageResponse = FailureResponse(DummySeedBytes(3), "What the hell happened here?")
    val asBytes = test.toBytes
    val hydrated = asBytes.toMessageResponse
    assert(hydrated.success == test.success)
    assert(hydrated.txIdOptStr == test.txIdOptStr)
    assert(hydrated.hashCode == test.hashCode)
    assert(hydrated == test)
  }

  "A Message " should " serialize and deserialize " in {
    val test= Message(MessagePayload(4.toByte, DummySeedBytes(34)), Guid(), Some(Guid()))
    val asBytes = test.toBytes
    val hydrated = asBytes.toMessage
    assert(hydrated.hashCode == test.hashCode)
    assert(hydrated == test)
  }



}
