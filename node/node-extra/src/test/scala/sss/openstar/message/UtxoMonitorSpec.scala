package sss.openstar.message


import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.openstar.DummySeedBytes
import sss.openstar.balanceledger.TxIndex
import sss.openstar.db.DbWithMigrationFixture
import sss.openstar.ledger.TxIdLen

import scala.language.postfixOps
import scala.util.Random

class UtxoMonitorSpec extends AnyFlatSpec with Matchers with DbWithMigrationFixture {

  val sut = new UtxoMonitor(100)

  private def makeIndex() = TxIndex(DummySeedBytes(TxIdLen), Random.nextInt())
  def txIndexStream: LazyList[TxIndex] = LazyList.continually(makeIndex())

  val index = TxIndex(DummySeedBytes(TxIdLen), 9)
  val index2 = TxIndex(DummySeedBytes(TxIdLen), 3)
  val index3 = TxIndex(DummySeedBytes(TxIdLen), 4)


  "UtxoMonitor" should "support mutiple inserts" in {
    val a = sut.watch(index)
    assert(a.isSuccess)
    assert(sut.watch(index2).isSuccess)
  }

  it should "support find" in {
    assert(
      sut.query(index) == Some(UnConsumedMonitoredUtxo(index))
    )
    assert(
      sut.query(index2) == Some(UnConsumedMonitoredUtxo(index2))
    )
    assert(sut.query(index3).isEmpty)

  }

  it should "support marking consumed" in {
    assert(sut.markConsumed(index, 78).isSuccess)

    val ConsumedMonitoredUtxo(`index`, 78, _) = sut.query(index).get

    assert(sut.markConsumed(index2, 79).isSuccess)
    val ConsumedMonitoredUtxo(`index2`, 79, _) = sut.query(index2).get

  }

  it should "support delete" in {
    val c = sut.count
    assert(sut.unwatch(index).isSuccess)
    assert(sut.count + 1 == c, "Delete should remove a row")
  }

  it should "support prune" in {
    val sut = new UtxoMonitor(10)

    println (s"Total count ${sut.count} start")
    val some = txIndexStream.take(10).toList
    val more = txIndexStream.take(5).toList
    some.foreach(sut.watch)
    more.foreach(sut.watch)
    some.foreach(sut.markConsumed(_, 7))
    println (s"Total count ${sut.count} pre prune")
    val deleted = sut.prune()

    println (s"Total deleted ${deleted}")

    more.foreach { shouldBeThere =>
      assert(sut.query(shouldBeThere).isDefined)
    }

    println (s"Total count ${sut.count} after all")

    val deleted2 = sut.prune()
    assert(deleted2.get == 0)
    some.foreach(sut.watch)
    println (s"Total count ${sut.count} after add more")
  }
}
