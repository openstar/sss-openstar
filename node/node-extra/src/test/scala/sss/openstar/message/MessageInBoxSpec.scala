package sss.openstar.message

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.ancillary.FutureOps.AwaitResult
import sss.openstar.TestSystem
import sss.openstar.account.{AccountKeysFactoryFixture, NodeIdTag, NodeVerifier}
import sss.openstar.balanceledger.{TxIndex, TxIndexLen}
import sss.openstar.db.MigrationFixture
import sss.openstar.message.MessageInBox.{MessagePage, MessageQueryType, SavedMessage}
import sss.openstar.message.payloads.MessageEcryption
import sss.openstar.message.payloads.MessageEcryption.{AttachmentDetails, MessageAttachments}
import sss.openstar.util.GuidFromByteString

import scala.language.postfixOps

class MessageInBoxSpec extends AnyFlatSpec with Matchers {

  object TestSystem extends TestSystem with MigrationFixture {
    migrateSqlSchema()
    val sut = MessageInBox(nodeIdTag.nodeId)
  }

  import TestSystem.cpuOnlyEc

  val index = TxIndex(TestSystem.seedBytes.apply(TxIndexLen), 9)

  val files = (0 to 2) map (i => AttachmentDetails(s"name$i", s"fType$i", 99))
  val attachments = Some(MessageAttachments("notused", files))

  val enc = MessageEcryption.encryptWithEmbeddedSecret(90, 99,
    index,
    TestSystem.nodeIdentity,
    Seq(TestSystem.nodeIdentity.defaultNodeVerifier),
    "Some message",
    attachments,
    TestSystem.seedBytes.secureSeed(16),
    GuidFromByteString(),
    Seq.empty
  ).await()


  val payLoad = MessagePayloadDecoder.toPayloadOpt(enc).get

  import TestSystem.sut

  val g1 = GuidFromByteString()

  "MessageInBox" should "support insert" in {

    val m = sut.addNew(Message(payLoad, g1))

  }

  it should "support create/retrieve from InBox" in {
    val pager = sut.inBoxPager(10)
    assert(pager.messages.size == 1)
    assert(g1 == pager.messages.head.msg.guid)
  }

  it should "support delete from InBox" in {
    val pager = sut.inBoxPager(10)
    sut.delete(pager.messages.head)

    val pager2 = sut.inBoxPager(10)
    assert(pager2.messages.isEmpty)
  }

  it should "support 'new' message" in {
    val g2 = GuidFromByteString()
    val m = sut.addNew(Message(payLoad, g2))
    val pager = sut.inBoxPager(10)

    val found: Seq[SavedMessage] = pager.messages

    val retrieved = found.find { sm =>
      sm.msg.guid == g2
    }

    assert(retrieved.isDefined)

    sut.deleteSent(retrieved.get)

    val pager2 = sut.inBoxPager(10)
    val notRetrieved = pager2.messages.find { sm =>
      sm.msg.guid == g2
    }
    assert(notRetrieved.isEmpty)
  }

  it should "support 'archived' messages" in {
    val g3 = GuidFromByteString()
    val m = sut.addNew(Message(payLoad, g3))
    m.map(sut.archive)
    val pager = sut.archivedPager(100)
    assert(pager.messages.size == 1)
    assert(g3 == pager.messages.head.msg.guid)

    val inPager = sut.inBoxPager(99)
    assert(inPager.messages.isEmpty)

    m.map(sut.delete)
    val pager2 = sut.archivedPager(100)
    assert(pager2.messages.isEmpty)

  }

  it should "support 'junk' messages" in {
    val g3 = GuidFromByteString()
    val m = sut.addJunk(Message(payLoad, g3))
    m.map(sut.archive)
    val pager = sut.archivedPager(100)
    assert(pager.messages.size == 1)
    assert(g3 == pager.messages.head.msg.guid)

    val inPager = sut.inBoxPager(99)

    assert(inPager.messages.isEmpty)
  }

  it should "support many new messages" in {

    1 to 100 foreach { _ =>
      val g = GuidFromByteString()
      val m = Message(payLoad, g)
      sut.addNew(m)

    }

    val pager = sut.inBoxPager(10)

    var count = 0

    def countAll(acc: Int, p: MessagePage[_]): Int = {
      count += 1

      val total = acc + p.messages.size
      if (p.hasPrev) countAll(total, p.prev)
      else total
    }

    assert(countAll(0, pager) === 100)
  }

  it should "support paging message queries" in {

    1 to 10 foreach { _ =>
      val g = GuidFromByteString()
      val m = Message(payLoad, g)
      sut.addNew(m)
      Thread.sleep(10)
    }

    {
      val all = sut.page(0, 10, MessageQueryType.NewestFirst)

      all.tail.foldLeft(all.head)((acc, e) => {
        assert(acc.savedAt.isAfter(e.savedAt))
        assert(acc.msg.parentGuid.isEmpty)
        assert(acc.owner == sut.owner)
        e
      })
    }
    {
      val all = sut.page(0, 10, MessageQueryType.DefaultOrder)

      all.tail.foldLeft(all.head)((acc, e) => {
        assert(acc.msg.parentGuid.isEmpty)
        assert(acc.owner == sut.owner)
        e
      })
    }
  }

}
