package sss.openstar.message.payloads

import org.scalatest.concurrent.ScalaFutures
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.ancillary.FutureOps.AwaitResult
import sss.openstar.account._
import sss.openstar.balanceledger.TxIndex
import sss.openstar.common.builders.{ConfigBuilder, CpuBoundExecutionContextFixture, PhraseFixture}
import sss.openstar.message.payloads.MessageEcryption.{AttachmentDetails, MessageAttachments}
import sss.openstar.util.GuidFromByteString

import java.util



class MessageEncryptionSpec extends AnyFlatSpec
  with Matchers
  with ScalaFutures
  with NodeIdentityManagerFixture
  with AccountKeysFactoryFixture
  with PhraseFixture
  with CpuBoundExecutionContextFixture
  with ConfigBuilder {

  clearNodeIdentityFixture()

  val tag1 = NodeIdTag("one")
  val tag2 = NodeIdTag("two")
  val tag3 = NodeIdTag("three")

  val lookupTestData = addNodeIdentityToFixture(tag1, tag2, tag3)

  val nId1: NodeIdentity = getNodeIdentity(tag1.nodeId, tag1.tag)
  val nId2: NodeIdentity = getNodeIdentity(tag2.nodeId, tag2.tag)
  val nId3: NodeIdentity = getNodeIdentity(tag3.nodeId, tag3.tag)


  "MessageEncryption" should "encrypt and decrypt a message with secret" in {

    val text = "Hello there monkeys!"
    val secret = Array.fill(16)(5.toByte)

    val files = (0 to 2) map (i => AttachmentDetails(s"name$i", s"fType$i", 99))

    val attachments = Option(MessageAttachments("unused", files))

    val fakeTxId = Array.fill(32)(4.toByte)

    val encryptedMessage = MessageEcryption
      .encryptWithEmbeddedSecret(
        2L,
        99L,
        TxIndex(fakeTxId, 0),
        nId1,
        Seq(nId2.defaultNodeVerifier, nId3.defaultNodeVerifier),
        text,
        attachments,
        secret,
        GuidFromByteString(),
        Seq.empty).await()

    val lookup: NodeIdTag => NodeVerifier = nIdTag =>
      getNodeIdentity(nIdTag.nodeId, nIdTag.tag).defaultNodeVerifier

    val textWithSecret = encryptedMessage.decrypt(nId2, lookup).await()
    val textWithSecret2 = encryptedMessage.decrypt(nId1, lookup).await()
    val textWithSecret3 = encryptedMessage.decrypt(nId1, lookup).await()

    assert(textWithSecret2.text == text)
    assert(util.Arrays.equals(textWithSecret2.secret, secret))
    assert(textWithSecret2.attachmentsOpt == attachments, "File compare fail")

    assert(textWithSecret3.text == text)
    assert(util.Arrays.equals(textWithSecret3.secret, secret))
    assert(textWithSecret3.attachmentsOpt == attachments, "File compare fail")

    assert(textWithSecret.text == text)
    assert(util.Arrays.equals(textWithSecret.secret, secret))
    assert(textWithSecret.attachmentsOpt == attachments, "File compare fail")

    assert(encryptedMessage.watch == Seq(TxIndex(fakeTxId, 0), TxIndex(fakeTxId, 1)))


  }

}
