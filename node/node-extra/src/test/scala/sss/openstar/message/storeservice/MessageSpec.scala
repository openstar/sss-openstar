package sss.openstar.message.storeservice

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.ancillary.ByteArrayComparisonOps
import sss.openstar.DummySeedBytes
import sss.openstar.db.DbWithMigrationFixture
import sss.openstar.message.{Message, MessagePayload}
import sss.openstar.util.GuidFromByteString
import sss.openstar.util.UniqueIncreasing.UniqueIncreasing
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

/**
  * Created by alan on 2/15/16.
  */
class MessageSpec extends AnyFlatSpec with Matchers with DbWithMigrationFixture with ByteArrayComparisonOps {

  val mp = new MessagePersist()

  val rndBytes = DummySeedBytes(99)
  val rndPayload = MessagePayload(100.toByte, rndBytes)
  val when =  LocalDateTime.now().minus(1, ChronoUnit.SECONDS);
  val identity = "karl"
  val lenny = "lenny"

  val tx = DummySeedBytes(12)
  val from = "from"

  "A message " should " be persistable " in {

    //val m1 = Message(identity, rndBytes, when.toDate.getTime)

    val g = GuidFromByteString()
    val msg = mp.pending(identity, g, None, rndPayload)
    mp.accept(identity, g)

  }

  it should " be retrievable " in {
    val retrieved = mp.page(0, identity, 100)
    assert(retrieved.size == 1)
    assert(retrieved.head._1.msgPayload == rndPayload)
  }


  "Messages " should " be retrievable in order " in {

    val msgs = (0 until 100) map (i => (MessagePayload(i.toByte, DummySeedBytes(i)), GuidFromByteString()))
    msgs.map { m => mp.pending(lenny, m._2, None, m._1) }.foreach(m => mp.accept(lenny, m.guid))

    def more(u:Long): Seq[(Message, UniqueIncreasing)] = {
      mp.page(u, lenny, 10)
    }

    def all(acc: Seq[Seq[(Message, UniqueIncreasing)]], count: Int): Seq[Seq[(Message, UniqueIncreasing)]] = {
      if(count > 0) {
        all( acc :+ more(acc.last.last._2), count - 1)
      } else acc
    }

    val tenPages = all(Seq(mp.page(0, lenny, 10)), 9)

    assert(tenPages.size == 10)
    val zipped = tenPages.flatten.zip(msgs)
      zipped.forall {
        case ((msg, unique), (msgPayload, guid)) =>
          msg.msgPayload == msgPayload &&
            msg.guid == guid
      }


  }

  they should " be deletable " in {

    val forDeletion = mp.page(0, lenny, 10)
    forDeletion.foreach(m => mp.delete(lenny, m._1.guid))
    val remaining = mp.page(0, lenny, 100)
    assert(remaining.size == 90)
    val indices = forDeletion.map(_._1.guid)
    assert(remaining.forall(m => !indices.contains(m._1.guid)))

  }
}
