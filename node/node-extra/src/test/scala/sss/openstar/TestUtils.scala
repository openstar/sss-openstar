package sss.openstar

import akka.actor.ActorSystem
import com.typesafe.config.Config
import scorex.crypto.signatures.{PublicKey, Signature}
import sss.ancillary.{ArgumentParser, Configure, Logging}
import sss.db.Db
import sss.db.ops.DbOps.DbRunOps
import sss.openstar.account.{AccountKeysFactoryFixture, NodeIdentity, NodeIdentityManagerFixture}
import sss.openstar.balanceledger.CoinbaseBalanceLedger
import sss.openstar.block.BlockChain
import sss.openstar.chains.Chains.GlobalChainIdBuilder
import sss.openstar.common.block.BlockTx
import sss.openstar.common.builders._
import sss.openstar.controller.Send
import sss.openstar.crypto.keypairs.DefaultAccountKeysFactoryBuilder
import sss.openstar.identityledger.Claim0
import sss.openstar.ledger.{LedgerItem, SignedTxEntry}
import sss.openstar.network.TestMessageEventBusOps._
import sss.openstar.network.{IncomingSerializedMessage, NetSend, SerializedMessage}
import sss.openstar.nodebuilder._
import sss.openstar.schemamigration.RequireTablePartitionsCount

import java.util.concurrent.atomic.AtomicReference
import scala.language.implicitConversions
import scala.util.{Success, Try}

trait TestSystem extends MessageEventBusBuilder
  with ActorSystemBuilder
  with RootActorContextBuilder
  with DecoderBuilder
  with AmountBuilderBuilder
  with WalletBuilder
  with SendTxBuilder
  with StarzBalanceLedgerBuilder
  with CardanoBalanceLedgerBuilder
  with OracleLedgerOwnerLedgerBuilder
  with BlockChainBuilder
  with IdentityServiceBuilder
  with DefaultAccountKeysFactoryBuilder
  with CpuBoundExecutionContextFromActorSystemBuilder
  with NodeConfigBuilder
  with NetSendFixture
  with NodeIdentityBuilder
  with NodeIdentityManagerFixture
  with SignerFactoryBuilder
  with KeyPersisterBuilder
  with GlobalChainIdBuilder
  with GlobalTableNameTagBuilder
  with SeedBytesBuilder
  with DbBuilder
  with IOExecutionContextBuilder
  with RequireConfig
  with Configure
  with Logging
  with PhraseFixture
  with BlockFactoriesAndMerklePersisterBuilder
  with RequireTablePartitionsCount
  with AccountKeysFactoryFixture {


  override protected lazy val tablePartitionsCount: Int = 10
  override val blocksPerTable: Int = 10

  lazy override val seedBytes = DummySeedBytes

  override lazy val actorSystem: ActorSystem = TestUtils.actorSystem

  final val nodeId: UniqueNodeIdentifier = "testSystem1"
  lazy override val prefixedConf: Config = config(nodeId)

  addNodeIdentityToFixture(nodeIdTag)

}

object TestUtils extends AccountKeysFactoryFixture {

  implicit def toSig(bs: Array[Byte]): Signature = Signature(bs)
  implicit def toPkey(bs: Array[Byte]): PublicKey = PublicKey(bs)

  implicit val actorSystem: ActorSystem = ActorSystem("OpenstarTests")

  def extractAsType[T: Manifest](a:Any): T = {
    a match {
      case in: TestIncoming =>
        in.msg match {
          case t: T =>
            t
          case _ => throw new IllegalArgumentException("Could not get type out")
        }
      case t: T =>
        t
      case _ =>
        throw new IllegalArgumentException("Could not get type out")
    }
  }

  def addEmptyBlock(balanceLedger: CoinbaseBalanceLedger[_],
                  nodeIdentity: NodeIdentity,
                  bc: BlockChain)(implicit db:Db): Unit = {

    (for {
      bHeader <- bc.lastCommittedBlockHeader
      b <- bc.block(bHeader.height + 1)
      _ <- bc.closeBlockFTx(bHeader)
    } yield ()).dbRunSyncGet

  }

  def addOneBlock(balanceLedger: CoinbaseBalanceLedger[_],
                  nodeIdentity: NodeIdentity,
                  bc: BlockChain, size: Int = 10)(implicit db:Db): Unit = {

    val (b, bHeader) = (for {
      bHeader <- bc.lastCommittedBlockHeader
      b <- bc.block(bHeader.height + 1)
    } yield (b,bHeader)).dbRunSyncGet

    def writeBlock(height: Long, size: Int): Unit = {

      def gen(size: Int): Unit = {

        def createRandomClaimString() = s"${System.currentTimeMillis()}"

        val key = PublicKey(DummySeedBytes.randomSeed(32))
        val claim = Claim0("ownerIdentity", createRandomClaimString() , key)
        val ste = SignedTxEntry(claim.toBytes)
        val le = LedgerItem(MessageKeys.IdentityLedger, claim.txId, ste.toBytes)

        val index = b.getNextIndex
        val bTx = BlockTx(index + 1, le)
        b.commit(bTx).dbRunSyncGet


        if(size != 0) gen(size - 1)

      }

      gen(size - 1)
      bc.closeBlock(bHeader)

    }

    writeBlock(b.height, size)
  }
}

trait BaseTestSystem extends AppLayer
  with SeedBytesBuilder
  with RequireArgumentParser
  with DecoderBuilder
  with PhraseFixture {

  lazy override val seedBytes = DummySeedBytes
  val args: ArgumentParser = new ArgumentParser(Array.empty)

}

trait TestSystem1 extends BaseTestSystem
  with StarzBalanceLedgerBuilder
  with AmountBuilderBuilder {

  lazy override implicit val actorSystem: ActorSystem = ActorSystem("OpenstarTests1")

  val nodeId: UniqueNodeIdentifier = "testSystem1"
  override lazy val configName: String = nodeId

  override def shutdown(): Unit = ()

  def sendF: NetSend = (msg, node) => {
    msg match {

      case SerializedMessage(chainId, msgCode, data) =>
        testSystem2.messageEventBus.simulateNetworkMessage(IncomingSerializedMessage(nodeId,
          SerializedMessage(chainId, msgCode, data)))

    }
  }

  lazy implicit override val send: Send = Send(sendF, makeToSerializedDecoder)

  val testSystem2: BaseTestSystem
}

trait TestSystem2 extends BaseTestSystem {
  lazy override implicit val actorSystem: ActorSystem = ActorSystem("OpenstarTests2")

  val nodeId: UniqueNodeIdentifier = "testSystem2"
  override lazy val configName: String = nodeId

  def sendF: NetSend = (msg, node) => {
    msg match {

      case SerializedMessage(chainId, msgCode, data) =>
        testSystem1.messageEventBus.simulateNetworkMessage(IncomingSerializedMessage(nodeId,
          SerializedMessage(chainId, msgCode, data)))

    }
  }

  lazy implicit override val send: Send = Send(sendF, makeToSerializedDecoder)

  val testSystem1: BaseTestSystem


}

