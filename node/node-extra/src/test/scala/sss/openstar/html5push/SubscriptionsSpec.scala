package sss.openstar.html5push

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.openstar.db.DbWithMigrationFixture
import sss.openstar.html5push.Subscriptions.Subscription

class SubscriptionsSpec extends AnyFlatSpec with Matchers with DbWithMigrationFixture {

  val sut = new Subscriptions()
  val who1 = "sfsdfsdfsdfsdfsdf"
  val who2 = "sfsdfsdfsdfsdfsdfadsasdasdasdasd"
  val sub = Subscription("sdfsdfsdfsdfsdfsdfsdfsdfsdfsdf", "asdasdasdasdasdasd", "sdfsdfsdfsdfsdf")

  "A subscription" should "be persisted and retrieved correctly" in {

    assert(sut.persist(who1, None).isSuccess)
    assert(sut.retrieve(who1).isEmpty)
    assert(sut.retrieve(who2).isEmpty)

    assert(sut.persist(who1, Some(sub)).isSuccess)
    val fromDb = sut.retrieve(who1)
    assert(fromDb === Some(sub))


    assert(sut.retrieve(who2).isEmpty)
    assert(sut.persist(who2, Some(sub)).isSuccess)
    val fromDb2 = sut.retrieve(who2)
    assert(fromDb2 === Some(sub))


    assert(sut.persist(who2, None).isSuccess)
    assert(sut.retrieve(who2).isEmpty)
    assert(sut.retrieve(who1).contains(sub))
    assert(sut.persist(who1, None).isSuccess)
    assert(sut.retrieve(who1).isEmpty)
  }

}
