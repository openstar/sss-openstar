package sss.openstar.wallet

import org.scalatest.concurrent.{Futures, ScalaFutures}
import org.scalatest.concurrent.ScalaFutures.convertScalaFuture
import org.scalatest.flatspec.{AnyFlatSpec, AsyncFlatSpec}
import org.scalatest.time.{Millis, Minutes, Seconds, Span}
import sss.ancillary.ByteArrayComparisonOps
import sss.openstar.account.{AccountKeysFactoryFixture, KeyPersisterFixture, NodeIdentityManager, NodeIdentityManagerFixture, NodeVerifier}
import sss.openstar.balanceledger._
import sss.openstar.chains.TxWriterActor.InternalTxResult
import sss.openstar.common.builders.{ConfigBuilder, CpuBoundExecutionContextBuilder, CpuBoundExecutionContextFixture, IOExecutionContextBuilder, KeyPersisterBuilder, NodeIdTagBuilder, PhraseFixture, RequirePhrase, SignerFactoryBuilder}
import sss.openstar.contract.SaleOrReturnSecretEnc
import sss.openstar.db.DbWithMigrationFixture
import sss.openstar.eventbus.EventPublish
import sss.openstar.identityledger.IdentityService
import sss.openstar.ledger.LedgerId
import sss.openstar.tools.SendTxSupport
import sss.openstar.util.{Amount, AmountBuilder}
import sss.openstar.wallet.WalletPersistence.Lodgement
import sss.openstar.{BusEvent, Currency, DummySeedBytes}

import java.util.UUID
import scala.concurrent.Future
import scala.reflect.ClassTag
import scala.util.Try

/**
  * Created by alan on 2/15/16.
  */



class WalletSpec extends AnyFlatSpec
  with DbWithMigrationFixture
  with ByteArrayComparisonOps
  with NodeIdentityManagerFixture
  with AccountKeysFactoryFixture
  with NodeIdTagBuilder
  with ConfigBuilder
  with PhraseFixture
  with SignerFactoryBuilder
  with KeyPersisterFixture
  with IOExecutionContextBuilder
  with CpuBoundExecutionContextFixture
  with ScalaFutures {

  implicit override val patienceConfig: PatienceConfig =
    PatienceConfig(timeout = scaled(Span(1, Minutes)), interval = scaled(Span(20, Millis)))


  val id = UUID.randomUUID().toString.substring(0, 8)
  val otherId = UUID.randomUUID().toString.substring(0, 8)

  //val pKey = PublicKeyAccount(DummySeedBytes(32))
  //val otherPKey = PublicKeyAccount(DummySeedBytes(32))
  val wp = new WalletPersistence(id, db)
  val identityService = IdentityService()
  lazy val nId = addToFixtureAndGetNodeIdentity(id, "defaultTag", "Phrase12$")
  lazy val otherNodeId = addToFixtureAndGetNodeIdentity(otherId, "defaultTag", "Phrase12$")


  implicit val sendTx: SendTxSupport.SendTx = (_, _, _, _) => Future.failed[InternalTxResult](new Exception(""))
  implicit val events: EventPublish = new EventPublish {
    override def publish[T <: BusEvent : ClassTag](event: T): Unit = ()
  }

  trait TestCurrency extends Currency
  case class TestCurrencyAmount(value: Long) extends Amount {
    override type C = TestCurrency

    override def newAmount(value: Long): Amount = TestCurrencyAmount(value)
  }
  implicit val cb: AmountBuilder[TestCurrency] = (value: Long) => TestCurrencyAmount(value)

  val clientWallet: ClientWallet = new ClientWallet(nId.id)
  val wallet = new Wallet[TestCurrency](nId.id, LedgerId(0.byteValue()), _ => false, identityService, wp, clientWallet, () => 0)
  val otherLedger = LedgerId(1.byteValue())
  val walletLedger2 = new Wallet[TestCurrency](nId.id, otherLedger, _ => false, identityService, wp, clientWallet, () => 0)

  val txId0 = DummySeedBytes(32)
  val txId1 = DummySeedBytes(32)
  val txId2 = DummySeedBytes(32)
  val txId3 = DummySeedBytes(32)
  val txId4 = DummySeedBytes(32)
  val txId5 = DummySeedBytes(32)

  val txId0OtherWallet = DummySeedBytes(32)
  val txIndexOtherWallet = TxIndex(txId0OtherWallet, 0)

  val txIndex0 = TxIndex(txId0, 0)
  val txIndex1 = TxIndex(txId1, 0)
  val txIndex2 = TxIndex(txId2, 0)
  val txIndex3 = TxIndex(txId3, 0)
  val txIndex4 = TxIndex(txId4, 0)
  val txIndex5 = TxIndex(txId5, 0)

  val funds3000 = Map(txIndex0 -> TxOutput(1000, wallet.encumberToIdentity(someIdentity = nId.id)),
    txIndex1 -> TxOutput(2000, wallet.encumberToIdentity(someIdentity = nId.id)))

  val funds10Other = Map(txIndexOtherWallet -> TxOutput(10, walletLedger2.encumberToIdentity(someIdentity = nId.id)))

  val funds10 = Map(txIndex2 -> TxOutput(10, wallet.encumberToIdentity(someIdentity = nId.id)))

  val funds10_4 = Map(txIndex4 -> TxOutput(10, wallet.encumberToIdentity(someIdentity = nId.id)))

  val funds20_5 = Map(txIndex5 -> TxOutput(20, wallet.encumberToTrustee(nId.id)))

  val fundsClient10 = Map(txIndex3 -> TxOutput(10, wallet.encumberToTrustee(nId.id)))

  def lodgementSaleOrReturn(owner: String) = txIndex2 -> TxOutput(10, SaleOrReturnSecretEnc(owner, "", None, 1))

  "A Wallet " should " create a good payment " in {
    assert(wallet.balance(0).get.value == 0)
    //TestBalanceLedgerQuery.mocks = funds3000
    wallet.credit(Lodgement(txIndex0, funds3000(txIndex0), 0))
    wallet.credit(Lodgement(txIndex1, funds3000(txIndex1), 0))
    assert(wallet.balance(0).get.value == 3000)
  }

  it should "create multiple usable txs" in {
    val amount = wallet.amount(1000)
    val tx1Opt = wallet.createTx(amount, 1).toOption

    assert(tx1Opt.isDefined, "Couldn't create a tx for 1000?")
    val tx1 = tx1Opt.get
    tx1.ins.foreach(println)
    tx1.outs.foreach(println)
    val amount2 = wallet.amount(1000)
    val tx2 = wallet.createTx(amount2, 1).get

    tx2.ins.foreach(println)
    tx2.outs.foreach(println)

    assert(tx1.ins.forall(in => !tx2.ins.contains(in)))

  }

  it should "credit SaleOrReturn Lodgements" in {
    val (goodIndex, goodout) = lodgementSaleOrReturn(nId.id)
    assert(wallet.apply(goodIndex, goodout).isDefined)

    {
      val (goodIndex, goodout) = lodgementSaleOrReturn("NOTME")
      assert(wallet.apply(goodIndex, goodout).isEmpty)
    }

  }

  it should "not allow client money to be spent" in {
    val bal1: Long = wallet.balance().get.value
    wallet(txIndex3, fundsClient10(txIndex3))
    assert(bal1 + 10 == wallet.balance().get.value)
    val amount10 = wallet.amount(10)
    val tx1OTry = wallet.createTx(amount10, 1)
    assert(tx1OTry.isFailure)
    wallet(txIndex4, funds10_4(txIndex4))
    val bal2 = wallet.balance().get.value
    assert(bal2 == bal1 + 20)
    var tx1OTryGood: Try[Tx] = wallet.createTx(amount10, 1)
    assert(tx1OTryGood.isSuccess)
    wallet.confirmSpent(txIndex4)
    val bal3 = wallet.balance().get.value
    val amount3 = wallet.amount(3)
    tx1OTryGood = wallet.createTx(amount3, 1)
    assert(tx1OTryGood.isFailure)

  }

  it should "list clients correctly" in {

    (0 to 10) map { i =>
      wallet.clientWallet.lodge(i + 1,s"randomuser$i")
    }
    val all = wallet.clientBalances(0, 100)

    assert(all.size >= 10, "Can't test with no entries")

    all.foreach { clientBal =>
      wallet.clientWallet.withdrawAll(clientBal.client)
    }
    val none = wallet.clientBalances(0, 100)
    assert(none.isEmpty, "All wallets should be drained")
    all.foreach { clientBal =>
      wallet.clientWallet.lodge(clientBal.balance.value, clientBal.client)
    }
    val replaced = wallet.clientBalances(0, 100)
    assert(replaced == all)
  }

  it should "respect the client balances paging params" in {
    val all = wallet.clientBalances(100, 10)
    assert(all.isEmpty)
  }

  it should "prevent the same wallet entry appearing twice in client wallet" in {

    val bal1: Long = wallet.balance().get.value
    val clientTotal1 = clientWallet.total
    val clientBal1 = clientWallet.balance(id)

    val optLodge = wallet(txIndex5, funds20_5(txIndex5))

    val bal2: Long = wallet.balance().get.value
    val clientTotal2 = clientWallet.total
    val clientBal2 = clientWallet.balance(id)

    val optLodge2 = wallet(txIndex5, funds20_5(txIndex5))

    val bal3: Long = wallet.balance().get.value
    val clientTotal3 = clientWallet.total
    val clientBal3 = clientWallet.balance(id)

    assert(bal3 == bal2, "Same tx lodged twice")
    assert(clientBal3 == clientBal2, "Same client money tx lodged twice")

  }


  "A Second Wallet" should "create a good payment and not interfere with other wallets" in {
    val wallet1Balance = wallet.balance(0)
    assert(walletLedger2.balance(0).get.value == 0)
    //TestBalanceLedgerQuery.mocks = funds3000
    walletLedger2.credit(Lodgement(txIndexOtherWallet, funds10Other(txIndexOtherWallet), 0))
    assert(walletLedger2.balance(0).get.value == 10)
    assert(wallet.balance(0) == wallet1Balance, "Lodgement to other wallet changed the balance of the first wallet?")
  }
}
