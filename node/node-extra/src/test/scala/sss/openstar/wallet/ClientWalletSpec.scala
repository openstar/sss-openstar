package sss.openstar.wallet

import java.util.UUID
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.ancillary.ByteArrayComparisonOps
import sss.db.Db
import sss.openstar.db.DbWithMigrationFixture

/**
  * Created by alan on 2/15/16.
  */



class ClientWalletSpec extends AnyFlatSpec with DbWithMigrationFixture with Matchers with ByteArrayComparisonOps {

  val id = UUID.randomUUID().toString.substring(0, 8)

  val clientWallet: ClientWallet = new ClientWallet(id)


  "A Client Wallet" should "accept a lodgement" in {
    val client = "a"
    val clientb = "b"
    val firstTime = clientWallet.lodge(10, client)
    var bal: Option[Long] = clientWallet.balance(client)
    assert(bal.contains(10), "lodgement of 20")
    assert(firstTime.isSuccess)
    val secTimeDiffClient = clientWallet.lodge( 10, clientb)
    assert(secTimeDiffClient.isSuccess)
    bal = clientWallet.balance(client)
    assert(bal.contains(10), "lodgement")
    val differentIndex = clientWallet.lodge( 10, client)
    assert(differentIndex.isSuccess)
    bal = clientWallet.balance(client)
    assert(bal.contains(20), "lodgement of 20")

  }

  it should "facilitate a happy path transfer to existing client" in {
    val clientFrom = "a"
    val clientTo = "b"
    clientWallet.withdrawAll(clientTo)
    var b = clientWallet.balance(clientTo)
    assert(b.isEmpty )
    clientWallet.lodge( 10, clientTo)
    clientWallet.transfer(10, clientFrom, clientTo)
    b = clientWallet.balance(clientTo)
    assert(b.contains(20), "initial plus transfer")
  }

  it should "facilitate a happy path transfer to new client" in {
    val clientFrom = "b"
    val clientTo = "c"
    var b: Option[Long] = clientWallet.balance(clientTo)
    assert(b.isEmpty)
    clientWallet.transfer(10, clientFrom, clientTo)
    b = clientWallet.balance(clientTo)
    assert(b.contains(10), " transfer")
  }

  it should "fail a transfer if insufficient funds" in {
    val clientFrom = "a"
    val clientTo = "d"

    val t = clientWallet.transfer(10, clientFrom, clientTo)
    assert(t.isSuccess)
    val b = clientWallet.balance(clientTo)
    assert(b.contains(10), " transfer")

    val attempt = clientWallet.transfer(50, clientTo, clientFrom)
    assert(attempt.isFailure, "50 exceeds balance of sender")

  }

  it should "fail a transfer if sender non existent" in {
    val clientFrom = "z"
    val clientTo = "a"

    val t = clientWallet.transfer(10, clientFrom, clientTo)
    assert(t.isFailure)

  }

  it should "allow a withdrawal" in {
    val clientTo = "b"
    var t = clientWallet.withdraw(1, clientTo)
    assert(t.isSuccess)
    assert(t.get == 9)
    t = clientWallet.withdrawAll(clientTo)
    assert(t.isSuccess)
    assert(t.get == 0)
  }

}
