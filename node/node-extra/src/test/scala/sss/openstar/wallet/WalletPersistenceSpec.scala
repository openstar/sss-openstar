package sss.openstar.wallet

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.ancillary.ByteArrayComparisonOps
import sss.openstar.DummySeedBytes
import sss.openstar.balanceledger.{TxIndex, TxOutput}
import sss.openstar.contract.NullEncumbrance
import sss.openstar.db.DbWithMigrationFixture
import sss.openstar.ledger.LedgerId
import sss.openstar.wallet.WalletPersistence.Lodgement

import java.util.UUID

/**
  * Created by alan on 2/15/16.
  */

class WalletPersistenceSpec extends AnyFlatSpec with Matchers with DbWithMigrationFixture with ByteArrayComparisonOps {

  val wp = new WalletPersistence(UUID.randomUUID().toString.substring(0,8), db)

  val txId0 = DummySeedBytes(32)
  val txId1 = DummySeedBytes(32)

  val txIndex0 = TxIndex(txId0, 0)
  val txOutput0 = TxOutput(50, NullEncumbrance)

  val txIndex1 = TxIndex(txId1, 0)
  val txIndex2 = TxIndex(txId1, 1)
  val txIndex3 = TxIndex(txId1, 2)

  val txIndexOtherLedger = TxIndex(txId1, 3)

  implicit val ledgerId = LedgerId(1.byteValue())
  val ledgerId2 = LedgerId(2.byteValue())

  "A TxIndex " should " be persistable " in {
    assert(wp.listUnSpent(0).isEmpty)
    wp.track(Lodgement(txIndex0, txOutput0, 0))
  }


  it should " be retrievable " in {
    assert(wp.listUnSpent(0).size == 1)
    wp.listUnSpent(0).foreach { t =>
      assert(t.txIndex == txIndex0)
      assert(t.txOutput.toTxOutput == txOutput0)
      assert(t.inBlock == 0)
    }
  }

  it should "be ignored by other ledgerIds" in {
    assert(wp.listUnSpent(0)(ledgerId2).isEmpty)
  }

  it should "handle more than one index" in {
    wp.track(Lodgement(txIndex1, txOutput0, 0))
    assert(wp.listUnSpent(0).size == 2)
  }

  it should "handle more than one ledger" in {
    wp.track(Lodgement(txIndexOtherLedger, txOutput0, 0))(ledgerId2)
    assert(wp.listUnSpent(0)(ledgerId2).size == 1)
  }

  it should " allow an index to be marked spent " in {
    assert(wp.listUnSpent(0).exists(_.txIndex == txIndex0))
    wp.markSpent(txIndex0)
    assert(!wp.listUnSpent(0).exists(_.txIndex == txIndex0))
  }

  it should " allow an index in another ledger to be marked spent " in {
    assert(wp.listUnSpent(0)(ledgerId2).exists(_.txIndex == txIndexOtherLedger))
    wp.markSpent(txIndexOtherLedger)
    assert(!wp.listUnSpent(0)(ledgerId2).exists(_.txIndex == txIndexOtherLedger))
  }


  it should " ignore re marking an index spent " in {
    wp.markSpent(txIndex0)
    assert(!wp.listUnSpent(0).contains(txIndex0))
  }

  it should " allow marking a second index spent" in {
    wp.markSpent(txIndex1)
    wp.markSpent(txIndex0)
    assert(wp.listUnSpent(0).isEmpty)
  }

  it should " allow marking a second index on the same tx spent" in {
    wp.track(Lodgement(txIndex2, txOutput0, 0))
    wp.markSpent(txIndex1)
    wp.markSpent(txIndex2)
    assert(wp.listUnSpent(0).isEmpty)
  }

  it should "allow marking an unspent index 'in flight'" in {
    wp.track(Lodgement(txIndex3, txOutput0, 2))
    assert(wp.listUnSpent(0).isEmpty)
    assert(wp.listUnSpent(2).size == 1)

    // reclaim block is lower than spend block
    intercept[IllegalArgumentException] {
      wp.markInFlight(txIndex3, 1)
    }

    wp.markInFlight(txIndex3, 2)
    //in flight now not  unspent
    assert(wp.listUnSpent(2).isEmpty)

    wp.markUnSpent(txIndex3, 2)
    // in flight can be retrunred to spendable
    assert(wp.listUnSpent(3).size == 1)

    wp.markSpent(txIndex3)
    // Spent cannot be re spent.
    assert(wp.listUnSpent(3).isEmpty)

  }

  it should "prevent marking a spent index 'in flight'" in {
    intercept[IllegalArgumentException] {
      wp.markInFlight(txIndex3, 1)
    }
  }

}
