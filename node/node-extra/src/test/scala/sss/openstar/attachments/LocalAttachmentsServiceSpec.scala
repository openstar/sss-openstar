package sss.openstar.attachments

import com.google.common.io.ByteStreams
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.openstar.DummySeedBytes
import sss.openstar.attachments.AttachmentsService.EncryptedAttachment
import sss.openstar.crypto.AESDetails.AESEncodedKey
import sss.openstar.crypto.CBCStreamEncryption
import sss.openstar.db.DbWithMigrationFixture
import sss.openstar.util.GuidFromByteString

import java.io.ByteArrayInputStream
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import scala.language.postfixOps

class LocalAttachmentsServiceSpec extends AnyFlatSpec with Matchers with DbWithMigrationFixture {

  val tempFolder = Files.createTempDirectory("testAttachments")
  tempFolder.toFile.deleteOnExit()
  val persister = new FileStreamPersister(tempFolder.toFile)

  val sut = new LocalAttachmentsService(persister).forUser("hello")
  val guid = GuidFromByteString()
  val name = "name"
  val mimeType = "application/something"
  val name2 = "name2"
  val pKey = DummySeedBytes(32)
  val pKey2 = DummySeedBytes(32)
  val testStr = (0 to 1000).map(i => s"$i").mkString
  val testStr2 = (900 to 1000).map(i => s"$i").mkString
  val bytes = testStr.getBytes(StandardCharsets.UTF_8)
  val bytes2 = testStr2.getBytes(StandardCharsets.UTF_8)

  private def checkContents(
                     aFound: Option[EncryptedAttachment],
                     aKey: Array[Byte],
                     aTestStr: String) = {
    val decrypted = CBCStreamEncryption.decrypt(
      aFound.get.input,
      AESEncodedKey(aKey),
      aFound.get.iv
    )

    val decBytes = ByteStreams.toByteArray(decrypted)
    val retreived = new String(decBytes, StandardCharsets.UTF_8)

    assert(retreived == aTestStr)
  }

  "Attachments" should "support adding file" in {
    val someData = new ByteArrayInputStream(bytes)
    val res = sut.saveEncrypted(
      guid, name, mimeType, AESEncodedKey(pKey), someData
    )
    assert(res.isSuccess)

  }

  it should "finding a file and replacing older file with newer" in {
    val found = sut.find(guid, name)
    checkContents(found, pKey, testStr)
    val someData = new ByteArrayInputStream(bytes2)
    val update = sut.saveEncrypted(guid, name, mimeType, AESEncodedKey(pKey2), someData)
    val found2 = sut.find(guid, name)
    checkContents(found2, pKey2, testStr2)
  }

  it should "support removing a file by guid and name" in {
    val res = sut.delete(guid, name)
    assert(res.isSuccess)
    assert(res.get == 1)
  }

  it should "support finding/deleting all files under guid" in {
    val someData = new ByteArrayInputStream(bytes)
    val save1 = sut.saveEncrypted(
      guid, name, mimeType, AESEncodedKey(pKey), someData
    )
    val save2 = sut.saveEncrypted(
      guid, name2, mimeType, AESEncodedKey(pKey), someData
    )
    //now there are 2
    val found = sut.filter(guid)
    assert(found.size == 2)
    val res = sut.delete(guid)
    assert(res.isSuccess)
    assert(res.get == 2)
  }

}
