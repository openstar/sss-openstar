package sss.openstar.attachments

import com.google.common.io.ByteStreams
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import sss.ancillary._
import sss.openstar.DummySeedBytes
import sss.openstar.attachments.AttachmentsService.EncryptedAttachment
import sss.openstar.attachments.Tester.config
import sss.openstar.crypto.AESDetails.AESEncodedKey
import sss.openstar.crypto.{AESDetails, CBCStreamEncryption}
import sss.openstar.db.DbWithMigrationFixture
import sss.openstar.util.GuidFromByteString

import java.io.ByteArrayInputStream
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import scala.language.postfixOps
import scala.util.control.NonFatal

class ITRemoteAttachmentsServiceSpec extends AnyFlatSpec with Matchers with DbWithMigrationFixture {

  val tempFolder = Files.createTempDirectory("testAttachments")
  tempFolder.toFile.deleteOnExit()
  val persister = new FileStreamPersister(tempFolder.toFile)

  val localAttachmentsService = new LocalAttachmentsService(persister)
  val localAttachments = localAttachmentsService.forUser("hello")
  val remoteAttachmentsServlet =
    ServletContext(RemoteAttachmentsServlet.servletContext, "",
      InitServlet(new RemoteAttachmentsServlet(localAttachmentsService), "/*")
    )
  val guid = GuidFromByteString()
  val name = "name"
  val mimeType = "application/something"

  val pKey = AESDetails.aesKeyFromByteArray(DummySeedBytes(16))

  val testStr = (0 to 1000).map(i => s"$i").mkString

  val bytes = testStr.getBytes(StandardCharsets.UTF_8)


  private def checkContents(
                     aFound: Option[EncryptedAttachment],
                     aKey: Array[Byte],
                     aTestStr: String) = {
    val decrypted = CBCStreamEncryption.decrypt(
      aFound.get.input,
      AESEncodedKey(aKey),
      aFound.get.iv
    )

    val decBytes = ByteStreams.toByteArray(decrypted)
    val retreived = new String(decBytes, StandardCharsets.UTF_8)

    assert(retreived == aTestStr)
  }

  "Remote Attachments" should "be downloadable" in {
    val someData = new ByteArrayInputStream(bytes)
    val res = localAttachments.saveEncrypted(
      guid, name, mimeType, pKey, someData
    )
    assert(res.isSuccess)

    val encLen = res.get

    implicit val httpConfig = DynConfig[ServerConfig](config.getConfig("httpServerConfig"))

    val server = ServerLauncher(
      remoteAttachmentsServlet.toHandler)

    val remoteService = new RemoteAttachmentsService(_ => Some("http://localhost:8080/attachments"))

    server.start()

    try {
      val foundOpt = remoteService.find("notlocal", guid, name)
      assert(foundOpt.isDefined)
      assert(foundOpt.get.encryptedLen == encLen)
      assert(foundOpt.get.mimeType == mimeType)
      assert(foundOpt.get.name == name)
      checkContents(foundOpt, pKey.value, testStr)

    } finally {
      try {
        server.stop()
      } catch { case
        NonFatal(t) =>
      }

    }
  }

}
