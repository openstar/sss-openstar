package sss.openstar.httpconfig

import akka.actor.ActorSystem
import akka.http.scaladsl.model.{HttpMethods, HttpRequest, HttpResponse, Uri}
import akka.http.scaladsl.server.Directives.concat
import sss.ancillary.{Configure, Logging}
import sss.openstar.http.RpcServer
import sss.openstar.http.RpcServer.{Bindings, RpcServerConfig}

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future, Promise}
import scala.util.Try

object HttpServiceBuilder extends Configure with Logging {


  trait ExecutionContextBuilder {
    implicit val cpuExecutionContext: ExecutionContext
  }

  trait ActorSystemBuilder {
    implicit val as: ActorSystem
  }

  trait RpcServerBuilder {
    self: ActorSystemBuilder with
      ExecutionContextBuilder =>

    private val maxWaitForTermination = 10.seconds

    private val triggerShutdownPromise = Promise[HttpResponse]()

    val stopUrl = "stop"
    val aliveUrl = "alive"


    lazy val utilHandlers: PartialFunction[HttpRequest, Future[HttpResponse]] = {
      case HttpRequest(HttpMethods.GET, Uri.Path(s"/${`aliveUrl`}"), _, _, _) =>
        Future.successful(HttpResponse())
      case HttpRequest(HttpMethods.GET, Uri.Path(s"/${`stopUrl`}"), _, _, _) =>
        triggerShutdownPromise.complete(Try(HttpResponse()))
        triggerShutdownPromise.future

    }

    //Add new handlers to below or replace altogether
    val rpcServiceHandlers: PartialFunction[HttpRequest, Future[HttpResponse]]

    lazy val rpcTlsServiceHandlers: PartialFunction[HttpRequest, Future[HttpResponse]] = rpcServiceHandlers

    lazy val rpcConfig: RpcServerConfig = RpcServerConfig.from(config.getConfig("rpc"))

    def createRpcServer(): RpcServer = {
      val rpcServer = new RpcServer(
        rpcConfig,
        rpcServiceHandlers,
        rpcTlsServiceHandlers
      )

      triggerShutdownPromise.future.onComplete { _ =>
        rpcServer.stop(maxWaitForTermination).andThen { seqTerms =>
          log.debug(s"Terminated http bindings $seqTerms")
          as.terminate().onComplete(_ => log.info(s"Terminated actorSystem ${as.name}"))
        }
      }
      rpcServer
    }

    def runRpcServer(): Unit = {
      val startupWaitDuration = 5.seconds
      val server = createRpcServer()
      val Bindings(httpF, httpsFOpt) = server.start()
      Await.ready(httpF, startupWaitDuration)
      httpsFOpt.foreach(Await.ready(_, startupWaitDuration))
    }

  }


  trait RouteBuilder {

    val delim = "'|'"
    val eol = ","
    val timeout = 200
    val configContextPath = "config"


    protected val httpConfigRouteImpl = new HttpConfigRoute(configContextPath, eol, delim, timeout)
    protected val externalIpRoute = new ExternalIpRoute(configContextPath)
    val configRoute = concat(externalIpRoute.configRoute, httpConfigRouteImpl.configRoute)

  }

}
