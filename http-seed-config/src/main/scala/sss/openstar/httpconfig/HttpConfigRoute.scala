package sss.openstar.httpconfig

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import sss.ancillary.{Logging, Memento}

import java.io.File
import java.util.concurrent.TimeUnit
import java.util.concurrent.locks.ReentrantLock
import scala.collection.concurrent.TrieMap

class HttpConfigRoute(configContextPath: String, eol: String, delim: String, timeout: Long) extends Logging {

  val resetParam = "reset"
  val payloadParam = "payload"
  val idParam = "id"
  val parentDirectoryPattern = ".."

  private val synchronizationMap: TrieMap[String, ReentrantLock] = TrieMap[String, ReentrantLock]()

  private[httpconfig] def serializeAsResult(asMap: Map[String, String]): String = asMap.values.mkString(":")

  private[httpconfig] def serialize(asMap: Map[String, String]): String = asMap.foldLeft("") {
    case (acc, (n, v)) => {
      s"$acc$n$delim$v$eol"
    }
  }

  private[httpconfig] def deserialize(serialisedString: String): Map[String, String] =
    if (serialisedString.isEmpty) Map.empty else {
      val aryLines = serialisedString.split(eol)
      val aryTuples = aryLines.map(_.split(delim))
      require(aryTuples.forall(_.length == 3), s"Badly delimited string $serialisedString split into aryTuples")
      aryTuples.map(ary => (ary.head, ary.last)).toMap
    }


  protected def write(isReset: Boolean, nameValues: Map[String, String], memento: Memento): Unit = {

    if (isReset) {
      memento.clear
    }

    val currMementoOpt = memento.read
    val mapOfExisting = currMementoOpt.map(deserialize).getOrElse(Map.empty)

    val newUpdatedMap = mapOfExisting ++ nameValues
    val serialisedToString = serialize(newUpdatedMap)
    //println(s"serialisedToString $serialisedToString")
    memento.write(serialisedToString)

  }

  val configRoute: Route = {
    get {
      pathPrefix(configContextPath / Remaining) { remainingPathToFile: String =>
        parameters(resetParam.optional, idParam.withDefault(""), payloadParam.withDefault("")) { (resetOpt, id, payload) =>
          val resetFlag = resetOpt.isDefined

          if (remainingPathToFile.contains(parentDirectoryPattern)) {
            complete(StatusCodes.BadRequest)
          }

          val lock = new ReentrantLock()
          //the lock that we get here may be from different thread
          val lockForFile = synchronizationMap.getOrElseUpdate(remainingPathToFile, lock)

          if (lockForFile.tryLock(timeout, TimeUnit.MILLISECONDS)) {
            //either we have a lock or other process that acquired lock is dead
            log.debug(s"Reset $resetFlag, id $idParam payload $payloadParam")
            try {
              val namedValues: Map[String, String] = if (id.nonEmpty) Map(id -> payload) else Map.empty

              //val pathToFile = new File(rootPath, remainingPathToFile)
              val name = new File(remainingPathToFile)
              val group = Option(name.getParent).filter(_.nonEmpty)
              val memento = Memento(name.getName, group)

              write(resetFlag, namedValues, memento)
              val readBack = memento.read
              val readBackMap = readBack.map(deserialize).getOrElse(Map.empty)
              val serializedResult = serializeAsResult(readBackMap)
              complete(serializedResult)
            } finally {
              lockForFile.unlock()
            }
          } else {
            //we do not have a lock, modifying things is dangerous, respond with sth appropriate / do a loop?
            complete(StatusCodes.Conflict)
          }
        }
      }
    }
  }

}
