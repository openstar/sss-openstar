package sss.openstar.httpconfig

import sss.ancillary.Logging
import akka.http.scaladsl.server.Directives._

class ExternalIpRoute(configContextPath: String) extends Logging {

  val configRoute = {
    get {
      path(configContextPath / "cgi-bin" / "externalIp") {
        extractClientIP {
          remote => {
            val ip = remote.toOption.map(_.getHostAddress).getOrElse("")
            log.debug(s"Remote IP detected: $ip")
            complete(ip)
          }
        }
      }
    }
  }
}
