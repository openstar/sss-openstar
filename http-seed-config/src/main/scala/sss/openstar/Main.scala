package sss.openstar

import akka.actor.ActorSystem
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.http.scaladsl.server.Route
import sss.openstar.httpconfig.HttpServiceBuilder.{ActorSystemBuilder, ExecutionContextBuilder, RouteBuilder, RpcServerBuilder}

import scala.concurrent.{ExecutionContext, Future}

object Main {
  def main(args: Array[String]): Unit = {

    object System extends
      RpcServerBuilder
      with ActorSystemBuilder
      with ExecutionContextBuilder
      with RouteBuilder {

      override implicit val as: ActorSystem = ActorSystem("sss-http-config")
      override implicit val cpuExecutionContext: ExecutionContext = as.dispatcher

      override val rpcServiceHandlers: PartialFunction[HttpRequest, Future[HttpResponse]] =
        utilHandlers orElse PartialFunction.fromFunction(Route.toFunction(configRoute))

    }

    System.runRpcServer()

  }
}