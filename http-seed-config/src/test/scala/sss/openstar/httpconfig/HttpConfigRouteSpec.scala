package sss.openstar.httpconfig

import akka.http.scaladsl.model.{ContentType, HttpCharsets, MediaType, StatusCodes}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.http.scaladsl.server._
import sss.openstar.httpconfig.HttpServiceBuilder.RouteBuilder
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class HttpConfigRouteSpec extends AnyWordSpec with Matchers with ScalatestRouteTest with RouteBuilder {

  "The service" should {

    "test map to string serialization" in {
      val m = Map(("a" -> "123"), ("b", "456"))
      val asString = httpConfigRouteImpl.serialize(m)
      val reconstituted = httpConfigRouteImpl.deserialize(asString)
      assert(m == reconstituted)

      val s = s"a${delim}123${eol}b${delim}456${eol}"
      val s2 = s"a${delim}123${eol}b${delim}456"

      val asMap = httpConfigRouteImpl.deserialize(s)
      val asMap2 = httpConfigRouteImpl.deserialize(s2)
      assert(asMap == Map(("a" -> "123"), ("b", "456")))
      assert(asMap2 == Map(("a" -> "123"), ("b", "456")))
      val asString2 = httpConfigRouteImpl.serialize(asMap)
      assert(s == asString2)
    }

    "return the response sent when reset is used" in {
      // tests:
      Get(s"/$configContextPath/networkname/0.0.1/chainOriginators?reset&id=alice&payload=alice:1234567890") ~> configRoute ~> check {
        responseAs[String] shouldEqual "alice:1234567890"
      }
    }

    "aggregrate gets when reset not used" in {
      // tests:
      Get(s"/$configContextPath/networkname/0.0.1/chainOriginators?&id=bob&payload=bob:0987654321") ~> configRoute ~> check {
        responseAs[String] shouldEqual "alice:1234567890:bob:0987654321"
      }
    }

    "gets when no set is present" in {
      // tests:
      Get(s"/$configContextPath/networkname/0.0.1/chainOriginators") ~> configRoute ~> check {
        responseAs[String] shouldEqual "alice:1234567890:bob:0987654321"
        contentType shouldEqual ContentType(MediaType.text("plain"), HttpCharsets.`UTF-8`)
      }
    }

    "reset when no set is present" in {
      // tests:
      Get(s"/$configContextPath/networkname/0.0.1/chainOriginators?reset") ~> configRoute ~> check {
        httpConfigRouteImpl.deserialize(responseAs[String]) shouldEqual Map.empty[String, String]
      }
    }


    "leave GET requests to other paths unhandled" in {
      // tests:
      Get("/kermit") ~> configRoute ~> check {
        handled shouldBe false
      }
    }

    "return a MethodNotAllowed error for PUT requests to the root path" in {
      // tests:
      Put() ~> Route.seal(configRoute) ~> check {
        status shouldEqual StatusCodes.MethodNotAllowed
        responseAs[String] shouldEqual "HTTP method not allowed, supported methods: GET"
      }
    }
  }
}