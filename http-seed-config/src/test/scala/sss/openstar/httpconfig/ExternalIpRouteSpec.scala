package sss.openstar.httpconfig

import akka.http.scaladsl.model.AttributeKeys.remoteAddress
import akka.http.scaladsl.model.RemoteAddress
import akka.http.scaladsl.model.headers.{`Remote-Address`, `X-Forwarded-For`, `X-Real-Ip`}

import akka.http.scaladsl.testkit.ScalatestRouteTest
import sss.openstar.httpconfig.HttpServiceBuilder.RouteBuilder
import org.scalatest.matchers.should.Matchers

import scala.annotation.nowarn
import org.scalatest.wordspec.AnyWordSpec

import java.net.InetAddress

class ExternalIpRouteSpec extends AnyWordSpec with Matchers with ScalatestRouteTest with RouteBuilder {

  val ip = "192.168.0.1"
  val remote = RemoteAddress(InetAddress.getByName(ip))
  val remote2 = RemoteAddress(InetAddress.getByName("192.168.0.2"))

  "The service" should {

    "return empty if can not get ip" in {
      Get(s"/$configContextPath/cgi-bin/externalIp") ~> configRoute ~> check {
        responseAs[String] shouldEqual ""
      }
    }

    "return external ip from XForwardedFor header" in {
      Get(s"/$configContextPath/cgi-bin/externalIp").withHeaders(`X-Forwarded-For`(remote)) ~> configRoute ~> check {
        responseAs[String] shouldEqual ip
      }
    }

    "return external ip from XRealIP header" in {
      Get(s"/$configContextPath/cgi-bin/externalIp").withHeaders(`X-Real-Ip`(remote)) ~> configRoute ~> check {
        responseAs[String] shouldEqual ip
      }
    }

    "return external ip from XForwardedFor header if both XForwardedFor and XRealIP set" in {
      Get(s"/$configContextPath/cgi-bin/externalIp").withHeaders(`X-Forwarded-For`(remote), `X-Real-Ip`(remote2)) ~> configRoute ~> check {
        responseAs[String] shouldEqual ip
      }
    }

    "return external ip from Remote-Address header" in {
      // Remote-Address header is deprecated, but still could be set, so let's test it's behavior
      val header = `Remote-Address`(remote) : @nowarn
      Get(s"/$configContextPath/cgi-bin/externalIp").withHeaders(header) ~> configRoute ~> check {
        responseAs[String] shouldEqual ip
      }
    }

    "return external ip from remote-address attribute" in {
      Get(s"/$configContextPath/cgi-bin/externalIp") ~> addAttribute(remoteAddress, remote) ~> configRoute ~> check {
        responseAs[String] shouldEqual ip
      }
    }

    "return external ip from XRealIP header if both XRealIP and Remote-Address set" in {
      Get(s"/$configContextPath/cgi-bin/externalIp").withHeaders(`X-Real-Ip`(remote)) ~> addAttribute(remoteAddress, remote2) ~> configRoute ~> check {
        responseAs[String] shouldEqual ip
      }
    }

    "leave GET requests to other paths unhandled" in {
      Get("/foo") ~> configRoute ~> check {
        handled shouldBe false
      }
    }
  }
}