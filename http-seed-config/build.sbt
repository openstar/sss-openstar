import sbt.Keys.{libraryDependencies, resolvers}

val excludeJetty = ExclusionRule(organization = "org.eclipse.jetty.aggregate")

val typesafeLoggingVersion = "3.9.5"

libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-stream" % Vers.akkaVer,
      "com.typesafe.akka" %% "akka-http" % Vers.akkaHttp,
      "com.typesafe.akka" %% "akka-http-xml" % Vers.akkaHttp,
      "com.typesafe.akka" %% "akka-discovery" % Vers.akkaVer,
      "com.typesafe.akka" %% "akka-stream-testkit" % Vers.akkaVer % Test,
      "com.typesafe.akka" %% "akka-http-testkit" % Vers.akkaHttp % Test,
      // https://mvnrepository.com/artifact/com.typesafe.akka/akka-http2-support
      "com.typesafe.akka" %% "akka-http2-support" % Vers.akkaHttp,
      "ch.qos.logback" % "logback-classic" % Vers.logbackClassicVersion,
      // https://mvnrepository.com/artifact/com.typesafe.scala-logging/scala-logging
      "com.typesafe.scala-logging" %% "scala-logging" % typesafeLoggingVersion,
      "com.mcsherrylabs" %% "sss-ancillary" % Vers.ancillaryVer excludeAll(excludeJetty)
    )


enablePlugins(JavaAppPackaging, DockerPlugin)

dockerExposedPorts ++= Seq(2000)

dockerRepository := Some("registry.gitlab.com")

Docker / packageName := "openstar/sss-openstar/http-seed-config"

Universal / mappings += {
  val conf = sourceDirectory.value / "main" / "resources" / "application.conf"
  conf -> "conf/application.conf"
}

dockerBaseImage := "openjdk:11"
