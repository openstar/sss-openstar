
# Openstar 

This is the repository for the Openstar DLT framework.

This is a standard `sbt` project from the point of view of build and test. 

The jars are published at `nexus.iog.solutions`

However, the best way to get started is to clone the template application included 
in this repository (and then replace the Openstar module dependencies with versioned jar dependencies).


To get a basic understanding of the framework see the 
<a href="https://gitlab.com/openstar/sss-openstar/-/wikis/home">Wiki</a>

